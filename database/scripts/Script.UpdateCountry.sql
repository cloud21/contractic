﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

UPDATE c SET c.HtmlCode = Code.Html, c.Active = 1 FROM dbo.Country c 
	INNER JOIN (VALUES('GBP', '£'),('EUR', '€'), ('USD', '$'), ('YEN', '¥')) AS Code(Currency, Html)
ON c.CurrencyCode = Code.Currency;