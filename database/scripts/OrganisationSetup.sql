INSERT INTO dbo.Organisation(Name, Active, Created, Code) SELECT org.Name, org.Active, org.Created, org.Code FROM 
	(VALUES ('Cloud21', 1, GETDATE(), 'cloud21')) AS org(Name, Active, Created, Code)
	LEFT JOIN dbo.Organisation o ON org.Name = o.Name
	WHERE o.Name IS NULL;

-- Update/Insert Module
MERGE INTO dbo.Module AS Target
USING (VALUES ('Contract','Contract Management'), 
			('Supplier', 'Supplier Management'), 
			('Organisation', 'Organisation Management'),
			('Person', 'Person Management'),
			('Task', 'Task Management'),
			('Report', 'Report Management'),
			('ContractDetail', 'Contract Properties and Details')
		)
       AS Source (Name, Description)
ON Target.Name = Source.Name
WHEN MATCHED THEN
UPDATE SET Description = Source.Description
WHEN NOT MATCHED BY TARGET THEN
INSERT (Name, Description) VALUES (Name, Description);
GO

-- System UserRoles
INSERT INTO dbo.UserRole(Name, Description, Active, OrganisationId)
SELECT rle.Name, rle.Description, rle.Active, o.Id FROM 
	(VALUES ('Administrator', 'System Administrator', 1)) AS rle(Name, Description, Active)
	CROSS JOIN dbo.Organisation o
	LEFT JOIN dbo.UserRole ur ON rle.Name = ur.Name AND ur.OrganisationId = o.Id
	WHERE ur.Name IS NULL;

-- System groupsets
INSERT INTO dbo.GroupSet(Name, Description, Active, OrganisationId)
	SELECT gsn.Name, gsn.Name, 1, o.Id FROM (VALUES('DEPARTMENT'), ('PROCEDURE'), ('CONTRACT_REFERENCE_FORMAT')) AS gsn(Name)
	CROSS JOIN dbo.Organisation o
	LEFT JOIN dbo.GroupSet gs ON gs.Name = gsn.Name
	WHERE gs.Name IS NULL;

-- System Groups
INSERT INTO [dbo].[Group](Name, Active, ModuleId, OrganisationId, Description, System)
SELECT g.Name, 1, m.Id, o.Id, g.Name, 1 FROM (VALUES('VALUE'), ('SUMMARY'), ('CCN'), ('CONTRACT_TERM_PERIOD'), ('CONTRACT_TERM_NOTICE'), ('CONTRACT_TERM_GOTO_MARKET')) AS g(Name)
CROSS JOIN Organisation o
INNER JOIN Module m ON m.Name = 'Contract'
LEFT JOIN [dbo].[Group] cd ON cd.Name = g.Name AND m.Id = cd.ModuleId AND o.Id = cd.OrganisationId
WHERE cd.Id IS NULL;

-- System reports
INSERT INTO dbo.Report(Name, Description, Command, Active, ExternalRef)
	SELECT rpt.* FROM (VALUES('Spend Analysis', 'Spend Analysis', 'EXEC sp_SpendAnalysis :Params, :OrganisationId', 1, 'spend-analysis'),
					('User Contract Actity', 'Report showing recent contract activity by user', 'EXEC dbo.sp_UserContractActivity :Params, :OrganisationId', 1, 'user-contract-activity'),
					('Contract Statistics', 'Organisation contract statistics', 'EXEC [dbo].[sp_ContractStatistics] :Params, :OrganisationId', 1, 'contract-statistics'),
					('User Task Activity', 'Report showing recent and upcoming tasks', 'EXEC dbo.sp_UserTaskActivity :Params, :OrganisationId', 1, 'user-task-activity'))
					AS rpt(Name, Description, Command, Active, ExternalRef)
		LEFT JOIN dbo.Report r ON r.Name = rpt.Name
		WHERE r.Name IS NULL;

-- Global reports
INSERT INTO dbo.ReportOrganisation(Name, Description, ReportId, OrganisationId, Active, Created)
SELECT r.Name, r.Description, r.Id, o.Id, 1, GETDATE() FROM 
	(VALUES
		('Spend Analysis'),
		('User Contract Actity'),
		('Contract Statistics'),
		('User Task Activity')) AS ReportName(Name)
CROSS JOIN Organisation o
INNER JOIN Report r ON r.Name = ReportName.Name
LEFT JOIN dbo.ReportOrganisation ro ON ro.ReportId = r.Id
WHERE ro.Id IS NULL;

-- Insert an admin user (username= username@domain.com, password= password)
INSERT INTO dbo.[User](LoginId, Password, Created, Active, PersonId, RoleId)
SELECT usr.LoginId, usr.Password, GETDATE(), 1 AS Active, p.Id AS PersonId, ur.Id AS RoleId
 FROM (VALUES('username@domain.com', '$2a$08$7mMAdT465E8lf.gp3QN2QOaJgEL6dQVBJKNGfdgoYxKjUDEO9QuJG')) AS usr(LoginId, Password)
 CROSS JOIN dbo.Organisation o
 INNER JOIN (SELECT MIN(p.Id) Id, p.OrganisationId FROM dbo.Person p GROUP BY OrganisationId) p ON p.OrganisationId = o.Id
 INNER JOIN dbo.UserRole ur ON ur.Name = 'Administrator' AND ur.OrganisationId = o.Id
 LEFT JOIN dbo.[User] u ON u.LoginId = usr.LoginId
 WHERE u.LoginId IS NULL;