USE [master]
GO
/****** Object:  Database [c21cms]    Script Date: 28/04/2016 20:44:49 ******/
CREATE DATABASE [c21cms]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'c2cms', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\c2cms.mdf' , SIZE = 569344KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'c2cms_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\c2cms_log.ldf' , SIZE = 5605504KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [c21cms] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [c21cms].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [c21cms] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [c21cms] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [c21cms] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [c21cms] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [c21cms] SET ARITHABORT OFF 
GO
ALTER DATABASE [c21cms] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [c21cms] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [c21cms] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [c21cms] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [c21cms] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [c21cms] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [c21cms] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [c21cms] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [c21cms] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [c21cms] SET  DISABLE_BROKER 
GO
ALTER DATABASE [c21cms] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [c21cms] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [c21cms] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [c21cms] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [c21cms] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [c21cms] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [c21cms] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [c21cms] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [c21cms] SET  MULTI_USER 
GO
ALTER DATABASE [c21cms] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [c21cms] SET DB_CHAINING OFF 
GO
ALTER DATABASE [c21cms] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [c21cms] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [c21cms] SET DELAYED_DURABILITY = DISABLED 
GO
USE [c21cms]
GO
/****** Object:  UserDefinedFunction [dbo].[ToProperCase]    Script Date: 28/04/2016 20:44:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ToProperCase](@string VARCHAR(255)) RETURNS VARCHAR(255)
AS
BEGIN
  DECLARE @i INT           -- index
  DECLARE @l INT           -- input length
  DECLARE @c NCHAR(1)      -- current char
  DECLARE @f INT           -- first letter flag (1/0)
  DECLARE @o VARCHAR(255)  -- output string
  DECLARE @w VARCHAR(10)   -- characters considered as white space

  SET @w = '[' + CHAR(13) + CHAR(10) + CHAR(9) + CHAR(160) + ' ' + ']'
  SET @i = 0
  SET @l = LEN(@string)
  SET @f = 1
  SET @o = ''

  IF @string IS NULL RETURN NULL;

  WHILE @i <= @l
  BEGIN
    SET @c = SUBSTRING(@string, @i, 1)
    IF @f = 1 
    BEGIN
     SET @o = @o + @c
     SET @f = 0
    END
    ELSE
    BEGIN
     SET @o = @o + LOWER(@c)
    END

    IF @c LIKE @w SET @f = 1

    SET @i = @i + 1
  END

  RETURN @o
END
GO
/****** Object:  Table [dbo].[Address]    Script Date: 28/04/2016 20:44:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Address](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Section1] [varchar](255) NOT NULL,
	[Section2] [varchar](255) NULL,
	[Section3] [varchar](255) NULL,
	[Section4] [varchar](255) NULL,
	[Section5] [varchar](255) NULL,
	[Section6] [varchar](255) NULL,
	[Section7] [varchar](255) NULL,
	[Section8] [varchar](255) NULL,
	[OrganisationId] [bigint] NOT NULL,
	[TypeId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AddressType]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AddressType](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [tinyint] NOT NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Audit]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Audit](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[ExternalId] [bigint] NOT NULL,
	[AuditActionId] [bigint] NOT NULL,
	[ModuleId] [bigint] NOT NULL,
	[UserId] [bigint] NULL,
	[Created] [datetime] NULL,
	[ChangeField] [varchar](4096) NULL,
	[ChangeFrom] [varchar](max) NULL,
	[ChangeTo] [varchar](max) NULL,
	[OrganisationId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AuditAction]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AuditAction](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](255) NULL,
	[Format] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Active] [tinyint] NOT NULL DEFAULT ((1)),
	[ExternalId] [bigint] NOT NULL,
	[AddressId] [bigint] NOT NULL,
	[ModuleId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contract]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Contract](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[SupplierId] [bigint] NOT NULL,
	[OwnerId] [bigint] NOT NULL,
	[GroupId] [bigint] NULL,
	[StatusId] [bigint] NOT NULL,
	[TypeId] [bigint] NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[Reference] [varchar](255) NOT NULL,
	[OrganisationId] [bigint] NOT NULL,
	[Description] [varchar](4096) NULL,
	[Value] [numeric](30, 10) NULL,
	[ValueUOM] [varchar](10) NULL,
	[Term] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContractChange]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractChange](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[CustomerChangeId] [varchar](255) NOT NULL,
	[ApprovalDate] [datetime] NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[Description] [varchar](max) NULL,
	[ContractId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContractDetail]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractDetail](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](255) NULL,
	[Value] [varchar](max) NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[ContractId] [bigint] NOT NULL,
	[GroupId] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContractDetailGroup]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractDetailGroup](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Description] [varchar](max) NULL,
	[Active] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContractGroup]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractGroup](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Description] [varchar](max) NULL,
	[Active] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContractReferenceFormat]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractReferenceFormat](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Format] [varchar](100) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Description] [varchar](max) NULL,
	[Default] [tinyint] NULL,
	[Active] [tinyint] NULL,
	[ContractTypeId] [bigint] NULL,
	[OrganisationId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContractType]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractType](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Description] [varchar](max) NULL,
	[Active] [tinyint] NOT NULL DEFAULT ((1)),
	[OrganisationId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_ContractType_Name_Organisation] UNIQUE NONCLUSTERED 
(
	[Name] ASC,
	[OrganisationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Country]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [bigint] IDENTITY(100,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[CurrencyCode] [varchar](50) NOT NULL,
	[CurrencyName] [varchar](255) NOT NULL,
	[DialingCode] [varchar](20) NULL,
	[HtmlCode] [varchar](20) NULL,
	[Active] [tinyint] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Document]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Document](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Title] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Received] [datetime] NOT NULL,
	[FilePath] [nvarchar](max) NOT NULL,
	[ExternalId] [bigint] NOT NULL,
	[DocumentTypeId] [bigint] NOT NULL,
	[StatusId] [bigint] NULL,
	[ModuleId] [bigint] NOT NULL,
	[FileType] [varchar](255) NULL,
	[FileName] [varchar](max) NULL,
	[OrganisationId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentType]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentType](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[OrganisationId] [bigint] NOT NULL,
	[Active] [tinyint] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_DocumentType_Name_OrganisationId] UNIQUE NONCLUSTERED 
(
	[Name] ASC,
	[OrganisationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Group]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Group](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [tinyint] NULL,
	[ModuleId] [bigint] NULL,
	[OrganisationId] [bigint] NOT NULL,
	[Description] [varchar](255) NULL,
	[System] [tinyint] NULL,
	[GroupSetId] [bigint] NULL,
	[ParentId] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_GroupSetId_OrganisationId_ModuleId_Name_Description] UNIQUE NONCLUSTERED 
(
	[GroupSetId] ASC,
	[OrganisationId] ASC,
	[ModuleId] ASC,
	[Name] ASC,
	[Description] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupObject]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupObject](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[ModuleId] [bigint] NOT NULL,
	[ExternalId] [bigint] NOT NULL,
	[Active] [tinyint] NULL,
	[GroupSetId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[GroupSetId] ASC,
	[ModuleId] ASC,
	[ExternalId] ASC,
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupSet]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GroupSet](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](255) NULL,
	[Active] [tinyint] NULL,
	[OrganisationId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[OrganisationId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupUser]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupUser](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Active] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Module]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Module](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UNQ_ModuleName] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Note]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Note](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Header] [varchar](255) NOT NULL,
	[Detail] [varchar](max) NOT NULL,
	[ExternalId] [bigint] NOT NULL,
	[NoteTypeId] [bigint] NOT NULL,
	[ModuleId] [bigint] NOT NULL,
	[Active] [tinyint] NULL DEFAULT ((1)),
	[UserId] [bigint] NULL,
	[Created] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NoteType]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NoteType](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [tinyint] NULL DEFAULT ((1)),
	[OrganisationId] [bigint] NOT NULL,
	[Description] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC,
	[OrganisationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Organisation]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Organisation](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Active] [tinyint] NOT NULL,
	[Code] [varchar](255) NULL,
	[Profile] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrganisationModule]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrganisationModule](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Effective] [datetime] NULL,
	[Active] [tinyint] NULL,
	[OrganisationId] [bigint] NULL,
	[ModuleId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Person]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Person](
	[Id] [bigint] IDENTITY(10000,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](255) NOT NULL,
	[MiddleName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[JobTitle] [nvarchar](255) NULL,
	[Gender] [char](1) NULL,
	[Created] [datetime] NOT NULL,
	[Active] [tinyint] NULL DEFAULT ((1)),
	[OrganisationId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Report]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Report](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Description] [varchar](4096) NOT NULL,
	[Command] [varchar](max) NULL,
	[Active] [tinyint] NOT NULL,
	[ExternalRef] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Report_ExternalRef] UNIQUE NONCLUSTERED 
(
	[ExternalRef] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Report_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReportOrganisation]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReportOrganisation](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[Description] [varchar](4096) NULL,
	[Schedule] [varchar](4096) NULL,
	[ReportId] [bigint] NOT NULL,
	[OrganisationId] [bigint] NOT NULL,
	[GroupId] [bigint] NULL,
	[OwnerId] [bigint] NULL,
	[Active] [tinyint] NOT NULL,
	[Created] [datetime] NOT NULL,
	[LastRun] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_ReportOrganisation_ReportId_OrganisationId] UNIQUE NONCLUSTERED 
(
	[ReportId] ASC,
	[OrganisationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReportType]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReportType](
	[Id] [bigint] IDENTITY(100,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Description] [varchar](4096) NOT NULL,
	[Active] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Status]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [tinyint] NULL DEFAULT ((1)),
	[ModuleId] [bigint] NOT NULL,
	[OrganisationId] [bigint] NOT NULL,
	[Description] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[OrganisationId] ASC,
	[ModuleId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Supplier](
	[Id] [bigint] IDENTITY(10000,1) NOT NULL,
	[Reference] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Created] [datetime] NOT NULL,
	[Active] [tinyint] NULL DEFAULT ((1)),
	[ExternalRef] [varchar](255) NULL,
	[OrganisationId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierPerson]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SupplierPerson](
	[Id] [bigint] IDENTITY(10000,1) NOT NULL,
	[PersonId] [bigint] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Active] [tinyint] NULL,
	[ExternalRef] [varchar](255) NULL,
	[OrganisationId] [bigint] NOT NULL,
	[SupplierId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Task]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ARITHABORT ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Task](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[Description] [varchar](max) NULL,
	[Created] [datetime] NOT NULL,
	[StatusId] [bigint] NULL,
	[ParentId] [bigint] NULL,
	[ExternalId] [bigint] NOT NULL,
	[ModuleId] [bigint] NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[OrganisationId] [bigint] NOT NULL,
	[Number]  AS ([Id]),
	[RepeatStart] [datetime] NULL,
	[RepeatEnd] [datetime] NULL,
	[RepeatYear] [varchar](10) NULL,
	[RepeatMonth] [varchar](2) NULL,
	[RepeatWeek] [varchar](2) NULL,
	[RepeatDay] [varchar](2) NULL,
	[RepeatDate] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Task_Number_OrganisationId] UNIQUE NONCLUSTERED 
(
	[Number] ASC,
	[OrganisationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaskAlert]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaskAlert](
	[Id] [bigint] IDENTITY(100,1) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[Year] [varchar](10) NULL,
	[Month] [varchar](2) NULL,
	[Week] [varchar](2) NULL,
	[Date] [datetime] NULL,
	[TaskId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaskContact]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskContact](
	[Id] [bigint] IDENTITY(100,1) NOT NULL,
	[ContactId] [bigint] NOT NULL,
	[TaskId] [bigint] NOT NULL,
	[Updated] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ContactId] ASC,
	[TaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[LoginId] [varchar](255) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[Created] [datetime] NULL,
	[Profile] [text] NULL,
	[Active] [tinyint] NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[LastUpdated] [datetime] NULL,
	[RoleId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_User_LoginId] UNIQUE NONCLUSTERED 
(
	[LoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserPermission]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPermission](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[ModuleId] [bigint] NOT NULL,
	[ExternalId] [bigint] NULL,
	[Read] [tinyint] NOT NULL,
	[Write] [tinyint] NOT NULL,
	[Delete] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Description] [varchar](4096) NOT NULL,
	[OrganisationId] [bigint] NOT NULL,
	[Active] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC,
	[OrganisationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRolePermission]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRolePermission](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[ModuleId] [bigint] NOT NULL,
	[ExternalId] [bigint] NULL,
	[Read] [tinyint] NOT NULL,
	[Write] [tinyint] NOT NULL,
	[Delete] [tinyint] NOT NULL,
	[UserRoleId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UtilDate]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UtilDate](
	[DatePoint] [date] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[DatePoint] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  UserDefinedFunction [dbo].[ExplodeDates]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ExplodeDates](@startdate datetime, @enddate datetime)
returns table as
return (
with 
 N0 as (SELECT 1 as n UNION ALL SELECT 1)
,N1 as (SELECT 1 as n FROM N0 t1, N0 t2)
,N2 as (SELECT 1 as n FROM N1 t1, N1 t2)
,N3 as (SELECT 1 as n FROM N2 t1, N2 t2)
,N4 as (SELECT 1 as n FROM N3 t1, N3 t2)
,N5 as (SELECT 1 as n FROM N4 t1, N4 t2)
,N6 as (SELECT 1 as n FROM N5 t1, N5 t2)
,nums as (SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) as num FROM N6)
SELECT DATEADD(day,num-1,@startdate) as thedate
FROM nums
WHERE num <= DATEDIFF(day,@startdate,@enddate) + 1
);
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_Address_Section1]    Script Date: 28/04/2016 20:44:50 ******/
CREATE NONCLUSTERED INDEX [IDX_Address_Section1] ON [dbo].[Address]
(
	[Section1] ASC,
	[OrganisationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_Contract_Reference_OrganisationId]    Script Date: 28/04/2016 20:44:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Contract_Reference_OrganisationId] ON [dbo].[Contract]
(
	[Reference] ASC,
	[OrganisationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_ContractDetail_Name_Description_ContractId_GroupId]    Script Date: 28/04/2016 20:44:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_ContractDetail_Name_Description_ContractId_GroupId] ON [dbo].[ContractDetail]
(
	[Name] ASC,
	[Description] ASC,
	[ContractId] ASC,
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_ContractReferenceFormat_Format_ContractTypeId_OrganisationId]    Script Date: 28/04/2016 20:44:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_ContractReferenceFormat_Format_ContractTypeId_OrganisationId] ON [dbo].[ContractReferenceFormat]
(
	[Format] ASC,
	[ContractTypeId] ASC,
	[OrganisationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQX_Default_ContractTypeId_OrganisationId]    Script Date: 28/04/2016 20:44:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQX_Default_ContractTypeId_OrganisationId] ON [dbo].[ContractReferenceFormat]
(
	[Default] ASC,
	[ContractTypeId] ASC,
	[OrganisationId] ASC
)
WHERE ([Default] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContractDetailGroup] ADD  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[ContractGroup] ADD  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[SupplierPerson] ADD  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([TypeId])
REFERENCES [dbo].[AddressType] ([Id])
GO
ALTER TABLE [dbo].[Audit]  WITH CHECK ADD FOREIGN KEY([AuditActionId])
REFERENCES [dbo].[AuditAction] ([Id])
GO
ALTER TABLE [dbo].[Audit]  WITH CHECK ADD FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([Id])
GO
ALTER TABLE [dbo].[Audit]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[Audit]  WITH CHECK ADD  CONSTRAINT [FK_Audit_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Audit] CHECK CONSTRAINT [FK_Audit_UserId]
GO
ALTER TABLE [dbo].[Contact]  WITH CHECK ADD FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([Id])
GO
ALTER TABLE [dbo].[Contact]  WITH CHECK ADD FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([Id])
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_ContractType_Id] FOREIGN KEY([TypeId])
REFERENCES [dbo].[ContractType] ([Id])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_ContractType_Id]
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_Group_Id] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ContractGroup] ([Id])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_Group_Id]
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_Organisation_Id] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_Organisation_Id]
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_Person_Id] FOREIGN KEY([OwnerId])
REFERENCES [dbo].[Person] ([Id])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_Person_Id]
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_Status_Id] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([Id])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_Status_Id]
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_Supplier_Id] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([Id])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_Supplier_Id]
GO
ALTER TABLE [dbo].[ContractChange]  WITH CHECK ADD FOREIGN KEY([ContractId])
REFERENCES [dbo].[Contract] ([Id])
GO
ALTER TABLE [dbo].[ContractDetail]  WITH CHECK ADD  CONSTRAINT [FK__ContractDetail_Contract_Id] FOREIGN KEY([ContractId])
REFERENCES [dbo].[Contract] ([Id])
GO
ALTER TABLE [dbo].[ContractDetail] CHECK CONSTRAINT [FK__ContractDetail_Contract_Id]
GO
ALTER TABLE [dbo].[ContractDetail]  WITH CHECK ADD  CONSTRAINT [FK__ContractDetail_Group_Id] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
GO
ALTER TABLE [dbo].[ContractDetail] CHECK CONSTRAINT [FK__ContractDetail_Group_Id]
GO
ALTER TABLE [dbo].[ContractReferenceFormat]  WITH CHECK ADD FOREIGN KEY([ContractTypeId])
REFERENCES [dbo].[ContractType] ([Id])
GO
ALTER TABLE [dbo].[ContractReferenceFormat]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[ContractType]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([Id])
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([Id])
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_DocumentType_Id] FOREIGN KEY([DocumentTypeId])
REFERENCES [dbo].[DocumentType] ([Id])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_DocumentType_Id]
GO
ALTER TABLE [dbo].[DocumentType]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[Group]  WITH CHECK ADD  CONSTRAINT [FK__Group_Group_Id] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Group] ([Id])
GO
ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [FK__Group_Group_Id]
GO
ALTER TABLE [dbo].[Group]  WITH CHECK ADD  CONSTRAINT [FK__Group_GroupSet_Id] FOREIGN KEY([GroupSetId])
REFERENCES [dbo].[GroupSet] ([Id])
GO
ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [FK__Group_GroupSet_Id]
GO
ALTER TABLE [dbo].[Group]  WITH CHECK ADD  CONSTRAINT [FK__Group_Module_Id] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([Id])
GO
ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [FK__Group_Module_Id]
GO
ALTER TABLE [dbo].[Group]  WITH CHECK ADD  CONSTRAINT [FK__Group_Organisatation_Id] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [FK__Group_Organisatation_Id]
GO
ALTER TABLE [dbo].[GroupObject]  WITH CHECK ADD FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
GO
ALTER TABLE [dbo].[GroupObject]  WITH CHECK ADD FOREIGN KEY([GroupSetId])
REFERENCES [dbo].[GroupSet] ([Id])
GO
ALTER TABLE [dbo].[GroupObject]  WITH CHECK ADD FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([Id])
GO
ALTER TABLE [dbo].[GroupSet]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[GroupUser]  WITH CHECK ADD FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
GO
ALTER TABLE [dbo].[GroupUser]  WITH CHECK ADD  CONSTRAINT [FK_GroupUser_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[GroupUser] CHECK CONSTRAINT [FK_GroupUser_UserId]
GO
ALTER TABLE [dbo].[Note]  WITH CHECK ADD FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([Id])
GO
ALTER TABLE [dbo].[Note]  WITH CHECK ADD FOREIGN KEY([NoteTypeId])
REFERENCES [dbo].[NoteType] ([Id])
GO
ALTER TABLE [dbo].[Note]  WITH CHECK ADD  CONSTRAINT [FK_Note_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Note] CHECK CONSTRAINT [FK_Note_UserId]
GO
ALTER TABLE [dbo].[NoteType]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[OrganisationModule]  WITH CHECK ADD FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([Id])
GO
ALTER TABLE [dbo].[OrganisationModule]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[ReportOrganisation]  WITH CHECK ADD  CONSTRAINT [FK_ReportOrganisation_Group_GroupId] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
GO
ALTER TABLE [dbo].[ReportOrganisation] CHECK CONSTRAINT [FK_ReportOrganisation_Group_GroupId]
GO
ALTER TABLE [dbo].[ReportOrganisation]  WITH CHECK ADD  CONSTRAINT [FK_ReportOrganisation_Organisation_OrganisationId] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[ReportOrganisation] CHECK CONSTRAINT [FK_ReportOrganisation_Organisation_OrganisationId]
GO
ALTER TABLE [dbo].[ReportOrganisation]  WITH CHECK ADD  CONSTRAINT [FK_ReportOrganisation_Person_PersionId] FOREIGN KEY([OwnerId])
REFERENCES [dbo].[Person] ([Id])
GO
ALTER TABLE [dbo].[ReportOrganisation] CHECK CONSTRAINT [FK_ReportOrganisation_Person_PersionId]
GO
ALTER TABLE [dbo].[ReportOrganisation]  WITH CHECK ADD  CONSTRAINT [FK_ReportOrganisation_Report_ReportId] FOREIGN KEY([ReportId])
REFERENCES [dbo].[Report] ([Id])
GO
ALTER TABLE [dbo].[ReportOrganisation] CHECK CONSTRAINT [FK_ReportOrganisation_Report_ReportId]
GO
ALTER TABLE [dbo].[Status]  WITH CHECK ADD FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([Id])
GO
ALTER TABLE [dbo].[Status]  WITH CHECK ADD FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([Id])
GO
ALTER TABLE [dbo].[Status]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[Status]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[Supplier]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[SupplierPerson]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[SupplierPerson]  WITH CHECK ADD FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([Id])
GO
ALTER TABLE [dbo].[SupplierPerson]  WITH CHECK ADD FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([Id])
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Module_Id] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([Id])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Module_Id]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Organisation_Id] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Organisation_Id]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Person_Id] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([Id])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Person_Id]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Status_Id] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([Id])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Status_Id]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Task_Id] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Task] ([Id])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Task_Id]
GO
ALTER TABLE [dbo].[TaskAlert]  WITH CHECK ADD FOREIGN KEY([TaskId])
REFERENCES [dbo].[Task] ([Id])
GO
ALTER TABLE [dbo].[TaskContact]  WITH CHECK ADD FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contact] ([Id])
GO
ALTER TABLE [dbo].[TaskContact]  WITH CHECK ADD FOREIGN KEY([TaskId])
REFERENCES [dbo].[Task] ([Id])
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([Id])
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[UserRole] ([Id])
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([Id])
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD  CONSTRAINT [FK_UserPermi_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[UserPermission] CHECK CONSTRAINT [FK_UserPermi_UserId]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([Id])
GO
ALTER TABLE [dbo].[UserRolePermission]  WITH CHECK ADD FOREIGN KEY([UserRoleId])
REFERENCES [dbo].[UserRole] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD CHECK  (([Gender]='F' OR [Gender]='M'))
GO
/****** Object:  StoredProcedure [dbo].[sp_ContractStatistics]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ContractStatistics](@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS

	DECLARE @XmlParams XML = CONVERT(XML, @Params);
	DECLARE @Report AS VARCHAR(255);
	
	SELECT @Report = @XmlParams.value('(//entry[string/text() = "report"]/*[position()=2]/*[position()=1]/text())[1]', 'VARCHAR(255)');

	IF @Report = 'max'
	BEGIN
		/*Total contract value managed by status*/
		SELECT name, MAX(total) AS [max] FROM
		(SELECT c.Id, s.Name AS name
				, SUM(CONVERT(XML, cd.Value).value('(/value/projected)[1]', 'NUMERIC(30,10)') ) AS [total]
			FROM [dbo].[Contract] c WITH (NOLOCK)
			INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
			INNER JOIN [dbo].[ContractDetail] cd WITH (NOLOCK) ON c.Id = cd.ContractId
			INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
			INNER JOIN [dbo].[Group] g ON g.Id = cd.GroupId AND g.Name = 'VALUE' AND g.ModuleId = m.Id
			WHERE c.OrganisationId = @OrganisationId
			GROUP BY c.Id, s.Name) a
			GROUP BY name;
	END
	ELSE IF @Report = 'value'
	BEGIN
		/*Total contract value managed by status*/
		SELECT s.Name AS name
				, SUM(CONVERT(XML, cd.Value).value('(/value/projected)[1]', 'NUMERIC(30,10)') ) AS [total]
			FROM [dbo].[Contract] c WITH (NOLOCK)
			INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
			INNER JOIN [dbo].[ContractDetail] cd WITH (NOLOCK) ON c.Id = cd.ContractId
			INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
			INNER JOIN [dbo].[Group] g ON g.Id = cd.GroupId AND g.Name = 'VALUE' AND g.ModuleId = m.Id
			WHERE c.OrganisationId = @OrganisationId
			GROUP BY s.Name;
	END
	ELSE IF @Report = 'status' 
	BEGIN
		/*Contract count by status*/
		SELECT s.Name AS name, COUNT(*) AS [total]
			FROM [dbo].[Contract] c WITH (NOLOCK)
			INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
			INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
			WHERE c.OrganisationId = @OrganisationId
			GROUP BY s.Name;
	END
GO
/****** Object:  StoredProcedure [dbo].[sp_SpendAnalysis]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_SpendAnalysis](@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS
	
	DECLARE @XmlParams XML = CONVERT(XML, @Params);
	DECLARE @ContractId BIGINT;
	
	SELECT @ContractId = @XmlParams.value('(//entry[string/text() = "contractId"]/list/string/text())[1]', 'BIGINT');

	WITH ContractValue(Id, ContractId, Name, ContractStartDate, ContractEndDate, Period, Value, StartDate) 
	AS (SELECT cd.Id, cd.ContractId, cd.Name, c.StartDate, c.EndDate, CONVERT(XML, cd.Description) AS Period, CONVERT(XML, cd.Value) AS Value, c.StartDate FROM [dbo].[ContractDetail] cd 
		INNER JOIN [Group] g ON cd.GroupId = g.Id 
		INNER JOIN [Contract] c ON c.Id = cd.ContractId WHERE c.Id = @ContractId AND g.Name = 'VALUE'),
	ContractValue2(ContractId, Name, ContractStartDate, ContractEndDate, PeriodFrequency, PeriodValue, StartDate, EndDate, Projected, Actual) 
	AS (SELECT ContractId, Name, ContractStartDate, ContractEndDate, Period.value('(//frequency)[1]', 'VARCHAR(5)') AS PeriodFrequency, Period.value('(//value)[1]', 'VARCHAR(5)') AS PeriodValue,
			CASE Period.value('(//frequency)[1]', 'VARCHAR(5)')
				WHEN 'Y' THEN DATEADD(YY, CONVERT(INT, Period.value('(//value)[1]', 'VARCHAR(5)')), StartDate)
				WHEN 'Q' THEN DATEADD(M, CONVERT(INT, Period.value('(//value)[1]', 'VARCHAR(5)')) * 3, StartDate)
				WHEN 'M' THEN DATEADD(M, CONVERT(INT, Period.value('(//value)[1]', 'VARCHAR(5)')), StartDate)
				ELSE CONVERT(DATETIME, Period.value('(//from)[1]', 'VARCHAR(20)'))
			END AS StartDate, CONVERT(DATETIME, Period.value('(//to)[1]', 'VARCHAR(20)'))
		, 
			CONVERT(NUMERIC(30,10), Value.value('(//projected)[1]', 'VARCHAR(MAX)')) AS Projected
			, CONVERT(NUMERIC(30,10), Value.value('(//actual)[1]', 'VARCHAR(MAX)')) AS Actual FROM ContractValue),
	ContractValue3(ContractId, Name, ContractStartDate, ContractEndDate, ValueStartDate, ValueEndDate, Projected, Actual)
	AS (SELECT ContractId, Name, ContractStartDate, ContractEndDate, StartDate AS ValueStartDate, CASE PeriodFrequency
				WHEN 'Y' THEN DATEADD(D, -1, DATEADD(YY, 1, StartDate))
				WHEN 'Q' THEN DATEADD(D, -1, DATEADD(M, 1, StartDate))
				WHEN 'M' THEN DATEADD(D, -1, DATEADD(M, 1, StartDate))
				ELSE EndDate
			END AS ValueEndDate, Projected, ISNULL(Actual, 0) AS Actual FROM ContractValue2),
	ContractValue4(ContractId, DatePoint, Name, Projected, Actual)
	AS (SELECT cv.ContractId, DATEDIFF(YY, cv.ContractStartDate, ud.DatePoint) AS DatePoint, cv.Name, Projected/DATEDIFF(D, ValueStartDate, ValueEndDate) AS ProjectedPerDay, Actual/DATEDIFF(D, ValueStartDate, ValueEndDate) AS ActualPerDay 
	FROM UtilDate ud LEFT JOIN ContractValue3 c ON ud.DatePoint BETWEEN c.ValueStartDate AND c.ValueEndDate
	CROSS JOIN (SELECT DISTINCT ContractId, Name, ContractStartDate, ContractEndDate FROM ContractValue) cv
			 WHERE ud.DatePoint BETWEEN cv.ContractStartDate AND cv.ContractEndDate)
	SELECT DatePoint, Name, SUM(Projected) AS Projected, SUM(Actual) AS Actual FROM ContractValue4
	 WHERE ContractId = @ContractId GROUP BY DatePoint, Name ORDER BY Name, DatePoint;
--SELECT * FROM ContractDetail;

--SELECT * FROM UtilDate ORDER BY DatePoint;
GO
/****** Object:  StoredProcedure [dbo].[sp_UserContractActivity]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UserContractActivity](@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS

	DECLARE @XmlParams XML = CONVERT(XML, @Params);
	DECLARE @PersonId BIGINT;
	DECLARE @Limit INT;
	DECLARE @Module VARCHAR(255);

	SELECT @PersonId = @XmlParams.value('(//entry[string/text() = "personId"]/*[position()=2]/*[position()=1]/text())[1]', 'BIGINT');
	SELECT @Limit = @XmlParams.value('(//entry[string/text() = "limit"]/*[position()=2]/*[position()=1]/text())[1]', 'INT');
	SELECT @Module = 'Contract'
	
	IF @Limit IS NULL SET @Limit = 2147483647;

	WITH RecentContract(UserId, ExternalId, LastRead)
	AS (SELECT a.UserId, ExternalId, MAX(Created) 
		FROM [dbo].[Audit] a  WITH (NOLOCK)
		INNER JOIN Module m ON a.ModuleId = m.Id 
		INNER JOIN AuditAction aa ON a.AuditActionId = aa.Id
		WHERE m.Name = @Module AND aa.Name = 'Read'
		GROUP BY a.UserId, ExternalId)
	SELECT TOP (@Limit) c.*
		FROM RecentContract rc 
		INNER JOIN dbo.Contract c WITH (NOLOCK) ON rc.Externalid = c.Id
		INNER JOIN dbo.[User] u WITH (NOLOCK) ON rc.UserId = u.Id
		WHERE u.PersonId = @PersonId 
		ORDER BY LastRead DESC;
GO
/****** Object:  StoredProcedure [dbo].[sp_UserTaskActivity]    Script Date: 28/04/2016 20:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UserTaskActivity](@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS
	DECLARE @XmlParams XML = CONVERT(XML, @Params);
	DECLARE @PersonId BIGINT;
	DECLARE @Limit INT;

	SELECT @PersonId = @XmlParams.value('(//entry[string/text() = "personId"]/*[position()=2]/*[position()=1]/text())[1]', 'BIGINT');
	SELECT @Limit = @XmlParams.value('(//entry[string/text() = "limit"]/*[position()=2]/*[position()=1]/text())[1]', 'INT');
	
	IF @Limit IS NULL SET @Limit = 2147483647;

	SELECT t.*
		FROM dbo.Task t WITH (NOLOCK)
		INNER JOIN dbo.Contract c WITH (NOLOCK) ON t.ExternalId = c.Id
		INNER JOIN dbo.Module m ON t.ModuleId = m.Id AND m.Name = 'Contract'
		WHERE c.OrganisationId = @OrganisationId AND (c.OwnerId = @PersonId OR t.PersonID = @PersonId)
	UNION
	SELECT t.*
		FROM dbo.Task t WITH (NOLOCK)
		INNER JOIN dbo.ContractDetail cd WITH (NOLOCK) ON t.ExternalId = cd.Id
		INNER JOIN dbo.Contract c WITH (NOLOCK) ON cd.ContractId = c.Id
		INNER JOIN dbo.Module m ON t.ModuleId = m.Id AND m.Name = 'Contract Detail'
		WHERE c.OrganisationId = @OrganisationId AND (c.OwnerId = @PersonId OR t.PersonID = @PersonId)
		ORDER BY t.StartDate DESC;
GO
USE [master]
GO
ALTER DATABASE [c21cms] SET  READ_WRITE 
GO
