﻿CREATE TABLE [dbo].[Audit] (
    [Id]             BIGINT         IDENTITY (1000, 1) NOT NULL,
    [Description]    VARCHAR (MAX)  NOT NULL,
    [ExternalId]     BIGINT         NOT NULL,
    [AuditActionId]  BIGINT         NOT NULL,
    [ModuleId]       BIGINT         NOT NULL,
    [UserId]         BIGINT         NULL,
    [Created]        DATETIME       NULL,
    [ChangeField]    VARCHAR (4096) NULL,
    [ChangeFrom]     VARCHAR (MAX)  NULL,
    [ChangeTo]       VARCHAR (MAX)  NULL,
    [OrganisationId] BIGINT         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([AuditActionId]) REFERENCES [dbo].[AuditAction] ([Id]),
    FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([Id]),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Audit_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);



