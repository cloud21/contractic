﻿CREATE TABLE [dbo].[Person] (
    [Id]             BIGINT         IDENTITY (10000, 1) NOT NULL,
    [Title]          NVARCHAR (50)  NOT NULL,
    [FirstName]      NVARCHAR (255) NOT NULL,
    [MiddleName]     NVARCHAR (255) NULL,
    [LastName]       NVARCHAR (255) NOT NULL,
    [JobTitle]       NVARCHAR (255) NULL,
    [Gender]         CHAR (1)       NULL,
    [Created]        DATETIME       NOT NULL,
    [Active]         TINYINT        DEFAULT ((1)) NULL,
    [OrganisationId] BIGINT         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CHECK ([Gender]='F' OR [Gender]='M'),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id])
);



