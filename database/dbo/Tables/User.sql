﻿CREATE TABLE [dbo].[User] (
    [Id]          BIGINT         IDENTITY (1000, 1) NOT NULL,
    [LoginId]     VARCHAR (255)  NOT NULL,
    [Password]    NVARCHAR (MAX) NOT NULL,
    [Created]     DATETIME       NULL,
    [Profile]     TEXT           NULL,
    [Active]      TINYINT        NOT NULL,
    [PersonID]    BIGINT         NOT NULL,
    [LastUpdated] DATETIME       NULL,
    [RoleId]      BIGINT         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([Id]),
    FOREIGN KEY ([RoleId]) REFERENCES [dbo].[UserRole] ([Id]),
    CONSTRAINT [UQ_User_LoginId] UNIQUE NONCLUSTERED ([LoginId] ASC)
);



