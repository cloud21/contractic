﻿CREATE TABLE [dbo].[Note] (
    [Id]         BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Header]     VARCHAR (255) NOT NULL,
    [Detail]     VARCHAR (MAX) NOT NULL,
    [ExternalId] BIGINT        NOT NULL,
    [NoteTypeId] BIGINT        NOT NULL,
    [ModuleId]   BIGINT        NOT NULL,
    [Active]     TINYINT       DEFAULT ((1)) NULL,
    [UserId]     BIGINT        NULL,
    [Created]    DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([Id]),
    FOREIGN KEY ([NoteTypeId]) REFERENCES [dbo].[NoteType] ([Id]),
    CONSTRAINT [FK_Note_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);



