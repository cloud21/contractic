﻿CREATE TABLE [dbo].[DocumentType] (
    [Id]             BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Name]           VARCHAR (255) NOT NULL,
    [Description]    VARCHAR (MAX) NOT NULL,
    [OrganisationId] BIGINT        NOT NULL,
    [Active]         TINYINT       DEFAULT ((1)) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [UQ_DocumentType_Name_OrganisationId] UNIQUE NONCLUSTERED ([Name] ASC, [OrganisationId] ASC)
);





