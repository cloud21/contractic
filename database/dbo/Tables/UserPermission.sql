﻿CREATE TABLE [dbo].[UserPermission] (
    [Id]         BIGINT  IDENTITY (1000, 1) NOT NULL,
    [UserId]     BIGINT  NOT NULL,
    [ModuleId]   BIGINT  NOT NULL,
    [ExternalId] BIGINT  NULL,
    [Read]       TINYINT NOT NULL,
    [Write]      TINYINT NOT NULL,
    [Delete]     TINYINT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([Id]),
    CONSTRAINT [FK_UserPermi_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);



