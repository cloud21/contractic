﻿CREATE TABLE [dbo].[AddressType] (
    [Id]     BIGINT       IDENTITY (1000, 1) NOT NULL,
    [Name]   VARCHAR (50) NOT NULL,
    [Active] TINYINT      DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([Name] ASC)
);

