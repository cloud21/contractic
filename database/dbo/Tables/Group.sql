﻿CREATE TABLE [dbo].[Group] (
    [Id]             BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Name]           VARCHAR (50)  NOT NULL,
    [Active]         TINYINT       NULL,
    [ModuleId]       BIGINT        NULL,
    [OrganisationId] BIGINT        NOT NULL,
    [Description]    VARCHAR (255) NULL,
    [System]         TINYINT       NULL,
    [GroupSetId]     BIGINT        NULL,
    [ParentId]       BIGINT        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Group_Group_Id] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[Group] ([Id]),
    CONSTRAINT [FK__Group_GroupSet_Id] FOREIGN KEY ([GroupSetId]) REFERENCES [dbo].[GroupSet] ([Id]),
    CONSTRAINT [FK__Group_Module_Id] FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([Id]),
    CONSTRAINT [FK__Group_Organisatation_Id] FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [UQ_GroupSetId_OrganisationId_ModuleId_Name_Description] UNIQUE NONCLUSTERED ([GroupSetId] ASC, [OrganisationId] ASC, [ModuleId] ASC, [Name] ASC, [Description] ASC)
);











