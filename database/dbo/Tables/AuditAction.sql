﻿CREATE TABLE [dbo].[AuditAction] (
    [Id]          BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Name]        VARCHAR (50)  NOT NULL,
    [Description] VARCHAR (255) NULL,
    [Format]      VARCHAR (MAX) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([Name] ASC)
);

