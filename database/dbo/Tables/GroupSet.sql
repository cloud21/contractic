﻿CREATE TABLE [dbo].[GroupSet] (
    [Id]             BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Name]           VARCHAR (50)  NOT NULL,
    [Description]    VARCHAR (255) NULL,
    [Active]         TINYINT       NULL,
    [OrganisationId] BIGINT        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    UNIQUE NONCLUSTERED ([OrganisationId] ASC, [Name] ASC)
);

