﻿CREATE TABLE [dbo].[UserRolePermission] (
    [Id]         BIGINT  IDENTITY (1000, 1) NOT NULL,
    [ModuleId]   BIGINT  NOT NULL,
    [ExternalId] BIGINT  NULL,
    [Read]       TINYINT NOT NULL,
    [Write]      TINYINT NOT NULL,
    [Delete]     TINYINT NOT NULL,
    [UserRoleId] BIGINT  NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([UserRoleId]) REFERENCES [dbo].[UserRole] ([Id]) ON DELETE CASCADE
);





