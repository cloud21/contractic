﻿CREATE TABLE [dbo].[ContractReferenceFormat] (
    [Id]             BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Format]         VARCHAR (100) NOT NULL,
    [Name]           VARCHAR (255) NOT NULL,
    [Description]    VARCHAR (MAX) NULL,
    [Default]        TINYINT       NULL,
    [Active]         TINYINT       NULL,
    [ContractTypeId] BIGINT        NULL,
    [OrganisationId] BIGINT        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([ContractTypeId]) REFERENCES [dbo].[ContractType] ([Id]),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id])
);






GO



GO
CREATE UNIQUE NONCLUSTERED INDEX [UQX_Default_ContractTypeId_OrganisationId]
    ON [dbo].[ContractReferenceFormat]([Default] ASC, [ContractTypeId] ASC, [OrganisationId] ASC) WHERE ([Default] IS NOT NULL);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_ContractReferenceFormat_Format_ContractTypeId_OrganisationId]
    ON [dbo].[ContractReferenceFormat]([Format] ASC, [ContractTypeId] ASC, [OrganisationId] ASC);

