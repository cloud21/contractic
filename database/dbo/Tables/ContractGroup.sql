﻿CREATE TABLE [dbo].[ContractGroup] (
    [Id]          BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Name]        VARCHAR (255) NOT NULL,
    [Description] VARCHAR (MAX) NULL,
    [Active]      TINYINT       DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

