﻿CREATE TABLE [dbo].[ReportOrganisation] (
    [Id]             BIGINT         IDENTITY (1000, 1) NOT NULL,
    [Name]           VARCHAR (255)  NULL,
    [Description]    VARCHAR (4096) NULL,
    [Schedule]       VARCHAR (4096) NULL,
    [ReportId]       BIGINT         NOT NULL,
    [OrganisationId] BIGINT         NOT NULL,
    [GroupId]        BIGINT         NULL,
    [OwnerId]        BIGINT         NULL,
    [Active]         TINYINT        NOT NULL,
    [Created]        DATETIME       NOT NULL,
    [LastRun]        DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ReportOrganisation_Group_GroupId] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[Group] ([Id]),
    CONSTRAINT [FK_ReportOrganisation_Organisation_OrganisationId] FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_ReportOrganisation_Person_PersionId] FOREIGN KEY ([OwnerId]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_ReportOrganisation_Report_ReportId] FOREIGN KEY ([ReportId]) REFERENCES [dbo].[Report] ([Id]),
    CONSTRAINT [UQ_ReportOrganisation_ReportId_OrganisationId] UNIQUE NONCLUSTERED ([ReportId] ASC, [OrganisationId] ASC)
);



