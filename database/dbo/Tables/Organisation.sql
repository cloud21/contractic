﻿CREATE TABLE [dbo].[Organisation] (
    [Id]      BIGINT         IDENTITY (1000, 1) NOT NULL,
    [Name]    NVARCHAR (255) NULL,
    [Created] DATETIME       NULL,
    [Active]  TINYINT        NOT NULL,
    [Code]    VARCHAR (255)  NULL,
    [Profile] VARCHAR (MAX)  NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([Code] ASC)
);

