﻿CREATE TABLE [dbo].[Module] (
    [Id]          BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Name]        VARCHAR (50)  NOT NULL,
    [Description] VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UNQ_ModuleName] UNIQUE NONCLUSTERED ([Name] ASC)
);

