CREATE TABLE [dbo].[ContractDetail] (
    [Id]          BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Name]        VARCHAR (50)  NOT NULL,
    [Description] VARCHAR (255) NULL,
    [Value]       VARCHAR (MAX) NOT NULL,
    [LastUpdated] DATETIME      NOT NULL,
    [ContractId]  BIGINT        NOT NULL,
    [GroupId]     BIGINT        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ContractDetail_Contract_Id] FOREIGN KEY ([ContractId]) REFERENCES [dbo].[Contract] ([Id]),
    CONSTRAINT [FK__ContractDetail_Group_Id] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[Group] ([Id])
);










GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_ContractDetail_Name_Description_ContractId_GroupId]
    ON [dbo].[ContractDetail]([Name] ASC, [Description] ASC, [ContractId] ASC, [GroupId] ASC);

