﻿CREATE TABLE [dbo].[ReportType] (
    [Id]          BIGINT         IDENTITY (100, 1) NOT NULL,
    [Name]        VARCHAR (255)  NOT NULL,
    [Description] VARCHAR (4096) NOT NULL,
    [Active]      TINYINT        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

