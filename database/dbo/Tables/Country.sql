﻿CREATE TABLE [dbo].[Country] (
    [Id]           BIGINT        IDENTITY (100, 1) NOT NULL,
    [Name]         VARCHAR (255) NULL,
    [CurrencyCode] VARCHAR (50)  NOT NULL,
    [CurrencyName] VARCHAR (255) NOT NULL,
    [DialingCode]  VARCHAR (20)  NULL,
    [HtmlCode]     VARCHAR (20)  NULL,
    [Active]       TINYINT       DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

