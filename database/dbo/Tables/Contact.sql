﻿CREATE TABLE [dbo].[Contact] (
    [Id]         BIGINT  IDENTITY (1000, 1) NOT NULL,
    [Active]     TINYINT DEFAULT ((1)) NOT NULL,
    [ExternalId] BIGINT  NOT NULL,
    [AddressId]  BIGINT  NOT NULL,
    [ModuleId]   BIGINT  NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Address] ([Id]),
    FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([Id])
);



