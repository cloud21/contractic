﻿CREATE TABLE [dbo].[OrganisationModule] (
    [Id]             BIGINT   IDENTITY (1000, 1) NOT NULL,
    [Effective]      DATETIME NULL,
    [Active]         TINYINT  NULL,
    [OrganisationId] BIGINT   NULL,
    [ModuleId]       BIGINT   NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([Id]),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id])
);

