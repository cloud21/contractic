﻿CREATE TABLE [dbo].[UserRole] (
    [Id]             BIGINT         IDENTITY (1000, 1) NOT NULL,
    [Name]           VARCHAR (255)  NOT NULL,
    [Description]    VARCHAR (4096) NOT NULL,
    [OrganisationId] BIGINT         NOT NULL,
    [Active]         TINYINT        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    UNIQUE NONCLUSTERED ([Name] ASC, [OrganisationId] ASC)
);

