﻿CREATE TABLE [dbo].[Status] (
    [Id]             BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Name]           VARCHAR (50)  NOT NULL,
    [Active]         TINYINT       DEFAULT ((1)) NULL,
    [ModuleId]       BIGINT        NOT NULL,
    [OrganisationId] BIGINT        NOT NULL,
    [Description]    VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([Id]),
    FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([Id]),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    UNIQUE NONCLUSTERED ([OrganisationId] ASC, [ModuleId] ASC, [Name] ASC)
);

