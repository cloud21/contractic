﻿CREATE TABLE [dbo].[GroupUser] (
    [Id]      BIGINT  IDENTITY (1000, 1) NOT NULL,
    [GroupId] BIGINT  NOT NULL,
    [UserId]  BIGINT  NOT NULL,
    [Active]  TINYINT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([GroupId]) REFERENCES [dbo].[Group] ([Id]),
    CONSTRAINT [FK_GroupUser_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);





