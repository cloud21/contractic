﻿CREATE TABLE [dbo].[Document] (
    [Id]             BIGINT         IDENTITY (1000, 1) NOT NULL,
    [Title]          NVARCHAR (255) NULL,
    [Description]    NVARCHAR (MAX) NULL,
    [Received]       DATETIME       NOT NULL,
    [FilePath]       NVARCHAR (MAX) NOT NULL,
    [ExternalId]     BIGINT         NOT NULL,
    [DocumentTypeId] BIGINT         NOT NULL,
    [StatusId]       BIGINT         NULL,
    [ModuleId]       BIGINT         NOT NULL,
    [FileType]       VARCHAR (255)  NULL,
    [FileName]       VARCHAR (MAX)  NULL,
    [OrganisationId] BIGINT         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([Id]),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([Id]),
    CONSTRAINT [FK_Document_DocumentType_Id] FOREIGN KEY ([DocumentTypeId]) REFERENCES [dbo].[DocumentType] ([Id])
);



