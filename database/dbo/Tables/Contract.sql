﻿CREATE TABLE [dbo].[Contract] (
    [Id]             BIGINT           IDENTITY (1000, 1) NOT NULL,
    [SupplierId]     BIGINT           NOT NULL,
    [OwnerId]        BIGINT           NOT NULL,
    [GroupId]        BIGINT           NULL,
    [StatusId]       BIGINT           NOT NULL,
    [TypeId]         BIGINT           NOT NULL,
    [Title]          VARCHAR (255)    NOT NULL,
    [StartDate]      DATETIME         NOT NULL,
    [EndDate]        DATETIME         NOT NULL,
    [Reference]      VARCHAR (255)    NOT NULL,
    [OrganisationId] BIGINT           NOT NULL,
    [Description]    VARCHAR (4096)   NULL,
    [Value]          NUMERIC (30, 10) NULL,
    [ValueUOM]       VARCHAR (10)     NULL,
    [Term]           VARCHAR (10)     NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Contract_ContractType_Id] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[ContractType] ([Id]),
    CONSTRAINT [FK_Contract_Group_Id] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[ContractGroup] ([Id]),
    CONSTRAINT [FK_Contract_Organisation_Id] FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Contract_Person_Id] FOREIGN KEY ([OwnerId]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Contract_Status_Id] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([Id]),
    CONSTRAINT [FK_Contract_Supplier_Id] FOREIGN KEY ([SupplierId]) REFERENCES [dbo].[Supplier] ([Id])
);












GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Contract_Reference_OrganisationId]
    ON [dbo].[Contract]([Reference] ASC, [OrganisationId] ASC);

