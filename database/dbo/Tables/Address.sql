﻿CREATE TABLE [dbo].[Address] (
    [Id]             BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Section1]       VARCHAR (255) NOT NULL,
    [Section2]       VARCHAR (255) NULL,
    [Section3]       VARCHAR (255) NULL,
    [Section4]       VARCHAR (255) NULL,
    [Section5]       VARCHAR (255) NULL,
    [Section6]       VARCHAR (255) NULL,
    [Section7]       VARCHAR (255) NULL,
    [Section8]       VARCHAR (255) NULL,
    [OrganisationId] BIGINT        NOT NULL,
    [TypeId]         BIGINT        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    FOREIGN KEY ([TypeId]) REFERENCES [dbo].[AddressType] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IDX_Address_Section1]
    ON [dbo].[Address]([Section1] ASC, [OrganisationId] ASC);

