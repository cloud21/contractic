﻿CREATE TABLE [dbo].[SupplierPerson] (
    [Id]             BIGINT        IDENTITY (10000, 1) NOT NULL,
    [PersonId]       BIGINT        NOT NULL,
    [Created]        DATETIME      NOT NULL,
    [Active]         TINYINT       DEFAULT ((1)) NULL,
    [ExternalRef]    VARCHAR (255) NULL,
    [OrganisationId] BIGINT        NOT NULL,
    [SupplierId]     BIGINT        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([Id]),
    FOREIGN KEY ([SupplierId]) REFERENCES [dbo].[Supplier] ([Id])
);

