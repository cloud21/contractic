﻿CREATE TABLE [dbo].[ContractChange] (
    [Id]               BIGINT        IDENTITY (1000, 1) NOT NULL,
    [CustomerChangeId] VARCHAR (255) NOT NULL,
    [ApprovalDate]     DATETIME      NOT NULL,
    [EffectiveDate]    DATETIME      NOT NULL,
    [Description]      VARCHAR (MAX) NULL,
    [ContractId]       BIGINT        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([ContractId]) REFERENCES [dbo].[Contract] ([Id])
);

