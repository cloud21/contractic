﻿CREATE TABLE [dbo].[ContractType] (
    [Id]             BIGINT        IDENTITY (1000, 1) NOT NULL,
    [Name]           VARCHAR (255) NOT NULL,
    [Description]    VARCHAR (MAX) NULL,
    [Active]         TINYINT       DEFAULT ((1)) NOT NULL,
    [OrganisationId] BIGINT        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [UQ_ContractType_Name_Organisation] UNIQUE NONCLUSTERED ([Name] ASC, [OrganisationId] ASC)
);



