﻿CREATE TABLE [dbo].[Task] (
    [Id]             BIGINT        IDENTITY (1000, 1) NOT NULL,
    [PersonID]       BIGINT        NOT NULL,
    [Description]    VARCHAR (MAX) NULL,
    [Created]        DATETIME      NOT NULL,
    [StatusId]       BIGINT        NULL,
    [ParentId]       BIGINT        NULL,
    [ExternalId]     BIGINT        NOT NULL,
    [ModuleId]       BIGINT        NOT NULL,
    [Title]          VARCHAR (255) NOT NULL,
    [StartDate]      DATETIME      NOT NULL,
    [EndDate]        DATETIME      NULL,
    [OrganisationId] BIGINT        NOT NULL,
    [Number]         AS            ([Id]),
    [RepeatStart]    DATETIME      NULL,
    [RepeatEnd]      DATETIME      NULL,
    [RepeatYear]     VARCHAR (10)  NULL,
    [RepeatMonth]    VARCHAR (2)   NULL,
    [RepeatWeek]     VARCHAR (2)   NULL,
    [RepeatDay]      VARCHAR (2)   NULL,
    [RepeatDate]     VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Task_Module_Id] FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([Id]),
    CONSTRAINT [FK_Task_Organisation_Id] FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Task_Person_Id] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Task_Status_Id] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([Id]),
    CONSTRAINT [FK_Task_Task_Id] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[Task] ([Id]),
    CONSTRAINT [UQ_Task_Number_OrganisationId] UNIQUE NONCLUSTERED ([Number] ASC, [OrganisationId] ASC)
);





