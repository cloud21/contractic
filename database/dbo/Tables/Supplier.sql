﻿CREATE TABLE [dbo].[Supplier] (
    [Id]             BIGINT         IDENTITY (10000, 1) NOT NULL,
    [Reference]      NVARCHAR (255) NOT NULL,
    [Name]           NVARCHAR (MAX) NOT NULL,
    [Created]        DATETIME       NOT NULL,
    [Active]         TINYINT        DEFAULT ((1)) NULL,
    [ExternalRef]    VARCHAR (255)  NULL,
    [OrganisationId] BIGINT         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id])
);

