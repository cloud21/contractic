﻿CREATE TABLE [dbo].[TaskContact] (
    [Id]        BIGINT   IDENTITY (100, 1) NOT NULL,
    [ContactId] BIGINT   NOT NULL,
    [TaskId]    BIGINT   NOT NULL,
    [Updated]   DATETIME NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([ContactId]) REFERENCES [dbo].[Contact] ([Id]),
    FOREIGN KEY ([TaskId]) REFERENCES [dbo].[Task] ([Id]),
    UNIQUE NONCLUSTERED ([ContactId] ASC, [TaskId] ASC)
);

