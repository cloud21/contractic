﻿CREATE TABLE [dbo].[TaskAlert] (
    [Id]        BIGINT       IDENTITY (100, 1) NOT NULL,
    [StartDate] DATETIME     NOT NULL,
    [EndDate]   DATETIME     NULL,
    [Year]      VARCHAR (10) NULL,
    [Month]     VARCHAR (2)  NULL,
    [Week]      VARCHAR (2)  NULL,
    [Date]      DATETIME     NULL,
    [TaskId]    BIGINT       NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([TaskId]) REFERENCES [dbo].[Task] ([Id])
);

