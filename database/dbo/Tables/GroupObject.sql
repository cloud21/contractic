﻿CREATE TABLE [dbo].[GroupObject] (
    [Id]         BIGINT  IDENTITY (1000, 1) NOT NULL,
    [GroupId]    BIGINT  NOT NULL,
    [ModuleId]   BIGINT  NOT NULL,
    [ExternalId] BIGINT  NOT NULL,
    [Active]     TINYINT NULL,
    [GroupSetId] BIGINT  NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([GroupId]) REFERENCES [dbo].[Group] ([Id]),
    FOREIGN KEY ([GroupSetId]) REFERENCES [dbo].[GroupSet] ([Id]),
    FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([Id]),
    UNIQUE NONCLUSTERED ([GroupSetId] ASC, [ModuleId] ASC, [ExternalId] ASC, [GroupId] ASC)
);

