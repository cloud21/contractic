﻿CREATE TABLE [dbo].[Report] (
    [Id]          BIGINT         IDENTITY (1000, 1) NOT NULL,
    [Name]        VARCHAR (255)  NOT NULL,
    [Description] VARCHAR (4096) NOT NULL,
    [Command]     VARCHAR (MAX)  NULL,
    [Active]      TINYINT        NOT NULL,
    [ExternalRef] VARCHAR (255)  NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_Report_ExternalRef] UNIQUE NONCLUSTERED ([ExternalRef] ASC),
    CONSTRAINT [UQ_Report_Name] UNIQUE NONCLUSTERED ([Name] ASC)
);







