﻿CREATE TABLE [dbo].[UtilDate] (
    [DatePoint] DATE NULL,
    [ID]        INT  IDENTITY (1, 1) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    UNIQUE NONCLUSTERED ([DatePoint] ASC)
);



