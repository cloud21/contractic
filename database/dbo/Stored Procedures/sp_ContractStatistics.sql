﻿CREATE PROCEDURE sp_ContractStatistics(@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS
/*Total contract value managed by status*/
SELECT s.Name
		, MAX(CONVERT(XML, cd.Value).value('(/value/projected)[1]', 'NUMERIC(30,10)') ) AS MaxProjected
		, SUM(CONVERT(XML, cd.Value).value('(/value/projected)[1]', 'NUMERIC(30,10)') ) AS TotalProjected
	FROM [dbo].[Contract] c WITH (NOLOCK)
	INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
	INNER JOIN [dbo].[ContractDetail] cd WITH (NOLOCK) ON c.Id = cd.ContractId
	INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
	INNER JOIN [dbo].[Group] g ON g.Id = cd.GroupId AND g.Name = 'VALUE' AND g.ModuleId = m.Id
	WHERE c.OrganisationId = @OrganisationId
	GROUP BY s.Name;


/*Contract count by status*/
SELECT s.Name, COUNT(*) AS TotalCount
	FROM [dbo].[Contract] c WITH (NOLOCK)
	INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
	INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
	WHERE c.OrganisationId = @OrganisationId
	GROUP BY s.Name;