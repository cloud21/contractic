﻿CREATE PROCEDURE sp_ContractStatistics(@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS

	DECLARE @XmlParams XML = CONVERT(XML, @Params);
	DECLARE @Report AS VARCHAR(255);
	
	SELECT @Report = @XmlParams.value('(//entry[string/text() = "report"]/*[position()=2]/*[position()=1]/text())[1]', 'VARCHAR(255)');

	IF @Report = 'max'
	BEGIN
		/*Total contract value managed by status*/
		SELECT name, MAX(total) AS [max] FROM
		(SELECT c.Id, s.Name AS name
				, SUM(CONVERT(XML, cd.Value).value('(/value/projected)[1]', 'NUMERIC(30,10)') ) AS [total]
			FROM [dbo].[Contract] c WITH (NOLOCK)
			INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
			INNER JOIN [dbo].[ContractDetail] cd WITH (NOLOCK) ON c.Id = cd.ContractId
			INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
			INNER JOIN [dbo].[Group] g ON g.Id = cd.GroupId AND g.Name = 'VALUE' AND g.ModuleId = m.Id
			WHERE c.OrganisationId = @OrganisationId
			GROUP BY c.Id, s.Name) a
			GROUP BY name;
	END
	ELSE IF @Report = 'value'
	BEGIN
		/*Total contract value managed by status*/
		SELECT s.Name AS name
				, SUM(CONVERT(XML, cd.Value).value('(/value/projected)[1]', 'NUMERIC(30,10)') ) AS [total]
			FROM [dbo].[Contract] c WITH (NOLOCK)
			INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
			INNER JOIN [dbo].[ContractDetail] cd WITH (NOLOCK) ON c.Id = cd.ContractId
			INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
			INNER JOIN [dbo].[Group] g ON g.Id = cd.GroupId AND g.Name = 'VALUE' AND g.ModuleId = m.Id
			WHERE c.OrganisationId = @OrganisationId
			GROUP BY s.Name;
	END
	ELSE IF @Report = 'status' 
	BEGIN
		/*Contract count by status*/
		SELECT s.Name AS name, COUNT(*) AS [total]
			FROM [dbo].[Contract] c WITH (NOLOCK)
			INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
			INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
			WHERE c.OrganisationId = @OrganisationId
			GROUP BY s.Name;
	END