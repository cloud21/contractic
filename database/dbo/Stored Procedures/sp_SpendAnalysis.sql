﻿CREATE PROCEDURE [dbo].[sp_SpendAnalysis](@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS
	
	DECLARE @XmlParams XML = CONVERT(XML, @Params);
	DECLARE @ContractId BIGINT;
	
	SELECT @ContractId = @XmlParams.value('(//entry[string/text() = "contractId"]/list/string/text())[1]', 'BIGINT');

	WITH ContractValue(Id, ContractId, Name, ContractStartDate, ContractEndDate, Period, Value, StartDate) 
	AS (SELECT cd.Id, cd.ContractId, cd.Name, c.StartDate, c.EndDate, CONVERT(XML, cd.Description) AS Period, CONVERT(XML, cd.Value) AS Value, c.StartDate FROM [dbo].[ContractDetail] cd 
		INNER JOIN [Group] g ON cd.GroupId = g.Id 
		INNER JOIN [Contract] c ON c.Id = cd.ContractId WHERE c.Id = @ContractId AND g.Name = 'VALUE'),
	ContractValue2(ContractId, Name, ContractStartDate, ContractEndDate, PeriodFrequency, PeriodValue, StartDate, EndDate, Projected, Actual) 
	AS (SELECT ContractId, Name, ContractStartDate, ContractEndDate, Period.value('(//frequency)[1]', 'VARCHAR(5)') AS PeriodFrequency, Period.value('(//value)[1]', 'VARCHAR(5)') AS PeriodValue,
			CASE Period.value('(//frequency)[1]', 'VARCHAR(5)')
				WHEN 'Y' THEN DATEADD(YY, CONVERT(INT, Period.value('(//value)[1]', 'VARCHAR(5)')), StartDate)
				WHEN 'Q' THEN DATEADD(M, CONVERT(INT, Period.value('(//value)[1]', 'VARCHAR(5)')) * 3, StartDate)
				WHEN 'M' THEN DATEADD(M, CONVERT(INT, Period.value('(//value)[1]', 'VARCHAR(5)')), StartDate)
				ELSE CONVERT(DATETIME, Period.value('(//from)[1]', 'VARCHAR(20)'))
			END AS StartDate, CONVERT(DATETIME, Period.value('(//to)[1]', 'VARCHAR(20)'))
		, 
			CONVERT(NUMERIC(30,10), Value.value('(//projected)[1]', 'VARCHAR(MAX)')) AS Projected
			, CONVERT(NUMERIC(30,10), Value.value('(//actual)[1]', 'VARCHAR(MAX)')) AS Actual FROM ContractValue),
	ContractValue3(ContractId, Name, ContractStartDate, ContractEndDate, ValueStartDate, ValueEndDate, Projected, Actual)
	AS (SELECT ContractId, Name, ContractStartDate, ContractEndDate, StartDate AS ValueStartDate, CASE PeriodFrequency
				WHEN 'Y' THEN DATEADD(D, -1, DATEADD(YY, 1, StartDate))
				WHEN 'Q' THEN DATEADD(D, -1, DATEADD(M, 1, StartDate))
				WHEN 'M' THEN DATEADD(D, -1, DATEADD(M, 1, StartDate))
				ELSE EndDate
			END AS ValueEndDate, Projected, ISNULL(Actual, 0) AS Actual FROM ContractValue2),
	ContractValue4(ContractId, DatePoint, Name, Projected, Actual)
	AS (SELECT cv.ContractId, DATEDIFF(YY, cv.ContractStartDate, ud.DatePoint) AS DatePoint, cv.Name, Projected/DATEDIFF(D, ValueStartDate, ValueEndDate) AS ProjectedPerDay, Actual/DATEDIFF(D, ValueStartDate, ValueEndDate) AS ActualPerDay 
	FROM UtilDate ud LEFT JOIN ContractValue3 c ON ud.DatePoint BETWEEN c.ValueStartDate AND c.ValueEndDate
	CROSS JOIN (SELECT DISTINCT ContractId, Name, ContractStartDate, ContractEndDate FROM ContractValue) cv
			 WHERE ud.DatePoint BETWEEN cv.ContractStartDate AND cv.ContractEndDate)
	SELECT DatePoint, Name, SUM(Projected) AS Projected, SUM(Actual) AS Actual FROM ContractValue4
	 WHERE ContractId = @ContractId GROUP BY DatePoint, Name ORDER BY Name, DatePoint;
--SELECT * FROM ContractDetail;

--SELECT * FROM UtilDate ORDER BY DatePoint;