﻿CREATE PROCEDURE sp_UserContractActivity(@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS

	DECLARE @XmlParams XML = CONVERT(XML, @Params);
	DECLARE @PersonId BIGINT;
	
	SELECT @PersonId = @XmlParams.value('(//entry[string/text() = "personId"]/*[position()=2]/*[position()=1]/text())[1]', 'BIGINT');

	WITH RecentContract(UserId, ExternalId, LastRead)
	AS (SELECT a.UserId, ExternalId, MAX(Created) 
		FROM [dbo].[Audit] a  WITH (NOLOCK)
		INNER JOIN Module m ON a.ModuleId = m.Id 
		INNER JOIN AuditAction aa ON a.AuditActionId = aa.Id
		WHERE m.Name = 'Contract' AND aa.Name = 'Read'
		GROUP BY a.UserId, ExternalId)
	SELECT c.*
		FROM RecentContract rc 
		INNER JOIN dbo.Contract c WITH (NOLOCK) ON rc.Externalid = c.Id
		INNER JOIN dbo.[User] u WITH (NOLOCK) ON rc.UserId = u.Id
		WHERE u.PersonId = @PersonId 
		ORDER BY LastRead DESC;