﻿CREATE PROCEDURE [dbo].[sp_UserContractActivity](@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS

	DECLARE @XmlParams XML = CONVERT(XML, @Params);
	DECLARE @PersonId BIGINT;
	DECLARE @Limit INT;
	DECLARE @Module VARCHAR(255);

	SELECT @PersonId = @XmlParams.value('(//entry[string/text() = "personId"]/*[position()=2]/*[position()=1]/text())[1]', 'BIGINT');
	SELECT @Limit = @XmlParams.value('(//entry[string/text() = "limit"]/*[position()=2]/*[position()=1]/text())[1]', 'INT');
	SELECT @Module = 'Contract'
	
	IF @Limit IS NULL SET @Limit = 2147483647;

	WITH RecentContract(UserId, ExternalId, LastRead)
	AS (SELECT a.UserId, ExternalId, MAX(Created) 
		FROM [dbo].[Audit] a  WITH (NOLOCK)
		INNER JOIN Module m ON a.ModuleId = m.Id 
		INNER JOIN AuditAction aa ON a.AuditActionId = aa.Id
		WHERE m.Name = @Module AND aa.Name = 'Read'
		GROUP BY a.UserId, ExternalId)
	SELECT TOP (@Limit) c.*
		FROM RecentContract rc 
		INNER JOIN dbo.Contract c WITH (NOLOCK) ON rc.Externalid = c.Id
		INNER JOIN dbo.[User] u WITH (NOLOCK) ON rc.UserId = u.Id
		WHERE u.PersonId = @PersonId 
		ORDER BY LastRead DESC;