﻿CREATE PROCEDURE [dbo].[sp_UserTaskActivity](@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS
	DECLARE @XmlParams XML = CONVERT(XML, @Params);
	DECLARE @PersonId BIGINT;
	DECLARE @Limit INT;

	SELECT @PersonId = @XmlParams.value('(//entry[string/text() = "personId"]/*[position()=2]/*[position()=1]/text())[1]', 'BIGINT');
	SELECT @Limit = @XmlParams.value('(//entry[string/text() = "limit"]/*[position()=2]/*[position()=1]/text())[1]', 'INT');
	
	IF @Limit IS NULL SET @Limit = 2147483647;

	SELECT t.*
		FROM dbo.Task t WITH (NOLOCK)
		INNER JOIN dbo.Contract c WITH (NOLOCK) ON t.ExternalId = c.Id
		INNER JOIN dbo.Module m ON t.ModuleId = m.Id AND m.Name = 'Contract'
		WHERE c.OrganisationId = @OrganisationId AND (c.OwnerId = @PersonId OR t.PersonID = @PersonId)
	UNION
	SELECT t.*
		FROM dbo.Task t WITH (NOLOCK)
		INNER JOIN dbo.ContractDetail cd WITH (NOLOCK) ON t.ExternalId = cd.Id
		INNER JOIN dbo.Contract c WITH (NOLOCK) ON cd.ContractId = c.Id
		INNER JOIN dbo.Module m ON t.ModuleId = m.Id AND m.Name = 'Contract Detail'
		WHERE c.OrganisationId = @OrganisationId AND (c.OwnerId = @PersonId OR t.PersonID = @PersonId)
		ORDER BY t.StartDate DESC;