package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractTypeDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.DataSetupTestCategory;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;
import contractic.service.util.UpdateTestCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class ContractTypeControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	 
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@Autowired
    private ContractTypeController contractTypeController;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	@Category({ DataSetupTestCategory.class, ReadTestCategory.class })
	@Test
	public void testGet() throws Exception {
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
		System.out.println(ref);
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	ContractTypeDO[] contractTypes = TestUtil.getContractTypes(mockMvc, token, organisationId);

    	// Assert that we received an array back
    	Assert.assertTrue(contractTypes.length >= 0);
    	
    	if(contractTypes.length > 0){
			ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/contracttype/%s", organisationId, contractTypes[0].getId()))
					.header("Authorization", token))
					.andExpect(status().isOk());
	    	MvcResult result = personAction.andReturn();
	    	String response = result.getResponse().getContentAsString();
	    	
	    	ContractTypeDO contractTypeDO = new ObjectMapper().readValue(response, ContractTypeDO.class);
	    	
	        Assert.assertNotNull(contractTypeDO);
    	}
	}

	@Category({ DataSetupTestCategory.class })
	@Test
	public void testDelete() throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
    	
    	SessionDO sessionDO = mapper.convertValue(session.getRef(), SessionDO.class);

    	// Create a type
		ContractTypeDO contractTypeDO = new ContractTypeDO();
		final String name = String.format("%s: %s", CreateTestCategory.TestID(), "TYPE NAME (DELETE TEST)");
		final String description = String.format("%s: %s", CreateTestCategory.TestID(), "TYPE DESCRIPTION (DELETE TEST)");
		contractTypeDO.setName(name);
		contractTypeDO.setDescription(description);
		contractTypeDO.setActive(true);
		
		ResultActions actions = postContractType(contractTypeDO, sessionDO.getOrganisationId(), sessionDO.getToken())
				.andExpect(status().isOk());
        
		// Find the type
        ContractTypeDO[] contractTypes = TestUtil.getContractTypes(mockMvc, sessionDO.getToken(), sessionDO.getOrganisationId());
        contractTypeDO = Arrays.stream(contractTypes).filter(type -> type.getName().equals(name)).findFirst().get();
        
        
        // Delete the type
        actions = deleteContractType(contractTypeDO, sessionDO.getOrganisationId(), sessionDO.getToken())
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        
		MessageDO message = mapper.readValue(response, MessageDO.class);
        
		// Assert that we processed successfully
        Assert.assertEquals(MessageDO.OK, message.getType());

	}
	
	@Category({ DataSetupTestCategory.class})
	@Test
	public void testDeleteException() throws Exception{

		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Create a type
		ContractTypeDO contractTypeDO = new ContractTypeDO();
		final String name = String.format("%s: %s", CreateTestCategory.TestID(), "TYPE NAME");
		final String description = String.format("%s: %s", CreateTestCategory.TestID(), "TYPE DESCRIPTION");
		contractTypeDO.setName(name);
		contractTypeDO.setDescription(description);
		contractTypeDO.setActive(true);
		
		ResultActions actions = postContractType(contractTypeDO, organisationId, token)
				.andExpect(status().isOk());
        
        ContractTypeDO[] contractTypes = TestUtil.getContractTypes(mockMvc, token, organisationId);
        contractTypeDO = Arrays.stream(contractTypes).filter(type -> type.getName().equals(name)).findFirst().get();
        
        // Associate the new contract type to a contract
        ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
        ContractDO contract = contracts[new Random().nextInt(contracts.length)];
        contract.setType(new ValueDO(contractTypeDO.getId()));
        actions = TestUtil.putContract(mockMvc, organisationId, token, contract);
        
        // Now attempt to delete the contract type
        actions = deleteContractType(contractTypeDO, organisationId, token)
        		.andExpect(status().isBadRequest());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        System.out.println(response);
        Assert.assertNotNull(response);
	}
	
	
	private ResultActions deleteContractType(final ContractTypeDO contractTypeDO, String organisationId, String token) throws Exception{
        return mockMvc.perform(delete(String.format("/organisation/%s/contracttype/%s", organisationId, contractTypeDO.getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token));
	}
	
	@Category({ DataSetupTestCategory.class, UpdateTestCategory.class })
	@Test
	public void testPut() throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
        
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
		// Get a single object
    	ContractTypeDO[] contractTypes = TestUtil.getContractTypes(mockMvc, token, organisationId);
    	if(contractTypes.length >= 0){
	    	ContractTypeDO contractTypeDO = contractTypes[new Random().nextInt(contractTypes.length)];
	        
	    	String description = contractTypeDO.getDescription();
	    	Matcher matcher = UpdateTestCategory.TestIDPattern.matcher(description);
	    	if(matcher.matches()){
	    		description = matcher.group(2).trim();
	    	}
	    	
	    	contractTypeDO.setDescription(String.format("%s: %s", UpdateTestCategory.TestID, description));
	 		contractTypeDO.setActive(!contractTypeDO.getActive());
	 		
	        // Update the contracttype
	        
	    	String content = mapper.writeValueAsString(contractTypeDO);
			ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/contracttype", organisationId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", token)
	        		.content(content))
	        		.andExpect(status().isOk());
	    	
			MvcResult result = actions.andReturn();
			String response = result.getResponse().getContentAsString();
			MessageDO message = mapper.readValue(response, MessageDO.class);
	        
			// Assert that we processed successfully
	        Assert.assertEquals(MessageDO.OK, message.getType());
	        
	        
	        contractTypes = TestUtil.getContractTypes(mockMvc, token, organisationId);
	        ContractTypeDO contractTypeDO2 = Arrays.stream(contractTypes).filter(ct -> ct.getId().equals(contractTypeDO.getId())).findFirst().get();
	        
	        Assert.assertEquals(contractTypeDO.getDescription(), contractTypeDO2.getDescription());
	        Assert.assertEquals(contractTypeDO.getActive(), contractTypeDO2.getActive());
    	}
        
	}
	
	@Category({ DataSetupTestCategory.class, CreateTestCategory.class })
	@Test
	public void testPost() throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
        
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Create a status
		final ContractTypeDO contractTypeDO = new ContractTypeDO();
		contractTypeDO.setName(String.format("%s: %s", CreateTestCategory.TestID(), "TYPE NAME (POST TEST)"));
		contractTypeDO.setDescription(String.format("%s: %s", CreateTestCategory.TestID(), "TYPE DESCRIPTION (POST TEST)"));
		contractTypeDO.setActive(true);
		
    	String content = mapper.writeValueAsString(contractTypeDO);

        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/contracttype", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
		MessageDO message = mapper.readValue(response, MessageDO.class);
		
		Assert.assertEquals(MessageDO.OK, message.getType());
	}
	
	private ResultActions postContractType(final ContractTypeDO contractTypeDO, String organisationId, String token) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		String content = mapper.writeValueAsString(contractTypeDO);

        return mockMvc.perform(post(String.format("/organisation/%s/contracttype", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content));
	}
	
	@Category({ DataSetupTestCategory.class, CreateTestCategory.class })
	@Test
	public void testPostException() throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
        
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Create a status
		final ContractTypeDO contractTypeDO = new ContractTypeDO();
		final String name = String.format("%s: %s", CreateTestCategory.TestID(), "TYPE NAME");
		final String description = String.format("%s: %s", CreateTestCategory.TestID(), "TYPE DESCRIPTION");
		contractTypeDO.setName(name);
		contractTypeDO.setDescription(description);
		contractTypeDO.setActive(true);
		
		ResultActions actions = postContractType(contractTypeDO, organisationId, token);
        actions.andExpect(status().isOk());
        
		actions = postContractType(contractTypeDO, organisationId, token);
        actions.andExpect(status().isBadRequest());        
        
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
		
		Assert.assertNotNull(response);
	}
}
