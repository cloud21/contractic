package contractic.service.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import contractic.model.data.store.ContractDetail;
import contractic.model.data.store.Group;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractDetailDO;
import contractic.service.domain.ContractExtensionDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.PeriodDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.TestUtil;

@RunWith(Parameterized.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class ContractExtensionDetailControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	String number;
	String extensionPeriod;
	String extensionFrom;
	String extensionTo;
	String noticePeriod;
	String noticePeriodOn;
	String expected;
	
	public  ContractExtensionDetailControllerTest(String number, String extensionPeriod, 
			String extensionFrom, String extensionTo, String noticePeriod, String noticePeriodOn, String expected){
		this.number = number;
		this.extensionPeriod = extensionPeriod;
		this.extensionFrom = extensionFrom;
		this.extensionTo = extensionTo;
		this.noticePeriod = noticePeriod;
		this.noticePeriodOn = noticePeriodOn;
		this.expected = expected;
	}
	
    @ClassRule public static final SpringClassRule SCR = new SpringClassRule();
    @Rule public final SpringMethodRule springMethodRule = new SpringMethodRule();
	
    @Parameterized.Parameters
    public static List<String[]> params() {
    	    	
        return Arrays.asList(new String[][] { 
        		{"1", "3", null, null, null, "30/04/2015", MessageDO.OK},
/*        		{"1", "3", null, null, null, "30/04/2015", MessageDO.ERROR},
        		{"2", "3", null, null, "1", null, MessageDO.OK},
        		{"3", null, null, null, null, null, MessageDO.ERROR},
        		{"4", null, "29/06/2014", "28/06/2015", "2", null, MessageDO.ERROR},*/
        		});
    }
    
	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}


	@Category({ CreateTestCategory.class })
	@Test
	public void testCreateBespokeDetail() throws Exception{
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Get the contract	
    	ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
    	ContractDO contract = contracts[new Random().nextInt(contracts.length)];
		
    	// A dummy detail
		final ContractDetailDO contractDetailDO = new ContractDetailDO();
		final ContractExtensionDO extensionDO = new ContractExtensionDO();
		contractDetailDO.setExtension(extensionDO);
		
		
		/*
		 * 	String number;
	String extensionPeriod;
	String extensionFrom;
	String extensionTo;
	String noticePeriod;
	String noticePeriodOn;
	String expected;
		 */
		if(number!=null)
			extensionDO.setNumber(new Integer(number));
		
		if(extensionPeriod!=null){
			PeriodDO periodDO = new PeriodDO();
			PeriodDO.Cycle cycle = new PeriodDO.Cycle("Y", new Integer(extensionPeriod));
			periodDO.setCycle(cycle);
			extensionDO.setPeriod(periodDO);
		}else if(extensionFrom!=null && extensionTo!=null){
			PeriodDO periodDO = new PeriodDO();
			periodDO.setFrom(extensionFrom);
			periodDO.setTo(extensionTo);
			extensionDO.setPeriod(periodDO);
		}
		
		if(noticePeriodOn!=null){
			PeriodDO period = new PeriodDO();
			period.setFrom(noticePeriodOn);
			extensionDO.setNotice(period);
		}
		
		contractDetailDO.setGroup(new ValueDO(null, ContractDetail.EXTENSION_GROUP_FIELD));
    	contractDetailDO.setContract(contract);
    	
    	MvcResult result = TestUtil.postDetail(mockMvc, token, organisationId, contractDetailDO)
    			.andExpect(status().isOk())
    			.andReturn();
    	String response = result.getResponse().getContentAsString();
    	MessageDO message = new ObjectMapper().readValue(response, MessageDO.class);
    	
        Assert.assertEquals(expected, message.getType());
	}
}
