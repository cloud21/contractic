package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.GroupSet;
import contractic.model.data.store.Module;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.GroupDO;
import contractic.service.domain.GroupSetDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ModuleDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;
import contractic.service.util.UpdateTestCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class GroupControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);

	}
	
	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}
	
	@Category({ ReadTestCategory.class })
	@Test
	public void testGetGroupsInGroupSet() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
		GroupSetDO groupSet = TestUtil.getGroupSet(mockMvc, token, organisationId, "DEPARTMENT");
		ModuleDO module = TestUtil.getModule(mockMvc, token, Module.PERSON);
		
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/group", organisationId))
				.header("Authorization", token)
				//.param("moduleId", module.getId())
				.param("groupSetId", groupSet.getId()));
    	MvcResult result = personAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	ObjectMapper mapper = new ObjectMapper();
    	GroupDO[] groups = mapper.readValue(response, GroupDO[].class);
    	
	}
	
	@Category({ ReadTestCategory.class })
	//@Ignore
	@Test
	public void testGetGroupsWithNoGroupSet() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		ModuleDO module = TestUtil.getModule(mockMvc, token, Module.CONTRACT);
		
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/group", organisationId))
				.header("Authorization", token)
				.param("moduleId", module.getId())
				);
    	MvcResult result = personAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	ObjectMapper mapper = new ObjectMapper();
    	GroupDO[] groups = mapper.readValue(response, GroupDO[].class);
    	
	}

	@Category({ ReadTestCategory.class })
	//@Ignore
	@Test
	public void testGet() throws Exception {
		
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
		String moduleId = TestUtil.getModuleId(mockMvc, Module.CONTRACT, token);
		GroupSetDO[] groupSets = TestUtil.getGroupSets(mockMvc, token, organisationId);
		Optional<GroupSetDO> groupSetOpt = Arrays.stream(groupSets).filter(gs -> gs.getName().equals("DEPARTMENT")).findFirst();
		assert groupSetOpt.isPresent();
    	
    	// Get the group
    	String groupId = null;
    	{
			ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/group", organisationId))
					.header("Authorization", token)
					//.param("groupSetId", groupSetOpt.get().getId())
					);
	    	MvcResult result = personAction.andReturn();
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	DocumentContext nDC = JsonPath.parse(response);
	    	groupId = nDC.read("[0].id");
    	}
    	
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/group", organisationId))
				.header("Authorization", token)
				.param("moduleId", moduleId)
				.param("groupId", groupId));
    	MvcResult result = personAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
        Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
	}

	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		// Get the PersonPOJO
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header("Authorization", "token"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	// Get the group
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/group", orgId))
				.header("Authorization", "token"));
    	result = personAction.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        System.out.println(response);
		
    	String groupId = nDC.read("$[0].id");
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/group/%s", orgId, groupId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token"))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.OK, type); 
	}
	
	@Category({ UpdateTestCategory.class })
	@Test
	public void testPut() throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
        
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		// Get a single group
    	
    	ModuleDO module = TestUtil.getModule(mockMvc, token, Module.PERSON);
    	GroupSetDO[] groupSets = TestUtil.getGroupSets(mockMvc, token, organisationId);
    	GroupSetDO groupSet = Arrays.stream(groupSets).filter(gs -> gs.getName().equals(GroupSet.DEPARTMENT)).findFirst().get();
    	
 		GroupDO[] groups = TestUtil.getGroup(mockMvc, token, module, groupSet, organisationId);
 		GroupDO group = groups[new Random().nextInt(groups.length)];
        
 		String name = group.getName();
 		String description = group.getDescription();
 		
 		if(description != null){
	    	Matcher matcher = UpdateTestCategory.TestIDPattern.matcher(description);
	    	if(matcher.matches()){
	    		description = matcher.group(2).trim();
	    	}
 		}
    	
 		if(name!=null){
 			Matcher matcher = UpdateTestCategory.TestIDPattern.matcher(name);
	    	if(matcher.matches()){
	    		name = matcher.group(2).trim();
	    	}
 		}

    	group.setName(name);
    	group.setDescription(description);
    	group.setActive(group.getActive() == null ? Boolean.FALSE : !group.getActive());
    	
        // Update the group
        
    	ResultActions actions = putGroup(token, organisationId, group);
    	
		MvcResult result = actions.andReturn();
		String response = result.getResponse().getContentAsString();
		MessageDO messageDO = mapper.readValue(response, MessageDO.class);
        
		// Assert that all went well
        Assert.assertEquals(MessageDO.OK, messageDO.getType());
        
        // Now make sure we updated
        groups = TestUtil.getGroup(mockMvc, token, module, groupSet, organisationId);
        GroupDO group1 = Arrays.stream(groups).filter(g -> g.getId().equals(group.getId())).findFirst().get();
        
        Assert.assertEquals(group.getName(), group1.getName());
        Assert.assertEquals(group.getDescription(), group1.getDescription());
        Assert.assertEquals(group.getModuleId(), group.getModuleId());
        Assert.assertEquals(group.getActive(), group1.getActive());
        
	}

	private ResultActions putGroup(String token,
			String organisationId, GroupDO group)
			throws Exception {
		String content = new ObjectMapper().writeValueAsString(group);
		ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/group", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
		return actions;
	}
	
	@Category({ CreateTestCategory.class })
	@Test
	public void testPost() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

		GroupSetDO[] groupSets = TestUtil.getGroupSets(mockMvc, token, organisationId);

    	ObjectMapper mapper = new ObjectMapper();
		
    	// Create a group
		final GroupDO groupDO = new GroupDO();
    	groupDO.setName(String.format("%s: %s", CreateTestCategory.TestID(), "GROUP NAME"));
    	groupDO.setDescription(String.format("%s: %s", CreateTestCategory.TestID(), "GROUP DESCRIPTION"));
    	groupDO.setGroupset(new ValueDO(null, groupSets[new Random().nextInt(groupSets.length)].getName()));
    	groupDO.setActive(Boolean.TRUE);
    	
    	String content = mapper.writeValueAsString(groupDO);
    	
        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/group", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        MessageDO messageDO = mapper.readValue(response, MessageDO.class);
        Assert.assertEquals(MessageDO.OK, messageDO.getType());
	}
}
