package contractic.service.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractDetailDO;
import contractic.service.domain.ContractValueDO;
import contractic.service.domain.GroupDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.PeriodDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.TestUtil;

@RunWith(Parameterized.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class ContractValueDetailControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
    @ClassRule
    public static final SpringClassRule SCR = new SpringClassRule();
    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	String name;
	String from;
	String to;
	String frequency;
	String frequencyValue;
	String projected;
	String actual;
	String expected;
	
	public  ContractValueDetailControllerTest(String name, String from, String to, String frequency, String frequencyValue, String projected, String actual, String expected){
		this.name = name;
		this.from = from;
		this.to = to;
		this.frequency = frequency;
		this.frequencyValue = frequencyValue;
		this.projected = projected;;
		this.actual = actual;
		this.expected = expected;
	}
	
    @Parameterized.Parameters
    public static List<String[]> params() {
    	Random random = new Random();
        return Arrays.asList(new String[][] { 
        		{"VALUE NAME1", "10/01/2015", "10/01/2017", null, null, "10000000", null, MessageDO.OK},
        		{"VALUE NAME2", "10/01/2015", "10/01/2017", null, null, null, null, MessageDO.ERROR},
        		{"VALUE NAME1", null, null, "Q", "9", "10000000", null, MessageDO.OK},
        		{"VALUE NAME2", null, null, "Y", "9", null, null, MessageDO.ERROR},
        		{"VALUE NAME1", null, null, "Q", "9", new Double(random.nextFloat() * Short.MAX_VALUE + 5000).toString(), 
        			new Double(random.nextFloat() * Short.MAX_VALUE + 5000).toString(), MessageDO.OK},
        		/*{"VALUE NAME3", null, null, GroupDO.VALUE_GROUP_NAME, MessageDO.ERROR}*/
        		});
    }
    
	@Before
	public void setUp() throws Exception {
		if(setUpIsDone.get())
			return;

		synchronized(setUpIsDone){
	        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
			String user = System.getProperty("username");
			String password = System.getProperty("password");
			String orgCode = System.getProperty("orgCode");
			
			user = user==null ? "mawandm@yahoo.co.uk" : user;
			password = password==null ? "semica" : password;
			orgCode = orgCode==null ? "homeoffice" : orgCode;
			
			SessionDO sessionDO = new SessionDO();
			sessionDO.setUsername(user);
			sessionDO.setPassword(password);
			
			// Get Organisation from which to get contracts
			session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
			setUpIsDone.set(true);
		}
	}


	@Category({ CreateTestCategory.class })
	@Test
	public void testCreateDetail() throws Exception{
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Get the contract	
    	ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
    	ContractDO contract = contracts[new Random().nextInt(contracts.length)];
		
    	// A dummy detail
		final ContractDetailDO contractDetailDO = new ContractDetailDO();
		
		if(name!=null)
			contractDetailDO.setName(String.format("%s: %s", CreateTestCategory.TestID(), name));
		
		PeriodDO period = new PeriodDO();
		if(frequency!=null && frequencyValue!=null){
			period.setCycle(new PeriodDO.Cycle(frequency, Integer.parseInt(frequencyValue)));
		}
		period.setFrom(from);
		period.setTo(to);
		contractDetailDO.setPeriod(period);
		
		ContractValueDO value = new ContractValueDO();
		value.setActual(actual);
		value.setProjected(projected);
		contractDetailDO.setValue(value);
    	contractDetailDO.setGroup(new ValueDO(null, GroupDO.VALUE_GROUP_NAME));
    	contractDetailDO.setContract(contract);
    	
    	MvcResult result = TestUtil.postDetail(mockMvc, token, organisationId, contractDetailDO)
    			.andExpect(status().isOk())
    			.andReturn();
    	String response = result.getResponse().getContentAsString();
    	MessageDO message = new ObjectMapper().readValue(response, MessageDO.class);
    	
        Assert.assertEquals(expected, message.getType());
	}
}
