package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class FileControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testPost() throws Exception{
		
		String endpoint = null;
		
		String orgId;
		{
			endpoint = "/organisation";
			// Get Organisation from which to get persons
			ResultActions organisationAction = mockMvc.perform(get(endpoint));
	    	MvcResult result = organisationAction.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	// Get the organisation's UUID
	    	orgId = nDC.read("$[0].id");
		}
		
        endpoint = String.format("/organisation/%s/file", orgId);
        System.out.println(endpoint);

        {
	        Path path = Paths.get("C:\\Users\\Michael Sekamanya\\Documents\\CTOProcurementContractsPortal.doc");
	        
	        MockMultipartFile firstFile = new MockMultipartFile("file", path.toFile().getName(), "undefined", Files.readAllBytes(path));
	
	        ResultActions action = mockMvc.perform(
	        		MockMvcRequestBuilders.fileUpload(endpoint)
	        		.file(firstFile)
	        		.header("Authorization", "token"))
	                .andExpect(status().isOk());
	        
	    	MvcResult result = action.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
        }
	}

	@Ignore
	@Test
	public void testGet() throws Exception {
		
		// Get Organisation from which to get persons
		ResultActions organisationAction = mockMvc.perform(get("/organisation"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	{
	    	// Search the person   	
			ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/file", orgId))
					.param("q", "Mich")
					.param("autoComplete", "true")
					.header("Authorization", "token"));
	    	result = personAction.andReturn();
	    	nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	response = result.getResponse().getContentAsString();
	    	System.out.println(response);
    	}
    	
	}
}
