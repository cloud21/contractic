package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.Module;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.AddressDO;
import contractic.service.domain.AutoCompleteDO;
import contractic.service.domain.ContactDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ModuleDO;
import contractic.service.domain.PersonDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.StaticDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class ContactControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	//@Ignore
	@Category({ ReadTestCategory.class })
	@Test
	public void testGet() throws Exception{

		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

		PersonDO[] persons = TestUtil.getPersons(mockMvc, token, organisationId);
		
		Assert.assertTrue(persons != null && persons.length>=0);
		
		if(persons.length > 0){
	    	final PersonDO pp = persons[new Random().nextInt(persons.length)];
	    	
	    	ModuleDO module = TestUtil.getModule(mockMvc, token, Module.PERSON);
			ContactDO[] contacts = TestUtil.getContacts(mockMvc, token, organisationId, pp.getId(), module);
			Assert.assertTrue(contacts != null && contacts.length>=0);
		}
	}
	
	@Ignore
	@Test
	public void testPut() throws Exception{
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername("contractic-admin@cloud21.net");
		sessionDO.setPassword("password");
		// Get Organisation from which to get contracts
		MessageDO session = TestUtil.createSession(mockMvc, sessionDO, "homeoffice");
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	ObjectMapper mapper = new ObjectMapper();
		PersonDO[] persons = null;
		// Get the address type's UUID
		{
    		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/person", organisationId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", token))
	        		.andExpect(status().isOk());
	    	MvcResult result = personAction.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	persons = mapper.readValue(response, PersonDO[].class);
		}
    	final PersonDO pp = Arrays.stream(persons)
    			.filter(person -> person.getFirstName().contains("AARON") && person.getLastName().contains("CURTIS"))
    			.collect(Collectors.toList()).get(0);
    	
    	ModuleDO module = TestUtil.getModule(mockMvc, token, Module.PERSON);
    	
		ContactDO[] contacts = TestUtil.getContacts(mockMvc, token, organisationId, pp.getId(), module);
        AddressDO[] addresses = TestUtil.getAddress(mockMvc, token, organisationId);
        StaticDO[] addressTypes = TestUtil.getAddressType(mockMvc, token, organisationId);
        
        ContactDO contact = contacts[0];
        contact.setAddress(new ValueDO(addresses[0].getId(), ""));
        
    	String content = mapper.writeValueAsString(contact);
    	
		ResultActions action = mockMvc.perform(put(String.format("/organisation/%s/contact", organisationId))
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.header("Authorization", token)
				.content(content)
				);
    	MvcResult result = action.andReturn();
    	String response = result.getResponse().getContentAsString();
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        action.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.OK, type); 
	}

	@Ignore
	@Test
	public void testPost() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	ObjectMapper mapper = new ObjectMapper();
		PersonDO[] persons = null;
		// Get the address type's UUID
		{
    		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/person", organisationId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", token))
	        		.andExpect(status().isOk());
	    	MvcResult result = personAction.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	persons = mapper.readValue(response, PersonDO[].class);
		}
    	final PersonDO pp = Arrays.stream(persons)
    			.filter(person -> person.getFirstName().contains("AARON") && person.getLastName().contains("CURTIS"))
    			.collect(Collectors.toList()).get(0);
    	
    	ModuleDO module = TestUtil.getModule(mockMvc, token, Module.PERSON);
    	
        AutoCompleteDO[] addreSuggestions = TestUtil.searchAddress(mockMvc, token, organisationId, "auckland");
       
        ContactDO contact = new ContactDO();
        contact.setAddress(new ValueDO(addreSuggestions[0].getId(), addreSuggestions[0].getValue()));
        contact.setModuleId(module.getId());
        contact.setExternalId(pp.getId());
        
    	String content = mapper.writeValueAsString(contact);
    	
		ResultActions action = mockMvc.perform(post(String.format("/organisation/%s/contact", organisationId))
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.header("Authorization", token)
				.content(content)
				);
    	MvcResult result = action.andReturn();
    	String response = result.getResponse().getContentAsString();
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        action.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.OK, type); 
	}
	
	@Test
	public void testPostNewAddress() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	ObjectMapper mapper = new ObjectMapper();
    	
		PersonDO[] persons = TestUtil.getPersons(mockMvc, token, organisationId);
		Assert.assertTrue(persons != null && persons.length>=0);
		
		if(persons.length > 0){
	    	final PersonDO person = persons[new Random().nextInt(persons.length)];
	    	final ModuleDO module = TestUtil.getModule(mockMvc, token, Module.PERSON);
	        final StaticDO[] addressTypes = TestUtil.getAddressType(mockMvc, token, organisationId);
	        StaticDO addressType = Arrays.stream(addressTypes).filter(t -> t.getValue().equalsIgnoreCase("Twitter")).collect(Collectors.toList()).get(0);
	       
	        ContactDO contact = new ContactDO();
	        contact.setAddress(new ValueDO(null, "#merke"));
	        contact.setModuleId(module.getId());
	        contact.setExternalId(person.getId());
	        contact.setType(new ValueDO(addressType.getId()));
	        
	    	String content = mapper.writeValueAsString(contact);
	    	
			ResultActions action = mockMvc.perform(post(String.format("/organisation/%s/contact", organisationId))
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					.header("Authorization", token)
					.content(content)
					);
	    	MvcResult result = action.andReturn();
	    	String response = result.getResponse().getContentAsString();
	        MessageDO message = mapper.readValue(response, MessageDO.class);
	        
	        Assert.assertEquals(MessageDO.OK, message.getType()); 
		}
	}
}
