package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContractDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.TaskContactDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.TestUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class TaskContactControllerTest {

	private MockMvc mockMvc;
	private MessageDO session;
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}
	
	private String getOrganisation() throws Exception, UnsupportedEncodingException {
		String orgId = null;
		// Get the organisation
		{
			ResultActions organisationAction = mockMvc.perform(get("/organisation"));
			MvcResult result = organisationAction.andReturn();
			DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
			
			String response = result.getResponse().getContentAsString();
			System.out.println(response);
			
			// Get the organisation's UUID
			orgId = nDC.read("$[0].id");
		}
		return orgId;
	}

	//@Ignore
	@Test
	public void testGet() throws Exception {
		
		// Get the organisation's UUID
    	String orgId = this.getOrganisation();
    	
    	// Get all tasks
    	String taskId = null;
    	{
			ResultActions taskAction = mockMvc.perform(get(String.format("/organisation/%s/task", orgId))
					.header("Authorization", "token"))
					.andExpect(status().isOk());
			MvcResult result = taskAction.andReturn();
			String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	DocumentContext nDC = JsonPath.parse(response);
	    	
	    	taskId = nDC.read("[0].id");
    	}
    	
    	///organisation/{orgId}/task/{taskId}/contact/{contactId}
        ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/task/%s/contact", orgId, taskId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token"))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        
        System.out.println(response);
        
        DocumentContext nDC = JsonPath.parse(response);

	}

	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		// Get the ContractPOJO
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header("Authorization", "token"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	// Get the contract
		ResultActions contractAction = mockMvc.perform(get(String.format("/organisation/%s/contract", orgId))
				.header("Authorization", "token"));
    	result = contractAction.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        System.out.println(response);
		
    	final ContractDO pp = new ContractDO();
    	pp.setId(nDC.read("$[0].id"));
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(pp);
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/contract/%s", orgId, pp.getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.INFO, type); 
	}
	
	@Ignore
	@Test
	public void testPut() throws Exception{
    	
		// Get the ContractPOJO
		String orgId = null;
		ContractDO pp = null;
		{
			ResultActions organisationAction = mockMvc.perform(get("/organisation")
					.header("Authorization", "token"));
	    	MvcResult result = organisationAction.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	// Get the organisation's UUID
	    	orgId = nDC.read("$[0].id");
	    	
	    	// Get the contract
			ResultActions contractAction = mockMvc.perform(get(String.format("/organisation/%s/contract", orgId))
					.header("Authorization", "token"));
	    	result = contractAction.andReturn();
	        response = result.getResponse().getContentAsString();
	        nDC = JsonPath.parse(response);
	        System.out.println(response);
	        
	    	pp = new ContractDO();
	    	pp.setId(nDC.read("$[0].id"));
	    	pp.setTitle(nDC.read("$[0].title"));
		}
		
		String addressTypeId = null;
		DocumentContext nDC = null;
		// Get the address type's UUID
		{
    		ResultActions addressTypeAction = mockMvc.perform(get("/addresstype")
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", "token"))
	        		.andExpect(status().isOk());
	    	MvcResult result = addressTypeAction.andReturn();
	    	nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	// Get the organisation's UUID
	    	List<String> res = nDC.read("$..[?(@.value =='Email')].id");
	    	addressTypeId = res.get(0);
		}
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(pp);
    	
        ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/contract", orgId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.INFO, type); 
	}
	
	@Ignore
	@Test
	public void testPost() throws Exception{
    	
		// Get the organisation's UUID
    	String orgId = this.getOrganisation();
    	
    	// Get all tasks
    	String taskId = null;
    	{
			ResultActions taskAction = mockMvc.perform(get(String.format("/organisation/%s/task", orgId))
					.header("Authorization", "token"))
					.andExpect(status().isOk());
			MvcResult result = taskAction.andReturn();
			String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	DocumentContext nDC = JsonPath.parse(response);
	    	
	    	taskId = nDC.read("[0].id");
    	}
    	
    	
    	// Get a specific task
    	{
			ResultActions taskAction = mockMvc.perform(get(String.format("/organisation/%s/task", orgId))
					.header("Authorization", "token")
					.param("taskId", taskId))
					.andExpect(status().isOk());
			MvcResult result = taskAction.andReturn();
			DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
			String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
    	}
    	
    	String contactId = null;
    	try{
    		ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/contact", orgId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", "token"))
	        		.andExpect(status().isOk());
	    	
    		MvcResult result = actions.andReturn();
    		String response = result.getResponse().getContentAsString();
    		DocumentContext nDC = JsonPath.parse(response);
	        System.out.println(response);
	        
	        contactId = nDC.read("[0].id");
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw t;
    	}
    	
    	// Get the contract	
    	final TaskContactDO contactDO = new TaskContactDO();
    	contactDO.setContact(new ValueDO(contactId, null));
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(contactDO);
    	
    	System.out.println(content);
    	
        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/task/%s/contact", orgId, taskId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        
        System.out.println(response);
        
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
		
        Assert.assertEquals(MessageDO.OK, type);
	}
}
