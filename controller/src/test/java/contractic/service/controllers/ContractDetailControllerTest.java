package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.ContractDetail;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContractCCNDO;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractDetailDO;
import contractic.service.domain.ContractValueDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.PeriodDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;
import contractic.service.util.UpdateTestCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class ContractDetailControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);

		
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	@Category({ ReadTestCategory.class })
	@Test
	public void testGet() throws Exception {
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
    	ContractDO contract = contracts[new Random().nextInt(contracts.length)];    	
    	
    	// Get for a specific contract
	
		ResultActions contractDetailAction = mockMvc.perform(get(String.format("/organisation/%s/contractdetail", organisationId))
				.header("Authorization", token)
				.param("contractId", contract.getId())
				//.param("group", "^(?!.*(VALUE|SUMMARY)).*$")
				.param("group", "CCN")
				)
				.andExpect(status().isOk());
		MvcResult result = contractDetailAction.andReturn();
    	String response = result.getResponse().getContentAsString();
    	
	}
	
	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		// Get the ContractPOJO
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header("Authorization", "token"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	// Get the contract
		ResultActions contractDetailAction = mockMvc.perform(get(String.format("/organisation/%s/contractdetail", orgId))
				.header("Authorization", "token"));
    	result = contractDetailAction.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        System.out.println(response);
		
    	final ContractDetailDO pp = new ContractDetailDO();
    	pp.setId(nDC.read("$[0].id"));
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(pp);
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/contractdetail/%s", orgId, pp.getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.INFO, type); 
	}
	

	
	@Category({ UpdateTestCategory.class })
	@Test
	public void testPut() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	
    	// Get the contract
    	ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
    	ContractDO contract = contracts[new Random().nextInt(contracts.length)];
    	
    	final String groupName = "^(?!.*(VALUE|SUMMARY|CCN|CONTRACT_TERM_PERIOD|CONTRACT_TERM_NOTICE|CONTRACT_TERM_GOTO_MARKET)).*$";
    	// Read details that aren't of any special system groups
    	ContractDetailDO[] details = TestUtil.getContractDetail(mockMvc, organisationId, token, contract.getId(), groupName);
    	
		ContractDetailDO contractDetailDO = null;
		String[] groupNames = new String[]{"REFERENCE", "TEST"};
		
		if(details.length == 0){
			contractDetailDO = new ContractDetailDO();
	    	contractDetailDO.setName(String.format("%s: %s", UpdateTestCategory.TestID, "NAME"));
	    	contractDetailDO.setDescription(String.format("%s: %s", UpdateTestCategory.TestID, "DESCRIPTION"));
	    	contractDetailDO.setText(String.format("%s: %s", UpdateTestCategory.TestID, "DETAIL"));
	    	contractDetailDO.setGroup(new ValueDO(null, groupNames[new Random().nextInt(groupNames.length)]));
	    	contractDetailDO.setContract(contract);
	    	
	    	MvcResult result = TestUtil.postDetail(mockMvc, token, organisationId, contractDetailDO)
	    			.andExpect(status().isOk())
	    			.andReturn();
	    	String response = result.getResponse().getContentAsString();
	    	MessageDO message = new ObjectMapper().readValue(response, MessageDO.class);
			
			final Map<?, ?> ref2 = (LinkedHashMap<?,?>)message.getRef();
			String id = (String)ref2.get("id");
			
			details = TestUtil.getContractDetail(mockMvc, organisationId, token, contract.getId(), groupName);
			contractDetailDO = Arrays.stream(details).filter(cd -> cd.getId().equals(id)).findFirst().get();
			
		}else{
			contractDetailDO = details[new Random().nextInt(details.length)];
		}
		
		// We strip of the test timestamps (which are included to avoid duplicates)
		String name = contractDetailDO.getName();
		String detail = contractDetailDO.getText();
    	String description = contractDetailDO.getDescription();
    	if(description==null) description = "DESCRIPTION";
    	if(detail==null) detail = "DETAIL";
    	if(name == null) name = "NAME";
    	Matcher matcher = UpdateTestCategory.TestIDPattern.matcher(description);
    	if(matcher.matches()){
    		description = matcher.group(2).trim();
    	}
    	
    	matcher = UpdateTestCategory.TestIDPattern.matcher(name);
    	if(matcher.matches()){
    		name = matcher.group(2).trim();
    	}
    	matcher = UpdateTestCategory.TestIDPattern.matcher(detail);
    	if(matcher.matches()){
    		detail = matcher.group(2).trim();
    	}
		
    	name = String.format("%s: %s", UpdateTestCategory.TestID, name);
    	description = String.format("%s: %s", UpdateTestCategory.TestID, description);
    	
    	contractDetailDO.setName(name.substring(0, Math.min(name.length(), 50)));
    	contractDetailDO.setText(String.format("%s: %s", UpdateTestCategory.TestID, detail));
    	contractDetailDO.setDescription(description.substring(0, Math.min(description.length(), 255)));
    	final ValueDO groupDO = contractDetailDO.getGroup() == null ? new ValueDO() : contractDetailDO.getGroup();
    	
    	List<String> groupNameList = new ArrayList<>(Arrays.asList(groupNames));
    	groupNameList.remove(groupDO.getValue());
    	groupNames = Arrays.stream(groupNames).filter(gn -> !gn.equalsIgnoreCase(groupDO.getValue())).collect(Collectors.toList()).toArray(new String[0]);
    	
    	groupDO.setValue(groupNames[new Random().nextInt(groupNames.length)]);
    	contractDetailDO.setGroup(groupDO);
    	
    	String content = mapper.writeValueAsString(contractDetailDO);
    	
        ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/contractdetail", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        MessageDO message = new ObjectMapper().readValue(response, MessageDO.class);
        
        // Assert that we processed successfully
        Assert.assertEquals(MessageDO.OK, message.getType());
        
        
        // Assert that the updates were actually made
        details = TestUtil.getContractDetail(mockMvc, organisationId, token, contract.getId(), groupName);

        final ContractDetailDO contractDetailDO1 = contractDetailDO;
        ContractDetailDO contractDetailDO2 = Arrays.stream(details).filter(cd -> cd.getId().equals(contractDetailDO1.getId())).findFirst().get();
        
        Assert.assertEquals(contractDetailDO1.getName(), contractDetailDO2.getName());
        Assert.assertEquals(contractDetailDO1.getDescription(), contractDetailDO2.getDescription());
        Assert.assertEquals(contractDetailDO1.getText(), contractDetailDO2.getText());
        String groupValueExpected = contractDetailDO1.getGroup().getValue();
        String groupValue = contractDetailDO2.getGroup().getValue();
        Assert.assertTrue(String.format("Expected %s but got %s", groupValueExpected, groupValue), groupValueExpected.equalsIgnoreCase(groupValue));
        
	}
	
	static class ContractSummary{
		private ValueDO contract;
		private ValueDO group;
		private String text;
		private String name;
		public ValueDO getContract() {
			return contract;
		}
		public void setContract(ValueDO contract) {
			this.contract = contract;
		}
		public ValueDO getGroup() {
			return group;
		}
		public void setGroup(ValueDO group) {
			this.group = group;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		
	}
	
	@Category({ CreateTestCategory.class })
	@Test
	public void testPostSummary() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Get the contract	
    	ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
    	ContractDO contract = contracts[new Random().nextInt(contracts.length)];
		
    	// A dummy SUMMARY
    	{
    		final ContractSummary contractDetailDO = new ContractSummary();
    		contractDetailDO.setText("Test");
    		contractDetailDO.setName("Summary 1");
	    	contractDetailDO.setContract(new ValueDO(contract.getId()));
	    	contractDetailDO.setGroup(new ValueDO(null, ContractDetail.SUMMARY_GROUP_FIELD));
	    	
	    	/*MvcResult result = TestUtil.postDetail(mockMvc, token, organisationId, contractDetailDO)
	    			.andExpect(status().isOk())
	    			.andReturn();*/
	    	
	    	ObjectMapper mapper = new ObjectMapper();
	    	String content = mapper.writeValueAsString(contractDetailDO);
	    	
	    	MvcResult result = mockMvc.perform(post(String.format("/organisation/%s/contractdetail", organisationId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", token)
	        		.content(content)).andExpect(status().isOk())
	    			.andReturn();;
	    	
	    	String response = result.getResponse().getContentAsString();
	    	MessageDO message = new ObjectMapper().readValue(response, MessageDO.class);
	    	
	        Assert.assertEquals(MessageDO.OK, message.getType());
    	}
	}
	
	@Category({ CreateTestCategory.class })
	@Test
	public void testPost() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Get the contract	
    	ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
    	ContractDO contract = contracts[new Random().nextInt(contracts.length)];
		
    	// A dummy CCN
    	{
    		final ContractDetailDO contractDetailDO = new ContractDetailDO();
    		final ContractCCNDO ccnDO = new ContractCCNDO();
    		PeriodDO period = new PeriodDO("2002-01-01", "2012-01-01");
    		ccnDO.setPeriod(period);
    		ccnDO.setReference(String.format("TEST-REF-%s", new Date().getTime()/1000));
    		ccnDO.setValue(String.format("%s: %s", CreateTestCategory.TestID(), "DUMMY CCN VALUE"));
    		contractDetailDO.setCcn(ccnDO);
	    	contractDetailDO.setContract(contract);
	    	contractDetailDO.setGroup(new ValueDO(null, "CCN"));
	    	
	    	
	    	MvcResult result = TestUtil.postDetail(mockMvc, token, organisationId, contractDetailDO)
	    			.andExpect(status().isOk())
	    			.andReturn();
	    	String response = result.getResponse().getContentAsString();
	    	MessageDO message = new ObjectMapper().readValue(response, MessageDO.class);
	    	
	        Assert.assertEquals(MessageDO.OK, message.getType());
    	}
        
        
    	{
    		final ContractDetailDO contractDetailDO = new ContractDetailDO();
	    	contractDetailDO.setName(String.format("%s: %s", CreateTestCategory.TestID(), "DUMMY NAME"));
	    	contractDetailDO.setValue(new ContractValueDO("788777", "788000"));
	    	contractDetailDO.setContract(contract);
	    	contractDetailDO.setGroup(new ValueDO(null, "VALUE"));
	    	
	    	MvcResult result = TestUtil.postDetail(mockMvc, token, organisationId, contractDetailDO)
	    			.andExpect(status().isOk())
	    			.andReturn();
	    	String response = result.getResponse().getContentAsString();
	    	MessageDO message = new ObjectMapper().readValue(response, MessageDO.class);
	        
	        Assert.assertEquals(MessageDO.OK, message.getType());
    	}
	}
	
}
