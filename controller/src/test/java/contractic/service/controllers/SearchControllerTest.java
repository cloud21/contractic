package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.Module;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ModuleDO;
import contractic.service.domain.PersonDO;
import contractic.service.domain.SearchEntityDO;
import contractic.service.domain.SessionDO;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class SearchControllerTest {

	private MockMvc mockMvc;
	private MessageDO session;
	 
	@InjectMocks
    private PersonController personController;
	
	@InjectMocks
    private OrganisationController organisationController;
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}
	
	@Category({ ReadTestCategory.class })
	@Test
	public void testSearch() throws Exception {
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Search the group	
    	String query = "Ceramics";

		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/search", organisationId))
				.header("Authorization", token)
				.param("q", query)
				.param("collection", String.join(",", new String[]{Module.CONTRACT.toLowerCase(),
						Module.PERSON.toLowerCase()})));
		MvcResult result = personAction.andReturn();
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	SearchEntityDO[] entity = new ObjectMapper().readValue(response, SearchEntityDO[].class);
    	
    	//System.out.println(response);
    	Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
	}
	
	@Category({ ReadTestCategory.class })
	@Test
	@Ignore
	public void testSearchGroup() throws Exception {
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Search the group	

    	String moduleId = TestUtil.getModuleId(mockMvc, Module.CONTRACT, token);
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/search/group", organisationId))
				.param("q", "value")
				.param("autoComplete", "true")
				.param("moduleId", moduleId)
				.header("Authorization", token));
		MvcResult result = personAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	//System.out.println(response);
    	Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
	}

	@Category({ ReadTestCategory.class })
	@Test
	@Ignore
	public void testSearchTask() throws Exception {
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	String moduleId = TestUtil.getModuleId(mockMvc, Module.CONTRACT, token);
    	
    	// Search tasks
    	{
    		
			ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/search/task", organisationId))
					.param("q", "change notice")
					.param("autoComplete", "true")
					.param("moduleId", moduleId)
					.header("Authorization", token));
			MvcResult result = personAction.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	//System.out.println(response);
	    	Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
    	}

    	
	}
	
	@Category({ ReadTestCategory.class })
	@Test
	@Ignore
	public void testSearchSupplier() throws Exception{
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Search the person   	
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/search/supplier", organisationId))
				.param("q", "Prob")
				.param("autoComplete", "true")
				.header("Authorization", token));
		MvcResult result = personAction.andReturn();
		DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
    	
	}
	
	@Category({ ReadTestCategory.class })
	@Test
	@Ignore
	public void testSearchPerson() throws Exception{
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Search the person   	
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/search/person", organisationId))
				.param("q", "Mich")
				.param("autoComplete", "true")
				.header("Authorization", token));
		MvcResult result = personAction.andReturn();
		DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
    	
	}
	
	@Category({ ReadTestCategory.class })
	@Test
	@Ignore
	public void testSearchContact() throws Exception{
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

		
		PersonDO[] persons = TestUtil.getPersons(mockMvc, token, organisationId);
		PersonDO person = Arrays.stream(persons).filter(p -> p.getFirstName().startsWith("MICHAEL") 
				//&& p.getMiddleName().startsWith("MAWANDA") 
				&& p.getLastName().startsWith("SEKAMANYA")).collect(Collectors.toList()).get(0);
		ModuleDO module = TestUtil.getModule(mockMvc, token, Module.PERSON);
		
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/search/contact", organisationId))
				.header("Authorization", token)
				.param("q", "soccer")
				.param("autoComplete", "true")
				);
		MvcResult result = personAction.andReturn();
		DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
		String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
    	
	}
	
	@Category({ ReadTestCategory.class })
	@Test
	@Ignore
	public void testSearchAddress() throws Exception{
		// Search 
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		
		// Get the module Id first
		String typeId = null;
		// Get the address type's UUID
		{
    		ResultActions actions = mockMvc.perform(get("/addresstype")
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", token))
	        		.andExpect(status().isOk());
	    	
    		MvcResult result = actions.andReturn();
    		String response = result.getResponse().getContentAsString();
    		DocumentContext nDC = JsonPath.parse(response);
	        System.out.println(response);
	        List<String> res = nDC.read("$..[?(@.value =='Email')].id");
	        typeId = res.get(0);
		}
		
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/search/address", organisationId))
				.param("q", "soccer")
				.param("autoComplete", "true")
				.param("typeId", typeId)
				.header("Authorization", token));
		MvcResult result = personAction.andReturn();
		DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
		String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
    	
	}
	
}
