package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.GroupSet;
import contractic.model.data.store.Module;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.GroupDO;
import contractic.service.domain.GroupSetDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ModuleDO;
import contractic.service.domain.PersonDO;
import contractic.service.domain.SessionDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.DataSetupTestCategory;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;
import contractic.service.util.UpdateTestCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class PersonControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	@Category({ DataSetupTestCategory.class, ReadTestCategory.class })
	@Test
	public void testGet() throws Exception {
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	PersonDO[] persons = TestUtil.getPersons(mockMvc, token, organisationId);

    	Assert.assertTrue(persons.length >= 0);
    	
    	if(persons.length > 0){
    		PersonDO person = persons[new Random().nextInt(persons.length)];
			ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/person/%s", organisationId, person.getId()))
					.header("Authorization", token));
	    	MvcResult result = personAction.andReturn();	    	
	    	PersonDO person2 = new ObjectMapper().readValue(result.getResponse().getContentAsString(), PersonDO.class);
	    	
	        Assert.assertEquals(person.getId(), person2.getId());
    	}
	}

	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	PersonDO[] persons = TestUtil.getPersons(mockMvc, token, organisationId);
    	    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/person/%s", organisationId, persons[0].getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.INFO, type); 
	}
	
	@Category({ DataSetupTestCategory.class, UpdateTestCategory.class })
	@Test
	public void testPut() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	ObjectMapper mapper = new ObjectMapper();

    	// Fetch all persons
    	PersonDO[] persons = TestUtil.getPersons(mockMvc, token, organisationId);
    	
    	Assert.assertTrue(persons!=null && persons.length>=0);
	
    	if(persons.length > 0){
	    	// Get any random the person	
	    	final PersonDO person = persons[new Random().nextInt(persons.length)];
	    	
	    	// Update the person
	    	person.setActive(false);
	    	
	    	ModuleDO module = TestUtil.getModule(mockMvc, token, Module.PERSON);
	    	GroupSetDO groupSet = TestUtil.getGroupSet(mockMvc, token, organisationId, GroupSet.DEPARTMENT);
	    	GroupDO[] group = TestUtil.getGroup(mockMvc, token, module, groupSet, organisationId);
	    	
	    	List<GroupDO> departmentGroups = new ArrayList<>(Arrays.asList(person.getDepartment()));
	    	
	    	// Here we make sure we are adding the person to a department that they aren't already apart of
	    	List<GroupDO> groups = new ArrayList<>(Arrays.asList(group));
	    	groups.removeAll(departmentGroups);
	    	
	    	GroupDO departmentGroup = groups.stream().findAny().get();
	    	departmentGroups.add(departmentGroup);
	
	    	// Update the person
	    	person.setDepartment(departmentGroups.toArray(new GroupDO[0]));
	    	
	    	String content = mapper.writeValueAsString(person);
	    	
	        ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/person", organisationId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", token)
	        		.content(content))
	        		.andExpect(status().isOk());
	        MvcResult result = actions.andReturn();
	        MessageDO message = mapper.readValue(result.getResponse().getContentAsString(), MessageDO.class);
	
	        Assert.assertEquals(MessageDO.OK, message.getType()); 
	        
	        
	        persons = TestUtil.getPersons(mockMvc, token, organisationId);
	        final PersonDO person2 = Arrays.stream(persons).filter(p -> p.getId().equals(person.getId())).findFirst().get();
	        
	        Assert.assertEquals(person.getActive(), person2.getActive());
	        Assert.assertEquals(person.getDepartment().length, person2.getDepartment().length);
    	}
        
	}
	
	@Category({ DataSetupTestCategory.class, CreateTestCategory.class })
	@Test
	public void testPost() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		
    	String[] titles = {"MR", "MRS", "DR", "PROF", "MS", "MISS"};
    	
    	Random random = new Random();
    	String format = "%s: %s";
    	
    	// Make the person
    	final PersonDO person = new PersonDO();
    	person.setTitle(titles[random.nextInt(titles.length)]);
    	person.setFirstName(String.format(format, CreateTestCategory.TestID(), "FIRST NAME"));
    	person.setMiddleName(String.format(format, CreateTestCategory.TestID(), "MIDDLE NAME"));
    	person.setLastName(String.format(format, CreateTestCategory.TestID(), "LAST NAME"));
    	person.setJobTitle(String.format(format, CreateTestCategory.TestID(), "JOB TITLE"));
    	person.setActive(random.nextBoolean());
    	
    	// Add them to a department
    	ModuleDO module = TestUtil.getModule(mockMvc, token, Module.PERSON);
    	GroupSetDO groupSet = TestUtil.getGroupSet(mockMvc, token, organisationId, "DEPARTMENT");
    	GroupDO[] group = TestUtil.getGroup(mockMvc, token, module, groupSet, organisationId);
    	
    	List<GroupDO> departmentGroups = new ArrayList<>();
    	
    	// Pick any department to add the person to
    	departmentGroups.add(Arrays.stream(group).findAny().get());
   
    	person.setDepartment(departmentGroups.toArray(new GroupDO[0]));
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(person);

        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/person", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        MessageDO message = mapper.readValue(result.getResponse().getContentAsString(), MessageDO.class);
        
        Assert.assertNotNull(message);
        Assert.assertEquals(MessageDO.OK, message.getType());
        
        Map<?, ?> ref2 = (LinkedHashMap<?,?>)message.getRef();
        String personId = (String)ref2.get("id");
        Assert.assertNotNull(personId);
        
        PersonDO[] persons2 = TestUtil.getPersons(mockMvc, token, organisationId);
        PersonDO person2 = Arrays.stream(persons2).filter(p -> p.getId().equals(personId)).findFirst().get();
        
        Assert.assertEquals(person.getDepartment().length, person2.getDepartment().length);
	}
}
