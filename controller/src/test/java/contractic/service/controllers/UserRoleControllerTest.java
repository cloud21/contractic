package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.Module;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ModuleDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserRoleDO;
import contractic.service.domain.UserRolePermissionDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.TestUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class UserRoleControllerTest {

	private MockMvc mockMvc;
	private MessageDO session;
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	//@Ignore
	@Test
	public void testGet() throws Exception {
		
		// Get Organisation from which to get contracts
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername("contractic-admin@cloud21.net");
		sessionDO.setPassword("12345");
		// Get Organisation from which to get contracts
		MessageDO session = TestUtil.createSession(mockMvc, sessionDO, "homeoffice");
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	// Get the userrole
    	String contractId = null;
    	{
			ResultActions contractAction = mockMvc.perform(get(String.format("/organisation/%s/userrole", organisationId))
					.header("Authorization", token))
					.andExpect(status().isOk());
	    	MvcResult result = contractAction.andReturn();
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	DocumentContext nDC = JsonPath.parse(response);
	    	
	    	contractId = nDC.read("[1].id");
    	}
    	
		ResultActions contractAction = mockMvc.perform(get(String.format("/organisation/%s/userrole/%s", organisationId, contractId))
				.header("Authorization", token))
				.andExpect(status().isOk());
    	MvcResult result = contractAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
        Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
	}

	@Ignore
	@Test
	public void testDelete() throws Exception {

		// Get Organisation from which to get contracts
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername("Kim.Hillman@cloud21.net");
		sessionDO.setPassword("12345");
		// Get Organisation from which to get contracts
		MessageDO session = TestUtil.createSession(mockMvc, sessionDO, "croydonnhs");
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		
    	ObjectMapper mapper = new ObjectMapper();
    	
    	
		// Get the ContractPOJO
		UserRoleDO roleDO = null;
		{
	    	
	    	// Get the userrole
			ResultActions contractAction = mockMvc.perform(get(String.format("/organisation/%s/userrole", organisationId))
					.header("Authorization", token)
					);
	    	MvcResult result = contractAction.andReturn();
	        String response = result.getResponse().getContentAsString();
	        DocumentContext nDC = JsonPath.parse(response);
	        System.out.println(response);
	        
	        UserRoleDO[] roles = mapper.readValue(response, UserRoleDO[].class);
	        List<UserRoleDO> roleList = Arrays.stream(roles).filter(role -> role.getName().equals("Procurement Clerks")).collect(Collectors.toList());
	        
	        Assert.assertEquals(roleList.size(), 1);
	        roleDO = roleList.get(0);
		}
		
		final UserRoleDO userRoleDO = roleDO;

    	String content = mapper.writeValueAsString(userRoleDO);
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/userrole/%s", organisationId, userRoleDO.getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.OK, type); 
	}
	
	@Test
	public void testPut() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		
    	ObjectMapper mapper = new ObjectMapper();
    	
    	
		// Get the ContractPOJO
		UserRoleDO roleDO = null;
		{
	    	
	    	// Get the userrole
			ResultActions contractAction = mockMvc.perform(get(String.format("/organisation/%s/userrole", organisationId))
					.header("Authorization", token)
					);
	    	MvcResult result = contractAction.andReturn();
	        String response = result.getResponse().getContentAsString();
	        DocumentContext nDC = JsonPath.parse(response);
	        System.out.println(response);
	        
	        UserRoleDO[] roles = mapper.readValue(response, UserRoleDO[].class);
	        List<UserRoleDO> roleList = Arrays.stream(roles).filter(role -> role.getName().equals("ContractManager")).collect(Collectors.toList());
	        
	        Assert.assertEquals(roleList.size(), 1);
	        roleDO = roleList.get(0);
		}
		
		final UserRoleDO userRoleDO = roleDO;
		userRoleDO.setDescription("Contract Management");
		
		final String[] moduleNames = {Module.CONTRACT, Module.ORGANISATION, Module.PERSON, Module.SUPPLIER};
		final Map<String, boolean[]> permissionMap = new HashMap<>();
		permissionMap.put(moduleNames[0], new boolean[] {true, true, false});
		permissionMap.put(moduleNames[1], new boolean[] {true, false, false});
		permissionMap.put(moduleNames[2], new boolean[] {true, false, false});
		permissionMap.put(moduleNames[3], new boolean[] {true, true, false});
		
		ModuleDO[] modules = TestUtil.getModule(mockMvc, token);
		
		UserRolePermissionDO[] userPermissions = Arrays.stream(moduleNames).map(moduleName -> {
			UserRolePermissionDO permissionDO = new UserRolePermissionDO();
			try {
				List<ModuleDO> moduleList = Arrays.stream(modules).filter(module -> module.getName().equals(moduleName)).collect(Collectors.toList());
				ModuleDO moduleDO = moduleList.get(0);
				permissionDO.setModule(new ValueDO(moduleDO.getId(), moduleDO.getName()));
				boolean[] permissions = permissionMap.get(moduleName);
				permissionDO.setOrganisationId(organisationId);
				permissionDO.setRead(permissions[0]);
				permissionDO.setWrite(permissions[1]);
				permissionDO.setDelete(permissions[2]);
				permissionDO.setRole(new ValueDO(userRoleDO.getId(), userRoleDO.getName()));
				return permissionDO;
			} catch (Exception e) {
				return null;
			}
		})
		.filter(permission -> permission != null)
		.collect(Collectors.toList())
		.toArray(new UserRolePermissionDO[0]);
		
		userRoleDO.setPermission(userPermissions);
    	String content = mapper.writeValueAsString(userRoleDO);
    	
        ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/userrole", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        
        System.out.println(response);
        
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.OK, type); 
	}
	
	@Test
	public void testPost() throws Exception{
    	
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		
    	ObjectMapper mapper = new ObjectMapper();
    	
    	
		// Get the ContractPOJO
    	final UserRoleDO userRoleDO = new UserRoleDO();
    	userRoleDO.setActive(true);
    	userRoleDO.setName("Procedurement");
		userRoleDO.setDescription("Procurement Management");
		
		final String[] moduleNames = {Module.CONTRACT, Module.ORGANISATION, Module.PERSON, Module.SUPPLIER};
		final Map<String, boolean[]> permissionMap = new HashMap<>();
		permissionMap.put(moduleNames[0], new boolean[] {true, true, false});
		permissionMap.put(moduleNames[1], new boolean[] {true, false, false});
		permissionMap.put(moduleNames[2], new boolean[] {true, false, false});
		permissionMap.put(moduleNames[3], new boolean[] {true, true, false});
		
		ModuleDO[] modules = TestUtil.getModule(mockMvc, token);
		
		UserRolePermissionDO[] userPermissions = Arrays.stream(moduleNames).map(moduleName -> {
			UserRolePermissionDO permissionDO = new UserRolePermissionDO();
			try {
				List<ModuleDO> moduleList = Arrays.stream(modules).filter(module -> module.getName().equals(moduleName)).collect(Collectors.toList());
				ModuleDO moduleDO = moduleList.get(0);
				permissionDO.setModule(new ValueDO(moduleDO.getId(), moduleDO.getName()));
				boolean[] permissions = permissionMap.get(moduleName);
				permissionDO.setOrganisationId(organisationId);
				permissionDO.setRead(permissions[0]);
				permissionDO.setWrite(permissions[1]);
				permissionDO.setDelete(permissions[2]);
				return permissionDO;
			} catch (Exception e) {
				return null;
			}
		})
		.filter(permission -> permission != null)
		.collect(Collectors.toList())
		.toArray(new UserRolePermissionDO[0]);
		
		userRoleDO.setPermission(userPermissions);
    	String content = mapper.writeValueAsString(userRoleDO);
    	
        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/userrole", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        
        System.out.println(response);
        
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.OK, type); 
	}
}
