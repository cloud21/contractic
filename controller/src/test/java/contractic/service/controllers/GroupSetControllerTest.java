package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.GroupSet;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.GroupDO;
import contractic.service.domain.GroupSetDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class GroupSetControllerTest {

	private MockMvc mockMvc;
	private MessageDO session;
	 
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	@Category({ ReadTestCategory.class })
	@Test
	public void testGet() throws Exception {
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	// Get the group
    	GroupSetDO groupSet = null;
    	{
	    	GroupSetDO[] groupSets = TestUtil.getGroupSets(mockMvc, token, organisationId);
	    	groupSet = groupSets[0];
    	}
    	
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/groupset", organisationId))
				.header("Authorization", token)
				//.param("groupSetId", groupSet.getId())
				.param("name", "DEPARTMENT")
				);
    	MvcResult result = personAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	GroupSetDO[] groupSets = mapper.readValue(response, GroupSetDO[].class);
    	System.out.println(response);
    	
        Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
	}

	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		// Get the PersonPOJO
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header("Authorization", "token"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	// Get the group
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/group", orgId))
				.header("Authorization", "token"));
    	result = personAction.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        System.out.println(response);
		
    	String groupId = nDC.read("$[0].id");
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/group/%s", orgId, groupId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token"))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.OK, type); 
	}
	
	@Test
	public void testPut() throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
        
    	// Get the organisation and address types
		String orgId = null;
		// Get the organisation's UUID
		{
			ResultActions organisationAction = mockMvc.perform(get("/organisation"));
	    	MvcResult result = organisationAction.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	// Get the organisation's UUID
	    	orgId = nDC.read("$[0].id");
		}
    	
		// Get a single group
 		GroupDO statusDO = null;
 		{
			ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/group", orgId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", "token"))
	        		.andExpect(status().isOk());
	    	
			MvcResult result = actions.andReturn();
			String response = result.getResponse().getContentAsString();
			DocumentContext nDC = JsonPath.parse(response);
	        System.out.println(response);
	        
	        Map<String, Object> map = nDC.read("$[0]");
	      
	        statusDO = mapper.convertValue(map, GroupDO.class);
	        
 		}
        
 		statusDO.setActive(!statusDO.getActive());
 		
        // Update the group
        {
        	String content = mapper.writeValueAsString(statusDO);
			ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/status", orgId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", "token")
	        		.content(content))
	        		.andExpect(status().isOk());
	    	
			MvcResult result = actions.andReturn();
			String response = result.getResponse().getContentAsString();
			DocumentContext nDC = JsonPath.parse(response);
	        System.out.println(response);
	        
	        String type = nDC.read("$.type");
	        actions.andExpect(status().isOk());
	        
	        Assert.assertEquals(MessageDO.OK, type); 
        }
	}
	
	@Test
	public void testPost() throws Exception{
    	
		String organisationId = null;
		// Get the organisation's UUID
		{
			ResultActions organisationAction = mockMvc.perform(get("/organisation"));
	    	MvcResult result = organisationAction.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	// Get the organisation's UUID
	    	organisationId = nDC.read("$[0].id");
		}

		String moduleId = null;
		// Get the address type's UUID
		{
    		ResultActions addressTypeAction = mockMvc.perform(get("/module")
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", "token"))
	        		.andExpect(status().isOk());
	    	MvcResult result = addressTypeAction.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	// Get the organisation's UUID
	    	List<String> res = nDC.read("$..[?(@.name =='Contract')].id");
	    	moduleId = res.get(0);
		}
				
    	// Create a group
		final GroupDO groupDO = new GroupDO();
		groupDO.setName("VALUE");
		groupDO.setDescription("Used for contract value fields. Only numerics are expected in this field");
		groupDO.setModuleId(moduleId);
		groupDO.setOrganisationId(organisationId);
		groupDO.setActive(true);
   	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(groupDO);
    	
    	System.out.println(content);
    	
        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/group", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        
        System.out.println(response);
        
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
		
        Assert.assertEquals(MessageDO.OK, type); 
	}
}
