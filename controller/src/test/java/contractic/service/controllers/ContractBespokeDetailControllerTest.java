package contractic.service.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractDetailDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.TestUtil;

@RunWith(Parameterized.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class ContractBespokeDetailControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	String name;
	String description;
	String value;
	String group;
	String expected;
	Integer status;
	
	public  ContractBespokeDetailControllerTest(String name, String description, String value, String group, String expected, String status){
		this.name = name;
		this.description = description;
		this.value = value;
		this.group = group;
		this.expected = expected;
		this.status = new Integer(status);
	}
	
    @ClassRule public static final SpringClassRule SCR = new SpringClassRule();
    @Rule public final SpringMethodRule springMethodRule = new SpringMethodRule();
	
    @Parameterized.Parameters
    public static List<String[]> params() {
    	    	
        return Arrays.asList(new String[][] { 
        		{"NAME1", String.format("%s: %s", CreateTestCategory.TestID(), "DUMMY DESCRIPTION1"), 
        			String.format("%s: %s", CreateTestCategory.TestID(), "TEST"), null, MessageDO.OK, "200"},
        		{"NAME1", String.format("%s: %s", CreateTestCategory.TestID(), "DUMMY DESCRIPTION1"), 
        				String.format("%s: %s", CreateTestCategory.TestID(), "TEST"), null, MessageDO.ERROR, "400"},
        		/*{"NAME2", null, "TEST", null, MessageDO.OK, "200"},
        		{"NAME3", null, null, null, MessageDO.ERROR, "400"}*/
        		});
    }
    
	@Before
	public void setUp() throws Exception {
		if(setUpIsDone.get())
			return;

		synchronized(setUpIsDone){
	        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
			String user = System.getProperty("username");
			String password = System.getProperty("password");
			String orgCode = System.getProperty("orgCode");
			
			user = user==null ? "mawandm@yahoo.co.uk" : user;
			password = password==null ? "semica" : password;
			orgCode = orgCode==null ? "homeoffice" : orgCode;
			
			SessionDO sessionDO = new SessionDO();
			sessionDO.setUsername(user);
			sessionDO.setPassword(password);
			
			// Get Organisation from which to get contracts
			session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
			setUpIsDone.set(true);
		}
	}


	@Category({ CreateTestCategory.class })
	@Test
	public void testCreateBespokeDetail() throws Exception{
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Get the contract	
    	ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
    	ContractDO contract = contracts[0];
		
    	// A dummy detail
		final ContractDetailDO contractDetailDO = new ContractDetailDO();
		contractDetailDO.setName(name);
    	contractDetailDO.setDescription(description);
    	contractDetailDO.setText(value);
    	if(group!=null)
    		contractDetailDO.setGroup(new ValueDO(null, group));
    	contractDetailDO.setContract(contract);
    	
    	MvcResult result = TestUtil.postDetail(mockMvc, token, organisationId, contractDetailDO)
    			.andExpect(status().is(status))
    			.andReturn();
    	String response = result.getResponse().getContentAsString();
    	MessageDO message = new ObjectMapper().readValue(response, MessageDO.class);
    	
    	System.out.println(response);
    	
        Assert.assertEquals(expected, message.getType());
	}
}
