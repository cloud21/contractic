package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContactDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.PersonDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.SupplierDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.DataSetupTestCategory;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;
import contractic.service.util.UpdateTestCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class SupplierControllerTest {

	private MockMvc mockMvc;
	private MessageDO session;
	 
	@InjectMocks
    private PersonController personController;
	
	@InjectMocks
    private OrganisationController organisationController;
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	@Category({ DataSetupTestCategory.class, ReadTestCategory.class })
	@Test
	public void testGet() throws Exception {
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		
    	SupplierDO[] suppliers = TestUtil.getSuppliers(mockMvc, token, organisationId);
    	
    	// Assert that we got an array back
    	Assert.assertTrue(suppliers.length >= 0);

    	// Test gell a single supplier
    	if(suppliers.length > 0){
    		SupplierDO supplier = suppliers[new Random().nextInt(suppliers.length)];
			ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/supplier/%s", organisationId, supplier.getId()))
					.header("Authorization", token));
	    	MvcResult result = personAction.andReturn();	    	
	    	SupplierDO supplier2 = new ObjectMapper().readValue(result.getResponse().getContentAsString(), SupplierDO.class);
	    	
	        Assert.assertEquals(supplier.getId(), supplier2.getId());
    	}
	}

	@Category({ DataSetupTestCategory.class })
	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		// Get the PersonPOJO
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header("Authorization", "token"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	// Get the person
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/person", orgId))
				.header("Authorization", "token"));
    	result = personAction.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        System.out.println(response);
		
    	final PersonDO pp = new PersonDO();
    	pp.setId(nDC.read("$[0].id"));
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(pp);
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/person/%s", orgId, pp.getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.INFO, type); 
	}
	
	@Category({ DataSetupTestCategory.class, UpdateTestCategory.class })
	@Test
	public void testPut() throws Exception{
    	
    	ObjectMapper mapper = new ObjectMapper();
    	
		// Get the PersonPOJO
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		
    	SupplierDO[] suppliers = TestUtil.getSuppliers(mockMvc, token, organisationId);
    	SupplierDO supplier = suppliers[new Random().nextInt(suppliers.length)];
		
		supplier.setName(String.format("%s: %s", UpdateTestCategory.TestID, supplier.getName()));
    	
    	String content = mapper.writeValueAsString(supplier);
    	
        ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/supplier", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        MessageDO message = mapper.readValue(response, MessageDO.class);
        
        Assert.assertEquals(MessageDO.OK, message.getType());
    
        suppliers = TestUtil.getSuppliers(mockMvc, token, organisationId);
        SupplierDO supplier2 = Arrays.stream(suppliers).filter(s -> s.getId().equals(supplier.getId())).findFirst().get();
        Assert.assertEquals(supplier.getName(), supplier2.getName());
	
	}
	
	@Category({ DataSetupTestCategory.class, CreateTestCategory.class })
	@Test
	public void testPost() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

		String addressTypeId = null;
		// Get the address type's UUID
		{
    		ResultActions addressTypeAction = mockMvc.perform(get("/addresstype")
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", "token"))
	        		.andExpect(status().isOk());
	    	MvcResult result = addressTypeAction.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	// Get the organisation's UUID
	    	List<String> res = nDC.read("$..[?(@.value =='Email')].id");
	    	addressTypeId = res.get(0);
		}
				
    	// Get the person	
    	final SupplierDO supplier = new SupplierDO();
    	supplier.setName("BT Europe");
    	supplier.setOrganisationId(organisationId);
    	supplier.setReference("BT-REF");
   	
    	//String id,String contactType, String contact, String externalId, String moduleId, String organisationId
    	ContactDO[] contacts = {new ContactDO(null, addressTypeId, "steve.krug@bt.com", null, null, organisationId)};
    	supplier.setContact(contacts);
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(supplier);
    	
    	
    	System.out.println(content);
    	
        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/supplier", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        
        System.out.println(response);
        
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
		
        Assert.assertEquals(MessageDO.OK, type); 
	}
}
