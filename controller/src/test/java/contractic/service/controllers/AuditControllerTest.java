package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.Module;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class AuditControllerTest {

	private MockMvc mockMvc;
	private MessageDO session;
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	private String getUserId(String orgId, String token, String email) throws Exception{
 		String userId = null;
 		
		ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/user", orgId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token))
        		.andExpect(status().isOk());
    	
		MvcResult result = actions.andReturn();
		String response = result.getResponse().getContentAsString();
		DocumentContext nDC = JsonPath.parse(response);
        System.out.println(response);
        
    	List<String> res = nDC.read(String.format("$..[?(@.login =='%s')].id", email));
    	userId = res.get(0);
	
	
 		return userId;
	}
	
	private String getModuleId(String name, String token) throws Exception{
		String moduleId = null;
		// Get the module

		ResultActions moduleAction = mockMvc.perform(get("/module")
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token))
        		.andExpect(status().isOk());
    	MvcResult result = moduleAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	List<String> res = nDC.read(String.format("$..[?(@.name =='%s')].id", name));
    	moduleId = res.get(0);

    	return moduleId;
	}
	
	//@Ignore
	@Category({ ReadTestCategory.class })
	@Test
	public void testGet() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	String userId = getUserId(organisationId, token, "contractic-admin@cloud21.net");
    	String moduleId = this.getModuleId(Module.ORGANISATION, token);
		
		ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/audit", organisationId))
				.param("moduleId", moduleId)
				//.param("userId", userId)
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token))
        		.andExpect(status().isOk());
    	
		MvcResult result = actions.andReturn();
		String response = result.getResponse().getContentAsString();
		DocumentContext nDC = JsonPath.parse(response);
        //System.out.println(response);

        //Assert.assertEquals(type, MessageDO.INFO); 
	}
}
