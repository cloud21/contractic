package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.Module;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContractDO;
import contractic.service.domain.DocumentDO;
import contractic.service.domain.DocumentTypeDO;
import contractic.service.domain.FileDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ModuleDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;
import contractic.service.util.UpdateTestCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class DocumentControllerTest {

	private MockMvc mockMvc;
	private MessageDO session;
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	@Category({ ReadTestCategory.class })
	@Test
	public void testGet() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		
		ModuleDO moduleDO = TestUtil.getModule(mockMvc, token, Module.CONTRACT);
    	
    	// Get the contract
    	ContractDO contracts[] = TestUtil.getContracts(mockMvc, token, organisationId);
    	ContractDO contractDO = contracts[0];
		
		ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/document", organisationId))
				.param("moduleId", moduleDO.getId())
				.param("externalId", contractDO.getId())
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token))
        		.andExpect(status().isOk());
    	
		MvcResult result = actions.andReturn();
		String response = result.getResponse().getContentAsString();
		DocumentContext nDC = JsonPath.parse(response);
        System.out.println(response);

	}
	
	@Test
	public void testDelete() throws Exception {
		
		// Get the PersonPOJO
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	ModuleDO moduleDO = TestUtil.getModule(mockMvc, token, Module.CONTRACT);
    	DocumentDO[] documents = TestUtil.getDocuments(mockMvc, token, organisationId, null, null);
    	DocumentDO document = documents[0];
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/document/%s", organisationId, document.getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        Assert.assertEquals(MessageDO.OK, type);
        
        DocumentDO[] documents1 = TestUtil.getDocuments(mockMvc, token, organisationId, null, null);
        
        Assert.assertEquals(documents.length, documents1.length);
	}
	
	@Category({ UpdateTestCategory.class })
	//@Ignore
	@Test
	public void testPut() throws Exception{
    	
		// Login
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	//ModuleDO moduleDO = TestUtil.getModule(mockMvc, token, Module.CONTRACT);
		
    	//ContractDO contracts[] = TestUtil.getContracts(mockMvc, token, organisationId);
    	//ContractDO contractDO = contracts[0];
    	
    	//String fileId = TestUtil.uploadDocument(mockMvc, token, organisationId, "C:\\Users\\Michael Sekamanya\\Documents\\CTOProcurementContractsPortal.doc");
		
    	// Read documents
        DocumentDO[] documents = TestUtil.getDocuments(mockMvc, token, organisationId, null, null);
        // Update a random document
        DocumentDO document = documents[new Random().nextInt(documents.length)];
        
        // Update document fields
        document.setTitle(String.format("%s: %s", UpdateTestCategory.TestID, document.getTitle()));
		document.setDescription(String.format("%s: %s", UpdateTestCategory.TestID, document.getDescription()));
		
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(document);
    	
        ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/document", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        // Did we execute without any errors?
        Assert.assertEquals(MessageDO.OK, type);
        
        // Did we actually save
        DocumentDO[] documents2 = TestUtil.getDocuments(mockMvc, token, organisationId, null, null);
        DocumentDO document2 = Arrays.stream(documents2).filter(d -> d.getId().equals(document.getId())).findFirst().get();
        Assert.assertEquals(document2.getDescription(), document.getDescription());
	}
	
	@Test
	public void testPost() throws Exception{
    	
		// Get the organisation
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	ModuleDO moduleDO = TestUtil.getModule(mockMvc, token, Module.CONTRACT);
		DocumentTypeDO[] documentTypes = TestUtil.getDocumentTypes(mockMvc, token, organisationId);
		ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
		
		String fileId = TestUtil.uploadDocument(mockMvc, token, organisationId, "C:\\Users\\Michael Sekamanya\\Documents\\CTOProcurementContractsPortal.doc");
		
    	// Create the Document
		final DocumentDO documentDO = new DocumentDO();
		documentDO.setModule(new ValueDO(moduleDO.getId(), null));
		documentDO.setType(new ValueDO(documentTypes[0].getId(), null));
		documentDO.setExternal(new ValueDO(contracts[0].getId(), null));
		documentDO.setTitle("Test Document Title");
		documentDO.setDescription("Test Document Description");
		
		FileDO fileDO = new FileDO();
		fileDO.setId(fileId);
		documentDO.setFile(fileDO);
   	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(documentDO);
    	
    	System.out.println(content);
    	
        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/document", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        
        System.out.println(response);
        
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
		
        Assert.assertEquals(MessageDO.OK, type); 
	}
	
}
