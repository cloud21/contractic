package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.filter.SubdomainFilter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class SessionControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testGetParam() throws Exception {
		
	}

	@Ignore
	@Test
	public void testPasswordResetPost() throws Exception{
    	
		// Get the PersonPOJO
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername("michael.sekamanya@cloud21.net");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(sessionDO);
    	
        ResultActions actions = mockMvc.perform(post("/session/reset")
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.content(content))
        		.andExpect(status().isOk());
    	MvcResult result = actions.andReturn();
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	MessageDO message = mapper.readValue(response, MessageDO.class);
		
		Assert.assertEquals(message.getType(), MessageDO.OK);
		
		// We wait here for background thread so complete
		//Thread.sleep(2 * 60 * 60 * 1000);
		Thread.sleep(Long.MAX_VALUE);
	}

	@Ignore
	@Test
	public void testPasswordResetPut() throws Exception{
    	
		// Get the PersonPOJO
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername("mawandm@yahoo.co.uk");
		sessionDO.setPassword("semica");
		sessionDO.setPassword2(sessionDO.getPassword());
		sessionDO.setToken("f7528d9b10684ee2d633ee2a9c07fc04");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(sessionDO);
    	
        ResultActions actions = mockMvc.perform(put("/session/reset")
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.content(content))
        		.andExpect(status().isOk());
    	MvcResult result = actions.andReturn();
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	MessageDO message = mapper.readValue(response, MessageDO.class);
		
		Assert.assertEquals(message.getType(), MessageDO.OK);
	}
	
	@Ignore
	@Test
	public void testPost() throws Exception{
    	
		// Get the PersonPOJO
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername("michael.sekamanya@cloud21.net");
		sessionDO.setPassword("12345");
    	
		ResultActions actions = createSession(sessionDO, "imperialnhs")
				.andExpect(status().isOk());
		
    	MvcResult result = actions.andReturn();
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
		
		Assert.assertNotNull(response);
	}
	
	@Test
	@Ignore
	public void testPostException() throws Exception{
    	
		// Get the PersonPOJO
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername("michael.sekamanya@cloud21.net");
		sessionDO.setPassword("semica123123132132132132165674895132132135198");
    	
		ResultActions actions = createSession(sessionDO, "imperialnhs")
				.andExpect(status().isBadRequest());
		
    	MvcResult result = actions.andReturn();
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
		
    	Assert.assertNotNull(response);
	}
	
	@Ignore
	@Test
	public void testPut() throws Exception{
    	
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername("contractic-admin@cloud21.net");
		sessionDO.setPassword("password");
		
		ResultActions actions = createSession(sessionDO, "homeoffice");
		
    	MvcResult result = actions.andReturn();
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	ObjectMapper mapper = new ObjectMapper();
    	
    	MessageDO message = mapper.readValue(response, MessageDO.class);
		
		Assert.assertEquals(message.getType(), MessageDO.OK);
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)message.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	
    	sessionDO.setUsername(null);
    	sessionDO.setPassword2("12345");
    	
    	
    	String content = mapper.writeValueAsString(sessionDO);
    	// Get the person
    	
		actions = mockMvc.perform(put("/session")
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
    	
    	MvcResult result1 = actions.andReturn();
        response = result1.getResponse().getContentAsString();
        System.out.println(response);

	}
	
	private ResultActions createSession(SessionDO sessionDO, String subdomain) throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(sessionDO);
    	
        return mockMvc.perform(post("/session")
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.content(content)
        		.header(SubdomainFilter.SUBDOMAIN, subdomain));
	}
	
	@Test
	public void testSessionExist() throws Exception{
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername("mawandm@yahoo.co.uk");
		sessionDO.setPassword("semica");
		
		
		ResultActions actions = createSession(sessionDO, "homeoffice");
		
    	MvcResult result = actions.andReturn();
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	MessageDO message = new ObjectMapper().readValue(response, MessageDO.class);
		
		Assert.assertEquals(MessageDO.OK, message.getType());
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)message.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	sessionDO.setToken(token);
    	sessionDO.setKillExisting(Boolean.TRUE);
    	
    	actions = createSession(sessionDO, "homeoffice");
	}
	
	@Ignore
	@Test
	public void testDelete() throws Exception{
    	
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername("contractic-admin@cloud21.net");
		sessionDO.setPassword("12345");
    	
		MvcResult result = createSession(sessionDO, "homeoffice")
				.andReturn();
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	ObjectMapper mapper = new ObjectMapper();
    	
    	MessageDO message = mapper.readValue(response, MessageDO.class);
		
		Assert.assertEquals(message.getType(), MessageDO.OK);
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)message.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	
    	sessionDO = new SessionDO();
    	sessionDO.setToken(token);
    	sessionDO.setUsername("contractic-admin@cloud21.net");

    	String content = mapper.writeValueAsString(sessionDO);
    	// Get the person
    	
		ResultActions actions = mockMvc.perform(delete("/session")
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
    	
    	MvcResult result1 = actions.andReturn();
        response = result1.getResponse().getContentAsString();
        System.out.println(response);

        //Assert.assertEquals(type, MessageDO.INFO); 
	}
}
