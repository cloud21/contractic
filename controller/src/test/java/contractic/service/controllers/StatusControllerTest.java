package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.MessageDO;
import contractic.service.domain.PersonDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.StatusDO;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class StatusControllerTest {

	private MockMvc mockMvc;
	private MessageDO session;
	 
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	@Category({ ReadTestCategory.class })
	@Test
	public void testGet() throws Exception {
		
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	
		String moduleId = null;
		// Get the address type's UUID
		{
    		ResultActions moduleAction = mockMvc.perform(get("/module")
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", token))
	        		.andExpect(status().isOk());
	    	MvcResult result = moduleAction.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	// Get the organisation's UUID
	    	List<String> res = nDC.read("$..[?(@.name =='Contract')].id");
	    	moduleId = res.get(0);
		}
    	
    	// Get the status
    	String statusId = null;
    	{
			ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/status", organisationId))
					.header("Authorization", token)
					.param("moduleId", moduleId));
	    	MvcResult result = personAction.andReturn();
	    	String response = result.getResponse().getContentAsString();
	    	DocumentContext nDC = JsonPath.parse(response);
	    	System.out.println(response);
	    	
	    	statusId = nDC.read("[0].id");
    	}
    	
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/status", organisationId))
				.header("Authorization", token)
				.param("moduleId", moduleId)
				.param("statusId", statusId));
    	MvcResult result = personAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
        Assert.assertTrue("Expected a non empty string response but got an empty", !response.equals(""));
	}

	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		// Get the PersonPOJO
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header("Authorization", "token"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	// Get the person
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/person", orgId))
				.header("Authorization", "token"));
    	result = personAction.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        System.out.println(response);
		
    	final PersonDO pp = new PersonDO();
    	pp.setId(nDC.read("$[0].id"));
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(pp);
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/person/%s", orgId, pp.getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.OK, type); 
	}
	
	@Ignore
	@Test
	public void testPut() throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
        
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
		// Get a single status
 		StatusDO statusDO = null;
 		{
			ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/status", organisationId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", "token"))
	        		.andExpect(status().isOk());
	    	
			MvcResult result = actions.andReturn();
			String response = result.getResponse().getContentAsString();
			DocumentContext nDC = JsonPath.parse(response);
	        System.out.println(response);
	        
	        Map<String, Object> map = nDC.read("$[0]");
	      
	        statusDO = mapper.convertValue(map, StatusDO.class);
	        
 		}
        
 		statusDO.setActive(!statusDO.getActive());
 		
        // Update the status
        {
        	String content = mapper.writeValueAsString(statusDO);
			ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/status", organisationId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", "token")
	        		.content(content))
	        		.andExpect(status().isOk());
	    	
			MvcResult result = actions.andReturn();
			String response = result.getResponse().getContentAsString();
			DocumentContext nDC = JsonPath.parse(response);
	        System.out.println(response);
	        
	        String type = nDC.read("$.type");
	        actions.andExpect(status().isOk());
	        
	        Assert.assertEquals(MessageDO.OK, type); 
        }
	}
	
	@Ignore
	@Test
	public void testPost() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

		String moduleId = null;
		// Get the address type's UUID
		{
    		ResultActions addressTypeAction = mockMvc.perform(get("/module")
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", "token"))
	        		.andExpect(status().isOk());
	    	MvcResult result = addressTypeAction.andReturn();
	    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
	    	
	    	String response = result.getResponse().getContentAsString();
	    	System.out.println(response);
	    	
	    	// Get the organisation's UUID
	    	List<String> res = nDC.read("$..[?(@.name =='Contract')].id");
	    	moduleId = res.get(0);
		}
				
    	// Create a status
		final StatusDO statusDO = new StatusDO();
		statusDO.setName("In Progress");
		statusDO.setModuleId(moduleId);
		statusDO.setOrganisationId(organisationId);
		statusDO.setActive(Boolean.TRUE);
   	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(statusDO);
    	
    	System.out.println(content);
    	
        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/status", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        
        System.out.println(response);
        
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
		
        Assert.assertEquals(MessageDO.OK, type); 
	}
}
