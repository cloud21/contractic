package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContractTypeDO;
import contractic.service.domain.DocumentTypeDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.DataSetupTestCategory;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;
import contractic.service.util.UpdateTestCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class DocumentTypeControllerTest {
	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	 
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@Autowired
    private DocumentTypeController documentTypeController;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	@Category({ DataSetupTestCategory.class, ReadTestCategory.class })
	@Test
	public void testGet() throws Exception {
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	DocumentTypeDO[] documentTypes = TestUtil.getDocumentTypes(mockMvc, token, organisationId);
    	
    	// Assert that we received an array back
    	Assert.assertTrue(documentTypes!=null && documentTypes.length >= 0);
    	
    	if(documentTypes.length > 0){
			ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/documenttype/%s", organisationId, documentTypes[0].getId()))
					.header("Authorization", token))
					.andExpect(status().isOk());
	    	MvcResult result = personAction.andReturn();	    	
	    	String response = result.getResponse().getContentAsString();
	    	DocumentTypeDO documentType = new ObjectMapper().readValue(response, DocumentTypeDO.class);
	    	
	        Assert.assertNotNull(documentType);
    	}
	}

	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		// Get the PersonPOJO
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header("Authorization", "token"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	// Get the person
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/documenttype", orgId))
				.header("Authorization", "token"));
    	result = personAction.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        System.out.println(response);
		
        List<String> res = nDC.read("$..[?(@.code =='SLA')].id");
    	String documentTypeId = res.get(0);
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/documenttype/%s", orgId, documentTypeId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token"))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.OK, type); 
	}
	
	@Category({ DataSetupTestCategory.class, UpdateTestCategory.class })
	@Test
	public void testPut() throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
        
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
		// Get a single status
    	DocumentTypeDO[] documentTypes = TestUtil.getDocumentTypes(mockMvc, token, organisationId);
    	Assert.assertTrue(documentTypes!=null && documentTypes.length >= 0);
    	
    	if(documentTypes.length > 0){
	    	DocumentTypeDO documentTypeDO = documentTypes[new Random().nextInt(documentTypes.length)];
	    	
	    	String description = documentTypeDO.getDescription();
	    	Matcher matcher = UpdateTestCategory.TestIDPattern.matcher(description);
	    	if(matcher.matches()){
	    		description = matcher.group(2).trim();
	    	}
	    	
	    	documentTypeDO.setDescription(String.format("%s: %s", UpdateTestCategory.TestID, description));
	 		documentTypeDO.setActive(!documentTypeDO.getActive());
	 		
	        // Update the documenttype
        	String content = mapper.writeValueAsString(documentTypeDO);
			ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/documenttype", organisationId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", token)
	        		.content(content))
	        		.andExpect(status().isOk());
	    	
			MvcResult result = actions.andReturn();
			MessageDO message = mapper.readValue(result.getResponse().getContentAsString(), MessageDO.class);
	        
	        Assert.assertEquals(MessageDO.OK, message.getType());
	        
	        documentTypes = TestUtil.getDocumentTypes(mockMvc, token, organisationId);
	        DocumentTypeDO documentTypeDO2 = Arrays.stream(documentTypes).filter(dt -> dt.getId().equals(documentTypeDO.getId())).findFirst().get();
	        
	        Assert.assertEquals(documentTypeDO.getDescription(), documentTypeDO2.getDescription());
	        Assert.assertEquals(documentTypeDO.getActive(), documentTypeDO2.getActive());
	        
    	}
	}
	
	@Category({ DataSetupTestCategory.class, CreateTestCategory.class })
	@Test
	public void testPost() throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
        
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Create a status
		final DocumentTypeDO documentTypeDO = new DocumentTypeDO();
		documentTypeDO.setName(String.format("%s: %s", CreateTestCategory.TestID(), "TYPE NAME (POST)"));
		documentTypeDO.setDescription(String.format("%s: %s", CreateTestCategory.TestID(), "TYPE DESCRIPTION (POST)"));
		documentTypeDO.setActive(true);
		
    	String content = mapper.writeValueAsString(documentTypeDO);

        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/documenttype", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
		MessageDO message = mapper.readValue(response, MessageDO.class);
		
		Assert.assertEquals(MessageDO.OK, message.getType());
	}
}
