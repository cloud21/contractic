package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.MessageDO;
import contractic.service.domain.NoteTypeDO;
import contractic.service.domain.SessionDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.DataSetupTestCategory;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;
import contractic.service.util.UpdateTestCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class NoteTypeControllerTest {

	private MockMvc mockMvc;
	private MessageDO session;
	 
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	@Category({ DataSetupTestCategory.class, ReadTestCategory.class })
	@Test
	public void testGet() throws Exception {
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	// Get the note type
    	NoteTypeDO[] noteTypes = TestUtil.getNoteTypes(mockMvc, token, organisationId);
    	
    	// We confirm that we received an array
    	Assert.assertTrue(noteTypes.length >= 0);
    	
    	// If we have note types setup? We test that we can retrieve a single note type
    	if(noteTypes.length > 0){
    		String noteTypeId = noteTypes[new Random().nextInt(noteTypes.length)].getId();
			ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/notetype/%s", organisationId, noteTypeId))
					.header("Authorization", token))
					.andExpect(status().isOk());
	    	MvcResult result = personAction.andReturn();
	    	String response = result.getResponse().getContentAsString();
	    	NoteTypeDO noteType = new ObjectMapper().readValue(response, NoteTypeDO.class);
	    	
	        Assert.assertEquals(noteTypeId, noteType.getId());
    	}
	}

	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		// Get the PersonPOJO
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header("Authorization", "token"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	// Get the person
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/notetype", orgId))
				.header("Authorization", "token"));
    	result = personAction.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        System.out.println(response);
		
        List<String> res = nDC.read("$..[?(@.code =='SLA')].id");
    	String noteTypeId = res.get(0);
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/notetype/%s", orgId, noteTypeId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token"))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.OK, type); 
	}
	
	@Category({ DataSetupTestCategory.class, UpdateTestCategory.class })
	@Test
	public void testPut() throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
        
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
		// Get a single status
    	NoteTypeDO[] noteTypes = TestUtil.getNoteTypes(mockMvc, token, organisationId);
    	NoteTypeDO noteTypeDO = noteTypes[new Random().nextInt(noteTypes.length)];
        
    	noteTypeDO.setDescription(String.format("%s: %s", UpdateTestCategory.TestID, noteTypeDO.getDescription()));
 		noteTypeDO.setActive(!noteTypeDO.getActive());
 		
        // Update the notetype
        
    	String content = mapper.writeValueAsString(noteTypeDO);
		ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/notetype", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
    	
		MvcResult result = actions.andReturn();
		String response = result.getResponse().getContentAsString();
		MessageDO message = mapper.readValue(response, MessageDO.class);
        
		// Assert that the process was successful
        Assert.assertEquals(MessageDO.OK, message.getType()); 
        
        // Assert the update was successfull
        noteTypes = TestUtil.getNoteTypes(mockMvc, token, organisationId);
        NoteTypeDO noteTypeDO2 = Arrays.stream(noteTypes).filter(nt -> nt.getId().equals(noteTypeDO.getId())).findFirst().get();
        Assert.assertEquals(noteTypeDO2.getDescription(), noteTypeDO.getDescription());
        
	}
	
	@Category({ DataSetupTestCategory.class, CreateTestCategory.class })
	@Test
	public void testPost() throws Exception{
    	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Create a status
		final NoteTypeDO noteTypeDO = new NoteTypeDO();
		noteTypeDO.setName(String.format("%s: %s", CreateTestCategory.TestID(), "DUMMY NAME"));
		noteTypeDO.setDescription(String.format("%s: %s", CreateTestCategory.TestID(), "DUMMY DESCRIPTION"));
		noteTypeDO.setActive(true);
		noteTypeDO.setOrganisationId(organisationId);
		
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(noteTypeDO);
    	
        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/notetype", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        
        MessageDO message = mapper.readValue(response, MessageDO.class);
        Assert.assertEquals(MessageDO.OK, message.getType());
        
        Map<?, ?> ref2 = (LinkedHashMap<?,?>)message.getRef();
        Assert.assertNotNull(ref2.get("id"));
	}
}
