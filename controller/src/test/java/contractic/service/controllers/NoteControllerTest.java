package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.Module;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContractDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ModuleDO;
import contractic.service.domain.NoteDO;
import contractic.service.domain.NoteTypeDO;
import contractic.service.domain.PersonDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.SupplierDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;
import contractic.service.util.UpdateTestCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class NoteControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}
	
	@Category({ ReadTestCategory.class })
	@Test
	public void testGet() throws Exception{

		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	ModuleDO moduleDO = TestUtil.getModule(mockMvc, token, Module.CONTRACT);
    	
    	ContractDO contracts[] = TestUtil.getContracts(mockMvc, token, organisationId);
    	ContractDO contractDO = contracts[new Random().nextInt(contracts.length)];
		
    	NoteDO[] notes = TestUtil.getNotes(mockMvc, token, organisationId, moduleDO.getId(), contractDO.getId(), null);
    	
    	Assert.assertTrue(notes.length >= 0);
	}
	
	
	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		// Get the PersonPOJO
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header("Authorization", "token"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	// Get the person
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/person", orgId))
				.header("Authorization", "token"));
    	result = personAction.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        System.out.println(response);
		
    	final PersonDO pp = new PersonDO();
    	pp.setId(nDC.read("$[0].id"));
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(pp);
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/person/%s", orgId, pp.getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.INFO, type); 
	}
	
	@Category({ UpdateTestCategory.class })
	//@Ignore
	@Test
	public void testPut() throws Exception{
    	
		// Get the NotePOJO
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	ModuleDO moduleDO = TestUtil.getModule(mockMvc, token, Module.CONTRACT);
		
    	ObjectMapper mapper = new ObjectMapper();
    			
    	NoteDO noteDO = null;
    	
    	ContractDO contracts[] = TestUtil.getContracts(mockMvc, token, organisationId);
    	// Pick a random contract to be used as the externalId
    	ContractDO contractDO = contracts[new Random().nextInt(contracts.length)];
    	NoteDO[] notes = TestUtil.getNotes(mockMvc, token, organisationId, moduleDO.getId(), contractDO.getId(), null);
    	// We create a new note if we don't have one for this selected external Id
    	if(notes.length==0){
    		noteDO = new NoteDO();
    		NoteTypeDO[] noteTypes = TestUtil.getNoteTypes(mockMvc, token, organisationId);
    		noteDO.setExternalId(contractDO.getId());
    		noteDO.setModuleId(moduleDO.getId());
    		noteDO.setType(new ValueDO(noteTypes[new Random().nextInt(noteTypes.length)].getId()));
    		noteDO.setHeader("DUMMY NOTE HEADER");
    		noteDO.setDetail("DUMMY NOTE DETAIL");
    		MessageDO res = createNote(mockMvc, token, organisationId, noteDO);
    		Assert.assertEquals(MessageDO.OK, res.getType());
    		
    		Map<?, ?> noteRef = (LinkedHashMap<?,?>)res.getRef();
    		String noteId = (String)noteRef.get("id");
    		
    		noteDO.setId(noteId);
    	}else{
			// Pick a random note to update
			noteDO = notes[new Random().nextInt(notes.length)];
    	}
		
    	// Update the note header and detail
		noteDO.setHeader(String.format("%s: %s", UpdateTestCategory.TestID, noteDO.getHeader()));
		noteDO.setDetail(String.format("%s: %s", UpdateTestCategory.TestID, noteDO.getDetail()));
		
    	String content = mapper.writeValueAsString(noteDO);
        ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/note", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        //Make sure we got an OK
        Assert.assertEquals(MessageDO.OK, type);
        
        // Get the note
        NoteDO[] notes1 = TestUtil.getNotes(mockMvc, token, organisationId, moduleDO.getId(), contractDO.getId(), noteDO.getId());
        Assert.assertEquals(1, notes1.length);
        
        // Test for correctness
        Assert.assertEquals(notes1[0].getHeader(), noteDO.getHeader());
        Assert.assertEquals(notes1[0].getDetail(), noteDO.getDetail());
        
	}
	
	private MessageDO createNote(MockMvc mockMvc, String token, String organisationId, NoteDO noteDO) throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(noteDO);
        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/note", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        return mapper.readValue(response, MessageDO.class);
	}
	
	@Category({ CreateTestCategory.class })
	//@Ignore
	@Test
	public void testPost() throws Exception{
    	
		// Get the organisation
		// Get the NotePOJO
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
	
		NoteTypeDO[] noteTypes = TestUtil.getNoteTypes(mockMvc, token, organisationId);
		NoteTypeDO noteType = noteTypes[new Random().nextInt(noteTypes.length)];
		
		// Get the external Id (e.g. person, contract)
    	SupplierDO suppliers[] = TestUtil.getSuppliers(mockMvc, token, organisationId);
    	// Pick a random object to be used as the externalId
    	SupplierDO supplierDO = suppliers[new Random().nextInt(suppliers.length)];
    	
    	
    	String header = "Supplier's response to complaint";
    	String detail = "When i analyzed your code. I have also faced the same problem but my problem is if i give value for both first "
				+ "and last name means it is working fine. but when i give only one value means it says 400. "
				+ "anyway use the .andDo(print()) method to find out the error";
    	String module = Module.SUPPLIER;
		
    	// Create the Note
		final NoteDO noteDO = new NoteDO();
		ModuleDO moduleDO = TestUtil.getModule(mockMvc, token, module);
		noteDO.setModuleId(moduleDO.getId());
		noteDO.setType(new ValueDO(noteType.getId()));
		noteDO.setExternalId(supplierDO.getId());
		noteDO.setHeader(String.format("%s: %s", CreateTestCategory.TestID(), header));
		noteDO.setDetail(String.format("%s: %s", CreateTestCategory.TestID(), detail));
   	
		MessageDO message = createNote(mockMvc, token, organisationId, noteDO);
		
		// Assert that we executed successfully
        Assert.assertEquals(MessageDO.OK, message.getType());
        
        // Get the newly posted noteId
		Map<?, ?> noteRef = (LinkedHashMap<?,?>)message.getRef();
		String noteId = (String)noteRef.get("id");
		noteDO.setId(noteId);
		
		NoteDO[] notes = TestUtil.getNotes(mockMvc, token, organisationId, moduleDO.getId(), supplierDO.getId(), noteId);
		Assert.assertEquals(1, notes.length);
		
		// Assert the update was successfull
		Assert.assertEquals(notes[0].getHeader(), noteDO.getHeader());
		Assert.assertEquals(notes[0].getDetail(), noteDO.getDetail());
		
	}
	
}
