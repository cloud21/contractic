package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.MessageDO;
import contractic.service.domain.PersonDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class UserControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	@Category({ ReadTestCategory.class })
	@Test
	public void testGet() throws Exception{

		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
		// Get all users
 		String userId = null;
 		{
			ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/user", organisationId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", token))
	        		.andExpect(status().isOk());
	    	
			MvcResult result = actions.andReturn();
			String response = result.getResponse().getContentAsString();
			DocumentContext nDC = JsonPath.parse(response);
	        System.out.println(response);
	        
	        userId = nDC.read("$[0].id");
 		}
        
        // Get a single user
        {
			ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/user/%s", organisationId, userId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", token))
	        		.andExpect(status().isOk());
	    	
			MvcResult result = actions.andReturn();
			String response = result.getResponse().getContentAsString();
        }
        
        //Assert.assertEquals(type, MessageDO.INFO); 
	}
	
	@Ignore
	@Test
	public void testPut() throws Exception{
		
        ObjectMapper mapper = new ObjectMapper();
        
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	UserDO[] users = TestUtil.getUsers(mockMvc, token, organisationId);
    	
		// Get all users
 		UserDO userDO = users[new Random().nextInt(users.length)];
        
 		userDO.setActive(userDO.getActive());
 		
        // Get a single user
        {
        	String content = mapper.writeValueAsString(userDO);
			ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/user", organisationId))
	        		.contentType(MediaType.APPLICATION_JSON_VALUE)
	        		.header("Authorization", token)
	        		.content(content))
	        		.andExpect(status().isOk());
	    	
			MvcResult result = actions.andReturn();
			String response = result.getResponse().getContentAsString();
			DocumentContext nDC = JsonPath.parse(response);
	        System.out.println(response);
        }
        
	}
	
	private MessageDO updateUser(MockMvc mockMvc, String token, String organisationId, UserDO userDO) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(userDO);
		ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/user", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
    	
		MvcResult result = actions.andReturn();
		String response = result.getResponse().getContentAsString();
		return mapper.readValue(response,  MessageDO.class);
	}
	
	@Test
	public void updateUserProfile() throws Exception{
		ObjectMapper mapper = new ObjectMapper();


		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		
		SessionDO sessionDO = mapper.convertValue(ref, SessionDO.class);
		UserDO userDO = sessionDO.getUser();
		Map<String, Object> profile = userDO.getProfile();
		
		Map<String, Object> dashboardMap = new HashMap<String, Object>();
		profile.put("dashboard", dashboardMap);
		
		LinkedList<Map<String, Object>> panelList = new LinkedList<Map<String, Object>>();
		dashboardMap.put("panels", panelList);
		
		Map<String, Object> contractPanelMap = new HashMap<>();
		panelList.add(contractPanelMap);
		contractPanelMap.put("name", "Favourite Contracts");
		contractPanelMap.put("id", "contract");
		
		Map<String, Object> contractPanelOptions = new HashMap<>();
		
		contractPanelMap.put("options", contractPanelOptions);
		contractPanelOptions.put("count", 10);
		
		MessageDO messageDO = updateUser(mockMvc, token, organisationId, userDO);
		
		Assert.assertEquals(MessageDO.OK, messageDO.getType());
		
		System.out.println(mapper.writeValueAsString(profile));
		
	}
	
	@Ignore
	@Test
	public void testPost() throws Exception{

		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
		// Get the person we want to make the user
    	PersonDO[] persons1 = TestUtil.getPersons(mockMvc, token, organisationId);
    	List<PersonDO> persons = Arrays.stream(persons1).filter(p -> p.getActive()).collect(Collectors.toList());
    	PersonDO person = persons.get(new Random().nextInt(persons.size()));
    	final UserDO user = new UserDO();
    	user.setLogin("mawandams@yahoo.com");
    	user.setOrganisationId(organisationId);
    	user.setPassword("semica");
    	user.setActive(true);
    	user.setPersonId(person.getId());
   	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(user);
    	
    	System.out.println(content);
    	
        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/user", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
    	

		MvcResult result = actions.andReturn();
		String response = result.getResponse().getContentAsString();
		DocumentContext nDC = JsonPath.parse(response);
        System.out.println(response);

        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
		
        Assert.assertEquals(MessageDO.OK, type);
	}
	
	@Test
	public void testDelete() throws Exception{
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	UserDO[] users = TestUtil.getUsers(mockMvc, token, organisationId);
    	UserDO user = users[new Random().nextInt(users.length)];
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/user/%s", organisationId, user.getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token));
        
        actions.andExpect(status().isBadRequest());
        MvcResult result = actions.andReturn();
        String output = result.getResponse().getContentAsString();
        
        System.out.println(output);
        
        Assert.assertNotNull(output);
	}
}
