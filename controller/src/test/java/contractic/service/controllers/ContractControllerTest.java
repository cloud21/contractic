package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.ContractDetail;
import contractic.model.data.store.GroupSet;
import contractic.model.data.store.Module;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractDetailDO;
import contractic.service.domain.ContractTermDO;
import contractic.service.domain.ContractTermPeriodDO;
import contractic.service.domain.ContractTypeDO;
import contractic.service.domain.GroupDO;
import contractic.service.domain.GroupSetDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ModuleDO;
import contractic.service.domain.PeriodDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.StatusDO;
import contractic.service.domain.SupplierDO;
import contractic.service.domain.UserDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;
import contractic.service.util.UpdateTestCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class ContractControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}

	@Category({ ReadTestCategory.class })
	@Test
	public void testGet() throws Exception {
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	// Get the contract
    	ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
    	
    	// Assert that we got an array back
    	Assert.assertTrue(contracts.length > 0);

	}

	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		// Get the ContractPOJO
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header("Authorization", "token"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	// Get the contract
		ResultActions contractAction = mockMvc.perform(get(String.format("/organisation/%s/contract", orgId))
				.header("Authorization", "token"));
    	result = contractAction.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        System.out.println(response);
		
    	final ContractDO pp = new ContractDO();
    	pp.setId(nDC.read("$[0].id"));
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(pp);
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/contract/%s", orgId, pp.getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.INFO, type); 
	}
	
	@Category({ UpdateTestCategory.class })
	@Test
	public void testPut() throws Exception{
		
		ObjectMapper mapper = new ObjectMapper();

		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		String sessionString = mapper.writeValueAsString(ref);
		SessionDO sessionDO = mapper.readValue(sessionString, SessionDO.class);
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
    	
    	ContractDO contract = contracts[new Random().nextInt(contracts.length)];
    	
    	contract.setTitle(String.format("%s: %s", UpdateTestCategory.TestID, contract.getTitle()));
    	contract.setDescription(String.format("%s: %s", UpdateTestCategory.TestID, contract.getDescription()));
    	
    	// Get Departments
    	UserDO user = sessionDO.getUser();
    	GroupDO[] groups = user.getPerson().getDepartment();
    	GroupDO department = Arrays.stream(groups).filter(g -> g.getName().equals(contract.getDepartment().getValue())).findFirst().get();
    	contract.setDepartment(new ValueDO(department.getId()));
    	
    	GroupSetDO groupSet = TestUtil.getGroupSet(mockMvc, token, organisationId, GroupSet.PROCEDURE);
    	ModuleDO module = TestUtil.getModule(mockMvc, token, Module.CONTRACT);
    	GroupDO[] procedures = TestUtil.getGroup(mockMvc, token, null, groupSet, organisationId);
    	GroupDO procedure = procedures[new Random().nextInt(procedures.length)];
    	
    	contract.setProcedure(new ValueDO(procedure.getId()));
    	
    	ContractTermDO[] terms = contract.getTerm();
    	Arrays.stream(terms).forEach(term -> {
    		ContractDetailDO periodDetail = term.getPeriod();
    		if(periodDetail != null){
    			periodDetail.getTerm().getProperties().period.getCycle().value +=1;
    		}
    			
    		ContractDetailDO noticeDetail = term.getNotice();
    		if(noticeDetail != null){
    			noticeDetail.getTerm().getProperties().period.getCycle().value +=1;
    		}
    		
    		ContractDetailDO gotoMarketDetail = term.getGotomarket();
    		if(gotoMarketDetail != null){
    			gotoMarketDetail.getTerm().getProperties().period.getCycle().value +=1;
    		}
    	});
    	
    	//contract.setProcedure(new Value);
    	// Now update the contract
    	String content = mapper.writeValueAsString(contract);
    	
		ResultActions action = mockMvc.perform(put(String.format("/organisation/%s/contract", organisationId))
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.header("Authorization", token)
				.content(content))
				.andExpect(status().isOk());
    	MvcResult result = action.andReturn();
    	MessageDO message = mapper.readValue(result.getResponse().getContentAsString(), MessageDO.class);
        
        Assert.assertEquals(MessageDO.OK, message.getType());
        
        contracts = TestUtil.getContracts(mockMvc, token, organisationId);
        ContractDO contract2 = Arrays.stream(contracts).filter(c -> c.getId().equals(contract.getId())).findFirst().get();
        
        Assert.assertEquals(contract.getTitle(), contract2.getTitle());
        Assert.assertEquals(contract.getDescription(), contract2.getDescription());

	}
	
	private ContractDetailDO createTermPeriod(Integer number, PeriodDO.Cycle cycle, String periodType, Boolean active){
		final ContractDetailDO contractDetailDO = new ContractDetailDO();
		ContractTermPeriodDO termPeriod = new ContractTermPeriodDO();
		ContractTermPeriodDO.Properties properties = new ContractTermPeriodDO.Properties();
		
    	PeriodDO periodDO = new PeriodDO();
    	periodDO.setCycle(cycle);
		properties.period = periodDO;
		properties.active = active;
		termPeriod.setProperties(properties);
		termPeriod.setNumber(number);
    	contractDetailDO.setGroup(new ValueDO(null, periodType));
    	contractDetailDO.setTerm(termPeriod);
    	return contractDetailDO;
	}
	
	@Category({ CreateTestCategory.class })
	@Test
	public void testPost() throws Exception{
    	
		ObjectMapper mapper = new ObjectMapper();
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		String sessionString = mapper.writeValueAsString(ref);
		SessionDO sessionDO = mapper.readValue(sessionString, SessionDO.class);
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	UserDO[] users = TestUtil.getUsers(mockMvc, token, organisationId);
    	SupplierDO[] suppliers = TestUtil.getSuppliers(mockMvc, token, organisationId);
    	ModuleDO module = TestUtil.getModule(mockMvc, token, Module.CONTRACT);
    	
    	StatusDO[] statuses = TestUtil.getStatuses(mockMvc, token, organisationId, module.getId());
    	
    	ContractTypeDO[] contractTypes = TestUtil.getContractTypes(mockMvc, token, organisationId);
    	
    	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    	
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DAY_OF_YEAR, new Random().nextInt(365));
    	
    	// Get the contract	
    	final ContractDO contractDO = new ContractDO();
    	contractDO.setTitle(String.format("%s: %s", CreateTestCategory.TestID(), "DUMMY TITLE"));
    	contractDO.setDescription(String.format("%s: %s", CreateTestCategory.TestID(), "DUMMY DESCRIPTION"));
    	contractDO.setStartDate(dateFormat.format(calendar.getTime()));
    	
    	PeriodDO periodDO = new PeriodDO();
    	periodDO.setCycle(new PeriodDO.Cycle("Y", new Random().nextInt(10)));
    	contractDO.setOwner(new ValueDO(users[new Random().nextInt(users.length)].getPerson().getId()));
    	contractDO.setSupplier(new ValueDO(suppliers[new Random().nextInt(suppliers.length)].getId()));
    	contractDO.setStatus(new ValueDO(statuses[new Random().nextInt(statuses.length)].getId()));
    	contractDO.setType(new ValueDO(contractTypes[new Random().nextInt(contractTypes.length)].getId()));
    	//contractDO.setReference(String.format("REF/TEST-%s", new Random().nextInt(Short.MAX_VALUE)));
    	GroupDO[] departmentGroupDO = sessionDO.getUser().getPerson().getDepartment();
    	contractDO.setDepartment(new ValueDO(departmentGroupDO[new Random().nextInt(departmentGroupDO.length)].getId()));
    	
    	ContractTermDO term = new ContractTermDO();
    	Integer number = 1;
    	term.setNumber(number);
    	term.setPeriod(createTermPeriod(number, new PeriodDO.Cycle(PeriodDO.Cycle.YEAR, 5), ContractDetail.CONTRACT_TERM_PERIOD_GROUP_FIELD, Boolean.TRUE));
    	term.setNotice(createTermPeriod(number, new PeriodDO.Cycle(PeriodDO.Cycle.MONTH, 3), ContractDetail.CONTRACT_TERM_NOTICE_GROUP_FIELD, Boolean.TRUE));
    	term.setGotomarket(createTermPeriod(number, new PeriodDO.Cycle(PeriodDO.Cycle.MONTH, 6), ContractDetail.CONTRACT_TERM_GOTO_MARKET_GROUP_FIELD, Boolean.TRUE));

    	ContractTermDO term2 = new ContractTermDO();
    	Integer number2 = 2;
    	term2.setNumber(number2);
    	term2.setPeriod(createTermPeriod(number2, new PeriodDO.Cycle(PeriodDO.Cycle.YEAR, 2), ContractDetail.CONTRACT_TERM_PERIOD_GROUP_FIELD, Boolean.TRUE));
    	term2.setNotice(createTermPeriod(number2, new PeriodDO.Cycle(PeriodDO.Cycle.MONTH, 3), ContractDetail.CONTRACT_TERM_NOTICE_GROUP_FIELD, Boolean.TRUE));
    	term2.setGotomarket(createTermPeriod(number2, new PeriodDO.Cycle(PeriodDO.Cycle.MONTH, 6), ContractDetail.CONTRACT_TERM_GOTO_MARKET_GROUP_FIELD, Boolean.TRUE));
    	
    	contractDO.setTerm(new ContractTermDO[]{term, term2});
    	
    	String content = mapper.writeValueAsString(contractDO);
    	System.out.println(content);
        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/contract", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        MessageDO message = mapper.readValue(response, MessageDO.class);
        
        Assert.assertEquals(MessageDO.OK, message.getType()); 
	}
	
	
	@Category({ CreateTestCategory.class })
	@Test
	public void testPostDuplicateReferenceException() throws Exception{
    	
		ObjectMapper mapper = new ObjectMapper();
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		String sessionString = mapper.writeValueAsString(ref);
		SessionDO sessionDO = mapper.readValue(sessionString, SessionDO.class);
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
    	
    	UserDO[] users = TestUtil.getUsers(mockMvc, token, organisationId);
    	SupplierDO[] suppliers = TestUtil.getSuppliers(mockMvc, token, organisationId);
    	ModuleDO module = TestUtil.getModule(mockMvc, token, Module.CONTRACT);
    	
    	StatusDO[] statuses = TestUtil.getStatuses(mockMvc, token, organisationId, module.getId());
    	
    	ContractTypeDO[] contractTypes = TestUtil.getContractTypes(mockMvc, token, organisationId);
    	
    	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    	
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DAY_OF_YEAR, new Random().nextInt(365));
    	
    	// Create the contract	
    	final ContractDO contractDO = new ContractDO();
    	contractDO.setTitle(String.format("%s: %s", CreateTestCategory.TestID(), "DUMMY TITLE"));
    	contractDO.setDescription(String.format("%s: %s", CreateTestCategory.TestID(), "DUMMY DESCRIPTION"));
    	contractDO.setStartDate(dateFormat.format(calendar.getTime()));
    	
    	contractDO.setOwner(new ValueDO(users[new Random().nextInt(users.length)].getPerson().getId()));
    	contractDO.setSupplier(new ValueDO(suppliers[new Random().nextInt(suppliers.length)].getId()));
    	contractDO.setStatus(new ValueDO(statuses[new Random().nextInt(statuses.length)].getId()));
    	contractDO.setType(new ValueDO(contractTypes[new Random().nextInt(contractTypes.length)].getId()));
    	GroupDO[] departmentGroupDO = sessionDO.getUser().getPerson().getDepartment();
    	contractDO.setDepartment(new ValueDO(departmentGroupDO[new Random().nextInt(departmentGroupDO.length)].getId()));
    	
        ResultActions actions = TestUtil.postContract(mockMvc, organisationId, token, contractDO)
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();

        // Create another contract with the same reference number
        ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
        ContractDO contract = contracts[new Random().nextInt(contracts.length)];
        
        contractDO.setReference(contract.getReference());
        actions = TestUtil.postContract(mockMvc, organisationId, token, contractDO)
        		.andExpect(status().isBadRequest());
        result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        System.out.println(response);
        Assert.assertNotNull(response);
	}
	
}
