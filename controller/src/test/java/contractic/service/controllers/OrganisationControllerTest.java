package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.MessageDO;
import contractic.service.domain.OrganisationDO;
import contractic.service.filter.SubdomainFilter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class OrganisationControllerTest {

	private MockMvc mockMvc;
	 
	@InjectMocks
    private OrganisationController organisationController;
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testGet() throws Exception {
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header(SubdomainFilter.SUBDOMAIN, "gosh"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	String value = nDC.read("$[0].id");
        
        Assert.assertFalse("Expected a non empty string response but got an empty", value.equals(response));
	}

	@Ignore
	@Test
	public void testGetParam() throws Exception {
		
		// Get all first and select the first
		ResultActions organisationAction = mockMvc.perform(get("/organisation"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String id = nDC.read("$[0].id");
		
		organisationAction = mockMvc.perform(get("/organisation/" + id));
    	result = organisationAction.andReturn();
    	nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	String value = nDC.read("$[0].id");
        
        Assert.assertFalse("Expected a non empty string response but got an empty", value.equals(response));
	}
	
	@Ignore
	@Test
	public void testPost() throws Exception{
    	
    	final OrganisationDO op = new OrganisationDO();
    	op.setName("CTO People");
    	op.setActive("true");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(op);
    	
        ResultActions actions = mockMvc.perform(post("/organisation").contentType(MediaType.APPLICATION_JSON_VALUE).content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        String response = result.getResponse().getContentAsString();
        DocumentContext nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        //actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.INFO, type); 
	}
	
	//@Ignore
	@Test
	public void testPut() throws Exception{
    	
		ResultActions organisationAction = mockMvc.perform(get("/organisation"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	String response = result.getResponse().getContentAsString();
    	
    	System.out.println(response);
    	
    	List<Map<String, Object>> res = nDC.read("$..[?(@.code =='croydonnhs')]");
		
    	ObjectMapper mapper = new ObjectMapper();
    	
    	String content = mapper.writeValueAsString(res.get(0));
    	
    	final OrganisationDO op = mapper.readValue(content, OrganisationDO.class);
    	
    	Map<String, Object> profileMap = op.getProfile();
    	profileMap.put(OrganisationDO.PROFILE_KEY_APPLOGO, "logo-croydon-health-services.png");
    	op.setProfile(profileMap);
    	    	
    	content = mapper.writeValueAsString(op);
    	
        ResultActions actions = mockMvc.perform(put("/organisation").contentType(MediaType.APPLICATION_JSON_VALUE).content(content))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        
        Assert.assertEquals(MessageDO.OK, type); 
	}
}
