package contractic.service.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.model.data.store.Module;
import contractic.service.ServiceContextConfig;
import contractic.service.WebAppContextConfig;
import contractic.service.domain.ContractDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.PersonDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.TaskDO;
import contractic.service.domain.UserDO;
import contractic.service.domain.ValueDO;
import contractic.service.util.CreateTestCategory;
import contractic.service.util.ReadTestCategory;
import contractic.service.util.TestUtil;
import contractic.service.util.UpdateTestCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContextConfig.class, ServiceContextConfig.class})
@WebAppConfiguration
public class TaskControllerTest {

	private static MockMvc mockMvc;
	private static MessageDO session;
	private static AtomicBoolean setUpIsDone = new AtomicBoolean(false);
	
	@Autowired
    protected WebApplicationContext webApplicationContext;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		String user = System.getProperty("username");
		String password = System.getProperty("password");
		String orgCode = System.getProperty("orgCode");
		
		user = user==null ? "mawandm@yahoo.co.uk" : user;
		password = password==null ? "semica" : password;
		orgCode = orgCode==null ? "homeoffice" : orgCode;
		
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername(user);
		sessionDO.setPassword(password);
		
		// Get Organisation from which to get contracts
		session = TestUtil.createSession(mockMvc, sessionDO, orgCode);
	
	}

	@After
	public void tearDown() throws Exception {
		SessionDO sessionDO = new ObjectMapper().convertValue(session.getRef(), SessionDO.class);    	
    	TestUtil.deleteSession(mockMvc, sessionDO);
	}
	
	@Category({ ReadTestCategory.class })
	@Test
	public void testGet() throws Exception {
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	// Get all tasks
	    TaskDO[] tasks = TestUtil.getTasks(mockMvc, token, organisationId);
	    
	    // Assert that we get an array back
	    Assert.assertTrue(tasks.length >= 0);	    
	    ObjectMapper mapper = new ObjectMapper();
	    
    	// Get a specific task
    	{
    		TaskDO task = Arrays.stream(tasks).findAny().get();
			ResultActions taskAction = mockMvc.perform(get(String.format("/organisation/%s/task", organisationId))
					.header("Authorization", token)
					.param("id", task.getId()))
					.andExpect(status().isOk());
			MvcResult result = taskAction.andReturn();   	
			String response = result.getResponse().getContentAsString();
			TaskDO[] task2 = mapper.readValue(response, TaskDO[].class);
	    	Assert.assertEquals(1, task2.length);
	    	Assert.assertEquals(task.getId(), task2[0].getId());
    	}
    	
    	// Get all tasks for the given parent
		Optional<TaskDO> parentTaskOptional = Arrays.stream(tasks).filter(t -> t.getChildren()).findAny();
		if(parentTaskOptional.isPresent()){
			ResultActions taskAction = mockMvc.perform(get(String.format("/organisation/%s/task", organisationId))
					.header("Authorization", token)
					.param("parentNumber", parentTaskOptional.get().getNumber()))
					.andExpect(status().isOk());
			MvcResult result = taskAction.andReturn();	    	
			String response = result.getResponse().getContentAsString();
			tasks = mapper.readValue(response, TaskDO[].class);
			Assert.assertTrue(tasks.length >= 0);
		}
	}

	@Ignore
	@Test
	public void testDelete() throws Exception {
		
		// Get the TaskPOJO
		ResultActions organisationAction = mockMvc.perform(get("/organisation")
				.header("Authorization", "token"));
    	MvcResult result = organisationAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	System.out.println(response);
    	
    	// Get the organisation's UUID
    	String orgId = nDC.read("$[0].id");
    	
    	// Get the task
		ResultActions taskAction = mockMvc.perform(get(String.format("/organisation/%s/task", orgId))
				.header("Authorization", "token"));
    	result = taskAction.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        System.out.println(response);
		
    	final TaskDO pp = new TaskDO();
    	pp.setId(nDC.read("$[0].id"));
    	
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(pp);
    	
        ResultActions actions = mockMvc.perform(delete(String.format("/organisation/%s/task/%s", orgId, pp.getId()))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", "token")
        		.content(content))
        		.andExpect(status().isOk());
        result = actions.andReturn();
        response = result.getResponse().getContentAsString();
        nDC = JsonPath.parse(response);
        String type = nDC.read("$.type");
        actions.andExpect(status().isOk());
        
        Assert.assertEquals(MessageDO.INFO, type); 
	}
	
	@Category({ UpdateTestCategory.class })
	@Test
	public void testPut() throws Exception{
    	
		// Get the TaskPOJO	
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");
		
    	Random random = new Random();
    	
    	TaskDO[] tasks = TestUtil.getTasks(mockMvc, token, organisationId);
    	TaskDO task = tasks[random.nextInt(tasks.length)];
    	
    	UserDO[] users = TestUtil.getUsers(mockMvc, token, organisationId);
    	UserDO user = Arrays.stream(users).filter(u -> !u.getPerson().getId().equals(task.getPerson().getId())).findAny().get();
    	
    	ObjectMapper mapper = new ObjectMapper();
		
    	/*
    	ValueDO repeatDO = new ValueDO("W", "6");
    	task.setRepeatStart("01/01/2016");
    	task.setRepeatEnd("31/12/2016");
    	task.setRepeat(repeatDO);
    	*/
    	
    	String title = task.getTitle();
    	Matcher matcher = UpdateTestCategory.TestIDPattern.matcher(title);
    	if(matcher.matches()){
    		title = matcher.group(2).trim();
    	}
    	task.setTitle(String.format("%s: %s", UpdateTestCategory.TestID, title));
    	
    	String description = task.getDescription();
    	if(description!=null){
	    	matcher = UpdateTestCategory.TestIDPattern.matcher(description);
	    	if(matcher.matches()){
	    		description = matcher.group(2).trim();
	    	}
	    	task.setDescription(String.format("%s: %s", UpdateTestCategory.TestID, description));
    	}
    	
    	task.setPerson(new ValueDO(user.getPerson().getId()));
		
    	String content = mapper.writeValueAsString(task);
    	
        ResultActions actions = mockMvc.perform(put(String.format("/organisation/%s/task", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        MessageDO message = mapper.readValue(result.getResponse().getContentAsString(), MessageDO.class);
        
        // Assert that we processed successfully
        Assert.assertEquals(MessageDO.OK, message.getType());
        
        tasks = TestUtil.getTasks(mockMvc, token, organisationId);
        TaskDO task2 = Arrays.stream(tasks).filter(t -> t.getId().equals(task.getId())).findFirst().get();
        Assert.assertEquals(task2.getTitle(), task.getTitle());
        Assert.assertEquals(task2.getDescription(), task.getDescription());
        Assert.assertEquals(task2.getPerson().getId(), task.getPerson().getId());
	}
	
	@Category({ CreateTestCategory.class })
	@Test
	public void testPost() throws Exception{
    	
		// The organisation to work under
		
		Map<?, ?> ref = (LinkedHashMap<?,?>)session.getRef();
		
    	// Get the organisation's UUID
    	String token = (String)ref.get("token");
    	String organisationId = (String)ref.get("organisationId");

    	Random random = new Random();
    	
		// The owner of the task
    	UserDO[] users = TestUtil.getUsers(mockMvc, token, organisationId);
    	UserDO user = users[random.nextInt(users.length)];
		
		// We need to get all statuses applicable to the Task module
		String moduleId = TestUtil.getModuleId(mockMvc, Module.TASK, token);
		
		// Status
    	ValueDO status = getModuleStatus(organisationId, moduleId, token);
		
    	// Get the module whose object we are creating a task for.
		String taskModuleId = TestUtil.getModuleId(mockMvc, Module.CONTRACT, token);
		
		ContractDO[] contracts = TestUtil.getContracts(mockMvc, token, organisationId);
		
		ObjectMapper mapper = new ObjectMapper();
    	
    	// Get task that may optionally be the parent for the new task
		TaskDO[] tasks = TestUtil.getTasks(mockMvc, token, organisationId);
    	
    	// Get the task	
    	final TaskDO taskDO = new TaskDO();
    	String format = "%s: %s";
    	taskDO.setTitle(String.format(format, CreateTestCategory.TestID(), "DUMMY TITLE"));
    	taskDO.setDescription(String.format(format, CreateTestCategory.TestID(), "DUMMY DESCRIPTION"));
    	taskDO.setStartDate("11/12/2010");
    	taskDO.setEndDate("10/12/2015");
    	taskDO.setPerson(new ValueDO(user.getPerson().getId()));
    	taskDO.setStatus(status);
    	taskDO.setModule(new ValueDO(taskModuleId));
    	Map<String, Object> external = mapper.convertValue(contracts[random.nextInt(contracts.length)], Map.class);
    	taskDO.setExternal(external);
    	try{
    		taskDO.setParentNumber(tasks[random.nextInt(tasks.length)+1].getNumber());
    	}catch(IndexOutOfBoundsException iob){
    		/*Ignore in this case, the new task will not have a parent*/
    	}
    	
    	String content = mapper.writeValueAsString(taskDO);

        ResultActions actions = mockMvc.perform(post(String.format("/organisation/%s/task", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content))
        		.andExpect(status().isOk());
        MvcResult result = actions.andReturn();
        MessageDO message = mapper.readValue(result.getResponse().getContentAsString(), MessageDO.class);
        Assert.assertEquals(MessageDO.OK, message.getType()); 
	}



	private ValueDO getModuleStatus(String orgId, String moduleId, String token)
			throws Exception, UnsupportedEncodingException {
		// Get the status
    	ValueDO status = null;
    	{
			ResultActions taskAction = mockMvc.perform(get(String.format("/organisation/%s/status", orgId))
					.header("Authorization", token)
					.param("moduleId", moduleId));
			MvcResult result = taskAction.andReturn();
	    	String response = result.getResponse().getContentAsString();
	    	DocumentContext nDC = JsonPath.parse(response);
	    	System.out.println(response);
	    	
	    	status = new ValueDO(nDC.read("$[0].id"), nDC.read("$[0].name"));
    	}
		return status;
	}
}
