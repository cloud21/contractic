package contractic.service.util;

import java.util.regex.Pattern;


/**
 * Marker interface for test that only update the system
 * @author Michael Sekamanya
 *
 */
public interface UpdateTestCategory extends CrudTestCategory{
	public static final String TestID = CrudTestCategory.TestID(UpdateTestCategory.class);
	public final static Pattern TestIDPattern = Pattern.compile(String.format("(%s@.*:)+(.*)", UpdateTestCategory.class.getSimpleName()));
}
