package contractic.service.util;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import contractic.service.domain.AddressDO;
import contractic.service.domain.AutoCompleteDO;
import contractic.service.domain.ContactDO;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractDetailDO;
import contractic.service.domain.ContractReferenceFormatDO;
import contractic.service.domain.ContractTypeDO;
import contractic.service.domain.CountryDO;
import contractic.service.domain.DocumentDO;
import contractic.service.domain.DocumentTypeDO;
import contractic.service.domain.GroupDO;
import contractic.service.domain.GroupSetDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ModuleDO;
import contractic.service.domain.NoteDO;
import contractic.service.domain.NoteTypeDO;
import contractic.service.domain.PersonDO;
import contractic.service.domain.ReportDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.StaticDO;
import contractic.service.domain.StatusDO;
import contractic.service.domain.SupplierDO;
import contractic.service.domain.TaskDO;
import contractic.service.domain.UserDO;
import contractic.service.domain.ValueDO;
import contractic.service.filter.SubdomainFilter;

public class TestUtil {
	public static MessageDO createSession(MockMvc mockMvc, SessionDO sessionDO, String subdomain) throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(sessionDO);
    	
        ResultActions actions = mockMvc.perform(post("/session")
        		.header(SubdomainFilter.SUBDOMAIN, subdomain)
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.content(content))
        		.andExpect(status().isOk());
    	MvcResult result = actions.andReturn();
    	String response = result.getResponse().getContentAsString();
    	return mapper.readValue(response, MessageDO.class);
	}
	
	public static MessageDO deleteSession(MockMvc mockMvc, SessionDO sessionDO) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		String content = mapper.writeValueAsString(sessionDO);
		ResultActions actions = mockMvc.perform(delete("/session")
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", sessionDO.getToken())
        		.content(content))
        		.andExpect(status().isOk());
    	MvcResult result = actions.andReturn();
    	String response = result.getResponse().getContentAsString();
    	return mapper.readValue(response, MessageDO.class);
	}
	
	
	public static String getModuleId(MockMvc mockMvc, String moduleName, String token) throws Exception,
	UnsupportedEncodingException {
		String taskModuleId = null;
		// Get the module whose object this task related. In this case, we create a task on a specific contract
		
			ResultActions moduleAction = mockMvc.perform(get("/module")
		    		.contentType(MediaType.APPLICATION_JSON_VALUE)
		    		.header("Authorization", token))
		    		.andExpect(status().isOk());
			MvcResult result = moduleAction.andReturn();
			DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
			
			String response = result.getResponse().getContentAsString();
			
			// Get the organisation's UUID
			List<String> res = nDC.read(String.format("$..[?(@.name =='%s')].id", moduleName));
			taskModuleId = res.get(0);
		
		return taskModuleId;
	}
	
	public static ModuleDO[] getModule(MockMvc mockMvc, String token) throws Exception{
		// Get the module whose object this task related. In this case, we create a task on a specific contract
		ObjectMapper mapper = new ObjectMapper();
		ResultActions moduleAction = mockMvc.perform(get("/module")
	    		.contentType(MediaType.APPLICATION_JSON_VALUE)
	    		.header("Authorization", token))
	    		.andExpect(status().isOk());
		MvcResult result = moduleAction.andReturn();
		DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
		
		String response = result.getResponse().getContentAsString();
		
		// Get the organisation's UUID
        return mapper.readValue(response, ModuleDO[].class);
	}
	
	public static CountryDO[] getCountry(MockMvc mockMvc, String token) throws Exception{
		// Get the country whose object this task related. In this case, we create a task on a specific contract
		ObjectMapper mapper = new ObjectMapper();
		ResultActions countryAction = mockMvc.perform(get("/country")
	    		.contentType(MediaType.APPLICATION_JSON_VALUE)
	    		.header("Authorization", token))
	    		.andExpect(status().isOk());
		MvcResult result = countryAction.andReturn();
		String response = result.getResponse().getContentAsString();
        return mapper.readValue(response, CountryDO[].class);
	}
	
	public static ValueDO[] getCurrency(MockMvc mockMvc, String token, Boolean active) throws Exception{
		// Get the country whose object this task related. In this case, we create a task on a specific contract
		ObjectMapper mapper = new ObjectMapper();
		ResultActions countryAction = mockMvc.perform(get("/currency")
	    		.contentType(MediaType.APPLICATION_JSON_VALUE)
	    		.header("Authorization", token)
	    		.param("active", active!=null ? active.toString() : null))
	    		.andExpect(status().isOk());
		MvcResult result = countryAction.andReturn();
		String response = result.getResponse().getContentAsString();
        return mapper.readValue(response, ValueDO[].class);
	}
	
	public static ModuleDO getModule(MockMvc mockMvc, String token, String name) throws Exception{
		// Get the module whose object this task related. In this case, we create a task on a specific contract
		ModuleDO[] modules = getModule(mockMvc, token);
    	List<ModuleDO> moduleList = Arrays.stream(modules).filter(module -> module.getName().equals(name)).collect(Collectors.toList());
    	return moduleList.size() == 1? moduleList.get(0) : null;
	}
	
	public static ContractDO[] getContracts(MockMvc mockMvc, String token, String organisationId) throws Exception{

		ResultActions contractAction = mockMvc.perform(get(String.format("/organisation/%s/contract", organisationId))
				.header("Authorization", token))
				.andExpect(status().isOk());
    	MvcResult result = contractAction.andReturn();
    	String response = result.getResponse().getContentAsString();
    	DocumentContext nDC = JsonPath.parse(response);
    	
    	ObjectMapper mapper = new ObjectMapper();
    	
    	return mapper.readValue(response, ContractDO[].class);
	
	}
	
	public static GroupSetDO[] getGroupSets(MockMvc mockMvc, String token, String organisationId) throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
    	// Get the group
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/groupset", organisationId))
				.header("Authorization", token)
				);
    	MvcResult result = personAction.andReturn();
    	String response = result.getResponse().getContentAsString();
    	//DocumentContext nDC = JsonPath.parse(response);
    	return mapper.readValue(response, GroupSetDO[].class);
	}
	
	public static GroupSetDO getGroupSet(MockMvc mockMvc, String token, String organisationId, String name) throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
    	// Get the group
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/groupset", organisationId))
				.header("Authorization", token)
				.param("name", name)
				);
    	MvcResult result = personAction.andReturn();
    	String response = result.getResponse().getContentAsString();
    	//DocumentContext nDC = JsonPath.parse(response);
    	GroupSetDO[] groupSets = mapper.readValue(response, GroupSetDO[].class);
    	if(groupSets !=null && groupSets.length>1)
    		throw new Exception(String.format("Found more than on group set matching %s", name));
    	return groupSets !=null && groupSets.length == 1? groupSets[0] : null;
	}
	
	public static GroupDO[] getGroup(MockMvc mockMvc, String token, ModuleDO module, GroupSetDO groupSet, String organisationId) throws Exception{
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/group", organisationId))
				.header("Authorization", token)
				.param("moduleId", module==null ? null : module.getId())
				.param("groupSetId", groupSet.getId()));
    	MvcResult result = personAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	ObjectMapper mapper = new ObjectMapper();
    	return mapper.readValue(response, GroupDO[].class);
	}
	
	public static AddressDO[] getAddress(MockMvc mockMvc, String token, String organisationId) throws Exception{
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/address", organisationId))
				.header("Authorization", token));
    	MvcResult result = personAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	
    	ObjectMapper mapper = new ObjectMapper();
    	return mapper.readValue(response, AddressDO[].class);
	}
	
	public static StaticDO[] getAddressType(MockMvc mockMvc, String token, String organisationid) throws Exception{

		ResultActions actions = mockMvc.perform(get("/addresstype")
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token))
        		.andExpect(status().isOk());
    	
		MvcResult result = actions.andReturn();
		String response = result.getResponse().getContentAsString();
		DocumentContext nDC = JsonPath.parse(response);
        
    	ObjectMapper mapper = new ObjectMapper();
    	return mapper.readValue(response, StaticDO[].class);

	}
	
	public static ContactDO[] getContacts(MockMvc mockMvc, String token, String organisationId,
			final String externalId, ModuleDO module)
			throws Exception, UnsupportedEncodingException, IOException,
			JsonParseException, JsonMappingException {
		
		ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/contact", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.param("moduleId", module.getId())
        		.param("externalId", externalId))
        		.andExpect(status().isOk());
    	
		MvcResult result = actions.andReturn();
		String response = result.getResponse().getContentAsString();
		DocumentContext nDC = JsonPath.parse(response);
        
        ObjectMapper mapper = new ObjectMapper();
        ContactDO[] contacts = mapper.readValue(response, ContactDO[].class);
		return contacts;
	}
	
	public static AutoCompleteDO[] searchAddress(MockMvc mockMvc, String token, String organisationId, String query) throws Exception{
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/search/address", organisationId))
				.param("q", query)
				.param("autoComplete", "true")
				.header("Authorization", token));
		MvcResult result = personAction.andReturn();
		String response = result.getResponse().getContentAsString();
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(response, AutoCompleteDO[].class);
	}
	
	
	public static PersonDO[] getPersons(MockMvc mockMvc, String token, String organisationId) throws Exception{

		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/person", organisationId))
				.header("Authorization", token));
    	MvcResult result = personAction.andReturn();
    	String response = result.getResponse().getContentAsString();
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(response, PersonDO[].class);
	}
	
	public static TaskDO[] getTasks(MockMvc mockMvc, String token, String organisationId) throws Exception{

		ResultActions taskAction = mockMvc.perform(get(String.format("/organisation/%s/task", organisationId))
				.header("Authorization", token))
				.andExpect(status().isOk());
		MvcResult result = taskAction.andReturn();
		String response = result.getResponse().getContentAsString();
    	return new ObjectMapper().readValue(response, TaskDO[].class);
	}
	
	public static ReportDO[] getReports(MockMvc mockMvc, String token, String organisationId) throws Exception{
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/report", organisationId))
				.header("Authorization", token));
    	MvcResult result = personAction.andReturn();
    	String response = result.getResponse().getContentAsString();
    	return new ObjectMapper().readValue(response, ReportDO[].class);
	}
	
	
	public static DocumentDO[] getDocuments(MockMvc mockMvc, String token, String organisationId, String moduleId, String externalId) throws Exception{
		ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/document", organisationId))
				.param("moduleId", moduleId)
				.param("externalId", externalId)
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token))
        		.andExpect(status().isOk());
    	MvcResult result = actions.andReturn();
    	String response = result.getResponse().getContentAsString();
    	return new ObjectMapper().readValue(response, DocumentDO[].class);
	}
	
	public static DocumentTypeDO[] getDocumentTypes(MockMvc mockMvc, String token, String organisationId) throws Exception{
		ResultActions moduleAction = mockMvc.perform(get(String.format("/organisation/%s/documenttype", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token))
        		.andExpect(status().isOk());
    	MvcResult result = moduleAction.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());
    	
    	String response = result.getResponse().getContentAsString();
    	return new ObjectMapper().readValue(response, DocumentTypeDO[].class);
	}
	
	public static String uploadDocument(MockMvc mockMvc, String token, String organisationId, String filePath) throws Exception{
        Path path = Paths.get(filePath);
        MockMultipartFile firstFile = new MockMultipartFile("file", path.toFile().getName(), "undefined", Files.readAllBytes(path));

        ResultActions action = mockMvc.perform(
        		MockMvcRequestBuilders.fileUpload(String.format("/organisation/%s/file", organisationId))
        		.file(firstFile)
        		.header("Authorization", token))
                .andExpect(status().isOk());
        
    	MvcResult result = action.andReturn();
    	DocumentContext nDC = JsonPath.parse(result.getResponse().getContentAsString());    	
    	return nDC.read("$.ref");
	}


	public static NoteDO[] getNotes(MockMvc mockMvc, String token,
			String organisationId, String moduleId, String externalId, String noteId) throws Exception {
		ResultActions personAction = mockMvc.perform(get(String.format("/organisation/%s/note", organisationId))
				.header("Authorization", token)
				.param("moduleId", moduleId)
				.param("noteId", noteId)
				.param("externalId", externalId))
				.andExpect(status().isOk());
		MvcResult result = personAction.andReturn();
        String response = result.getResponse().getContentAsString();
        return new ObjectMapper().readValue(response, NoteDO[].class);
	}


	public static NoteTypeDO[] getNoteTypes(MockMvc mockMvc, String token,
			String organisationId) throws Exception {
		ResultActions moduleAction = mockMvc.perform(get(String.format("/organisation/%s/notetype", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token))
        		.andExpect(status().isOk());
    	MvcResult result = moduleAction.andReturn();
    	String response = result.getResponse().getContentAsString();
    	return new ObjectMapper().readValue(response, NoteTypeDO[].class);
	}


	public static SupplierDO[] getSuppliers(MockMvc mockMvc, String token,
			String organisationId) throws Exception {
		ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/supplier", organisationId))
				.header("Authorization", token));
    	MvcResult result = actions.andReturn();
    	String response = result.getResponse().getContentAsString();
    	return new ObjectMapper().readValue(response, SupplierDO[].class);
    	
	}


	public static UserDO[] getUsers(MockMvc mockMvc, String token,
			String organisationId) throws Exception {
		ResultActions actions = mockMvc.perform(get(String.format("/organisation/%s/user", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token))
        		.andExpect(status().isOk());
    	MvcResult result = actions.andReturn();
    	String response = result.getResponse().getContentAsString();
    	return new ObjectMapper().readValue(response, UserDO[].class);
	}


	public static ContractTypeDO[] getContractTypes(MockMvc mockMvc,
			String token, String organisationId) throws Exception {
		ResultActions contractAction = mockMvc.perform(get(String.format("/organisation/%s/contracttype", organisationId))
				.header("Authorization", token));
		MvcResult result = contractAction.andReturn();
    	String response = result.getResponse().getContentAsString();
    	return new ObjectMapper().readValue(response, ContractTypeDO[].class);
	}

	public static ContractReferenceFormatDO[] getContractReferenceFormats(MockMvc mockMvc,
			String token, String organisationId) throws Exception {
		ResultActions contractAction = mockMvc.perform(get(String.format("/organisation/%s/contractreferenceformat", organisationId))
				.header("Authorization", token));
		MvcResult result = contractAction.andReturn();
    	String response = result.getResponse().getContentAsString();
    	return new ObjectMapper().readValue(response, ContractReferenceFormatDO[].class);
	}

	public static StatusDO[] getStatuses(MockMvc mockMvc, String token,
			String organisationId, String moduleId) throws Exception {
		ResultActions contractAction = mockMvc.perform(get(String.format("/organisation/%s/status", organisationId))
				.header("Authorization", token)
				.param("moduleId", moduleId));
		MvcResult result = contractAction.andReturn();
    	String response = result.getResponse().getContentAsString();
    	return new ObjectMapper().readValue(response, StatusDO[].class);
	}
	
	public static ResultActions postDetail(MockMvc mockMvc, String token, String organisationId, ContractDetailDO contractDetailDO) throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(contractDetailDO);
    	
        return mockMvc.perform(post(String.format("/organisation/%s/contractdetail", organisationId))
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
        		.header("Authorization", token)
        		.content(content));
	}
	
	public static ContractDetailDO[] getContractDetail(MockMvc mockMvc, String organisationId, String token, String contractId, String group) throws Exception{
    	// Get the contract
		ResultActions contractDetailAction = mockMvc.perform(get(String.format("/organisation/%s/contractdetail", organisationId))
				.header("Authorization", token)
				.param("contractId", contractId)
				.param("group", group));
    	MvcResult result = contractDetailAction.andReturn();
        String response = result.getResponse().getContentAsString();       
        return new ObjectMapper().readValue(response, ContractDetailDO[].class);
	}
	
	public static ResultActions postContract(MockMvc mockMvc, String organisationId, String token, ContractDO contract) throws Exception{
		String content = new ObjectMapper().writeValueAsString(contract);
		return mockMvc.perform(post(String.format("/organisation/%s/contract", organisationId))
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.header("Authorization", token)
				.content(content));
	}
	
	public static ResultActions putContract(MockMvc mockMvc, String organisationId, String token, ContractDO contract) throws Exception{
		String content = new ObjectMapper().writeValueAsString(contract);
		return mockMvc.perform(put(String.format("/organisation/%s/contract", organisationId))
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.header("Authorization", token)
				.content(content));
	}
}
