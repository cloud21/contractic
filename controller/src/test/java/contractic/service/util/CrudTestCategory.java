package contractic.service.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface CrudTestCategory {
	public static final Date date = new Date();
	public static String TestID(Class<?> klass) {
		return String.format("%s@%s", klass.getSimpleName(), new SimpleDateFormat("dd/MM/yyyy HHmmss").format(date));
	}
}
