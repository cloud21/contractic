package contractic.service.util;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import contractic.model.data.util.HibernateUtil;

public class TestQueryJSONTransform {

	@Before
	public void setUp() throws Exception {
		HibernateUtil.initialize();
	}

	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void test() throws JsonProcessingException {
		Session session = HibernateUtil.currentSession();
		
		
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("UserId", new Integer(1000));
		paramMap.put("Action", "Read");
		
		XStream xStream = new XStream(new StaxDriver());
		xStream.alias("map", java.util.Map.class);
		String xml = xStream.toXML(paramMap);
		
		
		Query query = session.createSQLQuery("EXEC dbo.sp_UserContractActivity :Params, :OrganisationId")
		.setParameter("Params", xml)
		.setParameter("OrganisationId", "1000");
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String, Object>> list = query.list();
		
    	ObjectMapper mapper = new ObjectMapper();
    	String content = mapper.writeValueAsString(list);
    	System.out.println(content);
	}

}
