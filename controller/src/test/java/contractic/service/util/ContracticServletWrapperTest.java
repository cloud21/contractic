package contractic.service.util;

import static org.junit.Assert.assertTrue;

import java.util.Enumeration;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

public class ContracticServletWrapperTest {

	public static boolean contains(Enumeration<String> enumeration, String test) {
		while(enumeration.hasMoreElements()){
			String val = enumeration.nextElement();
			System.out.println(val);
			if(val.equals(test))
				return true;
		}
		return false;
	}
	
	@Test
	public void test() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setServerName("www.example.com");
		request.setRequestURI("/foo");
		request.setQueryString("param1=value1&param");
		
		ContracticServletWrapper httpRequest = new ContracticServletWrapper(request);
		httpRequest.addHeader("testheader", "testvalue");
		
		Enumeration enume = httpRequest.getHeaderNames();
		while(enume.hasMoreElements())
			System.out.println(enume.nextElement());
				
		
		assertTrue(httpRequest.getHeader("testheader").equals("testvalue"));
	}

}
