package contractic.service.util;


/**
 * Marker interface for test that only update the system
 * @author Michael Sekamanya
 *
 */
public interface CreateTestCategory extends CrudTestCategory{
	public static String TestID(){ return CrudTestCategory.TestID(CreateTestCategory.class);}
}
