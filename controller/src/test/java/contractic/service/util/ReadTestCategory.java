package contractic.service.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Marker interface for test that only read from the system
 * @author Michael Sekamanya
 *
 */
public interface ReadTestCategory extends CrudTestCategory{}
