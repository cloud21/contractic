package contractic.service.filter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import contractic.service.util.ContracticServletWrapper;

public class SubdomainFilter implements Filter {
	public static final String SUBDOMAIN = "subdomain";
	private static final Logger logger = Logger.getLogger(SubdomainFilter.class);
	private FilterConfig filterConfig;
	private static final Pattern pattern = Pattern.compile("^(https?(://)?)?(((\\w+)\\.(.*))|localhost)$");
	private Properties environmentProperties = new Properties();
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	private void loadVersionProperties() throws IOException{
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		try(InputStream input = classLoader.getResourceAsStream("version.properties")){
			environmentProperties.load(input);			
		}
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		//logger.info(String.format("Filter %s executed for %s!!!", SubdomainFilter.class.getName(), request.getServerName()));
		final Matcher matcher = pattern.matcher(request.getServerName());
		final ContracticServletWrapper httpRequest = new ContracticServletWrapper((HttpServletRequest)request);
		if(matcher.matches()){			
			httpRequest.addHeader(SUBDOMAIN, matcher.group(5));
		}
		
/*		Enumeration enume = httpRequest.getHeaderNames();
		
		while(enume.hasMoreElements()){
			Object val = enume.nextElement();
			logger.info(val);
		}*/
		
		HttpServletResponse httpResponse = (HttpServletResponse)response;
		if(httpResponse!=null){
			httpResponse.addHeader("Version", environmentProperties.getProperty("version"));
			httpResponse.addHeader("Environment", environmentProperties.getProperty("environment"));
			
			/*logger.info(String.format("Version: %s, Environment: %s", httpResponse.getHeader("Version"), httpResponse.getHeader("Environment")));*/
		}
		
		filterChain.doFilter(httpRequest, response);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
		try {
			loadVersionProperties();
		} catch (IOException e) {
			throw new ServletException(e);
		}
	}

}

