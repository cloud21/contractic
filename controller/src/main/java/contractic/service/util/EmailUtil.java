package contractic.service.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.util.StringUtils;

public class EmailUtil {
	
	public final static String MAIL_SMTP_HOST = "mail.smtp.host";
	public final static String MAIL_SMTP_USER = "mail.smtp.user";
	public final static String MAIL_SMTP_PASSWORD = "mail.smtp.password";
	public final static String MAIL_SMTP_PORT = "mail.smtp.port";
	public final static String MAIL_SMTP_AUTH = "mail.smtp.auth";
	public static final String MAIL_SENDER = "mail.sender";
	
	public static void send(final Properties properties, final String from,
			final List<String> toList, String subject, String body, final List<File> files) {

	}

	private static class SMTPAuthenticator extends javax.mail.Authenticator {
		private String user;
		private String password;
		
		public SMTPAuthenticator(String user, String password){
			this.user = user;
			this.password = password;
		}
        public PasswordAuthentication getPasswordAuthentication() {
           return new PasswordAuthentication(this.user, this.password);
        }
    }
	
	public static void send(final Properties properties, final String from,
			final List<String> toList, String subject, String body) throws Exception {

		

		Boolean.parseBoolean((String)properties.get(MAIL_SMTP_AUTH));
		String user = properties.getProperty(MAIL_SMTP_USER);
		String password = properties.getProperty(MAIL_SMTP_PASSWORD);

		//if(StringUtils.hasText(str)				properties.put(EmailUtil.MAIL_SMTP_AUTH, true);)
		
		Boolean enableAuth = StringUtils.hasText(user);
		
		properties.put(MAIL_SMTP_AUTH, enableAuth.toString());
		// Get the default Session object.
		Authenticator auth = new SMTPAuthenticator(user, password);
		Session session = (enableAuth != null && enableAuth) 
				? Session.getDefaultInstance(properties, auth)
        : Session.getDefaultInstance(properties);

		// Create a default MimeMessage object.
		MimeMessage message = new MimeMessage(session);

		// Set From: header field of the header.
		message.setFrom(new InternetAddress(from));
		
		List<Exception> exceptionList = new ArrayList<>(toList.size());

		// Set To: header field of the header.
		toList.forEach(to -> {
			try {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			} catch (Exception e) {
				exceptionList.add(e);
			}
			});

		if(exceptionList.size() > 0)
			throw exceptionList.get(0);
		
		// Set Subject: header field
		message.setSubject(subject);

		// Now set the actual message
		message.setText(body);

		// Send message
		Transport.send(message);

	}
}
