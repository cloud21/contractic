package contractic.service.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DomainMatcher {
	private final Pattern pattern;
	public DomainMatcher(String regex){
		pattern = Pattern.compile(regex);
	}
	public String[] match(String target){
		final Matcher matcher = pattern.matcher(target);
		String[] groups = new String[matcher.groupCount()];
		return groups;
	}
}
