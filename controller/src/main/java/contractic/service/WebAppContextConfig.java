package contractic.service;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import contractic.service.controllers.ReportViewController;
import contractic.service.controllers.SessionController;
import contractic.service.util.EmailUtil;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "contractic.service.controllers" })
public class WebAppContextConfig extends WebMvcConfigurerAdapter{
	
	@Autowired
    private ServletContext context;
	
	@Value("${reportViewHome}")
	private String reportViewHome;
	@Value("${smtpUserName}")
	private String smtpUserName;
	@Value("${smtpPassword}")
	private String smtpPassword;
	@Value("${smtpHost}")
	private String smtpHost;
	@Value("${smtpPort}")
	private String smtpPort;
	@Value("${mailSender}")
	private String mailSender;
	
	@PostConstruct
	public void PostConstruct() throws Exception {
		context.setAttribute(ReportViewController.REPORT_VIEW_HOME, reportViewHome);
		context.setAttribute(EmailUtil.MAIL_SMTP_USER, smtpUserName);
		context.setAttribute(EmailUtil.MAIL_SMTP_PASSWORD, smtpPassword);
		context.setAttribute(EmailUtil.MAIL_SMTP_HOST, smtpHost);
		context.setAttribute(EmailUtil.MAIL_SMTP_PORT, smtpPort);
		context.setAttribute(EmailUtil.MAIL_SENDER, mailSender);
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations(
				"/static/");
	}

	@Override
	public void configureDefaultServletHandling(
			DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

/*	@Bean
	public SimpleMappingExceptionResolver exceptionResolver() {
		SimpleMappingExceptionResolver exceptionResolver = new SimpleMappingExceptionResolver();

		Properties exceptionMappings = new Properties();

		exceptionMappings.put("java.lang.Exception", "error/error");
		exceptionMappings.put("java.lang.RuntimeException", "error/error");

		exceptionResolver.setExceptionMappings(exceptionMappings);

		Properties statusCodes = new Properties();

		statusCodes.put("error/404", "404");
		statusCodes.put("error/error", "500");

		exceptionResolver.setStatusCodes(statusCodes);

		return exceptionResolver;
	}*/

	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();

		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/jsp/");
		viewResolver.setSuffix(".jsp");

		return viewResolver;
	}
	
	@Bean
	public CommonsMultipartResolver multipartResolver(){
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(50000000);
		return multipartResolver;
	}
	
}
