package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Address;
import contractic.service.domain.AddressDO;
import contractic.service.domain.AddressService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{orgId}/address")
public final class AddressController {
	
	private static final Logger logger = Logger.getLogger(AddressController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<Address, Long, AddressDO, MessageDO> addressService;
	
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{personId}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public AddressDO get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @PathVariable("addressId") String addressId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
						
			final Map<String, Object> params = new HashMap<>();
			params.put(AddressService.GET_PARAM_HASH, addressId);
			params.put(AddressService.GET_PARAM_ORGANISATIONID, orgId);
			final List<AddressDO> addresses = addressService.get(params);
			return addresses.get(0);
			
		}catch(Throwable ex){
			logger.warn(String.format("Failed to get address %s", addressId), ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<AddressDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			final Map<String, Object> params = new HashMap<>();
			params.put(AddressService.GET_PARAM_ORGANISATIONID, orgId);
			final List<AddressDO> addresses = addressService.get(params);
			return addresses;
		}catch(Throwable ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @RequestBody AddressDO pojo) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			pojo.setOrganisationId(orgId);
			return addressService.create(pojo);
		}catch(Throwable ex){
			return new MessageDO(null, "Contractic", "Failed to create address", MessageDO.ERROR);
		}
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @RequestBody AddressDO pojo) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			pojo.setOrganisationId(orgId);
			return addressService.update(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
		}
		return null;
	}
}
