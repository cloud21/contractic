package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.SupplierPerson;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.SupplierPersonDO;
import contractic.service.domain.SupplierPersonService;
import contractic.service.domain.SupplierService;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/supplier/{supplierId}/person")
public final class SupplierPersonController {
	
	private final static Logger logger = Logger.getLogger(SupplierPersonController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<SupplierPerson, Long, SupplierPersonDO, MessageDO> supplierPersonService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{supplierPersonId}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public SupplierPersonDO get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@PathVariable("supplierPersonId") String supplierPersonId, 
			@PathVariable("supplierId") String supplierId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(SupplierPersonService.GET_PARAM_HASHID, supplierPersonId);
			params.put(SupplierPersonService.GET_PARAM_ORGANISATIONID, organisationId);
			params.put(SupplierPersonService.GET_PARAM_SUPPLIERID, supplierId);
			List<SupplierPersonDO> personList = supplierPersonService.get(params);
			if(personList.size()==1)
				return personList.get(0);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<SupplierPersonDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathVariable("supplierId") String supplierId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			
			Map<String, Object> params = new HashMap<>();
			params.put(SupplierPersonService.GET_PARAM_SUPPLIERID, supplierId);
			return supplierPersonService.get(params);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, 
			@PathVariable("supplierId") String supplierId, 
			@RequestBody SupplierPersonDO pojo) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			pojo.setOrganisationId(organisationId);
			assert supplierId.equals(pojo.getSupplier().getId());
			
			return supplierPersonService.create(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing post request", ex);
			return new MessageDO(null, "Contractic", "Failed to create supplier person", MessageDO.ERROR);
		}
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, 
			@PathVariable("supplierId") String supplierId, 
			@RequestBody SupplierPersonDO pojo) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			
			logger.debug("Received PersonDO " + pojo);
			pojo.setOrganisationId(organisationId);
			assert supplierId.equals(pojo.getSupplier().getId());
			return supplierPersonService.update(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing put request", ex);
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{supplierPersonId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@PathVariable("supplierId") String supplierId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			SupplierPersonDO supplierDO = new SupplierPersonDO();
			supplierDO.setOrganisationId(organisationId);
			supplierDO.setId(supplierId);
			
			return supplierPersonService.delete(supplierDO);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
}
