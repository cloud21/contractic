package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.ContractType;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContractTypeDO;
import contractic.service.domain.ContractTypeService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/contracttype")
public final class ContractTypeController {
	
	private static final Logger logger = Logger.getLogger(ContractTypeController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<ContractType, Long, ContractTypeDO, MessageDO> contractTypeService;
		
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<ContractTypeDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable(value="organisationId") String organisationId) throws ContracticServiceException{
		
		UserDO sessionToken = sessionService.get(token);
		
		final Map<String, Object> params = new HashMap<>();
		params.put(ContractTypeService.GET_PARAM_ORGANISATIONID, organisationId);
		params.put(CommonService.USER, sessionToken);
		return contractTypeService.get(params);
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{typeId}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ContractTypeDO get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@PathVariable("typeId") String typeId) throws Exception{
		

			UserDO sessionToken = sessionService.get(token);

			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(ContractTypeService.GET_PARAM_HASH, typeId);
			params.put(ContractTypeService.GET_PARAM_ORGANISATIONID, organisationId);
			params.put(CommonService.USER, sessionToken);
			List<ContractTypeDO> contractTypeList = contractTypeService.get(params);
			if(contractTypeList.size()==1)
				return contractTypeList.get(0);
			else throw new Exception("An error occured while retrieving results");
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody ContractTypeDO contractTypeDO) throws ContracticServiceException{
		
		UserDO sessionToken = sessionService.get(token);
		// Enforce security here
		contractTypeDO.setOrganisationId(organisationId);
		contractTypeDO.setUser(sessionToken);
		return contractTypeService.create(contractTypeDO);
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody ContractTypeDO contractTypeDO) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		contractTypeDO.setOrganisationId(organisationId);
		contractTypeDO.setUser(sessionToken);
		return contractTypeService.update(contractTypeDO);

	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{typeId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organistaionId, 
			@PathVariable("typeId") String typeId) throws Exception{
		  
		UserDO sessionToken = sessionService.get(token);

		// Enforce security here
		ContractTypeDO contractTypeDO = new ContractTypeDO();
		contractTypeDO.setUser(sessionToken);
		contractTypeDO.setOrganisationId(organistaionId);
		contractTypeDO.setId(typeId);;
		
		return contractTypeService.delete(contractTypeDO);
	}
}
