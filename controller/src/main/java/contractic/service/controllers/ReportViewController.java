package contractic.service.controllers;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.PathParam;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import contractic.model.data.store.Report;
import contractic.model.data.store.ReportOrganisation;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ReportDO;

@Controller
@RequestMapping("/organisation/{organisationId}/view/report")
public class ReportViewController extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2196928605480237293L;

	private final static Logger logger = Logger.getLogger(ReportViewController.class);

	public static final String REPORT_VIEW_HOME = "report.view.home";
	
	@Autowired
	private DomainService<ReportOrganisation, Long, ReportDO, MessageDO> reportService;
	
	@Autowired
    private ServletContext context;
	
	private File getReportViewFile(String id) throws ContracticServiceException{
		ReportOrganisation reportOrganisation = reportService.get(id);
		String externalRef = reportOrganisation.getReport().getExternalRef();
		String reportViewHome = (String) context.getAttribute(REPORT_VIEW_HOME);
		return new File(reportViewHome, String.format("%s.html", externalRef));
	}
	
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
	public FileSystemResource get(@PathVariable("organisationId") String organisationId,
			@PathParam("id") String id, HttpServletResponse response){
		try{
			// Enforce security here

			File file = getReportViewFile(id);
			FileSystemResource fileResource = new FileSystemResource(file);
			
			response.setContentLength((int)file.length());
		    response.setContentType(MediaType.TEXT_HTML_VALUE);
		    response.setCharacterEncoding("UTF-8");
		    return fileResource;
			
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}

}
