package contractic.service.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.FileDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;

@Controller
@RequestMapping("/organisation/{organisationId}/file")
public final class FileController {
	
	private final static Logger logger = Logger.getLogger(FileController.class);
	
	@Autowired
	private DomainService<FileDO, Long, FileDO, MessageDO> fileService;
	
	@Autowired
    private ServletContext context;
	
    @ExceptionHandler(Throwable.class)
    public void handleException(Throwable t) {
		t.printStackTrace();
		logger.warn("An error occurred while processing request", t);
    }
	
	// Download a file with a given id
	@ResponseBody
	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public FileSystemResource get(/*@RequestHeader(value="Authorization") String token, */@PathVariable("organisationId") String organisationId,
			@PathVariable("id") String id, HttpServletResponse response){
		try{
			// Enforce security here
			FileDO fileDO = fileService.get(id);
			File file = new File(fileDO.getPath());
			FileSystemResource fileResource = new FileSystemResource(file);
			
			response.setContentLength((int)file.length());
		    response.setContentType("application/force-download");
		    String fileName = fileDO.getName();
		    response.setHeader("Content-Disposition","attachment; filename=\"" + (StringUtils.hasText(fileName) ? fileName : "unnamed") + "\"");//fileName);
		    return fileResource;
			
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	private FileDO saveFile(final File destinationFile, MultipartFile mFile) throws IOException{
		FileDO pojo = new FileDO();
		
		Path path = Paths.get(destinationFile.toURI());
		Files.write(path, mFile.getBytes());
		pojo.setPath(path.toFile().getPath());
		
		return pojo;
	}
	
	// Upload a new file and return a generated file id
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@RequestParam(value = "file", required = false) List<MultipartFile> files) throws Exception{
		
		try{

            for (MultipartFile mFile : files) {
                /*String originalFileName = mFile.getOriginalFilename();
                String fileName = mFile.getName();
                System.out.println(String.format("Original file: %s, file: %s", originalFileName, fileName));
                */
            	String ext = "";
            	if(mFile.getOriginalFilename()!=null)
            		ext = "." + FilenameUtils.getExtension(mFile.getOriginalFilename());
            	
                File file = File.createTempFile(organisationId, ext, (File)context.getAttribute(ServletContext.TEMPDIR));
                FileDO pojo = saveFile(file, mFile);
                pojo.setName(mFile.getOriginalFilename());
                pojo.setType(mFile.getContentType());
                
    			// Enforce security here
    			return fileService.create(pojo);
            }
			
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing post request", ex);
			return new MessageDO(null, "Contractic", "Failed to create file", MessageDO.ERROR);
		}
		
		return null;
	}		
}
