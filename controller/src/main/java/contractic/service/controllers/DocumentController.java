package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.service.domain.CommonService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DocumentDO;
import contractic.service.domain.DocumentService;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/document")
public final class DocumentController {
	
	private final static Logger logger = Logger.getLogger(DocumentController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DocumentService documentService;
		
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<DocumentDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathParam("documentId") String documentId, 
			@PathParam("externalId") String externalId,
			@PathParam("moduleId") String moduleId) throws Exception{
		
		UserDO user = sessionService.get(token);
		assert user != null;
		// Enforce security here
		
		final Map<String, Object> params = new HashMap<>();
		params.put(DocumentService.GET_LOG_READ, StringUtils.hasText(documentId) ? Boolean.TRUE : Boolean.FALSE);
		
		params.put(CommonService.USER, user);
		params.put(DocumentService.GET_PARAM_EXTERNALID, externalId);
		params.put(DocumentService.GET_PARAM_MODULEID, moduleId);
		params.put(DocumentService.GET_PARAM_HASHID, documentId);
		params.put(DocumentService.GET_PARAM_ORGANISATIONID, organisationId);
		return documentService.get(params);

	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, 
			@RequestBody DocumentDO documentDO) throws Exception{
		
		UserDO user = sessionService.get(token);
		assert user != null;
		// Enforce security here
		documentDO.setUser(user);
		documentDO.setOrganisationId(organisationId);
		return documentService.create(documentDO);
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, 
			@RequestBody DocumentDO pojo) throws Exception{
		
		UserDO user = sessionService.get(token);
		assert user != null;
		logger.debug("Received DocumentDO " + pojo);
		pojo.setUser(user);
		pojo.setOrganisationId(organisationId);
		return documentService.update(pojo);
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{documentId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, 
			@PathVariable("documentId") String documentId) throws Exception{
		
		UserDO user = sessionService.get(token);
		assert user != null;
		// Enforce security here
		DocumentDO documentDO = new DocumentDO();
		documentDO.setUser(user);
		documentDO.setId(documentId);
		return documentService.delete(documentDO);

	}
}
