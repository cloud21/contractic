package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.TaskContact;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.TaskContactDO;
import contractic.service.domain.TaskContactService;
import contractic.service.domain.UserDO;
import contractic.service.domain.ValueDO;

@RestController
@RequestMapping("/organisation/{orgId}/task/{taskId}/contact")
public final class TaskContactController {
	
	private static final Logger logger = Logger.getLogger(TaskContactController.class);
	
	@Autowired
	private DomainService<TaskContact, Long, TaskContactDO, MessageDO> taskContactService;
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<TaskContactDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @PathVariable("taskId") String taskId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			final Map<String, Object> params = new HashMap<>();
			params.put(TaskContactService.GET_PARAM_TASKID, taskId);
			params.put(TaskContactService.GET_PARAM_ORGANISATIONID, orgId);
			return taskContactService.get(params);
			
		}catch(Throwable ex){
			logger.warn(String.format("Failed to get contact for task", taskId), ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @PathVariable("taskId") String taskId, @RequestBody TaskContactDO contactDO) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			contactDO.setOrganisationId(orgId);
			contactDO.setTask(new ValueDO(taskId, null));
			return taskContactService.create(contactDO);
		}catch(Throwable ex){
			logger.warn(String.format("Failed to post contact %s", contactDO), ex);
			return new MessageDO(null, "Contractic", "Failed to create contact", MessageDO.ERROR);
		}
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @PathVariable("taskId") String taskId, @RequestBody TaskContactDO contactDO) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			contactDO.setOrganisationId(orgId);
			contactDO.setTask(new ValueDO(taskId, null));
			return taskContactService.update(contactDO);
		}catch(Throwable ex){
			logger.warn(String.format("Failed to post contact %s", contactDO), ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @RequestBody TaskContactDO contactDO) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			contactDO.setOrganisationId(orgId);
			return taskContactService.update(contactDO);
		}catch(Throwable ex){
			logger.warn(String.format("Failed to post contact %s", contactDO), ex);
			ex.printStackTrace();
		}
		return null;
	}
}
