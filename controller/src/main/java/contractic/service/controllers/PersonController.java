package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Person;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.PersonDO;
import contractic.service.domain.PersonService;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{orgId}/person")
public final class PersonController {
	
	private final static Logger logger = Logger.getLogger(PersonController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{personId}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public PersonDO get(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, 
			@PathVariable("personId") String personId) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);
		// Enforce security here
		Map<String, Object> params = new HashMap<>();
		params.put(DomainService.GET_PARAM_HASHID, personId);
		params.put(CommonService.USER, sessionToken);
		List<PersonDO> personList = personService.get(params);
		if(personList.size()==1)
			return personList.get(0);
		throw new RuntimeException("Could not retrieve the specified person");
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<PersonDO> get(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);
		Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, sessionToken);
		return personService.get(params);
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @RequestBody PersonDO pojo) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		// Enforce security here
		pojo.setOrganisationId(orgId);
		return personService.create(pojo);
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @RequestBody PersonDO pojo) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		logger.debug("Received PersonDO " + pojo);
		if(!StringUtils.hasText(pojo.getFirstName())){
			return new MessageDO(null, "Contractic", "Missing Firstname field", MessageDO.ERROR);
		}
		pojo.setOrganisationId(orgId);
		return personService.update(pojo);
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{personId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, @PathVariable("personId") String personId) throws ContracticServiceException{

		UserDO sessionToken = sessionService.get(token);
		// Enforce security here
		PersonDO personDO = new PersonDO();
		personDO.setOrganisationId(orgId);
		personDO.setId(personId);
		
		return personService.delete(personDO);
	}
}
