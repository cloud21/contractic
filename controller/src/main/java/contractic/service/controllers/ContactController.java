package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Contact;
import contractic.service.domain.ContactDO;
import contractic.service.domain.ContactService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{orgId}/contact")
public final class ContactController {
	
	private static final Logger logger = Logger.getLogger(ContactController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<Contact, Long, ContactDO, MessageDO> contactService;
	
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<ContactDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId,
			@PathParam("id") String id, 
			@PathParam("externalId") String externalId,
			@PathParam("moduleId") String moduleId,
			@PathParam("type") String type) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);

			// Enforce security here
			final Map<String, Object> params = new HashMap<>();
			params.put(ContactService.GET_PARAM_ORGANISATIONID, orgId);
			params.put(ContactService.GET_PARAM_EXTERNALID, externalId);
			params.put(ContactService.GET_PARAM_HASH, id);
			params.put(ContactService.GET_PARAM_MODULEID, moduleId);
			
			final List<ContactDO> addresses = contactService.get(params);
			return addresses;
		}catch(Throwable ex){
			logger.warn("Failed to get contacts %s", ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @RequestBody ContactDO contactDO) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			contactDO.setOrganisationId(orgId);
			return contactService.create(contactDO);
		}catch(Throwable ex){
			logger.warn(String.format("Failed to post contact %s", contactDO), ex);
			ex.printStackTrace();
			return new MessageDO(null, "Contractic", "Failed to create contract", MessageDO.ERROR);
		}
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @RequestBody ContactDO contactDO) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			contactDO.setOrganisationId(orgId);
			return contactService.update(contactDO);
		}catch(Throwable ex){
			logger.warn(String.format("Failed to post contact %s", contactDO), ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, @RequestBody ContactDO contactDO) throws Exception{
		
		try {
			UserDO sessionUser = sessionService.get(token);
			contactDO.setOrganisationId(orgId);
			contactDO.setSessionUser(sessionUser);
			return contactService.delete(contactDO);
		}catch(Throwable ex){
			logger.warn(String.format("Failed to post contact %s", contactDO), ex);
			ex.printStackTrace();
		}
		return null;
	}
}
