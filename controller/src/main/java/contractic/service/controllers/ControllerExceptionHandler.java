package contractic.service.controllers;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import contractic.model.ContracticModelException;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.MessageDO;



@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
	
	private static final Logger logger = Logger.getLogger(ControllerExceptionHandler.class);
	
	@ExceptionHandler({ ContracticServiceException.class })
	protected ResponseEntity<Object> handleInvalidRequest(ContracticServiceException cse, WebRequest request){	
		
		cse.printStackTrace();
		
		MessageDO message = cse.getMessageDO();
		
		Throwable cause = cse.getCause();
		if(cause instanceof ContracticModelException){
			message = new MessageDO();
			message.setType(MessageDO.ERROR);
			message.setBody(cause.getMessage());
		}
		
		if(message!=null && message.getBody()!=null)
			logger.error(message.getBody(), cse);
		else
			logger.error("Error found", cse);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return super.handleExceptionInternal(cse, message, headers, HttpStatus.BAD_REQUEST, request);
	}
	
	@ExceptionHandler({ Exception.class })
	protected ResponseEntity<Object> handleInvalidRequestException(Exception cse, WebRequest request){
		
		cse.printStackTrace();
		
		HttpHeaders headers = new HttpHeaders();
		MessageDO message = new MessageDO();
		message.setType(MessageDO.ERROR);
		message.setBody("An internal error occured");
		
		logger.error(message.getBody(), cse);
		
		return super.handleExceptionInternal(cse, message, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
	
	@ExceptionHandler({ RuntimeException.class })
	protected ResponseEntity<Object> handleInvalidRequestRuntimeException(RuntimeException cse, WebRequest request){
		
		return handleInvalidRequestException(cse, request);
	}
}
