package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Group;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.GroupDO;
import contractic.service.domain.GroupService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/group")
public final class GroupController {
	
	private final static Logger logger = Logger.getLogger(GroupController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<Group, Long, GroupDO, MessageDO> groupService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<GroupDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@PathParam("groupId") String groupId, 
			@PathParam("moduleId") String moduleId,
			@PathParam("groupSetId") String groupSetId) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;
			
			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(GroupService.GET_PARAM_HASH, groupId);
			params.put(GroupService.GET_PARAM_MODULEID, moduleId);
			params.put(GroupService.GET_PARAM_GROUPSETID, groupSetId);
			params.put(CommonService.USER, user);
			return groupService.get(params);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	// Create a new group
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, 
			@RequestBody GroupDO pojo) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			// Enforce security here
			pojo.setOrganisationId(organisationId);
			pojo.setUser(user);
			return groupService.create(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing post request", ex);
			return new MessageDO(null, "Contractic", "Create action failed", MessageDO.ERROR);
		}
	}
	
	// Update an existing group
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, 
			@RequestBody GroupDO pojo) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);

			logger.debug("Received GroupDO " + pojo);
			pojo.setOrganisationId(organisationId);
			return groupService.update(pojo);
		}catch(Throwable ex){
			logger.warn("An error occurred while processing put request", ex);
			return new MessageDO(null, "Contractic", "Update action failed", MessageDO.ERROR);
		}
	}
	
	// Delete an existing group indicator
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{groupId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @PathVariable("groupId") String groupId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		
			// Enforce security here
			GroupDO groupDO = new GroupDO();
			groupDO.setOrganisationId(organisationId);
			groupDO.setId(groupId);
			
			return groupService.delete(groupDO);
		}catch(Throwable ex){
			logger.warn("An error occurred while processing get request", ex);
			return new MessageDO(null, "Contractic", "Delete action failed", MessageDO.ERROR);
		}
	}
}
