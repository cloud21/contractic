package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.GroupSet;
import contractic.service.domain.CommonService;
import contractic.service.domain.DomainService;
import contractic.service.domain.GroupSetDO;
import contractic.service.domain.GroupSetDO;
import contractic.service.domain.GroupSetService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/groupset")
public final class GroupSetController {
	
	private final static Logger logger = Logger.getLogger(GroupSetController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<GroupSet, Long, GroupSetDO, MessageDO> groupSetService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<GroupSetDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@PathParam("id") String id,
			@PathParam("name") String name) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user!=null;
			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(GroupSetService.GET_PARAM_ORGANISATIONID, organisationId);
			params.put(GroupSetService.GET_PARAM_HASH, id);
			params.put(GroupSetService.GET_PARAM_NAME, name);
			params.put(CommonService.USER, user);
			return groupSetService.get(params);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	// Create a new group
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@RequestBody GroupSetDO pojo) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user!=null;
			// Enforce security here
			pojo.setOrganisationId(organisationId);
			pojo.setUser(user);
			return groupSetService.create(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing post request", ex);
			return new MessageDO(null, "Contractic", "Failed to create group", MessageDO.ERROR);
		}
	}
	
	// Update an existing group
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@RequestBody GroupSetDO pojo) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user!=null;
			
			logger.debug("Received GroupSetDO " + pojo);
			pojo.setOrganisationId(organisationId);
			pojo.setUser(user);
			return groupSetService.update(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing put request", ex);
		}
		return null;
	}
	
	// Delete an existing group indicator
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{groupSetId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @PathVariable("groupSetId") String groupSetId) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user!=null;
			// Enforce security here
			GroupSetDO groupDO = new GroupSetDO();
			groupDO.setOrganisationId(organisationId);
			groupDO.setId(groupSetId);
			groupDO.setUser(user);
			return groupSetService.delete(groupDO);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
}
