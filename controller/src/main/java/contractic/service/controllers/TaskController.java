package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Task;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.TaskDO;
import contractic.service.domain.TaskService;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/task")
public final class TaskController {
	
	private final static Logger logger = Logger.getLogger(TaskController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<Task, Long, TaskDO, MessageDO> taskService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<TaskDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathParam("id") String id, 
			@PathParam("externalId") String externalId,
			@PathParam("moduleId") String moduleId,
			@PathParam("parentNumber") String parentNumber) throws Exception{
		
		UserDO user = sessionService.get(token);
		assert user != null;
		// If taskId hasn't been provided, we need to have both the moduleId and the externalId provided
/*			if(!StringUtils.hasText(taskId) || (!StringUtils.hasText(moduleId) && StringUtils.hasText(externalId)))
				return null;*/
		
		Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, user);
		params.put(TaskService.GET_PARAM_EXTERNALID, externalId);
		params.put(TaskService.GET_PARAM_MODULEID, moduleId);
		params.put(TaskService.GET_PARAM_HASHID, id);
		params.put(TaskService.GET_LOG_READ, StringUtils.hasText(id) ? Boolean.TRUE : Boolean.FALSE);
		params.put(TaskService.GET_PARAM_PARENTNUMBER, parentNumber);
		return taskService.get(params);

	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, @RequestBody TaskDO pojo) throws Exception{
		
		UserDO user = sessionService.get(token);
		assert user != null;
		pojo.setUser(user);
		pojo.setOrganisationId(organisationId);
		return taskService.create(pojo);
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, @RequestBody TaskDO pojo) throws Exception{
		

		UserDO user = sessionService.get(token);
		assert user != null;
		
		logger.debug("Received TaskDO " + pojo);
		pojo.setUser(user);
		pojo.setOrganisationId(organisationId);
		return taskService.update(pojo);

	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{taskId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, @PathVariable("taskId") String taskId) throws Exception{
		
		UserDO user = sessionService.get(token);
		assert user != null;
		// Enforce security here
		TaskDO taskDO = new TaskDO();
		taskDO.setId(taskId);
		taskDO.setUser(user);
		taskDO.setOrganisationId(organisationId);
		
		return taskService.delete(taskDO);
	}
}
