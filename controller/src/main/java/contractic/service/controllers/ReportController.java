package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.ReportOrganisation;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ReportDO;
import contractic.service.domain.ReportService;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/report")
public final class ReportController {
	
	private final static Logger logger = Logger.getLogger(ReportController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<ReportOrganisation, Long, ReportDO, MessageDO> reportService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<ReportDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@PathParam("reportId") String reportId,
			@PathParam("run") Boolean run,
			@RequestParam final MultiValueMap<String, String> reportParams) throws Exception{
		
		try {
			UserDO userDO = sessionService.get(token);
			assert userDO != null;

			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(ReportService.GET_PARAM_ORGANISATIONID, organisationId);
			params.put(CommonService.USER, userDO);
			params.put(ReportService.GET_PARAM_EXECUTE, run);
			params.put(ReportService.GET_REPORT_PARAMS, reportParams);
			params.put(ReportService.GET_PARAM_HASH, reportId);
			return reportService.get(params);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	// Create a new group
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@RequestBody ReportDO pojo) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			pojo.setOrganisationId(organisationId);
			return reportService.create(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing post request", ex);
			return new MessageDO(null, "Contractic", "Failed to create group", MessageDO.ERROR);
		}
	}
	
	// Update an existing group
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@RequestBody ReportDO pojo) throws Exception{
		
		try {
			UserDO userDO = sessionService.get(token);
			assert userDO != null;

			pojo.setUser(userDO);
			pojo.setOrganisationId(organisationId);
			return reportService.update(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing put request", ex);
		}
		return null;
	}
	
	// Delete an existing group indicator
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{groupId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @PathVariable("groupId") String groupId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
					
			// Enforce security here
			ReportDO groupDO = new ReportDO();
			groupDO.setOrganisationId(organisationId);
			groupDO.setId(groupId);
			
			return reportService.delete(groupDO);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
}
