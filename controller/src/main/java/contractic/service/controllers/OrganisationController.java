package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Organisation;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.OrganisationDO;
import contractic.service.domain.OrganisationService;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;
import contractic.service.filter.SubdomainFilter;

@RestController
@RequestMapping("/organisation")
public final class OrganisationController {
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	
/*	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{orgid}", method=RequestMethod.GET
			, produces = {MediaType.APPLICATION_JSON_VALUE})
	public OrganisationDO get(@PathVariable("orgid") String orgid){
		try{
			return organisationService.getByUUID(orgid);
		}catch(Throwable ex){
			ex.printStackTrace();
		}
		return null;
	}*/
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET
			, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<OrganisationDO> get(@RequestHeader Map<String, String> headerMap){
		try{
			final Map<String, Object> params = new HashMap<>();
			params.put(OrganisationService.GET_PARAM_CODE, headerMap.get(SubdomainFilter.SUBDOMAIN));
			return organisationService.get(params);
		}catch(Throwable ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST
			, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestBody OrganisationDO pojo){
		try{
			return organisationService.create(pojo);
		}catch(Throwable ex){
			return new MessageDO(null, "Contractic", "Failed to create organisation", MessageDO.ERROR);
		}
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT
			, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestBody OrganisationDO pojo){
		try{
			return organisationService.update(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
		}
		return null;
	}
}
