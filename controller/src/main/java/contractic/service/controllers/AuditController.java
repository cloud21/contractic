package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Audit;
import contractic.service.domain.AuditDO;
import contractic.service.domain.AuditService;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.NoteService;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/audit")
public final class AuditController {
	
	private final static Logger logger = Logger.getLogger(AuditController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<Audit, Long, AuditDO, MessageDO> auditService;
		
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<AuditDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathParam("auditId") String auditId, 
			@PathParam("externalId") String externalId,
			@PathParam("moduleId") String moduleId,
			@PathParam("userId") String userId) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;
			// Enforce security here
			
/*			// If auditId hasn't been provided, we need to have both the moduleId and the externalId provided
			if(!StringUtils.hasText(auditId) && (!StringUtils.hasText(moduleId) || !StringUtils.hasText(externalId)))
				return null;
			*/
			Map<String, Object> params = new HashMap<>();
			params.put(AuditService.GET_PARAM_EXTERNALID, externalId);
			params.put(AuditService.GET_PARAM_MODULEID, moduleId);
			params.put(AuditService.GET_PARAM_HASHID, auditId);
			params.put(AuditService.GET_PARAM_USERID, userId);
			params.put(AuditService.GET_PARAM_ORGANISATIONID, organisationId);
			params.put(CommonService.USER, user);
			return auditService.get(params);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
}
