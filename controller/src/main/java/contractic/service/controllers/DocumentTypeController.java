package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.DocumentType;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DocumentTypeDO;
import contractic.service.domain.DocumentTypeService;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/documenttype")
public final class DocumentTypeController {
	
	private static final Logger logger = Logger.getLogger(DocumentTypeController.class);
	
	@Autowired
	private DomainService<DocumentType, Long, DocumentTypeDO, MessageDO> documentTypeService;
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
		
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<DocumentTypeDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable(value="organisationId") String organisationId) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		final Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, sessionToken);
		params.put(DocumentTypeService.GET_PARAM_ORGANISATIONID, organisationId);
		return documentTypeService.get(params);

	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{doctypeId}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public DocumentTypeDO get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@PathVariable("doctypeId") String doctypeId) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		// Enforce security here
		Map<String, Object> params = new HashMap<>();
		params.put(DocumentTypeService.GET_PARAM_HASH, doctypeId);
		params.put(DocumentTypeService.GET_PARAM_ORGANISATIONID, organisationId);
		params.put(CommonService.USER, sessionToken);
		List<DocumentTypeDO> documentTypeList = documentTypeService.get(params);
		
		if(documentTypeList.size()==1)
			return documentTypeList.get(0);
		throw new RuntimeException("Error processing request");

	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody DocumentTypeDO documentTypeDO) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);
		documentTypeDO.setUser(sessionToken);
		documentTypeDO.setOrganisationId(organisationId);
		return documentTypeService.create(documentTypeDO);

	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody DocumentTypeDO documentTypeDO) throws Exception{

		UserDO sessionToken = sessionService.get(token);
		documentTypeDO.setUser(sessionToken);
		documentTypeDO.setOrganisationId(organisationId);
		return documentTypeService.update(documentTypeDO);

	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{doctypeId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organistaionId, 
			@PathVariable("doctypeId") String doctypeId) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		// Enforce security here
		DocumentTypeDO docTypeDO = new DocumentTypeDO();
		docTypeDO.setUser(sessionToken);
		docTypeDO.setOrganisationId(organistaionId);
		docTypeDO.setId(doctypeId);;
		
		return documentTypeService.delete(docTypeDO);

	}
}
