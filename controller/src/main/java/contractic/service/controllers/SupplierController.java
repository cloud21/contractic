package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.service.domain.CommonService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.SupplierDO;
import contractic.service.domain.SupplierService;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{orgId}/supplier")
public final class SupplierController {
	
	private final static Logger logger = Logger.getLogger(SupplierController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private SupplierService supplierService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{supplierId}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public SupplierDO get(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, 
			@PathVariable("supplierId") String supplierId) throws Exception{
		
			UserDO userDO = sessionService.get(token);

			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(SupplierService.GET_PARAM_HASHID, supplierId);
			params.put(CommonService.USER, userDO);
			List<SupplierDO> personList = supplierService.get(params);
			if(personList.size()==1)
				return personList.get(0);
			throw new RuntimeException("Could not retrieve the specified supplier");
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<SupplierDO> get(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId) throws Exception{
		
		UserDO userDO = sessionService.get(token);
		// Enforce security here
		
		Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, userDO);
		return supplierService.get(params);
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, @RequestBody SupplierDO pojo) throws Exception{
		
		UserDO userDO = sessionService.get(token);
		// Enforce security here
		pojo.setUser(userDO);
		// Enforce security here
		pojo.setOrganisationId(orgId);
		return supplierService.create(pojo);
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, @RequestBody SupplierDO pojo) throws Exception{
		
		UserDO userDO = sessionService.get(token);
		// Enforce security here
		pojo.setUser(userDO);
		// Enforce security here
		logger.debug("Received PersonDO " + pojo);
		pojo.setOrganisationId(orgId);
		return supplierService.update(pojo);
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{supplierId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, @PathVariable("supplierId") String supplierId) throws Exception{
		
		// Enforce security here
		SupplierDO supplierDO = new SupplierDO();
		UserDO userDO = sessionService.get(token);
		// Enforce security here
		supplierDO.setUser(userDO);
		// Enforce security here
		supplierDO.setOrganisationId(orgId);
		supplierDO.setId(supplierId);
		
		return supplierService.delete(supplierDO);
	}
}
