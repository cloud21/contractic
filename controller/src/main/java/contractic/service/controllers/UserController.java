package contractic.service.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.User;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;
import contractic.service.domain.UserService;
import contractic.service.domain.ValueDO;
import contractic.service.util.EmailUtil;
import contractic.service.util.HashingUtil;

@RestController
@RequestMapping("/organisation/{orgId}/user")
public final class UserController {
	
	private final static Logger logger = Logger.getLogger(UserController.class);
	
	@Autowired
	private ServletContext context;
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	@Autowired
	private DomainService<User, Long, UserDO, MessageDO> userService;
	
	// This helps to ensure no password information is ever transmitted back
	private void checkNoPasswordShown(List<UserDO> userList){
		if(userList.stream().anyMatch(user -> StringUtils.hasText(user.getPassword())))
			throw new IllegalArgumentException("Illegal data to be displayed");
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{userId}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public UserDO get(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, 
			@PathVariable("userId") String userId) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		// Enforce security here
		Map<String, Object> params = new HashMap<>();
		params.put(UserService.GET_PARAM_HASHID, userId);
		params.put(UserService.GET_PARAM_ORGANISATIONID, orgId);
		params.put(CommonService.USER, sessionToken);
		List<UserDO> personList = userService.get(params);
		checkNoPasswordShown(personList);
		if(personList.size()==1)
			return personList.get(0);
		else throw new Exception("An error occured while retrieving results");

	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<UserDO> get(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		// Enforce security here
		Map<String, Object> params = new HashMap<>();
		params.put(UserService.GET_PARAM_ORGANISATIONID, orgId);
		params.put(CommonService.USER, sessionToken);
		List<UserDO> userList = userService.get(params);
		checkNoPasswordShown(userList);
		return userList;

	}
	
	private void notifyUser(UserDO userDO, HttpServletRequest request) throws Exception{
		Map<String, Object> profileMap = userDO.getProfile();
		
		Map<String, Object> resetMap = new HashMap<>();
		Date date = new Date();
		String key = HashingUtil.get(date.getTime());
		resetMap.put("key", key);
		
		resetMap.put("expire", date.getTime()  + 1 * 24 * 60 * 60 * 1000 /*Configure me*/);
		profileMap.put("resetkey", resetMap);
		userService.update(userDO);
		
		// 2. Send the reset token by email to the user
		String user = (String)context.getAttribute(EmailUtil.MAIL_SMTP_USER);
		String password = (String)context.getAttribute(EmailUtil.MAIL_SMTP_PASSWORD);
		String host = (String)context.getAttribute(EmailUtil.MAIL_SMTP_HOST);
		String port = (String)context.getAttribute(EmailUtil.MAIL_SMTP_PORT);
		String sender = (String)context.getAttribute(EmailUtil.MAIL_SENDER);
		
		final Properties properties = new Properties();
		properties.setProperty(EmailUtil.MAIL_SMTP_HOST, host);
		properties.setProperty(EmailUtil.MAIL_SMTP_USER, user);
		properties.setProperty(EmailUtil.MAIL_SMTP_PASSWORD, password);
		properties.setProperty(EmailUtil.MAIL_SMTP_PORT, port);
		properties.setProperty(EmailUtil.MAIL_SENDER, sender);
		
		final String contextPath = SessionController.getURLWithContextPath(request);
		final String messageBody = String.format("Please follow this link to reset your password: %s/#/?resetkey=%s", 
				contextPath, 
				key);
		SessionController.sendResetEmail(properties, userDO, messageBody);
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId, 
			@RequestBody UserDO pojo,
			HttpServletRequest request) throws Exception{
		

		UserDO sessionToken = sessionService.get(token);
	
		
		// Validation
		if(!StringUtils.hasText(pojo.getLogin()))
			return new MessageDO(null, "Contractic", "Email address must be supplied", MessageDO.ERROR);
		
		pojo.setPassword(RandomStringUtils.randomAlphanumeric(17));
		
	/*			if(!StringUtils.hasText(pojo.getPassword()))
					return new MessageDO(null, "Contractic", "Password is missing", MessageDO.ERROR);*/
		
		if(!StringUtils.hasText(pojo.getPersonId()))
			return new MessageDO(null, "Contractic", "A valid person must be selected. The person must be created from "
					+ "an existing person on the system", MessageDO.ERROR);
		
		pojo.setOrganisationId(orgId);
		MessageDO messageDO = userService.create(pojo);
		if(messageDO.getType().equals(MessageDO.OK)){
			pojo.setId(((ValueDO)messageDO.getRef()).getId());
			pojo.setProfile(new HashMap<String, Object>());
			notifyUser(pojo, request);
		}
		return messageDO;

	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, @RequestBody UserDO pojo) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		logger.debug("Received PersonDO " + pojo);
		pojo.setOrganisationId(orgId);
		return userService.update(pojo);

	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{userId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, 
			@PathVariable("userId") String userId) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		// Enforce security here
		UserDO userDO = new UserDO();
		userDO.setOrganisationId(orgId);
		userDO.setId(userId);
		
		return userService.delete(userDO);

	}
}
