package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.ContractReferenceFormat;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContractReferenceFormatDO;
import contractic.service.domain.ContractReferenceFormatService;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/contractreferenceformat")
public final class ContractReferenceFormatController {
	
	private static final Logger logger = Logger.getLogger(ContractReferenceFormatController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<ContractReferenceFormat, Long, ContractReferenceFormatDO, MessageDO> contractReferenceFormatService;
		
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<ContractReferenceFormatDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable(value="organisationId") String organisationId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);

			final Map<String, Object> params = new HashMap<>();
			params.put(ContractReferenceFormatService.GET_PARAM_ORGANISATIONID, organisationId);
			params.put(CommonService.USER, sessionToken);
			return contractReferenceFormatService.get(params);
		}catch(Throwable ex){
			logger.warn(String.format("Failed to retrieve address types for session %s", token), ex);
			ex.printStackTrace();;
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{typeId}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ContractReferenceFormatDO get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@PathVariable("typeId") String typeId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);

			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(ContractReferenceFormatService.GET_PARAM_HASH, typeId);
			params.put(ContractReferenceFormatService.GET_PARAM_ORGANISATIONID, organisationId);
			params.put(CommonService.USER, sessionToken);
			List<ContractReferenceFormatDO> contractReferenceFormatList = contractReferenceFormatService.get(params);
			if(contractReferenceFormatList.size()==1)
				return contractReferenceFormatList.get(0);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody ContractReferenceFormatDO contractReferenceFormatDO) throws Exception{
		
		try {
			UserDO sessionUser = sessionService.get(token);

			// Enforce security here
			contractReferenceFormatDO.setSessionUser(sessionUser);
			contractReferenceFormatDO.setOrganisationId(organisationId);
			return contractReferenceFormatService.create(contractReferenceFormatDO);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing post request", ex);
			return new MessageDO(null, "Contractic", String.format("Failed to create contract reference format"), MessageDO.ERROR);
		}
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody ContractReferenceFormatDO contractReferenceFormatDO) throws Exception{
		
		try {
			UserDO sessionUser = sessionService.get(token);
			contractReferenceFormatDO.setSessionUser(sessionUser);
			contractReferenceFormatDO.setOrganisationId(organisationId);
			return contractReferenceFormatService.update(contractReferenceFormatDO);
		}catch(Throwable ex){
			logger.warn("An error occurred while processing put request", ex);
			return new MessageDO(null, "Contractic", "Failed to update contract reference format", MessageDO.ERROR);
		}
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{typeId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organistaionId, 
			@PathVariable("typeId") String typeId) throws Exception{
		
		try {
			UserDO sessionUser = sessionService.get(token);

			// Enforce security here
			ContractReferenceFormatDO contractReferenceFormatDO = new ContractReferenceFormatDO();
			
			contractReferenceFormatDO.setSessionUser(sessionUser);
			contractReferenceFormatDO.setOrganisationId(organistaionId);
			contractReferenceFormatDO.setId(typeId);;
			
			return contractReferenceFormatService.delete(contractReferenceFormatDO);
		}catch(Throwable ex){
			logger.warn("An error occurred while processing get request", ex);
			return new MessageDO(null, "Contractic", "Failed to delete contract reference format", MessageDO.ERROR);
		}
	}
}
