package contractic.service.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Contract;
import contractic.model.data.store.ContractDetail;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractDetailDO;
import contractic.service.domain.ContractDetailService;
import contractic.service.domain.ContractTermDO;
import contractic.service.domain.ContractTermPeriodDO;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/contract/{contractId}/term")
public final class ContractTermController {
	
	private final static Logger logger = Logger.getLogger(ContractTermController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<Contract, Long, ContractDO, MessageDO> contractService;

	@Autowired
	private DomainService<ContractDetail, Long, ContractDetailDO, MessageDO> contractDetailService;
	
	private List<ContractDetailDO> getTermContractDetail(UserDO user, String organisationId, String contractId, String detailType) throws ContracticServiceException{
		Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, user);
		params.put(ContractDetailService.GET_PARAM_ORGANISATIONID, organisationId);
		params.put(ContractDetailService.GET_PARAM_CONTRACTID, contractId);
		params.put(ContractDetailService.GET_PARAM_GROUPNAME, detailType);
		
		return contractDetailService.get(params);
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{number}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ContractTermDO get(@RequestHeader(value="Authorization") String token
			, @PathVariable("organisationId") String organisationId
			, @PathVariable("contractId") String contractId
			, @PathVariable("number") final Integer number) throws Exception{

		if(number==null){
			throw new ContracticServiceException("Term number not supplied");
		}
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;
			Collection<ContractTermDO> termList = get(token, organisationId, contractId);
			termList.removeIf(term -> term.getNumber() == number);
			// Ensure that if we have received a detail Id, then we must return only one result or none
			assert termList.size() <= 1;
			return termList.iterator().next();

		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public Collection<ContractTermDO> get(@RequestHeader(value="Authorization") String token
			, @PathVariable("organisationId") String organisationId
			, @PathVariable("contractId") String contractId) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;

			
			List<ContractDetailDO> contractTermPeriodDetailList = getTermContractDetail(user, organisationId, contractId, ContractDetail.CONTRACT_TERM_PERIOD_GROUP_FIELD);
			List<ContractDetailDO> contractTermNoticeDetailList = getTermContractDetail(user, organisationId, contractId, ContractDetail.CONTRACT_TERM_NOTICE_GROUP_FIELD);
			List<ContractDetailDO> contractTermGoToMarketDetailList = getTermContractDetail(user, organisationId, contractId, ContractDetail.CONTRACT_TERM_GOTO_MARKET_GROUP_FIELD);
			
			final Map<Integer, ContractTermDO> termMap = contractTermPeriodDetailList.stream()
					.collect(Collectors.toMap(c -> c.getTerm().getNumber(), c -> {
						ContractTermDO termDO = new ContractTermDO();
						termDO.setPeriod(c);
						termDO.setNumber(c.getTerm().getNumber());
						return termDO;
					}));
			
			contractTermNoticeDetailList.forEach(notice -> {
				ContractTermPeriodDO periodDO = notice.getTerm();
				ContractTermDO termDO = termMap.get(periodDO.getNumber());
				termDO.setNotice(notice);
			});
			
			
			contractTermGoToMarketDetailList.forEach(gotomarket -> {
				ContractTermPeriodDO periodDO = gotomarket.getTerm();
				ContractTermDO termDO = termMap.get(periodDO.getNumber());
				termDO.setGotomarket(gotomarket);
			});
			
			return termMap.values();
			
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody ContractTermDO termDO) throws Exception{
		
		try {

			final UserDO user = sessionService.get(token);
			assert user != null;
			
			Integer number = termDO.getNumber();
			ContractDetailDO[] periodDetail = new ContractDetailDO[]
				{termDO.getPeriod()
					, termDO.getNotice()
					, termDO.getGotomarket()
				};
			
			List<Throwable> cse = new ArrayList<>();
			
			Arrays.stream(periodDetail).forEach(detail -> {
				try {
					// Enforce security here
					detail.setUser(user);
					contractDetailService.create(detail);
				}catch(Throwable ex){
					cse.add(ex);
				}	
			});
			
			if(cse.size()>0){
				Throwable ex = cse.get(0);
				if (ex instanceof ContracticServiceException){
					MessageDO message = ((ContracticServiceException)ex).getMessageDO();
					if(message!=null){
						return message;
					}
				}
				logger.error("An error occurred while processing post request", ex);
				return new MessageDO(null, "Contractic", "Failed to create contract detail", MessageDO.ERROR);
			}
			
			return new MessageDO(null, "Contractic", String.format("Contract term %s detail created successfully", number), MessageDO.OK);
			
		}catch(ContracticServiceException ex){
			MessageDO messageDO = ex.getMessageDO();
			if(messageDO == null)
				throw new Exception(ex);
			else return messageDO;
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing post request", ex);
			return new MessageDO(null, "Contractic", "Failed to create contract", MessageDO.ERROR);
		}
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody ContractDO pojo) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;
			
			logger.debug("Received ContractDO " + pojo);
			pojo.setUser(user);
			pojo.setOrganisationId(organisationId);
			return contractService.update(pojo);
		}catch(ContracticServiceException ex){
			MessageDO messageDO = ex.getMessageDO();
			if(messageDO == null){
				logger.warn("An error occurred while processing put request", ex);
			}
			else return messageDO;
		}catch(Throwable ex){
			logger.warn("An error occurred while processing put request", ex);
		}
		throw new Exception("An error occurred while service the request");
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{contractId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, 
			@PathVariable("contractId") String contractId) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;
			// Enforce security here
			ContractDO contractDO = new ContractDO();
			contractDO.setId(contractId);
			contractDO.setOrganisationId(organisationId);
			contractDO.setUser(user);
			return contractService.delete(contractDO);
		}catch(Throwable ex){
			logger.warn("An error occurred while processing get request", ex);
			throw new Exception("An error occurred while service the request");
		}
	}
}
