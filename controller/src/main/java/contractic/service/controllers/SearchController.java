package contractic.service.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Address;
import contractic.model.data.store.Contact;
import contractic.model.data.store.Group;
import contractic.model.data.store.Person;
import contractic.model.data.store.Supplier;
import contractic.model.data.store.Task;
import contractic.service.domain.AddressDO;
import contractic.service.domain.AddressService;
import contractic.service.domain.AutoCompleteDO;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContactDO;
import contractic.service.domain.ContactService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.GroupDO;
import contractic.service.domain.GroupService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.PersonDO;
import contractic.service.domain.PersonService;
import contractic.service.domain.SearchEntityDO;
import contractic.service.domain.SearchService;
import contractic.service.domain.SessionDO;
import contractic.service.domain.SupplierDO;
import contractic.service.domain.SupplierService;
import contractic.service.domain.TaskDO;
import contractic.service.domain.TaskService;
import contractic.service.domain.UserDO;
import contractic.service.util.SearchClient.SearchEntity;

@RestController
@RequestMapping("/organisation/{organisationId}/search")
public final class SearchController {
	
	private final static Logger logger = Logger.getLogger(SearchController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	@Autowired
	private DomainService<Supplier, Long, SupplierDO, MessageDO> supplierService;
	@Autowired
	private DomainService<Group, Long, GroupDO, MessageDO> groupService;
	@Autowired
	private DomainService<Task, Long, TaskDO, MessageDO> taskService;
	@Autowired
	private DomainService<Address, Long, AddressDO, MessageDO> addressService;
	@Autowired
	private DomainService<Contact, Long, ContactDO, MessageDO> contactService;
	@Autowired
	private DomainService<SearchEntity, Long, SearchEntityDO, MessageDO> searchService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<?> search(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathParam("q") String q,
			@PathParam("collection") String collection,
			@PathParam("autoComplete") String autoComplete) throws Exception{
		
		if(!StringUtils.hasText(q)){
			return new ArrayList<SearchEntityDO>();
		}
		
			UserDO sessionToken = sessionService.get(token);

			if(!StringUtils.hasText(collection))
				throw new Exception("Collection to search has not been supplied");
			
			String[] qtokens = q.split(" ");
			
			// Enforce security here
			String[] collections = collection.split(",");
			final List<SearchEntityDO> results = Collections.synchronizedList(new ArrayList<SearchEntityDO>());
			Arrays.stream(collections)
				.parallel().forEach(element -> {
				Map<String, Object> params = new HashMap<>();
				/*params.put(SearchService.GET_PARAM_QUERY, String.format("*%s*", String.join("*", qtokens)));*/
				params.put(SearchService.GET_PARAM_QUERY, q);
				if(StringUtils.hasText(element))
					params.put(SearchService.GET_PARAM_COLLECTION, element.toLowerCase());
				params.put(CommonService.USER, sessionToken);
				try {
					results.addAll(searchService.get(params));
				} catch (Exception e) {
					// Perform a direct database search in this case using methods
					logger.warn(String.format("Error when performing search on collection '%s'", element), e);
				}
			});

			return results;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/person", method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<?> searchPerson(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathParam("q") String q,
			@PathParam("autoComplete") String autoComplete) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);

			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(PersonService.GET_PARAM_SEARCH_QUERY, q);
			params.put(PersonService.GET_PARAM_ACTIVE, Boolean.TRUE);
			params.put(CommonService.USER, sessionToken);
			List<PersonDO> personList = personService.get(params);
			
			// Return values used for auto complete search
			if(Boolean.parseBoolean(autoComplete)){
				return personList.stream().map(personDO -> {
						final AutoCompleteDO autoDO = new AutoCompleteDO();
						autoDO.setId(personDO.getId());
						autoDO.setValue(String.join(" ", new String[]{
								(personDO.getFirstName() == null ? "" : personDO.getFirstName()), 
								(personDO.getMiddleName() == null ? "" : personDO.getMiddleName()), 
								(personDO.getLastName() == null ? "" : personDO.getLastName())
								}));
						return autoDO;
					}).collect(Collectors.toList());
			} else {
				return personList;
			}
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/supplier", method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<?> searchSupplier(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathParam("q") String q,
			@PathParam("autoComplete") String autoComplete) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);

			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(CommonService.USER, sessionToken);
			params.put(SupplierService.GET_PARAM_SEARCH_QUERY, q);
			params.put(SupplierService.GET_PARAM_ACTIVE, Boolean.TRUE);
			List<SupplierDO> supplierList = supplierService.get(params);
			
			// Return values used for auto complete search
			if(Boolean.parseBoolean(autoComplete)){
				return supplierList.stream().map(supplierDO -> {
						final AutoCompleteDO autoDO = new AutoCompleteDO();
						autoDO.setId(supplierDO.getId());
						autoDO.setValue(supplierDO.getName());
						return autoDO;
					}).collect(Collectors.toList());
			} else {
				return supplierList;
			}
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/group", method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<?> searchGroup(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathParam("q") String q,
			@PathParam("autoComplete") String autoComplete,
			@PathParam("moduleId") String moduleId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);

			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(GroupService.GET_PARAM_SEARCH_QUERY, q);
			params.put(GroupService.GET_PARAM_MODULEID, moduleId);
			params.put(GroupService.GET_PARAM_ACTIVE, Boolean.TRUE);
			params.put(CommonService.USER, sessionToken);
			List<GroupDO> groupList = groupService.get(params);
			
			// Return values used for auto complete search
			if(Boolean.parseBoolean(autoComplete)){
				return groupList.stream().map(groupDO -> {
						final AutoCompleteDO autoDO = new AutoCompleteDO();
						autoDO.setId(groupDO.getId());
						autoDO.setValue(groupDO.getName());
						return autoDO;
					}).collect(Collectors.toList());
			} else {
				return groupList;
			}
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/task", method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<?> searchTask(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathParam("q") String q,
			@PathParam("autoComplete") String autoComplete,
			@PathParam("moduleId") String moduleId,
			@PathParam("externalId") String externalId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);

			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(TaskService.GET_PARAM_SEARCH_QUERY, q);
			params.put(TaskService.GET_PARAM_MODULEID, moduleId);
			params.put(TaskService.GET_PARAM_EXTERNALID, externalId);
			params.put(TaskService.GET_PARAM_ACTIVE, Boolean.TRUE);
			params.put(CommonService.USER, sessionToken);
			List<TaskDO> taskList = taskService.get(params);
			
			// Return values used for auto complete search
			if(Boolean.parseBoolean(autoComplete)){
				return taskList.stream().map(taskDO -> {
						final AutoCompleteDO autoDO = new AutoCompleteDO();
						autoDO.setId(taskDO.getId());
						autoDO.setValue(taskDO.getTitle());
						return autoDO;
					}).collect(Collectors.toList());
			} else {
				return taskList;
			}
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/address", method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<?> searchAddress(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathParam("q") String q,
			@PathParam("autoComplete") String autoComplete,
			@PathParam("typeId") String typeId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(AddressService.GET_PARAM_ORGANISATIONID, organisationId);
			params.put(AddressService.GET_PARAM_SEARCH_QUERY, q);
			params.put(AddressService.GET_PARAM_TYPEID, typeId);
			params.put(AddressService.GET_PARAM_ACTIVE, Boolean.TRUE);
			List<AddressDO> addressList = addressService.get(params);
			
			// Return values used for auto complete search
			if(Boolean.parseBoolean(autoComplete)){
				return addressList.stream().map(addressDO -> {
						final AutoCompleteDO autoDO = new AutoCompleteDO();
						autoDO.setId(addressDO.getId());
						autoDO.setValue(addressDO.getSection1());
						return autoDO;
					}).collect(Collectors.toList());
			} else {
				return addressList;
			}
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/contact", method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<?> searchContact(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathParam("q") String q,
			@PathParam("autoComplete") String autoComplete,
			@PathParam("typeId") String typeId,
			/*This helps us search for contacts that relate to this module
			 * For example for the contract module, related contacts are:
			 * 1. Supplier
			 * 2. Manager
			 * For the task module, related contacts are
			 * 3. Task Assignee
			 * */
			@PathParam("moduleId") String moduleId,
			@PathParam("externalId") String externalId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(ContactService.GET_PARAM_ORGANISATIONID, organisationId);
			params.put(ContactService.GET_PARAM_SEARCH_QUERY, q);
			params.put(ContactService.GET_PARAM_TYPEID, typeId);
			params.put(ContactService.GET_PARAM_ACTIVE, Boolean.TRUE);
			params.put(ContactService.GET_PARAM_MODULEID, moduleId);
			params.put(ContactService.GET_PARAM_EXTERNALID, externalId);
			List<ContactDO> addressList = contactService.get(params);
			
			// Return values used for auto complete search
			if(Boolean.parseBoolean(autoComplete)){
				return addressList.stream().map(addressDO -> {
						final AutoCompleteDO autoDO = new AutoCompleteDO();
						autoDO.setId(addressDO.getId());
						autoDO.setValue(addressDO.getAddress().getValue());
						return autoDO;
					}).collect(Collectors.toList());
			} else {
				return addressList;
			}
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
}
