package contractic.service.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.User;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.OrganisationDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;
import contractic.service.domain.UserService;
import contractic.service.filter.SubdomainFilter;
import contractic.service.util.ConcurrencyUtil;
import contractic.service.util.EmailUtil;
import contractic.service.util.HashingUtil;

@RestController
@RequestMapping("/session")
public final class SessionController {
	
	private static final Logger logger = Logger.getLogger(SessionController.class);

	@Autowired
	private ServletContext context;
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	@Autowired
	private DomainService<User, Long, UserDO, MessageDO> userService;
	
	// Create a new session reset
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/reset",method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO resetpost(@RequestBody SessionDO sessionDO, HttpServletRequest request) throws Exception{

		if(sessionDO != null && sessionDO.getUsername() != null){
			// 1. Create a reset token in the user's profile
			UserDO userDO = getUser(sessionDO);
			sessionDO.setUser(userDO);
			Map<String, Object> profileMap = userDO.getProfile();
			
			Map<String, Object> resetMap = new HashMap<>();
			Date date = new Date();
			String key = HashingUtil.get(date.getTime());
			resetMap.put("key", key);
			
			resetMap.put("expire", date.getTime()  + 1 * 24 * 60 * 60 * 1000 /*Configure me*/);
			profileMap.put("resetkey", resetMap);
			userService.update(userDO);
			
			// 2. Send the reset token by email to the user
			String user = (String)context.getAttribute(EmailUtil.MAIL_SMTP_USER);
			String password = (String)context.getAttribute(EmailUtil.MAIL_SMTP_PASSWORD);
			String host = (String)context.getAttribute(EmailUtil.MAIL_SMTP_HOST);
			String port = (String)context.getAttribute(EmailUtil.MAIL_SMTP_PORT);
			String sender = (String)context.getAttribute(EmailUtil.MAIL_SENDER);
			
			final Properties properties = new Properties();
			properties.setProperty(EmailUtil.MAIL_SMTP_HOST, host);
			properties.setProperty(EmailUtil.MAIL_SMTP_USER, user);
			properties.setProperty(EmailUtil.MAIL_SMTP_PASSWORD, password);
			properties.setProperty(EmailUtil.MAIL_SMTP_PORT, port);
			properties.setProperty(EmailUtil.MAIL_SENDER, sender);
			
			final String contextPath = getURLWithContextPath(request);
			final String messageBody = String.format("Please follow this link to reset your password: %s/#/?resetkey=%s", 
					contextPath, 
					key);
			sendResetEmail(properties, userDO, messageBody);
			
			return new MessageDO(null, "Password reset", "Check your email to reset your password", MessageDO.OK);
		}else{
			return new MessageDO(null, "Password reset", "Invalid data supplied", MessageDO.ERROR);
		}

	}

	public static String getURLWithContextPath(HttpServletRequest request) {
		String scheme = request.getScheme();
		int port = request.getServerPort();
		final int httpsPort = 443;
		final int httpPort = 80;
		String portString = port == httpsPort || port == httpPort ? "" : ":" + port;
		return scheme + "://" + request.getServerName() + portString + request.getContextPath();
	}
	
	public static void sendResetEmail(final Properties properties, UserDO userDO, String body) throws Exception {
		String from = properties.getProperty(EmailUtil.MAIL_SENDER);
		final String[] to = new String[]{userDO.getLogin()};
		final String subject = "[Contractic] Password reset";
		//final String appPath = 

		ConcurrencyUtil.IOBoundExecutorService.submit(new Runnable(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					EmailUtil.send(properties, from, Arrays.asList(to), subject, body);
				} catch (Exception e) {
					logger.warn(String.format("Failed to send password reset email for user '%s'", userDO.getLogin()), e);
					e.printStackTrace();
				}
			}
		});
	}

	private UserDO getUser(SessionDO sessionDO)
			throws ContracticServiceException {
		final Map<String, Object> params = new HashMap<>();
		params.put(UserService.GET_PARAM_USERNAME, sessionDO.getUsername());
		final List<UserDO> userList = userService.get(params);
		assert userList.size() == 1;
		UserDO userDO = userList.get(0);
		return userDO;
	}
	
	// Reset the password and create a new session
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/reset",method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO resetput(@RequestBody SessionDO sessionDO) throws ContracticServiceException{

		if(sessionDO == null 
				|| !StringUtils.hasText(sessionDO.getUsername()) 
				|| !StringUtils.hasText(sessionDO.getPassword()) 
				|| !StringUtils.hasText(sessionDO.getPassword2())
				|| !StringUtils.hasText(sessionDO.getToken())){
			return new MessageDO(null, "Password reset", "Invalid password reset information", MessageDO.ERROR);
		}
			
		if(!sessionDO.getPassword().equals(sessionDO.getPassword2()))
			return new MessageDO(null, "Password reset", "Passwords do not match", MessageDO.ERROR);
		
		UserDO userDO = getUser(sessionDO);
		Map<String, Object> profile = userDO.getProfile();
		if(profile == null){
			logger.info("User does not have a profile set");
			return null;
		}
		Map<String, Object> resetMap = (Map<String, Object>)profile.get("resetkey");
		if(resetMap == null){
			logger.warn(String.format("No reset key map found for user '%s", userDO.getId()));
			return new MessageDO(null, null, "Failed to create your session, please contact the system administrator", MessageDO.ERROR);
		}
		String key = (String)resetMap.get("key");
		if(key == null)
			return new MessageDO(null, "Password reset", "Reset key is invalid", MessageDO.ERROR);
		// Check that it hasn't expired
		
		if(!key.equals(sessionDO.getToken()))
			return new MessageDO(null, "Password reset", "Reset key is invalid", MessageDO.ERROR);
		
		userDO.setPassword(sessionDO.getPassword());
		profile.remove("resetkey");
		userService.update(userDO);
		sessionDO.setUser(userDO);
		
		return sessionService.create(sessionDO);
		
	}
	
	// Create a new session
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestBody SessionDO sessionDO, 
			@RequestHeader Map<String, String> headerMap, 
			@RequestHeader(value="subdomain", required=false) String organisationDomain) throws ContracticServiceException{
		//logger.info(String.format("Receiving request for client domain code '%s', %s", subdomain, organisationDomain));

		if(sessionDO == null || sessionDO.getUsername() == null || sessionDO.getPassword() == null){
			throw new ContracticServiceException("Username or password is invalid");
		}
		
		String organisationDomainCode = headerMap.get(SubdomainFilter.SUBDOMAIN);
		
		UserDO userDO = getUser(sessionDO);
		
		if(!userDO.getOrganisation().get(OrganisationDO.FIELD_CODE).equals(organisationDomainCode))
			return new MessageDO(null, "Domain Error", "You seem to be loggin in from an invalid domain", MessageDO.ERROR);
		
		sessionDO.setUser(userDO);
		
		Map<String, Object> profile = userDO.getProfile();
		profile.put(UserDO.LAST_LOGIN, new Date().getTime());
		userService.update(userDO);
		
		return sessionService.create(sessionDO);

	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestBody SessionDO sessionDO, @RequestHeader(value="Authorization") String token, 
			@RequestHeader Map<String, String> headerMap, @RequestHeader(value="subdomain", required=false) String organisationDomain) throws ContracticServiceException{
		UserDO userDO = sessionService.get(token);		
		sessionDO.setUser(userDO);			
		return sessionService.update(sessionDO);

	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @RequestBody SessionDO sessionDO) throws ContracticServiceException{
		assert token.equals(sessionDO.getToken());
		sessionDO.setToken(token);
		UserDO userDO = getUser(sessionDO);
		sessionDO.setUser(userDO);
		return sessionService.delete(sessionDO);
	}
}
