package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import contractic.model.data.store.ContractDetail;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContractDetailDO;
import contractic.service.domain.ContractDetailService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/contractdetail")
public final class ContractDetailController {
	
	private final static Logger logger = Logger.getLogger(ContractDetailController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<ContractDetail, Long, ContractDetailDO, MessageDO> contractDetailService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<ContractDetailDO> get(@RequestHeader(value="Authorization") String token
			, @PathVariable("organisationId") String organisationId
			, @PathParam("id") String id
			, @PathParam("contractId") String contractId
			, @PathParam("group") String group) throws Exception{
		

		UserDO sessionToken = sessionService.get(token);
		
		Map<String, Object> params = new HashMap<>();
		params.put(ContractDetailService.GET_PARAM_HASHID, id);
		params.put(ContractDetailService.GET_PARAM_ORGANISATIONID, organisationId);
		params.put(ContractDetailService.GET_PARAM_CONTRACTID, contractId);
		params.put(ContractDetailService.GET_PARAM_GROUPNAME, group);
		params.put(CommonService.USER, sessionToken);
		
		if(!StringUtils.hasText(id) && !StringUtils.hasText(contractId)){
			throw new ContracticServiceException("Contract Id not supplied");
		}
		
		List<ContractDetailDO> contractDetailList = contractDetailService.get(params);
		
		// Ensure that if we have received a detail Id, then we must return only one result or none
		if(StringUtils.hasText(id))
			assert contractDetailList.size() <= 1;
		
		return contractDetailList;
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody ContractDetailDO contractDetailDO) throws Exception{
		try{
			UserDO sessionToken = sessionService.get(token);
			// Enforce security here
			contractDetailDO.setUser(sessionToken);
			return contractDetailService.create(contractDetailDO);
		}catch(Exception ex){
			logger.warn(String.format("An error occurred when putting object %s", new ObjectMapper().writeValueAsString(contractDetailDO)), ex);
			throw ex;
		}
		
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody ContractDetailDO contractDetailDO) throws Exception{
		try{
			UserDO sessionToken = sessionService.get(token);
			// Enforce security here
			contractDetailDO.setUser(sessionToken);
			return contractDetailService.update(contractDetailDO);
		}catch(Exception ex){
			logger.warn(String.format("An error occurred when putting object %s", new ObjectMapper().writeValueAsString(contractDetailDO)), ex);
			throw ex;
		}
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{contractDetailId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, @PathVariable("contractDetailId") String contractDetailId) throws Exception{
		
		ContractDetailDO contractDetailDO = new ContractDetailDO();
		
		try{
			UserDO sessionToken = sessionService.get(token);	
			contractDetailDO.setId(contractDetailId);
			contractDetailDO.setUser(sessionToken);
			
			return contractDetailService.delete(contractDetailDO);
		}catch(Exception ex){
			logger.warn(String.format("An error occurred when deleting object %s", new ObjectMapper().writeValueAsString(contractDetailDO)), ex);
			throw ex;
		}
		
	}
}
