package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Contract;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/contract")
public final class ContractController {
	
	private final static Logger logger = Logger.getLogger(ContractController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<Contract, Long, ContractDO, MessageDO> contractService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{contractId}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ContractDO get(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, 
			@PathVariable("contractId") String contractId) throws Exception{
		
		UserDO user = sessionService.get(token);
		assert user != null;

		// Enforce security here
		Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, user);
		params.put(ContractService.GET_PARAM_HASHID, contractId);
		
		// Enable logging when viewing contract detail
		params.put(ContractService.GET_LOG_READ, Boolean.TRUE);
		List<ContractDO> contractList = contractService.get(params);
		if(contractList.size()==1)
			return contractList.get(0);
		else throw new RuntimeException("Invalid request. Failed to retrieve contract");

	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<ContractDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId) throws Exception{
		
		UserDO user = sessionService.get(token);
		assert user!=null;

		// Enforce security here
		
		Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, user);
		
		// Disable logging when we are displaying for the contract manager
		params.put(ContractService.GET_LOG_READ, Boolean.FALSE);
		return contractService.get(params);

	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody ContractDO pojo) throws Throwable{

		UserDO user = sessionService.get(token);
		assert user != null;
		
		pojo.setUser(user);
		pojo.setOrganisationId(organisationId);
		return contractService.create(pojo);

	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody ContractDO pojo) throws Exception{
		
		UserDO user = sessionService.get(token);
		assert user != null;
		
		logger.debug("Received ContractDO " + pojo);
		pojo.setUser(user);
		pojo.setOrganisationId(organisationId);
		return contractService.update(pojo);
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{contractId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, 
			@PathVariable("contractId") String contractId) throws Exception{
	
		UserDO user = sessionService.get(token);
		assert user != null;
		// Enforce security here
		ContractDO contractDO = new ContractDO();
		contractDO.setId(contractId);
		contractDO.setOrganisationId(organisationId);
		contractDO.setUser(user);
		return contractService.delete(contractDO);
	}
}
