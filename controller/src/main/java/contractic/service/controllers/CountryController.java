package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Country;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.CountryDO;
import contractic.service.domain.CountryService;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;
import contractic.service.domain.ValueDO;

@RestController
public final class CountryController {
	
	private static final Logger logger = Logger.getLogger(CountryController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<Country, Long, CountryDO, MessageDO> countryService;

	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value = "/country", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<CountryDO> getCountry(@RequestHeader(value="Authorization") String token) throws Exception{
		
		try {
			UserDO userDO = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here	
			final Map<String, Object> params = new HashMap<>();
			return countryService.get(params);
		}catch(Throwable ex){
			logger.warn(String.format("Failed to retrieve address types for session %s", token), ex);
			ex.printStackTrace();;
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value = "/currency", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public Set<ValueDO> getCurrency(@RequestHeader(value="Authorization") String token,
			@PathParam(value="actuve") Boolean active) throws Exception{
		
		try {
			UserDO userDO = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here	
			final Map<String, Object> params = new HashMap<>();
			params.put(CountryService.GET_PARAM_ACTIVE, active);
			final List<CountryDO> countryList = countryService.get(params);
			final Set<ValueDO> currencySet = countryList.stream()
					.map(countryDO -> new ValueDO(countryDO.getCurrencyCode(), countryDO.getHtmlCode()))
					.collect(Collectors.toSet());
			return currencySet;
		}catch(Throwable ex){
			logger.warn(String.format("Failed to retrieve address types for session %s", token), ex);
			ex.printStackTrace();;
		}
		return null;
	}
}
