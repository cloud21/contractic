package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.UserRole;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;
import contractic.service.domain.UserRoleDO;
import contractic.service.domain.UserRoleService;

@RestController
@RequestMapping("/organisation/{organisationId}/userrole")
public final class UserRoleController {
	
	private final static Logger logger = Logger.getLogger(UserRoleController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<UserRole, Long, UserRoleDO, MessageDO> userRoleService;
	
	/**
	 * Get a user role
	 * @param token
	 * @param organisationId
	 * @param roleId
	 * @return a collection of user roles.
	 * @throws Exception
	 */
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<UserRoleDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user!=null;

			// Enforce security here
			
			Map<String, Object> params = new HashMap<>();
			params.put(CommonService.USER, user);
			params.put(UserRoleService.GET_PARAM_ORGANISATIONID, organisationId);
			
			// Disable logging when we are displaying for the contract manager
			params.put(UserRoleService.GET_LOG_READ, Boolean.FALSE);
			return userRoleService.get(params);
			
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
		
	
	/**
	 * Get a user role
	 * @param token
	 * @param organisationId
	 * @param roleId
	 * @return a collection of user roles.
	 * @throws Exception
	 */
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{roleId}", method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public UserRoleDO get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@PathVariable("roleId") String roleId) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user!=null;

			// Enforce security here
			
			Map<String, Object> params = new HashMap<>();
			params.put(CommonService.USER, user);
			params.put(UserRoleService.GET_PARAM_HASHID, roleId);
			params.put(UserRoleService.GET_PARAM_ORGANISATIONID, organisationId);
			
			// Disable logging when we are displaying for the contract manager
			params.put(UserRoleService.GET_LOG_READ, Boolean.FALSE);
			List<UserRoleDO> userRoleList = userRoleService.get(params);
			return (userRoleList.size()==1)
				? userRoleList.get(0) : null;
			
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	// Create new
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody UserRoleDO pojo) throws Exception{
		
		
		
		try {

			UserDO user = sessionService.get(token);
			assert user != null;
			
			pojo.setUser(user);
			pojo.setOrganisationId(organisationId);
			return userRoleService.create(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing post request", ex);
			return new MessageDO(null, "Contractic", "Failed to create user role", MessageDO.ERROR);
		}
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody UserRoleDO pojo) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;
			
			logger.debug("Received UserRoleDO " + pojo);
			pojo.setUser(user);
			pojo.setOrganisationId(organisationId);
			return userRoleService.update(pojo);
		}catch(ContracticServiceException ex){
			MessageDO messageDO = ex.getMessageDO();
			if(messageDO == null){
				logger.warn("An error occurred while processing put request", ex);
			}
			else return messageDO;
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing put request", ex);
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{roleId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @PathVariable("organisationId") String organisationId, 
			@PathVariable("roleId") String roleId) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;
			// Enforce security here
			UserRoleDO roleDO = new UserRoleDO();
			roleDO.setId(roleId);
			roleDO.setOrganisationId(organisationId);
			roleDO.setUser(user);
			return userRoleService.delete(roleDO);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
}
