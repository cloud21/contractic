package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.StatusDO;
import contractic.service.domain.StatusService;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{orgId}/status")
public final class StatusController {
	
	private final static Logger logger = Logger.getLogger(StatusController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private StatusService statusService;
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<StatusDO> get(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, 
			@PathParam("statusId") String statusId, 
			@PathParam("moduleId") String moduleId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			Map<String, Object> params = new HashMap<>();
			params.put(StatusService.GET_PARAM_ORGANISATIONID, orgId);
			params.put(StatusService.GET_PARAM_HASH, statusId);
			params.put(StatusService.GET_PARAM_MODULEID, moduleId);
			return statusService.get(params);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	// Create a new status
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, @RequestBody StatusDO pojo) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			pojo.setOrganisationId(orgId);
			return statusService.create(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing post request", ex);
			return new MessageDO(null, "Contractic", "Failed to create status", MessageDO.ERROR);
		}
	}
	
	// Update an existing status
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, @RequestBody StatusDO pojo) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
			
			logger.debug("Received StatusDO " + pojo);
			pojo.setOrganisationId(orgId);
			return statusService.update(pojo);
		}catch(ContracticServiceException ex){
			logger.warn("An error occurred while processing put request", ex);
			MessageDO message = ex.getMessageDO();
			return message;
		}
	}
	
	// Delete an existing status indicator
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{statusId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, @PathVariable("statusId") String statusId) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
					
			// Enforce security here
			StatusDO statusDO = new StatusDO();
			statusDO.setOrganisationId(orgId);
			statusDO.setId(statusId);
			
			return statusService.delete(statusDO);
		}catch(ContracticServiceException ex){
			logger.warn("An error occurred while processing get request", ex);
			MessageDO message = ex.getMessageDO();
			return message;
		}
	}
}
