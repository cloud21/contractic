package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.Note;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.NoteDO;
import contractic.service.domain.NoteService;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{orgId}/note")
public final class NoteController {
	
	private final static Logger logger = Logger.getLogger(NoteController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<Note, Long, NoteDO, MessageDO> noteService;
		
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET , produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<NoteDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("orgId") String orgId,
			@PathParam("noteId") String noteId, 
			@PathParam("externalId") String externalId,
			@PathParam("moduleId") String moduleId) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;

			// Enforce security here
			
			// If noteId hasn't been provided, we need to have both the moduleId and the externalId provided
			if(!StringUtils.hasText(noteId) && (!StringUtils.hasText(moduleId) || !StringUtils.hasText(externalId)))
				return null;
			
			Map<String, Object> params = new HashMap<>();
			params.put(NoteService.GET_PARAM_EXTERNALID, externalId);
			params.put(NoteService.GET_PARAM_MODULEID, moduleId);
			params.put(NoteService.GET_PARAM_HASHID, noteId);
			params.put(CommonService.USER, user);
			return noteService.get(params);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, 
			@RequestBody NoteDO pojo) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;
			
			pojo.setSessionUser(user);
			
			// Enforce security here
			return noteService.create(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing post request", ex);
			return new MessageDO(null, "Contractic", "Failed to create note", MessageDO.ERROR);
		}
	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, @RequestBody NoteDO pojo) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;
			
			pojo.setSessionUser(user);
			
			logger.debug("Received NoteDO " + pojo);
			return noteService.update(pojo);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing put request", ex);
		}
		return null;
	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{noteId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, @PathVariable("orgId") String orgId, @PathVariable("noteId") String noteId) throws Exception{
		
		try {
			UserDO user = sessionService.get(token);
			assert user != null;
			
			// Enforce security here
			NoteDO noteDO = new NoteDO();
			noteDO.setSessionUser(user);
			noteDO.setId(noteId);
			return noteService.delete(noteDO);
		}catch(Throwable ex){
			ex.printStackTrace();
			logger.warn("An error occurred while processing get request", ex);
		}
		return null;
	}
}
