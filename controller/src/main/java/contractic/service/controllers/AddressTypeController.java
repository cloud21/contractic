package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.AddressType;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.SessionDO;
import contractic.service.domain.StaticDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/addresstype")
public final class AddressTypeController {
	
	private static final Logger logger = Logger.getLogger(AddressTypeController.class);
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	
	@Autowired
	private DomainService<AddressType, Long, StaticDO, MessageDO> addressTypeService;
		
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<StaticDO> get(@RequestHeader(value="Authorization") String token) throws Exception{
		
		try {
			UserDO sessionToken = sessionService.get(token);
		} catch (ContracticServiceException e) {
			throw new Exception("Session has expired");
		}
		
		try{
			// Enforce security here
			
			final Map<String, Object> params = new HashMap<>();
			return addressTypeService.get(params);
		}catch(Throwable ex){
			logger.warn(String.format("Failed to retrieve address types for session %s", token), ex);
			ex.printStackTrace();;
		}
		return null;
	}
}
