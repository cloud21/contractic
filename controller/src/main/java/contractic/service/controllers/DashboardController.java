package contractic.service.controllers;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServlet;
import javax.ws.rs.PathParam;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import contractic.model.data.store.Contract;
import contractic.model.data.store.ReportOrganisation;
import contractic.model.data.store.Task;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ReportDO;
import contractic.service.domain.ReportService;
import contractic.service.domain.SessionDO;
import contractic.service.domain.TaskDO;
import contractic.service.domain.TaskService;
import contractic.service.domain.UserDO;



@Controller
@RequestMapping("/organisation/{organisationId}/dashboard")
public class DashboardController extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2196928605480237293L;

	private final static Logger logger = Logger.getLogger(DashboardController.class);

	public static final String REPORT_VIEW_HOME = "report.view.home";
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	@Autowired
	private DomainService<Contract, Long, ContractDO, MessageDO> contractService;
	@Autowired
	private DomainService<Task, Long, TaskDO, MessageDO> taskService;
	@Autowired
	private DomainService<ReportOrganisation, Long, ReportDO, MessageDO> reportService;
	
	private List<ContractDO> makeContracts(Object data, UserDO userDO) throws ContracticServiceException{
		List<?> list = (List<?>)data;
		List<ContracticServiceException> cse = new ArrayList<>();
		List<ContractDO> contractList = list.stream().parallel().map(item -> {
			Map<?, ?> ref = (HashMap<?,?>)item;
			BigInteger contractId = (BigInteger)ref.get("Id");
			final Map<String, Object> params = new HashMap<>();
			params.put(ContractService.GET_PARAM_ID, contractId.longValue());
			params.put(CommonService.USER, userDO);
			try{
				List<ContractDO> cList = contractService.get(params);
				return (cList.size()==1) ? cList.get(0) : null;
			}catch(ContracticServiceException ex){
				cse.add(ex);
				return null;
			}
			
		}).filter(item -> item != null).collect(Collectors.toList());
		
		/*If we have an error in the lambda above, we throw the exception here*/
		if(cse.size()!=0)
			throw cse.get(0);
		return contractList;
	}
	
	private List<TaskDO> makeTasks(Object data, UserDO userDO) throws ContracticServiceException{
		List<?> list = (List<?>)data;
		List<ContracticServiceException> cse = new ArrayList<>();
		List<TaskDO> contractList = list.stream().parallel().map(item -> {
			Map<?, ?> ref = (HashMap<?,?>)item;
			BigInteger contractId = (BigInteger)ref.get("Id");
			final Map<String, Object> params = new HashMap<>();
			params.put(TaskService.GET_PARAM_ID, contractId.longValue());
			params.put(CommonService.USER, userDO);
			try{
				List<TaskDO> cList = taskService.get(params);
				return (cList.size()==1) ? cList.get(0) : null;
			}catch(ContracticServiceException ex){
				cse.add(ex);
				return null;
			}
			
		}).filter(item -> item != null).collect(Collectors.toList());
		
		/*If we have an error in the lambda above, we throw the exception here*/
		if(cse.size()!=0)
			throw cse.get(0);
		return contractList;
	}
	
	@ResponseBody
	@RequestMapping(value="/contract", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ContractDO> getContractPanel(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@RequestParam final MultiValueMap<String, String> reportParams) throws Exception{

		UserDO userDO = sessionService.get(token);
		assert userDO != null;

		final List<String> paramList = new ArrayList<>();
		paramList.add(userDO.getPerson().getId());
		reportParams.put("personId", paramList);
		
		// Enforce security here
		Map<String, Object> params = new HashMap<>();
		params.put(ReportService.GET_PARAM_ORGANISATIONID, organisationId);
		params.put(CommonService.USER, userDO);
		params.put(ReportService.GET_PARAM_EXECUTE, Boolean.TRUE);
		params.put(ReportService.GET_REPORT_PARAMS, reportParams);
		params.put(ReportService.GET_EXTERNAL_REF, "user-contract-activity" /*Parametize me*/);

		List<ReportDO> reportList = reportService.get(params);
		assert reportList.size() == 1;
		
		ReportDO reportDO = reportList.get(0);
		return makeContracts(reportDO.getData(), userDO);
	}

	@ResponseBody
	@RequestMapping(value="/task", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TaskDO> getTaskPanel(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId,
			@RequestParam final MultiValueMap<String, String> reportParams) throws Exception{

		UserDO userDO = sessionService.get(token);
		assert userDO != null;

		final List<String> paramList = new ArrayList<>();
		paramList.add(userDO.getPerson().getId());
		reportParams.put("personId", paramList);
		
		// Enforce security here
		Map<String, Object> params = new HashMap<>();
		params.put(ReportService.GET_PARAM_ORGANISATIONID, organisationId);
		params.put(CommonService.USER, userDO);
		params.put(ReportService.GET_PARAM_EXECUTE, Boolean.TRUE);
		params.put(ReportService.GET_REPORT_PARAMS, reportParams);
		params.put(ReportService.GET_EXTERNAL_REF, "user-task-activity" /*Parametize me*/);

		List<ReportDO> reportList = reportService.get(params);
		assert reportList.size() == 1;
		
		ReportDO reportDO = reportList.get(0);
		return makeTasks(reportDO.getData(), userDO);
	}
	
	@ResponseBody
	@RequestMapping(value="/statistics", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object getStatisticsPanel(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @PathParam("report") String report) throws Exception{

		UserDO userDO = sessionService.get(token);
		assert userDO != null;

		final MultiValueMap<String, String> reportParams = new LinkedMultiValueMap<>();
		final List<String> paramList = new ArrayList<>();
		paramList.add(report);
		reportParams.put("report", paramList);
		
		// Enforce security here
		Map<String, Object> params = new HashMap<>();
		params.put(ReportService.GET_PARAM_ORGANISATIONID, organisationId);
		params.put(CommonService.USER, userDO);
		params.put(ReportService.GET_PARAM_EXECUTE, Boolean.TRUE);
		params.put(ReportService.GET_REPORT_PARAMS, reportParams);
		params.put(ReportService.GET_EXTERNAL_REF, "contract-statistics" /*Parametize me*/);

		List<ReportDO> reportList = reportService.get(params);
		assert reportList.size() == 1;
		
		ReportDO reportDO = reportList.get(0);
		return reportDO.getData();

	}
	
	
}
