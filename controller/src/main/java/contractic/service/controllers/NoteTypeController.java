package contractic.service.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contractic.model.data.store.NoteType;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.NoteTypeDO;
import contractic.service.domain.NoteTypeService;
import contractic.service.domain.SessionDO;
import contractic.service.domain.UserDO;

@RestController
@RequestMapping("/organisation/{organisationId}/notetype")
public final class NoteTypeController {
	
	private static final Logger logger = Logger.getLogger(NoteTypeController.class);
	
	@Autowired
	private DomainService<NoteType, Long, NoteTypeDO, MessageDO> noteTypeService;
	
	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
		
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<NoteTypeDO> get(@RequestHeader(value="Authorization") String token, 
			@PathVariable(value="organisationId") String organisationId) throws Exception{

			UserDO sessionToken = sessionService.get(token);

			final Map<String, Object> params = new HashMap<>();
			params.put(NoteTypeService.GET_PARAM_ORGANISATIONID, organisationId);
			return noteTypeService.get(params);

	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{typeId}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public NoteTypeDO get(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, 
			@PathVariable("typeId") String typeId) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		// Enforce security here
		Map<String, Object> params = new HashMap<>();
		params.put(NoteTypeService.GET_PARAM_HASH, typeId);
		params.put(NoteTypeService.GET_PARAM_ORGANISATIONID, organisationId);
		List<NoteTypeDO> documentTypeList = noteTypeService.get(params);
		if(documentTypeList.size()==1)
			return documentTypeList.get(0);
		throw new RuntimeException("Invalid request");
	}
	
	// Create a new organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO post(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody NoteTypeDO noteTypeDO) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		// Enforce security here
		noteTypeDO.setUser(sessionToken);
		noteTypeDO.setOrganisationId(organisationId);
		return noteTypeService.create(noteTypeDO);

	}
	
	// Update an existing organisation
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(method=RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO put(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organisationId, @RequestBody NoteTypeDO noteTypeDO) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);
		noteTypeDO.setUser(sessionToken);
		noteTypeDO.setOrganisationId(organisationId);
		return noteTypeService.update(noteTypeDO);

	}
	
	@Produces({MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	@RequestMapping(value="/{doctypeId}", method=RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public MessageDO delete(@RequestHeader(value="Authorization") String token, 
			@PathVariable("organisationId") String organistaionId, 
			@PathVariable("doctypeId") String doctypeId) throws Exception{
		
		UserDO sessionToken = sessionService.get(token);

		// Enforce security here
		NoteTypeDO noteTypeDO = new NoteTypeDO();
		noteTypeDO.setUser(sessionToken);
		noteTypeDO.setOrganisationId(organistaionId);
		noteTypeDO.setId(doctypeId);;
		
		return noteTypeService.delete(noteTypeDO);
	}
}
