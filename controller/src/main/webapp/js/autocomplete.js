function autoComplete(selector, dataSource, handler){
	console.log("Setting up autocomplete for " + selector);
    $(selector).autocomplete({
        source: dataSource,
        minLength: 2,
        select: handler,
 
        html: true, // optional (jquery.ui.autocomplete.html.js required)
 
      // optional (if other layers overlap autocomplete list)
        open: function(event, ui) {
            $(".ui-autocomplete").css("z-index", 1000);
        }
    });
}