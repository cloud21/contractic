/**
 * 
 */

$(document).ready(function(){
	
	$('.tab').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('.tab').removeClass('tab-current');
		$('.tab-content').removeClass('current');

		$(this).addClass('tab-current');
		$("#"+tab_id).addClass('current');
	})

})