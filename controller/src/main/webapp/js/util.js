/**
 * Allow for click to select on data-table rows
 */

$(document).ready(function(){
	var selectedRow = null;
	$('.data-table tbody tr').on('click', function() {
		if(selectedRow)
			$(selectedRow).toggleClass('data-table-row-select');
	    $(this).toggleClass('data-table-row-select');
	    selectedRow = this;
	});
});

var __selectedRow = null;
function toggleRowSelection(row){
	console.log("Toggling selected row");
	if(__selectedRow)
		$(__selectedRow).toggleClass('data-table-row-select');
    $(row).toggleClass('data-table-row-select');
    __selectedRow = row;
}

function copyHtml(targetSelector, templateSelector){
	  var template = $(templateSelector).clone().wrap('<p>').parent();
	  $(targetSelector).append(template.html());
}