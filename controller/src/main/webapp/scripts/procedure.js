/**
 * Organisation Procedures
 */

function procedureController($scope, applicationService, confirmService, sessionService, filterFilter){
	
	sessionService.validate();
	
	$scope.AlertType = AlertType;
	$scope.dialog = {show: false, readonly: false};
	$scope.addressTypes = applicationService.get(ADDRESS_TYPES_KEY);
	$scope.alert = {};
	$scope.showConfirm = false;
	$scope.searchProcedure = '';
	$scope.sortField = 'name';
	
	$scope.currentProcedurePage = 0;
    $scope.pageProcedureSize = 10;
    $scope.numberOfPages=function(){
        return $scope.procedures== null ? 0 : Math.ceil($scope.procedures.length/$scope.pageProcedureSize);                
    }
    
    $scope.$watch('searchProcedure', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.procedResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.procedures = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentProcedurePage = 0;
	}, true)
	
	$scope.loadProcedures = function(){
		var groupsets = applicationService.get(SYSTEM_GROUPSET_KEY);
		var proceduregroupset = groupsets["PROCEDURE"];
		$scope.proceduresLoaded = false;
		loadGroups(applicationService, {groupSetId: proceduregroupset.id/*, moduleId: $scope.moduleId*/}, 
				function(response){
			var data = response.data;
			$scope.procedures = data;
			$scope.procedResults = $scope.procedures;
			$scope.proceduresLoaded = true;
		}, function(response){
			
		})
	}
	
	$scope.setSelected = function(item){
		$scope.procedure = item;
	}
	
	$scope.newProcedure = function(){
		$scope.procedureFormSubmitted = false;
		$scope.alert = {};
		$scope.procedure = {groupset: new ValueDO(null, 'PROCEDURE')};
		$scope.action = 'New';
		$scope.dialog.show = true;
	}
	
	$scope.editProcedure = function(){
		//$scope.procedure = {groupset: new ValueDO(null, 'PROCEDURE')};
		$scope.procedureFormSubmitted = false;
		$scope.alert = {};
		$scope.action = 'Edit';
		loadGroups(applicationService, {groupId: $scope.procedure.id}, function(response){
			var data = response.data;
			if(data && data.length == 1){
				$scope.procedure = data[0];
				$scope.dialog.show = true;
			}
		}, function(response){
			if(response.status == 400){
				$scope.alert = {body: response.data.body, type: AlertType.ERROR};
			}
		});
	}
	
	$scope.saveProcedure = function(){
		
		$scope.procedureFormSubmitted = true;
		if($scope.procedureForm.name.$valid){
			
			$scope.procedureFormSubmitted = false;
			
		
		saveGroup(applicationService, $scope.procedure, function(response){
			$scope.alert = response.data;
		}, function(response){
			if(response.status == 400){
				$scope.alert = {body: response.data.body, type: AlertType.ERROR};
			}
		});
		
	}
		else{
			$scope.alert = {body: "Not all required fields for procedure have been filled in", type: AlertType.ERROR};
		}
	}
	
	$scope.deleteProcedure = function(){
		$scope.alert = {};
		if(!$scope.procedure || !$scope.procedure.id)
			return;
		$scope.showConfirm = true;
		var showConfirm = confirmService.getShowConfirm();
		showConfirm({
			message: "Are you sure want to delete the selected procedure",
			title: "Confirm",
			confirm: function(){
				deleteGroup(applicationService, $scope.procedure, function(response){
					$scope.alert = response.data;
					$scope.loadProcedures();
				}, function(response){
					if(response.status == 400){
						$scope.alert = {body: response.data.body, type: AlertType.ERROR};
					}
				});
			}
		});
	}
	
	$scope.closeDialog = function(){
		$scope.dialog.show = false;
		$scope.alert = {};
	}
	
	$scope.loadProcedures();
}

var procedure = angular.module("procedure", [ 'ngRoute' ]);
procedure.controller("procedureController", procedureController);