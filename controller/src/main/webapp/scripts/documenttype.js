/**
 * 
 */

/**
 * Load organisation persons
 * @param $scope
 * @param $http
 */
function loadDocumentTypes(applicationService, params, success, error) {
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false,
		params: params
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/documenttype");
	applicationService.httpget(config, request, success, error);
}

/**
 * Load a single documentType
 * @param $scope
 * @param $http
 */
function loadDocumentType(applicationService, documentType, success, error) {
	if (!documentType.id || documentType.id == '')
		return;
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/documenttype/", documentType.id);
	applicationService.httpget(config, request, success, error);
}

function saveDocumentType(applicationService, documentType, success, error) {

	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, "/documenttype");
	if (documentType.id && documentType.id != '') {
		applicationService.httpput(config, request, documentType, success, error);
	} else {
		applicationService.httppost(config, request, documentType, success, error);
	}
}

function deleteDocumentType(applicationService, documentType, success, error) {

	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	if (documentType.id && documentType.id != '') {
		var request = "".concat("/contractic/organisation/", organisationId, 
				"/documenttype/", documentType.id);
		applicationService.httpdel(config, request, documentType, success, error);
	}
	
}

/**
 * Person controller function
 * @param $scope
 * @param $http
 * @param applicationService
 */
function documentTypeController($scope, applicationService, confirmService, sessionService, filterFilter) {
	
	sessionService.validate();
	
	/*A single documentType being edited or created*/
	$scope.documentType = {};
	/*List of all documentTypes on the system*/
	$scope.documentTypes = [{}];
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);

	$scope.alert = {};
	$scope.go = applicationService.go;
	
	$scope.AlertType = AlertType;
	$scope.dialog = {show: false, readonly: true}
	$scope.showConfirm = false;
	
    $scope.searchDocumentType = '';
	
	$scope.currentDocumentListPage = 0;
    $scope.pageDocumentsSize = 10;
    $scope.numberOfPages=function(){
        return $scope.documentTypes== null ? 0 : Math.ceil($scope.documentTypes.length/$scope.pageDocumentsSize);                
    }
    
    $scope.$watch('searchDocumentType', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.documentTypesResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.documentTypes = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentDocumentListPage = 0;
	}, true)

	$scope.newDocumentType = function(){
		$scope.documentTypeFormSubmitted = false;
		$scope.documentTypeForm.$setPristine();
		$scope.action = "Add New";
		$scope.documentType = {};
		$scope.alert = {};
		$scope.dialog.show = true;
	};
	
	$scope.editDocumentType = function(){
		$scope.documentTypeFormSubmitted = false;
		$scope.documentTypeForm.$setPristine();
		$scope.alert = {};
		$scope.action = "Edit";
		loadDocumentType(applicationService, $scope.documentType, function(response) {
			$scope.documentType = response.data;
			$scope.dialog.show = true;
		}, function(response){
			if(response.status == 400){
				$scope.alert = {body: response.data.body, type: AlertType.ERROR};
			}
		});
	};
	
	$scope.refreshList = function(){
		$scope.documentTypesLoaded = false;
		loadDocumentTypes(applicationService, null, 
				function(response) {
			$scope.documentTypes = response.data;
			$scope.documentTypesResults = $scope.documentTypes;
			$scope.documentTypesLoaded = true;
		}, function(response){
			
		});
	}
	
	$scope.saveDocumentType = function(){
		$scope.documentTypeFormSubmitted = true;
		if($scope.documentTypeForm.documentDescription.$valid && $scope.documentTypeForm.code.$valid) {
			$scope.documentTypeFormSubmitted = false;
			
			$scope.alert = {};
			saveDocumentType(applicationService, $scope.documentType, function(response) {
				$scope.alert = response.data;
			}, function(response){
				if(response.status == 400){
					$scope.alert = {body: response.data.body, type: AlertType.ERROR};
				}
			});
		}
	else{ $scope.alert = {body: "Not all required fields for Document Type have been filled in", type: AlertType.ERROR};  }
	};
	
	$scope.setSelected = function(item){
		$scope.documentType = {id: item.id};
	}
	
	$scope.deleteDocumentType = function(){
		$scope.alert = {};
		if(!$scope.documentType || !$scope.documentType.id)
			return;
		$scope.showConfirm = true;
		var showConfirm = confirmService.getShowConfirm();
		showConfirm({
			message: "Are you sure want to delete the selected document type",
			title: "Confirm",
			confirm: function(){
				deleteDocumentType(applicationService, $scope.documentType, function(response){
					$scope.alert = response.data;
					$scope.refreshList();
				}, function(response){
					if(response.status == 400){
						$scope.alert = {body: response.data.body, type: AlertType.ERROR};
					}
				});
			}
		});
	}
	
	
	$scope.closeDialog = function(){
		$scope.alert = {};
		$scope.dialog.show = false;
		$scope.refreshList();
	}
	
	$scope.refreshList();
}


var documentType = angular.module("documentType", [ 'ngRoute' ]);

/* Add/Edit Controller */
documentType.controller("documentTypeController", documentTypeController);
