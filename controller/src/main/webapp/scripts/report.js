/**
 * 
 */

function loadReports(applicationService, params, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params: params
		};
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/report");
	applicationService.httpget(config, request, success, error);
}

function saveReport(applicationService, report, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/report");
	applicationService.httpput(config, request, report, success, error);
}

function reportViewController($scope, applicationService, reportService, sessionService){
	
	sessionService.validate();
	
	$scope.fetchReport = function(){
		var organisationId = applicationService.get(ORGANISATION_ID_KEY);
		var reportId = reportService.getReportId();
		if(reportId == null){
			return;
		}
		$scope.reportUrl = '/contractic/organisation/' + organisationId +'/view/report?id=' + reportId;
		$scope.showReport = true;
	}
	$scope.fetchReport();
}

function reportController($scope, applicationService, reportService, sessionService) {
	
	sessionService.validate();
	
	$scope.report = {};
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.reportsLoaded = false;
	$scope.showDialog = false;

	$scope.AlertType = {
		OK : 'ok',
		WARNING : 'warning',
		ERROR : 'error',
		INFO : 'info'
	};
	
	$scope.setSelectedReport = function(item){
		$scope.report = item;
	}

	$scope.loadReports = function(){
		$scope.reportsLoaded = false;
		loadReports(applicationService, null, function(response){
			$scope.reports = response.data;
			$scope.reportsLoaded = true;
		}, function(response){
			
		});
	}
	
	$scope.editReport = function(){
		if(!$scope.report || !$scope.report.id)
			return;
		$scope.showDialog = true;
		loadReports(applicationService, {reportId: $scope.report.id}, 
				function(response){
			var data = response.data;
			if(data && data.length == 1)
				$scope.report = data[0];
		}, function(response){
			
		});
	}
	
	$scope.closeDialog = function(){
		$scope.showDialog = false;
		$scope.report = null;
		$scope.alert = null;
		$scope.loadReports();
	}
	
	$scope.saveReport = function(){
		saveReport(applicationService, $scope.report, function(response){
			$scope.alert = response.data;
		}, function(response){
			
		});
	}
	
	$scope.runReport = function(){
		reportService.setReportId($scope.report.id);
		$scope.go('/reports/run');
	}

	$scope.loadReports();
	/*Clear the report id*/
	reportService.setReportId(null);
}


var report = angular.module("report", [ 'ngRoute' ]);

/* Contract Service */
report.service('reportService', ['$location', function($location) {
	var REPORT_ID_KEY = "report.id.key";
	// private variable
	var hashtable = {};
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		go : function(path){
	    	$location.path( path );
		},
		setReportId : function(reportId){
			hashtable[REPORT_ID_KEY] = reportId;
		},
		getReportId : function(){
			return hashtable[REPORT_ID_KEY];
		}
	}
}]);

/* Add/Edit Controller */
report.controller("reportController", reportController);
report.controller("reportViewController", reportViewController);

//configure our routes
report.config(function($routeProvider) {
	$routeProvider

	// route for the home page
	.when('/reports/run', {
		templateUrl : window.viewsBase + '/report-viewer.html'
	});
});
