/**
 * Note management module
 */

function resetSession(applicationService, session, success, error){
	var config = {
			cache : false
		};
	
	var request = "/contractic/session/reset";
	if(session.token && session.token!='')
		applicationService.httpput(config, request, session, success, error);
	else
		applicationService.httppost(config, request, session, success, error);
}

function updateSession(applicationService, session, success, error){
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};
	
	var request = "/contractic/session";
	applicationService.httpput(config, request, session, success, error);
}

function createSession(applicationService, session, success, error){
	var config = {
			cache : false
		};
	
	var request = "/contractic/session";
	applicationService.httppost(config, request, session, success, error);
}

function deleteSession(applicationService, session, success, error){
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};
	
	var request = "/contractic/session";
	applicationService.httpdel(config, request, session, success, error);
}

function sessionController($scope, $routeParams, applicationService, sessionService, searchService){
		
	//applicationService.set(AUTH_KEY, 'd2VudHdvcnRobWFuOkNoYW5nZV9tZQ==');
	$scope.go = applicationService.go;
	
	$scope.AlertType = AlertType;
	$scope.session = {username: sessionService.getUsername(), password: null, password2: null, token: $routeParams.resetkey}
	$scope.resetPassword = false;
	$scope.captureResetPassword = $scope.session.token!=null && $scope.session.token!='';
		
	$scope.organisation = null;
	loadOrganisation(applicationService, null, function(response){
		
		var headers = response.headers();
		$scope.version = headers["version"];
		$scope.environment = headers["environment"];
		
		var data = response.data;
		if(data && data.length == 1){
			$scope.organisation = data[0];
		}
	}, function(response){
		
	});
	
	$scope.executeSearch = function(){
		/*
		 * This line is only useful when the controller is being initialized
		 * Afte rthe controller has been initialized i.e. we are on the search.html page,
		 * the call to $scope.go('/search') has no effect so the search is executed through
		 * searchService.executeSearch(...) function
		 */
		searchService.setQuery($scope.query);
		$scope.go('/search');
		searchService.executeSearch($scope.query);
	}
	
	$scope.resetSession = function(){
		var success = null;
		if($scope.session.token && $scope.session.token!=''){
			success = function(response){
				var data = response.data;
				$scope.resetPassword = false;
				if(data && data.type == AlertType.OK){
					$scope.createSession();
				}else{
					$scope.alert = data;
				}
			}
		}else{
			success = function(response){
				var data = response.data;
				$scope.alert = data;
				$scope.resetPassword = false;
			}			
		}
		resetSession(applicationService, $scope.session, 
				success, 
				function(response) {
						
				});
	}
	
	$scope.signinButtonClick = function(){
		if($scope.resetPassword || $scope.captureResetPassword)
			$scope.resetSession();
		else
			$scope.createSession();
	}
	
	$scope.createSessionKillExisting = function(){
		$scope.session.killExisting = true;
		$scope.createSession();
	}
	
	$scope.createSession = function(){
		$scope.signingIn = true;
		$scope.alert = {}
		if(!$scope.session.username || !$scope.session.password){
			$scope.alert = {body: "Could not create your session, invalid username and password", type: AlertType.ERROR};
			$scope.signingIn = false;
			return;
		}
		
		createSession(applicationService, $scope.session, 
				function(response) {
					var data = response.data;
					if(data.type != AlertType.OK){
						$scope.alert = data;
						$scope.signingIn = false;
						return;
					}
					$scope.session = data.ref;
					applicationService.set(ORGANISATION_ID_KEY, $scope.session.organisationId);
					applicationService.setAuthHeader($scope.session.token);
					sessionService.setUsername($scope.session.username);
					sessionService.setUser($scope.session.user);
					$scope.loadModules(function(response){
						$scope.loadAddressTypes(function(response){
							$scope.loadGroupSets(function(response){
								$scope.loadCurrencies(function(response){
									$routeParams.resetkey = null;
									applicationService.getLocation().search('resetkey', null);
									applicationService.go('/dashboard');
									$scope.signingIn = false;
								});
							});
						});
					});
				}, 
				function(response) {
					var data = response.data;
					$scope.alert = data;
					$scope.signingIn = false;
					$scope.sessionExists = true;
				});
	}
	
	$scope.deleteSession = function(){
		$scope.session = {username: sessionService.getUsername(), token: applicationService.getAuthKey()}
		deleteSession(applicationService, $scope.session, 
				function(response) {
					var data = response.data;
					if(data.type != AlertType.OK){
						$scope.alert = data;
					}
					applicationService.go('/');
					sessionService.clear();
				}, 
				function(response) {
						applicationService.go('/');
				});
	}
	
	$scope.loadAddressTypes = function(callback) {
		var config = {
			headers : applicationService.getAuthHeader()
		};
		applicationService.httpget(config, "/contractic/addresstype", 
				function(response) {
					applicationService.set(ADDRESS_TYPES_KEY, response.data);
					callback(response);
				}, function(response){
					
				});
	}
	
	$scope.loadModules = function(callback) {
		var config = {
			headers : applicationService.getAuthHeader()
		};
		applicationService.httpget(config, "/contractic/module",  
				function(response) {
					var data = response.data;
					var modules = {};
					
					for(var idx = 0; idx<data.length; ++idx){
						applicationService.set(data[idx].name, data[idx].id);
						modules[data[idx].id] = data[idx];
					}
					
					applicationService.set(SYSTEM_MODULE_KEY, modules);
					callback(response);
				}, function(response){
					console.log(JSON.stringify(response));
				});
	}
	
	$scope.loadGroupSets = function (callback) {
		var config = {
			headers : applicationService.getAuthHeader()
		};
		loadGroupSets(applicationService, null, function(response) {
					var data = response.data;
					var modules = {};
					
					for(var idx = 0; idx<data.length; ++idx){
						modules[data[idx].name] = data[idx];
					}
					applicationService.set(SYSTEM_GROUPSET_KEY, modules);
					callback(response);
				}, function(response){
					console.log(JSON.stringify(response));
				});
	}
	
	$scope.loadCurrencies = function (callback) {
		var config = {
			headers : applicationService.getAuthHeader(),
			params: {active: true}
		};
		applicationService.httpget(config, "/contractic/currency",  
				function(response) {
					var data = response.data;
					applicationService.set(SYSTEM_CURRENCYSET_KEY, data);
					callback(response);
				}, function(response){
					console.log(JSON.stringify(response));
				});
	}
}

var session = angular.module("session", [ 'ngRoute' ]);

/* Session Service */
session.service('sessionService', ['applicationService', function(applicationService) {

	// private variable
	var hashtable = {};
	var USER_KEY = "user.key";
	
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		getUsername: function(){
			return hashtable['username'];
		},
		setUsername: function(username){
			hashtable['username'] = username;
		},
		setUser: function(user){
			hashtable[USER_KEY] = user;
		},
		getUser: function(){
			return hashtable[USER_KEY];
		},
		clear: function(){
			hashtable = {};
		},
		validate: function(){
			var user = this.getUser();
			if(!user){
				applicationService.go("/");
			}
		}
	}
}]);


/* Add/Edit Controller */
session.controller("sessionController", sessionController);