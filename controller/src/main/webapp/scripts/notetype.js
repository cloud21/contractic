/**
 * 
 */

/**
 * Load organisation persons
 * @param $scope
 * @param $http
 */
function loadNoteTypes(applicationService, params, success, error) {
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false,
		params: params
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/notetype");
	applicationService.httpget(config, request, success, error);
}

/**
 * Load a single noteType
 * @param $scope
 * @param $http
 */
function loadNoteType(applicationService, typeId, success, error) {
	if (!typeId || typeId == '')
		return;
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/notetype/", typeId);
	applicationService.httpget(config, request, success, error);
}

function saveNoteType(applicationService, noteType, success, error) {

	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, "/notetype");
	if (noteType.id && noteType.id != '') {
		applicationService.httpput(config, request, noteType, success, error);
	} else {
		applicationService.httppost(config, request, noteType, success, error);
	}
}

function deleteNoteType(applicationService, noteType, success, error) {
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	if (noteType.id && noteType.id != '') {
		var request = "".concat("/contractic/organisation/", organisationId, 
				"/notetype/", noteType.id);
		applicationService.httpdel(config, request, noteType, success, error);
	}
	
}

/**
 * Person controller function
 * @param $scope
 * @param $http
 * @param applicationService
 */
function noteTypeController($scope, $http, applicationService, sessionService) {
	
	sessionService.validate();
	
	/*A single noteType being edited or created*/
	$scope.noteType = {};
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);

	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.dialog = {show: false, readonly: true}
	
	$scope.AlertType = AlertType;
	
      $scope.searchNoteType = '';
	
	$scope.currentNoteTypeListPage = 0;
    $scope.pageNoteTypeSize = 10;
    $scope.numberOfPages=function(){
        return $scope.noteTypes== null ? 0 : Math.ceil($scope.noteTypes.length/$scope.pageNoteTypeSize);                
    }

	$scope.newNoteType = function(){
		$scope.noteTypeFormSubmitted = false;
		$scope.noteTypeForm.$setPristine();
		$scope.action = "Add New";
		$scope.noteType = {};
		$scope.alert = {};
		$scope.dialog.show = true;
	};
	
	$scope.editNoteType = function(){
		$scope.noteTypeFormSubmitted = false;
		$scope.noteTypeForm.$setPristine();
		$scope.alert = {};
		$scope.action = "Edit";
		loadNoteType(applicationService, $scope.noteType.id, function(response) {
			$scope.noteType = response.data;
			$scope.dialog.show = true;
		}, function(response){
			if(response.status == 400){
				$scope.alert = {body: response.data.body, type: AlertType.ERROR};
			}
		});
	};
	
	$scope.refreshList = function(){
		$scope.noteTypesLoaded = false;
		loadNoteTypes(applicationService, null, function(response) {
			$scope.noteTypes = response.data;
			$scope.noteTypesLoaded = true;
		}, function(response){
			
		});
	}
	
	$scope.saveNoteType = function(){
		$scope.noteTypeFormSubmitted = true;
		if($scope.noteTypeForm.name.$valid) {
			$scope.noteTypeFormSubmitted = false;
			
		saveNoteType(applicationService, $scope.noteType, function(response) {
			$scope.alert = response.data;
		}, function(response){
			if(response.status == 400){
				$scope.alert = {body: response.data.body, type: AlertType.ERROR};
			}
		});
	}
		else{
			$scope.alert = {body: "Not all required fields for Note Type have been filled in", type: AlertType.ERROR};
		}
	};
	
	$scope.setSelected = function(item){
		$scope.noteType = {id: item.id};
	}
	
	$scope.deleteNoteType = function(){
		deleteNoteType(applicationService, $scope.noteType, function(response) {
			$scope.alert = response.data;
		}, function(response){
			if(response.status == 400){
				$scope.alert = {body: response.data.body, type: AlertType.ERROR};
			}
		});
	}
	
	$scope.closeDialog = function(){
		$scope.alert = {};
		$scope.dialog.show = false;
		$scope.refreshList();
	}
	
	$scope.refreshList();
}


var noteType = angular.module("noteType", [ 'ngRoute' ]);

/* Add/Edit Controller */
noteType.controller("noteTypeController", noteTypeController);
