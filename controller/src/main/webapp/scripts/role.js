
var USER_ID_KEY = "user.role.id";

/**
 * Load organisation user role
 * @param $scope
 * @param $http
 */
function loadUserRoles(applicationService, params, success, error) {

	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false,
		params: params
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, "/userrole");
	applicationService.httpget(config, request, success, error);
}

function loadUserRole(applicationService, roleId, success, error) {
	if (!roleId || roleId == '')
		return;
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/userrole/", roleId);
	applicationService.httpget(config, request, success, error);
}

function saveUserRole(applicationService, userRole, success, error) {
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, "/userrole");
	if (userRole.id && userRole.id != '') {
		applicationService.httpput(config, request,
				userRole, success, error);
	} else {
		applicationService.httppost(config, request,
				userRole, success, error);
	}
}

function getModuleMap(applicationService){
	var modules = {};
	
	var mdl = applicationService.get(SYSTEM_MODULE_KEY);
	var moduleNames = ['Contract', 'Supplier', 'Organisation', 'Person'];
	for(var moduleId in mdl){
		for(var idx in moduleNames){
			var name = mdl[idx];
			if (name == mdl[moduleId].name)
				modules[moduleId] = name;
		}
	}
	return modules;
}

/**
 * UserRole controller function
 * @param $scope
 * @param $http
 * @param applicationService
 */
function userRoleController($scope, $http, applicationService, sessionService) {
	
	sessionService.validate();
	
	$scope.userRole = {};
	$scope.userRoles = [{}];
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.dialog = {show: false, readonly: false};
	$scope.modules = getModuleMap(applicationService);
	$scope.AlertType = AlertType;
	
	loadUserRole(applicationService, $scope.userRole.id, 
			function(response) {
				var userRole = response.data;
				$scope.userRole = userRole;
			}, function(response){
		
	});

	$scope.newUserRole = function(){
		$scope.alert = {};
		$scope.userRole = {};
		
		var userPermissions = [];
		// Set the default module permissions
		for(var moduleId in $scope.modules){
			permissions = {module: {id: moduleId, value: $scope.modules[moduleId]}};
			userPermissions.push(permissions);
		}
		
		$scope.userRole.permission = userPermissions;
		
		$scope.dialog.readonly = false;
		$scope.dialog.show = true;
	};
	
	$scope.closeDialog = function(){
		$scope.alert = {};
		$scope.dialog.show = false;
		$scope.refreshList();
	}
	
	$scope.saveUserRole = function(){
		saveUserRole(applicationService, $scope.userRole, function(response) {
			$scope.alert = response.data;
		}, function(response){
			
		});
	};
	
	$scope.editUserRole = function(){
		if($scope.userRole && $scope.userRole.id && $scope.userRole.id!=""){
			
			var userPermissions = $scope.userRole.permission || [];

			/*Populate any missing module permissions*/
			
			for(var moduleId in $scope.modules){
				var permissions = null;
				for(var idx in $scope.userRole.permission){
					var permission = $scope.userRole.permission[idx];
					if(permission.module && permission.module.id && permission.module.id === moduleId)
						permissions = permission;
				}
				if(permissions===null){
					permissions = {module: {id: moduleId, value: $scope.modules[moduleId]}};
					userPermissions.push(permissions);
				}
			}
			
			$scope.userRole.permission = userPermissions;
			
			$scope.dialog.show = true;
			$scope.dialog.readonly = false;
			
		}
	};
	
	$scope.refreshList = function(){
		$scope.usersLoaded = false;
		loadUserRoles(applicationService, null, function(response) {
			$scope.userRoles = response.data;
			$scope.userRolesLoaded = true;
		}, function(response){
			
		});
	}
	
	$scope.setSelected = function(userRole){
		$scope.userRole = userRole;
	}
	$scope.refreshList();
}

var userRole = angular.module("userRole", [ 'ngRoute' ]);

/* Add/Edit Controller */
userRole.controller("userRoleController", userRoleController);