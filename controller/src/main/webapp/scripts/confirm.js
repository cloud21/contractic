/**
 * 
 */
function confirmController($scope, confirmService){
	$scope.properties = {message: null, title: null};
	$scope.showConfirm = false;
	
	confirmService.reset();
	$scope.closeConfirm = function(){
		$scope.properties = {message: null, title: null};
		$scope.showConfirm = false;
	}
	
	$scope.confirm = function(){
		if($scope.properties.confirm){
			try{
				$scope.properties.confirm();
			}catch(err){
				console.log(err.message);
			}
		}
		$scope.closeConfirm();
	}
	
	$scope.showConfirm = function(properties){
		$scope.properties = properties;
		$scope.showConfirm = true;
	}
	
	confirmService.setShowConfirm($scope.showConfirm);
}

var confirm = angular.module("confirm", [ 'ngRoute' ]);

/* Add/Edit Controller */
confirm.controller("confirmController", confirmController);

confirm.service('confirmService', function() {
	var SHOW_CONFIRM = "show.confirm";
	var hashtable = {};
		
	return {
		setShowConfirm : function(showConfirm){
			hashtable[SHOW_CONFIRM] = showConfirm;
		},
		getShowConfirm : function(){
			return hashtable[SHOW_CONFIRM];
		},
		reset : function(){
			hashtable = {};
		}
	}
});