/**
 * 
 */

var ORGANISATION_ID_KEY = "organisation.id";
var ADDRESS_TYPES_KEY = "address.types";

var PERSON_MODULE = 'Person';
var SUPPLIER_MODULE = 'Supplier';
var SYSTEM_MODULE_KEY = "system.modules";
var SYSTEM_GROUPSET_KEY = "application.groupset";
var SYSTEM_CURRENCYSET_KEY = "application.currencyset";

var AlertType = {
		OK : 'OK',
		WARNING : 'WARNING',
		ERROR : 'ERROR',
		INFO : 'INFO'
	};

var app = angular.module("contractic", [ 'ngRoute', 
                                         'contract', 
                                         'document', 
                                         'note',
                                         'audit',
                                         'contact',
                                         'address',
                                         'task',
                                         'report',
                                         'profile',
                                         'person',
                                         'supplier',
                                         'user',
                                         'documentType',
                                         'contractType',
                                         'noteType',
                                         'eventType',
                                         'addressType',
                                         'statusIndicator',
                                         'session',
                                         'userRole',
                                         'department',
                                         'confirm',
                                         'dashboard',
                                         'procedure',
                                         'contractReferenceFormat',
                                         'search',
                                         'ngFileUpload']);

// http://www.abequar.net/posts/jquery-ui-datepicker-with-angularjs
app.directive('datepicker', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
            	//var params = $.parseJSON(attrs["datepicker"]);
            	
                element.datepicker({
                    dateFormat:attrs['dateformat'] ? attrs['dateformat'] : 'dd/mm/yy',
                    onSelect:function (date) {
                        scope.$apply(function () {
                        	if(attrs['multipledate']==='true')
                        		ngModelCtrl.$setViewValue(ngModelCtrl.$viewValue + (ngModelCtrl.$viewValue ? "," : "") + date);
                        	else
                        		ngModelCtrl.$setViewValue(date);
                        });
                    }
                });
            });
        }
    }
});



/**
 * Filter for range functionality mainly used on <select/>s
 */
app.filter('range', function() {
	  return function(input, min, max) {
	    min = parseInt(min); //Make string input int
	    max = parseInt(max);
	    for (var i=min; i<max; i++)
	      input.push(i);
	    return input;
	  };
	});

// Simple filter functionality
app.filter('startFrom', function() {
    return function(input, start) {
    	if (!input || !input.length) { return; }
        start = +start; //parse to int
        return input.slice(start);
    }
});

/*
 * Filter to display number of bytes
 * Ref: https://bitcalm.com/blog/useful-guidelines-and-filters-for-angularjs/
 */ 

app.filter('bytes', function() {
    return function(bytes) {
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes) || bytes == 0) return '0';
        var units = {1: 'KB', 2: 'MB', 3: 'GB', 4: 'TB'},
            measure, floor, precision;
        if (bytes > 1099511627775) {
            measure = 4;
        } else if (bytes > 1048575999 && bytes <= 1099511627775) {
            measure = 3;
        } else if (bytes > 1024000 && bytes <= 1048575999) {
            measure = 2;
        } else if (bytes <= 1024000) {
            measure = 1;
        }
        floor = Math.floor(bytes / Math.pow(1024, measure)).toString().length;
        if (floor > 3) {
            precision = 0
        } else {
            precision = 3 - floor;
        }
        return (bytes / Math.pow(1024, measure)).toFixed(precision) + units[measure];
    }
});



function handleError500($location, response){
	var status = response.status;
	if(status == 500){
		$location.path('/error');
	}
}

/* Application Service */
app.service('applicationService', ['$location', '$http', function($location, $http) {

	var AUTH_KEY = "authkey";
	// private variable
	var hashtable = {};
	
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		getLocation : function(){
			return $location;
		},
		go : function(path){
	    	$location.path( path );
		},
		setAuthHeader : function(authkey){
			hashtable[AUTH_KEY] = authkey;
		},
		getAuthKey : function(){
			return hashtable[AUTH_KEY];
		},
		getAuthHeader : function(){
			var authkey = hashtable[AUTH_KEY];
			return {
				'Authorization' : authkey,
				'Content-Type' : 'application/json; charset=UTF-8',
				'Accept' : 'application/json;'
			}
		},
		
		httpget : function(config, request, success, error){
			if(this.get(ORGANISATION_ID_KEY) == undefined){
				this.go("/");
			}
			if(config && config.cache==false){
				request = "".concat(request, "?time=", new Date().getTime());
			}
			$http.get(request, config)
				.then(success, function(response){
					
					handleError500($location, response);
					
					var status = response.status;
					if(status == 400){
						this.go("/");
					}else{
						error(response);
					}
				});
		},
		
		httpput : function(config, request, content, success, error){
			$http.put(request, content, config)
				.then(success, function(response){
					handleError500($location, response);
					error(response);
				});
		},
		
		httppost : function(config, request, content, success, error){
			$http.post(request, content, config)
			.then(success, function(response){
				handleError500($location, response);
				error(response);
			});
		},
		
		httpdel : function(config, request, content, success, error){
			var delconfig = {};
			for (var i in config)
				delconfig[i] = config[i];
			delconfig['method'] = 'DELETE';
			delconfig['url'] = request;
			delconfig['data'] = content;
			$http(delconfig)
			.then(success, function(response){
				handleError500($location, response);
				error(response);
			});
		}
	}
}]);

app.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }]);

/*app.service('Upload', ['ngFileUpload']);*/

/*app.service('fileUpload', ['applicationService', 'Upload', function (applicationService, Upload) {
    this.uploadFileToUrl = function(file, uploadUrl, success, error){
       var fd = new FormData();
       fd.append('file', file);
       var config = {
    	          transformRequest: angular.identity,
    	          headers: {'Content-Type': undefined, 'Authorization' : 'Basic ' + applicationService.get('authkey')}
    	       };
       
       $http.post(uploadUrl, fd, config)
    
       .success(success)
    
       .error(error);
    }
 }]);*/

function loadOrganisation(applicationService, params, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params: params
		};

	var request = "".concat("/contractic/organisation");
	applicationService.httpget(config, request, success, error);
}

/* Person Controller - This is only here for testing purposes (getting the orgId), should sit a level above this e.g. index.html*/
app.controller("applicationController", function($scope, applicationService) {

	$scope.go = applicationService.go;

	
});

app.controller("adminController", function($scope, $http, applicationService) {
	$scope.go = applicationService.go;
});


// configure our routes
app.config(function($routeProvider, $controllerProvider) {
	
	// Will be used to lazily load report controllers
	window.registerController = $controllerProvider.register;
	
	window.viewsBase = "views";
	
	if(window.IE!=null && window.IE == 8)
		window.viewsBase = "viewsie8";
	
	$routeProvider

	.when('/', {
		templateUrl : viewsBase + '/index.html'
	})
	
	// route for the home page
	.when('/?resetkey=:resetkey', {
		templateUrl : window.viewsBase + '/index.html'
	})

	// route for the about page
	.when('/passreset', {
		templateUrl : window.viewsBase + '/passreset.html'
	})
	
	// route for the dashboard page
	.when('/dashboard', {
		templateUrl : window.viewsBase + '/dashboard.html'
	})
	
	// route for the contract manager page
	.when('/contracts', {
		templateUrl : window.viewsBase + '/contract-manager.html'
	})
	
	// route for the home page
	.when('/contract/new', {
		templateUrl : window.viewsBase + '/contract-add-edit.html'
	})

	// route for the about page
	.when('/contract/edit', {
		templateUrl : window.viewsBase + '/contract-add-edit.html'
	})
	
	// route for the task manager page
	.when('/tasks', {
		templateUrl : window.viewsBase + '/task-manager.html'
	})
	
	// route for the home page
	.when('/task/new', {
		templateUrl : window.viewsBase + '/task-add-edit.html'
	})

	// route for the about page
	.when('/task/edit', {
		templateUrl : window.viewsBase + '/task-add-edit.html'
	})
	
	// route for the reports page
	.when('/reports', {
		templateUrl : window.viewsBase + '/reports.html'
	})
	
	// route for the document manager page
	.when('/documents', {
		templateUrl : window.viewsBase + '/document-manager.html'
	})
	
	// route for the admin page
	.when('/admin', {
		templateUrl : window.viewsBase + '/administration.html'
	})

	// route for the admin page
	.when('/person', {
		templateUrl : window.viewsBase + '/admin-person-list.html'
	})
	
	// route for the home page
	.when('/person/new', {
		templateUrl : window.viewsBase + '/admin-person-add-edit.html'
	})

	// route for the about page
	.when('/person/edit', {
		templateUrl : window.viewsBase + '/admin-person-add-edit.html'
	})
	
	// route for the admin page
	.when('/supplier', {
		templateUrl : window.viewsBase + '/admin-supplier-list.html'
	})
	
	// route for the home page
	.when('/supplier/new', {
		templateUrl : window.viewsBase + '/admin-supplier-add-edit.html'
	})

	// route for the about page
	.when('/supplier/edit', {
		templateUrl : window.viewsBase + '/admin-supplier-add-edit.html'
	})
	
	.when('/role', {
		templateUrl : window.viewsBase + '/admin-userrole-manager.html'
	})
	
	// route for the admin page
	.when('/user', {
		templateUrl : window.viewsBase + '/admin-user-list.html'
	})
	
	// route for the admin page
	.when('/documenttype', {
		templateUrl : window.viewsBase + '/admin-documenttype-list.html'
	})
	
	// route for the admin page
	.when('/contracttype', {
		templateUrl : window.viewsBase + '/admin-contracttype-list.html'
	})
		
	// route for the admin page
	.when('/contractreferenceformat', {
		templateUrl : window.viewsBase + '/admin-contractreferenceformat.html'
	})
	
	// route for the admin page
	.when('/eventtype', {
		templateUrl : window.viewsBase + '/admin-eventtype-list.html'
	})
		
	// route for the admin page
	.when('/addresstype', {
		templateUrl : window.viewsBase + '/admin-addresstype-list.html'
	})
		
	// route for the admin page
	.when('/documentgroup', {
		templateUrl : window.viewsBase + '/admin-documentgroup-list.html'
	})
		
	// route for the admin page
	.when('/notetype', {
		templateUrl : window.viewsBase + '/admin-notetype-list.html'
	})
		
	// route for the admin page
	.when('/contractgroup', {
		templateUrl : window.viewsBase + '/admin-contractgroup-list.html'
	})
		
	// route for the admin page
	.when('/status', {
		templateUrl : window.viewsBase + '/admin-status-list.html'
	})
	
	// route for the admin page
	.when('/department', {
		templateUrl : window.viewsBase + '/admin-department-manager.html'
	})
	
	.when('/procedure', {
		templateUrl : window.viewsBase + '/admin-procedure-manager.html'
	})
	
	.when('/reports', {
		templateUrl : window.viewsBase + '/report-manager.html'
	})
	
	// route for the my-account page
	.when('/profile', {
		templateUrl : window.viewsBase + '/my-account.html'
	})
	
	.when('/help', {
		templateUrl : window.viewsBase + '/help/help-page.html'
	})
	
	.when('/error', {
		controller : 'applicationController',
		templateUrl : window.viewsBase + '/error.html'
	})
	
	.when('/search', {
		templateUrl : window.viewsBase + '/search.html'
	});
});