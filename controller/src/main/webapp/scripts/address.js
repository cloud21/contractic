/**
 * 
 */

function autoCompleteAddress(inputSelector, onselect){
	autoComplete(inputSelector, 
			function(request, response){
				var config = {
						headers : applicationService.getAuthHeader(),
						cache : false,
						params: {q : request.term, autoComplete : true}
					};
		
					var organisationId = applicationService.get(ORGANISATION_ID_KEY);
					$http.get("".concat("/contractic/organisation/", organisationId, "/search/person"), config)
						.success(function(data) {
							response(data);
							});
			}, 
			function(event, ui){
				onselect(event, ui);
			});
}

var address = angular.module("address", [ 'ngRoute' ]);

/* Contract Service */
address.service('addressService', ['$location', function($location) {

	// private variable
	var hashtable = {};
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		
		go : function(path){
	    	$location.path( path );
		},
		
		createNew : function(){
			return {
				typeId: null,
				value: null,
				id: null,
				externalId: null,
				moduleId: null
			}
		}
	}
}]);