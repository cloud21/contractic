/**
 * Search Management
 */
function loadSearch(applicationService, params, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params: params
		};
	
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/search");
	applicationService.httpget(config, request, success, error);
}

function saveSearch(applicationService, search, success, error){
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var endpoint = "".concat("/contractic/organisation/", organisationId, "/search");
	if (search.id && search.id != '') {
		applicationService.httpput(config, endpoint, search, success, error);
	} else {
		applicationService.httppost(config, endpoint, search, success, error);
	}
}

function deleteSearch(applicationService, search, success, error) {

	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	if (search.id && search.id != '') {
		var request = "".concat("/contractic/organisation/", organisationId, 
				"/search/", search.id);
		applicationService.httpdel(config, request, search, success, error);
	}
}

/**
 * Person controller function
 * @param $scope
 * @param $http
 * @param applicationService
 */
function searchController($scope, applicationService, sessionService, searchService, 
		personService, contractService, supplierService, taskService) {
	
	sessionService.validate();

	$scope.alert = {};
	$scope.go = applicationService.go;

	$scope.AlertType = AlertType;
	
	$scope.currentResultPage = 0;
    $scope.pageResultSize = 10;
    $scope.numberOfPages=function(){
        return $scope.results== null ? 0 : Math.ceil($scope.results.length/$scope.pageResultSize);                
    }

    $scope.executeSearch = function(query){
    	$scope.query = query;
		$scope.resultsLoaded = false;
		loadSearch(applicationService, {q: query, collection: "Contract,Person,Supplier,Task,Note,Document,ContractDetail"}, function(response) {
			var data = response.data;
			$scope.results = data;
			$scope.resultsLoaded = true;
		}, function(response){
			
		});
	}
    
    
    $scope.openTarget = function(term){
    	if(term.module.name == 'Person'){
			personService.set(PERSON_ID_KEY, term.external.id);
			applicationService.go('/person/edit');
    	}else if(term.module.name == 'Supplier'){
			supplierService.set(SUPPLIER_ID_KEY, term.external.id);
			$scope.go('/supplier/edit');
    	}else if(term.module.name == 'Contract'){
			contractService.set(CONTRACT_ID_KEY, term.external.id);
			applicationService.go('/contract/edit');
    	}else if(term.module.name == 'Task'){
			taskService.set(TASK_ID_KEY, term.external.id);
			applicationService.go('/task/edit');
    	}
    }
    
    searchService.setSearchFunction($scope.executeSearch);
    
    /*
     * Since we load the search page from the sessionController in the main-app-header.html template,
     * this method initializes the search the first time it is executed. Subsequent calls from the 
     * sessionService will find the controller initialized and call searchService.executeSearch(...)
     */
    $scope.executeSearch(searchService.getQuery());
}

var search = angular.module("search", [ 'ngRoute' ]);

/* Search Service */
search.service('searchService', ['applicationService', function(applicationService) {

	// private variable
	var hashtable = {};
	var SEARCH_QUERY_KEY = "search.query.key";
	var SEARCH_FUNCTION = "search.function";
	
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		getQuery : function(){
			return hashtable[SEARCH_QUERY_KEY];
		},
		setQuery : function(query){
			hashtable[SEARCH_QUERY_KEY] = query;
		},
		setSearchFunction : function(searchFunction){
			hashtable[SEARCH_FUNCTION] = searchFunction;
		},
		executeSearch: function(query){
			var searchFunction = hashtable[SEARCH_FUNCTION];
			if(searchFunction != null)
				searchFunction(query);
		}
	}
}]);

/* Add/Edit Controller */
search.controller("searchController", searchController);