/**
 * 
 */

/**
 * Load organisation persons
 * @param $scope
 * @param $http
 */
function loadEventTypes($scope, $http) {
	console.log("Loading event types...");
}

function saveEventType($scope) {

	var contacts = $('input[id=contact]');
	var contactType = $('select[id=contactType]');
	var contactId = $('input[id=contactId]');
	
	$scope.person.contact.length = 0;
	for (var idx = 0; idx < contacts.length; ++idx) {
		var contact = {
			'typeId' : $(contactType[idx]).val(),
			'value' : $(contacts[idx]).val(),
			'id' : $(contactId[idx]).val()
		};
		$scope.person.contact.push(contact);
	}

	var config = {
		headers : {
			'Authorization' : 'Basic d2VudHdvcnRobWFuOkNoYW5nZV9tZQ==',
			'Content-Type' : 'application/json; charset=UTF-8',
			'Accept' : 'application/json;'
		}
	};

	
	
	if ($scope.person.id && $scope.person.id != '') {
		$http.put(
				"".concat("/contractic/organisation/",
						$scope.organisationId, "/person"), $scope.person,
				config).success(function(data) {
			$scope.alert = data;
		});
	} else {
		$http.post("".concat("/contractic/organisation/", $scope.organisationId, "/person"),
				$scope.person, config).success(
				function(data) {
					$scope.alert = data;
				});
	}
}

/**
 * Person controller function
 * @param $scope
 * @param $http
 * @param applicationService
 */
function eventTypeController($scope, $http, applicationService) {
	$scope.person = {};
	$scope.person.contact = [{}];
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	
	$scope.addressTypes = [{id : "email", value: "Email"}, {id : "postal", value: "Postal"}];
	
	$scope.AlertType = {
		OK : 'ok',
		WARNING : 'warning',
		ERROR : 'error',
		INFO : 'info'
	};

	loadEventTypes($scope, $http);

	$scope.newEventType = function(){
		$scope.person = {};
		$scope.go('/person/new');
	};
	
	$scope.savePerson = function(){
		savePerson($scope);
	};

	
}


var eventType = angular.module("eventType", [ 'ngRoute' ]);

/* Person Service */
eventType.service('personService', ['$location', function($location) {

	// private variable
	var hashtable = {};
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		
		go : function(path){
	    	$location.path( path );
		}
	}
}]);

/* Add/Edit Controller */
eventType.controller("eventTypeController", eventTypeController);

// configure our routes
contract.config(function($routeProvider) {
	$routeProvider

	// route for the home page
	.when('/person/new', {
		templateUrl : window.viewsBase + '/admin-person-add-edit.html'
	})

	// route for the about page
	.when('/person/edit', {
		templateUrl : window.viewsBase + '/admin-person-add-edit.html'
	});
});