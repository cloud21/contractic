/**
 * 
 */

/**
 * Load organisation persons
 * @param $scope
 * @param $http
 */
function loadAddressTypes($scope, $http) {
	console.log("Loading address types...");
}

/**
 * Person controller function
 * @param $scope
 * @param $http
 * @param applicationService
 */
function addressTypeController($scope, $http, applicationService) {
	$scope.person = {};
	$scope.person.contact = [{}];
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	
	$scope.addressTypes = [{id : "email", value: "Email"}, {id : "postal", value: "Postal"}];
	
	$scope.AlertType = {
		OK : 'ok',
		WARNING : 'warning',
		ERROR : 'error',
		INFO : 'info'
	};

	loadAddressTypes($scope, $http);

	$scope.newAddressType = function(){
		$scope.person = {};
		$scope.go('/person/new');
	};
	
	$scope.savePerson = function(){
		savePerson($scope);
	};

	
}


var addressType = angular.module("addressType", [ 'ngRoute' ]);

/* Person Service */
addressType.service('personService', ['$location', function($location) {

	// private variable
	var hashtable = {};
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		
		go : function(path){
	    	$location.path( path );
		}
	}
}]);

/* Add/Edit Controller */
addressType.controller("addressTypeController", addressTypeController);

// configure our routes
contract.config(function($routeProvider) {
	$routeProvider

	// route for the home page
	.when('/person/new', {
		templateUrl : window.viewsBase + '/admin-person-add-edit.html'
	})

	// route for the about page
	.when('/person/edit', {
		templateUrl : window.viewsBase + '/admin-person-add-edit.html'
	});
});