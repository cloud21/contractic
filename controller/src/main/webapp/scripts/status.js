/**
 * 
 */

/**
 * Load organisation persons
 * @param $scope
 * @param $http
 */
function loadStatuses(applicationService, params, success, error) {
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false,
		params: params
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", 
			organisationId, "/status");
	applicationService.httpget(config, request, success, error);
}


function saveStatus(applicationService, status, success, error) {
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var endpoint = "".concat("/contractic/organisation/", organisationId, "/status");
	
	if (status.id && status.id != '') {
		applicationService.httpput(config, endpoint, status, success, error);
	} else {
		applicationService.httppost(config, endpoint, status, success, error);
	}
}

function deleteStatus(applicationService, status, success, error) {

	if (!status.id && status.id == '')
		return;
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/status/", status.id);
	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};
	applicationService.httpdel(config, request, content, success, error);
}

/**
 * Person controller function
 * @param $scope
 * @param $http
 * @param applicationService
 */
function statusIndicatorController($scope, $http, applicationService, confirmService, sessionService, filterFilter) {
	
	sessionService.validate();
	
	/*A single status being edited or created*/
	$scope.status = {};
	/*List of all statuses on the system*/
	$scope.statuses = [{}];
	/*When multiple selections are made using the checkboxes*/
	$scope.selection = [];
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.modules = applicationService.get(SYSTEM_MODULE_KEY);
	$scope.showConfirm = false;
	
	$scope.alert = {};
	$scope.go = applicationService.go;
	
	
	$scope.AlertType = AlertType;
	
     $scope.searchStatus = '';
	
	$scope.currentStatusListPage = 0;
    $scope.pageStatusTypeSize = 10;
    $scope.numberOfPages=function(){
        return $scope.statuses== null ? 0 : Math.ceil($scope.statuses.length/$scope.pageStatusTypeSize);                
    }
    
    $scope.$watch('searchStatus', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.statusResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.statuses = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentStatusListPage = 0;
	}, true)

	$scope.loadStatuses = function(){
		$scope.statusesLoaded = false;
		loadStatuses(applicationService, null, function(response) {
			$scope.statuses = response.data;
			$scope.statusResults = $scope.statuses;
			$scope.statusesLoaded = true;
		}, function(){
			
		});
	}

	$scope.newStatus = function(){
		
		$scope.statusFormsubmitted = false;
		$scope.statusForm.$setPristine();
		
		$scope.action = "Add New";
		$scope.status = {};
		$scope.alert = {};
		$scope.showDialog = true;
	};
	
	$scope.editStatus = function(){
		
		$scope.statusFormsubmitted = false;
		$scope.statusForm.$setPristine();
		$scope.alert = {};
		
		if(!$scope.status.id || $scope.status.id == ''){
			return;
		}
		
		$scope.action = "Edit";
		loadStatuses(applicationService, {statusId : $scope.status.id}, function(response) {
			var data = response.data;
			if(data && data.length==1){
				$scope.status = data[0];
				$scope.status.active = $scope.status.active === 'true';
				$scope.showDialog = true;
			}
		}, function(){
			
		});
	};
	
	$scope.refreshList = function(){
		$scope.loadStatuses();
	}
	
	$scope.saveStatus = function(){
		$scope.statusFormsubmitted = true;
		if($scope.statusForm.name.$valid && $scope.statusForm.moduleId.$valid) {
			$scope.statusFormsubmitted = false;
			
		
		saveStatus(applicationService, $scope.status, function(response) {
			$scope.alert = response.data;
		}, function(response) {
			
		});
	}
		else { $scope.alert = {body: "Not all required fields for new Status have been filled in", type: AlertType.ERROR}; }
	}
	
	$scope.setSelected = function(id){
		$scope.status = {id: id};
	}
	
	$scope.deleteStatus = function(){
		if(!$scope.status || !$scope.status.id)
			return;
		$scope.showConfirm = true;
		var showConfirm = confirmService.getShowConfirm();
		showConfirm({
			message: "Are you sure want to delete the selected status",
			title: "Confirm",
			confirm: function(){
				deleteStatus(applicationService, $scope.status, function(response){
					$scope.alert = response.data;
					$scope.loadStatuses();
				}, function(response){
					
				});
			}
		});
	}
	
	$scope.closeDialog = function(){
		$scope.showDialog = false;
		$scope.status = {};
		$scope.alert = {};
		$scope.loadStatuses();
	}
	
	$scope.loadStatuses();
}


var statusIndicator = angular.module("statusIndicator", [ 'ngRoute' ]);

/* Add/Edit Controller */
statusIndicator.controller("statusIndicatorController", statusIndicatorController);
