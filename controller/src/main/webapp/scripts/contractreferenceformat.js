/**
 * 
 */

/**
 * Load organisation persons
 * @param $scope
 * @param $http
 */
function loadContractReferenceFormats(applicationService, params, success, error) {
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false,
		params: params
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/contractreferenceformat");
	applicationService.httpget(config, request, success, error);
}

/**
 * Load a single contractReferenceFormat
 * @param $scope
 * @param $http
 */
function loadContractReferenceFormat(applicationService, formatId, success, error) {
	if (!formatId || formatId == '')
		return;
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/contractreferenceformat/", formatId);
	applicationService.httpget(config, request, success, error);
}

function saveContractReferenceFormat(applicationService, contractReferenceFormat, success, error) {

	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	if (contractReferenceFormat.id && contractReferenceFormat.id != '') {
		applicationService.httpput(config, "".concat("/contractic/organisation/",
				organisationId, "/contractreferenceformat"), contractReferenceFormat, success, error);
	} else {
		applicationService.httppost(config, "".concat("/contractic/organisation/",
				organisationId, "/contractreferenceformat"), contractReferenceFormat, success, error);
	}
}

function deleteContractReferenceFormat(applicationService, contractReferenceFormat, success, error) {

	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	if (contractReferenceFormat.id && contractReferenceFormat.id != '') {
		var request = "".concat("/contractic/organisation/", organisationId, 
				"/contractreferenceformat/", contractReferenceFormat.id);
		applicationService.httpdel(config, request, contractReferenceFormat, success, error);
	}
}

/**
 * Person controller function
 * @param $scope
 * @param $http
 * @param applicationService
 */
function contractReferenceFormatController($scope, applicationService, confirmService, sessionService, filterFilter) {
	
	sessionService.validate();
	
	/*A single contractReferenceFormat being edited or created*/
	$scope.contractReferenceFormat = {};
	/*List of all contractReferenceFormats on the system*/
	$scope.contractReferenceFormats = [{}];

	$scope.alert = {};
	$scope.go = applicationService.go;
	
	$scope.AlertType = AlertType;
	$scope.dialog = {show: false, readonly: true}
	$scope.showConfirm = false;
	
	$scope.searchContractList = '';
	
	$scope.currentContractListPage = 0;
    $scope.pageContractsSize = 10;
    $scope.numberOfPages=function(){
        return $scope.contractReferenceFormats== null ? 0 : Math.ceil($scope.contractReferenceFormats.length/$scope.pageContractsSize);                
    }
    
    $scope.$watch('searchContractList', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.conRefResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.contractReferenceFormats = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentContractListPage = 0;
	}, true)

	$scope.newContractReferenceFormat = function(){
		$scope.contractReferenceFormatFormsubmitted = false;
		$scope.contractReferenceFormatForm.$setPristine();
		$scope.action = "Add New";
		$scope.contractReferenceFormat = {};
		$scope.alert = {};
		$scope.dialog.show = true;
	};
	
	$scope.editContractReferenceFormat = function(){
		$scope.contractReferenceFormatFormsubmitted = false;
		$scope.contractReferenceFormatForm.$setPristine();
		$scope.alert = {};
		$scope.action = "Edit";
		loadContractReferenceFormat(applicationService, $scope.contractReferenceFormat.id, 
				function(response) {
					$scope.contractReferenceFormat = response.data;
					$scope.dialog.show = true;
				}, 
				function(response){
			
		});
	};
	
	$scope.refreshList = function(){
		$scope.contractReferenceFormatsLoaded = false;
		loadContractReferenceFormats(applicationService, null, function(response) {
			$scope.contractReferenceFormats = response.data;
			$scope.conRefResults = $scope.contractReferenceFormats;
			$scope.contractReferenceFormatsLoaded = true;
		}, function(response){
			
		});
	}
	
	$scope.saveContractReferenceFormat = function(){
		
		$scope.contractReferenceFormatFormsubmitted = true;
		if($scope.contractReferenceFormatForm.contract_name.$valid && $scope.contractReferenceFormatForm.contractActive.$valid) {
			$scope.contractReferenceFormatFormsubmitted = false;
			
			saveContractReferenceFormat(applicationService, $scope.contractReferenceFormat, function(response){
				$scope.alert = response.data;
			}, function(response){
				if(response.status == 400){
					$scope.alert = {body: response.data.body, type: AlertType.ERROR};
				}
			});
		}else {
			$scope.alert = {body: "Not all required fields for Contract Type have been filled in", type: AlertType.ERROR};
		}
	};
	
	$scope.setSelected = function(item){
		$scope.contractReferenceFormat = {id: item.id};
	}
	
	$scope.deleteContractReferenceFormat = function(){
		$scope.alert = {};
		if(!$scope.contractReferenceFormat || !$scope.contractReferenceFormat.id)
			return;
		$scope.showConfirm = true;
		var showConfirm = confirmService.getShowConfirm();
		showConfirm({
			message: "Are you sure want to delete the selected contract reference format",
			title: "Confirm",
			confirm: function(){
				deleteContractReferenceFormat(applicationService, $scope.contractReferenceFormat, function(response){
					$scope.alert = response.data;
					$scope.refreshList();
				}, function(response){
					
				});
			}
		});
	}
	
	$scope.closeDialog = function(){
		$scope.alert = {};
		$scope.dialog.show = false;
		$scope.refreshList();
	}
	
	$scope.refreshList();
	loadContractTypes(applicationService, null, function(response) {
		$scope.contractTypes = response.data;
	}, function(response) {

	});
}


var contractReferenceFormat = angular.module("contractReferenceFormat", [ 'ngRoute' ]);

/* Add/Edit Controller */
contractReferenceFormat.controller("contractReferenceFormatController", contractReferenceFormatController);
