
var TASK_ID_KEY = "task.id.key";


/**
 * Utilities
 * @param applicationService
 * @param params
 * @param success
 * @param error
 */
function loadTasks(applicationService, params, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params: params
		};
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/task");
	applicationService.httpget(config, request, success, error);
}

function saveTask(applicationService, task, success, error){
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var endpoint = "".concat("/contractic/organisation/", organisationId, "/task");
	
	if (task.id && task.id != '') {
		applicationService.httpput(config, endpoint, task, success, error);
	} else {
		applicationService.httppost(config, endpoint, task, success, error);
	}
}

function loadSubTasks(applicationService, tasks, idx, loadedSubTaskParent){
	var item = tasks[idx];
	
	var tree = {
			tasks: null,
			findEnd: function(sidx){
				var itm = tree.tasks[sidx];
				for(var jdx = sidx+1; jdx<tree.tasks.length; ++jdx){
					/*
					 * If this task is not a child of item, then we are at the end of 
					 */
					if(tree.tasks[jdx].parentNumber != itm.number){
						break;
					}else if(tree.tasks[jdx].children === true){
						/*If this indexed item has children, then we remove it from the loadedSubTaskParent*/
						var p = loadedSubTaskParent[tree.tasks[jdx].number];
						if(p) p.expanded = false;
						return tree.findEnd(jdx);
					}
				}
				return jdx;
			}
	}
	
	if(loadedSubTaskParent[item.number] != null){
		if(loadedSubTaskParent[item.number].expanded === true){
			tree.tasks = tasks;
			var jdx = tree.findEnd(idx);
			
			/*Now delete all this items subtasks. They will be reloaded when we next expand*/
			tasks.splice(idx+1, jdx-idx-1);
			loadedSubTaskParent[item.number].expanded = false;
			return;
		}
	}
	
	loadedSubTaskParent[item.number] = {loaded: false, expanded: false, parentNumber: item.parentNumber};
	
	loadTasks(applicationService, {parentNumber: item.number},
			function(response) {
				var data = response.data;
				if (!data){
					return;
				}

				tasks.splice.apply(tasks, [idx+1, 0].concat(response.data));
				loadedSubTaskParent[item.number].loaded = true;
				loadedSubTaskParent[item.number].expanded = true;
			}, 
			function(response){
		
	});
	
}

/**
 * Controllers
 */
function taskListController($scope, applicationService, taskService, filterFilter) {
	$scope.task = {};
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.moduleId = applicationService.get("Task");
	$scope.selected = "";
	$scope.searchTasks = '';
	
	$scope.AlertType = AlertType;
	
	$scope.currentTaskPage = 0;
    $scope.pageTasksSize = 10;
    $scope.numberOfPages=function(){
        return $scope.tasks== null ? 0 : Math.ceil($scope.tasks.length/$scope.pageTasksSize);                
    }
    
    $scope.$watch('searchTasks', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.taskResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.tasks = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentTaskPage = 0;
	}, true)
	
	$scope.loadTasks = function(){
		loadTasks(applicationService, {},
				function(response) {
					var data = response.data;
					$scope.tasks = data;
					$scope.taskResults = $scope.tasks;
					$scope.tasksLoaded = true;
				}, 
				function(response){
			
		});
	}

	$scope.newTask = function(){
		$scope.task = {};
		$scope.go('/task/new');
	}
	
	$scope.editTask = function(){
		if($scope.task.id && $scope.task.id!=""){
			taskService.set(TASK_ID_KEY, $scope.task.id);
			$scope.go('/task/edit');
		}
	};
	

	$scope.loadedSubTaskParent = {};
	$scope.showSubTasks = function(idx){
		try{
			loadSubTasks(applicationService, $scope.tasks, idx, $scope.loadedSubTaskParent);
		}finally{
			
		}
	}
	
	$scope.setSelectedTask = function(task){
		$scope.task = {id: task.id};
	}
	
	$scope.loadTasks();
}



function taskAddEditController($scope, $filter, $document, applicationService, taskService) {
	$scope.task = {
			id: taskService.get(TASK_ID_KEY), 
			person: new ValueDO(null, null), 
			parent: new ValueDO(null, null)
	};
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.moduleId = applicationService.get("Task");
	
	$scope.taskList = function(){
		$scope.go('/tasks');
	}
	
	$scope.saveTask = function() {
		var saveTaskService = taskService.getSaveTask();
		if(saveTaskService){
			saveTaskService(function(response){
				$scope.alert = response.data;				
			});
		}
	};
	
	/*Contract Note Management*/
    $scope.loadNotes = function(){
   	 $scope.$broadcast ('loadNotes');
    }
    
    /*Audit Log Management*/
    $scope.loadAudit = function(){
   	 $scope.$broadcast ('loadAudit');
    }
}

/**
 * Controller for the generic task controller
 * @param $scope
 * @param $filter
 * @param applicationService
 * @param taskService
 */
function taskController($scope, $filter, applicationService, taskService) {
	$scope.task = {
			id: $scope.taskId,
			person: new ValueDO(null, null), 
			parent: new ValueDO(null, null)
	};
	$scope.moduleId = applicationService.get("Task");
	$scope.addressTypes = applicationService.get(ADDRESS_TYPES_KEY);
	$scope.contacts = [];
	
    $scope.addressTypeTable = {};
    $scope.makeAddressTypeTable = function(){
		for(var idx in $scope.addressTypes)
			$scope.addressTypeTable[$scope.addressTypes[idx].id] = $scope.addressTypes[idx].value;
    }
	
	/*Auto complete task assignee*/
	autoComplete('#taskPerson', 
			function(request, response){
				var config = {
						headers : applicationService.getAuthHeader(),
						cache : false,
						params: {q : request.term, autoComplete : true}
					};
				var organisationId = applicationService.get(ORGANISATION_ID_KEY);
				applicationService.httpget(config, "".concat("/contractic/organisation/", organisationId, "/search/person"), 
						function(result) {
							var data = result.data;
							response(data);
					}, function(response){
						
					});
			}, 
			function(event, ui){
				$scope.task.person.id = ui.item.id;
				$scope.task.person.value = ui.item.value;
			});
	
	/*Auto complete task assignee*/
	autoComplete('#taskParent', 
			function(request, response){
				var config = {
						headers : applicationService.getAuthHeader(),
						cache : false,
						params: {q : request.term, externalId: $scope.task.id, moduleId: $scope.moduleId, autoComplete : true}
					};
		
					var organisationId = applicationService.get(ORGANISATION_ID_KEY);
					applicationService.httpget(config, "".concat("/contractic/organisation/", organisationId, "/search/task"), 
							function(result) {
								var data = result.data;
								response(data);
						}, function(response){
							
						});
			}, 
			function(event, ui){
				$scope.task.parent.id = ui.item.id;
				$scope.task.parent.value = ui.item.value;
			});
		
	$scope.saveTask = function(after) {
		
		$scope.taskFormSubmitted = true;
		
		if($scope.taskForm.$valid) {
			$scope.taskFormSubmitted = false;
			
		
		saveTask(applicationService, $scope.task, function(response){
			if(after)
				after(response);
			var data = response.data;
			if(!$scope.task.id)
				$scope.task.id = data.ref.id;
			
		}, function(response){
			
		});
		
	}
		
		else{
			after({data: {body: "Not all required fields for task have been filled in", type: AlertType.ERROR}})
		}
	};
	
	$scope.loadTaskStatuses = function(){
		$scope.statusesLoaded = false;
		loadStatuses(applicationService, {moduleId : $scope.moduleId},
				function(response) {
					$scope.taskstatuses = response.data;
					$scope.statusesLoaded = true;
				}, 
				function(){
					
				});
	}
	
	$scope.loadTask = function(taskId, after){
		if(!taskId)
			return;
		loadTasks(applicationService, {id: taskId},
				function(response) {
					var data = response.data;
					if(data && data.length == 1){
						var task = data[0];
						task.startDate = $filter('date')(task.startDate, 'dd/MM/yyyy');
						task.endDate = $filter('date')(task.endDate, 'dd/MM/yyyy');
						if(!task.repeat)
							task.repeat = {id: null, value: null};
						else if(task.repeat.id){
							$scope.taskrepeat = true;
							task.repeat.repeatStart = $filter('date')(task.repeat.repeatStart, 'dd/MM/yyyy');
							task.repeat.repeatEnd = $filter('date')(task.repeat.repeatEnd, 'dd/MM/yyyy');
						}
						$scope.task = task;
						$scope.loadTaskContacts();
						if(after)
							after();
					}
				}, 
				function(response){
					
				});
	}
	
 	$scope.createTask = function(task){
		$scope.task = task;
		$scope.taskrepeat = false;
		$scope.loadTaskContacts();
	}
	
	/*
	 * We can only delete a task contact or add a new one. We cannot edit
	 * To enable this, if a task contact has an id, we can only delete it and nothing more
	 * if a task contact does not have an id, we can only add it
	 * */
	$scope.changeTaskContact = function(item){
		if(!$scope.taskContactsLoaded)
			return;
		var action = null;
		/*Only perform save operation if the contact is not a Task contact*/
		if(item.selected == true && item.moduleId != $scope.moduleId && item.externalId != $scope.task.id){
			/*set the contact id = null so that we force a POST rather than a PUT*/
			item.id=null; 
			item.moduleId = $scope.moduleId;
			item.externalId = $scope.task.id;
			action = saveContact;
		}else if(item.selected == false && item.id!=null && item.moduleId == $scope.moduleId){
			item.moduleId = null;
			item.externalId = null;
			action = deleteContact;
		}else return;
		action(applicationService, item, function(response){
			var data = response.data;
			if(data.ref)
				item.id = data.ref.id;
		}, function(response){
			
		})
	}
	
	$scope.taskContactsLoaded = false;
    $scope.loadTaskContacts = function() {
    	var params = {moduleId: $scope.moduleId, externalId: $scope.task.id};
		loadContacts(applicationService, params, function(response) {
			$scope.taskcontacts = response.data;
			/*Now load all task affiliated contacts*/
			$scope.loadContacts();
			$scope.taskContactsLoaded = true;
		}, function (reponse){
			
		});
    };
    
    /*Load all task affiliated contacts: Task Owner, Contract Supplier and Owner (if task is a contract task) */
    $scope.loadContacts = function(){
    	
		var personModuleId = applicationService.get('Person');
		var supplierModuleId = applicationService.get('Supplier');
		var task = $scope.task;
		var params = [{moduleId: personModuleId, externalId: task.person.id}];
		if(task.module.value === 'Contract'){
			params.push({moduleId: personModuleId, externalId: task.external.owner.id});
			params.push({moduleId: supplierModuleId, externalId: task.external.supplier.id});
		}

		$scope.contactsLoaded = false;
		$scope.contacts = [];
		for(var idx=0; idx<params.length; ++idx){
			loadContacts(applicationService, {moduleId: params[idx].moduleId, externalId : params[idx].externalId}, function(response) {
				var data = response.data;
				
				for(var idx=0; idx<data.length; ++idx){
					data[idx].selected = false;
					for(var jdx=0; jdx<$scope.taskcontacts.length; ++jdx){
						if(data[idx].address.id === $scope.taskcontacts[jdx].address.id){
							data[idx] = $scope.taskcontacts[jdx];
							data[idx].selected = true;
							break;
						}
					}
				}
				
				$scope.contacts = $scope.contacts.concat(data);
				$scope.contactsLoaded = true;				
			}, function (reponse){
				
			});
		}
		
    };
    
	$scope.taskrepeat = false;
	$scope.taskmonthdays = [];
	$scope.taskmonthdays.length = 31;
	$scope.taskweekdays = [];
	$scope.taskweekdays.length = 7;
	
	$scope.setRepeat = function(repeatId){
		if($scope.task.repeat && repeatId===$scope.task.repeat.id){
			return;
		}
		$scope.task.repeat = {id: repeatId};
	}
	
    $scope.makeAddressTypeTable();
	$scope.loadTaskStatuses();
	
	taskService.reset();
	taskService.setLoadTask($scope.loadTask);
	taskService.setSaveTask($scope.saveTask);
	taskService.setCreateTask($scope.createTask);
	
	/*If this controller is loaded within a parent that has specified a task to be loaded, then we load the task here*/
	$scope.loadTask($scope.task.id, null);
	
}


var task = angular.module("task", [ 'ngRoute' ]);

/* Contract Service */
task.service('taskService', ['$location', function($location) {
	var LOAD_TASK = "load.task";
	var SAVE_TASK = "save.task";
	var CREATE_TASK = "create.task";
	
	// private variable
	var hashtable = {};
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		
		go : function(path){
	    	$location.path( path );
		},
		/*This is only to be used by the taskController*/
		setLoadTask : function(loadTask){
			hashtable[LOAD_TASK] = loadTask;
		},
		/**
		 * External service users call this function to request to load a specific
		 * user
		 */
		getLoadTask : function(){
			return hashtable[LOAD_TASK];
		},
		
		
		/*This is only to be used by the taskController*/
		setSaveTask : function(saveTask){
			hashtable[SAVE_TASK] = saveTask;
		},
		/**
		 * External service users call this function to request to load a specific
		 * user
		 */
		getSaveTask : function(){
			return hashtable[SAVE_TASK];
		},
		
		
		createNew : function(){
			return {
				id: null,
				type: new ValueDO(null, null),
				id: null,
				contact: new ValueDO(null, null)
			}
		},
		
		getCreateTask : function(){
			return hashtable[CREATE_TASK];
		},
		setCreateTask : function(createTask){
			hashtable[CREATE_TASK] = createTask;
		},
		
		reset : function(){
			hashtable = {};
		}
	}
}]);


task.controller("taskController", taskController);
/* Add/Edit Controller */
task.controller("taskListController", taskListController);
task.filter("indentTask", function(){
	   return function(taskNumber, parentNumber, scope){
		   var levels = 0;
		   var task = scope.loadedSubTaskParent[parentNumber];
		   while(task != null){
			   levels += 1;
			   task = scope.loadedSubTaskParent[task.parentNumber];
		   }
	      // Your logic
	      return levels>0 ? (Array(levels+1).join('+') + '\u00A0' + taskNumber) : taskNumber;
		  //return levels;x
	   }
	});

task.controller("taskAddEditController", taskAddEditController);

task.directive('taskRow', function () {
    return {
       restrict : 'A',
       replace : true,
       templateUrl : window.viewsBase + '/task-manager-task-row.html',
       link: function (scope) { 
           scope.Click = function (index) {
               scope.selectedIndex = index;
               alert("Transitioning to " + scope.items[index].Value);
           };
        },
        scope : { item : '='}
    }
});
