/**
 * 
 */

function profileController($scope, applicationService, sessionService, contactService, sessionService, filterFilter) {
	
	sessionService.validate();
	
	$scope.profile = {};
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.user = sessionService.getUser();
	$scope.person = $scope.user.person;
	$scope.AlertType = AlertType;
	contactService.setExternalId($scope.person.id);
	$scope.session = {password: null, password2: null, password3: null};
	
	$scope.passwordChanged = function(){
        $scope.oldMatch = null;
        $scope.isMatch = null;
        if($scope.session.password!=null && $scope.session.password2!=null && $scope.session.password2!=null){
    		$scope.oldMatch = $scope.session.password == $scope.session.password2;
    		$scope.isMatch = newValues[1] == newValues[2];
        }
	}
   $scope.$watch(["session.password", "session.password2" , "session.password3"], $scope.passwordChanged);
	
	$scope.newProfile = function(){
		$scope.profile = {};
	}
	
	$scope.saveProfile = function() {
		
		if(!($scope.session.password == null && $scope.session.password2 == null && $scope.session.password3 == null))
		{
			if(($scope.session.password && !$scope.session.password2) || ($scope.session.password2 && !$scope.session.password3)){
				$scope.alert = {body: "Please enter Old and New passwords", type: AlertType.ERROR};
				$scope.passwordForm.password.$setPristine();
				return;
			}
			
			if($scope.session.password2 && $scope.session.password3 && $scope.session.password2 != $scope.session.password3){
				$scope.alert = {body: "New and old passwords do not match", type: AlertType.ERROR};
				return;
			}			
			
		}
		
		
		$scope.personFormSubmitted = true;
		if($scope.personForm.$valid){
			
			$scope.personFormSubmitted = false;
			
			$scope.alert = {};
		
		var saveContact = contactService.getOnSave();
		/*Save the person first*/
		savePerson(applicationService, $scope.person, function(response){
			var data = response.data;
			if(data.type != AlertType.OK){
				$scope.alert = data;
				return;
			}
		}, function(response){
			
		});
		
		//$scope.session
		if($scope.session.password && $scope.session.password2 && $scope.session.password3){
			updateSession(applicationService, $scope.session, function(response){
				data = response.data;
				if(data.type != AlertType.OK){
					$scope.alert = data;
					return;
				}
			}, function(response){
				
			});
		}
			
		/*Save the contact*/
		saveContact();
	}
	else{
			$scope.alert = {body: "Not all required fields for department have been filled in", type: AlertType.ERROR};
		
	}
	};

}

var compareTo = function() {
    return {
      require: "ngModel",
      scope: {
        otherModelValue: "=compareTo"
      },
      link: function(scope, element, attributes, ngModel) {

        ngModel.$validators.compareTo = function(modelValue) {
          return modelValue == scope.otherModelValue;
        };

        scope.$watch("otherModelValue", function() {
          ngModel.$validate();
        });
      }
    };
  };

  
  


var profile = angular.module("profile", [ 'ngRoute' ]);

profile.directive("compareTo", compareTo);
/* Add/Edit Controller */
profile.controller("profileController", profileController);
