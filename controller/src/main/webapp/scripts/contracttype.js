/**
 * 
 */

/**
 * Load organisation persons
 * @param $scope
 * @param $http
 */
function loadContractTypes(applicationService, params, success, error) {
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false,
		params: params
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/contracttype");
	applicationService.httpget(config, request, success, error);
}

/**
 * Load a single contractType
 * @param $scope
 * @param $http
 */
function loadContractType(applicationService, typeId, success, error) {
	if (!typeId || typeId == '')
		return;
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/contracttype/", typeId);
	applicationService.httpget(config, request, success, error);
}

function saveContractType(applicationService, contractType, success, error) {

	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	if (contractType.id && contractType.id != '') {
		applicationService.httpput(config, "".concat("/contractic/organisation/",
				organisationId, "/contracttype"), contractType, success, error);
	} else {
		applicationService.httppost(config, "".concat("/contractic/organisation/",
				organisationId, "/contracttype"), contractType, success, error);
	}
}

function deleteContractType(applicationService, contractType, success, error) {

	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	if (contractType.id && contractType.id != '') {
		var request = "".concat("/contractic/organisation/", organisationId, 
				"/contracttype/", contractType.id);
		applicationService.httpdel(config, request, contractType, success, error);
	}
}

/**
 * Person controller function
 * @param $scope
 * @param $http
 * @param applicationService
 */
function contractTypeController($scope, $http, applicationService, confirmService, sessionService, filterFilter) {
	
	sessionService.validate();
	
	/*A single contractType being edited or created*/
	$scope.contractType = {};
	/*List of all contractTypes on the system*/
	$scope.contractTypes = [{}];
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);

	$scope.alert = {};
	$scope.go = applicationService.go;
	
	$scope.AlertType = AlertType;
	$scope.dialog = {show: false, readonly: true}
	$scope.showConfirm = false;
	
	$scope.searchContractList = '';
	
	$scope.currentContractListPage = 0;
    $scope.pageContractsSize = 10;
    $scope.numberOfPages=function(){
        return $scope.contractTypes== null ? 0 : Math.ceil($scope.contractTypes.length/$scope.pageContractsSize);                
    }
    
    $scope.$watch('searchContractList', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.conTypeResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.contractTypes = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentContractListPage = 0;
	}, true)

	$scope.newContractType = function(){
		$scope.contractTypeFormsubmitted = false;
		$scope.contractTypeForm.$setPristine();
		$scope.action = "Add New";
		$scope.contractType = {};
		$scope.alert = {};
		$scope.dialog.show = true;
	};
	
	$scope.editContractType = function(){
		$scope.contractTypeFormsubmitted = false;
		$scope.contractTypeForm.$setPristine();
		$scope.alert = {};
		$scope.action = "Edit";
		loadContractType(applicationService, $scope.contractType.id, 
				function(response) {
					$scope.contractType = response.data;
					$scope.dialog.show = true;
				}, 
				function(response){
					if(response.status == 400){
						$scope.alert = {body: response.data.body, type: AlertType.ERROR};
					}
		});
	};
	
	$scope.refreshList = function(){
		$scope.contractTypesLoaded = false;
		loadContractTypes(applicationService, null, function(response) {
			$scope.contractTypes = response.data;
			$scope.conTypeResults = $scope.contractTypes;
			$scope.contractTypesLoaded = true;
		}, function(response){
			
		});
	}
	
	$scope.saveContractType = function(){
		
		$scope.contractTypeFormsubmitted = true;
		if($scope.contractTypeForm.contract_name.$valid && $scope.contractTypeForm.contractActive.$valid) {
			$scope.contractTypeFormsubmitted = false;
			
			saveContractType(applicationService, $scope.contractType, function(response){
				$scope.alert = response.data;
			}, function(response){
				if(response.status == 400){
					$scope.alert = {body: response.data.body, type: AlertType.ERROR};
				}
			});
		}else {
			$scope.alert = {body: "Not all required fields for Contract Type have been filled in", type: AlertType.ERROR};
		}
	};
	
	$scope.setSelected = function(item){
		$scope.contractType = {id: item.id};
	}
	
	$scope.deleteContractType = function(){
		$scope.alert = {};
		if(!$scope.contractType || !$scope.contractType.id)
			return;
		$scope.showConfirm = true;
		var showConfirm = confirmService.getShowConfirm();
		showConfirm({
			message: "Are you sure want to delete the selected contract type",
			title: "Confirm",
			confirm: function(){
				deleteContractType(applicationService, $scope.contractType, function(response){
					$scope.alert = response.data;
					$scope.refreshList();
				}, function(response){
					if(response.status == 400){
						$scope.alert = {body: response.data.body, type: AlertType.ERROR};
					}
				});
			}
		});
	}
	
	$scope.closeDialog = function(){
		$scope.alert = {};
		$scope.dialog.show = false;
		$scope.refreshList();
	}
	
	$scope.refreshList();
}


var contractType = angular.module("contractType", [ 'ngRoute' ]);

/* Add/Edit Controller */
contractType.controller("contractTypeController", contractTypeController);
