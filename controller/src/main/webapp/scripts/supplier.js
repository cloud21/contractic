/**
 * 
 */

var SUPPLIER_ID_KEY = "supplier.id";

/**
 * Load organisation suppliers
 * 
 * @param $scope
 * @param $http
 */
function loadSuppliers(applicationService, params, success, error) {
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	applicationService.httpget(config, "".concat("/contractic/organisation/",
			organisationId, "/supplier"), success, error);
}

function loadSupplier(applicationService, id, success, error) {
	if (!id || id == '')
		return;

	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId,
			"/supplier/", id);
	applicationService.httpget(config, request, success, error);
}

function saveSupplier(applicationService, supplier, success, error) {

	var config = {
		headers : applicationService.getAuthHeader()
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId,
			"/supplier");
	if (supplier.id && supplier.id != '') {
		applicationService.httpput(config, request, supplier, success, error);
	} else {
		applicationService.httppost(config, request, supplier, success, error);
	}
}

function deleteSupplier(applicationService, supplier, success, error) {

	if (!supplier.id && supplier.id == '')
		return;

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId,
			"/supplier/", supplier.id);

	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};
	applicationService.httpdel(config, request, content, success, error);
}

/**
 * Person controller function
 * 
 * @param $scope
 * @param $http
 * @param applicationService
 */
function supplierListController($scope, $http, applicationService,
		supplierService, confirmService, sessionService, filterFilter) {

	sessionService.validate();

	$scope.suppliers = {};
	$scope.supplier = {};
	$scope.selection = [];
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.searchSupplier = '';
	$scope.sortField = 'reference';
	supplierService.set(SUPPLIER_ID_KEY, null);
	$scope.showConfirm = false;

	$scope.AlertType = AlertType;

	$scope.resetFilters = function() {

		$scope.searchSupplier = '';
	};

	$scope.currentSupplierPage = 0;
	$scope.pageSupplierSize = 10;
    
	$scope.numberOfPages = function() {
		return $scope.suppliers == null ? 0 : Math.ceil($scope.suppliers.length
				/ $scope.pageSupplierSize);
	}

	$scope.$watch('searchSupplier', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.supplierResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.suppliers = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentSupplierPage = 0;
	}, true);

	$scope.newSupplier = function() {
		supplierService.set(SUPPLIER_ID_KEY, null);
		$scope.go('/supplier/new');
	};

	$scope.editSupplier = function() {
		if ($scope.supplier && $scope.supplier.id && $scope.supplier.id != "") {
			supplierService.set(SUPPLIER_ID_KEY, $scope.supplier.id);
			$scope.go('/supplier/edit');
		}
	};

	$scope.refreshList = function() {
		$scope.suppliersLoaded = false;
		loadSuppliers(applicationService, null, function(response) {
			var data = response.data;
			$scope.suppliers = data;
			$scope.supplierResults = $scope.suppliers;
			$scope.suppliersLoaded = true;
		}, function(response) {

		});
	}

	$scope.deleteSupplier = function() {
		$scope.alert = {};
		if (!$scope.supplier || !$scope.supplier.id)
			return;
		$scope.showConfirm = true;
		var showConfirm = confirmService.getShowConfirm();
		showConfirm({
			message : "Are you sure want to delete the selected supplier",
			title : "Confirm",
			confirm : function() {
				deleteSupplier(applicationService, $scope.supplier, function(
						response) {
					$scope.alert = response.data;
					$scope.refreshList();
				}, function(response) {

				});
			}
		});
	}

	$scope.setSelected = function(supplier) {
		$scope.supplier = supplier;
	}
	$scope.refreshList();
}

function supplierAddEditController($scope, $http, applicationService,
		supplierService, noteService, contactService, sessionService) {

	sessionService.validate();

	$scope.supplier = {
		id : supplierService.get(SUPPLIER_ID_KEY)
	};
	$scope.contacts = [];

	$scope.reset = function() {
		$scope.supplier = {};
		$scope.supplier.contact = [ {} ];
		$scope.alert = {};
	}

	$scope.newSupplier = function() {
		$scope.supplierFormSubmitted = false;
		$scope.supplierForm.$setPristine();
		$scope.reset();
	};

	$scope.supplierList = function() {
		$scope.go('/supplier');
	}

	$scope.saveSupplier = function() {
		$scope.supplierFormSubmitted = true;
		if ($scope.supplierForm.$valid) {

			$scope.supplierFormSubmitted = false;

			$scope.alert = {};
			var saveContact = contactService.getOnSave();
			saveSupplier(applicationService, $scope.supplier,
					function(response) {
						$scope.alert = response.data;
						if ($scope.alert.type == AlertType.OK && saveContact) {
							saveContact();
						}
					}, function(response) {
						if (response.status == 400) {
							$scope.alert = {
								body : response.data.body,
								type : AlertType.ERROR
							};
						}
					});
		} else {
			$scope.alert = {
				body : "Not all required fields for the Supplier have been filled in",
				type : AlertType.ERROR
			};
		}
	};

	autoComplete('#contactName', function(request, response) {
		var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params : {
				q : request.term,
				autoComplete : true
			}
		};

		var organisationId = applicationService.get(ORGANISATION_ID_KEY);
		applicationService.httpget(config, "".concat(
				"/contractic/organisation/", organisationId, "/search/person"),
				function(result) {
					var data = result.data;
					response(data);
				}, function(response) {

				});
	}, function(event, ui) {
		$scope.user.personId = ui.item.id;
	});

	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.go = applicationService.go;

	$scope.AlertType = AlertType;
	$scope.addressTypes = applicationService.get(ADDRESS_TYPES_KEY);

	loadSupplier(applicationService, $scope.supplier.id, function(response) {
		var supplier = response.data;
		if (supplier)
			supplier.active = response.data.active === 'true';
		if (!supplier.contact || supplier.contact.length == 0) {
			supplier.contact = [ {} ];
		}
		$scope.supplier = supplier;
	}, function(response) {

	});

	/* Contact Management */

	/* Clear any data from a previous module */
	noteService.clear();
	$scope.loadNotes = function() {
		$scope.$broadcast('loadNotes');
	}

	noteService.setOnSave(function(alert) {
		$scope.alert = alert;
	});

	$scope.$on('saveNote', function(e, alert) {
		$scope.alert = alert;
	});

	/* Audit Log Management */
	$scope.loadAudit = function() {
		$scope.$broadcast('loadAudit');
	}

	$scope.loadContacts = function() {
		$scope.$broadcast('loadContacts');
	}

	$scope.loadContacts();
}

var supplier = angular.module("supplier", [ 'ngRoute' ]);

/* Add/Edit Controller */
supplier.controller("supplierAddEditController", supplierAddEditController);
/* Supplier List */
supplier.controller("supplierListController", supplierListController);

/* Person Service */
supplier.service('supplierService', [ '$location', function($location) {

	// private variable
	var hashtable = {};
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},

		go : function(path) {
			$location.path(path);
		}
	}

} ]);


