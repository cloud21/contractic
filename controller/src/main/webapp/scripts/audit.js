/**
 * Audit management module
 */

function loadAudits(applicationService, params, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params: params
		};
	
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/audit");
	applicationService.httpget(config, request, success, error);
}

function auditController($scope, $http, applicationService, filterFilter){
	$scope.AlertType = AlertType;
	$scope.moduleId = applicationService.get($scope.moduleName);
	
    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.numberOfPages=function(){
        return $scope.audits == null ? 0 : Math.ceil($scope.audits.length/$scope.pageSize);                
    }
    
    $scope.$watch('searchAudits', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.auditsResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.audits = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentPage = 0;
	}, true);
	
	$scope.loadAudits = function(){
		$scope.auditsLoaded = false;
		var params = {moduleId: $scope.moduleId, externalId: $scope.externalId, userId: $scope.userId};
		loadAudits(applicationService, params, function(response) {
			$scope.audits = response.data;
			$scope.auditsResults = $scope.audits;
			$scope.auditsLoaded = true;
		}, function (reponse){
			
		});
	}
	
	$scope.closeAuditDialog = function(){
		$scope.resetAudit();
		$scope.showAuditDialog = false;
		$scope.loadAudits();
	}
	
	$scope.resetAudit = function(){
		$scope.audit = {};
	}
	
	$scope.setSelectedAudit = function(id){
		$scope.resetAudit();
		$scope.audit.id = id;
	}
	
	$scope.$on('loadAudit', function(e) {  
		$scope.loadAudits();        
    });
	
}

var audit = angular.module("audit", [ 'ngRoute' ]);

/* Add/Edit Controller */
audit.controller("auditController", auditController);