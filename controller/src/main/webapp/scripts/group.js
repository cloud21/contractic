/**
 * Group Management
 */
function loadGroups(applicationService, params, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params: params
		};
	
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/group");
	applicationService.httpget(config, request, success, error);
}

function loadGroupSets(applicationService, params, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params: params
		};
	
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/groupset");
	applicationService.httpget(config, request, success, error);
}

function saveGroup(applicationService, group, success, error){
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var endpoint = "".concat("/contractic/organisation/", organisationId, "/group");
	if (group.id && group.id != '') {
		applicationService.httpput(config, endpoint, group, success, error);
	} else {
		applicationService.httppost(config, endpoint, group, success, error);
	}
}

function deleteGroup(applicationService, group, success, error) {

	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	if (group.id && group.id != '') {
		var request = "".concat("/contractic/organisation/", organisationId, 
				"/group/", group.id);
		applicationService.httpdel(config, request, group, success, error);
	}
}