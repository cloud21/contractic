
var PERSON_ID_KEY = "person.id";

/**
 * Load organisation persons
 * @param $scope
 * @param $http
 */
function loadPersons(applicationService, params, success, error) {

	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false,
		params: params
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, "/person");
	applicationService.httpget(config, request, success, error);
}

function loadPerson(applicationService, personId, success, error) {
	if (!personId || personId == '')
		return;
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/person/", personId);
	applicationService.httpget(config, request, success, error);
}

function savePersonContact($scope, $http, applicationService) {
	var contacts = $('input[id=contact]');
	var contactType = $('select[id=contactType]');
	var contactId = $('input[id=contactId]');
	
	var config = {
			headers : applicationService.getAuthHeader()
		};
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);

	for (var idx = 0; idx < contacts.length; ++idx) {
		var contact = {
			typeId : $(contactType[idx]).val(),
			value : $(contacts[idx]).val(),
			id : $(contactId[idx]).val(),
			organisationId : organisationId,
			externalId : $scope.person.id,
			moduleId : applicationService.get(PERSON_MODULE)
		};
		
		if (contact.id && contact.id != '') {
			$http.put("".concat("/contractic/organisation/", organisationId, "/contact"), contact,
					config).success(function(data) {
				$scope.alert = data;
			});
		} else {
			$http.post("".concat("/contractic/organisation/", organisationId, "/contact"),
					contact, config).success(
					function(data) {
						$scope.alert = data;
					});
		}
	}
}

function savePerson(applicationService, person, success, error) {

	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	if (person.id && person.id != '') {
		applicationService.httpput(config, "".concat("/contractic/organisation/", organisationId, "/person"), person,
				success, error);
	} else {
		applicationService.httppost(config, "".concat("/contractic/organisation/", organisationId, "/person"), person,
				success, error);
	}
}

function deletePerson(applicationService, person, success, error) {

	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false
		};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	if (person.id && person.id != '') {
		var request = "".concat("/contractic/organisation/", organisationId, 
				"/person/", person.id);
		applicationService.httpdel(config, request, person, success, error);
	}
}

/**
 * Person controller function
 * @param $scope
 * @param $http
 * @param applicationService
 */
function personAddEditController($scope, $http, applicationService, personService, noteService, contactService, sessionService) {
	
	sessionService.validate();
	
	$scope.person = {id: personService.get(PERSON_ID_KEY)};
	//$scope.person.contact = [{}];
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.moduleName = "Person";
	$scope.moduleId = applicationService.get($scope.moduleName);
	
	//$scope.addressTypes = [{id : "email", value: "Email"}, {id : "postal", value: "Postal"}];
	
	$scope.AlertType = AlertType;

	$scope.addressTypes = applicationService.get(ADDRESS_TYPES_KEY);
	
	$scope.loadDepartments = function(then){
		var groupsets = applicationService.get(SYSTEM_GROUPSET_KEY);
		var departmentgroupset = groupsets["DEPARTMENT"];
		$scope.departmentsLoaded = false;
		loadGroups(applicationService, {groupSetId: departmentgroupset.id/*, moduleId: $scope.moduleId*/}, 
				function(response){
			var data = response.data;
			$scope.departments = data;
			$scope.departmentsLoaded = true;
			if(then)
				then();
		}, function(response){
			
		})
	}
	
	//loadGroups(applicationService, )
	
	$scope.loadPerson = function(){
		loadPerson(applicationService, $scope.person.id, function(response) {
			var person = response.data;
			if(!person.contact || person.contact.length == 0){
				person.contact = [];
			}
			$scope.person = person;
			
			var data = $scope.departments;
			/*So that we are able to use the checkbox in the view.*/
			for(var idx=0; idx<data.length;++idx){
				var department = data[idx];
				department.ispersondept = false;
				for(var jdx=0; jdx<$scope.person.department.length; ++jdx){
					var pdepartment = $scope.person.department[jdx];
					if(department.id ===  pdepartment.id){
						department.ispersondept = true;
						break;
					}
				}
			}
			
		}, function(response){
			
		});
	}

	$scope.newPerson = function(){
		$scope.personFormSubmitted = false;
		$scope.personForm.$setPristine();
		$scope.person = {};
		$scope.alert = {};
	};
	
	$scope.personList = function(){
		$scope.go('/person');
	}
	
	$scope.savePerson = function(){
		/*Populate departments*/
		$scope.personFormSubmitted = true;
		if($scope.personForm.title.$valid && $scope.personForm.firstName.$valid && $scope.personForm.lastName.$valid) {
			$scope.personFormSubmitted = false;
			
		var departments = [];
		for(var idx=0; idx<$scope.departments.length; ++idx){
			if($scope.departments[idx].ispersondept)
				departments.push($scope.departments[idx]);
		}
		$scope.person.department = departments;
		
		/*Then save*/
		var saveContact = contactService.getOnSave();
		savePerson(applicationService, $scope.person, function(response) {
			$scope.alert = response.data;
			if($scope.alert.type == $scope.AlertType.OK && saveContact){
				saveContact();
			}
		}, function(response){
			if(response.status == 400){
				$scope.alert = {body: response.data.body, type: AlertType.ERROR};
			}
		});
		}
		else{
			$scope.alert = {body: "Not all required fields for the Person have been filled in", type: AlertType.ERROR};
		}
	};
	
	$scope.setSelectedDepartment = function(item){
		$scope.department = item;
	}
	
 	/*Clear any data from a previous module*/
    noteService.clear();
    $scope.loadNotes = function(){
   	 $scope.$broadcast ('loadNotes');
    }
    
    noteService.setOnSave(function(alert){
   	 $scope.alert = alert;
    });
    
	$scope.$on('saveNote', function(e, alert) {
		$scope.alert = alert;
   });
    
    /*Audit Log Management*/
    $scope.loadAudit = function(){
   	 $scope.$broadcast ('loadAudit');
    }
    
    $scope.loadContacts = function(){
    	$scope.$broadcast ('loadContacts');
    }
    
    $scope.loadContacts();
    $scope.loadDepartments($scope.loadPerson);
}


function personListController($scope, applicationService, personService, confirmService, sessionService, filterFilter) {
	sessionService.validate();
	
	$scope.persons = [{}];
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.searchPerson = '';
	$scope.sortField = 'firstName';
	personService.set(PERSON_ID_KEY, null);
	$scope.showConfirm = false;
	
	$scope.AlertType = AlertType;
	
	$scope.currentPersonPage = 0;
    $scope.pageSize = 10;
    $scope.numberOfPages=function(){
        return $scope.persons == null ? 0 : Math.ceil($scope.persons.length/$scope.pageSize);                
    }
    
    $scope.$watch('searchPerson', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.personResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.persons = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentPersonPage = 0;
	}, true);
    
	$scope.newPerson = function(){
		$scope.go('/person/new');
	};
	
	$scope.editPerson = function(){	
		if($scope.person && $scope.person.id){
			personService.set(PERSON_ID_KEY, $scope.person.id);
			$scope.go('/person/edit');
		}
	};
	
	$scope.refreshList = function(){
		$scope.personsLoaded = false;
		loadPersons(applicationService, null, function(response) {
			$scope.persons = response.data;
			$scope.personResults = $scope.persons;
			/*
			 * We override  department objects toString method so we are able to print the departments
			 * in the view
			 * */
			for(var idx=0; idx<$scope.persons.length;++idx){
				var person = $scope.persons[idx];
				var departments = person.department
				if(!person || !departments)
					continue;
				var departments = $.map(departments, function(item) {
				    return item.name;
				});
				person.department = departments;
			}
			
			$scope.personsLoaded = true;
		}, function(response){
			
		});
	}
	
	$scope.deletePerson = function(){
		$scope.alert = {};
		if(!$scope.person || !$scope.person.id)
			return;
		$scope.showConfirm = true;
		var showConfirm = confirmService.getShowConfirm();
		showConfirm({
			message: "Are you sure want to delete the selected person",
			title: "Confirm",
			confirm: function(){
				deletePerson(applicationService, $scope.person, function(response){
					$scope.alert = response.data;
					if($scope.alert.type == AlertType.OK){
						$scope.refreshList();
					}
				}, function(response){
					
				});
			}
		});
	}
	
	$scope.setSelected = function(item){
		$scope.person = item;
	}
	
	$scope.refreshList();
}

var person = angular.module("person", [ 'ngRoute' ]);

/* Add/Edit Controller */
person.controller("personAddEditController", personAddEditController);
/* Person List Controller*/
person.controller("personListController", personListController);

/* Person Service */
person.service('personService', ['$location', function($location) {

	// private variable
	var hashtable = {};
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		
		go : function(path){
	    	$location.path( path );
		}
	}
}]);

/*// configure our routes
person.config(function($routeProvider) {
	$routeProvider

	// route for the home page
	.when('/person/new', {
		templateUrl : window.viewsBase + '/admin-person-add-edit.html'
	})

	// route for the about page
	.when('/person/edit', {
		templateUrl : window.viewsBase + '/admin-person-add-edit.html'
	});
});*/