var USER_ID_KEY = "user.id";

/**
 * Load organisation users
 * 
 * @param $scope
 * @param $http
 */
function loadUsers(applicationService, params, success, error) {

	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false,
		params : params
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId,
			"/user");
	applicationService.httpget(config, request, success, error);
}

function loadUser(applicationService, userId, success, error) {
	if (!userId || userId == '')
		return;

	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId,
			"/user/", userId);
	applicationService.httpget(config, request, success, error);
}

function saveUser(applicationService, user, success, error) {

	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId,
			"/user");
	if (user.id && user.id != '') {
		applicationService.httpput(config, request, user, success, error);
	} else {
		applicationService.httppost(config, request, user, success, error);
	}
}

function deleteUser(applicationService, user, success, error) {

	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	if (user.id && user.id != '') {
		var request = "".concat("/contractic/organisation/", organisationId,
				"/user/", user.id);
		applicationService.httpdel(config, request, user, success, error);
	}
}

/**
 * User controller function
 * 
 * @param $scope
 * @param $http
 * @param applicationService
 */
function userAddEditController($scope, $http, applicationService, userService,
		sessionService) {

	sessionService.validate();

	$scope.user = {
		id : userService.get(USER_ID_KEY)
	};
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.modules = {};
	$scope.permissions = [];

	/*
	 * This code is copied from role.js so needs to be refactored to be shared
	 * correctly
	 */
	var modules = applicationService.get(SYSTEM_MODULE_KEY);
	$scope.moduleNames = [ 'Contract', 'Supplier', 'Organisation', 'Person' ];
	for ( var moduleId in modules) {
		for ( var idx in $scope.moduleNames) {
			var name = $scope.moduleNames[idx];
			if (name == modules[moduleId].name)
				$scope.modules[moduleId] = name;
		}
	}

	loadUserRoles(applicationService, null, function(response) {
		$scope.userRoles = response.data;
	}, function(response) {

	});

	// $scope.addressTypes = [{id : "email", value: "Email"}, {id : "postal",
	// value: "Postal"}];

	/* Auto complete */
	autoComplete('#personName', function(request, response) {
		var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params : {
				q : request.term,
				autoComplete : true
			}
		};

		var organisationId = applicationService.get(ORGANISATION_ID_KEY);
		applicationService.httpget(config, "".concat(
				"/contractic/organisation/", organisationId, "/search/person"),
				function(result) {
					var data = result.data;
					response(data);
				}, function(response) {

				});
	}, function(event, ui) {
		$scope.user.personId = ui.item.id;
		$scope.loadContacts();
	});

	$scope.AlertType = AlertType;

	$scope.loadContacts = function() {
		$scope.contactsLoaded = false;
		$scope.contacts = [];
		var moduleId = applicationService.get("Person");
		loadContacts(applicationService, {
			moduleId : moduleId,
			externalId : $scope.user.personId
		}, function(response) {
			$scope.contacts = response.data;
			$scope.contactsLoaded = true;
		}, function(reponse) {

		});
	}

	$scope.setSelectedContact = function(item) {
		$scope.user.login = item.address.value;
		$scope.contact = item;
	}

	loadUser(applicationService, $scope.user.id, function(response) {
		var user = response.data;
		$scope.user = user;
		
		/*
		 * This code is copied from role.js so needs to be refactored to be
		 * shared correctly
		 */
		var userPermissions = $scope.user.role.permission || [];

		/* Populate any missing module permissions */

		for ( var moduleId in $scope.modules) {
			var permissions = null;
			for ( var idx in $scope.user.role.permission) {
				var permission = $scope.user.role.permission[idx];
				if (permission.module && permission.module.id
						&& permission.module.id === moduleId)
					permissions = permission;
			}
			if (permissions === null) {
				permissions = {
					module : {
						id : moduleId,
						value : $scope.modules[moduleId]
					}
				};
				userPermissions.push(permissions);
			}
		}

		$scope.permissions = userPermissions;

	}, function(response) {

	});

	$scope.newUser = function() {
		$scope.userAddFormsubmitted = false;
		$scope.useraddForm.$setPristine();
		$scope.alert = {};
		$scope.user = {};
	};

	$scope.userList = function() {
		$scope.go('/user');
	}

	$scope.saveUser = function() {
		$scope.userAddFormsubmitted = true;
		if ($scope.useraddForm.personName.$valid
				&& $scope.useraddForm.userLogin.$valid
				&& $scope.useraddForm.user_role.$valid) {

			$scope.userAddFormsubmitted = false;

			saveUser(applicationService, $scope.user, function(response) {
				$scope.alert = response.data;
			}, function(response) {
				if (response.status == 400) {
					$scope.alert = {
						body : response.data.body,
						type : AlertType.ERROR
					};
				}
			});
		} else {
			$scope.alert = {
				body : "Not all required fields for User have been filled in",
				type : AlertType.ERROR
			};
		}

	};

	$scope.loadAudit = function() {
		$scope.$broadcast('loadAudit');
	}

	$scope.setUserPermissions = function() {
		$scope.permissions = $scope.user.role.permission;
	}
}

function userListController($scope, $http, applicationService, userService,
		confirmService, sessionService, filterFilter) {

	sessionService.validate();

	$scope.users = [ {} ];
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.searchUser = '';
	$scope.sortField = 'login';
	$scope.showConfirm = false;

	$scope.currentUserPage = 0;
	$scope.pageUserSize = 10;
	$scope.numberOfPages = function() {
		return $scope.users == null ? 0 : Math.ceil($scope.users.length
				/ $scope.pageUserSize);
	}
	
	$scope.$watch('searchUser', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.usersResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.users = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentUserPage = 0;
	}, true);

	userService.set(USER_ID_KEY, null);

	$scope.AlertType = AlertType;

	$scope.newUser = function() {
		$scope.go('/user/new');
	};

	$scope.editUser = function() {
		if ($scope.user && $scope.user.id && $scope.user.id != "") {
			userService.set(USER_ID_KEY, $scope.user.id);
			$scope.go('/user/edit');
		}
	};
	
	
	
	$scope.deleteUser = function() {
		$scope.alert = {};
		if (!$scope.user || !$scope.user.id)
			return;
		$scope.showConfirm = true;
		var showConfirm = confirmService.getShowConfirm();
		showConfirm({
			message : "Are you sure want to delete the selected user",
			title : "Confirm",
			confirm : function() {
				deleteUser(applicationService, $scope.user, function(response) {
					$scope.alert = response.data;
					if ($scope.alert.type == AlertType.OK) {
						$scope.refreshList();
					}
				}, function(response) {

				});
			}
		});
	}

	$scope.refreshList = function() {
		$scope.usersLoaded = false;
		loadUsers(applicationService, null, function(response) {
			$scope.users = response.data;
			$scope.usersResults = $scope.users;
			$scope.usersLoaded = true;
		}, function(response) {

		});
	}

	$scope.setSelected = function(id) {
		$scope.user = {
			id : id
		}
	}
	$scope.refreshList();
}

var user = angular.module("user", [ 'ngRoute' ]);

/* Add/Edit Controller */
user.controller("userAddEditController", userAddEditController);
/* User List Controller */
user.controller("userListController", userListController);

/* User Service */
user.service('userService', [ '$location', function($location) {

	// private variable
	var hashtable = {};
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},

		go : function(path) {
			$location.path(path);
		}
	}
} ]);

// configure our routes
user.config(function($routeProvider) {
	$routeProvider

	// route for the home page
	.when('/user/new', {
		templateUrl : window.viewsBase + '/admin-user-add-edit.html'
	})

	// route for the about page
	.when('/user/edit', {
		templateUrl : window.viewsBase + '/admin-user-add-edit.html'
	});
});