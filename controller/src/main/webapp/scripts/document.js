/**
 * Document management module
 */
var DOCUMENT_ID_KEY = "document.id.key";

function loadDocuments(applicationService, params, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params: params
		};
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/document");
	applicationService.httpget(config, request, success, error);
}

function saveDocument(applicationService, document, success, error) {
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var endpoint = "".concat("/contractic/organisation/", organisationId, "/document");
	
	if (document.id && document.id != '') {
		applicationService.httpput(config, endpoint, document, success, error);
	} else {
		applicationService.httppost(config, endpoint, document, success, error);
	}
}

function documentListController($scope, $http, $window, applicationService, documentService, sessionService, filterFilter) {
	
	sessionService.validate();
	
	$scope.document = {};
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.fileicon = documentService.fileicon();
	$scope.searchDocumentList='';
	
	$scope.currentDocumentPage = 0;
    $scope.documentpageSize = 10;
    $scope.numberOfPages=function(){
        return $scope.documents == null ? 0 : Math.ceil($scope.documents.length/$scope.documentpageSize);                
    }
    
    $scope.$watch('searchDocumentList', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.docResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.documents = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentDocumentPage = 0;
	}, true)
	
	$scope.AlertType = AlertType;

	$scope.loadDocuments = function(){
		$scope.documentsLoaded = false;
		loadDocuments(applicationService, {}, function(response) {
			$scope.documents = response.data;
			$scope.docResults = $scope.documents;
			$scope.documentsLoaded = true;
		}, function(response){
			
		});
	};

	$scope.createDocument = function(){
		createDocument($scope);
	}
	
	$scope.editDocument = function(){
		if($scope.document && $scope.document.id && $scope.document.id!=""){
			documentService.set(DOCUMENT_ID_KEY, $scope.document.id);
			$scope.go('/document/edit');
		}
	}
	
	$scope.saveDocument = function(){
		saveDocument($scope);
	};
	
	$scope.setSelectedDocument = function(item){
		$scope.document = {id: item.id, file: item.file};
	}
	
	$scope.downloadDocument = function(){
		if(!$scope.document || !$scope.document.file.id || $scope.document.file.id == '')
			return;
		var endpoint = "".concat("/contractic/organisation/", $scope.organisationId, "/file/", $scope.document.file.id);
		$window.location.href = endpoint;
	}

	$scope.loadDocuments();
}

function documentEditController($scope, $http, $window, applicationService, documentService, sessionService) {
	
	sessionService.validate();
	
	$scope.document = {};
	$scope.documentId = documentService.get(DOCUMENT_ID_KEY);
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.fileicon = documentService.fileicon();
	
	$scope.AlertType = AlertType;

	$scope.loadDocument = function(){
		$scope.documentLoaded = false;
		loadDocuments(applicationService, {documentId: documentId}, function(response) {
			$scope.document = response.data;
			$scope.documentLoaded = true;
		}, function(response){
			
		});
	};
	
	$scope.saveDocument = function(){
		saveDocument($scope);
	};
	
	$scope.setSelectedDocument = function(item){
		$scope.document = {id: item.id, file: item.file};
	}
	
	$scope.downloadDocument = function(){
		if(!$scope.document || !$scope.document.file.id || $scope.document.file.id == '')
			return;
		var endpoint = "".concat("/contractic/organisation/", $scope.organisationId, "/file/", $scope.document.file.id);
		$window.location.href = endpoint;
	}

	$scope.loadDocument();
}

var dokument = angular.module("document", [ 'ngRoute' ]);

/* List Controller */
dokument.controller("documentListController", documentListController);

dokument.controller("documentEditController", documentEditController);

/* Contract Service */
dokument.service('documentService', ['$location', function($location) {

	// private variable
	var hashtable = {};
	var fileicons = {};
	fileicons["doc"] = "icons/32px/doc.png";
	fileicons["pdf"] = "icons/32px/pdf.png";
	fileicons["docx"] = "icons/32px/doc.png";
	fileicons["pptx"] = "icons/32px/ppt.png";
	fileicons["vsd"] = "icons/32px/vsd.png";
	fileicons["xls"] = "icons/32px/xls.png";
	fileicons["xlsx"] = "icons/32px/xls.png";
	fileicons["default"] = "icons/32px/_blank.png";
	
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		
		go : function(path){
	    	$location.path( path );
		},
		fileicon : function(){
			return fileicons;
		}
	}
}]);

// configure our routes
dokument.config(function($routeProvider) {
	$routeProvider

	// route for the home page
	.when('/document/create', {
		templateUrl : window.viewsBase + '/document-add-edit.html'
	})

	// route for the about page
	.when('/document/edit', {
		templateUrl : window.viewsBase + '/document-add-edit.html'
	});
});

dokument.constant("DOCUMENT_FILE_ICONS", {
    "DOC": ".doc",
    "DOCX": ".docx",
    "PPTX": ".pptx",
    "PDF": ".pdf",
    "VSD": ".vsd"
});