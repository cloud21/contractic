/**
 * Organisation Departments
 */

function departmentController($scope, applicationService, confirmService, sessionService, filterFilter){
	
	sessionService.validate();
	
	$scope.AlertType = AlertType;
	$scope.dialog = {show: false, readonly: false};
	$scope.addressTypes = applicationService.get(ADDRESS_TYPES_KEY);
	$scope.alert = {};
	$scope.showConfirm = false;
	$scope.searchDepartment = '';
	$scope.sortField = 'name';
	
	$scope.currentDepartmentPage = 0;
    $scope.pageDepartmentSize = 10;
    $scope.numberOfPages=function(){
        return $scope.departments== null ? 0 : Math.ceil($scope.departments.length/$scope.pageDepartmentSize);                
    }
    
    $scope.$watch('searchDepartment', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.deptResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.departments = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentDepartmentPage = 0;
	}, true);
	
	$scope.loadDepartments = function(){
		var groupsets = applicationService.get(SYSTEM_GROUPSET_KEY);
		var departmentgroupset = groupsets["DEPARTMENT"];
		$scope.departmentsLoaded = false;
		loadGroups(applicationService, {groupSetId: departmentgroupset.id/*, moduleId: $scope.moduleId*/}, 
				function(response){
			var data = response.data;
			$scope.departments = data;
			$scope.deptResults = $scope.departments;
			$scope.departmentsLoaded = true;
		}, function(response){
			
		})
	}
	
	$scope.setSelected = function(item){
		$scope.department = item;
	}
	
	$scope.newDepartment = function(){
		$scope.departmentFormSubmitted = false;
		$scope.alert = {};
		$scope.department = {groupset: new ValueDO(null, 'DEPARTMENT')};
		$scope.action = 'New';
		$scope.dialog.show = true;
	}
	
	$scope.editDepartment = function(){
		//$scope.department = {groupset: new ValueDO(null, 'DEPARTMENT')};
		$scope.departmentFormSubmitted = false;
		$scope.alert = {};
		$scope.action = 'Edit';
		loadGroups(applicationService, {groupId: $scope.department.id}, function(response){
			var data = response.data;
			if(data && data.length == 1){
				$scope.department = data[0];
				$scope.dialog.show = true;
			}
		}, function(response){
			
		});
	}
	
	$scope.saveDepartment = function(){
		
		$scope.departmentFormSubmitted = true;
		if($scope.departmentForm.name.$valid){
			
			$scope.departmentFormSubmitted = false;
			
		
		saveGroup(applicationService, $scope.department, function(response){
			$scope.alert = response.data;
		}, function(response){
			
		})
		
	}
		else{
			$scope.alert = {body: "Not all required fields for department have been filled in", type: AlertType.ERROR};
		}
	}
	
	$scope.deleteDepartment = function(){
		$scope.alert = {};
		if(!$scope.department || !$scope.department.id)
			return;
		$scope.showConfirm = true;
		var showConfirm = confirmService.getShowConfirm();
		showConfirm({
			message: "Are you sure want to delete the selected department",
			title: "Confirm",
			confirm: function(){
				deleteGroup(applicationService, $scope.department, function(response){
					$scope.alert = response.data;
					$scope.loadDepartments();
				}, function(response){
					
				});
			}
		});
	}
	
	$scope.closeDialog = function(){
		$scope.dialog.show = false;
		$scope.alert = {};
	}
	
	$scope.loadDepartments();
}

var department = angular.module("department", [ 'ngRoute' ]);
department.controller("departmentController", departmentController);