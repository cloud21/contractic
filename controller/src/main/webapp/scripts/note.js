/**
 * Note management module
 */

function loadNotes(applicationService, params, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params: params
		};
	
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/note");
	//config, request, success, error
	applicationService.httpget(config, request, success, error);
}

function saveNote(applicationService, note, success, error) {
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var endpoint = "".concat("/contractic/organisation/", organisationId, "/note");
	
	if (note.id && note.id != '') {
		applicationService.httpput(config, endpoint, note, success, error);
	} else {
		applicationService.httppost(config, endpoint, note, success, error);
	}
}

function noteController($scope, $http, applicationService, noteService, filterFilter){
	$scope.AlertType = AlertType;
	$scope.moduleId = applicationService.get($scope.moduleName);
	$scope.onSave = noteService.getOnSave();
	
	
	$scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.numberOfPages=function(){
        return $scope.notes == null ? 0 : Math.ceil($scope.notes.length/$scope.pageSize);                
    }
    
    $scope.$watch('searchNotes', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.noteResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.notes = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentPage = 0;
	}, true);
	
	$scope.addNote = function(){
		$scope.resetNote();
		$scope.noteAction = "Add"
		$scope.showNoteDialog = true;
	}
	
	$scope.editNote = function(){
		if($scope.note && $scope.note.id && $scope.note.id!=""){
			var params = {moduleId: $scope.moduleId, externalId: $scope.contract.id, noteId : $scope.note.id};
			loadNotes(applicationService, params, function(response) {
				if(response.data && response.data.length==1){
					$scope.note = response.data[0];
					$scope.noteAction = "Edit";
					$scope.showNoteDialog = true;
				}
				
				
			}, function (reponse){
				
			});
		}
	}
	
	$scope.saveNote = function(){
		$scope.noteFormSubmitted = true;
		if($scope.noteForm.$valid){
			
			$scope.noteFormSubmitted = false;
			
		
		$scope.note.externalId = $scope.externalId || noteService.getExternalId();
		$scope.note.moduleId = $scope.moduleId;
		
		if(!$scope.note.externalId || $scope.note.externalId == ''){
			$scope.alert = {body: "No externalId supplied", type: AlertType.ERROR};
			return;
		}
		
		saveNote(applicationService, $scope.note, function(response) {
			$scope.alert = response.data;
			if($scope.onSave)
				$scope.onSave($scope.alert);
		}, function(response) {
			if(response.status == 400){
				$scope.alert = {body: response.data.body, type: AlertType.ERROR};
				if($scope.onSave)
					$scope.onSave($scope.alert);
			}
		});
		
	}
		else{
			$scope.alert = {body: "Not all required fields for Note have been filled in", type: AlertType.ERROR};
		}
		
	}
	
	$scope.loadNotes = function(){
		$scope.notesLoaded = false;
		var params = {moduleId: $scope.moduleId, externalId: $scope.externalId || noteService.getExternalId()};
		loadNotes(applicationService, params, function(response) {
			$scope.notes = response.data;
			$scope.noteResults = $scope.notes;
			$scope.notesLoaded = true;
		}, function (reponse){
			
		});
	}
	
	$scope.closeNoteDialog = function(){
		$scope.resetNote();
		$scope.showNoteDialog = false;
		/*Clear the alert by supplying null message*/
		if($scope.onSave)
			$scope.onSave(null);
		$scope.loadNotes();
	}
	
	$scope.resetNote = function(){
		$scope.note = {moduleId: $scope.moduleId, externalId: $scope.externalId};
	}
	
	$scope.setSelectedNote = function(item){
		$scope.resetNote();
		$scope.note.id = item.id;
	}
	
	$scope.$on('loadNotes', function(e) {  
		$scope.loadNotes();        
    });
	
	loadNoteTypes(applicationService, null, function(response) {
		$scope.noteTypes = response.data;
	}, function(response){
		
	});
}

var note = angular.module("note", [ 'ngRoute' ]);

/* Add/Edit Controller */
note.controller("noteController", noteController);

/* Note Service
 * NOTE: Call clear() the first time this service is used within each module
 * in order to clear any data that may have been used by a previous module 
 * */
note.service('noteService', ['$http', function($http) {

	var ALERT_KEY = "alert.key";
	var EXTERNAL_ID = "external.id";
	var MODULE_NAME = "module.name";
	var ON_SAVE = "on.save";
	// private variable
	var hashtable = {};
	
	
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		setAlert : function(alert){
			hashtable[ALERT_KEY] = alert;
		},
		getAlert : function(){
			return hashtable[ALERT_KEY];
		},
		setExternalId : function(externalId){
			hashtable[EXTERNAL_ID] = externalId;
		},
		getExternalId : function(){
			return hashtable[EXTERNAL_ID];
		},
		getModuleName : function(){
			return hastable[MODULE_NAME];
		},
		setModuleName : function(name){
			hashtable[MODULE_NAME] = name;
		},
		setOnSave : function(onSave){
			hashtable[ON_SAVE] = onSave;
		},
		getOnSave : function(){
			return hashtable[ON_SAVE];
		},
		clear : function(){
			hashtable = {};
		}
	}
}]);