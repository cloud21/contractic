var CONTRACT_ID_KEY = "contract.id";

/**
 * Contract Detail Management
 * 
 * @param $scope
 * @param $http
 * @param applicationService
 */

/**
 * End of Contract Detail Management
 */

/**
 * Load organisation contracts
 * 
 * @param $scope
 * @param $http
 */
function loadContracts(applicationService, params, success, error) {

	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false,
		params : params
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId,
			"/contract");
	applicationService.httpget(config, request, success, error);
}

function saveContract(applicationService, contract, success, error) {

	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var endpoint = "".concat("/contractic/organisation/", organisationId,
			"/contract");
	if (contract.id && contract.id != '') {
		applicationService.httpput(config, endpoint, contract, success, error);
	} else {
		applicationService.httppost(config, endpoint, contract, success, error);
	}
}

function ValueDO(id, value) {
	this.id = id;
	this.value = value;
}

function loadContractHeader(applicationService, contractId, success, error) {
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};
	var request = "".concat("/contractic/organisation/", organisationId,
			"/contract/", contractId);
	applicationService.httpget(config, request, success, error);
}

function loadContractDetail(applicationService, params, success, error) {
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false,
		params : params
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var request = "".concat("/contractic/organisation/", organisationId,
			"/contractdetail");
	applicationService.httpget(config, request, success, error);
}

function saveContractDetail(applicationService, detail, success, error) {
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var endpoint = "".concat("/contractic/organisation/", organisationId,
			"/contractdetail");

	if (detail.id && detail.id != '') {
		applicationService.httpput(config, endpoint, detail, success, error);
	} else {
		applicationService.httppost(config, endpoint, detail, success, error);
	}
}

function deleteContractDetail(applicationService, id, success, error) {
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};

	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var endpoint = "".concat("/contractic/organisation/", organisationId,
			"/contractdetail/", id);
	applicationService.httpdel(config, endpoint, null, success, error);
}

/**
 * Contract controller function
 * 
 * @param $scope
 * @param $http
 * @param applicationService
 */

function contractAddEditControllerExtensionManagement($scope,
		applicationService) {

	$scope.extAlert = {};

	$scope.$on('removeTerm', function(e, alert) {
		$scope.valuealert = {
			type : AlertType.OK,
			body : "Term Deleted"
		};
	});

	function getTermPeriod(type) {
		return {
			id : null,
			contract : new ValueDO($scope.contract.id, null),
			group : new ValueDO(null, type),
			term : {
				number : null,
				properties : {
					period : {
						cycle : {
							frequency : null,
							value : null
						},
					},
					active : false
				},
				start : null
			}
		};
	}

	/* Contract extension management */
	$scope.getNewTerm = function() {
		return {
			number : null,
			period : getTermPeriod('CONTRACT_TERM_PERIOD'),
			notice : getTermPeriod('CONTRACT_TERM_NOTICE'),
			gotomarket : getTermPeriod('CONTRACT_TERM_GOTO_MARKET')
		};
	}

	$scope.alertTerm = function(index) {
		$scope.extIndex = index;
		$scope.extAlert = {
			type : AlertType.WARNING,
			body : "Do you want to delete term?"
		};

	}

	$scope.addTerm = function() {
		var term = $scope.term;
		if (term.period.term.properties.period.cycle.frequency
				&& term.period.term.properties.period.cycle.value) {
			/* If we haven't an id, then this is a new contact */
			if (!term.period.id) {
				if ($scope.contract.term.length == 0)
					term.period.term.properties.active = true;
				$scope.contract.term.push(term);
			}
		}
		$scope.term = $scope.getNewTerm();
	}

	$scope.editTerm = function(item) {
		if (!item.period)
			item.period = getTermPeriod('CONTRACT_TERM_PERIOD');
		if (!item.notice)
			item.notice = getTermPeriod('CONTRACT_TERM_NOTICE');
		if (!item.gotomarket)
			item.gotomarket = getTermPeriod('CONTRACT_TERM_GOTO_MARKET');
		$scope.term = item;
	}

	$scope.moveTermUp = function(index) {

		if ($scope.contract.term[index - 1]) // if previous element exists
		{
			// swap elements
			$scope.contract.term.splice(index - 1, 2,
					$scope.contract.term[index],
					$scope.contract.term[index - 1]);

		} else {

			return 0;
		}

	}

	$scope.moveTermDown = function(index) {
		if ($scope.contract.term[index + 1]) // if next element exists
		{
			// swap elements
			$scope.contract.term.splice(index, 2,
					$scope.contract.term[index + 1],
					$scope.contract.term[index]);

		} else {

			return 0;
		}
	}

	$scope.saveTerm = function() {

		/*
		 * If extension have been disabled, then at the point of saving the
		 * contract (which) then calls saving the extension, we remove all
		 * extensions
		 */
		if ($scope.oneOffPurchase) {
			var idx = $scope.contract.term.length;
			while (idx > 0) {
				$scope.removeTerm(idx);
				idx = $scope.contrac.term.length;
			}
			return;
		}

		for (var idx = 0; idx < $scope.contract.term.length; ++idx) {
			var term = $scope.contract.term[idx];
			term.number = idx + 1;
			if (term.period) {
				term.period.number = term.number;
				term.period.term.number = term.number;
				if (idx == 0)
					term.period.term.properties.active = true;
			}
			if (term.notice) {
				term.notice.number = term.number;
				term.notice.term.number = term.number;
			}
			if (term.gotomarket) {
				term.gotomarket.number = term.number;
				term.gotomarket.term.number = term.number;
			}
		}
	}

	$scope.setSelectedTerm = function(term) {
		$scope.term = term;
	}

	$scope.termHoverIn = function(item) {
		$scope.hoveredTerm = item;
	}

	$scope.termHoverOut = function() {
		$scope.hoveredTerm = null;
	}

	$scope.removeTerm = function(index) {
		var term = $scope.contract.term[index];

		/* If this is a new term, then just remove if */
		if (!term.period.id)
			$scope.contract.term.splice(index, 1);

		/* if this is an existing extension, remove it remotely and locally */
		deleteContractDetail(
				applicationService,
				term.period.id,
				function(response) {
					var data = response.data;
					$scope.extAlert = {
						type : AlertType.OK,
						body : "Term deleted"
					};
					if (data.type == AlertType.OK) {
						if (term.notice.id) {
							deleteContractDetail(
									applicationService,
									term.notice.id,
									function(response) {
										var data = response.data;
										$scope.extAlert = {
											type : AlertType.OK,
											body : "Term deleted"
										};
										if (data.type == AlertType.OK) {
											if (term.gotomarket.id) {
												deleteContractDetail(
														applicationService,
														term.gotomarket.id,
														function(response) {
															var data = response.data;
															$scope.extAlert = {
																type : AlertType.OK,
																body : "Term deleted"
															};
															if (data.type == AlertType.OK) {
																$scope.contract.term
																		.splice(
																				index,
																				1);

															}
														}, function(response) {

														});
											} else {
												$scope.contract.term.splice(
														index, 1);

											}
										}
									}, function(response) {

									});
						} else {
							$scope.contract.term.splice(index, 1);

						}
					}
				}, function(response) {

				});
	}

	// $scope.extAlert = { type : AlertType.OK, body : "Term deleted"};
	$scope.oneOffPurchase = true;
	$scope.term = $scope.getNewTerm();

	/* -- Contract ccn management */

}

function contractAddEditControllerValueManagement($scope, applicationService) {
	/* Contract value management */
	$scope.loadValue = function() {
		var params = {
			contractId : $scope.contract.id,
			group : "VALUE"
		};
		$scope.valuesLoaded = false;
		$scope.values = {};
		loadContractDetail(applicationService, params, function(response) {
			$scope.values = response.data;
			$scope.valuesLoaded = true;
		}, function(response) {

		});
	}

	$scope.addValue = function() {
		$scope.contractValueForm.$setPristine();
		$scope.valueAction = "Add";
		$scope.showValueDialog = true;
		$scope.value = {
			contract : new ValueDO($scope.contract.id, null),
			group : new ValueDO(null, "VALUE"),
			cycle : {
				frequency : null,
				value : null
			},
			from : null,
			to : null
		};
	}

	$scope.editValue = function() {
		$scope.valueAction = "Edit";
		if ($scope.value == null)
			return;
		var params = {
			id : $scope.value.id
		};
		loadContractDetail(applicationService, params, function(response) {
			if (response.data && response.data.length == 1) {
				$scope.value = response.data[0];
				$scope.showValueDialog = true;
			}
		}, function(response) {

		});
	}

	$scope.setSelectedValue = function(value) {
		$scope.value = {
			id : value.id
		};
	}

	$scope.closeValueDialog = function() {
		$scope.showValueDialog = false;
		$scope.value = {};
		$scope.valuealert = {};
		$scope.loadValue();
	}
	$scope.saveValue = function() {

		$scope.contractValueFormsubmitted = true;

		if ($scope.contractValueForm.contractVName.$valid
				&& $scope.contractValueForm.projectedValue.$valid) {

			$scope.contractValueFormsubmitted = false;

			if ($scope.value && $scope.value.value
					&& $scope.value.value.projected)
				$scope.value.value.projected = $scope.value.value.projected
						.replace(/\D/g, '');

			if ($scope.value && $scope.value.value && $scope.value.value.actual)
				$scope.value.value.actual = $scope.value.value.actual.replace(
						/\D/g, '');

			if ($scope.value.value.projected == '') {
				$scope.valuealert = {
					type : AlertType.ERROR,
					body : "Only values allowed for the value field"
				};
				return;
			}
			saveContractDetail(applicationService, $scope.value, function(
					response) {
				$scope.valuealert = response.data;
			}, function(response) {

			})

		} else {
			$scope.valuealert = {
				type : AlertType.ERROR,
				body : "Not all required fields for contract value have been filled in"
			};
		}
	}

	$scope.valueDescription = function(item) {
		if (!item.period)
			return null;
		if (!item.period.cycle && item.period.from && item.period.to)
			return item.period.from + '-' + item.period.to;

		var cycle = null;
		for ( var idx in $scope.cycles)
			if ($scope.cycles[idx].id === item.period.cycle.frequency)
				cycle = $scope.cycles[idx];
		if (cycle && cycle.description)
			return cycle.value + ' ' + item.period.cycle.value;

	}

	$scope.deleteValue = function() {
		deleteContractDetail(applicationService, $scope.value.id, function(
				response) {
			var data = response.data;
			$scope.alert = response.data;
			if (data && data.type === AlertType.OK)
				$scope.loadValue();
		}, function(response) {

		});
	}
}

function contractAddEditControllerCCNManagement($scope, $filter,
		applicationService) {
	$scope.addCCN = function() {
		$scope.ccnAction = "Add";
		$scope.showCCNDialog = true;
		$scope.ccn = {
			contract : new ValueDO($scope.contract.id, ""),
			group : new ValueDO("", "CCN"),
			ccn : {
				period : {
					from : null,
					to : null
				}
			}
		};
	}

	$scope.editCCN = function() {
		$scope.ccnAction = "Edit";
		if ($scope.ccn == null)
			return;
		var params = {
			id : $scope.ccn.id
		};
		loadContractDetail(applicationService, params, function(response) {
			if (response.data && response.data.length == 1) {
				$scope.ccn = response.data[0];
				$scope.ccn.ccn.period.from = $filter('date')(
						$scope.ccn.ccn.period.from, 'dd/MM/yyyy');
				$scope.ccn.ccn.period.to = $filter('date')(
						$scope.ccn.ccn.period.to, 'dd/MM/yyyy');
				$scope.showCCNDialog = true;
			}
		}, function(response) {

		});
	}

	$scope.loadCCN = function() {
		$scope.ccns = {};
		var params = {
			contractId : $scope.contract.id,
			group : "CCN"
		};
		$scope.ccnLoaded = false;
		loadContractDetail(applicationService, params, function(response) {
			$scope.ccns = response.data;
			$scope.ccnsResults = $scope.ccns;
			$scope.ccnLoaded = true;
		}, function(response) {

		});
	}

	$scope.saveCCN = function() {
		$scope.ccnFormSubmitted = true;
		if ($scope.ccnForm.$valid) {
			$scope.ccnFormSubmitted = false;

			// $scope.ccnForm.contract_CCN_value.$setPristine();
			// $scope.ccnForm.contract_CCN_from.$setPristine();
			// $scope.ccnForm.contract_CCN_to.$setPristine();
			// $scope.ccnForm.CCN_description.$setPristine();

			saveContractDetail(applicationService, $scope.ccn, function(
					response) {
				$scope.alert = response.data;
			}, function(response) {

			})
		} else {
			$scope.alert = {
				body : "Not all required fields for CCN have been filled in",
				type : AlertType.ERROR
			};
		}
	}

	$scope.setSelectedCCN = function(ccn) {
		$scope.ccn = {
			id : ccn.id
		};
	}

	$scope.closeCCNDialog = function() {
		$scope.showCCNDialog = false;
		$scope.ccn = {};
		$scope.alert = {};
		$scope.loadCCN();
	}

	$scope.deleteCCN = function() {
		deleteContractDetail(applicationService, $scope.ccn.id, function(
				response) {
			var data = response.data;
			$scope.alert = response.data;
			if (data && data.type === AlertType.OK)
				$scope.loadCCN();
		}, function(response) {

		});
	}

}

function contractAddEditControllerDetailManagement($scope, applicationService) {
	$scope.loadDetails = function() {
		var params = {
			contractId : $scope.contract.id,
			group : "^(?!.*(VALUE|SUMMARY|CCN|CONTRACT_TERM_PERIOD|CONTRACT_TERM_NOTICE|CONTRACT_TERM_GOTO_MARKET)).*$"
		}
		$scope.detailsLoaded = false;
		$scope.details = {};
		loadContractDetail(applicationService, params, function(response) {
			$scope.details = response.data;
			$scope.detailsResults = response.data;
			$scope.detailsLoaded = true;
		}, function(response) {

		});
	}

	$scope.addDetail = function() {
		$scope.detailAction = "Add";
		$scope.showDetailDialog = true;
		$scope.detail = {
			contract : new ValueDO($scope.contract.id, null)
		};
	}

	$scope.saveDetail = function() {
		$scope.detailFormSubmitted = true;

		if ($scope.detailForm.$valid) {
			$scope.detailFormSubmitted = false;

			saveContractDetail(applicationService, $scope.detail, function(
					response) {
				$scope.alert = response.data;
			}, function(response) {

			})
		} else {
			$scope.alert = {
				body : "Not all required fields for Details have been filled in",
				type : AlertType.ERROR
			};
		}
	}

	$scope.editDetail = function() {
		$scope.detailAction = "Edit";
		if ($scope.detail == null)
			return;
		var params = {
			id : $scope.detail.id
		};
		loadContractDetail(applicationService, params, function(response) {
			if (response.data && response.data.length == 1) {
				$scope.detail = response.data[0];
				$scope.showDetailDialog = true;
			}
		}, function(response) {

		});
	};

	$scope.deleteDetail = function() {
		deleteContractDetail(applicationService, $scope.detail.id, function(
				response) {
			var data = response.data;
			$scope.alert = response.data;
			if (data && data.type === AlertType.OK)
				$scope.loadDetails();
		}, function(response) {

		});
	}

	$scope.setSelectedDetail = function(detail) {
		$scope.detail = {
			id : detail.id
		};
	}

	$scope.closeDetailDialog = function() {
		$scope.showDetailDialog = false;
		$scope.detail = {};
		$scope.alert = {};
		;
		$scope.loadDetails();
	}
}

function contractAddEditControllerSummaryManagement($scope, applicationService) {
	$scope.addSummary = function() {
		$scope.summaryAction = "Add";
		$scope.showSummaryDialog = true;
		$scope.summary = {
			contract : {id: $scope.contract.id},
			group : {value: "SUMMARY"},
			name: null,
			text: null
		};
	}

	$scope.editSummary = function() {
		$scope.summaryAction = "Edit";
		if ($scope.summary == null)
			return;
		var params = {
			id : $scope.summary.id
		};
		loadContractDetail(applicationService, params, function(response) {
			if (response.data && response.data.length == 1) {
				$scope.summary = response.data[0];
				$scope.showSummaryDialog = true;
			}
		}, function(response) {

		});
	}

	$scope.loadSummary = function() {
		$scope.summarys = {};
		var params = {
			contractId : $scope.contract.id,
			group : "SUMMARY"
		};
		$scope.summaryLoaded = false;
		loadContractDetail(applicationService, params, function(response) {
			$scope.summarys = response.data;
			$scope.summarysResults = $scope.summarys;
			$scope.summaryLoaded = true;
		}, function(response) {

		});
	}

	$scope.saveSummary = function() {
		$scope.summaryFormSubmitted = true;
		if ($scope.summaryForm.$valid) {
			$scope.summaryFormSubmitted = false;

			saveContractDetail(applicationService, $scope.summary, function(
					response) {
				$scope.alert = response.data;

				// $scope.summaryForm.contractSummaryValue_id.$setPristine();
				// $scope.summaryForm.contractSummaryValue_text.$setPristine();
			}, function(response) {

			})
		} else {
			$scope.alert = {
				body : "Not all required fields for Summary have been filled in",
				type : AlertType.ERROR
			};
		}
	}

	$scope.setSelectedSummary = function(summary) {
		$scope.summary = {
			id : summary.id
		};
	}

	$scope.closeSummaryDialog = function() {
		$scope.showSummaryDialog = false;
		$scope.summary = {};
		$scope.alert = {};
		$scope.loadSummary();
	}

	$scope.deleteSummary = function() {
		deleteContractDetail(applicationService, $scope.summary.id, function(
				response) {
			var data = response.data;
			$scope.alert = response.data;
			if (data && data.type === AlertType.OK)
				$scope.loadSummary();
		}, function(response) {

		});
	}

}

function contractAddEditControllerDocumentManagement($scope, $window,
		applicationService, Upload) {
	$scope.documentFile = null;
	$scope.loadDocuments = function() {
		var params = {
			moduleId : $scope.moduleId,
			externalId : $scope.contract.id
		};
		$scope.documentsLoaded = false;
		loadDocuments(applicationService, params, function(response) {
			$scope.documents = response.data;
			$scope.documentsResults = $scope.documents;
			$scope.documentsLoaded = true;
		}, function(response) {

		});
	}

	$scope.createDocument = function() {
		$scope.document = {
			type : new ValueDO(null, null),
			group : new ValueDO(null, null),
			status : new ValueDO(null, null),
			module : new ValueDO(null, null),
			external : new ValueDO(null, null),
			file : {
				id : null
			}
		};
		$scope.documentAction = "Create";
		$scope.showDocumentDialog = true;
	}

	$scope.closeDocumentDialog = function() {
		$scope.showDocumentDialog = false;
		$scope.loadDocuments();
		$scope.document = {};
		$scope.alert = {};
	}

	$scope.editDocument = function() {
		$scope.documentAction = "Edit";

		if ($scope.document && $scope.document.id && $scope.document.id != "") {
			var params = {
				moduleId : $scope.moduleId,
				external : new ValueDO($scope.contract.id, ""),
				documentId : $scope.document.id
			};
			loadDocuments(applicationService, params, function(response) {
				if (response.data && response.data.length == 1) {
					$scope.document = response.data[0];
					$scope.showDocumentDialog = true;
				}
			}, function(reponse) {

			});
		}
	}

	$scope.deleteDocument = function() {

	}

	$scope.downloadDocument = function() {
		if (!$scope.document || !$scope.document.file.id
				|| $scope.document.file.id == '')
			return;
		var endpoint = "".concat("/contractic/organisation/",
				$scope.organisationId, "/file/", $scope.document.file.id);
		$window.location.href = endpoint;
	}

	$scope.uploadFile = function() {
		var file = $scope.documentFile;
		var uploadUrl = "".concat("/contractic/organisation/",
				$scope.organisationId, "/file");

		Upload.upload({
			url : uploadUrl,
			data : {
				file : file
			},
			headers : applicationService.getAuthHeader()
		}).then(
				function(response) {
					var data = response.data;
					if (data && data.ref) {
						$scope.document.file.id = data.ref;

						saveDocument(applicationService, $scope.document,
								function(response) {
									$scope.alert = response.data;
								}, function(response) {
									var data = response.data;
									if (data.type)
										$scope.alert = data;
								});
					}
				},
				function(response) {
					console.log('Error status: ' + resp.status);
				},
				function(evt) {
					var progressPercentage = parseInt(100.0 * evt.loaded
							/ evt.total);
					console.log('progress: ' + progressPercentage + '% '
							+ evt.config.data.file.name);
				});
	};

	$scope.saveDocument = function() {
		$scope.documentsFormSubmitted = true;

		if ($scope.documentsForm.$valid) {
			$scope.documentsFormSubmitted = false;

			$scope.document.external.id = $scope.contract.id;
			$scope.document.module.id = $scope.moduleId;

			if (!$scope.document.external.id
					|| $scope.document.external.id == '') {
				$scope.alert = {
					body : "No externalId supplied",
					type : AlertType.ERROR
				};
				return;
			}

			/*
			 * If we have a file - that is normally when we are creating a new
			 * document
			 */
			if ($scope.documentFile) {
				$scope.uploadFile();
			} else if ($scope.document.file.id && $scope.document.file.id != "") {
				/* if we are just editing the document */
				saveDocument(applicationService, $scope.document, function(
						response) {
					$scope.alert = response.data;
				}, function(response) {
					var data = response.data;
					if (data.type)
						$scope.alert = data;
				});
			} else {
				$scope.alert = {
					body : "Please upload a document",
					type : AlertType.ERROR
				};
			}

		} else {
			$scope.alert = {
				body : "Not all required fields for Document have been filled in",
				type : AlertType.ERROR
			};
		}
	}

	$scope.setSelectedDocument = function(document) {
		$scope.document = {
			id : document.id,
			file : {
				id : document.file.id
			}
		};
	}
}

function contractAddEditControllerCalendarManagement($scope,
		applicationService, taskService) {

	$scope.saveTask = function() {
		var saveTaskService = taskService.getSaveTask();
		if (saveTaskService) {
			saveTaskService(function(response) {
				$scope.alert = response.data;
			});
		}
	};

	$scope.loadedSubTaskParent = {};
	$scope.showSubTasks = function(idx) {
		loadSubTasks(applicationService, $scope.tasks, idx,
				$scope.loadedSubTaskParent);
	}

	$scope.setSelectedTask = function(task) {
		$scope.task = {
			id : task.id
		};
	}

	$scope.loadTasks = function() {
		$scope.tasks = [];
		function makeExternalObject(moduleId, externalId) {
			return {
				moduleId : moduleId,
				externalId : externalId
			}
		}
		var external = [];
		external.push(makeExternalObject($scope.moduleId, $scope.contract.id));

		var contractDetailModuleId = applicationService.get("Contract Detail");
		for (var idx = 0; idx < $scope.contract.term.length; ++idx) {
			if ($scope.contract.term[idx].period.id)
				external.push(makeExternalObject(contractDetailModuleId,
						$scope.contract.term[idx].period.id));
			if ($scope.contract.term[idx].notice.id)
				external.push(makeExternalObject(contractDetailModuleId,
						$scope.contract.term[idx].notice.id));
			if ($scope.contract.term[idx].gotomarket.id)
				external.push(makeExternalObject(contractDetailModuleId,
						$scope.contract.term[idx].gotomarket.id));
		}

		for (var idx = 0; idx < external.length; ++idx) {
			loadTasks(applicationService, {
				moduleId : external[idx].moduleId,
				externalId : external[idx].externalId
			}, function(response) {
				var data = response.data;
				$scope.tasks = $scope.tasks.concat(data);
				$scope.calendarResults = $scope.tasks;
				$scope.tasksLoaded = true;
			}, function(response) {

			});
		}
	}

	$scope.editTask = function() {
		var loadTask = taskService.getLoadTask();
		loadTask($scope.task.id, function() {
			$scope.calendarAction = "Edit Task";
			$scope.showTaskDialog = true;
		});
	}

	$scope.createTask = function() {
		$scope.task = {
			external : $scope.contract,
			person : new ValueDO(null, null),
			status : new ValueDO(null, null),
			module : new ValueDO($scope.moduleId, null),
			parent : !$scope.task ? null : new ValueDO($scope.task.id,
					$scope.task.title)
		};
		$scope.calendarAction = "Create Task";
		var createTask = taskService.getCreateTask();
		createTask($scope.task);
		$scope.showTaskDialog = true;
	}

	$scope.setSelectedTask = function(task) {
		$scope.task = task;
	}

	$scope.closeTaskDialog = function() {
		$scope.showTaskDialog = false;
		$scope.loadTasks();
		$scope.task = {};
		$scope.alert = {};
		$scope.loadedSubTaskParent = {};
	}
}

function contractAddEditControllerAutoComplete($scope, applicationService) {
	/* Auto complete */
	$scope.autoComplete('#personName', function(request, response) {
		var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params : {
				q : request.term,
				autoComplete : true
			}
		};

		var organisationId = applicationService.get(ORGANISATION_ID_KEY);
		applicationService.httpget(config, "".concat(
				"/contractic/organisation/", organisationId, "/search/person"),
				function(result) {
					var data = result.data;
					response(data);
				}, function(response) {

				});
	}, function(event, ui) {
		$scope.contract.owner.id = ui.item.id;
		$scope.contract.owner.value = ui.item.value;
	});

	/* Auto complete task assignee */

	$scope.autoComplete('#taskPerson', function(request, response) {
		var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params : {
				q : request.term,
				autoComplete : true
			}
		};

		var organisationId = applicationService.get(ORGANISATION_ID_KEY);
		applicationService.httpget(config, "".concat(
				"/contractic/organisation/", organisationId, "/search/person"),
				function(result) {
					var data = result.data;
					response(data);
				}, function(response) {

				});
	}, function(event, ui) {
		$scope.task.person.id = ui.item.id;
		$scope.task.person.value = ui.item.value;
	});

	/* Auto complete supplier name */
	$scope.autoComplete('#supplierName', function(request, response) {
		var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params : {
				q : request.term,
				autoComplete : true
			}
		};

		var organisationId = applicationService.get(ORGANISATION_ID_KEY);
		applicationService.httpget(config, ""
				.concat("/contractic/organisation/", organisationId,
						"/search/supplier"), function(result) {
			var data = result.data;
			response(data);
		}, function(response) {

		});
	}, function(event, ui) {
		$scope.contract.supplier.id = ui.item.id;
		$scope.contract.supplier.value = ui.item.value;
	});

	/* Auto complete contract detail group */
	$scope.autoComplete('#contractDetailGroup', function(req, res) {
		var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params : {
				q : req.term,
				autoComplete : true,
				moduleId : $scope.moduleId
			}
		};
		applicationService.httpget(config, "".concat(
				"/contractic/organisation/", applicationService
						.get(ORGANISATION_ID_KEY), "/search/group"), function(
				response) {
			res(response.data);
		}, function(response) {

		});
	}, function(event, ui) {
		$scope.detail.group.id = ui.item.id;
		$scope.detail.group.value = ui.item.value;
	});

	// departmentName
	$scope.autoComplete('#departmentName', function(req, res) {
		var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params : {
				q : req.term,
				autoComplete : true,
				moduleId : $scope.moduleId
			}
		};
		applicationService.httpget(config, "".concat(
				"/contractic/organisation/", applicationService
						.get(ORGANISATION_ID_KEY), "/person/", user.person.id),
				function(response) {
					var data = response.data.department;
					var results = [];
					for (var idx = 0; idx < data.length; ++idx) {
						results.push({
							id : data[idx].id,
							value : data[idx].name
						});
					}
					res(results);
				}, function(response) {

				});
	}, function(event, ui) {
		$scope.contract.department.id = ui.item.id;
		$scope.contract.department.name = ui.item.value;
	});
}

function contractAddEditController($scope, $rootScope, $window, $filter,
		applicationService, contractService, taskService, documentService,
		Upload, noteService, reportService, sessionService, filterFilter) {

	sessionService.validate();

	$scope.fileicon = documentService.fileicon();
	$scope.autoComplete = autoComplete;
	$scope.searchContractDetails = '';
	$scope.searchSummaries = '';
	$scope.searchCCN = '';
	$scope.searchCalendar = '';
	$scope.searchDocuments = '';
	$scope.searchNotes = '';
	$scope.searchAudits = '';

	$scope.currentPage = 0;
	$scope.pageSize = 10;

	$scope.currentBespokeDetailPage = 0;
	$scope.bespokeDetailPageSize = 10;

	$scope.currentSummaryPage = 0;
	$scope.summaryPageSize = 10;

	$scope.currentCNNPage = 0;
	$scope.ccnPageSize = 10;

	$scope.currentCalendarPage = 0;
	$scope.calendarPageSize = 10;

	$scope.currentDocumentPage = 0;
	$scope.documentPageSize = 10;

	$scope.numberOfBespokeDetailPages = function() {

		return $scope.details == null ? 0 : Math.ceil($scope.details.length
				/ $scope.pageSize);
	}
	
	$scope.$watch('searchContractDetails', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.detailsResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.details = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentPage = 0;
	}, true);

	$scope.numberOfSummaryPages = function() {

		return $scope.summarys == null ? 0 : Math.ceil($scope.summarys.length
				/ $scope.summaryPageSize);
	}
	
	$scope.$watch('searchSummaries', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.summarysResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.summarys = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentSummaryPage = 0;
	}, true);
	
	

	$scope.numberOfCCNPages = function() {

		return $scope.ccns == null ? 0 : Math.ceil($scope.ccns.length
				/ $scope.ccnPageSize);
	}
	
	$scope.$watch('searchCCN', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.ccnsResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.ccns = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentCNNPage = 0;
	}, true);
	
	

	$scope.numberOfCalendarPages = function() {

		return $scope.tasks == null ? 0 : Math.ceil($scope.tasks.length
				/ $scope.calendarPageSize);
	}
	
	$scope.$watch('searchCalendar', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.calendarResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.tasks = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentCalendarPage = 0;
	}, true);

	$scope.numberOfDocumentPages = function() {

		return $scope.documents == null ? 0 : Math.ceil($scope.documents.length
				/ $scope.documentPageSize);

	}
	
	$scope.$watch('searchDocuments', function(newVal, oldVal) {
		/* We then filter $scope.items for the new value entered */
		var filtered = filterFilter($scope.documentsResults, newVal);
		/* Now we have the filtered results in an array. So we count them */

		$scope.documents = filtered;
		/* We then update noOfPages with the current count */
		
		$scope.currentDocumentPage = 0;
	}, true);

	$scope.contract = {
		id : contractService.get(CONTRACT_ID_KEY),
		supplier : new ValueDO(null, null),
		owner : new ValueDO(null, null),
		type : new ValueDO(null, null),
		status : new ValueDO(null, null),
		department : {},
		term : []
	};

	var user = sessionService.getUser();
	/*
	 * if(user && user.person && user.person.department){ var department =
	 * user.person.department; $scope.contract.department = new
	 * ValueDO(department.id, department.name); }
	 */

	$scope.AlertType = AlertType;
	$scope.addressTypes = applicationService.get(ADDRESS_TYPES_KEY);
	$scope.cycles = [ {
		id : "D",
		value : "Days",
		description : "Daily"
	}, {
		id : "M",
		value : "Months",
		description : "Monthly"
	}, {
		id : "Q",
		value : "Quarters",
		description : "Quarterly"
	}, {
		id : "Y",
		value : "Years",
		description : "Annually"
	} ];

	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.moduleId = applicationService.get("Contract");
	$scope.externalId = $scope.contract.id;

	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.departments = user.person.department;
	$scope.currencies = applicationService.get(SYSTEM_CURRENCYSET_KEY);

	$scope.loadProcedures = function() {
		var groupsets = applicationService.get(SYSTEM_GROUPSET_KEY);
		var proceduregroupset = groupsets["PROCEDURE"];
		$scope.proceduresLoaded = false;
		loadGroups(applicationService, {
			groupSetId : proceduregroupset.id
		/* , moduleId: $scope.moduleId */}, function(response) {
			var data = response.data;
			$scope.procedures = data;
			$scope.proceduresLoaded = true;
		}, function(response) {

		})
	}

	/*
	 * autoCompleteAddress('#taskContact', function(event, ui){ })
	 */

	$scope.contractList = function() {
		$scope.go('/contracts');
	}

	$scope.saveContractReturn = function() {
		$scope.saveContract($scope.contractList);
	}

	$scope.saveContractContinue = function() {
		$scope.saveContract(null);
	}

	$scope.saveContract = function(then) {
		$scope.userFormSubmitted = true;
		if ($scope.userForm.middleName.$valid
				&& $scope.userForm.type.$valid
				&& $scope.userForm.text1.$valid
				&& $scope.userForm.type.$valid
				&& $scope.userForm.depType.$valid
				&& $scope.userForm.supplier.$valid
				&& $scope.userForm.initial_start.$valid
				&& $scope.userForm.manager.$valid
				&& $scope.userForm.initialValue.$valid
				&& $scope.userForm.currency.$valid
				&& $scope.userForm.status.$valid
				&& ($scope.oneOffPurchase || ($scope.contract.term.length > 0 && $scope.contract.term[0].period != null))) {
			$scope.userFormSubmitted = false;

			$scope.saveTerm();
			$scope.alert = {};
			$scope.contractLoaded = false;
			saveContract(applicationService, $scope.contract,
					function(response) {
						try {
							var data = response.data;
							$scope.alert = data;
							if ($scope.alert.type != AlertType.OK)
								return;
							if (data && data.ref) {
								$scope.contract = data.ref;
								noteService.setExternalId($scope.contract.id);
							}

							if (then) {
								contractService.set('contractSaved', data);
								then();
							}
						} finally {
							$scope.contractLoaded = true;
						}
					}, function(response) {
						if (response.status == 400) {
							$scope.alert = {
								body : response.data.body,
								type : AlertType.ERROR
							};
						}
						$scope.contractLoaded = true;
					});
		} else {
			$scope.alert = {
				body : "Not all required fields for the Contract have been filled in",
				type : AlertType.ERROR
			};
		}
	};

	$scope.loadHeader = function() {
		if (!$scope.contract.id || $scope.contract.id == ''){
			$scope.contractLoaded = true;
			return;
		}
		$scope.contractLoaded = false;
		loadContractHeader(applicationService, $scope.contract.id, function(
				response) {
			try {
				var contract = response.data;
				if (contract) {
					contract.startDate = $filter('date')(contract.startDate,
							'dd/MM/yyyy');
					contract.endDate = $filter('date')(contract.endDate,
							'dd/MM/yyyy');
				}
				$scope.oneOffPurchase = contract.term.length == 0;
				$scope.contract = contract;
			} finally {
				$scope.contractLoaded = true;
			}
		}, function(response) {

		});
		/* $scope.loadValue(); */
	}

	/* Contract Note Management */

	/* Clear any data from a previous module */
	noteService.clear();
	$scope.loadNotes = function() {
		$scope.$broadcast('loadNotes');
	}

	noteService.setOnSave(function(alert) {
		$scope.alert = alert;
	});

	$scope.$on('saveNote', function(e, alert) {
		$scope.alert = alert;
	});

	/* Audit Log Management */
	$scope.loadAudit = function() {
		$scope.$broadcast('loadAudit');
	}

	/* Contract Calendar Task Management */

	$scope.loadContractStatuses = function() {
		loadStatuses(applicationService, {
			moduleId : applicationService.get("Contract")
		}, function(response) {
			$scope.statuses = response.data;
			$scope.statusesLoaded = true;
		}, function() {

		});
	}

	$scope.AlertType = AlertType;
	loadContractTypes(applicationService, null, function(response) {
		$scope.contractTypes = response.data;
	}, function(response) {

	});

	$scope.loadContractStatuses();

	loadDocumentTypes(applicationService, null, function(response) {
		$scope.documentTypes = response.data;
	}, function(response) {

	});

	contractAddEditControllerAutoComplete($scope, applicationService);
	contractAddEditControllerExtensionManagement($scope, applicationService);
	contractAddEditControllerValueManagement($scope, applicationService);
	contractAddEditControllerCCNManagement($scope, $filter, applicationService);
	contractAddEditControllerSummaryManagement($scope, applicationService);
	contractAddEditControllerDetailManagement($scope, applicationService);
	contractAddEditControllerDocumentManagement($scope, $window,
			applicationService, Upload);
	contractAddEditControllerCalendarManagement($scope, applicationService,
			taskService);

	$scope.loadHeader();
	$scope.loadProcedures();
}

function contractListController($scope, applicationService, contractService,
		sessionService, $filter, filterFilter) {

	sessionService.validate();

	$scope.contracts = [ {} ];
	$scope.contract = {};
	$scope.organisationId = applicationService.get(ORGANISATION_ID_KEY);
	$scope.alert = {};
	$scope.go = applicationService.go;
	$scope.searchContracts = '';

	$scope.currentContractPage = 0;
	$scope.contractPageSize = 10;

	$scope.numberOfContractManagerPages = function() {

		return $scope.contracts == null ? 0 : Math.ceil($scope.contracts.length
				/ $scope.contractPageSize);
	}
	
	$scope.$watch('searchContracts', function(newVal, oldVal) {
		
		var filtered = filterFilter($scope.contractResults, newVal);
		

		$scope.contracts = filtered;
		
		
		$scope.currentContractPage = 0;
	}, true);

	// Clear the contract id from the contract service
	contractService.set(CONTRACT_ID_KEY, null);

	$scope.AlertType = AlertType;

	$scope.newContract = function() {
		$scope.go('/contract/new');
	};

	$scope.editContract = function() {
		if ($scope.contract && $scope.contract.id && $scope.contract.id != "") {
			contractService.set(CONTRACT_ID_KEY, $scope.contract.id);
			$scope.go('/contract/edit');
		}
	};

	$scope.refreshList = function() {
		$scope.contractsLoaded = false;
		loadContracts(applicationService, null, function(response) {
			var data = response.data;
			$scope.contracts = data;
			$scope.contractResults = $scope.contracts;
			$scope.contractsLoaded = true;
		}, function(response) {

		});
	}
	
	

	$scope.setSelected = function(contract) {
		$scope.contract = {
			id : contract.id
		};
	}

	var saveContractAlert = contractService.get('contractSaved');
	if (saveContractAlert) {
		$scope.alert = saveContractAlert;
		contractService.set('contractSaved', null);
	}

	$scope.initializeCurrencies = function() {
		$scope.currencies = {};
		var currencies = applicationService.get(SYSTEM_CURRENCYSET_KEY);
		for (var idx = 0; idx < currencies.length; ++idx) {
			$scope.currencies[currencies[idx].id] = currencies[idx].value;
		}
	}

	$scope.initializeCurrencies();
	$scope.refreshList();
}

var contract = angular.module("contract", [ 'ngRoute' ]);

/* Add/Edit Controller */
contract.controller("contractAddEditController", contractAddEditController);
/* Contract List Controller */
contract.controller("contractListController", contractListController);

/* Contract Service */
contract.service('contractService', [ '$location', function($location) {

	// private variable
	var hashtable = {};
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},

		go : function(path) {
			$location.path(path);
		}
	}
} ]);

contract.filter('period', function() {

	return function(cycle) {
		var result = null;
		if (cycle == 'D') {
			result = 'Day(s)';
		} else if (cycle == 'W') {
			result = 'Week(s)';
		} else if (cycle == 'M') {
			result = 'Month(s)';
		} else if (cycle == 'Y') {
			result = 'Year(s)';
		}
		return result;
	};
});

contract.filter('owner', function() {
	    return function (value) {
	    	var str1 = value;
	    	var str2 = str1.replace(/\,/g,"");
	    	return str2;
	    }; 
	});

/* Add/Edit Controller */
contract.controller("contractAddEditController", contractAddEditController);

/* Contract List Controller */
contract.controller("contractListController", contractListController);

/*
 * // configure our routes contract.config(function($routeProvider) {
 * $routeProvider // route for the home page .when('/contract/new', {
 * templateUrl : window.viewsBase + '/contract-add-edit.html' }) // route for
 * the about page .when('/contract/edit', { templateUrl : window.viewsBase +
 * '/contract-add-edit.html' }); });
 */