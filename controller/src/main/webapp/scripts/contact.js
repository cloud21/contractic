/**
 * Contact management module
 */

function loadContacts(applicationService, params, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params: params
		};
	
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/contact");
	applicationService.httpget(config, request, success, error);
}

function deleteContact(applicationService, contact, success, error) {
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var endpoint = "".concat("/contractic/organisation/", organisationId, "/contact");
	applicationService.httpdel(config, endpoint, contact, success, error);
	
}

function saveContact(applicationService, contact, success, error) {
	
	var config = {
		headers : applicationService.getAuthHeader(),
		cache : false
	};
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	var endpoint = "".concat("/contractic/organisation/", organisationId, "/contact");
	
	if (contact.id && contact.id != '') {
		applicationService.httpput(config, endpoint, contact, success, error);
	} else {
		applicationService.httppost(config, endpoint, contact, success, error);
	}
}

function contactController($scope, $http, applicationService, contactService){
	$scope.AlertType = AlertType;
	$scope.moduleId = applicationService.get($scope.moduleName);
	$scope.externalId = $scope.externalId || contactService.getExternalId();	
    $scope.addressTypes = applicationService.get(ADDRESS_TYPES_KEY);
    $scope.params = contactService.getParams();
    $scope.contacts = [];
    var enableEdit = contactService.getEnableEdit();
    $scope.enableEdit = (enableEdit === undefined) ? true : enableEdit;
    
    $scope.addressTypeTable = {};
    $scope.makeAddressTypeTable = function(){
		for(var idx in $scope.addressTypes)
			$scope.addressTypeTable[$scope.addressTypes[idx].id] = $scope.addressTypes[idx].value;
    }
    
	autoComplete('#contactValue', 
			function(request, response){
				var config = {
						headers : applicationService.getAuthHeader(),
						cache : false,
						params: {q : request.term, autoComplete : true}
					};
		
					var organisationId = applicationService.get(ORGANISATION_ID_KEY);
					if(!$scope.contact.type || !$scope.contact.type.id)
						return;
					applicationService.httpget(config, "".concat("/contractic/organisation/", organisationId, "/search/address"), 
						function(result) {
							var data = result.data;
							response(data);
							},
						function(response){
								
							});
			}, 
			function(event, ui){
				$scope.contact.address.id = ui.item.id;
				$scope.contact.address.value = ui.item.value;
			});
    
	$scope.editContact = function(item){
		$scope.contact = item;
	}
	
	$scope.saveContact = function(){
		//$scope.contact.externalId = $scope.externalId || contactService.getExternalId();
		//$scope.contact.moduleId = $scope.moduleId;
		if(!$scope.contacts)
			return;
		
		for(var idx=0; idx<$scope.contacts.length; ++idx){
			var contact = $scope.contacts[idx];
			saveContact(applicationService, contact, function(response) {
			}, function(response) {
				
			});
		}
	}
	
	$scope.loadContacts = function(){
		$scope.contactsLoaded = false;
		var params = contactService.getParams();
		if(!params){
			params = [{moduleId: $scope.moduleId, externalId: $scope.externalId || contactService.getExternalId()}];
		}
		for(var idx=0; idx<params.length; ++idx){
			var moduleId = params[idx].moduleId;
			var externalId = params[idx].externalId;
			if(!moduleId || !externalId)
				continue;
			loadContacts(applicationService, {moduleId: moduleId, externalId : externalId}, function(response) {
				$scope.contacts = $scope.contacts.concat(response.data);
				$scope.contactsLoaded = true;
			}, function (reponse){
				
			});
		}
	}
	
	$scope.getNewContact = function(){
		return {
				type: {id: null, value: null},
				moduleId: $scope.moduleId,
				value: null,
				externalId: $scope.externalId || contactService.getExternalId(),
				address: {id: null, value: null}
		}
	}
	$scope.addContact = function(){
		if($scope.contact && $scope.contact.address && $scope.contact.address.value && $scope.contact.address.value!=''){
			$scope.contact.type.value = $scope.addressTypeTable[$scope.contact.type.id];
			/*If we haven't an id, then this is a new contact*/
			if(!$scope.contact.id){
				$scope.contact.moduleId = $scope.moduleId;
				$scope.contact.externalId = $scope.externalId || contactService.getExternalId();
				$scope.contacts.push($scope.contact);
			}
		}
		$scope.contact = $scope.getNewContact();
	}
	
	$scope.removeContact = function(index){
		var contact = $scope.contacts[index];
		deleteContact(applicationService, contact, function(response){
			var data = response.data;
			if(data.type == AlertType.OK){
				$scope.contacts.splice(index, 1);
			}
		}, function(response){
			
		});
	}
	
	$scope.resetContact = function(){
		$scope.contact = {moduleId: $scope.moduleId, externalId: $scope.externalId};
	}
	
	$scope.setSelectedContact = function(item){
		$scope.resetContact();
		$scope.contact = item;
	}
	
	$scope.$on('loadContacts', function(e) {  
		$scope.loadContacts();        
    });
	
	contactService.setOnSave($scope.saveContact);
	$scope.makeAddressTypeTable();
	$scope.loadContacts();
}

var contact = angular.module("contact", [ 'ngRoute' ]);

/* Add/Edit Controller */
contact.controller("contactController", contactController);

/* Contact Service
 * NOTE: Call clear() the first time this service is used within each module
 * in order to clear any data that may have been used by a previous module 
 * */
contact.service('contactService', ['$http', function($http) {

	var ALERT_KEY = "alert.key";
	var EXTERNAL_ID = "external.id";
	var MODULE_NAME = "module.name";
	var ON_SAVE = "on.save";
	var PARAMS = "params";
	var ENABLE_EDIT = "enable.edit";
	// private variable
	var hashtable = {};
	
	
	return {
		set : function(key, value) {
			hashtable[key] = value;
		},
		get : function(key) {
			return hashtable[key];
		},
		setAlert : function(alert){
			hashtable[ALERT_KEY] = alert;
		},
		getAlert : function(){
			return hashtable[ALERT_KEY];
		},
		getEnableEdit : function(){
			return hashtable[ENABLE_EDIT];
		},
		setEnableEdit : function(enable){
			hashtable[ENABLE_EDIT] = enable;
		},
		setExternalId : function(externalId){
			hashtable[EXTERNAL_ID] = externalId;
		},
		getExternalId : function(){
			return hashtable[EXTERNAL_ID];
		},
		getModuleName : function(){
			return hastable[MODULE_NAME];
		},
		setModuleName : function(name){
			hashtable[MODULE_NAME] = name;
		},
		setOnSave : function(onSave){
			hashtable[ON_SAVE] = onSave;
		},
		getOnSave : function(){
			return hashtable[ON_SAVE];
		},
		getParams : function(){
			return hashtable[PARAMS];
		},
		setParams : function(params){
			hashtable[PARAMS] = params;
		},
		clear : function(){
			hashtable = {};
		}
	}
}]);