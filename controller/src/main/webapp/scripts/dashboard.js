/**
 * 
 */

function loadDashboardReport(applicationService, panel, params, success, error){
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);	
	var config = {
			headers : applicationService.getAuthHeader(),
			cache : false,
			params: params
		};
	
	var organisationId = applicationService.get(ORGANISATION_ID_KEY);
	
	var request = "".concat("/contractic/organisation/", organisationId, 
			"/dashboard/", panel);
	applicationService.httpget(config, request, success, error);
}


function contractPanelController($scope, applicationService, contractService, sessionService) {
	
	sessionService.validate();
	
	$scope.go = applicationService.go;
	$scope.dashBoardDataLoaded = false;
	$scope.user = sessionService.getUser();
	
	$scope.loadRecentContracts = function(){
		var profile = $scope.user.profile;
		var count = 10;
		if(profile && profile.dashboard 
				&& profile.dashboard.panels 
				&& profile.dashboard.panels.contract 
				&& profile.dashboard.panels.contract.options
				&& profile.dashboard.panels.contract.options.count)
			count = profile.dashboard.panels.contract.options.count;
			
		loadDashboardReport(applicationService, "contract", {limit: count}, function(response){
			try{
				$scope.contracts = response.data;
			}finally{
				$scope.dashBoardDataLoaded = true;
			}
		}, function(response){
			
		});
	}
	
	$scope.loadPanels = function(){
		$scope.loadRecentContracts();
	}
	
	$scope.hoverIn = function(item){
	    $scope.contract = item;
	};

	$scope.hoverOut = function(){
		$scope.contract = null;
	};
	
	$scope.viewContract = function(item){
		if(item && item.id){
			contractService.set(CONTRACT_ID_KEY, item.id);
			$scope.go('/contract/edit');
		}
	}
	
	$scope.loadPanels();
}

function taskPanelController($scope, applicationService, taskService, sessionService) {
	
	sessionService.validate();
	
	$scope.go = applicationService.go;
	$scope.dashBoardDataLoaded = false;
	$scope.user = sessionService.getUser();
	
	$scope.loadRecentTasks = function(){
		var profile = $scope.user.profile;
		var count = 10;
		if(profile && profile.dashboard 
				&& profile.dashboard.panels 
				&& profile.dashboard.panels.task 
				&& profile.dashboard.panels.task.options
				&& profile.dashboard.panels.task.options.count)
			count = profile.dashboard.panels.task.options.count;
			
		loadDashboardReport(applicationService, "task", {limit: count}, function(response){
			try{
				$scope.tasks = response.data;
			}finally{
				$scope.dashBoardDataLoaded = true;
			}
		}, function(response){
			
		});
	}
	
	$scope.loadPanels = function(){
		$scope.loadRecentTasks();
	}
	
	$scope.hoverIn = function(item){
	    $scope.task = item;
	};

	$scope.hoverOut = function(){
		$scope.task = null;
	};
	
	$scope.viewTask = function(item){
		if(item && item.id){
			taskService.set(TASK_ID_KEY, item.id);
			$scope.go('/task/edit');
		}
	}
	
	$scope.loadPanels();
}

function statisticsPanelController($scope, applicationService, contractService, sessionService) {
	
	sessionService.validate();
	
	$scope.go = applicationService.go;
	$scope.dashBoardDataLoaded = false;
	$scope.statistics = {value: null, status: null, max: null}
	
	$scope.loadStatusPanel = function(){
		loadDashboardReport(applicationService, "statistics", {report: "status"}, function(response){
			try{
				$scope.statistics.status = response.data;
			}finally{
				$scope.dashBoardDataLoaded = true;
			}
		}, function(response){
			
		});
	}
	
	$scope.loadValuePanel = function(){
		loadDashboardReport(applicationService, "statistics", {report: "value"}, function(response){
			try{
				$scope.statistics.value = response.data;
			}finally{
				$scope.dashBoardDataLoaded = true;
			}
		}, function(response){
			
		});
	}
	
	$scope.loadMaxPanel = function(){
		loadDashboardReport(applicationService, "statistics", {report: "max"}, function(response){
			try{
				$scope.statistics.max = response.data;
			}finally{
				$scope.dashBoardDataLoaded = true;
			}
		}, function(response){
			
		});
	}
	
	$scope.loadPanels = function(){
		$scope.loadStatusPanel();
		$scope.loadValuePanel();
		$scope.loadMaxPanel();
	}
	
	$scope.loadPanels();
}

function dashboardController($scope, applicationService, sessionService, sessionService) {
	
	sessionService.validate();
	
	$scope.go = applicationService.go;
	$scope.dashBoardDataLoaded = true;
	$scope.showDashboardConfig = false;
	$scope.selectedPanel = null;
	$scope.user = sessionService.getUser();
	
	$scope.configureDashboard = function(){
		$scope.showDashboardConfig = !$scope.showDashboardConfig;
	}
	$scope.closeDashboardConfig = function(){
		$scope.showDashboardConfig = false;
	}
	$scope.saveDashboardConfig = function(){
		saveUser(applicationService, $scope.user, function(response) {
			$scope.alert = response.data;
		}, function(response){
			
		});
	}
	
	//$scope.loadDashboarOptions
}

var dashboard = angular.module("dashboard", []);
dashboard.controller("contractPanelController", contractPanelController);
dashboard.controller("statisticsPanelController", statisticsPanelController);
dashboard.controller("taskPanelController", taskPanelController);
dashboard.controller("dashboardController", dashboardController);