<!DOCTYPE html>
<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="../css/signin.css" rel="stylesheet" type="text/css" />
<meta charset="ISO-8859-1">
<link rel="icon" href="../images/favicon.ico">
<title>Contractic - Professional Contract Management</title>
</head>
<body>
	<div id="nav" style="vertical-align: top;"><div style="float: right;">Sign Up</div></div>
	<div id="body">
		<div id="header">
			<div align="left" style="float: left; text-align: left;">
				<div>
					<img src="../images/contracticlogo2.png" />
				</div>
				<div style="clear: both;">
					<img src="../images/contractic-tagline.png" />
				</div>
			</div>
			<div align="right" style="float: right">
				<img src="../images/ICHTrust_logo_425x53.jpg" />
			</div>
		</div>
		<div align="center" style="clear: both; padding: 50px; overflow: auto">
		<div align="left" id="form-content"
			style="clear: both; width: 80%;">
			<div id="desc-content"
				style="float: left; overflow: auto; width: 55%">
				<ul>
					<li>
						<h3>Current and Legacy Contract Storage</h3>
						<ul>
							<li><h4>Contract document management</h4></li>
							<li><h4>Scan and upload documents of various formats</h4></li>
						</ul>
					</li>
					<li>
						<h3>Monitor Contract Performance</h3>
						<ul>
							<li><h4>Contract calendar management</h4></li>
							<li><h4>Configure reminder alerts on key contract dates</h4></li>
						</ul>
					</li>
					<li>
						<h3>Scheduled Management Reporting</h3>
						<ul>
							<li><h4>Key Performance Indicators</h4></li>
							<li><h4>Full audit for regulatory reporting</h4></li>
						</ul>
					</li>
				</ul>
			</div>

			<div
				style="float: right; width: 45%; border-radius: 5px; background-color: #b4e4ec; padding: 40px">
				<form class="form-signin">
					<h2 class="form-signin-heading">Please sign in</h2>
					<input type="email" id="inputEmail"
						class="form-control signin-control" placeholder="Email address"
						required autofocus> <input type="password"
						id="inputPassword" class="form-control signin-control"
						placeholder="Password" required>
					<div class="checkbox">
						<label> <input type="checkbox" value="remember-me">
							Remember me
						</label>
					</div>
					<button class="btn btn-lg btn-primary btn-block signin-control"
						type="submit">Sign in</button>
				</form>
			</div>
			<!-- /container -->
		</div>
		</div>
		<div align="center" id="footer" style="clear: both;">Copyright � cloud21.net</div>
	</div>
</body>
</html>