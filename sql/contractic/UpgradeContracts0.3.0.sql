-- GroupSet
INSERT INTO GroupSet(Name, Description, Active, OrganisationId)
SELECT 'PROCEDURE', 'CONTRACT PROCEDUREMENT PROCEDURE', 1, o.Id FROM dbo.Organisation o WHERE o.Id NOT IN (SELECT DISTINCT gs.OrganisationId FROM dbo.GroupSet gs WHERE gs.Name = 'PROCEDURE');
INSERT INTO GroupSet(Name, Description, Active, OrganisationId)
SELECT 'DEPARTMENT', 'CONTRACT PROCEDUREMENT DEPARTMENT', 1, o.Id FROM dbo.Organisation o WHERE o.Id NOT IN (SELECT DISTINCT gs.OrganisationId FROM dbo.GroupSet gs WHERE gs.Name = 'DEPARTMENT');

-- Groups
INSERT INTO [dbo].[Group](Name, OrganisationId, GroupSetId) 
SELECT DISTINCT 'TEMP PROCEDURE', o.Id, gs.Id FROM dbo.Organisation o, [dbo].[GroupSet] gs 
WHERE gs.Name = 'PROCEDURE' AND gs.OrganisationId = o.Id AND o.Id 
NOT IN (SELECT DISTINCT gs.OrganisationId FROM dbo.GroupSet gs INNER JOIN [dbo].[Group] g ON g.OrganisationId = gs.OrganisationId AND g.GroupSetId = gs.Id WHERE gs.Name = 'PROCEDURE');

-- System Groups
INSERT INTO [dbo].[Group](Name, Active, ModuleId, OrganisationId, Description, System)
SELECT g.Name, 1, m.Id, o.Id, g.Name, 1 FROM (VALUES('VALUE'), ('SUMMARY'), ('CCN'), ('CONTRACT_TERM'), ('CONTRACT_TERM_REMINDER')) AS g(Name)
CROSS JOIN Organisation o
INNER JOIN Module m ON m.Name = 'Contract'
LEFT JOIN [dbo].[Group] cd ON cd.Name = g.Name AND m.Id = cd.ModuleId AND o.Id = cd.OrganisationId
WHERE cd.Id IS NULL;

INSERT INTO [dbo].[Group](Name, OrganisationId, GroupSetId) 
SELECT DISTINCT 'TEMP DEPARTMENT', o.Id, gs.Id FROM dbo.Organisation o, [dbo].[GroupSet] gs 
WHERE gs.Name = 'DEPARTMENT' AND gs.OrganisationId = o.Id AND o.Id 
NOT IN (SELECT DISTINCT gs.OrganisationId FROM dbo.GroupSet gs INNER JOIN [dbo].[Group] g ON g.OrganisationId = gs.OrganisationId AND g.GroupSetId = gs.Id WHERE gs.Name = 'DEPARTMENT');

-- GroupObjects
INSERT INTO dbo.GroupObject(GroupId, ModuleId, ExternalId, Active, GroupSetId)
SELECT g.Id AS GroupId, m.Id AS ModuleId, c.Id AS ExternalId, 1, g.GroupSetId FROM [dbo].[Contract] c 
LEFT JOIN (SELECT MAX(g.Id) Id, gs.OrganisationId, g.GroupSetId FROM [dbo].[GroupSet] gs 
INNER JOIN [dbo].[Group] g ON g.GroupSetId = gs.Id WHERE gs.Name = 'PROCEDURE' GROUP BY gs.OrganisationId, g.GroupSetId) g ON g.OrganisationId = c.OrganisationId 
LEFT JOIN dbo.Module m ON m.Name = 'Contract' WHERE c.Id NOT IN 
(SELECT DISTINCT c.Id FROM [dbo].[Contract] c INNER JOIN [dbo].[GroupObject] gro ON gro.ExternalId = c.Id 
INNER JOIN [dbo].[Group] g ON g.Id = gro.GroupId AND g.OrganisationId = c.OrganisationId 
INNER JOIN [dbo].[Module] m ON m.Id = gro.ModuleId AND m.Name = 'Contract'
INNER JOIN [dbo].[GroupSet] gs ON gs.OrganisationId = g.OrganisationId AND gs.Id = g.GroupSetId AND gs.Name = 'PROCEDURE');

INSERT INTO dbo.GroupObject(GroupId, ModuleId, ExternalId, Active, GroupSetId)
SELECT g.Id AS GroupId, m.Id AS ModuleId, c.Id AS ExternalId, 1, g.GroupSetId FROM [dbo].[Contract] c 
LEFT JOIN (SELECT MAX(g.Id) Id, gs.OrganisationId, g.GroupSetId FROM [dbo].[GroupSet] gs 
INNER JOIN [dbo].[Group] g ON g.GroupSetId = gs.Id WHERE gs.Name = 'DEPARTMENT' GROUP BY gs.OrganisationId, g.GroupSetId) g ON g.OrganisationId = c.OrganisationId 
LEFT JOIN dbo.Module m ON m.Name = 'Contract' WHERE c.Id NOT IN 
(SELECT DISTINCT c.Id FROM [dbo].[Contract] c INNER JOIN [dbo].[GroupObject] gro ON gro.ExternalId = c.Id 
INNER JOIN [dbo].[Group] g ON g.Id = gro.GroupId AND g.OrganisationId = c.OrganisationId 
INNER JOIN [dbo].[Module] m ON m.Id = gro.ModuleId AND m.Name = 'Contract'
INNER JOIN [dbo].[GroupSet] gs ON gs.OrganisationId = g.OrganisationId AND gs.Id = g.GroupSetId AND gs.Name = 'DEPARTMENT');


--INSERT INTO dbo.Country(Name, CurrencyCode, CurrencyName, DialingCode) SELECT Name, CurrencyCode, CurrencyName, DialingCode FROM c21cms.dbo.Country;

--INSERT INTO c21cmstest.[dbo].[Report](r.Name, r.Description, r.Command, r.Active, r.ExternalRef)
--SELECT r.Name, r.Description, r.Command, r.Active, r.ExternalRef FROM c21cms.[dbo].[Report] r
--	LEFT JOIN c21cmstest.[dbo].[Report] rt ON r.Name = rt.Name
--	WHERE rt.Name IS NULL;

--INSERT INTO ReportOrganisation([Name], [Description], [ReportId], [OrganisationId], [Active], [Created])
--SELECT r.Name, r.Description, r.Id, o.Id, 1, GETDATE() FROM [dbo].[Report] r 
--	CROSS JOIN [dbo].[Organisation] o
--	LEFT JOIN [dbo].[ReportOrganisation] ro
--	ON r.Id = ro.ReportId AND o.Id = ro.OrganisationId
--WHERE ro.Id IS NULL;
