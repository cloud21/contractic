DECLARE @Params VARCHAR(MAX) = '<?xml version="1.0" ?>
<org.springframework.util.LinkedMultiValueMap>
    <targetMap class="linked-hash-map">
        <entry>
            <string>contractId</string>
            <list>
                <string>1005</string>
            </list>
        </entry>
        <entry>
            <string>expenditure</string>
            <linked-list>
                <string>Support Expenditure</string>
            </linked-list>
        </entry>
        <entry>
            <string>granularity</string>
            <linked-list>
                <string>W</string>
            </linked-list>
        </entry>
        <entry>
            <string>reportId</string>
            <linked-list>
                <string>a9b7ba70783b617e9998dc4dd82eb3c5</string>
            </linked-list>
        </entry>
        <entry>
            <string>run</string>
            <linked-list>
                <string>true</string>
            </linked-list>
        </entry>
    </targetMap>
</org.springframework.util.LinkedMultiValueMap>'
declare @myDoc xml
set @myDoc = CONVERT(XML, @Params);
SELECT @myDoc.value('(//entry[string/text() = "granularity"]/linked-list/string/text())[1]', 'varchar(255)');
EXEC [dbo].[sp_SpendAnalysis] @Params, 1000