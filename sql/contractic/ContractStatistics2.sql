/*
1. Projected spend over 1 year
2. Highest contract value managed
3. Number of open contracts
4. Number of Completed Contracts
5. Total contract value managed by status
*/

/*
<?xml version="1.0" ?><value><actual>511111</actual><projected>21267918</projected></value>
*/
SELECT CONVERT(XML, '<?xml version="1.0" ?><value><actual>511111</actual><projected>21267918</projected></value>')
	.value('(/value/projected)[1]', 'NUMERIC(30,10)');

/*Highest contract value managed by status*/
SELECT s.Name, MAX(CONVERT(XML, cd.Value).value('(/value/projected)[1]', 'NUMERIC(30,10)') )
	FROM [dbo].[Contract] c 
	INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
	INNER JOIN [dbo].[ContractDetail] cd ON c.Id = cd.ContractId
	INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
	INNER JOIN [dbo].[Group] g ON g.Id = cd.GroupId AND g.Name = 'VALUE' AND g.ModuleId = m.Id
	WHERE c.OrganisationId = 1000
	GROUP BY s.Name;

ALTER PROCEDURE sp_ContractStatistics(@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS

	DECLARE @XmlParams XML = CONVERT(XML, @Params);
	DECLARE @Report AS VARCHAR(255);
	
	SELECT @Report = @XmlParams.value('(//entry[string/text() = "report"]/*[position()=2]/*[position()=1]/text())[1]', 'VARCHAR(255)');

	IF @Report = 'max'
	BEGIN
		/*Total contract value managed by status*/
		SELECT name, MAX(total) AS [max] FROM
		(SELECT c.Id, s.Name AS name
				, SUM(CONVERT(XML, cd.Value).value('(/value/projected)[1]', 'NUMERIC(30,10)') ) AS [total]
			FROM [dbo].[Contract] c WITH (NOLOCK)
			INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
			INNER JOIN [dbo].[ContractDetail] cd WITH (NOLOCK) ON c.Id = cd.ContractId
			INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
			INNER JOIN [dbo].[Group] g ON g.Id = cd.GroupId AND g.Name = 'VALUE' AND g.ModuleId = m.Id
			WHERE c.OrganisationId = @OrganisationId
			GROUP BY c.Id, s.Name) a
			GROUP BY name;
	END
	ELSE IF @Report = 'value'
	BEGIN
		/*Total contract value managed by status*/
		SELECT s.Name AS name
				, SUM(CONVERT(XML, cd.Value).value('(/value/projected)[1]', 'NUMERIC(30,10)') ) AS [total]
			FROM [dbo].[Contract] c WITH (NOLOCK)
			INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
			INNER JOIN [dbo].[ContractDetail] cd WITH (NOLOCK) ON c.Id = cd.ContractId
			INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
			INNER JOIN [dbo].[Group] g ON g.Id = cd.GroupId AND g.Name = 'VALUE' AND g.ModuleId = m.Id
			WHERE c.OrganisationId = @OrganisationId
			GROUP BY s.Name;
	END
	ELSE IF @Report = 'status' 
	BEGIN
		/*Contract count by status*/
		SELECT s.Name AS name, COUNT(*) AS [total]
			FROM [dbo].[Contract] c WITH (NOLOCK)
			INNER JOIN [dbo].[Status] s ON s.OrganisationId = c.OrganisationId AND c.StatusId = s.Id
			INNER JOIN [dbo].[Module] m ON s.ModuleId = m.Id AND m.Name = 'Contract'
			WHERE c.OrganisationId = @OrganisationId
			GROUP BY s.Name;
	END

SELECT * FROM [dbo].[Document];
SELECT * FROM [dbo].[NoteType];
SELECT * FROM [dbo].[Person];
SELECT * FROM [dbo].[Task];
SELECT * FROM [dbo].[ContractDetail];
SELECT * FROM [dbo].[ContractType];

EXEC [dbo].[sp_ContractStatistics] '<entry><string>report</string><list><string>max</string></list></entry>', 1000;

DELETE FROM [dbo].[ContractDetail] WHERE Id = 51097;

SELECT CONVERT(XML, '<entry><string>report</string><list><string>value</string></list></entry>').value('(//entry[string/text() = "report"]/*[position()=2]/*[position()=1]/text())[1]', 'VARCHAR(255)');