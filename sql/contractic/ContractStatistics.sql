SELECT * FROM dbo.Task;
SELECT * FROM [dbo].[Note];
ALTER TABLE [dbo].[Person] ALTER COLUMN [LastName] NVARCHAR(255) NOT NULL;
SELECT o.*, p.*,u.* FROM [dbo].[User] u INNER JOIN [dbo].[Person] p ON u.PersonId = p.Id INNER JOIN [dbo].[Organisation] o  ON o.Id = p.OrganisationId;
SELECT * FROM Module;
SELECT * FROM AuditAction;
SELECT * FROM GroupObject WHERE ModuleId = 1009;
SELECT g.*, g0.* FROM GroupObject g0, [Group] g WHERE g.Id = g0.GroupId AND g0.ModuleId = 1009 AND ExternalId = 10425;
-- SELECT ExternalId, COUNT(*) FROM [dbo].[Audit] WHERE ModuleId = 1005 AND UserId = 1000 AND AuditActionId = 1003 GROUP BY ExternalId ORDER BY COUNT(*) DESC;

WITH RecentContract(UserId, ExternalId, LastRead)
AS (SELECT a.UserId, ExternalId, MAX(a.Created) 
	FROM [dbo].[Audit] a  WITH (NOLOCK)
	INNER JOIN Module m ON a.ModuleId = m.Id 
	INNER JOIN AuditAction aa ON a.AuditActionId = aa.Id
	INNER JOIN dbo.[User] u WITH (NOLOCK) ON a.UserId = u.Id
	WHERE m.Name = 'Contract' AND aa.Name = 'Read'
	AND u.PersonId = 10003 
	GROUP BY a.UserId, ExternalId)
SELECT c.*, rc.*
	FROM RecentContract rc 
	INNER JOIN dbo.Contract c WITH (NOLOCK) ON rc.Externalid = c.Id
	ORDER BY LastRead DESC;


ALTER PROCEDURE sp_UserContractActivity(@Params VARCHAR(MAX), @OrganisationId BIGINT)
AS

	DECLARE @XmlParams XML = CONVERT(XML, @Params);
	DECLARE @PersonId BIGINT;
	
	SELECT @PersonId = @XmlParams.value('(//entry[string/text() = "personId"]/*[position()=2]/*[position()=1]/text())[1]', 'BIGINT');

	WITH RecentContract(UserId, ExternalId, LastRead)
	AS (SELECT a.UserId, ExternalId, MAX(Created) 
		FROM [dbo].[Audit] a  WITH (NOLOCK)
		INNER JOIN Module m ON a.ModuleId = m.Id 
		INNER JOIN AuditAction aa ON a.AuditActionId = aa.Id
		WHERE m.Name = 'Contract' AND aa.Name = 'Read'
		GROUP BY a.UserId, ExternalId)
	SELECT c.*
		FROM RecentContract rc 
		INNER JOIN dbo.Contract c WITH (NOLOCK) ON rc.Externalid = c.Id
		INNER JOIN dbo.[User] u WITH (NOLOCK) ON rc.UserId = u.Id
		WHERE u.PersonId = @PersonId 
		ORDER BY LastRead DESC;

EXEC [dbo].[sp_RecentContract] 1001;
SELECT * FROM [dbo].[Report];
SELECT * FROM [dbo].[ReportOrganisation];
INSERT INTO Report(Name, Description, Command, Active, ExternalRef) VALUES('Contract Statistics', 'Organisation contract statistics', 'EXEC [dbo].[sp_ContractStatistics] :OrganisationId', 1, 'contract-statistics');
INSERT INTO ReportOrganisation(Name, Description, ReportId, OrganisationId, Active, Created)
	VALUES('Contract Statistics', 'Organisation contract statistics', 1003, 1000, 1, GETDATE());

SELECT CONVERT(XML, 
'
<org.springframework.util.LinkedMultiValueMap>
    <targetMap class="linked-hash-map">
        <entry>
            <string>reportId</string>
            <linked-list>
                <string>b8c37e33defde51cf91e1e03e51657da</string>
            </linked-list>
        </entry>
        <entry>
            <string>run</string>
            <linked-list>
                <string>true</string>
            </linked-list>
        </entry>
        <entry>
            <string>personId</string>
            <list>
                <string>10003</string>
            </list>
        </entry>
        <entry>
            <string>report</string>
            <linked-list>
                <string>status</string>
            </linked-list>
        </entry>
        <entry>
            <string>groupId</string>
            <linked-list>
                <string>groupId</string>
            </linked-list>
        </entry>
        <entry>
            <string>granularity</string>
            <linked-list>
                <string>granularity</string>
            </linked-list>
        </entry>
        <entry>
            <string>aggregateBy</string>
            <linked-list>
                <string>type</string>
                <string>valueName</string>
            </linked-list>
        </entry>
    </targetMap>
</org.springframework.util.LinkedMultiValueMap>
').value('(//entry[string/text() = "report"]/*[position()=2]/*[position()=1]/text())[1]', 'VARCHAR(255)');

EXEC [dbo].[sp_UserContractActivity] '<map>
    <entry>
        <string>Action</string>
        <string>Read</string>
    </entry>
    <entry>
        <string>personId</string>
        <list><int>10003</int></list>
    </entry>
</map>', 1000;


ALTER TABLE [dbo].[Report] ADD UNIQUE(Name);