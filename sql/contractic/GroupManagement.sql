SELECT * FROM dbo.[User];
SELECT * FROM dbo.Organisation;
SELECT u.* FROM dbo.Person p, dbo.[User] u WHERE u.PersonID = p.Id AND OrganisationId = 1000;

--ALTER TABLE [Group] ALTER COLUMN ModuleId BIGINT;
-- Create a groupset
INSERT INTO GroupSet(Name, Description, Active, OrganisationId) VALUES('DEPARTMENT', 'Organisation Department Hierarchy', 1, 1000);
/*Add groups to the groupset*/
INSERT INTO [Group](Name, OrganisationId, GroupSetId) VALUES('Marketing', 1000, 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId) VALUES('Finance', 1000, 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) VALUES('Information Technology', 1000, 1000, 21036);
/*Add users to the group*/
INSERT INTO GroupUser(GroupId, UserId) VALUES(21034, 1000);
INSERT INTO GroupUser(GroupId, UserId) VALUES(21036, 1004);
INSERT INTO GroupUser(GroupId, UserId) VALUES(21039, 1006);
INSERT INTO GroupUser(GroupId, UserId) VALUES(21036, 1007);
/*Add some group objects*/
INSERT INTO GroupObject(GroupId, ModuleId, ExternalId) VALUES(21036, 1005, 1003);

INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Accident and emergency services' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Allergy services in hospital' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Children & Adolescent Services' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Colorectal cancer services' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Heart' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Cardiac services (community)' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Heart' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Respiratory Medicine' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Heart' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Critical care' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Diabetic Medicine' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Diagnostic Physiological Measurement' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Ear, Nose & Throat' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Endocrine and thyroid surgery' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Geriatric Medicine' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Gynaecology' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Maternity' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Maternity services' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Maternity' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Head and neck cancer services' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Cancer' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Head and neck cancer services' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Cancer' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Renal cancer services' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Cancer' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Immunology' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Mental Health - Adults of all ages' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Neurology' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Neurology' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Neurosurgery' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Neurology' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Occupational therapy services' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Oral and Maxillofacial Surgery' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Ophthalmology' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Orthotics and Prosthetics' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Orthology' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Orthopaedics' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Orthology' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Sports and Exercise Medicine' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Trauma services' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = '' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Vascular services' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Vascular' AND OrganisationId = 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId, ParentId) SELECT 'Vascular surgery' , 1000, 1000, (SELECT Id FROM [Group] WHERE Name = 'Vascular' AND OrganisationId = 1000);


SELECT * FROM GroupObject;
SELECT * FROM Module;
SELECT * FROM [Group];
SELECT * FROM Contract;
SELECT * FROM GroupUser;
SELECT * FROM [Group] WHERE GroupSetId = 1000;


INSERT INTO [Group](Name, OrganisationId, GroupSetId) VALUES('Heart', 1000, 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId) VALUES('Maternity', 1000, 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId) VALUES('Cancer', 1000, 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId) VALUES('Neurology', 1000, 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId) VALUES('Orthology', 1000, 1000);
INSERT INTO [Group](Name, OrganisationId, GroupSetId) VALUES('Vascular', 1000, 1000);

SELECT p.*,g.Id, p.Id % g.Id FROM dbo.Person p LEFT JOIN (SELECT * FROM [Group] WHERE GroupSetId = 1000) g ON g.Id % p.Id % p.OrganisationId BETWEEN 0 AND 50;
SELECT p.*,g.Id, p.Id % g.Id % p.OrganisationId FROM dbo.Person p CROSS JOIN (SELECT * FROM [Group] WHERE GroupSetId = 1000) g;

INSERT INTO GroupObject(GroupId, ModuleId, ExternalId, Active, GroupSetId)
SELECT (SELECT TOP 1 Id FROM [Group] g WHERE GroupSetId = 1000 AND g.Id % p.Id % p.OrganisationId BETWEEN 0 AND 5000 ORDER BY NEWID()), m.Id, p.Id, 1, 1000 FROM
	Person p LEFT JOIN Module m ON m.Name = 'Person'
	WHERE p.OrganisationId = 1000;

INSERT INTO GroupObject(GroupId, ModuleId, ExternalId, Active, GroupSetId)
SELECT (SELECT TOP 1 Id FROM [Group] g WHERE GroupSetId = 1000 AND g.Id % p.Id % p.OrganisationId BETWEEN 0 AND 5000 ORDER BY NEWID()), m.Id, p.Id, 1, 1000 FROM
	Person p LEFT JOIN Module m ON m.Name = 'Person'
	WHERE p.OrganisationId = 1000;

SELECT * FROM dbo.Person WHERE OrganisationId = 1000 ORDER BY FirstName;

SELECT * FROM dbo.[User];
SELECT DISTINCT gobj.*, u.Id, u.LoginId, p.Id, p.OrganisationId  FROM GroupObject gobj INNER JOIN PERSON p ON p.Id = gobj.ExternalId INNER JOIN Module m ON gobj.ModuleId = m.Id AND m.Name = 'Person' INNER JOIN [User] u ON u.PersonID = p.Id;
SELECT DISTINCT gobj.* FROM GroupObject gobj INNER JOIN [Contract] c ON c.Id = gobj.ExternalId INNER JOIN Module m ON gobj.ModuleId = m.Id AND m.Name = 'Contract' WHERE c.OrganisationId = 1000;
SELECT * FROM GroupObject WHERE ExternalId = 10278 AND ModuleId = 1009;

UPDATE GroupObject SET GroupId = 21058 WHERE Id = 1008;

INSERT INTO GroupObject(GroupId, ModuleId, ExternalId, GroupSetId) VALUES(21066, 1009, 10003, 1000);

SELECT * FROM [dbo].[Contract];
INSERT INTO GroupObject(GroupId, ModuleId, ExternalId, Active, GroupSetId) VALUES(21066, 1005, 21014, 1, 1000);
SELECT * FROM [dbo].[ContractDetail];