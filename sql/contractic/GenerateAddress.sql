WITH nmb(value) 
AS 
(SELECT * FROM (VALUES (0), (1),(2),(3),(4),(5),(6),(7),(8),(9),(10)) AS nmb(value)),
street(name) 
AS (SELECT * FROM (VALUES ('Gabriels Hill'),('Gagetown Terrace'),('Gainsborough Drive'),('Gallants Lane'),('Gandys Lane'),('Garden Close'),('Garden Of England Park'),('Garrington Close'),('Gatcombe Close'),('Gatland Lane'),
('Gault Close'),('Gentian Close'),('George Street'),('Georgian Drive'),('Gibbs Hill'),('Gibraltar Lane'),('Giddy Horn Lane'),('Gilbert Terrace'),('Gladstone Road'),
('Gleaners Close'),('Glebe Gardens'),('Glebe Lane'),('Glebe Meadow'),('Gleneagles Drive'),('Glenleigh Road'),('Glenleigh Terrace'),('Glenwood Close'),('Gloucester Road'),
('Goddington Lane'),('Goldthorne Close'),('Gooch Close'),('Goodwin Drive'),('Goodwood Close'),('Gore Court Road'),('Gorham Drive'),('Goudhurst Close'),('Grace Avenue'),
('Grampian Way'),('Granary Close'),('Grange Lane'),('Grant Drive'),('Granville Road'),('Grasslands'),('Gravelly Bottom Road'),('Gravelly Ways'),('Graveney Road'),
('Grecian Street'),('Green Hill'),('Green Hill Lane'),('Green Lane '),('Greenborough Close'),('Greenfields'),('Greenhithe'),('Greensands Road'),('Greenside'),('Greenway'),
('Greenway Court Road'),('Greenway Forstal'),('Greenway Forstal Lane'),('Greenways'),('Greenwich Close'),('Grenadier Close'),('Gresham Road'),('Grey Wethers'),('Greyfriars Close'),
('Greystones Road'),('Groombridge Square'),('Grove Green'),('Grove Green Lane'),('Grove Green Road'),('Grove Lane'),('Grove Road'),('Grovelands'),('Grovewood Court'),('Grovewood Drive'),
('Gullands'),('Guston Road'),('Saddlers Close'),('Salisbury Road'),('Salters Cross'),('Salts Avenue'),('Salts Lane'),('Saltwood Road'),('Samphire Close'),('Sandbourne Drive'),('Sandling Lane'),('Sandling Road'),
('Sandway Road '),('Sandy Lane'),('Sandy Mount'),('Saxons Drive'),('School Lane '),('Scott Street'),('Scragged Oak Road '),('Selbourne Walk'),('Selby Road'),('Senacre Lane'),('Senacre Square'),
('Seven Mile Lane'),('Sevington Park'),('Shaftesbury Drive'),('Sharsted Way'),('Sheals Crescent'),('Shearers Close'),('Shearwater'),('Shelley Road'),('Shenley Grove'),('Shepherds Gate Drive'),
('Shepherds Way'),('Sheppey Road'),('Shepway Court'),('Sherbourne Drive'),('Sheridan Close'),('Sheringham Close'),('Shernolds'),('Shillingheld Close'),('Shingle Barn Lane'),('Shirley Way'),
('Shoreham Walk'),('Shortlands Green'),('Shropshire Terrace'),('Shrubsole Drive'),('Sidney Street'),('Silchester Court'),('Silver Birch Walk'),('Silverdale'),('Simmonds Lane'),('Sissinghurst Drive'),
('Sittingbourne Road'),('Skinners Way'),('Skye Close'),('Smallhythe Close'),('Smiths Hill'),('Snowdon Avenue'),('Snowdon Parade'),('Somerfield Lane'),('Somerfield Road'),('Somerset Road'),('Somner Walk'),
('South Bank'),('South Court'),('South Crescent'),('South Lane'),('South Park Road'),('South Street'),('Southwood'),('Speedwell Close'),('Speldhurst Court'),('Spencer Way'),('Spindle Glade'),('Sportsfield'),
('Spot Lane'),('Springett Way'),('Springfield Avenue'),('Springvale'),('Springwood Close'),('Springwood Road'),('Spurway'),('Square Hill Road'),('St Andrews Close'),('St Andrews Road'),('St Annes Court'),
('St Barnabas Close'),('St Faiths Lane'),('St Faiths Street'),('St Georges Square'),('St Helens Corner'),('St Helens Lane'),('St Heliers Close'),('St Hilarys Houses'),('St Laurence Avenue'),('St Lukes Avenue'),
('St Lukes Road'),('St Margarets Close'),('St Martins Close'),('St Marys Close'),('St Michaels Road'),('St Peter Street'),('St Philips Avenue'),('St Ronans Close'),('St Stephens Square'),('St Welcumes Way'),
('Stadler Close'),('Staffa Road'),('Stagshaw Close'),('Stan Lane'),('Stanford Drive'),('Stanhope Close'),('Stansted Close'),('Staplers Court'),('Starch Bottom'),('Station Approach'),('Station Hill'),('Station Road '),
('Stede Hill'),('Sterling Avenue'),('Stevenson Close'),('Stickfast Lane'),('Stockbury Drive'),('Stockett Lane '),('Stockton Close'),('Stoneacre Court'),('Stoneacre Lane'),('Stratford Drive'),('Streetfield'),('Stuart Close'),
('Styles Lane'),('Suffolk Road'),('Surrey Road'),('Sussex Road'),('Sutton Road '),('Sutton Street'),('Sutton Valence Hill'),('Swadelands Close'),('Swanton Road'),('Sycamore Crescent'),('Symonds Lane')) AS street(name)),
town(postcode, name)
AS (SELECT * FROM (VALUES ('ME19', 'WEST MALLING'),('ME6', 'SNODLAND, WEST MALLING'),('ME9', 'SITTINGBOURNE'),('ME10', 'SITTINGBOURNE'),('ME12', 'SHEERNESS'),
('ME3', 'ROCHESTER'),('ME2', 'ROCHESTER'),('ME1', 'ROCHESTER'),('ME11', 'QUEENBOROUGH'),('ME18', 'MAIDSTONE'),
('ME17', 'MAIDSTONE'),('ME16', 'MAIDSTONE'),('ME15', 'MAIDSTONE'),('ME14', 'MAIDSTONE'),('ME8', 'GILLINGHAM'),
('ME7', 'GILLINGHAM'),('ME13', 'FAVERSHAM'),('ME5', 'CHATHAM'),('ME4', 'CHATHAM'),('ME20', 'AYLESFORD')) AS town(postcode, name))
INSERT INTO c21cms.dbo.Address([Section1], [Section2], [Section3],[Section4], [OrganisationId], [TypeId])
SELECT TOP 1 PERCENT CONVERT(VARCHAR(5), n6.value) AS section1,  s.name AS section2, t.name AS section3, 
	'ME' + CONVERT(VARCHAR(5), n1.value) + CONVERT(VARCHAR(5), n2.value) + ' ' + CONVERT(VARCHAR(5), n3.value) + CHAR(65 + n4.value) + CHAR(65 + n5.value) AS section4,
	1000, 1001
	FROM nmb n1, nmb n2, nmb n3, nmb n4, nmb n5, nmb n6, street s, town t
WHERE n1.value != 0 AND n4.value!=n5.value and n3.value!=0 AND t.postcode = 'ME' + CONVERT(VARCHAR(5), n1.value) + CONVERT(VARCHAR(5), n2.value)
AND n6.value != 0 ORDER BY NEWID();