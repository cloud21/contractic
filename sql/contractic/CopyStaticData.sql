INSERT INTO ContractType(Name, Description, OrganisationId, Active)
SELECT Name, Description, 1002, 1 FROM [dbo].ContractType WHERE OrganisationId = 1001;

INSERT INTO DocumentType(Code, Description, OrganisationId, Active)
SELECT Code, Description, 1002, 1 FROM [dbo].[DocumentType] WHERE OrganisationId = 1001;

INSERT INTO NoteType(Name, Active, OrganisationId, Description)
SELECT Name, Active, 1002, Description FROM NoteType WHERE OrganisationId = 1001;

INSERT INTO Status(Name, Active, ModuleId, OrganisationId, Description)
SELECT Name, Active, ModuleId, 1002, Description FROM Status WHERE OrganisationId = 1001;

INSERT INTO Supplier(Reference, Name, Created, Active, ExternalRef, OrganisationId)
SELECT Reference, Name, GETDATE(), Active, ExternalRef, 1002 FROM Supplier WHERE OrganisationId = 1001;

INSERT INTO Status(Name, Active, ModuleId, OrganisationId, Description)
VALUES ('Archived', 1, 1005, 1001, 'Contract has been archived');