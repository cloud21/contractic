CREATE TABLE Module(Id bigint primary key IDENTITY(1000,1), 
	Name VARCHAR(50) NOT NULL UNIQUE, Description VARCHAR(255) NOT NULL);
GO

INSERT INTO Module(Name, Description) VALUES ('Contract', 'Contract Management'), 
	('Supplier', 'Supplier Management'), 
	('Organisation', 'Organisation Management'),
	('Person', 'Person Management'),
	('Task', 'Task Management');
GO

CREATE TABLE [dbo].[Organisation](
	[Id] [bigint]  PRIMARY KEY IDENTITY(1000,1),
	[Name] [nvarchar](255) NOT NULL UNIQUE,
	[Created] [datetime] NULL,
	[Active] [tinyint] NOT NULL,
	ExternalRef VARCHAR(255));
GO

CREATE TABLE [dbo].[OrganisationModule](
	[Id] [bigint]  PRIMARY KEY IDENTITY(1000,1),
	[Effective] [datetime] NULL,
	[Active] [tinyint] NULL,
	[OrganisationId] [bigint] NULL REFERENCES Organisation(Id),
	[ModuleId] [bigint] NOT NULL REFERENCES Module(Id));
GO

-- This is a status that a specific module item can take on. For example a contract can assume a status of Complete, Incomplete, Archived e.t.c
-- An task or event can take on different status such as Reminded, Done
CREATE TABLE Status(Id bigint primary key IDENTITY(1000,1), 
	Name VARCHAR(50) NOT NULL, 
	Description VARCHAR(255) NOT NULL,
	Active TINYINT DEFAULT 1,
	ModuleId BIGINT NOT NULL REFERENCES Module(Id),
	OrganisationId BIGINT NOT NULL REFERENCES Organisation(Id),
	UNIQUE (OrganisationId, ModuleId, Name));
GO

CREATE TABLE Person(Id BIGINT PRIMARY KEY IDENTITY(10000,1), 
	Title NVARCHAR(50) NOT NULL, 
	FirstName NVARCHAR(255) NOT NULL, 
	MiddleName NVARCHAR(255),
	LastName NVARCHAR(255) NOT NULL,
	JobTitle NVARCHAR(255),
	Gender CHAR(1) CHECK (Gender IN('M', 'F')),
	Created DATETIME NOT NULL,
	Active TINYINT DEFAULT 1,
	ExternalRef VARCHAR(255), /*This is used to integrate with external systems*/
	OrganisationId BIGINT NOT NULL REFERENCES Organisation(Id));
GO

CREATE TABLE AddressType(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	Name VARCHAR(50) NOT NULL UNIQUE, Active TINYINT NOT NULL DEFAULT 1);
GO

CREATE TABLE Address(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	Section1 VARCHAR(255) NOT NULL,
	Section2 VARCHAR(255),
	Section3 VARCHAR(255),
	Section4 VARCHAR(255),
	Section5 VARCHAR(255),
	Section6 VARCHAR(255),
	Section7 VARCHAR(255),
	Section8 VARCHAR(255),
	OrganisationId BIGINT NOT NULL REFERENCES Organisation(Id),
	TypeId BIGINT NOT NULL REFERENCES AddressType(Id),
	Active TINYINT NOT NULL DEFAULT 1);
GO

CREATE TABLE Contact(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	Active TINYINT NOT NULL DEFAULT 1,
	ExternalId BIGINT NOT NULL REFERENCES Person(Id),
	AddressId BIGINT NOT NULL REFERENCES Address(Id),
	ModuleId BIGINT NOT NULL REFERENCES Module(Id));
GO

CREATE TABLE DocumentType(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	Code NVARCHAR(50) NOT NULL UNIQUE,
	Description NVARCHAR(255) NOT NULL,
	OrganisationId BIGINT NOT NULL REFERENCES Organisation(Id),
	Active TINYINT DEFAULT 1,
	UNIQUE(OrganisationId, Code));
GO

CREATE TABLE Document(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	Title NVARCHAR(255),
	Description NVARCHAR(MAX),
	Received DATETIME NOT NULL,
	Path NVARCHAR(MAX) NOT NULL,
	ExternalId BIGINT NOT NULL, /*References the Id for the Module with Id=ModuleId. If null, permission applies to entire module*/
	DocumentTypeId BIGINT NOT NULL REFERENCES DocumentType(Id),
	StatusId BIGINT NOT NULL REFERENCES Status(Id),
	ModuleId BIGINT NOT NULL REFERENCES Module(Id));
GO

CREATE TABLE dbo.[User] (Id BIGINT PRIMARY KEY IDENTITY(1000,1), 
	LoginId VARCHAR(255) NOT NULL UNIQUE, 
	Password NVARCHAR(MAX) NOT NULL, 
	Created DATETIME, 
	Profile VARCHAR(MAX), 
	Active TINYINT NOT NULL, 
	PersonID BIGINT NOT NULL REFERENCES Person(Id), LastUpdated DATETIME);
GO

CREATE TABLE dbo.UserPermission(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	UserId BIGINT NOT NULL REFERENCES [User](Id),
	ModuleId BIGINT NOT NULL REFERENCES Module(Id),
	ExternalId BIGINT, /*References the Id for the Module with Id=ModuleId. If null, permission applies to entire module*/
	[Read] TINYINT NOT NULL,
	Write TINYINT NOT NULL,
	[Delete] TINYINT NOT NULL);

CREATE TABLE Supplier(Id BIGINT PRIMARY KEY IDENTITY(10000,1), 
	Reference NVARCHAR(255) NOT NULL,
	Name NVARCHAR(MAX) NOT NULL /*UNIQUE???*/, 
	Created DATETIME NOT NULL,
	Active TINYINT DEFAULT 1,
	ExternalRef VARCHAR(255),
	OrganisationId BIGINT NOT NULL REFERENCES Organisation(Id));
GO

CREATE TABLE SupplierPerson(Id BIGINT PRIMARY KEY IDENTITY(10000,1), 
	PersonId BIGINT NOT NULL REFERENCES Person(Id),
	Created DATETIME NOT NULL,
	Enabled TINYINT DEFAULT 1,
	ExternalRef VARCHAR(255),
	OrganisationId BIGINT NOT NULL REFERENCES Organisation(Id));
GO

CREATE TABLE NoteType(Id bigint primary key IDENTITY(1000,1), 
	Name VARCHAR(50) NOT NULL, 
	Active TINYINT DEFAULT 1,
	OrganisationId BIGINT NOT NULL REFERENCES Organisation(Id),
	UNIQUE(Name, OrganisationId));
GO
-- This is just test data
INSERT INTO NoteType(Name, Active, OrganisationId) VALUES('Info', 1, 1000), ('Important', 1, 1000);
GO

CREATE TABLE Note(Id bigint primary key IDENTITY(1000,1), 
	Header VARCHAR(255) NOT NULL, 
	Detail VARCHAR(MAX) NOT NULL,
	ExternalId BIGINT NOT NULL, /*Reference to which entity within the system this not applies*/
	NoteTypeId BIGINT NOT NULL REFERENCES NoteType(Id),
	ModuleId BIGINT NOT NULL REFERENCES Module(Id),
	Active TINYINT DEFAULT 1);
GO

CREATE TABLE ContractType(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	Name VARCHAR(255) NOT NULL,
	Description VARCHAR(MAX),
	Active TINYINT NOT NULL DEFAULT 1,
	OrganisationId BIGINT NOT NULL REFERENCES Organisation(Id),
	UNIQUE(Name, OrganisationId));
GO

CREATE TABLE ContractGroup(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	Name VARCHAR(255) NOT NULL,
	Description VARCHAR(MAX),
	Active TINYINT NOT NULL DEFAULT 1);
GO

CREATE TABLE Contract(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	SupplierId BIGINT NOT NULL REFERENCES Supplier(Id),
	OwnerId BIGINT NOT NULL REFERENCES Person(Id),
	GroupId BIGINT NULL REFERENCES ContractGroup(Id),
	StatusId BIGINT NOT NULL REFERENCES Status(Id),
	TypeId BIGINT NOT NULL REFERENCES ContractType(Id),
	Title VARCHAR(255) NOT NULL,
	StartDate DATETIME NOT NULL,
	EndDate DATETIME NOT NULL);
GO

CREATE TABLE ContractDetailGroup(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	Name VARCHAR(255) NOT NULL,
	Description VARCHAR(MAX),
	Active TINYINT NOT NULL DEFAULT 1);
GO

CREATE TABLE ContractDetail(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	Name VARCHAR(50) NOT NULL,
	Description VARCHAR(255) NULL,
	Value VARCHAR(MAX) NOT NULL,
	LastUpdated DATETIME NOT NULL,
	ContractId BIGINT NOT NULL REFERENCES Contract(Id),
	GroupId BIGINT REFERENCES ContractDetailGroup(Id))
GO

CREATE TABLE ContractChange(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	CustomerChangeId VARCHAR(255) NOT NULL,
	ApprovalDate DATETIME NOT NULL,
	EffectiveDate DATETIME NOT NULL,
	Description VARCHAR(MAX),
	ContractId BIGINT NOT NULL REFERENCES Contract(Id))
GO

--ContractChangeContac
CREATE TABLE Task(Id BIGINT PRIMARY KEY IDENTITY(1000,1),
	PersonID BIGINT NOT NULL REFERENCES Person(Id),
	Description VARCHAR(255) NOT NULL,
	LoggedDate DATETIME NOT NULL,
	DueDate DATETIME NOT NULL,
	StatusId BIGINT NOT NULL REFERENCES Status(Id),
	ParentId BIGINT REFERENCES Task(Id),
	ExternalId BIGINT NOT NULL,
	ModuleId BIGINT NOT NULL REFERENCES Module(Id));