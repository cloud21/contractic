import json
import requests

url = 'http://localhost:8983/solr/admin/cores'


def create_core(core):
	params = {
		'action' : 'CREATE',
		'name': core,
		'instanceDir': core,
		'config':'solrconfig.xml',
		'schema':'schema.xml',
		'dataDir':'data',
		'wr':'json'
	}
	r = requests.get(url, params)
	print (r)

def create_cores():
	cores = ['supplier', 'document', 'contract', 'note', 'task', 'person']
	for core in cores:
		create_core(core)
		
create_cores()
	
