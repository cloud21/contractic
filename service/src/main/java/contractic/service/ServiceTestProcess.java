package contractic.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.context.ApplicationContext;

import contractic.model.data.store.Contract;
import contractic.model.data.store.Document;
import contractic.model.data.store.Task;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractService;
import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DocumentDO;
import contractic.service.domain.DomainService;
import contractic.service.domain.MessageDO;
import contractic.service.domain.OrganisationDO;
import contractic.service.domain.TaskDO;
import contractic.service.domain.TaskService;
import contractic.service.domain.UserDO;

public class ServiceTestProcess implements Runnable {
	private final ApplicationContext context;
	private Random random = new Random();
	private final static int MIN_WAIT = 5000;
	private final static int MAX_WAIT = 5000;
	private final static int WAIT_FACTOR = 2;
	
	private final UserDO userDO;
	private final DomainService<Contract, Long, ContractDO, MessageDO> contractService;
	private final DomainService<Task, Long, TaskDO, MessageDO> taskService;
	private final DomainService<Document, Long, DocumentDO, MessageDO> documentService;
	
	public ServiceTestProcess(final UserDO userDO, final ApplicationContext context){
		this.userDO = userDO;
		this.context = context;
		this.contractService = (DomainService<Contract, Long, ContractDO, MessageDO>)context.getBean("contractService");
		this.taskService = (DomainService<Task, Long, TaskDO, MessageDO>)context.getBean("taskService");
		this.documentService = (DomainService<Document, Long, DocumentDO, MessageDO>)context.getBean("documentService");
	}
	
	// 1. Get Organisation
	// 2.
	// Thread 1 - Select all contracts, select a single contract, view contract details, view contract tasks, view contract documents
	// Thread 2 - Select all tasks
	// Thread 3 - Select all 
	
	private List<ContractDO> getContract(final UserDO userDO) throws ContracticServiceException{
	    final Map<String, Object> params = new HashMap<>();
	    params.put(CommonService.USER, userDO);
	    return contractService.get(params);
	}
	
	
	
	private ContractDO getContract(String id, final UserDO userDO) throws ContracticServiceException{
	    final Map<String, Object> params = new HashMap<>();
	    params.put(CommonService.USER, userDO);
	    params.put(ContractService.GET_PARAM_HASHID, id);
	    List<ContractDO> list = contractService.get(params);
	    assert list.size() == 1;
	    return list.get(0);
	}
	
	private void saveContract(ContractDO contractDO) throws ContracticServiceException{	    
	    contractService.update(contractDO);
	}
	
	private List<TaskDO> getTask(final UserDO userDO) throws ContracticServiceException{
	    final Map<String, Object> params = new HashMap<>();
	    params.put(CommonService.USER, userDO);
	    return taskService.get(params);
	}
	
	private TaskDO getTask(String id, final UserDO userDO) throws ContracticServiceException{
	    final Map<String, Object> params = new HashMap<>();
	    params.put(CommonService.USER, userDO);
	    params.put(TaskService.GET_PARAM_HASHID, id);
	    
	    List<TaskDO> list = taskService.get(params);
	    assert list.size() == 1;
	    return list.get(0);
	}
	
	private long waitLength(int min, int max, int factor){
		assert min > 0;
		assert max >= min;
		assert factor > 0;
		return random.nextInt(max) * factor + min;
	}
	
	private void process(UserDO userDO) throws ContracticServiceException, InterruptedException{
		Random random = new Random();
		
		// Get all contracts
		List<ContractDO> contractList = getContract(userDO);
		System.out.println(String.format("Thread: %s, retrieved %s contracts", Thread.currentThread().getName(), contractList.size()));
		
		// Pause just a bit
		long waitLen = waitLength(MIN_WAIT, MAX_WAIT, WAIT_FACTOR);
		System.out.println(String.format("Thread: %s, sleeping for %s", Thread.currentThread().getName(), waitLen));
		Thread.sleep(waitLen);
		
		// Select any contract from the list
		int idx = random.nextInt(contractList.size());
		ContractDO contractDO = getContract(contractList.get(idx).getId(), userDO);
		System.out.println(String.format("Thread: %s, retrieved contract %s", Thread.currentThread().getName(), contractDO.getId()));
		
		// Update the contract
		contractDO.setUser(userDO);
		this.contractService.update(contractDO);
		
		// Do other bits here such as viewing contract notes e.t.c.
		
		// Pause just a bit
		waitLen = waitLength(MIN_WAIT, MAX_WAIT, WAIT_FACTOR);
		System.out.println(String.format("Thread: %s, sleeping for %s", Thread.currentThread().getName(), waitLen));
		Thread.sleep(waitLen);
		
		// Get all tasks
		List<TaskDO> taskList = getTask(userDO);
		System.out.println(String.format("Thread: %s, retrieved %s tasks", Thread.currentThread().getName(), taskList.size()));
		
		
		// Pause just a bit
		waitLen = waitLength(MIN_WAIT, MAX_WAIT, WAIT_FACTOR);
		System.out.println(String.format("Thread: %s, sleeping for %s", Thread.currentThread().getName(), waitLen));
		Thread.sleep(waitLen);
		
		// Select any task from the list
		idx = random.nextInt(taskList.size());
		TaskDO taskDO = getTask(taskList.get(idx).getId(), userDO);
		System.out.println(String.format("Thread: %s, retrieved task %s", Thread.currentThread().getName(), taskDO.getId()));
	}
	
	@Override
	public void run() {
		while(!Thread.currentThread().isInterrupted()){
			try {
				process(userDO);
			} catch (ContracticServiceException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
