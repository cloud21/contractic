package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Audit;
import contractic.model.data.store.Contact;
import contractic.model.data.store.Contract;
import contractic.model.data.store.Module;
import contractic.model.data.store.Note;
import contractic.model.data.store.NoteType;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Supplier;
import contractic.model.data.store.Task;
import contractic.model.data.store.User;
import contractic.service.util.Cache;
import contractic.service.util.ConcurrencyUtil;
import contractic.service.util.HashingUtil;
import contractic.service.util.SearchClient.SearchEntity;

public class NoteService implements
		DomainService<Note, Long, NoteDO, MessageDO> {

	public final static String GET_PARAM_NAME = "name";

	public final static String NOTE_IDCACHE_NAME = "note.idcache.name";
	public static final String GET_PARAM_EXTERNALID = "external.id";
	public static final String GET_PARAM_MODULEID = "module.id";
	public static final String USERID = "user.id";
	public static final String GET_LOG_READ = "log.read";

	private static final Logger logger = Logger.getLogger(NoteService.class);

	private Cache<String, Long> noteIdCache;

	@Autowired
	private CrudOperation<Note> noteDAO;

	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	@Autowired
	private DomainService<Contact, Long, ContactDO, MessageDO> contactService;
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	@Autowired
	private DomainService<Supplier, Long, SupplierDO, MessageDO> supplierService;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<NoteType, Long, NoteTypeDO, MessageDO> noteTypeService;
	@Autowired
	private DomainService<Contract, Long, ContractDO, MessageDO> contractService;
	@Autowired
	private DomainService<Task, Long, TaskDO, MessageDO> taskService;
	@Autowired
	private DomainService<User, Long, UserDO, MessageDO> userService;
	@Autowired
	private DomainService<Audit, Long, AuditDO, MessageDO> auditService;
	@Autowired
	private DomainService<SearchEntity, Long, SearchEntityDO, MessageDO> searchService;

	public Note get(final String uuid) throws ContracticServiceException {
		final Long id = this.noteIdCache.get(uuid);
		return get(id);
	}

	public Note get(final Long id) throws ContracticServiceException {
		if (id == null)
			throw new ContracticServiceException("Null note id supplied");

		Note note = new Note();
		note.setId(id);

		try {
			List<Note> notes = noteDAO.read(note);
			return notes.size() == 1 ? notes.get(0) : null;

		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"An error occured while retrieving note with id %d", id));
		}
	}

	private NoteDO makeDO(final Note note, String organisationId)
			throws NoSuchAlgorithmException, ContracticServiceException {
		String hashId = HashingUtil.get(note.getId());
		NoteDO noteDO = new NoteDO();
		noteDO.setId(hashId);
		noteDO.setHeader(note.getHeader());
		noteDO.setDetail(note.getDetail());
		noteDO.setActive(note.getActive());
		noteDO.setCreated(note.getCreated() != null ? Long.toString(note
				.getCreated().getTime()) : "");

		// Get the ModuleID
		Module module = note.getModule();
		if (module == null) {
			throw new ContracticServiceException(String.format(
					"Note supplied without a module: %s", note));
		}

		// A note read will always be of the same moduleId, so we can get rid of
		// these lines here
		final Map<String, Object> params = new HashMap<>();
		params.put(ModuleService.GET_PARAM_ID, module.getId());
		final List<ModuleDO> moduleList = moduleService.get(params);
		assert (moduleList.size() == 1);
		final ModuleDO moduleDO = moduleList.get(0);

		// Now get the not type id;
		params.clear();
		params.put(NoteTypeService.GET_PARAM_NOTEID, note.getNoteType().getId()); // Parametize
																					// me
		params.put(NoteTypeService.GET_PARAM_ORGANISATIONID, organisationId);
		final List<NoteTypeDO> noteTypeList = noteTypeService.get(params);
		assert (moduleList.size() == 1);
		final NoteTypeDO noteTypeDO = noteTypeList.get(0);

		// Set the user id
		if (note.getUser() != null) {
			params.clear();
			params.put(UserService.GET_PARAM_ID, note.getUser().getId()); // Parametize
																			// me
			final List<UserDO> userList = userService.get(params);
			assert (userList.size() == 1);
			final UserDO userDO = userList.get(0);
			noteDO.setUser(new ValueDO(userDO.getId(), userDO
					.getPersonShortName()));
		}
		noteDO.setModuleId(moduleDO.getId());
		noteDO.setType(new ValueDO(noteTypeDO.getId(), noteTypeDO.getName()));

		return noteDO;
	}

	public List<NoteDO> get(final Map<String, Object> params)
			throws ContracticServiceException {
		final String hash = (String) params.get(GET_PARAM_HASHID);
		final String externalId = (String) params.get(GET_PARAM_EXTERNALID);
		Long noteId = (Long) params.get(GET_PARAM_ID);
		final String moduleId = (String) params.get(GET_PARAM_MODULEID);
		final UserDO userDO = (UserDO) params.get(CommonService.USER);
		Boolean enableLog = (Boolean) params.get(GET_LOG_READ);
		enableLog = enableLog == null ? false : enableLog;

		Module module = null;

		final List<Note> notes = new LinkedList<>();
		try {
			Note note = new Note();
			
			if(noteId != null){
				note = get(noteId);
				notes.add(note);
				module = note.getModule();
			}
			else if (StringUtils.hasText(hash)) {
				note = get(hash);
				notes.add(note);
				module = note.getModule();
			} else {
				if (!StringUtils.hasText(moduleId))
					throw new ContracticServiceException(
							"Missing moduleId, NoteService needs a moduleId");
				module = moduleService.get(moduleId);
				note.setModule(module);
				if (StringUtils.hasText(externalId)) {
					Long extId = getExternalId(externalId, module);
					note.setExternalId(extId);
				}
				notes.addAll(noteDAO.read(note));
			}

		} catch (ContracticModelException e) {
			throw new ContracticServiceException(
					"An error occured while attempting to read address types",
					e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();

		final List<NoteDO> pojoList = notes
				.stream()
				.map(note -> {
					NoteDO noteDO = null;
					try {
						noteDO = makeDO(note, userDO.getOrganisationId());
						// Side effect 1
						noteIdCache.put(noteDO.getId(), note.getId());

					} catch (ContracticServiceException
							| NoSuchAlgorithmException ex) {
						cse.add(new ContracticServiceException(String.format(
								"An error occured when hashing the note %s",
								note), ex));
					}

					return noteDO;
				}).collect(Collectors.toList());

		if (cse.size() != 0)
			throw cse.get(0);

		final ModuleDO moduleDO = CommonService.getModule(moduleService,
				module.getName());
		if (enableLog) {
			pojoList.forEach(note -> {
				try {
					logSessionReadAction(userDO, note.getExternalId(),
							moduleDO,
							String.format("Note %s", note.getHeader()));
				} catch (Exception e) {
					logger.error("Failing to write audit logs", e);
				}
			});
		}

		return pojoList;
	}

	private void saveSearchEntity(Note note) {
		ConcurrencyUtil.IOBoundExecutorService.submit(() -> {
			SearchEntityDO searchEntity = new SearchEntityDO();
			searchEntity.setId(note.getId());
			searchEntity.setCollection(SearchService.COLLECTION.NOTE);
			searchEntity.setOrganisationId(note.getUser().getPerson()
					.getOrganisation().getId());
			searchEntity.setText(String.join(",",
					new String[] { note.getHeader(), note.getDetail() }));

			try {
				searchService.create(searchEntity);
			} catch (Exception e) {
				logger.warn("An error occurred when indexing note", e);
			}
		});
	}

	public MessageDO create(NoteDO noteDO) throws ContracticServiceException {
		// Construct the person object
		final Note note = new Note();

		UserDO userDO = noteDO.getSessionUser();

		note.setCreated(new Date());
		note.setDetail(noteDO.getDetail());
		note.setHeader(noteDO.getHeader());
		note.setActive(noteDO.getActive());

		Module module = moduleService.get(noteDO.getModuleId());
		NoteType noteType = noteTypeService.get(noteDO.getType().getId());
		Long externalId = getExternalId(noteDO.getExternalId(), module);

		note.setModule(module);
		note.setExternalId(externalId);
		note.setNoteType(noteType);

		User user = userService.get(noteDO.getSessionUser().getId());

		ValueDO result = new ValueDO();
		// Save the person object
		try {
			note.setUser(user);
			noteDAO.create(note);

			String hashId = HashingUtil.get(note.getId());
			// Update the cache
			this.noteIdCache.put(hashId, note.getId());
			result.setId(hashId);

			saveSearchEntity(note);

			final ModuleDO moduleDO = CommonService.getModule(moduleService,
					note.getModule().getName());
			// Now log create
			logSessionCreateAction(userDO, noteDO.getExternalId(), moduleDO,
					String.format("New note '%s'", note.getHeader()));

		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"Failed to create note: %s", noteDO), e);
		}

		// final String icon, final String title, final String body, final
		// String type
		MessageDO message = new MessageDO(null, "Contractic",
				"Note created successfully", MessageDO.OK);
		message.setRef(result);
		return message;
	}

	private Long getExternalId(String externalIdString, Module module)
			throws ContracticServiceException {
		Long externalId = null;
		String moduleName = module.getName();
		if (moduleName.equals("Person")) {
			Person person = personService.get(externalIdString);
			externalId = person.getId();
		} else if (moduleName.equals("Supplier")) {
			Supplier supplier = supplierService.get(externalIdString);
			externalId = supplier.getId();
		} else if (moduleName.equals("Contract")) {
			Contract contract = contractService.get(externalIdString);
			externalId = contract.getId();
		} else if (moduleName.equals("Organisation")) {
			Organisation organisation = organisationService
					.get(externalIdString);
			externalId = organisation.getId();
		} else if (moduleName.equals("Task")) {
			Task task = taskService.get(externalIdString);
			externalId = task.getId();
		}
		return externalId;
	}

	public MessageDO update(NoteDO noteDO) throws ContracticServiceException {
		// Construct the note object
		UserDO userDO = noteDO.getSessionUser();

		// Save the person object
		try {

			// Get the most recent view of the note
			Note note = get(noteDO.getId());

			// Get the note Domain object representation so that we can perform
			// Audit logging on it
			final Map<String, Object> params = new HashMap<>();
			// params.put(GET_PARAM_ORGANISATIONID, dojo.getOrganisationId());
			params.put(GET_LOG_READ, Boolean.FALSE);
			params.put(GET_PARAM_HASHID, noteDO.getId());
			params.put(CommonService.USER, userDO);
			List<NoteDO> thisContract = get(params);
			assert thisContract.size() == 1;

			AuditJob<NoteDO> auditJob = new NoteAudit();
			List<AuditDO> auditList = auditJob.run(thisContract.get(0), noteDO);

			// Now update fields
			note.setDetail(noteDO.getDetail());
			note.setHeader(noteDO.getHeader());
			note.setActive(noteDO.getActive());

			NoteType noteType = noteTypeService.get(noteDO.getType().getId());
			note.setNoteType(noteType);

			noteDAO.update(note);

			String hashId = HashingUtil.get(note.getId());
			// Update the cache
			this.noteIdCache.put(hashId, note.getId());

			saveSearchEntity(note);

			final ModuleDO moduleDO = CommonService.getModule(moduleService,
					note.getModule().getName());
			// Now log update
			auditList.forEach(audit -> {
				try {
					logSessionUpdateAction(
							userDO,
							noteDO.getExternalId(),
							moduleDO,
							String.format("Note field '%s' edited",
									audit.getField()), audit.getField(),
							audit.getFrom(), audit.getTo());
				} catch (Exception e) {
					logger.error("Failing to write audit logs", e);
				}
			});
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"Failed to create note: %s", noteDO), e);
		}

		// final String icon, final String title, final String body, final
		// String type
		return new MessageDO(null, "Contractic", "Note updated successfully",
				MessageDO.OK);
	}

	public MessageDO delete(NoteDO noteDO) {
		return null;
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.noteIdCache = new Cache(NOTE_IDCACHE_NAME, String.class,
					Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(
					String.format("Initializing %s service failed", this
							.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	private void logSessionDeleteAction(UserDO userDO, String externalId,
			ModuleDO moduleDO, String logMessage)
			throws ContracticServiceException {
		// ModuleDO moduleDO = CommonService.getModule(moduleService,
		// Module.CONTRACT);
		CommonService.logCreateAction(auditService, userDO, externalId,
				moduleDO, logMessage, null, null, null);
	}

	private void logSessionCreateAction(UserDO userDO, String externalId,
			ModuleDO moduleDO, String logMessage)
			throws ContracticServiceException {
		CommonService.logCreateAction(auditService, userDO, externalId,
				moduleDO, logMessage, null, null, null);
	}

	private void logSessionReadAction(UserDO userDO, String externalId,
			ModuleDO moduleDO, String logMessage)
			throws ContracticServiceException {
		CommonService.logReadAction(auditService, userDO, externalId, moduleDO,
				logMessage, null, null, null);
	}

	private void logSessionUpdateAction(UserDO userDO, String externalId,
			ModuleDO moduleDO, String logMessage, String field, String from,
			String to) throws ContracticServiceException {
		CommonService.logUpdateAction(auditService, userDO, externalId,
				moduleDO, logMessage, field, from, to);
	}
}
