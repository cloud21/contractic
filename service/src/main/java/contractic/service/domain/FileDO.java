package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"path"})
public class FileDO {
	private String id;
	private String name;
	private String type;
	private String path;
	private String ext;
	
	public FileDO(){}
	public FileDO(String id, String name, String type, String path){
		this.id = id;
		this.name = name;
		this.type = type;
		this.path = path;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	@Override
	public String toString() {
		return "FileDO [id=" + id + ", name=" + name + ", type=" + type
				+ ", path=" + path + "]";
	}
	
}
