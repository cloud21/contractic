package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"organisationId", "user"})
public class SupplierDO {
	private String id;
	private String reference;
	private String name;
	private String organisationId;
	private String externalRef;
	private String active;
	private String created;
	private UserDO user;
	
	// This is optional when reading but needed when creating the supplier
	private ContactDO[] contact;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	public String getExternalRef() {
		return externalRef;
	}
	public void setExternalRef(String externalId) {
		this.externalRef = externalId;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public ContactDO[] getContact() {
		return contact;
	}
	public void setContact(ContactDO[] contact) {
		this.contact = contact;
	}
	public UserDO getUser() {
		return user;
	}
	public void setUser(UserDO user) {
		this.user = user;
	}

	
}
