package contractic.service.domain;

import java.util.Arrays;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@JsonIgnoreProperties({"organisationId"})
public class AddressDO {
	private String id;
	private String section1;
	private String section2;
	private String section3;
	private String section4;
	private String section5;
	private String section6;
	private String section7;
	private String section8;
	private String organisationId;
	private String typeId;
	private ValueDO type;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSection1() {
		return section1;
	}
	public void setSection1(String section1) {
		this.section1 = section1;
	}
	public String getSection2() {
		return section2;
	}
	public void setSection2(String section2) {
		this.section2 = section2;
	}
	public String getSection3() {
		return section3;
	}
	public void setSection3(String section3) {
		this.section3 = section3;
	}
	public String getSection4() {
		return section4;
	}
	public void setSection4(String section4) {
		this.section4 = section4;
	}
	public String getSection5() {
		return section5;
	}
	public void setSection5(String section5) {
		this.section5 = section5;
	}
	public String getSection6() {
		return section6;
	}
	public void setSection6(String section6) {
		this.section6 = section6;
	}
	
	public String getSection7() {
		return section7;
	}
	public void setSection7(String section7) {
		this.section7 = section7;
	}
	public String getSection8() {
		return section8;
	}
	public void setSection8(String section8) {
		this.section8 = section8;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public ValueDO getType() {
		return type;
	}
	public void setType(ValueDO type) {
		this.type = type;
	}
	
	public String stringify(){
		String[] fields = new String[]{section1, section2, section3, section4, section5, section6, section7, section8};
		final StringBuilder sb = new StringBuilder();
		Arrays.stream(fields).forEach(field -> {
			if(StringUtils.hasText(field))
				sb.append(String.format(",%s", field));
		});
		return sb.substring(1);
	}
}
