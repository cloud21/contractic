package contractic.service.domain;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.io.xml.StaxDriver;

@XStreamAlias("ccn")
public class ContractCCNDO {
	private PeriodDO period;
	@XStreamOmitField
	private String reference;
	@XStreamOmitField
	private String value;
	
	public PeriodDO getPeriod() {
		return period;
	}
	public void setPeriod(PeriodDO period) {
		this.period = period;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Convert to ContractValue from XML
	 * @param xml
	 * @return
	 */
	public final static ContractCCNDO fromXml(String xml){
		XStream xstream = new XStream();
		xstream.processAnnotations(ContractCCNDO.class);
		return (ContractCCNDO)xstream.fromXML(xml);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public final static String toXml(ContractCCNDO value){
		XStream xstream = new XStream(new StaxDriver());
		xstream.processAnnotations(ContractCCNDO.class);
		return xstream.toXML(value);
	}
	
}
