package contractic.service.domain;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Contract;
import contractic.model.data.store.Document;
import contractic.model.data.store.DocumentType;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Status;
import contractic.service.util.Cache;
import contractic.service.util.ConcurrencyUtil;
import contractic.service.util.HashingUtil;
import contractic.service.util.SearchClient.SearchEntity;

public class DocumentService implements DomainService<Document, Long, DocumentDO, MessageDO> {

	public final static String GET_PARAM_CODE = "name";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public final static String DOCUMENTTYPE_IDCACHE_NAME = "document.idcache.name";
	public static final String GET_PARAM_ID = "document.id";
	public static final String GET_PARAM_EXTERNALID = "external.id";
	public static final String GET_PARAM_MODULEID = "module.id";
	public static final String USERID = "user.id";
	
	public static final String DOCUMENT_FILE_ROOT = "documentFileRoot";
	public static final String GET_LOG_READ = "enable.read.log";
	
	private Cache<String, Long> documentIdCache;
	
	private String documentFileRoot;
	
	private static final Logger logger = Logger.getLogger(DocumentService.class);

	public void setDocumentFileRoot(String documentFileRoot) {
		this.documentFileRoot = documentFileRoot;
	}


	@Autowired
	private CrudOperation<Document> documentDAO;
	
	@Autowired
	private DomainService<DocumentType, Long, DocumentTypeDO, MessageDO> documentTypeService;
	
	@Autowired
	private DomainService<Status, Long, StatusDO, MessageDO> statusService;
	
	@Autowired
	private DomainService<FileDO, Long, FileDO, MessageDO> fileService;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	
	@Autowired
	private DomainService<Contract, Long, ContractDO, MessageDO> contractService;
	
	@Autowired
	private DomainService<SearchEntity, Long, SearchEntityDO, MessageDO> searchService;

	public Document get(final String uuid) throws ContracticServiceException{
		final Long id = this.documentIdCache.get(uuid);
		return get(id);
	}
	
	public Document get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		Document document = new Document();
		document.setId(id);
		
		try {
			List<Document> types = documentDAO.read(document);
			return types.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving document with id %d", id));
		}
	}
	
	private DocumentDO makeDO(final Document document, final UserDO userDO) throws NoSuchAlgorithmException, ContracticServiceException, InterruptedException{
		String hashId = HashingUtil.get(document.getId());
		DocumentDO documentDO = new DocumentDO();
		documentDO.setId(hashId);
		documentDO.setDescription(document.getDescription());
		documentDO.setCreated(Long.toString(document.getReceived().getTime()));
		documentDO.setTitle(document.getTitle());
		
		
		final Map<String, Object> params = new HashMap<>();
		
		// Get and populate the external reference (object to which this document belongs)
		// Null is returned if the document is not accessible to the session user
		ValueDO externalValueDO = new ValueDO();
		params.clear();
		if(document.getModule().getName().equalsIgnoreCase("Contract")){
			params.put(CommonService.USER, userDO);
			params.put(ContractService.GET_LOG_READ, Boolean.FALSE);
			params.put(ContractService.GET_PARAM_ID, document.getExternalId());
			final List<ContractDO> contractList = contractService.get(params);
			if(contractList.size() == 0)
				return null;
			assert contractList.size() == 1 : String.format("Found %s contracts with id %s", contractList.size(), document.getExternalId());
			ContractDO contractDO = contractList.get(0);
			externalValueDO.setId(contractDO.getId());
			externalValueDO.setValue(contractDO.getTitle());
		}
		documentDO.setExternal(externalValueDO);
		
		// Populate the ModuleId
		ModuleDO moduleDO = CommonService.getModule(moduleService, document.getModule().getName());
		documentDO.setModule(new ValueDO(moduleDO.getId(), moduleDO.getName()));
		
		// Populate the document type
		params.clear();
		params.put(CommonService.USER, userDO);
		params.put(DocumentTypeService.GET_PARAM_ORGANISATIONID, userDO.getOrganisationId());
		params.put(DocumentTypeService.GET_PARAM_ID, document.getDocumentType().getId());
		List<DocumentTypeDO> doctypeList = documentTypeService.get(params);
		assert(doctypeList.size() == 1);
		documentDO.setType(new ValueDO(doctypeList.get(0).getId(), doctypeList.get(0).getDescription()));
		
/*		if(document.getStatus() != null){
			// Populate the status
			params.clear();
			params.put(StatusService.GET_PARAM_ID, document.getStatus().getId());
			params.put(StatusService.GET_PARAM_ORGANISATIONID, organisationDO.getId());
			List<StatusDO> statusList = statusService.get(params);
			assert(statusList.size() == 1);
			documentDO.setStatus(new ValueDO(statusList.get(0).getId(), statusList.get(0).getName()));
		}*/
		

		
		final File documentFile = new File(this.documentFileRoot, document.getFilePath());
		if(documentFile.exists()){
			FileDO fileDO = new FileDO();
			fileDO.setPath(documentFile.getPath());
			fileDO.setType(document.getFileType());
			
			// This updates the file service cache so that the file is then available for download
			String ext = FilenameUtils.getExtension(document.getFilePath());
			
			String name = document.getFileName();
			if(!StringUtils.hasText(name))
				name = StringUtils.hasText(ext) ? "unnamed." + ext : name;
			
			fileDO.setName(name);
			
			fileDO.setExt(StringUtils.hasText(ext) ? ext : "");
			fileService.update(fileDO);
			documentDO.setFile(fileDO);
			documentDO.setSize(Long.toString(documentFile.length()));
		}
			
		
		return documentDO;
	}
	
	/**
	 * Retrieves documents based on supplied query parameters
	 * @param params
	 * @return
	 * @throws ContracticServiceException
	 */
	public List<DocumentDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASHID);
		 final String code = (String)params.get(GET_PARAM_CODE);
		 final String externalIdString = (String)params.get(GET_PARAM_EXTERNALID);
		 final String moduleId = (String)params.get(GET_PARAM_MODULEID);
		 
		 final String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);
		 
		final UserDO userDO = (UserDO)params.get(CommonService.USER);
		if(userDO == null)
			throw new ContracticServiceException("Invalid session user provided");
		
		 
		if(!StringUtils.hasText(organisationId)){
			throw new ContracticServiceException("OrganisationId not supplied");
			}
		 
		Long id = (Long)params.get(GET_PARAM_ID);

		List<Document> documentList = null;
		try {
			Document document = new Document();
			
			Organisation organisation = organisationService.get(organisationId);
			if(organisation == null)
				throw new ContracticServiceException("Invalid OrganisationId supplied");
			
			document.setOrganisation(organisation);
			
			if(StringUtils.hasText(hash)){
				id = this.documentIdCache.get(hash);
			}
			
			if(id !=null){
				document.setId(id);
			}
			
			if(StringUtils.hasText(externalIdString)){
				// Populate the ModuleId
				final Module module = moduleService.get(moduleId);
				document.setModule(module);
				
				Long externalId = commonService.getExternalId(externalIdString, module);
				document.setExternalId(externalId);
			}
							
			documentList = documentDAO.read(document);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read documents", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		final List<DocumentDO> pojoList = documentList.stream()
			.parallel()
			.map(document -> {
				DocumentDO documentDO = null;
				try{
					documentDO = makeDO(document, userDO);
					
					/*
					 * if a document is not accessible because the accessing user has no permission to the
					 * underlying owning module, then we get a null returned
					 */
					if(documentDO != null){
						documentIdCache.put(documentDO.getId(), document.getId());
					}

				}catch(final ContracticServiceException | NoSuchAlgorithmException | InterruptedException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the document %s", document), ex));
				}
				
				return documentDO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		/*
		 * Here we have no errors but filter out any null documents
		 */
		return pojoList.stream().filter(document -> document!=null).collect(Collectors.toList());
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.documentIdCache = new Cache(DOCUMENTTYPE_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	public MessageDO delete(DocumentDO documentDO) throws ContracticServiceException {
		final String hash = documentDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Invalid document hash id provided");
			
		final Long id = documentIdCache.get(hash);	

		
		// Save the person object
		try {

			Document document = new Document();
			document.setId(id);
			List<Document> documentList = documentDAO.read(document);
			assert(documentList.size()==1);
			document = documentList.get(0);
			
			documentDAO.delete(document);
		
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update document: %s", documentDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Document type '%s' updated successfully", documentDO.getTitle()), MessageDO.OK);
	}
	
	private File saveFile(FileDO fileDO) throws IOException{
		File file = new File(fileDO.getPath());
		File destinationFile = new File(this.documentFileRoot, file.getName());
		
		Path path = Paths.get(destinationFile.toURI());
		Files.write(path, Files.readAllBytes(Paths.get(file.toURI())));
		
		return destinationFile;
	}

	private void saveSearchEntity(Document doc) {
		ConcurrencyUtil.IOBoundExecutorService.submit(() -> {
			SearchEntityDO searchEntity = new SearchEntityDO();
			searchEntity.setId(doc.getId());
			searchEntity.setCollection(Module.DOCUMENT.toLowerCase());
			searchEntity.setOrganisationId(doc.getOrganisation().getId());
			searchEntity.setText(String.join(",", new String[]{doc.getTitle(), doc.getDescription(), doc.getFileName()}));

			try {
				searchService.create(searchEntity);
			} catch (Exception e) {
				logger.warn(String.format("An error occurred when indexing document '%s'",  doc.getTitle()), e);
			}
		});
	}
	
	public MessageDO update(DocumentDO documentDO) throws ContracticServiceException {
		final String hash = documentDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Invalid document hash id provided");
			
		final Long id = documentIdCache.get(hash);
				
		// Save the object
		try {
			
			Document document = new Document();
			document.setId(id);
			
			Organisation organisation = organisationService.get(documentDO.getOrganisationId());
			document.setOrganisation(organisation);

			List<Document> documentList = documentDAO.read(document);
			assert(documentList.size()==1);
			document = documentList.get(0);
			
			document.setTitle(documentDO.getTitle());
			document.setDescription(documentDO.getDescription());
			
			if(documentDO.getFile()!=null && StringUtils.hasText(documentDO.getFile().getId())){
				final FileDO fileDO = fileService.get(documentDO.getFile().getId());
				if(fileDO!=null && Paths.get(fileDO.getPath()).toFile().exists()){
					File documentFile = saveFile(fileDO);
					String relativePath = new File(this.documentFileRoot).toURI().relativize(documentFile.toURI()).getPath();
					document.setFilePath(relativePath);
					document.setFileName(fileDO.getName());
					document.setFileType(fileDO.getType());
				}
			}

			// Populate the document type
			final DocumentType documentType = documentTypeService.get(documentDO.getType().getId());
			document.setDocumentType(documentType);
			
			// Populate the status
			if(documentDO.getStatus()!=null && StringUtils.hasText(documentDO.getStatus().getId())){
				final Status status = statusService.get(documentDO.getStatus().getId());
				document.setStatus(status);
			}
			
			documentDAO.update(document);
		
			saveSearchEntity(document);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update document: %s", documentDO), e);
		} catch (IOException e) {
			throw new ContracticServiceException(String.format("Failed to update document: %s", documentDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Document '%s' updated successfully", documentDO.getTitle()), MessageDO.OK);
	}

	
	public MessageDO create(final DocumentDO documentDO) throws ContracticServiceException {
		final Document document = new Document();
		document.setTitle(documentDO.getTitle());
		document.setDescription(documentDO.getDescription());
		document.setReceived(new Date());
		
		Organisation organisation = organisationService.get(documentDO.getOrganisationId());
		document.setOrganisation(organisation);
		
		// Populate the ModuleId
		final Module module = moduleService.get(documentDO.getModule().getId());
		document.setModule(module);

		//Populate the external id
		final Long externalId = commonService.getExternalId(documentDO.getExternal().getId(), document.getModule());
		document.setExternalId(externalId);

		// Populate the document type
		final DocumentType documentType = documentTypeService.get(documentDO.getType().getId());
		document.setDocumentType(documentType);
		
		// Populate the status
		if(documentDO.getStatus()!=null && StringUtils.hasText(documentDO.getStatus().getId())){
			final Status status = statusService.get(documentDO.getStatus().getId());
			document.setStatus(status);
		}
		
		// Save the document object
		try {
			
			if(documentDO.getFile()==null || !StringUtils.hasText(documentDO.getFile().getId())){
				throw new ContracticServiceException("Document is missing a file attachment. Please attach file before uploading");
			}
			
			final FileDO fileDO = fileService.get(documentDO.getFile().getId());
			
			if(fileDO==null || !Paths.get(fileDO.getPath()).toFile().exists()){
				throw new ContracticServiceException("Document attachment is invalid or missing");
			}
			
			File documentFile = saveFile(fileDO);
			String relativePath = new File(this.documentFileRoot).toURI().relativize(documentFile.toURI()).getPath();
			document.setFilePath(relativePath);
			document.setFileName(fileDO.getName());
			document.setFileType(fileDO.getType());
			
			documentDAO.create(document);
			
			String hashId = HashingUtil.get(document.getId());
			// Update the cache
			this.documentIdCache.put(hashId, document.getId());
		
			saveSearchEntity(document);
			
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create document : %s", documentDO), e);
		} catch (IOException e) {
			throw new ContracticServiceException(String.format("Failed to create document : %s", documentDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Document '%s' created successfully", documentDO.getTitle()), MessageDO.OK);
	}


}
