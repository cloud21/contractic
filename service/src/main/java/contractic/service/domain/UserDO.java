package contractic.service.domain;

import java.util.Map;

import org.apache.commons.lang.WordUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * User domain object
 * @author Michael Sekamanya
 *
 */
@JsonIgnoreProperties({"password", "organisationId"})
public class UserDO {
	public static final String LAST_LOGIN = "lastlogin";
	public static final String RESET_KEY = "resetkey";
	private String id;
	private String login;
	private String password;
	private String organisationId;
	private String personId;
	private String personName;
	private String personShortName;
	private Map<String, Object> profile;
	private Boolean active;
	private PermissionDO[] permission;
	private ValueDO role;
	private Map<String, Object> organisation;
	private PersonDO person;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPersonId() {
		return personId;
	}
	
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	public Map<String, Object> getProfile() {
		return profile;
	}
	public void setProfile(Map<String, Object> profile) {
		this.profile = profile;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	// These permissions are only useful when creating the user.
	// To update the user's permissions, use the permission's service
	public PermissionDO[] getPermission() {
		return permission;
	}
	public void setPermission(PermissionDO[] permission) {
		this.permission = permission;
	}

	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getPersonShortName() {
		if(this.personName == null)
			return null;
		return WordUtils.initials(personName);
	}
	public void setPersonShortName(String personShortName) {
		this.personShortName = personShortName;
	}
	public ValueDO getRole() {
		return role;
	}
	public void setRole(ValueDO role) {
		this.role = role;
	}
	public Map<String, Object> getOrganisation() {
		return organisation;
	}
	public void setOrganisation(Map<String, Object> organisation) {
		this.organisation = organisation;
	}
	public PersonDO getPerson() {
		return person;
	}
	public void setPerson(PersonDO person) {
		this.person = person;
	}

}
