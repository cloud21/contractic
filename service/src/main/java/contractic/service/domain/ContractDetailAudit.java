package contractic.service.domain;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import contractic.model.data.store.ContractDetail;

public class ContractDetailAudit implements AuditJob<ContractDetailDO> {

	private final List<AuditDO> auditList = new LinkedList<>();

	@Override
	public List<AuditDO> run(ContractDetailDO source, ContractDetailDO target) {
		ValueDO sourceGroup = source.getGroup();
		ValueDO targetGroup = target.getGroup();
		
		String detailType = null;
		
		if(!Objects.equals(source.getDescription(), target.getDescription())){
			if(Objects.equals(sourceGroup, targetGroup) && targetGroup!=null){
				if(Objects.equals(targetGroup.getValue(), ContractDetail.VALUE_GROUP_FIELD)){
					detailType = ContractDetail.VALUE_GROUP_FIELD;
				}else if(Objects.equals(targetGroup.getValue(), ContractDetail.SUMMARY_GROUP_FIELD)){
					detailType = ContractDetail.SUMMARY_GROUP_FIELD;
				}else if(Objects.equals(targetGroup.getValue(), ContractDetail.CCN_GROUP_FIELD)){
					detailType = ContractDetail.CCN_GROUP_FIELD;
				}else{
					detailType = ContractDetail.DETAIL;
				}
			}
		}
		
		// Log change
		if(!Objects.equals(source.getName(), target.getName()))
			auditList.add(AuditJob.createAudit(ContractDetail.NAME, source.getName(), target.getName()));
		
		if(!Objects.equals(source.getText(), target.getText()))
			auditList.add(AuditJob.createAudit(ContractDetail.VALUE, source.getText(), target.getText()));

		if(detailType != null){
			if(!Objects.equals(target.getValue(), source.getValue()))
				auditList.add(AuditJob.createAudit(detailType, source.getValue().toString(), target.getValue().toString()));
			if(!Objects.equals(target.getPeriod(), source.getPeriod()))
				auditList.add(AuditJob.createAudit(detailType, source.getPeriod().toString(), target.getPeriod().toString()));
			
		}
		
		if(!Objects.equals(sourceGroup, targetGroup) && sourceGroup!=null){
			auditList.add(AuditJob.createAudit(ContractDetail.GROUP, sourceGroup.getValue(), targetGroup.getValue()));
		}
		
		return auditList;
	}

}
