package contractic.service.domain;

/**
 * This object is used to return values to controllers that would need an id and their display component
 * @author Michael Sekamanya
 *
 */
public class ValueDO {
	private String id;
	private String value;
	
	public ValueDO(){
	}
	
	public ValueDO(String id){
		this.id = id;
	}
	
	public ValueDO(String id, String value){
		this.id = id;
		this.value = value;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValueDO other = (ValueDO) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
}
