package contractic.service.domain;

import java.util.List;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Module;
import contractic.model.data.store.User;
import contractic.model.data.store.UserRole;
import contractic.model.data.store.UserRolePermission;
import contractic.model.data.store.UserRolePermissionAction;

public class ServiceAccessValidator {
	
	/*
	 * NOTE:
	 * For this to work, we are not accessing any other domain service other than the user service.
	 * This is because domain services methods are already patched with security checkers so we have to
	 * circumvent this by using direct access to DAO.
	 * 
	 * The only service used here the userService is not subject to this because the method used userService.get()
	 * is not patched with security checkers since it should not be used by the controller or client API
	 * 
	 */
	
	private final CrudOperation<Module> moduleDAO;
	private final CrudOperation<UserRolePermission> userRolePermissionDAO;
	private final DomainService<User, Long, UserDO, MessageDO> userService;
	public ServiceAccessValidator(final CrudOperation<Module> moduleDAO, 
			final CrudOperation<UserRolePermission> userRolePermissionDAO, 
			final DomainService<User, Long, UserDO, MessageDO> userService){
		this.moduleDAO = moduleDAO;
		this.userRolePermissionDAO = userRolePermissionDAO;
		this.userService = userService;
	}
	
	private UserRolePermission getPermissions(String moduleName, UserRole userRole) throws ContracticModelException{
		final UserRolePermission permission = new UserRolePermission();
		Module module = ModuleService.getModule(this.moduleDAO, moduleName);
		permission.setModule(module);
		permission.setUserRole(userRole);
		List<UserRolePermission> roleList = userRolePermissionDAO.read(permission);
		if(roleList.size()!=1)
			return null;
		return roleList.get(0);
	}
	
	public void checkPermission(UserDO userDO, UserRolePermissionAction action, String moduleName)
			throws ContracticServiceException {
		User user = userService.get(userDO.getId());
		String message = "Could not perform action. Access denied";
		
		UserRole userRole = user.getUserRole();
		
		/*
		 * The Administrator role is hard corded as the admin role and anyone assigned to this role will always have permission
		 * to do anything
		 * */
		if(userRole.getName().equals("Administrator"))
			return;
		
		try{
			UserRolePermission userPermission = this.getPermissions(moduleName, userRole);		
			if(userPermission == null || checkActionPermission(action, userPermission) == false){
				throw new ContracticServiceException(message, new MessageDO(null, "Permission Error", message, MessageDO.ERROR));
			}
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while checking user permissions", e);
		}
	}

	private Boolean checkActionPermission(UserRolePermissionAction action,
			UserRolePermission userPermission) {
		Boolean allowed;
		switch (action){
		case READ:
			allowed = userPermission.getRead();
			break;
		case WRITE:
			allowed = userPermission.getWrite();
			break;
		case DELETE:
			allowed = userPermission.getDelete();
			break;
			default:
				throw new RuntimeException("Unsupported action supplied");
		}
		return allowed;
	}
}
