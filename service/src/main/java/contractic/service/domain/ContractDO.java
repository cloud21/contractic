package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"user", "organisationId"})
public class ContractDO {
	private String id;
	private ValueDO owner;
	private ValueDO status;
	private ValueDO supplier;
	private ValueDO department;
	private ValueDO procedure;
	private String title;
	private ContractValueDO value;
	private String description;
	private ValueDO type;
	private String startDate;
	private String endDate;
	private ValueDO group;
	private String reference;
	private String organisationId;
	private Double initialValue;
	private String valueUOM;
	private ContractTermDO[] term;
	
	// Session User
	private UserDO user;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ValueDO getOwner() {
		return owner;
	}
	public void setOwner(ValueDO owner) {
		this.owner = owner;
	}
	public ValueDO getStatus() {
		return status;
	}
	public void setStatus(ValueDO status) {
		this.status = status;
	}
	public ValueDO getSupplier() {
		return supplier;
	}
	public void setSupplier(ValueDO supplier) {
		this.supplier = supplier;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public ValueDO getType() {
		return type;
	}
	public void setType(ValueDO type) {
		this.type = type;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public ValueDO getGroup() {
		return group;
	}
	public void setGroup(ValueDO group) {
		this.group = group;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public ContractValueDO getValue() {
		return value;
	}
	public void setValue(ContractValueDO value) {
		this.value = value;
	}
	public UserDO getUser() {
		return user;
	}
	public void setUser(UserDO user) {
		this.user = user;
	}
	public ValueDO getDepartment() {
		return department;
	}
	public void setDepartment(ValueDO department) {
		this.department = department;
	}
	public ValueDO getProcedure() {
		return procedure;
	}
	public void setProcedure(ValueDO procedure) {
		this.procedure = procedure;
	}
	public Double getInitialValue() {
		return initialValue;
	}
	public void setInitialValue(Double valueEstimate) {
		this.initialValue = valueEstimate;
	}
	public String getValueUOM() {
		return valueUOM;
	}
	public void setValueUOM(String valueUOM) {
		this.valueUOM = valueUOM;
	}
	public ContractTermDO[] getTerm() {
		return term;
	}
	public void setTerm(ContractTermDO[] term) {
		this.term = term;
	}
	
}
