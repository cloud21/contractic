package contractic.service.domain;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import contractic.model.data.store.Task;
import contractic.service.util.DateUtil;

/**
 * This class is used to create an audit trail of changes on the source object that are picked form the target object
 * @author Michael Sekamanya
 *
 */
public interface AuditJob<T> {
	List<AuditDO> run(T source, T target);
	public static AuditDO createAudit(String field, String from, String to){
		AuditDO auditDO = new AuditDO();
		auditDO.setField(field);
		auditDO.setFrom(from);
		auditDO.setTo(to);
		return auditDO;
	}
	
	public static AuditDO auditDateFieldChange(String source, String target, String fieldName){
		if(source == null && target == null)
			return null;
		final Date startDate = DateUtil.tryParse(target);
		String startDateString = null;
		if(startDate != null)
			startDateString = Long.toString(startDate.getTime());
		if(!Objects.equals(source, startDateString))
			return AuditJob.createAudit(fieldName, source, startDateString);
		return null;
	}
	
	public static AuditDO auditForeignFieldChange(ValueDO source, ValueDO target, String fieldName){
		if(source == null && target == null)
			return null;
		else if((source == null) != (target == null))
			return AuditJob.createAudit(fieldName, source == null ? null : source.getValue(), 
					target == null ? null : target.getValue());
		else if(!Objects.equals(source.getValue(), target.getValue()))
			return AuditJob.createAudit(fieldName, source.getValue(), target.getValue());
		return null;
	}
}
