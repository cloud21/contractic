package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"collection", "id"})
public class SearchEntityDO {
	/*Top level module*/
	private ModuleDO module;
	private Object external;
	/*Second level modules*/
	private ContractDetailDO contractDetail;
	private NoteDO note;
	private TaskDO task;
	private DocumentDO document;
	
	/*These are internal fields and should not be exposed via the REST service
	 * They mirror the SearchEntity object
	 * */
	private Long id;
	private String collection;
	private String text;
	private Long organisationId;
	
	public String getCollection() {
		return collection;
	}
	public void setCollection(String collection) {
		this.collection = collection;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}
	public ModuleDO getModule() {
		return module;
	}
	public void setModule(ModuleDO module) {
		this.module = module;
	}
	public Object getExternal() {
		return external;
	}
	public void setExternal(Object external) {
		this.external = external;
	}
	public ContractDetailDO getContractDetail() {
		return contractDetail;
	}
	public void setContractDetail(ContractDetailDO contractDetail) {
		this.contractDetail = contractDetail;
	}
	public NoteDO getNote() {
		return note;
	}
	public void setNote(NoteDO note) {
		this.note = note;
	}
	public TaskDO getTask() {
		return task;
	}
	public void setTask(TaskDO task) {
		this.task = task;
	}
	public DocumentDO getDocument() {
		return document;
	}
	public void setDocument(DocumentDO document) {
		this.document = document;
	}
}
