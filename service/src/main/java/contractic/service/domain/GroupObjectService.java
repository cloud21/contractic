package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Group;
import contractic.model.data.store.GroupObject;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class GroupObjectService implements DomainService<GroupObject, Long, GroupObjectDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	public final static String GROUPOBJECT_IDCACHE_NAME = "groupobject.idcache.name";
	public static final String GET_PARAM_ID = "group.id";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public static final String GET_PARAM_ACTIVE = "active";
	public static final String USERID = "user.id";
	
	private Cache<String, Long> groupObjectIdCache;
	
	@Autowired
	private CrudOperation<GroupObject> groupObjectDAO;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;

	
	public GroupObject get(final String uuid) throws ContracticServiceException{
		final Long id = this.groupObjectIdCache.get(uuid);
		return get(id);
	}
	
	public GroupObject get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		GroupObject group = new GroupObject();
		group.setId(id);
		
		try {
			List<GroupObject> groupList = groupObjectDAO.read(group);
			GroupObject groupSet = groupList.get(0);
			return groupSet;
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving group with id %d", id));
		}
	}
	
	
	private GroupObjectDO makeDO(final GroupObject groupObject) throws NoSuchAlgorithmException, ContracticServiceException{
		String hashId = HashingUtil.get(groupObject.getId());
		GroupObjectDO groupObjectDO = new GroupObjectDO();
		groupObjectDO.setId(hashId);
		groupObjectDO.setActive(Boolean.toString(groupObject.getActive()));
				
		return groupObjectDO;
	}
	
	public List<GroupObjectDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASH);
		 Long id = (Long)params.get(GET_PARAM_ID);

		 if(id == null || !StringUtils.hasText(hash))
			 throw new ContracticServiceException("Either Id or the hash Id must be supplied");
		 
		List<GroupObject> groupList = null;
		try {
			GroupObject groupObject = new GroupObject();
		
			groupObject.setId(id);
			if(StringUtils.hasText(hash)){
				id = this.groupObjectIdCache.get(hash);
			}
			
			groupList = groupObjectDAO.read(groupObject);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read groups", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		final List<GroupObjectDO> pojoList = groupList.stream()
			.map(group -> {
				GroupObjectDO groupDO = null;
				try{
					groupDO = makeDO(group);						
					// Side effect 1
					groupObjectIdCache.put(groupDO.getId(), group.getId());

				}catch(final ContracticServiceException | NoSuchAlgorithmException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the group %s", group)));
				}
				
				return groupDO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.groupObjectIdCache = new Cache(GROUPOBJECT_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	public MessageDO delete(GroupObjectDO groupDO) throws ContracticServiceException {
		return null;
	}

	public MessageDO update(GroupObjectDO groupDO) throws ContracticServiceException {
		return null;
	}

	/**
	 * Creates the group. Validation of input is left to the client (e.g. controllers)
	 * @param groupSetDO
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO create(GroupObjectDO groupSetDO) throws ContracticServiceException {
		return null;
	}
}
