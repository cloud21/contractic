package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Group;
import contractic.model.data.store.GroupObject;
import contractic.model.data.store.GroupSet;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class GroupService implements DomainService<Group, Long, GroupDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	public final static String DOCUMENTTYPE_IDCACHE_NAME = "group.idcache.name";
	public static final String GET_PARAM_ID = "group.id";
	public static final String GET_PARAM_MODULEID = "module.id";
	public static final String GET_PARAM_ACTIVE = "active";
	public static final String GET_PARAM_SEARCH_QUERY = "search.query";
	public static final String USERID = "user.id";
	public static final String GET_PARAM_GROUPSETID = "groupset.id";
	public static final String GET_PARAM_EXTERNALID = "external.id";
	public static final String GET_PARAM_GROUPSET = "group.set";
	public static final String GET_PARAM_MODULE = "module";
	
	private Cache<String, Long> groupIdCache;
	
	@Autowired
	private CrudOperation<Group> groupDAO;
	@Autowired
	private CrudOperation<GroupObject> groupObjectDAO;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	@Autowired
	private DomainService<GroupSet, Long, GroupSetDO, MessageDO> groupSetService;
	@Autowired
	private CommonService commonService;
	@Autowired
	private DomainService<GroupObject, Long, GroupObjectDO, MessageDO> groupObjectService;
	
	public Group get(final String uuid) throws ContracticServiceException{
		final Long id = this.groupIdCache.get(uuid);
		return get(id);
	}
	
	public Group get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		Group group = new Group();
		group.setId(id);
		
		try {
			List<Group> groupList = groupDAO.read(group);
			return groupList.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving group with id %d", id));
		}
	}
	
	
	private GroupDO makeDO(final Group group, String organisationId) throws NoSuchAlgorithmException, ContracticServiceException{
		String hashId = HashingUtil.get(group.getId());
		GroupDO groupDO = new GroupDO();
		groupDO.setId(hashId);
		groupDO.setName(group.getName());
		groupDO.setDescription(group.getDescription());
		groupDO.setActive(group.getActive());
		
		Map<String, Object> params = new HashMap<>();
		Module module = group.getModule();
		if(module != null){
			params.put(ModuleService.GET_PARAM_ID, module.getId());
			List<ModuleDO> moduleList = moduleService.get(params);
			assert(moduleList.size() == 1);
			groupDO.setModuleId(moduleList.get(0).getId());
		}
		
		params.clear();
		GroupSet groupSet = group.getGroupSet();
		if(groupSet != null){
			params.put(GroupSetService.GET_PARAM_ORGANISATIONID, organisationId);
			params.put(GroupSetService.GET_PARAM_ID, groupSet.getId());
			List<GroupSetDO> groupSetList = groupSetService.get(params);
			assert(groupSetList.size() == 1);
			GroupSetDO groupSetDO = groupSetList.get(0);
			groupDO.setGroupset(new ValueDO(groupSetDO.getId(), groupSetDO.getName()));
		}
		
		return groupDO;
	}
	
	public List<GroupDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASH);
		 final String name = (String)params.get(GET_PARAM_NAME);
		 final Long id = (Long)params.get(GET_PARAM_ID);
		 final String groupSetId = (String)params.get(GET_PARAM_GROUPSETID);
		 final String moduleId = (String)params.get(GET_PARAM_MODULEID);
		 Module module = (Module)params.get(GET_PARAM_MODULE);
		 GroupSet groupSet = (GroupSet)params.get(GET_PARAM_GROUPSET);
		 
		 // When this is supplied, then we have to look for the group in the GroupObject collection
		 final Long externalId = (Long)params.get(GET_PARAM_EXTERNALID);
		 
		 final String queryString = (String)params.get(GET_PARAM_SEARCH_QUERY);

		 final UserDO userDO = (UserDO)params.get(CommonService.USER);
		 String organisationId = userDO.getOrganisationId();
		if(!StringUtils.hasText(organisationId)){
			throw new ContracticServiceException("OrganisationId not supplied");
		}
		List<Group> groupList = null;
		try {
			Group group = new Group();
			
			Organisation organisation = organisationService.get(organisationId);
			group.setOrganisation(organisation);
			
			
			// This allows us to get a group for a specified object
			if(externalId!=null && StringUtils.hasText(moduleId) && StringUtils.hasText(groupSetId)){
				// Get the module
				module = moduleService.get(moduleId);
				groupSet = groupSetService.get(groupSetId);			
				GroupObject groupObject = new GroupObject();
				
				assert groupSet.getOrganisation().getId() == organisation.getId() : "Organisations does not match";
				
				groupObject.setGroupSet(groupSet);
				groupObject.setExternalId(externalId);
				groupObject.setModule(module);
				List<GroupObject> groupObjectList = groupObjectDAO.read(groupObject);

				groupList = groupObjectList.stream().map(go -> go.getGroup()).collect(Collectors.toList());
				
			}else if(externalId!=null && module!=null && groupSet!=null){
				// This branch is a query from to find which group an object is in
				// The query will typically have been initiated from within the services domain package class 
				// E.g. to check if an object e.g. a contract is in the same group as the querying user
				GroupObject groupObject = new GroupObject();
				
				assert groupSet.getOrganisation().getId() == organisation.getId() : "Organisations does not match";
				
				groupObject.setGroupSet(groupSet);
				groupObject.setExternalId(externalId);
				groupObject.setModule(module);
				List<GroupObject> groupObjectList = groupObjectDAO.read(groupObject);
				groupList = groupObjectList.stream().map(go -> go.getGroup()).collect(Collectors.toList());
			}else{
				
				group.setOrganisation(organisation);
				
				if(StringUtils.hasText(name)){
					group.setName(name);
				}
				
				if(queryString!=null){
					group.setDescription(queryString);
				}
				
				group.setId(id);
				if(StringUtils.hasText(hash)){
					group.setId(this.groupIdCache.get(hash));
				}
				
				// This code branch is mainly for the groups with NULL groupset.
				// This logic is applicable to groups such as VALUE, CCN, SUMMARY of contracts which were developed before
				// GroupSets were introduced
				if(StringUtils.hasText(moduleId)){
					module = moduleService.get(moduleId);
					group.setModule(module);
				}
				
				if(StringUtils.hasText(groupSetId) && StringUtils.hasText(moduleId)){
					/*
					 *  if groupset has been supplied, we search for group in the groupobjects. This is because the groupset concept was introduced
					 *  after the group concept had been developed. So all groups in groupobjects will have their corresponding groupset.
					 *  Since groups in the group collection may not have moduleId (legacy groups such as VALUE, SUMMARY, CCN), we use
					 *  the groupobject collection to determine which groups belong to a groupset 
					 */
					groupSet = groupSetService.get(groupSetId);
					
					module = moduleService.get(moduleId);
								
					GroupObject groupObject = new GroupObject();
					
					assert groupSet.getOrganisation().getId() == organisation.getId() : "Organisations does not match";
					
					groupObject.setGroupSet(groupSet);
					groupObject.setModule(module);
					List<GroupObject> groupObjectList = groupObjectDAO.read(groupObject);
					groupList = new ArrayList<>(groupObjectList.stream().map(go -> go.getGroup()).collect(Collectors.toSet()));

				}else{
					if(StringUtils.hasText(groupSetId)){
						groupSet = groupSetService.get(groupSetId);
						group.setGroupSet(groupSet);
					}
					groupList = groupDAO.read(group);
				}
			}
			
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read groups", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		final List<GroupDO> pojoList = groupList.stream()
			.map(group -> {
				GroupDO groupDO = null;
				try{
					groupDO = makeDO(group, organisationId);						
					// Side effect 1
					groupIdCache.put(groupDO.getId(), group.getId());

				}catch(final ContracticServiceException | NoSuchAlgorithmException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the group %s", group), ex));
				}
				
				return groupDO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.groupIdCache = new Cache(DOCUMENTTYPE_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	public MessageDO delete(GroupDO groupDO) throws ContracticServiceException {
		final String hash = groupDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("");
			
		final Long id = groupIdCache.get(hash);	
		
		final String organisationId = groupDO.getOrganisationId();
		
		// Save the person object
		try {

			Group group = new Group();
			group.setId(id);
			Organisation organisation = organisationService.get(organisationId);
			group.setOrganisation(organisation);
			List<Group> groupList = groupDAO.read(group);
			assert(groupList.size()==1);
			group = groupList.get(0);
			
			groupDAO.delete(group);
		
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Failed to update group: %s", groupDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Group '%s' deleted successfully", groupDO.getName()), MessageDO.OK);
	}

	public MessageDO update(GroupDO groupDO) throws ContracticServiceException {
		final String hash = groupDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Invalid group hash id provided");
					
		final String organisationId = groupDO.getOrganisationId();
		if(!StringUtils.hasText(organisationId))
			throw new ContracticServiceException("Invalid organisation hash id provided");
		
		final Long id = groupIdCache.get(hash);
		
		// Save the person object
		try {

			Group group = new Group();
			group.setId(id);
			Organisation organisation = organisationService.get(organisationId);
			group.setOrganisation(organisation);
			List<Group> groupList = groupDAO.read(group);
			assert(groupList.size()==1);
			group = groupList.get(0);
			
			group.setName(WordUtils.capitalize(groupDO.getName()));
			group.setDescription(WordUtils.capitalize(groupDO.getDescription()));
			group.setActive(groupDO.getActive());
			String moduleId = groupDO.getModuleId();
			if(StringUtils.hasText(moduleId)){
				group.setModule(moduleService.get(moduleId));
			}
			
			groupDAO.update(group);
		
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Failed to update group: %s", groupDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Group '%s' updated successfully", groupDO.getName()), MessageDO.OK);
	}

	/**
	 * Creates the group. Validation of input is left to the client (e.g. controllers)
	 * @param groupDO
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO create(GroupDO groupDO) throws ContracticServiceException {
		final Group group = new Group();
		group.setName(WordUtils.capitalize(groupDO.getName()));
		group.setDescription(WordUtils.capitalize(StringUtils.hasText(groupDO.getDescription()) ? groupDO.getDescription() : groupDO.getName()));
		group.setActive(true);
		
		// Set the organisation
		Organisation organisation = organisationService.get(groupDO.getOrganisationId());
		group.setOrganisation(organisation);
		
		ValueDO groupSetDO = groupDO.getGroupset();
		if(groupSetDO!=null){
			String groupSetId = groupSetDO.getId();
			String groupSetName = groupSetDO.getValue();
			if(StringUtils.hasText(groupSetName)){
				final Map<String, Object> params = new HashMap<>();
				params.put(GroupSetService.GET_PARAM_NAME, groupSetName);
				params.put(GroupSetService.GET_PARAM_ORGANISATIONID, groupDO.getOrganisationId());
				List<GroupSetDO> groupSetList = groupSetService.get(params);
				assert groupSetList.size() == 1 : String.format("Could not find group set '%s", groupSetName);
				GroupSet groupSet = groupSetService.get(groupSetList.get(0).getId());
				group.setGroupSet(groupSet);
			}else if(StringUtils.hasText(groupSetId)){
				GroupSet groupSet = groupSetService.get(groupSetId);
				group.setGroupSet(groupSet);
			}
		}
		
		// Set the module
		if(StringUtils.hasText(groupDO.getModuleId())){
			Module module = moduleService.get(groupDO.getModuleId());
			group.setModule(module);
		}
		
		// Save the person object
		try {
			groupDAO.create(group);
			
			String hashId = HashingUtil.get(group.getId());
			// Update the cache
			this.groupIdCache.put(hashId, group.getId());
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create group: %s", groupDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Group '%s' created successfully", groupDO.getName()), MessageDO.OK);
	}
	
	/**
	 * Returns all groups that an object belongs to within a specific group set. An object can can exist in more than one group
	 * in a specific group set
	 * @param groupService
	 * @param externalId
	 * @param groupSetDO
	 * @param moduleDO
	 * @return
	 * @throws ContracticServiceException
	 */
	public static List<GroupDO> getObjectGroup(final DomainService<Group, Long, GroupDO, MessageDO> groupService, final Long externalId, GroupSetDO groupSetDO, ModuleDO moduleDO) throws ContracticServiceException{
		final Map<String, Object> params = new HashMap<>();
		params.put(GroupService.GET_PARAM_GROUPSETID, groupSetDO.getId());
		params.put(GroupService.GET_PARAM_MODULEID, moduleDO.getId());
		params.put(GroupService.GET_PARAM_EXTERNALID, externalId);
		params.put(CommonService.USER, groupSetDO.getUser());
		return groupService.get(params);
	}
	
	/**
	 * Save object group
	 * @param contract
	 * @param contractDO
	 * @throws ContracticServiceException
	 * @throws ContracticModelException
	 */
	private static void saveObjectGroup( 
			CrudOperation<GroupObject> groupObjectDAO, final Long externalId, Group group, GroupSet groupSet, Module module) throws ContracticServiceException, ContracticModelException{
		// 
		GroupObject groupObject = new GroupObject();
		groupObject.setGroup(group);
		groupObject.setModule(module);
		groupObject.setExternalId(externalId);
		groupObject.setGroupSet(groupSet);
		
		groupObjectDAO.create(groupObject);
		
	}
	
	public static void saveObjectGroup( 
			CrudOperation<GroupObject> groupObjectDAO, final Long externalId, Group[] group, GroupSet groupSet, Module module) throws ContracticServiceException, ContracticModelException{
		GroupObject groupObject = new GroupObject();
		groupObject.setModule(module);
		groupObject.setExternalId(externalId);
		groupObject.setGroupSet(groupSet);
		
		List<GroupObject> groupObjectList = groupObjectDAO.read(groupObject);
		List<ContracticServiceException> cse = new ArrayList<>();
		groupObjectList.stream().forEach(gO -> {
			try {
				groupObjectDAO.delete(gO);
			} catch (Exception e) {
				cse.add(new ContracticServiceException(String.format("An error occured when deleting group object %s", gO), e));
			}
		});
		if(cse.size() > 0)
			throw cse.get(0);
		
		//Set<Group> groupS = Arrays.stream(group).collect(Collectors.toSet());
		
		Arrays.stream(group).collect(Collectors.toSet()).forEach((gO -> {
			try {
				saveObjectGroup(groupObjectDAO, externalId, gO, groupSet, module);
			} catch (Exception e) {
				cse.add(new ContracticServiceException(String.format("An error occured when saving group object %s", gO), e));
			}
		}));
		if(cse.size() > 0)
			throw cse.get(0);
	}
		// 
}
