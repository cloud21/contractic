package contractic.service.domain;

public class ModuleDO {
	private String id;
	private String name;
	private String description;
	
	public ModuleDO(){}
	
	public ModuleDO(String id){
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
