package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Address;
import contractic.model.data.store.AddressType;
import contractic.model.data.store.Organisation;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public final class AddressService implements DomainService<Address, Long, AddressDO, MessageDO> {
	
	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_TYPEID = "type.id";
	public final static String GET_PARAM_ORGANISATIONID = "organisation.id";
	
	public final static String ADDRESS_IDCACHE_NAME = "address.idcache.name";
	private static final String GET_PARAM_SECTION1 = "address.section1";
	private static final String GET_PARAM_SECTION2 = "address.section2";
	private static final String GET_PARAM_SECTION3 = "address.section3";
	private static final String GET_PARAM_SECTION4 = "address.section4";
	private static final String GET_PARAM_SECTION5 = "address.section5";
	private static final String GET_PARAM_SECTION6 = "address.section6";
	public static final String GET_PARAM_ID = "address.id";
	public static final String GET_PARAM_SEARCH_QUERY = "search.query";
	public static final String GET_PARAM_ACTIVE = "active";

	// This cache helps us hide database IDs from each request
	private Cache<String, Long> addressIdCache;

	@Autowired
	private CrudOperation<Address> addressDAO;

	@Autowired
	private OrganisationService organisationService;


	@Autowired
	private DomainService<AddressType, Long, StaticDO, MessageDO> addressTypeService;


	// Public methods accessible to controllers
/*	public AddressDO getByUUID(String uuid) throws ContracticServiceException {
		Long id = addressIdCache.get(uuid);
		return get(id);
	}*/

	private AddressDO makeDO(Address address, String organisationId) throws NoSuchAlgorithmException, ContracticServiceException {
		AddressDO addressDO = new AddressDO();
		String hashId = HashingUtil.get(address.getId());
		addressDO.setId(hashId);
		addressDO.setSection1(address.getSection1());
		addressDO.setSection2(address.getSection2());
		addressDO.setSection3(address.getSection3());
		addressDO.setSection4(address.getSection4());
		addressDO.setSection5(address.getSection5());
		
		Map<String, Object> params = new HashMap<>();
		params.put(AddressTypeService.GET_PARAM_ID, address.getType().getId());
		List<StaticDO> typeList = addressTypeService.get(params);
		assert typeList.size() == 1;
		StaticDO typeDO = typeList.get(0);
		addressDO.setType(new ValueDO(typeDO.getId(), typeDO.getValue()));
		
		return addressDO;
	}

	public List<AddressDO> get(final Map<String, Object> params) throws ContracticServiceException {
		
		final String hash = (String)params.get(GET_PARAM_HASH);
		final Long id = (Long)params.get(GET_PARAM_ID);
		final String typeId = (String)params.get(GET_PARAM_TYPEID);
		final String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);
		final String section1 = (String)params.get(GET_PARAM_SECTION1);
		final String section2 = (String)params.get(GET_PARAM_SECTION2);
		final String section3 = (String)params.get(GET_PARAM_SECTION3);
		final String section4 = (String)params.get(GET_PARAM_SECTION4);
		final String section5 = (String)params.get(GET_PARAM_SECTION5);
		final String section6 = (String)params.get(GET_PARAM_SECTION6);
		final String searchQuery = (String)params.get(GET_PARAM_SEARCH_QUERY);
		
		List<Address> addresses = null;
		try {
			Address address = new Address();
			
			if(StringUtils.hasText(typeId)){
				final AddressType addressType = this.addressTypeService.get(typeId);
				address.setType(addressType);
			}
			
			if(!StringUtils.hasText(organisationId)){
				throw new ContracticServiceException("Organisation id must be supplied");
			}
			
			final Organisation organisation = this.organisationService.get(organisationId);
			address.setOrganisation(organisation);
			
			address.setId(id);
			if(StringUtils.hasText(hash)){
				address.setId(this.addressIdCache.get(hash));
			}
			
			if(StringUtils.hasText(searchQuery)){
				address.setSection1(searchQuery);
			}
			
			addresses = addressDAO.read(address);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read organisations",e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();

		List<AddressDO> pojoList = addresses
				.stream()
				.map(address -> {
					AddressDO orgPOJO = null;
					try {
						orgPOJO = this.makeDO(address, organisationId);

						// Side effect 1
						addressIdCache.put(orgPOJO.getId(), address.getId());

					} catch (NoSuchAlgorithmException ex) {
						cse.add(new ContracticServiceException(String.format("An error occured when hashing the address %s", address)));
					} catch(ContracticServiceException ex){
						cse.add(ex);
					}

					return orgPOJO;
				}).collect(Collectors.toList());

		if (cse.size() != 0)
			throw cse.get(0);

		return pojoList;
	}

	/**
	 * Update an existing object
	 * 
	 * @param pojo
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO update(AddressDO pojo) throws ContracticServiceException {

		if (!StringUtils.hasText(pojo.getId())) {
			return new MessageDO(null, "Contractic",
					"Security data has been corrupted, can't continue",
					MessageDO.ERROR);
		}

		if (!StringUtils.hasText(pojo.getSection1())) {
			return new MessageDO(null, "Contractic",
					"The first section of the address can't be empty",
					MessageDO.ERROR);
		}

		Long id = this.addressIdCache.get(pojo.getId());
		if (id == null) {
			return new MessageDO(null, "Contractic",
					"Security data has been corrupted, can't continue",
					MessageDO.ERROR);
		}

		Address address = new Address();
		try {

			address.setId(id);
			List<Address> addressList = addressDAO.read(address);
			if (addressList.size() != 1) {
				return new MessageDO(null, "Contractic",
						"No such address exists. Data corruption detected",
						MessageDO.ERROR);
			}
			assert addressList.size() == 1;
			address = addressList.get(0);
			address.setSection1(pojo.getSection1());
			address.setSection2(pojo.getSection2());
			address.setSection3(pojo.getSection3());
			address.setSection4(pojo.getSection4());
			address.setSection6(pojo.getSection6());
			address.setSection7(pojo.getSection7());
			address.setSection8(pojo.getSection8());

		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"Failed to update organisation: '%s'", pojo), e);
		}

		try {
			addressDAO.update(address);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"Failed to update organisation: '%s'", pojo), e);
		}

		// final String icon, final String title, final String body, final
		// String type
		return new MessageDO(null, "Contractic", String.format(
				"Organisation '%s' updated successfully", pojo.getSection1()),
				MessageDO.OK);
	}

	private String updateIdCache(Address address) throws NoSuchAlgorithmException{
		String hashId = HashingUtil.get(address.getId());
		addressIdCache.put(hashId, address.getId());
		return hashId;
	}
	
	/**
	 * Create a new organisation
	 * 
	 * @param pojo
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO create(AddressDO pojo)
			throws ContracticServiceException {
		
		// Attempt to format addresses of different types e.g. Postal addresses should be formated to so that specific fields are in specific locations
		// E.g. post code could being section7
		
		// Also check if an address already exists and just return a reference to it to avoid duplicate addresses
		
		final Address address = new Address();

		if (!StringUtils.hasText(pojo.getSection1())) {
			return new MessageDO(null, "Contractic",
					"Section1 can't be empty", MessageDO.ERROR);
		}

		Organisation organisation = organisationService.get(pojo.getOrganisationId());
		AddressType addressType = addressTypeService.get(pojo.getTypeId());
		
		address.setOrganisation(organisation);
		address.setType(addressType);
		address.setSection1(pojo.getSection1());
		address.setSection2(pojo.getSection2());
		address.setSection3(pojo.getSection3());
		address.setSection4(pojo.getSection4());
		address.setSection5(pojo.getSection5());
		address.setSection6(pojo.getSection6());
		address.setSection7(pojo.getSection7());
		address.setSection8(pojo.getSection8());
		
		try {
			addressDAO.create(address);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"Failed to create the address: %s", pojo), e);
		}

		try {
			String hashId = updateIdCache(address);
			pojo.setId(hashId);
		} catch (NoSuchAlgorithmException e) {
			throw new ContracticServiceException(String.format(
					"Failed to update the cache with address: %s", pojo), e);
		}
		
		// final String icon, final String title, final String body, final
		// String type
		return new MessageDO(null, "Contractic", String.format(
				"Address %s created successfully", pojo.getSection1()),
				MessageDO.OK);
	}
	
	public Address get(String uuid) throws ContracticServiceException {
		
		if (uuid == null)
			throw new ContracticServiceException("Invalid address hash supplied (null)");
		
		Long id = this.addressIdCache.get(uuid);
		if(id == null)
			return null;
		
		Address address = new Address();
		address.setId(id);

		List<Address> orgList = null;
		try {
			orgList = addressDAO.read(address);
			if (orgList.size() != 1)
				return null;
			return orgList.get(0);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(
					String.format(
							"An error occured while attempting to read organisation with id %s",
							id));
		}
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.addressIdCache = new Cache(ADDRESS_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(
					String.format("Initializing %s service failed", this
							.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// Delete cache here

	}

	@Override
	public Address get(Long id) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageDO delete(AddressDO dojo) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	// Private utility methods
}
