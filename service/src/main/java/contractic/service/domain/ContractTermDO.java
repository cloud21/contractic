package contractic.service.domain;


public class ContractTermDO {
	private Integer number;
	private ContractDetailDO period;
	private ContractDetailDO notice;
	private ContractDetailDO gotomarket;
	
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}

	public ContractDetailDO getPeriod() {
		return period;
	}
	public void setPeriod(ContractDetailDO period) {
		this.period = period;
	}
	public ContractDetailDO getNotice() {
		return notice;
	}
	public void setNotice(ContractDetailDO notice) {
		this.notice = notice;
	}
	public ContractDetailDO getGotomarket() {
		return gotomarket;
	}
	public void setGotomarket(ContractDetailDO gotomarket) {
		this.gotomarket = gotomarket;
	}

}
