package contractic.service.domain;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.StaxDriver;

public class ContractTermPeriodDO {
	@XStreamAlias("properties")
	public static class Properties{
		public PeriodDO period;
		public Boolean active;
		/**
		 * Convert to ContractValue from XML
		 * @param xml
		 * @return
		 */
		public final static Properties fromXml(String xml){
			XStream xstream = new XStream();
			xstream.processAnnotations(Properties.class);
			return (Properties)xstream.fromXML(xml);
		}
		
		/**
		 * 
		 * @param value
		 * @return
		 */
		public final static String toXml(Properties value){
			XStream xstream = new XStream(new StaxDriver());
			xstream.processAnnotations(Properties.class);
			return xstream.toXML(value);
		}
	}
	
	private Integer number;
	private Properties properties;
	private String start;
	
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public Properties getProperties() {
		return properties;
	}
	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
}
