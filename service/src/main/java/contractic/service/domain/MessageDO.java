package contractic.service.domain;

/**
 * Message to be passed to client
 * @author Michael Sekamanya
 *
 */
public class MessageDO {

	public final static String ERROR = "ERROR";
	public final static String WARNING = "WARNING";
	public final static String INFO = "INFO";
	public final static String OK = "OK";
	
	
	private String icon;
	private String title;
	private String body;
	private String type;
	private Object ref;
	
	public MessageDO(final String body, final String type) {
		this(null, null, body, type);
	}
	
	public MessageDO(final String title, final String body, final String type) {
		this(null, title, body, type);
	}
	
	public MessageDO(final String icon, final String title, final String body, final String type) {
		this.icon = icon;
		this.title = title;
		this.body = body;
		this.type = type;
	}
	
	public MessageDO(){}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getRef() {
		return ref;
	}

	public void setRef(Object ref) {
		this.ref = ref;
	}



	
}
