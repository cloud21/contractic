package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Audit;
import contractic.model.data.store.AuditAction;
import contractic.model.data.store.Contract;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Status;
import contractic.model.data.store.Task;
import contractic.model.data.store.UserRolePermissionAction;
import contractic.service.util.Cache;
import contractic.service.util.DateUtil;
import contractic.service.util.HashingUtil;

public class TaskService 
	implements DomainService<Task, Long, TaskDO, MessageDO>{
	
	/*
	 * This the unique task id and is expected to be of type Long
	 */
	public static final String GET_PARAM_ID = "task.id";
	public static final String GET_PARAM_NUMBER = "task.number";
	public final static String TASK_IDCACHE_NAME = "task.idcache.name";
	public static final String GET_PARAM_SEARCH_QUERY = "search.query";
	public static final String GET_PARAM_EXTERNALID = "external.id";
	public static final String GET_PARAM_MODULEID = "module.id";
	public static final String GET_PARAM_PARENTNUMBER = "task.parent.number";
	public static final String GET_PARAM_ACTIVE = "active";
	public static final String USERID = "user.id";
	
	public static final String REPEAT_YEAR = "Y";
	public static final String REPEAT_MONTH = "M";
	public static final String REPEAT_DAY = "D";
	public static final String REPEAT_WEEK = "W";
	
	private static final Logger logger = Logger.getLogger(TaskService.class);

	public static final String GET_LOG_READ = "enable.read.log";

	public static final String GET_PARAM_MODULENAME = "module.name";

	// This cache helps us hide database IDs from each request
	private Cache<String, Long> taskIdCache;
	
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	
	@Autowired
	private DomainService<Status, Long, StatusDO, MessageDO> statusService;
	
	@Autowired
	private DomainService<Audit, Long, AuditDO, MessageDO> auditService;
	@Autowired
	private DomainService<AuditAction, Long, StaticDO, MessageDO> auditActionService;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	
	@Autowired
	private CrudOperation<Task> taskDAO;

	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private DomainService<Contract, Long, ContractDO, MessageDO> contractService;

	@Autowired
	private CrudOperation<Module> moduleDAO;
	
	@Override
	public Task get(String hash) throws ContracticServiceException {
		final Long id = this.taskIdCache.get(hash);
		return get(id);
	}

	@Override
	public Task get(Long id) throws ContracticServiceException {
		if(id==null)
			throw new ContracticServiceException("Null id supplied");
		
		Task task = new Task();
		task.setId(id);
		
		try {
			List<Task> contractList = taskDAO.read(task);
			return contractList.get(0);
			
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving task with id %d", id), e);
		}
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.taskIdCache = new Cache(TASK_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MessageDO update(TaskDO taskDO) throws ContracticServiceException {
		
		final UserDO userDO = taskDO.getUser();
		if(userDO == null)
			throw new ContracticServiceException("Invalid session user provided");
		
		
		final Module module = moduleService.get(taskDO.getModule().getId());
		
		chekPermission(userDO, UserRolePermissionAction.WRITE, module);
		
		if(!StringUtils.hasText(taskDO.getId())){
			return new MessageDO(null, "Contractic", "Security data has been corrupted, can't continue", MessageDO.ERROR);
		}
		
		final Long taskId = this.taskIdCache.get(taskDO.getId());
		if(taskId == null){
			return new MessageDO(null, "Contractic", "Security data has been corrupted, can't continue", MessageDO.ERROR);
		}
		
		// Save the task object
		try {
			
			// Construct the task object
			Task task = this.get(taskId);
						
			AuditJob<TaskDO> auditJob = new TaskAudit();
			List<AuditDO> auditList = auditJob.run(makeDO(task, userDO), taskDO);
			
			if(taskDO.getParent()!=null){
				String id = taskDO.getParent().getId();
				String value = taskDO.getParent().getValue();
				Task parent = null;
				if(StringUtils.hasText(id))
					parent = this.get(taskDO.getParent().getId());
				else if(StringUtils.hasText(value)){
					Long number = null;
					try{
						number = Long.parseLong(value);
					}catch(NumberFormatException nfe){
						MessageDO message = new MessageDO(null, "Contractic", String.format("Invalid parent number '%s'", value), MessageDO.ERROR);
						throw new ContracticServiceException(message.getBody(), message, nfe);
					}
					Map<String, Object> params = new HashMap<>();
					
					params.put(GET_PARAM_NUMBER, number);
					params.put(CommonService.USER, userDO);
					
					List<TaskDO> parentTaskDOList = get(params);
					assert parentTaskDOList.size() == 1;
					TaskDO parentTaskDO = parentTaskDOList.get(0);
					parent = get(parentTaskDO.getId());
				}
				task.setParent(parent);
			}
			
			task.setTitle(taskDO.getTitle());
			task.setDescription(taskDO.getDescription());
			
			final Person person = personService.get(taskDO.getPerson().getId());
			task.setPerson(person);
			if(taskDO.getStatus()!=null && taskDO.getStatus().getId()!=null){
				final Status status = statusService.get(taskDO.getStatus().getId());
				task.setStatus(status);
			}
			
			try {
				populateRepeat(taskDO, task);
			} catch (Exception e) {
				logger.warn("Failed to populate repeat info", e);
				return new MessageDO(null, "Contractic", e.getMessage(), MessageDO.ERROR);
			}
			
			taskDAO.update(task);
			
			String hashId = HashingUtil.get(task.getId());
			// Update the cache
			this.taskIdCache.put(hashId, task.getId());
			
			auditList.forEach(audit -> {
				try {
					logSessionUpdateAction(userDO, taskDO.getId(), String.format("Field '%s' edited", audit.getField()), audit.getField(), audit.getFrom(), audit.getTo());
				} catch (Exception e) {
					logger.error("Failing to write audit logs", e);
				}
			});
			
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create task: %s", taskDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Task '%s' updated successfully", taskDO.getTitle()), MessageDO.OK);
	}
	
	private void resetTaskRepeat(Task task){
		task.setRepeatDate(null);
		task.setRepeatDay(null);
		task.setRepeatEnd(null);
		task.setRepeatMonth(null);
		task.setRepeatStart(null);
		task.setRepeatWeek(null);
		task.setRepeatYear(null);
	}
	
	private void populateRepeat(Task source, TaskDO target) throws Exception{

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		if(StringUtils.hasText(source.getRepeatDate())){
			String[] dates = source.getRepeatDate().split(",");
			List<String> dateList = new ArrayList<>(dates.length);
			for(String dateString : dates){
				Date date = DateUtil.tryParse(dateString);
				if(date==null)
					throw new Exception(String.format("Invalid date '%s'", dateString));
				dateList.add(dateFormat.format(date));
			}
			if(dateList.size()>0){
				target.setRepeat(new ValueDO(null, String.join(",", dateList)));
			}
			return;
		}
		
		if(StringUtils.hasText(source.getRepeatYear()))
		{
			Date date = DateUtil.tryParse(String.format("%d-%s", new Date().getYear(), source.getRepeatYear()));
			if(date == null){
				throw new Exception ("Invalid date supplied for repeat");
			}
			String dateString = new SimpleDateFormat("dd/MM").format(date);
			target.setRepeat(new ValueDO(REPEAT_YEAR, dateString));
		}
		
		if(StringUtils.hasText(source.getRepeatMonth()))
		{
			target.setRepeat(new ValueDO(REPEAT_MONTH, source.getRepeatMonth()));
		}
		
		if(StringUtils.hasText(source.getRepeatWeek()))
		{
			target.setRepeat(new ValueDO(REPEAT_WEEK, source.getRepeatWeek()));
		}
		
		if(StringUtils.hasText(source.getRepeatDay()))
		{
			target.setRepeat(new ValueDO(REPEAT_DAY, ""));
		}
		
		if(source.getRepeatStart()!=null)
			target.setRepeatStart(Long.toString(source.getRepeatStart().getTime()));
		if(source.getRepeatEnd()!=null)
			target.setRepeatEnd(Long.toString(source.getRepeatEnd().getTime()));
	}

	/*
	 * Repeat format:
	 * id: Y, value: MM-dd
	 * id: M, value: {1-31}, if month has less than 31 days, then the last day of the month if 31 is selected
	 * id: W, value: {0-6}
	 * id: D, value: null
	 * id: null, value: yyyy-MM-dd
	 */
	private void populateRepeat(TaskDO source, Task target) throws Exception{
		ValueDO repeat = source.getRepeat();
		if(repeat==null)
			return;
		
		// Clear previous repeats first;
		resetTaskRepeat(target);
		
		final String id = repeat.getId();
		final String value = repeat.getValue();
		
		if(!StringUtils.hasText(value))
			return;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		if(!StringUtils.hasText(id)){
			String[] dates = value.split(",");
			List<String> dateList = new ArrayList<>(dates.length);
			for(String dateString : dates){
				Date date = DateUtil.tryParse(dateString);
				if(date==null)
					throw new Exception(String.format("Invalid date '%s' supplied", dateString));
				dateList.add(dateFormat.format(date));
			}
			if(dateList.size()>0){
				// Clear previous repeats first;
				
				target.setRepeatDate(String.join(",", dateList));
			}
			return;
		}
		
		if(id.equals(REPEAT_YEAR))
		{
			Date date = DateUtil.tryParse(String.format("%s/%d", value, new Date().getYear()));
			if(date == null){
				throw new Exception ("Invalid date supplied for repeat");
			}
			String dateString = new SimpleDateFormat("MM-dd").format(date);
			

			target.setRepeatYear(dateString);
		}
		if(id.equals(REPEAT_MONTH))
		{
			try{
				Long day = Long.parseLong(value);
				if(day>31)
					throw new NumberFormatException("Supplied day-of-month is greater than 31");

				target.setRepeatMonth(day.toString());
			}catch(NumberFormatException nfe){
				String message = "Invalid day-of-month supplied";
				throw new Exception(message, nfe);
			}
		}
		if(id.equals(REPEAT_WEEK))
		{
			try{
				Long day = Long.parseLong(value);
				if(day>6)
					throw new NumberFormatException("Supplied day-of-week is greater than 6");

				target.setRepeatWeek(day.toString());
			}catch(NumberFormatException nfe){
				String message = "Invalid day-of-week supplied";
				throw new Exception(message, nfe);
			}
		}
		if(id.equals(REPEAT_DAY)){
			target.setRepeatDay("*");
		}
		
		if(source.getRepeatStart() != null)
			target.setRepeatStart(DateUtil.tryParse(source.getRepeatStart()));
		
		if(source.getRepeatEnd()!=null)
			target.setRepeatEnd(DateUtil.tryParse(source.getRepeatEnd()));
	}

	@Override
	public MessageDO create(TaskDO taskDO) throws ContracticServiceException {
		
		final UserDO userDO = taskDO.getUser();
		if(userDO == null)
			throw new ContracticServiceException("Invalid session user provided");
		
		final Module module = moduleService.get(taskDO.getModule().getId());
		
		chekPermission(userDO, UserRolePermissionAction.WRITE, module);	
		
		// Construct the task object
		final Task task = new Task();
		task.setTitle(taskDO.getTitle());
		
		if(!StringUtils.hasText(taskDO.getOrganisationId()))
			taskDO.setOrganisationId(userDO.getOrganisationId());
		
		Organisation organisation = organisationService.get(taskDO.getOrganisationId());
		task.setOrganisation(organisation);
		
		final Person person = personService.get(taskDO.getPerson().getId());
		task.setPerson(person);
		if(taskDO.getStatus()!=null && StringUtils.hasText(taskDO.getStatus().getId())){
			final Status status = statusService.get(taskDO.getStatus().getId());
			task.setStatus(status);
		}

		//Populate the external id
		if(taskDO.getExternal() != null){
			String externalIdString = (String)taskDO.getExternal().get("id");
			final Long externalId = commonService.getExternalId(externalIdString, module);
			task.setExternalId(externalId);
		}
		task.setModule(module);
		
		
		if(taskDO.getParent()!=null){
			String id = taskDO.getParent().getId();
			String value = taskDO.getParent().getValue();
			Task parent = null;
			if(StringUtils.hasText(id))
				parent = this.get(taskDO.getParent().getId());
			else if(StringUtils.hasText(value)){
				Long number = null;
				try{
					number = Long.parseLong(value);
				}catch(NumberFormatException nfe){
					MessageDO message = new MessageDO(null, "Contractic", String.format("Invalid parent number '%s'", value), MessageDO.ERROR);
					throw new ContracticServiceException(message.getBody(), message, nfe);
				}
				Map<String, Object> params = new HashMap<>();
				
				params.put(GET_PARAM_NUMBER, number);
				params.put(CommonService.USER, userDO);
				
				List<TaskDO> parentTaskDOList = get(params);
				assert parentTaskDOList.size() == 1;
				TaskDO parentTaskDO = parentTaskDOList.get(0);
				parent = get(parentTaskDO.getId());
			}
			task.setParent(parent);
		}
		
		task.setStartDate(DateUtil.tryParse(taskDO.getStartDate()));
		task.setEndDate(DateUtil.tryParse(taskDO.getEndDate()));
		task.setDescription(taskDO.getDescription());
		task.setCreated(new Date());
		
		if(StringUtils.hasText(taskDO.getRepeatStart()))
			task.setRepeatStart(DateUtil.tryParse(taskDO.getRepeatStart()));
		if(StringUtils.hasText(taskDO.getRepeatEnd()))
			task.setRepeatEnd(DateUtil.tryParse(taskDO.getRepeatEnd()));
		
		try {
			populateRepeat(taskDO, task);
		} catch (Exception e) {
			logger.warn("Failed to populate repeat info", e);
			return new MessageDO(null, "Contractic", e.getMessage(), MessageDO.ERROR);
		}
		
		ValueDO returnValue = null;
		// Save the person object
		try {
			taskDAO.create(task);
			
			String hashId = HashingUtil.get(task.getId());
			// Update the cache
			this.taskIdCache.put(hashId, task.getId());
			
			returnValue = new ValueDO(hashId, null);
		
			logSessionCreateAction(userDO, hashId, String.format("New task '%s'", task.getTitle()));
			
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create task: %s", taskDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		MessageDO message = new MessageDO(null, "Contractic", String.format("Task '%s' created successfully", taskDO.getTitle()), MessageDO.OK);
		message.setRef(returnValue);
		return message;
	}

	private void chekPermission(final UserDO userDO, final UserRolePermissionAction permissionAction, final Module module)
			throws ContracticServiceException {
		switch(module.getName()){
		case Module.CONTRACT:
		case Module.CONTRACTDETAIL:
			commonService.checkPermission(userDO, permissionAction, Module.CONTRACT);
			break;
		case Module.PERSON:
			commonService.checkPermission(userDO, permissionAction, Module.PERSON);
			break;
		case Module.SUPPLIER:
			commonService.checkPermission(userDO, permissionAction, Module.SUPPLIER);
			break;
		default:
			throw new ContracticServiceException("Invalid module");
		}
	}

	@Override
	public List<TaskDO> get(Map<String, Object> params)
			throws ContracticServiceException {
		final String hash = (String)params.get(GET_PARAM_HASHID);
		final String externalIdString = (String)params.get(GET_PARAM_EXTERNALID);
		final String moduleId = (String)params.get(GET_PARAM_MODULEID);
		final String moduleName = (String)params.get(GET_PARAM_MODULENAME);

		final Long id = (Long)params.get(GET_PARAM_ID);
		final Long number = (Long)params.get(GET_PARAM_NUMBER);
		final String parentNumberString = (String)params.get(GET_PARAM_PARENTNUMBER);
		
		final String queryString = (String)params.get(GET_PARAM_SEARCH_QUERY);
		
		Boolean logEnabled = (Boolean)params.get(GET_LOG_READ);
		logEnabled = logEnabled != null && logEnabled == Boolean.TRUE; 

		final UserDO userDO = (UserDO)params.get(CommonService.USER);
		if(userDO == null)
			throw new ContracticServiceException("Invalid session user provided");
		
		final String organisationId = userDO.getOrganisationId();		
		if(!StringUtils.hasText(organisationId)){
			throw new ContracticServiceException("OrganisationId not supplied");
			}
		
		List<Task> taskList = null;
			try {
				
				Task task = new Task();
				
				Organisation organisation = organisationService.get(organisationId);
				if(organisation == null)
					throw new ContracticServiceException("Invalid OrganisationId supplied");
				
				task.setOrganisation(organisation);
				
				if(queryString != null){
					task.setTitle(queryString);
				}
				
				task.setId(id);
				if(StringUtils.hasText(hash)){
					task.setId(this.taskIdCache.get(hash));
				}
				
				if(number!=null)
					task.setNumber(number);
				
				if(StringUtils.hasText(parentNumberString)){
					Long parentNumber = Long.parseLong(parentNumberString);
					if(parentNumber!=null){
						Task parentTask = new Task();
						parentTask.setNumber(parentNumber);
						parentTask.setOrganisation(organisation);
						
						List<Task> parentTaskList = taskDAO.read(parentTask);
						assert parentTaskList.size() == 1 : String.format("Got %s tasks instead of 1", parentTaskList.size());
						parentTask = parentTaskList.get(0);
						
						task.setParent(parentTask);
					}
				}
				
				// Populate the ModuleId
				if(StringUtils.hasText(moduleId)){
					final Module module = moduleService.get(moduleId);
					task.setModule(module);
				}else if(StringUtils.hasText(moduleName)){
					Module module = ModuleService.getModule(moduleDAO, moduleName);
					task.setModule(module);
				}
				
				if(StringUtils.hasText(externalIdString)){
					// We need to have received the moduleId
					if(task.getModule() == null)
						throw new ContracticServiceException("ExternalId provided but module is missing");
					Long externalId = commonService.getExternalId(externalIdString, task.getModule());
					task.setExternalId(externalId);
				}
								
				taskList = taskDAO.read(task);
			} catch (ContracticModelException e) {
				throw new ContracticServiceException("An error occured while attempting to read tasks", e);
			}

			// List of all exceptions that occur in the predicate
			List<ContracticServiceException> cse = new ArrayList<>();
		
			final List<TaskDO> pojoList = taskList.stream()
				//.parallel()
				.map(task -> {
					TaskDO taskDO = null;
					try{
						taskDO = makeDO(task, userDO);						
						if(taskDO != null){
							taskIdCache.put(taskDO.getId(), task.getId());
						}
					}catch(final ContracticServiceException | NoSuchAlgorithmException ex){
						cse.add(new ContracticServiceException(String.format("An error occured when hashing the task %s", task), ex));
					}
					
					return taskDO;
				}).collect(Collectors.toList());
			
			if(cse.size()!=0)
				throw cse.get(0);
			
			if(logEnabled){
				pojoList.forEach(task -> {
					try {
						logSessionReadAction(userDO, task.getId(), String.format("Task %s", task.getTitle()));
					} catch (Exception e) {
						logger.error("Failing to write audit logs", e);
					}
				});
			}
			
			return pojoList.stream().filter(task -> task!=null).collect(Collectors.toList());
	}

	
	private TaskDO makeDO(final Task task, final UserDO userDO) throws ContracticServiceException, NoSuchAlgorithmException {
		
		final Map<String, Object> params = new HashMap<>();
		
		String hashId = HashingUtil.get(task.getId());
		TaskDO taskDO = new TaskDO();
		
		taskDO.setId(hashId);
		taskDO.setTitle(task.getTitle());
		taskDO.setStartDate(Long.toString(task.getStartDate().getTime()));
		if(task.getEndDate()!=null)
			taskDO.setEndDate(Long.toString(task.getEndDate().getTime()));
		taskDO.setDescription(task.getDescription());
		taskDO.setNumber(task.getNumber().toString());
		
		// Get and populate the external reference (object to which this task belongs)
		params.clear();
		if(task.getModule().getName().equalsIgnoreCase(Module.CONTRACT)){
			params.put(CommonService.USER, userDO);
			params.put(ContractService.GET_LOG_READ, Boolean.FALSE);
			params.put(ContractService.GET_PARAM_ID, task.getExternalId());
			final List<ContractDO> contractList = contractService.get(params);
			if(contractList.size() == 0)
				return null;
			assert contractList.size() == 1 : String.format("Found %s contracts for task %s", task.getExternalId(), task.getId());
			ContractDO contractDO = contractList.get(0);
			final Map<String, Object> externalValueDO = new ObjectMapper().convertValue(contractDO,Map.class);
			taskDO.setExternal(externalValueDO);
		}
		
		// Populate the parent task details
		params.clear();
		if(task.getParent() != null){
			taskDO.setParentNumber(Long.toString(task.getParent().getNumber()));
			params.clear();
			params.put(CommonService.USER, userDO);
			params.put(GET_PARAM_ID, task.getParent().getId());
			List<TaskDO> parentList = get(params);
			assert(parentList.size() == 1);
			TaskDO parentDO = parentList.get(0);
			taskDO.setParent(new ValueDO(parentDO.getId(), parentDO.getTitle()));
		}
		
		final Set<Task> childrenTasks = task.getChildren();
		taskDO.setChildren(childrenTasks!=null && childrenTasks.stream().findFirst().isPresent());
		
		// Get and populate the owner
		params.clear();
		params.put(DomainService.GET_PARAM_ID, task.getPerson().getId());
		params.put(CommonService.USER, userDO);
		final List<PersonDO> personList = personService.get(params);
		assert(personList.size() == 1);
		PersonDO personDO = personList.get(0);
		taskDO.setPerson(new ValueDO(personDO.getId(), String.join(" ", new String[]{
				(personDO.getFirstName() == null ? "" : personDO.getFirstName()), 
				(personDO.getMiddleName() == null ? "" : personDO.getMiddleName()), 
				(personDO.getLastName() == null ? "" : personDO.getLastName())
				})));
		
		// Get and populate the status
		if(task.getStatus()!=null){
			params.clear();
			params.put(StatusService.GET_PARAM_ID, task.getStatus().getId());
			params.put(StatusService.GET_PARAM_ORGANISATIONID, userDO.getOrganisationId());
			final List<StatusDO> statusList = statusService.get(params);
			assert statusList.size() == 1;
			StatusDO statusDO = statusList.get(0);
			taskDO.setStatus(new ValueDO(statusDO.getId(), statusDO.getName()));
		}
		// Populate repeat info
		try {
			populateRepeat(task, taskDO);
		} catch (Exception e) {
			logger.warn("An error occurred when populating a taskDO object with repeat information", e);
		}
		
		// Populate the module
		ModuleDO moduleDO = CommonService.getModule(moduleService, task.getModule().getName());
		taskDO.setModule(new ValueDO(moduleDO.getId(), moduleDO.getName()));
		
		return taskDO;
	}

	public MessageDO delete(final TaskDO taskDO) throws ContracticServiceException{
		Long taskId = this.taskIdCache.get(taskDO.getId());
		
		final UserDO userDO = taskDO.getUser();
		if(userDO == null)
			throw new ContracticServiceException("Invalid session user provided");

		final Module module = moduleService.get(taskDO.getModule().getId());
		chekPermission(userDO, UserRolePermissionAction.DELETE, module);
		
		if(taskId == null){
			//final String icon, final String title, final String body, final String type
			return new MessageDO(null, "Internal system error", "Invalid task (may have been deleted)", MessageDO.WARNING);
		}
		
		Task task = new Task();
		try {
			
			task.setId(taskId);
			List<Task> contractList = taskDAO.read(task);
			assert(contractList.size() == 1);
			task = contractList.get(0);
			
			// Delete from data source
			taskDAO.delete(task);
			
			// if successfull, remove from Id cache
			this.taskIdCache.remove(taskDO.getId());
			
			return new MessageDO(null, "", String.format("Task '%s' deleted successfully!", task.getTitle()), MessageDO.OK);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occurred when performing delete operation on task object %s", task), e);
		}
	}
	
	/*
	 * Logging within the task module
	 */
	private void logSessionDeleteAction(UserDO userDO, String taskId, String logMessage)
			throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService, Module.TASK);
		CommonService.logCreateAction(auditService, userDO, taskId, 
				moduleDO, logMessage, null, null, null);
	}

	private void logSessionCreateAction(UserDO userDO, String taskId, String logMessage)
			throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService, Module.TASK);
		CommonService.logCreateAction(auditService, userDO, taskId, 
				moduleDO, logMessage, null, null, null);
	}
	
	private void logSessionReadAction(UserDO userDO, String contractId, String logMessage)
			throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService, Module.TASK);
		CommonService.logReadAction(auditService, userDO, contractId, 
				moduleDO, logMessage, null, null, null);
	}
	
	private void logSessionUpdateAction(UserDO userDO, String contractId, String logMessage, String field, String from, String to)
			throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService, Module.TASK);
		CommonService.logUpdateAction(auditService, userDO, contractId, 
				moduleDO, logMessage, field, from, to);
	}
	
}
