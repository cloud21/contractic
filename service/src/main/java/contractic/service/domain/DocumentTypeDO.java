package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

// Represents any static data mainly used for drop down lists
@JsonIgnoreProperties({"organisationId", "user"})
public class DocumentTypeDO {
	private String id;
	private String name;
	private String description;
	private String organisationId;
	private Boolean active;
	private UserDO user;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public UserDO getUser() {
		return user;
	}
	public void setUser(UserDO user) {
		this.user = user;
	}

}
