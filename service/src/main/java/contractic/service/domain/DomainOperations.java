package contractic.service.domain;

import java.util.List;
import java.util.Map;

/**
 * Operations by controllers to the domain serviecs
 * @author Michael Sekamanya
 *
 * @param <T> Result type
 * @param <D> Domain type
 */
public interface DomainOperations
	<T, D> 
{
	// All services are intended to implement these methods
	T update(D dojo) throws ContracticServiceException;
	T create(D dojo) throws ContracticServiceException;
	List<D> get(Map<String, Object> params) throws ContracticServiceException;
}
