package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Audit;
import contractic.model.data.store.AuditAction;
import contractic.model.data.store.Module;
import contractic.model.data.store.User;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public final class SessionService implements DomainService<UserDO, Long, SessionDO, MessageDO>{
	public final static String SESSION_IDCACHE_NAME = "session.idcache.name";
	public final static String IDCACHE_SESSION_NAME = "idcache.session.name";

	public static final Object SESSION_TOKEN = "session.token";
	
	// Simple authentication method
	// =============================================================================
	// This caches authentication tokens
	// When a user logs out, we remove the authentication token from here
	// When there is no authentication token here, the user has to be log in afresh
	
	/*****
	 * THIS HAS TO BE A 1-1 BIDI CACHE TO ENSURE THAT ONLY ONE SESSION EXISTS FOR A GIVEN USER
	 */
	
	private Cache<String, Long> sessionIdCache;
	private Cache<Long, String> idSessionCache;
		
	@Autowired
	private DomainService<User, Long, UserDO, MessageDO> userService;
	@Autowired
	private DomainService<Audit, Long, AuditDO, MessageDO> auditService;
	@Autowired
	private DomainService<AuditAction, Long, StaticDO, MessageDO> auditActionService;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	
	private static ResourceBundle errorResourceBundle = DomainService.getErrorBundle();
	
	private static Logger logger = Logger.getLogger(SessionService.class);
	
	@Autowired
	private CommonService commonService;
	
	/**
	 * Validate an authentication token
	 * @param token The token to authenticate
	 * @return The supplied token or null if the token is invalid 
	 */
	public UserDO get(String token){
		Long userDAOId = sessionIdCache.get(token);
		final Map<String, Object> params = new HashMap<>();
		params.put(UserService.GET_PARAM_ID, userDAOId);
		List<UserDO> userList;
		try {
			userList = userService.get(params);
			assert userList.size() == 1;
			return userList.get(0);
		} catch (ContracticServiceException e) {
			throw new RuntimeException("An exception occurred while retrieving user from system token", e);
		}

	}
	
	public MessageDO delete(final SessionDO sessionDO) throws ContracticServiceException{
		// Validate the user
		String username = sessionDO.getUsername();
		String token = sessionDO.getToken();
		if(!StringUtils.hasText(username))
			throw new ContracticServiceException("Invalid username supplied");
		
		if(!StringUtils.hasText(token))
			throw new ContracticServiceException("Invalid token supplied");
		
		Map<String, Object> params = new HashMap<>();
		params.put(UserService.GET_PARAM_USERNAME, username);
		
		List<UserDO> users = userService.get(params);
		if(users == null || users.size() != 1)
			throw new ContracticServiceException("Invalid username supplied");
		
		Long userId = sessionIdCache.get(token);
		User user = userService.get(userId);
		assert(user != null);
		
		if(!sessionIdCache.remove(token))
			throw new RuntimeException(String.format("Failed to remove session token for user '%s'", username));
		if(!idSessionCache.remove(user.getId()))
			throw new RuntimeException(String.format("Failed to remove user id for user '%s'", username));
		
		// Log activity here
		String logMessage = String.format("Session deleted for user '%s'", sessionDO.getUser().getLogin());
		logSessionDeleteAction(sessionDO.getUser(), logMessage);
		
		return new MessageDO(null, "Contratic", "Successfully deleted token", MessageDO.OK);
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.sessionIdCache = new Cache(SESSION_IDCACHE_NAME, String.class, Long.class);
			this.idSessionCache = new Cache(IDCACHE_SESSION_NAME, Long.class, String.class);
		} catch (Exception e) {
			throw new RuntimeException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub
		sessionIdCache.clear();
	}

	@Override
	public UserDO get(Long id) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageDO update(SessionDO sessionDO) throws ContracticServiceException {
		
		/*This only allows us to update the password*/
		
		UserDO userDO = sessionDO.getUser();
		User user = userService.get(userDO.getId());
		
		boolean valid = BCrypt.checkpw(sessionDO.getPassword(), user.getPassword());
		MessageDO message = null;
		if(valid){
			userDO.setPassword(sessionDO.getPassword2());
			userService.update(userDO);
			message = new MessageDO(null, "Contractic", "Session update successful", MessageDO.OK);
		}else{
			message = new MessageDO(null, "Contractic", "Login unsuccessful.", MessageDO.ERROR);
		}
		
		return message;
	}

	private SessionDO makeDO(User user, UserDO userDO) throws NoSuchAlgorithmException{
		SessionDO session = new SessionDO();
		long salt = System.currentTimeMillis() % user.getId();
		String token = HashingUtil.get(salt);
		session.setToken(token);
		session.setOrganisationId(userDO.getOrganisationId());
		session.setUser(userDO);
		session.setUsername(userDO.getLogin());
		return session;
	}
	
	@Override
	public MessageDO create(SessionDO sessionDO) throws ContracticServiceException {
		
		UserDO userDO = sessionDO.getUser();
		User user = userService.get(userDO.getId());
		Boolean active = user.getActive();
		if(active==null || !active){
			String code = "1000";
			String format = errorResourceBundle.getString(code);
			String message = String.format(format, user.getLoginId());
			throw new ContracticServiceException(message, new MessageDO(null, "Contratic", message, MessageDO.ERROR));
		}
		
		final boolean valid = BCrypt.checkpw(sessionDO.getPassword(), user.getPassword());
		if(valid){
			
			String sessionKey = null;
			try{
				sessionKey = idSessionCache.get(user.getId());
			}catch(NullPointerException npe){
				/*Ignore as there is no existing session*/
				logger.warn("Did not find session - ignoring exception", npe);
			}
			
			if(StringUtils.hasText(sessionKey)){
				Boolean killExisting = sessionDO.getKillExisting();
				if(killExisting!=null && killExisting){
					sessionDO.setToken(sessionKey);
					delete(sessionDO);
				}else{
					String code = "1002";
					String format = errorResourceBundle.getString(code);
					String message = String.format(format, user.getLoginId());
					throw new ContracticServiceException(message, new MessageDO(null, "Contratic", message, MessageDO.ERROR));
				}
			}
			
			try {
				SessionDO session = makeDO(user, userDO);
				
				this.sessionIdCache.put(session.getToken(), user.getId());
				this.idSessionCache.put(user.getId(), session.getToken());

				MessageDO message = new MessageDO(null, "Contractic", "Login successful", MessageDO.OK);
				message.setRef(session);
				
				String logMessage = String.format("Session created for user '%s'", userDO.getLogin());
				logSessionCreateAction(userDO, logMessage);
				
				return message;
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException("Failed to generate a security token");
			}
		}else{
			String code = "1001";
			String format = errorResourceBundle.getString(code);
			String message = String.format(format, user.getLoginId());
			throw new ContracticServiceException(message, new MessageDO(null, "Contratic", message, MessageDO.ERROR));
		}
	}
	
	private void logSessionDeleteAction(UserDO userDO, String logMessage)
			throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService, Module.ORGANISATION);
		CommonService.logDeleteAction(auditService, userDO, userDO.getOrganisationId(), 
				moduleDO, logMessage, null, null, null);
	}

	private void logSessionCreateAction(UserDO userDO, String logMessage)
			throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService, Module.ORGANISATION);
		CommonService.logCreateAction(auditService, userDO, userDO.getOrganisationId(), 
				moduleDO, logMessage, null, null, null);
	}

	@Override
	public List<SessionDO> get(Map<String, Object> params)
			throws ContracticServiceException {
		final String token = (String)params.get(SESSION_TOKEN);
		// TODO Auto-generated method stub
		return null;
	}


}
