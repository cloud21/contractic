package contractic.service.domain;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.xstream.XStream;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Audit;
import contractic.model.data.store.AuditAction;
import contractic.model.data.store.Contract;
import contractic.model.data.store.ContractDetail;
import contractic.model.data.store.ContractReferenceFormat;
import contractic.model.data.store.ContractType;
import contractic.model.data.store.Group;
import contractic.model.data.store.GroupObject;
import contractic.model.data.store.GroupSet;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Status;
import contractic.model.data.store.Supplier;
import contractic.model.data.store.Task;
import contractic.model.data.store.User;
import contractic.model.data.store.UserRolePermission;
import contractic.model.data.store.UserRolePermissionAction;
import contractic.service.util.Cache;
import contractic.service.util.ConcurrencyUtil;
import contractic.service.util.DateUtil;
import contractic.service.util.HashingUtil;
import contractic.service.util.SearchClient.SearchEntity;

public class ContractService implements
		DomainService<Contract, Long, ContractDO, MessageDO> {

	public static final String CONTRACT_IDCACHE_NAME = "contract.idcache.name";
	public static final String GET_PARAM_SEARCH_QUERY = "search.query";

	/*
	 * This field allows to disable logging of contract read. Logging of reads
	 * is only ever needed when the read requests are coming from the controller
	 */
	public static final String GET_LOG_READ = "log.read";

	private static final Logger logger = Logger
			.getLogger(ContractService.class);

	// This cache helps us hide database IDs from each request
	private Cache<String, Long> contractIdCache;

	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	@Autowired
	private DomainService<User, Long, UserDO, MessageDO> userService;
	@Autowired
	private DomainService<Supplier, Long, SupplierDO, MessageDO> supplierService;
	@Autowired
	private DomainService<ContractType, Long, ContractTypeDO, MessageDO> contractTypeService;
	@Autowired
	private DomainService<Status, Long, StatusDO, MessageDO> statusService;
	@Autowired
	private CrudOperation<Contract> contractDAO;
	@Autowired
	private CrudOperation<Module> moduleDAO;
	@Autowired
	private CrudOperation<GroupObject> groupObjectDAO;
	@Autowired
	private CrudOperation<UserRolePermission> userRolePermissionDAO;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<ContractDetail, Long, ContractDetailDO, MessageDO> contractDetailService;
	@Autowired
	private DomainService<Audit, Long, AuditDO, MessageDO> auditService;
	@Autowired
	private DomainService<AuditAction, Long, StaticDO, MessageDO> auditActionService;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	@Autowired
	private DomainService<Group, Long, GroupDO, MessageDO> groupService;
	@Autowired
	private DomainService<GroupSet, Long, GroupSetDO, MessageDO> groupSetService;
	@Autowired
	private DomainService<SearchEntity, Long, SearchEntityDO, MessageDO> searchService;
	@Autowired
	private DomainService<ContractReferenceFormat, Long, ContractReferenceFormatDO, MessageDO> contractReferenceFormatService;
	@Autowired
	private DomainService<Task, Long, TaskDO, MessageDO> taskService;
	@Autowired
	private CommonService commonService;

	@Override
	public Contract get(String hash) throws ContracticServiceException {
		final Long id = this.contractIdCache.get(hash);
		return get(id);
	}

	@Override
	public Contract get(Long id) throws ContracticServiceException {
		if (id == null)
			throw new ContracticServiceException("Null id supplied");

		Contract contract = new Contract();
		contract.setId(id);

		try {
			List<Contract> contractList = contractDAO.read(contract);
			contract = contractList.get(0);

			String hashId = HashingUtil.get(contract.getId());
			// Update the cache
			this.contractIdCache.put(hashId, contract.getId());

			return contract;

		} catch (ContracticModelException | NoSuchAlgorithmException e) {
			throw new ContracticServiceException(
					String.format(
							"An error occured while retrieving contract with id %d",
							id));
		}
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.contractIdCache = new Cache(CONTRACT_IDCACHE_NAME,
					String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(
					String.format("Initializing %s service failed", this
							.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	/*
	 * private void updateContractGroup(Contract contract, ContractDO
	 * contractDO) throws ContracticServiceException, ContracticModelException{
	 * // ValueDO groupDO = contractDO.getDepartment(); if(groupDO==null)
	 * return; Long externalId = contract.getId(); String groupId =
	 * groupDO.getId(); Group group = groupService.get(groupId); Module module =
	 * ModuleService.getModule(moduleDAO, Module.CONTRACT);
	 * 
	 * GroupSetDO departmentGroupSetDO =
	 * GroupSetService.getDepartmentGroupSet(groupSetService,
	 * contractDO.getOrganisationId()); GroupSet departmentGroupSet =
	 * groupSetService.get(departmentGroupSetDO.getId());
	 * 
	 * GroupObject groupObject = new GroupObject(); groupObject.setGroup(group);
	 * groupObject.setModule(module); groupObject.setExternalId(externalId);
	 * groupObject.setGroupSet(departmentGroupSet);
	 * 
	 * List<GroupObject> groupObjectList = groupObjectDAO.read(groupObject);
	 * if(groupObjectList.size() == 0) groupObjectDAO.create(groupObject); else
	 * if(groupObjectList.size() == 1) groupObjectDAO.update(groupObject); else
	 * throw new
	 * ContracticServiceException("Found more than one group object. Update aborted"
	 * );
	 * 
	 * }
	 */

	private void saveDepartment(final ContractDO contractDO, Contract contract)
			throws ContracticServiceException, ContracticModelException {
		GroupSetDO departmentGroupSetDO = GroupSetService.getGroupSet(
				groupSetService, contractDO.getOrganisationId(),
				GroupSet.DEPARTMENT);
		GroupSet departmentGroupSet = groupSetService.get(departmentGroupSetDO
				.getId());
		Module module = ModuleService.getModule(moduleDAO, Module.CONTRACT);
		Long externalId = contract.getId();

		ValueDO groupDO = contractDO.getDepartment();
		if (groupDO == null)
			return;
		Group group = groupService.get(groupDO.getId());

		GroupService.saveObjectGroup(groupObjectDAO, externalId,
				new Group[] { group }, departmentGroupSet, module);
	}

	private void saveProcedure(final ContractDO contractDO, Contract contract)
			throws ContracticServiceException, ContracticModelException {
		GroupSetDO departmentGroupSetDO = GroupSetService.getGroupSet(
				groupSetService, contractDO.getOrganisationId(),
				GroupSet.PROCEDURE);
		GroupSet procedureGroupSet = groupSetService.get(departmentGroupSetDO
				.getId());
		Module module = ModuleService.getModule(moduleDAO, Module.CONTRACT);
		Long externalId = contract.getId();

		List<ContracticServiceException> cse = new ArrayList<>();
		ValueDO groupDO = contractDO.getProcedure();
		if (groupDO == null)
			return;
		Group group = groupService.get(groupDO.getId());

		GroupService.saveObjectGroup(groupObjectDAO, externalId,
				new Group[] { group }, procedureGroupSet, module);
	}

	@Override
	public MessageDO update(final ContractDO contractDO)
			throws ContracticServiceException {

		UserDO userDO = contractDO.getUser();

		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE,
				Module.CONTRACT);

		if (userDO == null)
			throw new ContracticServiceException("No user session supplied");

		if (!StringUtils.hasText(contractDO.getId())) {
			return new MessageDO(null, "Contractic",
					"Security data has been corrupted, can't continue",
					MessageDO.ERROR);
		}

		final Long contractId = this.contractIdCache.get(contractDO.getId());
		if (contractId == null) {
			return new MessageDO(null, "Contractic",
					"Security data has been corrupted, can't continue",
					MessageDO.ERROR);
		}

		// Save the contract object
		try {

			// Construct the contract object
			Contract contract = this.get(contractId);

			// We need to be able to turn this off by enabling and disabling
			// logging
			final Map<String, Object> params = new HashMap<>();
			params.put(GET_LOG_READ, Boolean.FALSE);
			params.put(GET_PARAM_ID, contractId);
			params.put(CommonService.USER, userDO);
			List<ContractDO> thisContract = get(params);
			assert thisContract.size() == 1;

			AuditJob<ContractDO> auditJob = new ContractAudit();
			List<AuditDO> auditList = auditJob.run(thisContract.get(0),
					contractDO);

			final Person person = personService.get(contractDO.getOwner()
					.getId());
			final Supplier supplier = supplierService.get(contractDO
					.getSupplier().getId());
			final ContractType type = contractTypeService.get(contractDO
					.getType().getId());
			final Status status = statusService.get(contractDO.getStatus()
					.getId());

			setReference(contractDO, type);
			
			// Set reference
			contract.setReference(contractDO.getReference());
			contract.setTitle(contractDO.getTitle());
			contract.setOwner(person);
			contract.setSupplier(supplier);
			contract.setStatus(status);
			contract.setStartDate(DateUtil.tryParse(contractDO.getStartDate()));
			contract.setType(type);
			contract.setDescription(contractDO.getDescription());
			contract.setValue(contractDO.getInitialValue());
			contract.setValueUOM(contractDO.getValueUOM());
			// populateInitialTerm(contractDO, contract);

			if (contractDO.getTerm() != null && contractDO.getTerm().length > 0) {
				populateContractTermDates(contractDO, contract, userDO);
			}

			contractDAO.update(contract);

			saveDepartment(contractDO, contract);
			saveProcedure(contractDO, contract);
			saveContractTermPeriods(contractDO, userDO);
			saveTermPeriodsToCalendar(contractDO, userDO);

			String hashId = HashingUtil.get(contract.getId());
			// Update the cache
			this.contractIdCache.put(hashId, contract.getId());

			// logSessionUpdateAction(UserDO userDO, String contractId, String
			// field, String from, String to)
			auditList.forEach(audit -> {
				try {
					logSessionUpdateAction(userDO, contractDO.getId(), String
							.format("Field '%s' edited", audit.getField()),
							audit.getField(), audit.getFrom(), audit.getTo());
				} catch (Exception e) {
					logger.error("Failing to write audit logs", e);
				}
			});
			
			saveSearchEntity(contract);
			
		} catch (Exception e) {
			MessageDO messageDO = new MessageDO();
			messageDO.setBody(String.format(
					"Failed to update contract: %s", contractDO.getTitle()));
			messageDO.setType(MessageDO.ERROR);
			throw new ContracticServiceException(messageDO.getBody(), messageDO, e);
		}

		// final String icon, final String title, final String body, final
		// String type
		MessageDO message = new MessageDO(null, "Contractic", String.format(
				"Contract '%s' updated successfully", contractDO.getTitle()),
				MessageDO.OK);
		
		message.setRef(contractDO);
		
		return message;
	}

	private void setReference(ContractDO contractDO, final ContractType type)
			throws ContracticServiceException {
		String reference = contractDO.getReference();
		if (StringUtils.hasText(reference))
			return;

		// List<ContractReferenceFormatDO> referenceFormatList =
		// ContractReferenceFormatService.getReferenceFormat()
		Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, contractDO.getUser());
		params.put(ContractReferenceFormatService.GET_PARAM_ORGANISATIONID,
				contractDO.getOrganisationId());
		List<ContractReferenceFormatDO> formatList = contractReferenceFormatService
				.get(params);

		final List<ContracticServiceException> csel = new ArrayList<>();

		// If the contract type is supplied, then find reference for that type
		if (type != null) {
			formatList
					.removeIf(f -> {
						if (f.getContractType() == null)
							return false;

						ContractType formatContractType = null;
						try {
							formatContractType = contractTypeService.get(f
									.getContractType().getId());
						} catch (Exception e) {
							csel.add(new ContracticServiceException(
									"Error when determining contract type", e));
							return false;
						}
						return !Objects.equals(formatContractType.getId(),
								type.getId());
					});
		} else {
			formatList.removeIf(f -> f.getContractType() == null
					|| f.getContractType().getId() == null);
		}

		if (csel.size() != 0)
			throw csel.get(0);

		// if the list is still > 0, remove all non defaults
		if (formatList.size() > 1)
			formatList.removeIf(f -> f.getDifault() == null || !f.getDifault());

		String format = null;
		if (formatList.size() == 1) {
			ContractReferenceFormatDO formatDO = formatList.get(0);
			format = formatDO.getFormat();
		}

		if (format == null)
			format = "##########";

		User user = userService.get(contractDO.getUser().getId());

		contractDO.setReference(generateReference(format, user.getId()));

	}

	private static long getTimeSinceMidnight() {
		Calendar c = Calendar.getInstance();
		long now = c.getTimeInMillis();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		long passed = now - c.getTimeInMillis();
		return passed / 1000;
	}

	private static BigInteger generateRandomInt(int length, SecureRandom random) {
		BigInteger lowerBound = BigInteger.valueOf(10).pow(length);
		BigInteger upperBound = BigInteger.valueOf(10).pow(length + 1)
				.subtract(BigInteger.valueOf(1));
		BigInteger value;
		do {
			value = new BigInteger(lowerBound.bitLength(), random);
		} while (value.compareTo(lowerBound) >= 0
				&& value.compareTo(upperBound) <= 0);
		return value;
	}

	/* Public static for testing */
	public static String generateReference(String format, Long userId) {
		final Matcher matcher = ContractReferenceFormatService.ContractReferencePattern
				.matcher(format);
		int idx = 0;
		List<String> keyList = new ArrayList<>();
		;

		while (matcher.find()) {

			String matched = matcher.group(idx);
			int length = matched.length();
			if (matched.startsWith("#")) {
				int seconds = (int) getTimeSinceMidnight();
				long seed = HashingUtil.combine(seconds, userId.intValue());
				SecureRandom random = new SecureRandom();
				random.setSeed(seed);
				BigInteger value = generateRandomInt(length, random);
				keyList.add(value.toString());
			}
			++idx;
		}

		for (idx = 0; idx < keyList.size(); ++idx) {
			format = format.replaceFirst(
					ContractReferenceFormatService.ContractReferencePattern
							.pattern(), keyList.get(idx));
		}
		return format;
	}

	/*
	 * private void populateInitialTerm(Contract contract, ContractDO
	 * contractDO){ LocalDateTime startDateTime =
	 * LocalDateTime.ofInstant(contract.getStartDate().toInstant(),
	 * ZoneId.systemDefault()); LocalDateTime endDateTime =
	 * LocalDateTime.ofInstant(contract.getEndDate().toInstant(),
	 * ZoneId.systemDefault());
	 * 
	 * PeriodDO period = new PeriodDO();
	 * 
	 * int years = Period.between(startDateTime.toLocalDate(),
	 * endDateTime.toLocalDate()).getYears(); if(years > 0){ period.setCycle(new
	 * PeriodDO.Cycle("Y", years)); }else{ int months =
	 * Period.between(startDateTime.toLocalDate(),
	 * endDateTime.toLocalDate()).getMonths(); if(months > 0){
	 * period.setCycle(new PeriodDO.Cycle("M", months)); }else{ int days =
	 * Period.between(startDateTime.toLocalDate(),
	 * endDateTime.toLocalDate()).getDays(); if(days > 0){ period.setCycle(new
	 * PeriodDO.Cycle("D", days)); } } } contractDO.setTerm(period); }
	 */
	private List<ContractDetailDO> getTermContractDetail(UserDO user,
			String contractId, String detailType)
			throws ContracticServiceException {
		Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, user);
		params.put(ContractDetailService.GET_PARAM_ORGANISATIONID,
				user.getOrganisationId());
		params.put(ContractDetailService.GET_PARAM_CONTRACTID, contractId);
		params.put(ContractDetailService.GET_PARAM_GROUPNAME, detailType);

		return contractDetailService.get(params);
	}

	private List<ContractTermDO> getContractTerm(String contractId, UserDO user)
			throws ContracticServiceException {

		List<ContractDetailDO> contractTermPeriodDetailList = getTermContractDetail(
				user, contractId,
				ContractDetail.CONTRACT_TERM_PERIOD_GROUP_FIELD);
		List<ContractDetailDO> contractTermNoticeDetailList = getTermContractDetail(
				user, contractId,
				ContractDetail.CONTRACT_TERM_NOTICE_GROUP_FIELD);
		List<ContractDetailDO> contractTermGoToMarketDetailList = getTermContractDetail(
				user, contractId,
				ContractDetail.CONTRACT_TERM_GOTO_MARKET_GROUP_FIELD);

		final Map<Integer, ContractTermDO> termMap = contractTermPeriodDetailList
				.stream().collect(
						Collectors.toMap(c -> c.getTerm().getNumber(), c -> {
							ContractTermDO termDO = new ContractTermDO();
							termDO.setPeriod(c);
							termDO.setNumber(c.getTerm().getNumber());
							return termDO;
						}));

		contractTermNoticeDetailList.forEach(notice -> {
			ContractTermPeriodDO periodDO = notice.getTerm();
			ContractTermDO termDO = termMap.get(periodDO.getNumber());
			if (termDO != null)
				termDO.setNotice(notice);
		});

		contractTermGoToMarketDetailList.forEach(gotomarket -> {
			ContractTermPeriodDO periodDO = gotomarket.getTerm();
			ContractTermDO termDO = termMap.get(periodDO.getNumber());
			if (termDO != null)
				termDO.setGotomarket(gotomarket);
		});

		return new ArrayList<>(termMap.values());

	}

	/*
	 * private void populateInitialTerm(ContractDO contractDO, Contract
	 * contract){ PeriodDO term = contractDO.getTerm(); PeriodDO.Cycle cycle =
	 * term.getCycle(); String frequency = cycle.frequency;
	 * 
	 * LocalDateTime endDateTime = null; LocalDateTime startDateTime =
	 * LocalDateTime.ofInstant(contract.getStartDate().toInstant(),
	 * ZoneId.systemDefault());
	 * 
	 * switch(frequency){ case "D": endDateTime =
	 * startDateTime.plusDays(cycle.value).minusDays(1); break; case "M":
	 * endDateTime = startDateTime.plusMonths(cycle.value).minusDays(1); break;
	 * case "Y": endDateTime =
	 * startDateTime.plusYears(cycle.value).minusDays(1); break; default: throw
	 * new
	 * IllegalArgumentException("Invalid frequency supplied with initial term");
	 * }
	 * 
	 * Instant instant = endDateTime.atZone(ZoneId.systemDefault()).toInstant();
	 * contract.setEndDate(Date.from(instant)); }
	 */

	private MessageDO saveContractTermPeriods(final ContractDO contractDO,
			final UserDO user) throws Exception {
		final ContractTermDO[] termList = contractDO.getTerm();
		if (termList != null) {
			List<Exception> cse = new ArrayList<>();

			Arrays.stream(termList)
					.forEach(
							termDO -> {
								ContractDetailDO[] periodDetail = new ContractDetailDO[] {
										termDO.getPeriod(), termDO.getNotice(),
										termDO.getGotomarket() };

								Arrays.stream(periodDetail).forEach(detail -> {
									try {
										// Enforce security here
										saveContractTermPeriod(contractDO,
												user, detail);
									} catch (Exception ex) {
										cse.add(ex);
									}
								});
							});

			if (cse.size() > 0) {
				throw cse.get(0);
			}
		}

		return new MessageDO(null, "Contractic",
				"Contract terms created successfully", MessageDO.OK);
	}

	private void saveContractTermPeriod(final ContractDO contractDO,
			final UserDO user, ContractDetailDO detail)
			throws ContracticServiceException {
		detail.setUser(user);
		if (!StringUtils.hasText(detail.getId())) {
			detail.setContract(contractDO);
			contractDetailService.create(detail);
		} else
			contractDetailService.update(detail);
	}

	private void saveSearchEntity(Contract contract) {
		ConcurrencyUtil.IOBoundExecutorService.submit(() -> {
			SearchEntityDO searchEntity = new SearchEntityDO();
			searchEntity.setId(contract.getId());
			searchEntity.setCollection(Module.CONTRACT);
			searchEntity.setOrganisationId(contract.getOrganisation().getId());
			searchEntity.setText(String.join(
					",",
					new String[] { contract.getTitle(),
							contract.getDescription(),
							contract.getOwner().getFirstName(),
							contract.getOwner().getMiddleName(),
							contract.getOwner().getLastName(),
							contract.getReference(),
							contract.getSupplier().getName(),
							contract.getType().getName() }));

			try {
				searchService.create(searchEntity);
			} catch (Exception e) {
				logger.warn(String.format("An error occurred when indexing contract '%s-%s'",  contract.getReference(), contract.getTitle()), e);
			}
		});
	}

	@Override
	public MessageDO create(ContractDO contractDO)
			throws ContracticServiceException {

		UserDO userDO = contractDO.getUser();

		if (userDO == null)
			throw new ContracticServiceException("No user session supplied");

		// Construct the contract object
		final Contract contract = new Contract();
		contract.setTitle(contractDO.getTitle());

		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE,
				Module.CONTRACT);

		Organisation organisation = organisationService.get(contractDO
				.getOrganisationId());
		contract.setOrganisation(organisation);

		final Person person = personService.get(contractDO.getOwner().getId());
		final Supplier supplier = supplierService.get(contractDO.getSupplier()
				.getId());
		final ContractType type = contractTypeService.get(contractDO.getType()
				.getId());
		final Status status = statusService.get(contractDO.getStatus().getId());

		setReference(contractDO, type);
		
		contract.setOwner(person);
		contract.setSupplier(supplier);
		contract.setStatus(status);
		contract.setStartDate(DateUtil.tryParse(contractDO.getStartDate()));
		contract.setEndDate(DateUtil.tryParse(contractDO.getStartDate()));
		contract.setType(type);
		contract.setReference(contractDO.getReference());
		contract.setDescription(contractDO.getDescription());
		contract.setValue(contractDO.getInitialValue());
		contract.setValueUOM(contractDO.getValueUOM());
		// populateInitialTerm(contractDO, contract);
		if (contractDO.getTerm() != null && contractDO.getTerm().length > 0) {
			populateContractTermDates(contractDO, contract, userDO);
		}

		// Save the person object
		try {
			contractDAO.create(contract);

			String hashId = HashingUtil.get(contract.getId());
			// Update the cache
			this.contractIdCache.put(hashId, contract.getId());

			// Update the contract group
			saveDepartment(contractDO, contract);
			saveProcedure(contractDO, contract);

			contractDO.setId(hashId);
			saveContractTermPeriods(contractDO, userDO);
			saveTermPeriodsToCalendar(contractDO, userDO);

			logSessionCreateAction(userDO, hashId,
					String.format("New contract '%s'", contract.getTitle()));

			saveSearchEntity(contract);

		} catch (Exception e) {
			MessageDO messageDO = new MessageDO();
			messageDO.setBody(String.format(
					"Failed to create contract: %s", contractDO.getTitle()));
			messageDO.setType(MessageDO.ERROR);
			throw new ContracticServiceException(messageDO.getBody(), messageDO, e);
		}

		// final String icon, final String title, final String body, final
		// String type
		MessageDO message = new MessageDO(null, "Contractic", String.format(
				"Contract '%s' created successfully", contractDO.getTitle()),
				MessageDO.OK);
		message.setRef(contractDO);
		return message;

	}

	private MessageDO saveTermPeriodsToCalendar(ContractDO contractDO,
			UserDO userDO) throws Exception {
		// TODO Auto-generated method stub
		final ContractTermDO[] termList = contractDO.getTerm();
		if (termList != null) {
			ObjectMapper mapper = new ObjectMapper();

			List<Exception> cse = new ArrayList<>();

			Arrays.stream(termList)
					.forEach(
							termDO -> {
								ContractDetailDO[] periodDetail = new ContractDetailDO[] {
										termDO.getPeriod(), termDO.getNotice(),
										termDO.getGotomarket() };
								int termNumber = termDO.getNumber();
								Arrays.stream(periodDetail)
										.forEach(detail -> {
											try {
												// Enforce security here
												String title = null;
												String groupName = detail
														.getGroup().getValue();
												if (groupName
														.equals(ContractDetail.CONTRACT_TERM_GOTO_MARKET_GROUP_FIELD))
													title = (termNumber == 1) ? "Go to market (Initial Term)"
															: String.format(
																	"Go to market (Extension %s)",
																	termNumber - 1);
												else if (groupName
														.equals(ContractDetail.CONTRACT_TERM_NOTICE_GROUP_FIELD))
													title = (termNumber == 1) ? "Expiry notice (Initial Term)"
															: String.format(
																	"Expiry notice (Extension %s)",
																	termNumber - 1);
												else if (groupName
														.equals(ContractDetail.CONTRACT_TERM_PERIOD_GROUP_FIELD))
													title = (termNumber == 1) ? "Contract start (Initial Term)"
															: String.format(
																	"Contract start (Extension %s)",
																	termNumber - 1);
												saveTermPeriodToCalendar(
														contractDO, userDO,
														detail, title);
											} catch (Exception ex) {
												try {
													logger.warn(
															String.format(
																	"An error occured while saving term period %s to calendar",
																	mapper.writeValueAsString(periodDetail)),
															ex);
												} catch (Exception e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												cse.add(ex);
											}
										});
							});

			if (cse.size() > 0) {
				throw cse.get(0);
			}
		}

		return new MessageDO(null, "Contractic",
				"Contract terms created successfully", MessageDO.OK);
	}

	// -- BEGIN NOTICE
	// THESE WOULD BE REMOVED WHEN A GENERIC get(Map<String, Object> params)
	// RETURNING MODEL OBJECTS IS INTRODUCED

	// -- END NOTICE

	private List<TaskDO> getDetailTask(ContractDetailDO contractDetailDO,
			UserDO userDO, ModuleDO moduleDO) throws ContracticServiceException {
		final Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, userDO);
		params.put(TaskService.GET_PARAM_EXTERNALID, contractDetailDO.getId());
		params.put(TaskService.GET_PARAM_MODULEID, moduleDO.getId());
		return taskService.get(params);
	}

	private void saveTermPeriodToCalendar(ContractDO contractDO, UserDO userDO,
			ContractDetailDO detail, String title)
			throws ContracticServiceException, JsonProcessingException {

		ModuleDO moduleDO = CommonService.getModule(moduleService,
				Module.CONTRACTDETAIL);
		List<TaskDO> detailTaskList = getDetailTask(detail, userDO, moduleDO);

		logger.info(String.format("CONTRACT TERM CALENDAR EVENT - %s - %s",
				detailTaskList.size() > 0 ? "UPDATING" : "CREATING",
				new ObjectMapper().writeValueAsString(detail)));

		if (detailTaskList.size() > 0) {

			TaskDO taskDO = detailTaskList.get(0);
			taskDO.setUser(userDO);
			taskDO.setTitle(title);
			taskDO.setStartDate(detail.getTerm().getStart());
			taskDO.setEndDate(taskDO.getStartDate());
			taskService.update(taskDO);
		} else {
			TaskDO taskDO = new TaskDO();
			taskDO.setUser(userDO);
			taskDO.setTitle(title);
			taskDO.setModule(new ValueDO(moduleDO.getId()));
			Map<String, Object> externalMap = new HashMap<>();
			externalMap.put("id", detail.getId());
			taskDO.setExternal(externalMap);
			taskDO.setPerson(contractDO.getOwner());
			taskDO.setStartDate(detail.getTerm().getStart());
			taskDO.setEndDate(taskDO.getStartDate());
			taskService.create(taskDO);
		}

	}

	@Override
	public List<ContractDO> get(Map<String, Object> params)
			throws ContracticServiceException {
		final String hash = (String) params.get(GET_PARAM_HASHID);
		Boolean enableLog = (Boolean) params.get(GET_LOG_READ);
		enableLog = enableLog == null ? false : enableLog;
		Long id = (Long) params.get(GET_PARAM_ID);

		UserDO userDO = (UserDO) params.get(CommonService.USER);
		if (userDO == null)
			throw new ContracticServiceException(
					"Invalid session user provided");

		commonService.checkPermission(userDO, UserRolePermissionAction.READ,
				Module.CONTRACT);

		final String organisationId = userDO.getOrganisationId();
		if (!StringUtils.hasText(organisationId)) {
			throw new ContracticServiceException("OrganisationId not supplied");
		}

		List<Contract> contracts = null;
		try {
			Contract contract = new Contract();

			Organisation organisation = organisationService.get(organisationId);
			contract.setOrganisation(organisation);

			if (StringUtils.hasText(hash)) {
				id = this.contractIdCache.get(hash);
			}
			if (id != null)
				contract.setId(id);

			contracts = contractDAO.read(contract);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(
					"An error occured while attempting to read contracts", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();

		ModuleDO moduleDO = CommonService.getModule(moduleService,
				Module.CONTRACT);
		GroupSetDO departmentGroupSetDO = GroupSetService.getGroupSet(
				groupSetService, organisationId, GroupSet.DEPARTMENT);
		GroupSetDO procedureGroupSetDO = GroupSetService.getGroupSet(
				groupSetService, organisationId, GroupSet.PROCEDURE);
		
		departmentGroupSetDO.setUser(userDO);
		procedureGroupSetDO.setUser(userDO);

		List<ContractDO> pojoList = new ArrayList<>();
		GroupDO[] userDepartmentGroupDO = userDO.getPerson().getDepartment();
		if (userDepartmentGroupDO == null)
			return pojoList;

		// Get the user's department groups
		List<Group> userDepartmentGroupList = Arrays
				.stream(userDepartmentGroupDO)
				.map(departmentGroup -> {
					try {
						return groupService.get(departmentGroup.getId());
					} catch (Exception ex) {
						cse.add(new ContracticServiceException(
								String.format(
										"An error occured when getting the user deparment groups %s",
										departmentGroup), ex));
					}
					return null;
				}).collect(Collectors.toList());

		// If we had any exception, we throw it here
		if (cse.size() != 0)
			throw cse.get(0);

		pojoList = contracts
				.stream()
				.parallel()
				.filter(contract -> {
					// If user is not an administrator, check that user is a
					// member of this contract's department group or its any of
					// its parent
					// Log all exclusions
					// Get user's department group
					// Get the contract's department group

					try {
						List<GroupDO> contractDepartmentGroups = GroupService
								.getObjectGroup(groupService, contract.getId(),
										departmentGroupSetDO, moduleDO);
						assert contractDepartmentGroups.size() == 1 : String
								.format("Contract '%s' exists in %s groups",
										contract,
										contractDepartmentGroups.size());
						Group contractDepartmentGroup = groupService
								.get(contractDepartmentGroups.get(0).getId());

						return userDepartmentGroupList.stream().anyMatch(
								userDepartmentGroup -> {
									return compareGroups(
											contractDepartmentGroup,
											userDepartmentGroup);
								});

					} catch (Exception ex) {
						cse.add(new ContracticServiceException(
								String.format(
										"An error occured when getting the contract and user deparment groups %s",
										contract), ex));
					}
					return false;

				})
				.map(contract -> {
					ContractDO contractDO = null;
					try {
						contractDO = makeDO(contract, organisationId,
								departmentGroupSetDO, procedureGroupSetDO,
								userDO);
						// Side effect 1
						contractIdCache.put(contractDO.getId(),
								contract.getId());

						// Populate the contract value.
						// We have to do it here after the cache has been
						// updated with this contract so that the contract
						// details service
						// can reference it.
						// populateContractValue(contractDO, contract, userDO);

						populateContractTerm(contractDO, userDO);

					} catch (ContracticServiceException
							| NoSuchAlgorithmException ex) {
						cse.add(new ContracticServiceException(
								String.format(
										"An error occured when hashing the contract %s",
										contract), ex));
					}

					return contractDO;
				}).collect(Collectors.toList());

		if (cse.size() != 0)
			throw cse.get(0);

		if (enableLog) {
			pojoList.forEach(contract -> {
				try {
					logSessionReadAction(userDO, contract.getId(),
							String.format("Contract %s", contract.getTitle()));
				} catch (Exception e) {
					logger.error("Failing to write audit logs", e);
				}
			});
		}
		return pojoList;
	}

	private boolean compareGroups(Group contractDepartmentGroup,
			Group userDepartmentGroup) {
		long contractDepartmentId = contractDepartmentGroup.getId();
		long userDepartmentId = userDepartmentGroup.getId();
		return contractDepartmentId == userDepartmentId;
	}

	private void logSessionDeleteAction(UserDO userDO, String contractId,
			String logMessage) throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService,
				Module.CONTRACT);
		CommonService.logCreateAction(auditService, userDO, contractId,
				moduleDO, logMessage, null, null, null);
	}

	private void logSessionCreateAction(UserDO userDO, String contractId,
			String logMessage) throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService,
				Module.CONTRACT);
		CommonService.logCreateAction(auditService, userDO, contractId,
				moduleDO, logMessage, null, null, null);
	}

	private void logSessionReadAction(UserDO userDO, String contractId,
			String logMessage) throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService,
				Module.CONTRACT);
		CommonService.logReadAction(auditService, userDO, contractId, moduleDO,
				logMessage, null, null, null);
	}

	private void logSessionUpdateAction(UserDO userDO, String contractId,
			String logMessage, String field, String from, String to)
			throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService,
				Module.CONTRACT);
		CommonService.logUpdateAction(auditService, userDO, contractId,
				moduleDO, logMessage, field, from, to);
	}

	private ContractDO makeDO(Contract contract, String organisationId,
			GroupSetDO departmentGroupSet, GroupSetDO procedureGroupSet,
			UserDO userDO) throws ContracticServiceException,
			NoSuchAlgorithmException {
		String hashId = HashingUtil.get(contract.getId());
		ContractDO contractDO = new ContractDO();

		contractDO.setId(hashId);
		contractDO.setTitle(contract.getTitle());
		contractDO.setStartDate(Long
				.toString(contract.getStartDate().getTime()));
		contractDO.setEndDate(Long.toString(contract.getEndDate().getTime()));
		contractDO.setReference(contract.getReference());
		contractDO.setDescription(contract.getDescription());
		contractDO.setOrganisationId(organisationId);
		contractDO.setInitialValue(contract.getValue());
		contractDO.setValueUOM(contract.getValueUOM());
		// populateInitialTerm(contract, contractDO);

		final Map<String, Object> params = new HashMap<>();
		// Get and populate the supplier
		params.clear();
		params.put(SupplierService.GET_PARAM_ID, contract.getSupplier().getId());
		params.put(CommonService.USER, userDO);
		final List<SupplierDO> supplierList = supplierService.get(params);
		assert (supplierList.size() == 1);
		final SupplierDO supplier = supplierList.get(0);
		contractDO
				.setSupplier(new ValueDO(supplier.getId(), supplier.getName()));

		// Get and populate the owner
		params.clear();
		params.put(DomainService.GET_PARAM_ID, contract.getOwner().getId());
		params.put(CommonService.USER, userDO);
		final List<PersonDO> personList = personService.get(params);
		assert (personList.size() == 1);
		final PersonDO personDO = personList.get(0);
		contractDO.setOwner(new ValueDO(personDO.getId(), personDO
				.getShortName()));

		// Get and populate the status
		params.clear();
		params.put(StatusService.GET_PARAM_ID, contract.getStatus().getId());
		params.put(StatusService.GET_PARAM_ORGANISATIONID, organisationId);
		final List<StatusDO> statusList = statusService.get(params);
		assert (statusList.size() == 1);
		final StatusDO statusDO = statusList.get(0);
		contractDO.setStatus(new ValueDO(statusDO.getId(), statusDO.getName()));

		// Get and populate the contract type
		params.clear();
		params.put(ContractTypeService.GET_PARAM_ID, contract.getType().getId());
		params.put(ContractTypeService.GET_PARAM_ORGANISATIONID, organisationId);
		final List<ContractTypeDO> typeList = contractTypeService.get(params);
		assert (typeList.size() == 1);
		final ContractTypeDO typeDO = typeList.get(0);
		contractDO.setType(new ValueDO(typeDO.getId(), typeDO.getName()));

		// Get and populate the group
		{
			params.clear();
			params.put(CommonService.USER, userDO);
			params.put(GroupService.GET_PARAM_GROUPSETID,
					departmentGroupSet.getId());
			params.put(GroupService.GET_PARAM_EXTERNALID, contract.getId());
			ModuleDO moduleDO = CommonService.getModule(moduleService,
					Module.CONTRACT);
			params.put(GroupService.GET_PARAM_MODULEID, moduleDO.getId());
			final List<GroupDO> groupList = groupService.get(params);
			assert groupList.size() <= 1;
			if (groupList.size() == 1) {
				final GroupDO groupDO = groupList.get(0);
				contractDO.setDepartment(new ValueDO(groupDO.getId(), groupDO
						.getName()));
			}
		}

		{
			params.clear();
			params.put(CommonService.USER, userDO);
			params.put(GroupService.GET_PARAM_GROUPSETID,
					procedureGroupSet.getId());
			params.put(GroupService.GET_PARAM_EXTERNALID, contract.getId());
			ModuleDO moduleDO = CommonService.getModule(moduleService,
					Module.CONTRACT);
			params.put(GroupService.GET_PARAM_MODULEID, moduleDO.getId());
			final List<GroupDO> groupList = groupService.get(params);
			assert groupList.size() <= 1;
			if (groupList.size() == 1) {
				final GroupDO groupDO = groupList.get(0);
				contractDO.setProcedure(new ValueDO(groupDO.getId(), groupDO
						.getName()));
			}
		}

		return contractDO;
	}

	private void populateContractValue(ContractDO contractDO,
			Contract contract, UserDO userDO) throws ContracticServiceException {
		final Map<String, Object> params = new HashMap<>();
		// Populate the contract value
		params.put(ContractDetailService.GET_PARAM_ORGANISATIONID,
				contractDO.getOrganisationId());
		params.put(ContractDetailService.GET_PARAM_CONTRACTID,
				contractDO.getId());
		params.put(ContractDetailService.GET_PARAM_GROUPNAME, "VALUE");
		params.put(CommonService.USER, userDO);
		final List<ContractDetailDO> contractDetailList = contractDetailService
				.get(params);
		XStream xstream = new XStream();
		xstream.processAnnotations(ContractValueDO.class);

		try {
			final BigDecimal value = contractDetailList
					.stream()
					.map(v -> new BigDecimal(v.getValue().getProjected()
							.replaceAll("[^0-9.]", "")))
					.collect(Collectors.toList()).stream()
					.reduce(BigDecimal.ZERO, BigDecimal::add);
			contractDO
					.setValue(new ContractValueDO(value.toPlainString(), null));
		} catch (NumberFormatException nfe) {
			logger.warn(
					"Could not parse values for contract " + contract.getId(),
					nfe);
		}
	}

	private void populateContractTerm(ContractDO contractDO, UserDO userDO)
			throws ContracticServiceException {
		Collection<ContractTermDO> termCollection = this.getContractTerm(
				contractDO.getId(), userDO);
		contractDO.setTerm(termCollection.toArray(new ContractTermDO[0]));
	}

	private void populateContractTermDates(ContractDO contractDO,
			Contract contract, UserDO userDO) throws ContracticServiceException {
		ContractTermDO[] termDO = contractDO.getTerm();
		if (termDO == null)
			return;
		List<ContractTermDO> termCollection = Arrays.asList(termDO);
		Collections.sort(termCollection,
				(c1, c2) -> c1.getNumber().compareTo(c2.getNumber()));

		final Long endDate = new Long(0);

		LocalDateTime startDateTime = LocalDateTime.ofInstant(contract
				.getStartDate().toInstant(), ZoneId.systemDefault());

		termCollection
				.forEach(term -> {
					LocalDateTime endDateTime = null;
					if (term.getPeriod() != null) {
						ContractTermPeriodDO termPeriod = term.getPeriod()
								.getTerm();
						PeriodDO period = termPeriod.getProperties().period;
						PeriodDO.Cycle termCycle = period.getCycle();
						String frequency = period.getCycle().frequency;
						Integer number = termPeriod.getNumber();

						switch (frequency) {
						case PeriodDO.Cycle.DAY:
							endDateTime = startDateTime.plusDays(
									termCycle.value).minusDays(1);
							break;
						case PeriodDO.Cycle.MONTH:
							endDateTime = startDateTime.plusMonths(
									termCycle.value).minusDays(1);
							break;
						case PeriodDO.Cycle.YEAR:
							endDateTime = startDateTime.plusYears(
									termCycle.value).minusDays(1);
							break;
						default:
							throw new IllegalArgumentException(
									"Invalid frequency supplied with initial term");
						}
						termPeriod.setStart(startDateTime
								.format(DateTimeFormatter
										.ofPattern("yyyy-MM-dd HH:mm:ss")));

						if (number == 1) {
							Instant instant = endDateTime.atZone(
									ZoneId.systemDefault()).toInstant();
							contract.setEndDate(Date.from(instant));
						}
					}

					if (term.getNotice() != null) {
						ContractTermPeriodDO noticeTermPeriod = term
								.getNotice().getTerm();
						PeriodDO noticePeriod = noticeTermPeriod
								.getProperties().period;
						PeriodDO.Cycle termCycle = noticePeriod.getCycle();
						String noticeFrequency = noticePeriod.getCycle().frequency;
						LocalDateTime noticeDateTime = null;
						switch (noticeFrequency) {
						case PeriodDO.Cycle.DAY:
							noticeDateTime = endDateTime
									.minusDays(termCycle.value);
							break;
						case PeriodDO.Cycle.MONTH:
							noticeDateTime = endDateTime
									.minusMonths(termCycle.value);
							break;
						case PeriodDO.Cycle.YEAR:
							noticeDateTime = endDateTime
									.minusYears(termCycle.value);
							break;
						}
						noticeTermPeriod.setStart(noticeDateTime
								.format(DateTimeFormatter
										.ofPattern("yyyy-MM-dd HH:mm:ss")));
					}

					if (term.getGotomarket() != null) {
						ContractTermPeriodDO gotoMarketTermPeriod = term
								.getGotomarket().getTerm();
						PeriodDO gotoMarketPeriod = gotoMarketTermPeriod
								.getProperties().period;
						String gotoMarketFrequency = gotoMarketPeriod
								.getCycle().frequency;
						PeriodDO.Cycle termCycle = gotoMarketPeriod.getCycle();
						LocalDateTime gotoMarketDateTime = null;
						switch (gotoMarketFrequency) {
						case PeriodDO.Cycle.DAY:
							gotoMarketDateTime = endDateTime
									.minusDays(termCycle.value);
							break;
						case PeriodDO.Cycle.MONTH:
							gotoMarketDateTime = endDateTime
									.minusMonths(termCycle.value);
							break;
						case PeriodDO.Cycle.YEAR:
							gotoMarketDateTime = endDateTime
									.minusYears(termCycle.value);
							break;
						}
						gotoMarketTermPeriod.setStart(gotoMarketDateTime
								.format(DateTimeFormatter
										.ofPattern("yyyy-MM-dd HH:mm:ss")));
					}

					startDateTime.adjustInto(endDateTime.plusDays(1));
				});
	}

	public MessageDO delete(final ContractDO pojo)
			throws ContracticServiceException {
		Long contractId = this.contractIdCache.get(pojo.getId());

		if (contractId == null) {
			// final String icon, final String title, final String body, final
			// String type
			return new MessageDO(null, "Internal system error",
					"Invalid contract (may have been deleted)",
					MessageDO.WARNING);
		}

		UserDO userDO = pojo.getUser();
		if (userDO == null)
			throw new ContracticServiceException(
					"A user must be supplied to perform this action");

		commonService.checkPermission(userDO, UserRolePermissionAction.DELETE,
				Module.CONTRACT);

		Contract contract = new Contract();
		try {

			contract = get(contractId);

			// Delete from data source
			contractDAO.delete(contract);

			// if successfull, remove from Id cache
			this.contractIdCache.remove(pojo.getId());

			return new MessageDO(null, "",
					String.format("Contract '%s' deleted successfully!",
							contract.getTitle()), MessageDO.OK);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(
					String.format(
							"An error occurred when performing delete operation on contract object %s",
							contract), e);
		}
	}

}
