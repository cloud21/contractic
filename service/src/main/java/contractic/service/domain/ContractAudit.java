package contractic.service.domain;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import contractic.model.data.store.Contract;

public class ContractAudit implements AuditJob<ContractDO> {

	private final List<AuditDO> auditList = new LinkedList<>();
/*	private AuditDO createAudit(String field, String from, String to){
		AuditDO auditDO = new AuditDO();
		auditDO.setField(field);
		auditDO.setFrom(from);
		auditDO.setTo(to);
		return auditDO;
	}*/
	
	@Override
	public List<AuditDO> run(ContractDO source, ContractDO target) {
		
		// Log change
		if(!Objects.equals(source.getReference(), target.getReference()))
			auditList.add(AuditJob.createAudit(Contract.REFERENCE, source.getReference(), target.getReference()));
		
		if(!Objects.equals(source.getTitle(), target.getTitle()))
			auditList.add(AuditJob.createAudit(Contract.TITLE, source.getTitle(), target.getTitle()));

		if(!Objects.equals(source.getDescription(), target.getDescription()))
			auditList.add(AuditJob.createAudit(Contract.DESCRIPTION, source.getDescription(), target.getDescription()));

		/*
		ValueDO sourceOwner = source.getOwner();
		ValueDO targetOwner = target.getOwner();
		
		if(sourceOwner != null || sourceOwner!= null || !source.getOwner().getId().equals(target.getOwner().getId()) && !source.getOwner().getValue().equals(target.getOwner().getValue()))
			auditList.add(createAudit(Contract.OWNER, source.getOwner().getValue(), target.getOwner().getValue()));

		if(!source.getSupplier().getId().equals(target.getSupplier().getId()) 
				&& !source.getSupplier().getValue().equals(target.getSupplier().getValue()))
			auditList.add(createAudit(Contract.OWNER, source.getSupplier().getValue(), target.getSupplier().getValue()));		
		*/
		return auditList;
	}

}
