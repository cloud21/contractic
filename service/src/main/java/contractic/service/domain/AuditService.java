package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Audit;
import contractic.model.data.store.AuditAction;
import contractic.model.data.store.Contract;
import contractic.model.data.store.Module;
import contractic.model.data.store.Note;
import contractic.model.data.store.NoteType;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Supplier;
import contractic.model.data.store.Task;
import contractic.model.data.store.User;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class AuditService implements DomainService<Audit, Long, AuditDO, MessageDO> {

	public final static String GET_PARAM_USERID = "user.id";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	private static final String GET_PARAM_ID = "id";
		
	public final static String AUDIT_IDCACHE_NAME = "audit.idcache.name";
	public static final String GET_PARAM_EXTERNALID = "external.id";
	public static final String GET_PARAM_MODULEID = "module.id";

	private Cache<String, Long> auditIdCache;
	
	@Autowired
	private CrudOperation<Audit> auditDAO;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	@Autowired
	private DomainService<Supplier, Long, SupplierDO, MessageDO> supplierService;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<AuditAction, Long, StaticDO, MessageDO> auditActionService;
	@Autowired
	private DomainService<Contract, Long, ContractDO, MessageDO> contractService;
	@Autowired
	private DomainService<Task, Long, TaskDO, MessageDO> taskService;
	@Autowired
	private DomainService<User, Long, UserDO, MessageDO> userService;
	@Autowired
	private CommonService commonService;

	public Audit get(final String uuid) throws ContracticServiceException{
		final Long id = this.auditIdCache.get(uuid);
		return get(id);
	}
	
	public Audit get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null audit id supplied");
		
		Audit audit = new Audit();
		audit.setId(id);
		
		try {
			List<Audit> auditList = auditDAO.read(audit);
			return auditList.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving audit with id %d", id));
		}
	}
	
	
	private AuditDO makeDO(final Audit audit) throws NoSuchAlgorithmException, ContracticServiceException{
		String hashId = HashingUtil.get(audit.getId());
		AuditDO auditDO = new AuditDO();
		auditDO.setId(hashId);
		auditDO.setDescription(audit.getDescription());
		auditDO.setField(audit.getChangeField());
		auditDO.setFrom(audit.getChangeFrom());
		auditDO.setTo(audit.getChangeTo());
		auditDO.setCreated(audit.getCreated() != null ? Long.toString(audit.getCreated().getTime()) : "");
		
		// Get the ModuleID
		Module module = audit.getModule();
		if(module == null){
			throw new ContracticServiceException(String.format("Note supplied without a module: %s", audit));
		}
		final Map<String, Object> params = new HashMap<>();
		
		// Set the module Id and the external Id
		{
			params.put(ModuleService.GET_PARAM_ID, module.getId());
			final List<ModuleDO> moduleList = moduleService.get(params);
			assert(moduleList.size() == 1);
			final ModuleDO moduleDO = moduleList.get(0);
			auditDO.setModule(new ValueDO(moduleDO.getId(), moduleDO.getName()));
			
/*			String externalId = commonService.getExternalId(audit.getExternalId(), audit.getModule(), audit.getUser().getPerson().getOrganisation());
			auditDO.setExternalId(externalId);*/
		}
		
		// Set the audit action id
		{
			params.clear();
			params.put(AuditActionService.GET_PARAM_ID, audit.getAction().getId()); // Parametize me
			final List<StaticDO> auditList = auditActionService.get(params);
			assert(auditList.size() == 1);
			final StaticDO staticDO = auditList.get(0);
			auditDO.setAction(new ValueDO(staticDO.getId(), staticDO.getValue()));
		}
		
		// Set the user id
		{
			params.clear();
			params.put(UserService.GET_PARAM_ID, audit.getUser().getId()); // Parametize me
			final List<UserDO> userList = userService.get(params);
			assert(userList.size() == 1);
			final UserDO userDO = userList.get(0);
			auditDO.setUser(new ValueDO(userDO.getId(), userDO.getPersonShortName()));
		}
		
		return auditDO;
	}
	
	public List<AuditDO> get(final Map<String, Object> params) throws ContracticServiceException{
		final String hash = (String)params.get(GET_PARAM_HASHID);
		final String externalId = (String)params.get(GET_PARAM_EXTERNALID);
		final Long auditId = (Long)params.get(GET_PARAM_ID);
		final String moduleId = (String)params.get(GET_PARAM_MODULEID);
		final String userId = (String)params.get(GET_PARAM_USERID);
		final String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);
		 
		final UserDO userDO = (UserDO)params.get(CommonService.USER);
		if(userDO == null)
			throw new ContracticServiceException("Invalid session user provided");
		
		if(!StringUtils.hasText(moduleId) && !StringUtils.hasText(userId))
			throw new ContracticServiceException("Missing moduleId, AuditService needs a moduleId or userId");
		
		if(!StringUtils.hasText(organisationId))
			throw new ContracticServiceException("Organisation Id must be specified");
		
		Organisation organisation = organisationService.get(organisationId);
		 
		List<Audit> auditList = null;
		try {
			Audit audit = new Audit();
			audit.setOrganisation(organisation);
			
			if(StringUtils.hasText(moduleId)){
				Module module = moduleService.get(moduleId);
				audit.setModule(module);
				
				if(StringUtils.hasText(externalId)){
					Long extId = commonService.getExternalId(externalId, module);
					audit.setExternalId(extId);
				}
			}
				
				audit.setId(auditId);
				if(StringUtils.hasText(hash)){
					Long id = this.auditIdCache.get(hash);
					audit.setId(id);
				}
				
				if(StringUtils.hasText(userId)){
					User user = this.userService.get(userId);
					audit.setUser(user);
				}
				

								
				auditList = auditDAO.read(audit);
			} catch (ContracticModelException e) {
				throw new ContracticServiceException("An error occured while attempting to read address types", e);
			}

			// List of all exceptions that occur in the predicate
			List<ContracticServiceException> cse = new ArrayList<>();
		
			final List<AuditDO> pojoList = auditList.stream()
				.parallel()
				.map(audit -> {
					AuditDO auditDO = null;
					try{
						auditDO = makeDO(audit);						
						// Side effect 1
						auditIdCache.put(auditDO.getId(), audit.getId());

					}catch(ContracticServiceException | NoSuchAlgorithmException ex){
						cse.add(new ContracticServiceException(String.format("An error occured when hashing the audit %s", audit)));
					}
					
					return auditDO;
				}).collect(Collectors.toList());
			
			if(cse.size()!=0)
				throw cse.get(0);
			
			return pojoList;
	}
	
	public MessageDO create(AuditDO auditDO) throws ContracticServiceException{
		// Construct the person object
		final Audit audit = new Audit();
		
		Module module = moduleService.get(auditDO.getModule().getId());
		AuditAction auditAction = auditActionService.get(auditDO.getAction().getId());
		Long externalId = commonService.getExternalId(auditDO.getExternalId(), module);
		User user = userService.get(auditDO.getUser().getId());
		Organisation organisation = organisationService.get(auditDO.getOrganisationId());
		
		audit.setUser(user);
		audit.setModule(module);
		audit.setExternalId(externalId);
		audit.setAction(auditAction);
		audit.setDescription(auditDO.getDescription());
		audit.setChangeField(auditDO.getField());
		audit.setChangeFrom(auditDO.getFrom());
		audit.setChangeTo(auditDO.getTo());
		audit.setCreated(new Date());
		audit.setOrganisation(organisation);
		
		// Save the person object
		try {
			auditDAO.create(audit);
			
			String hashId = HashingUtil.get(audit.getId());
			// Update the cache
			this.auditIdCache.put(hashId, audit.getId());
			
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create note: %s", auditDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", "Audit saved successfully", MessageDO.OK);
	}
	
	public MessageDO update(AuditDO auditDO) throws ContracticServiceException{
		// Construct the note object
		throw new RuntimeException("Not implemented");
	}
	
	public MessageDO delete(AuditDO auditDO){
		return null;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.auditIdCache = new Cache(AUDIT_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}
}
