package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.dao.NoteTypeDAO;
import contractic.model.data.store.Module;
import contractic.model.data.store.NoteType;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.UserRolePermissionAction;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class NoteTypeService implements DomainService<NoteType, Long, NoteTypeDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	public final static String NOTETYPE_IDCACHE_NAME = "notetype.idcache.name";
	public static final String GET_PARAM_NOTEID = "note.id";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public static final String USERID = "user.id";
	
	private Cache<String, Long> noteTypeIdCache;
	
	@Autowired
	private CommonService commonService;
	@Autowired
	private CrudOperation<NoteType> noteTypeDAO;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	
	public NoteType get(final String uuid) throws ContracticServiceException{
		final Long id = this.noteTypeIdCache.get(uuid);
		return get(id);
	}
	
	public NoteType get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		NoteType noteType = new NoteType();
		noteType.setId(id);
		
		try {
			List<NoteType> types = noteTypeDAO.read(noteType);
			return types.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving note type with id %d", id));
		}
	}
	
	
	private NoteTypeDO makeDO(final NoteType noteType) throws NoSuchAlgorithmException{
		String hashId = HashingUtil.get(noteType.getId());
		NoteTypeDO staticDO = new NoteTypeDO();
		staticDO.setId(hashId);
		staticDO.setName(noteType.getName());
		staticDO.setDescription(noteType.getDescription());
		staticDO.setActive(noteType.getActive());
		return staticDO;
	}
	
	public List<NoteTypeDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASH);
		 final String name = (String)params.get(GET_PARAM_NAME);
		 Long noteId = (Long)params.get(GET_PARAM_NOTEID);
		 String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);
		 
			if(!StringUtils.hasText(organisationId)){
				throw new ContracticServiceException("OrganisationId not supplied");
			}

			List<NoteType> types = null;
			try {
				NoteType type = new NoteType();
				
				if(StringUtils.hasText(name)){
					type.setName(name);
				}
				
				if(StringUtils.hasText(hash)){
					noteId = this.noteTypeIdCache.get(hash);
				}
				
				if(noteId != null){
					type.setId(noteId);
				}
				
				Organisation organisation = organisationService.get(organisationId);
				type.setOrganisation(organisation);
				
				types = noteTypeDAO.read(type);
			} catch (ContracticModelException e) {
				throw new ContracticServiceException("An error occured while attempting to read address types", e);
			}

			// List of all exceptions that occur in the predicate
			List<ContracticServiceException> cse = new ArrayList<>();
			
			final List<NoteTypeDO> pojoList = types.stream()
				.map(type -> {
					NoteTypeDO staticDO = null;
					try{
						staticDO = makeDO(type);						
						// Side effect 1
						noteTypeIdCache.put(staticDO.getId(), type.getId());

					}catch(NoSuchAlgorithmException ex){
						cse.add(new ContracticServiceException(String.format("An error occured when hashing the note type %s", type)));
					}
					
					return staticDO;
				}).collect(Collectors.toList());
			
			if(cse.size()!=0)
				throw cse.get(0);
			
			return pojoList;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.noteTypeIdCache = new Cache(NOTETYPE_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}
	
	public MessageDO delete(NoteTypeDO noteTypeDO) throws ContracticServiceException {
		
		UserDO userDO = noteTypeDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.DELETE, Module.ORGANISATION);
		
		final String hash = noteTypeDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("");
			
		final Long id = noteTypeIdCache.get(hash);	
		
		final String organisationId = noteTypeDO.getOrganisationId();
		Organisation organisation = organisationService.get(organisationId);
		
		// Save the person object
		try {

			NoteType noteType = new NoteType();
			noteType.setId(id);
			
			noteType.setOrganisation(organisation);
			List<NoteType> typeList = noteTypeDAO.read(noteType);
			assert(typeList.size()==1);
			noteType = typeList.get(0);
			
			noteTypeDAO.delete(noteType);
		
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to delete note type: %s", noteTypeDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Note type '%s' updated successfully", noteTypeDO.getName()), MessageDO.OK);
	}

	public MessageDO update(NoteTypeDO noteTypeDO) throws ContracticServiceException {
		
		UserDO userDO = noteTypeDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		final String hash = noteTypeDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Invalid document hash id provided");
			
		final Long id = noteTypeIdCache.get(hash);
		
		final String organisationId = noteTypeDO.getOrganisationId();
		Organisation organisation = organisationService.get(organisationId);
		
		// Save the person object
		try {

			NoteType noteType = new NoteType();
			
			// Find the document type and load it into cache
			noteType.setId(id);
			noteType.setOrganisation(organisation);
			List<NoteType> typeList = noteTypeDAO.read(noteType);
			assert(typeList.size()==1);
			noteType = typeList.get(0);
			
			// Update the fields
			noteType.setName(noteTypeDO.getName());
			noteType.setDescription(noteTypeDO.getDescription());
			noteType.setActive(noteTypeDO.getActive());
			
			// Update the data store
			noteTypeDAO.update(noteType);
		
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update note type: %s", noteTypeDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Note type '%s' updated successfully", noteTypeDO.getName()), MessageDO.OK);
	}

	public MessageDO create(NoteTypeDO noteTypeDO) throws ContracticServiceException {
		
		UserDO userDO = noteTypeDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		final NoteType noteType = new NoteType();
		noteType.setName(noteTypeDO.getName());
		noteType.setDescription(noteTypeDO.getDescription());
		noteType.setActive(noteTypeDO.getActive());
		
		Organisation organisation = organisationService.get(noteTypeDO.getOrganisationId());
		noteType.setOrganisation(organisation);
		
		ValueDO resultValue = new ValueDO();
		// Save the person object
		try {
			noteTypeDAO.create(noteType);
			
			String hashId = HashingUtil.get(noteType.getId());
			// Update the cache
			this.noteTypeIdCache.put(hashId, noteType.getId());
			resultValue.setId(hashId);
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create note type: %s", noteTypeDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		MessageDO message = new MessageDO(null, "Contractic", String.format("Note type '%s' created successfully", noteTypeDO.getName()), MessageDO.OK);
		message.setRef(resultValue);
		return message;
	}
}
