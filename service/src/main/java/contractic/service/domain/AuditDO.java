package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"organisationId"})
public class AuditDO {
	private String id;
	private String description;
	private String field;
	private String from;
	private String to;
	private String externalId;
	private ValueDO module;
	private String created;
	private ValueDO user;
	private ValueDO action;
	private String organisationId;
	
	public AuditDO(){}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public ValueDO getModule() {
		return module;
	}
	public void setModule(ValueDO module) {
		this.module = module;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public ValueDO getUser() {
		return user;
	}
	public void setUser(ValueDO user) {
		this.user = user;
	}
	public ValueDO getAction() {
		return action;
	}
	public void setAction(ValueDO action) {
		this.action = action;
	}

	public String getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}

}
