package contractic.service.domain;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * A services offered by a single domain service to other domain services
 * @author Michael Sekamanya
 *
 * @param <M> The data model type (most likely the one modelled by the ORM layer)
 * @param <I> The domain object identifier type
 */
public interface DomainService
	<M, I, D, V>
{
	
	
	public static ResourceBundle getErrorBundle(){
		String name = "ServiceErrors";
		return ResourceBundle.getBundle(name);
	}
	public final static String GET_PARAM_ID = "model.id";
	public static final String GET_PARAM_HASHID = "hash.id";
	
	// All services are intended to implement these methods
	M get(String hash) throws ContracticServiceException;
	M get(I id) throws ContracticServiceException;
	void init() throws ContracticServiceException;
	void destroy() throws ContracticServiceException;
	V update(D dojo) throws ContracticServiceException;
	V create(D dojo) throws ContracticServiceException;
	V delete(D dojo) throws ContracticServiceException;
	List<D> get(Map<String, Object> params) throws ContracticServiceException;
}
