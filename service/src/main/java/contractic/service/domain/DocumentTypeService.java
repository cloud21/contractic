package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.DocumentType;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.UserRolePermissionAction;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class DocumentTypeService implements DomainService<DocumentType, Long, DocumentTypeDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	private final static String DOCUMENTTYPE_IDCACHE_NAME = "documenttype.idcache.name";
	public static final String GET_PARAM_ID = "doctype.id";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public static final String USERID = "user.id";
	
	private Cache<String, Long> documentTypeIdCache;
	
	@Autowired
	private CommonService commonService;
	@Autowired
	private CrudOperation<DocumentType> documentTypeDAO;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	
	public DocumentType get(final String uuid) throws ContracticServiceException{
		final Long id = this.documentTypeIdCache.get(uuid);
		return get(id);
	}
	
	public DocumentType get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		DocumentType documentType = new DocumentType();
		documentType.setId(id);
		
		try {
			List<DocumentType> types = documentTypeDAO.read(documentType);
			return types.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving document type with id %d", id));
		}
	}
	
	
	private DocumentTypeDO makeDO(final DocumentType documentType) throws NoSuchAlgorithmException, ContracticServiceException{
		String hashId = HashingUtil.get(documentType.getId());
		DocumentTypeDO documentTypeDO = new DocumentTypeDO();
		documentTypeDO.setId(hashId);
		documentTypeDO.setName(documentType.getName());
		documentTypeDO.setDescription(documentType.getDescription());
		documentTypeDO.setActive(documentType.getActive());
		
		Map<String, Object> params = new HashMap<>();
		params.put(OrganisationService.GET_PARAM_ID, documentType.getOrganisation().getId());
		List<OrganisationDO> orgList = organisationService.get(params);
		assert(orgList.size() == 1);
		documentTypeDO.setOrganisationId(orgList.get(0).getId());
		
		return documentTypeDO;
	}
	
	public List<DocumentTypeDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASH);
		 final String name = (String)params.get(GET_PARAM_NAME);
		 final Long id = (Long)params.get(GET_PARAM_ID);
		 String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);

		if(!StringUtils.hasText(organisationId)){
			throw new ContracticServiceException("OrganisationId not supplied");
		}
	 
		List<DocumentType> types = null;
		try {
			DocumentType type = new DocumentType();
			
			Organisation organisation = organisationService.get(organisationId);
			type.setOrganisation(organisation);
			
			type.setId(id);
			type.setName(name);
			
						
			if(StringUtils.hasText(hash)){
				type.setId(this.documentTypeIdCache.get(hash));
			}
							
			types = documentTypeDAO.read(type);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read address types", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		final List<DocumentTypeDO> pojoList = types.stream()
			.map(type -> {
				DocumentTypeDO doctypeDO = null;
				try{
					doctypeDO = makeDO(type);						
					// Side effect 1
					documentTypeIdCache.put(doctypeDO.getId(), type.getId());

				}catch(final ContracticServiceException | NoSuchAlgorithmException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the document type %s", type)));
				}
				
				return doctypeDO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.documentTypeIdCache = new Cache(DOCUMENTTYPE_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	public MessageDO delete(DocumentTypeDO documentTypeDO) throws ContracticServiceException {
		
		UserDO userDO = documentTypeDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.DELETE, Module.ORGANISATION);
		
		final String hash = documentTypeDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("");
			
		final Long id = documentTypeIdCache.get(hash);	
		
		final String organisationId = documentTypeDO.getOrganisationId();
		Organisation organisation = organisationService.get(organisationId);
		
		// Save the person object
		try {

			DocumentType documentType = new DocumentType();
			documentType.setId(id);
			
			documentType.setOrganisation(organisation);
			List<DocumentType> doctypeList = documentTypeDAO.read(documentType);
			assert(doctypeList.size()==1);
			documentType = doctypeList.get(0);
			
			documentTypeDAO.delete(documentType);
		
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Failed to delete document type: %s", documentTypeDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Document type '%s' updated successfully", documentTypeDO.getName()), MessageDO.OK);
	}

	public MessageDO update(DocumentTypeDO documentTypeDO) throws ContracticServiceException {
		
		UserDO userDO = documentTypeDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		final String hash = documentTypeDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Invalid document hash id provided");
			
		final Long id = documentTypeIdCache.get(hash);
		
		final String organisationId = documentTypeDO.getOrganisationId();
		Organisation organisation = organisationService.get(organisationId);
		
		// Save the person object
		try {

			DocumentType documentType = new DocumentType();
			
			// Find the document type and load it into cache
			documentType.setId(id);
			documentType.setOrganisation(organisation);
			List<DocumentType> doctypeList = documentTypeDAO.read(documentType);
			assert(doctypeList.size()==1);
			documentType = doctypeList.get(0);
			
			// Update the fields
			documentType.setName(documentTypeDO.getName());
			documentType.setDescription(documentTypeDO.getDescription());
			documentType.setActive(documentTypeDO.getActive());
			
			// Update the data store
			documentTypeDAO.update(documentType);
		
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update document type: %s", documentTypeDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Document type '%s' updated successfully", documentTypeDO.getName()), MessageDO.OK);
	}

	public MessageDO create(DocumentTypeDO documentTypeDO) throws ContracticServiceException {
		
		UserDO userDO = documentTypeDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		final DocumentType documentType = new DocumentType();
		documentType.setName(documentTypeDO.getName());
		documentType.setDescription(documentTypeDO.getDescription());
		documentType.setActive(documentTypeDO.getActive());
		
		Organisation organisation = organisationService.get(documentTypeDO.getOrganisationId());
		documentType.setOrganisation(organisation);
		
		// Save the person object
		try {
			documentTypeDAO.create(documentType);
			
			String hashId = HashingUtil.get(documentType.getId());
			// Update the cache
			this.documentTypeIdCache.put(hashId, documentType.getId());
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create document type: %s", documentTypeDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Document type '%s' created successfully", documentTypeDO.getName()), MessageDO.OK);
	}
}
