package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"organisationId", "user"})
public class UserRolePermissionDO {
	private String id;
	private ValueDO module;
	private Boolean read;
	private Boolean write;
	private Boolean delete;
	private String organisationId;
	private UserDO user;
	private String externalId;
	private ValueDO role;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ValueDO getModule() {
		return module;
	}
	public void setModule(ValueDO module) {
		this.module = module;
	}
	public Boolean getRead() {
		return read;
	}
	public void setRead(Boolean read) {
		this.read = read;
	}
	public Boolean getWrite() {
		return write;
	}
	public void setWrite(Boolean write) {
		this.write = write;
	}
	public Boolean getDelete() {
		return delete;
	}
	public void setDelete(Boolean delete) {
		this.delete = delete;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	public UserDO getUser() {
		return user;
	}
	public void setUser(UserDO user) {
		this.user = user;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public ValueDO getRole() {
		return role;
	}
	public void setRole(ValueDO role) {
		this.role = role;
	}
	
}
