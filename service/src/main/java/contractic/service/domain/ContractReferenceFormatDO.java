package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"sessionUser, organisationId"})
public class ContractReferenceFormatDO {
	private String id;
	private String format;
	private String name;
	private String description;
	private Boolean difault;
	private Boolean active;
	private ValueDO contractType;
	private String organisationId;
	private UserDO sessionUser;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getDifault() {
		return difault;
	}
	public void setDifault(Boolean difault) {
		this.difault = difault;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public ValueDO getContractType() {
		return contractType;
	}
	public void setContractType(ValueDO contractType) {
		this.contractType = contractType;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	public UserDO getSessionUser() {
		return sessionUser;
	}
	public void setSessionUser(UserDO sessionUser) {
		this.sessionUser = sessionUser;
	}
	
	
	
}
