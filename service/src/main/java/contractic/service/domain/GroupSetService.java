package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.GroupSet;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class GroupSetService implements DomainService<GroupSet, Long, GroupSetDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	public final static String GROUPSET_IDCACHE_NAME = "groupset.idcache.name";
	public static final String GET_PARAM_ID = "group.id";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public static final String GET_PARAM_ACTIVE = "active";
	public static final String USERID = "user.id";
	
	private Cache<String, Long> groupSetIdCache;
	
	@Autowired
	private CrudOperation<GroupSet> groupSetDAO;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	
	public GroupSet get(final String uuid) throws ContracticServiceException{
		final Long id = this.groupSetIdCache.get(uuid);
		return get(id);
	}
	
	public GroupSet get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		GroupSet group = new GroupSet();
		group.setId(id);
		
		try {
			List<GroupSet> groupList = groupSetDAO.read(group);
			GroupSet groupSet = groupList.get(0);
			return groupSet;
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving group with id %d", id));
		}
	}
	
	
	private GroupSetDO makeDO(final GroupSet groupSet, final String organisationId) throws NoSuchAlgorithmException, ContracticServiceException{
		String hashId = HashingUtil.get(groupSet.getId());
		GroupSetDO groupDO = new GroupSetDO();
		groupDO.setId(hashId);
		groupDO.setName(groupSet.getName());
		groupDO.setDescription(groupSet.getDescription());
		groupDO.setActive(groupSet.getActive());
		groupDO.setOrganisationId(organisationId);
		
		return groupDO;
	}
	
	public List<GroupSetDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASH);
		 final String name = (String)params.get(GET_PARAM_NAME);
		 final Long id = (Long)params.get(GET_PARAM_ID);
		 
		 final String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);

		if(!StringUtils.hasText(organisationId)){
			throw new ContracticServiceException("OrganisationId not supplied");
		}
		List<GroupSet> groupList = null;
		try {
			GroupSet group = new GroupSet();
			group.setId(id);
			
			Organisation organisation = organisationService.get(organisationId);
			group.setOrganisation(organisation);
			
			if(StringUtils.hasText(name)){
				group.setName(name);
			}
						
			if(StringUtils.hasText(hash)){
				group.setId(this.groupSetIdCache.get(hash));
			}
							
			groupList = groupSetDAO.read(group);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read groups", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		final List<GroupSetDO> pojoList = groupList.stream()
			.map(group -> {
				GroupSetDO groupDO = null;
				try{
					groupDO = makeDO(group, organisationId);						
					// Side effect 1
					groupSetIdCache.put(groupDO.getId(), group.getId());

				}catch(final ContracticServiceException | NoSuchAlgorithmException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the group %s", group)));
				}
				
				return groupDO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.groupSetIdCache = new Cache(GROUPSET_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	public MessageDO delete(GroupSetDO groupDO) throws ContracticServiceException {
		final String hash = groupDO.getId();			
		final Long id = groupSetIdCache.get(hash);
		final String organisationId = groupDO.getOrganisationId();
		
		// Save the person object
		try {

			GroupSet group = new GroupSet();
			group.setId(id);
			Organisation organisation = organisationService.get(organisationId);
			group.setOrganisation(organisation);
			List<GroupSet> groupList = groupSetDAO.read(group);
			assert(groupList.size()==1);
			group = groupList.get(0);
			
			groupSetDAO.delete(group);
		
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update group: %s", groupDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("GroupSet '%s' deleted successfully", groupDO.getName()), MessageDO.OK);
	}

	public MessageDO update(GroupSetDO groupDO) throws ContracticServiceException {
		final String hash = groupDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Invalid group hash id provided");
					
		final String organisationId = groupDO.getOrganisationId();
		if(!StringUtils.hasText(organisationId))
			throw new ContracticServiceException("Invalid organisation hash id provided");
		
		final Long id = groupSetIdCache.get(hash);
		
		// Save the person object
		try {

			GroupSet group = new GroupSet();
			group.setId(id);
			Organisation organisation = organisationService.get(organisationId);
			group.setOrganisation(organisation);
			List<GroupSet> groupList = groupSetDAO.read(group);
			assert(groupList.size()==1);
			group = groupList.get(0);
			
			group.setName(groupDO.getName());
			group.setDescription(groupDO.getDescription());
			group.setActive(groupDO.getActive());
			
			groupSetDAO.update(group);
		
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update group: %s", groupDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("GroupSet '%s' updated successfully", groupDO.getName()), MessageDO.OK);
	}

	/**
	 * Creates the group. Validation of input is left to the client (e.g. controllers)
	 * @param groupSetDO
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO create(GroupSetDO groupSetDO) throws ContracticServiceException {
		final GroupSet groupSet = new GroupSet();
		groupSet.setName(groupSetDO.getName());
		groupSet.setDescription(StringUtils.hasText(groupSetDO.getDescription()) ? groupSetDO.getDescription() : groupSetDO.getName());
		groupSet.setActive(groupSetDO.getActive());
		
		// Set the organisation
		Organisation organisation = organisationService.get(groupSetDO.getOrganisationId());
		groupSet.setOrganisation(organisation);
		
		// Save the person object
		try {
			groupSetDAO.create(groupSet);
			
			String hashId = HashingUtil.get(groupSet.getId());
			// Update the cache
			this.groupSetIdCache.put(hashId, groupSet.getId());
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create group: %s", groupSetDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("GroupSet '%s' created successfully", groupSetDO.getName()), MessageDO.OK);
	}
	
	
	public static GroupSetDO getGroupSet(final DomainService<GroupSet, Long, GroupSetDO, MessageDO> groupSetService, 
			final String organisationId, String name) throws ContracticServiceException{
		final Map<String, Object> params = new HashMap<>();
		params.put(GroupSetService.GET_PARAM_ORGANISATIONID, organisationId);
		params.put(GroupSetService.GET_PARAM_NAME, name);
		final List<GroupSetDO> groupSetList = groupSetService.get(params);
		return (groupSetList.size() == 1) ? groupSetList.get(0) : null;
	}
}
