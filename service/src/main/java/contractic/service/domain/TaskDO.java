package contractic.service.domain;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"user", "organisationId"})
public class TaskDO {
	private String id;
	private ValueDO person;
	private String description;
	private String title;
	private ValueDO status;
	private String created;
	private String parentNumber;
	private Map<String, Object> external;
	private ValueDO module;
	private String startDate;
	private String endDate;
	private String organisationId;
	private String number;
	private Boolean children;
	private ValueDO parent;
	private ValueDO repeat;
	private String repeatStart;
	private String repeatEnd;
	private UserDO user;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ValueDO getPerson() {
		return person;
	}
	public void setPerson(ValueDO person) {
		this.person = person;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String name) {
		this.title = name;
	}
	public ValueDO getStatus() {
		return status;
	}
	public void setStatus(ValueDO status) {
		this.status = status;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getParentNumber() {
		return parentNumber;
	}
	public void setParentNumber(String parentNumber) {
		this.parentNumber = parentNumber;
	}
	public Map<String, Object> getExternal() {
		return external;
	}
	public void setExternal(Map<String, Object> external) {
		this.external = external;
	}
	public ValueDO getModule() {
		return module;
	}
	public void setModule(ValueDO module) {
		this.module = module;
	}

	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}

	public Boolean getChildren() {
		return children;
	}
	public void setChildren(Boolean children) {
		this.children = children;
	}
	public ValueDO getParent() {
		return parent;
	}
	public void setParent(ValueDO parent) {
		this.parent = parent;
	}
	public ValueDO getRepeat() {
		return repeat;
	}
	public void setRepeat(ValueDO repeat) {
		this.repeat = repeat;
	}

	public String getRepeatStart() {
		return repeatStart;
	}
	public void setRepeatStart(String repeatStart) {
		this.repeatStart = repeatStart;
	}
	public String getRepeatEnd() {
		return repeatEnd;
	}
	public void setRepeatEnd(String repeatEnd) {
		this.repeatEnd = repeatEnd;
	}
	public UserDO getUser() {
		return user;
	}
	public void setUser(UserDO user) {
		this.user = user;
	}
}
