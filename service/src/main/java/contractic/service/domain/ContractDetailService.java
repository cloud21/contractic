package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Contract;
import contractic.model.data.store.ContractDetail;
import contractic.model.data.store.Group;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.service.util.Cache;
import contractic.service.util.ConcurrencyUtil;
import contractic.service.util.DateUtil;
import contractic.service.util.HashingUtil;
import contractic.service.util.SearchClient.SearchEntity;

public class ContractDetailService implements
		DomainService<ContractDetail, Long, ContractDetailDO, MessageDO> {

	private static final Logger logger = Logger
			.getLogger(ContractDetailService.class);

	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public static final String GET_PARAM_ID = "contract.id";
	public final static String CONTRACTDETAIL_IDCACHE_NAME = "contractdetail.idcache.name";
	public static final String GET_PARAM_SEARCH_QUERY = "search.query";
	public static final String GET_PARAM_CONTRACTID = "contract.id";
	public static final String USERID = "user.id";

	public static final String GET_PARAM_GROUPID = "group.id";
	public static final String GET_PARAM_GROUPNAME = "group.name";

	// This cache helps us hide database IDs from each request
	private Cache<String, Long> contractDetailIdCache;

	@Autowired
	private DomainService<Contract, Long, ContractDO, MessageDO> contractService;

	@Autowired
	private DomainService<Group, Long, GroupDO, MessageDO> groupService;

	@Autowired
	private CrudOperation<ContractDetail> contractDetailDAO;

	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;

	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;

	@Autowired
	private DomainService<SearchEntity, Long, SearchEntityDO, MessageDO> searchService;

	@Override
	public ContractDetail get(String hash) throws ContracticServiceException {
		final Long contractDetailId = this.contractDetailIdCache.get(hash);
		return get(contractDetailId);
	}

	@Override
	public ContractDetail get(Long id) throws ContracticServiceException {
		if (id == null)
			throw new ContracticServiceException("Null id supplied");

		ContractDetail contract = new ContractDetail();
		contract.setId(id);

		try {
			List<ContractDetail> contractList = contractDetailDAO
					.read(contract);
			contract = contractList.get(0);

			String hashId = HashingUtil.get(contract.getId());
			// Update the cache
			this.contractDetailIdCache.put(hashId, contract.getId());

			return contract;

		} catch (ContracticModelException | NoSuchAlgorithmException e) {
			throw new ContracticServiceException(
					String.format(
							"An error occured while retrieving contract with id %d",
							id));
		}
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.contractDetailIdCache = new Cache(CONTRACTDETAIL_IDCACHE_NAME,
					String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(
					String.format("Initializing %s service failed", this
							.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	private void saveSearchEntity(ContractDetail contractDetail) {
		ConcurrencyUtil.IOBoundExecutorService.submit(() -> {
			SearchEntityDO searchEntity = new SearchEntityDO();
			searchEntity.setId(contractDetail.getId());
			searchEntity.setCollection(Module.CONTRACTDETAIL);
			searchEntity.setOrganisationId(contractDetail.getContract()
					.getOrganisation().getId());

			String text = null;
			Group detailGroup = contractDetail.getGroup();

			if (detailGroup != null) {
				switch (detailGroup.getName()) {
				case ContractDetail.SUMMARY_GROUP_FIELD:
				case ContractDetail.CCN_GROUP_FIELD:
					text = String.join(", ",
							new String[] { contractDetail.getName(),
									contractDetail.getValue() });
					break;
				case ContractDetail.CONTRACT_TERM_PERIOD_GROUP_FIELD:
				case ContractDetail.CONTRACT_TERM_GOTO_MARKET_GROUP_FIELD:
				case ContractDetail.CONTRACT_TERM_NOTICE_GROUP_FIELD:
					return;
				}
			}

			/* if text has not been set then this is a bespoke contract detail */
			if (text == null) {
				text = String.join(
						", ",
						new String[] { contractDetail.getName(),
								contractDetail.getDescription(),
								contractDetail.getValue() });
			}
			searchEntity.setText(text);

			try {
				searchService.create(searchEntity);
			} catch (Exception e) {
				logger.warn(String.format(
						"An error occurred when indexing contract detail '%s'",
						String.join(
								", ",
								new String[] {
										Long.toString(contractDetail.getId()),
										contractDetail.getName(),
										contractDetail.getDescription(),
										contractDetail.getValue() }), e));
			}
		});
	}

	@Override
	public MessageDO update(ContractDetailDO dojo)
			throws ContracticServiceException {

		UserDO userDO = dojo.getUser();

		if (!StringUtils.hasText(dojo.getId())) {
			return new MessageDO(null, "Contractic",
					"Security data has been corrupted, can't continue",
					MessageDO.ERROR);
		}

		// Save the contract object
		try {

			// Construct the contract object
			ContractDetail contractDetail = this.get(dojo.getId());

			contractDetail.setName(dojo.getName());
			populateValue(dojo, contractDetail);
			populateDescription(dojo, contractDetail);
			populateCCN(dojo, contractDetail);
			populateExtension(dojo, contractDetail);
			populateTermPeriod(dojo, contractDetail);

			Group group = null;
			ValueDO groupValueDO = dojo.getGroup();
			if (groupValueDO != null
					&& StringUtils.hasText(groupValueDO.getValue())) {
				if (StringUtils.hasText(groupValueDO.getId()))
					group = groupService.get(groupValueDO.getId());
				else {
					// Create a group if it doesn't exist
					group = createGroup(contractDetail.getContract(),
							groupValueDO, userDO);
				}
			}

			contractDetail.setGroup(group);
			contractDetail.setLastUpdated(new Date());
			contractDetailDAO.update(contractDetail);

			String hashId = HashingUtil.get(contractDetail.getId());
			// Update the cache
			this.contractDetailIdCache.put(hashId, contractDetail.getId());

			saveSearchEntity(contractDetail);

		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"Failed to create contract detail: %s", dojo), e);
		}

		// final String icon, final String title, final String body, final
		// String type
		return new MessageDO(null, "Contractic",
				"Contract detail updated successfully", MessageDO.OK);
	}

	// Public static for testing
	public static void populateValue(ContractDetailDO source,
			ContractDetail destination) throws ContracticServiceException {
		ValueDO group = source.getGroup();
		if (group != null && group.getValue() != null) {
			String value = group.getValue();
			if (StringUtils.hasText(value)) {
				if (value.equals(ContractDetail.VALUE_GROUP_FIELD)) {
					ContractValueDO valueDO = source.getValue();
					if (valueDO == null) {
						MessageDO message = new MessageDO(
								"Invalid value specified", MessageDO.ERROR);
						throw new ContracticServiceException(message.getBody(),
								message);
					}

					try {
						String projected = valueDO.getProjected();
						if (!StringUtils.hasText(projected))
							throw new NullPointerException(
									"Project value supplied is null");
						Double.parseDouble(valueDO.getProjected());
					} catch (NullPointerException | NumberFormatException nfe) {
						MessageDO message = new MessageDO(
								"Invalid projected value supplied",
								MessageDO.ERROR);
						throw new ContracticServiceException(message.getBody(),
								message);
					}

					String actual = valueDO.getActual();
					if (StringUtils.hasText(actual)) {
						try {
							Double.parseDouble(actual);
						} catch (NullPointerException | NumberFormatException nfe) {
							MessageDO message = new MessageDO(
									"Invalid actual value supplied",
									MessageDO.ERROR);
							throw new ContracticServiceException(
									message.getBody(), message);
						}
					}

					String valueXML = ContractValueDO.toXml(valueDO);
					destination.setValue(valueXML);
					return;

				} else if (value
						.equals(ContractDetail.CONTRACT_TERM_PERIOD_GROUP_FIELD)) {
					Date date = DateUtil.tryParse(value);
					if (date == null) {
						MessageDO message = new MessageDO(String.format(
								"Failed to parse date '%s'", value),
								MessageDO.ERROR);
						throw new ContracticServiceException(message.getBody(),
								message);
					}
					destination.setValue(date.toString());
					return;
				}
			}
		}
		destination.setValue(source.getText());
	}

	// Public static for testing
	public static void populateValue(ContractDetail source,
			ContractDetailDO destination) {
		final Group group = source.getGroup();
		if (group != null && group.getName() != null) {
			String name = group.getName();
			if (name.equals(ContractDetail.VALUE_GROUP_FIELD)) {
				ContractValueDO valueDO = ContractValueDO.fromXml(source
						.getValue());
				destination.setValue(valueDO);
				return;
			}
		}
		destination.setText(source.getValue());
	}

	public static void populateDescription(ContractDetailDO source,
			ContractDetail destination) {
		ValueDO group = source.getGroup();
		if (group != null) {
			String value = group.getValue();
			if (StringUtils.hasText(value)) {
				if (value.equals(ContractDetail.VALUE_GROUP_FIELD)) {
					String valueXML = PeriodDO.toXml(source.getPeriod());
					destination.setDescription(valueXML);
					return;
				}
			}
		}
		destination.setDescription(source.getDescription());
	}

	public static void populateDescription(ContractDetail source,
			ContractDetailDO destination) {
		final Group group = source.getGroup();
		final String description = source.getDescription();
		if (group != null && group.getName() != null && description != null) {
			if (group.getName().equals(ContractDetail.VALUE_GROUP_FIELD)) {
				PeriodDO valueDO = PeriodDO.fromXml(source.getDescription());
				destination.setPeriod(valueDO);
				return;
			}
		}
		destination.setDescription(source.getDescription());
	}

	private void populateTermPeriod(ContractDetailDO source,
			ContractDetail destination) {
		final ValueDO group = source.getGroup();
		if (group == null || group.getValue() == null)
			return;
		String value = group.getValue();
		if (value.equals(ContractDetail.CONTRACT_TERM_PERIOD_GROUP_FIELD)
				|| value.equals(ContractDetail.CONTRACT_TERM_NOTICE_GROUP_FIELD)
				|| value.equals(ContractDetail.CONTRACT_TERM_GOTO_MARKET_GROUP_FIELD)) {
			ContractTermPeriodDO termPeriod = source.getTerm();
			String propertiesXml = ContractTermPeriodDO.Properties
					.toXml(termPeriod.getProperties());
			destination.setDescription(propertiesXml);
			destination.setName(termPeriod.getNumber().toString());
			destination.setValue(DateUtil.toString(
					DateUtil.tryParse(termPeriod.getStart()),
					"yyyy-MM-dd HH:mm:ss"));
		}
	}

	private void populateTermPeriod(ContractDetail source,
			ContractDetailDO destination) {
		final Group group = source.getGroup();
		final String description = source.getDescription();
		if (group == null || group.getName() == null)
			return;
		String value = group.getName();
		if (value.equals(ContractDetail.CONTRACT_TERM_PERIOD_GROUP_FIELD)
				|| value.equals(ContractDetail.CONTRACT_TERM_NOTICE_GROUP_FIELD)
				|| value.equals(ContractDetail.CONTRACT_TERM_GOTO_MARKET_GROUP_FIELD)) {
			ContractTermPeriodDO contractTerm = new ContractTermPeriodDO();
			ContractTermPeriodDO.Properties propertiesDO = ContractTermPeriodDO.Properties
					.fromXml(description);
			contractTerm.setProperties(propertiesDO);
			Date start = DateUtil.tryParse(source.getValue());
			contractTerm.setStart(Long.toString(start.getTime()));
			contractTerm.setNumber(new Integer(source.getName()));
			destination.setTerm(contractTerm);
			destination.setName(null);
		}
	}

	private void populateCCN(ContractDetailDO source, ContractDetail destination) {
		final ContractCCNDO ccnDO = source.getCcn();
		if (ccnDO == null)
			return;
		final ValueDO group = source.getGroup();
		if (group == null || group.getValue() == null)
			return;
		String groupValue = group.getValue();
		if (groupValue.equals(ContractDetail.CCN_GROUP_FIELD)) {

			destination.setName(ccnDO.getReference());
			destination.setValue(ccnDO.getValue());
			String valueXML = PeriodDO.toXml(ccnDO.getPeriod());
			destination.setDescription(valueXML);

		}
	}

	private void populateCCN(ContractDetail source, ContractDetailDO destination) {
		final Group group = source.getGroup();
		final String description = source.getDescription();
		if (group == null || group.getName() == null)
			return;
		if (group.getName().equals(ContractDetail.CCN_GROUP_FIELD)) {
			ContractCCNDO ccnDO = new ContractCCNDO();
			PeriodDO periodDO = description == null ? new PeriodDO() : PeriodDO
					.fromXml(description);
			if (periodDO.getFrom() != null)
				periodDO.setFrom(Long.toString(DateUtil.tryParse(
						periodDO.getFrom()).getTime()));
			if (periodDO.getTo() != null)
				periodDO.setTo(Long.toString(DateUtil
						.tryParse(periodDO.getTo()).getTime()));
			ccnDO.setReference(source.getName());
			ccnDO.setValue(source.getValue());
			ccnDO.setPeriod(periodDO);
			destination.setCcn(ccnDO);
			/* Clear name and description as we don't need them */
			destination.setName(null);
			destination.setDescription(null);
			destination.setText(null);
		}
	}

	private void populateExtension(ContractDetailDO source,
			ContractDetail destination) throws ContracticServiceException {
		final ValueDO group = source.getGroup();
		if (group == null || group.getValue() == null)
			return;
		String groupValue = group.getValue();
		if (groupValue.equals(ContractDetail.EXTENSION_GROUP_FIELD)) {

			ContractExtensionDO extensionDO = source.getExtension();

			if (extensionDO == null) {
				MessageDO message = new MessageDO(
						"Invalid extension detail supplied", MessageDO.ERROR);
				throw new ContracticServiceException(message.getBody(), message);
			}

			PeriodDO extensionPeriod = extensionDO.getPeriod();
			PeriodDO noticePeriod = extensionDO.getNotice();

			/*
			 * if(extensionDO.getNumber()==null){ MessageDO message = new
			 * MessageDO("Invalid extension number", MessageDO.ERROR); throw new
			 * ContracticServiceException(message.getBody(), message); }
			 */

			if (extensionPeriod == null) {
				MessageDO message = new MessageDO(
						"Invalid extension period supplied", MessageDO.ERROR);
				throw new ContracticServiceException(message.getBody(), message);
			}

			if (noticePeriod == null) {
				MessageDO message = new MessageDO(
						"Invalid notice period supplied", MessageDO.ERROR);
				throw new ContracticServiceException(message.getBody(), message);
			}

			if (extensionDO.getNumber() != null)
				destination.setName(extensionDO.getNumber().toString());
			String periodXML = PeriodDO.toXml(extensionPeriod);
			destination.setValue(periodXML);
			String noticeXML = PeriodDO.toXml(noticePeriod);
			destination.setDescription(noticeXML);
		}
	}

	private void populateExtension(ContractDetail source,
			ContractDetailDO destination) {
		final Group group = source.getGroup();
		final String description = source.getDescription();
		final String value = source.getValue();
		if (group == null || group.getName() == null)
			return;
		if (group.getName().equals(ContractDetail.EXTENSION_GROUP_FIELD)) {
			ContractExtensionDO extensionDO = new ContractExtensionDO();
			PeriodDO periodDO = description == null ? new PeriodDO() : PeriodDO
					.fromXml(description);
			if (periodDO.getFrom() != null)
				periodDO.setFrom(Long.toString(DateUtil.tryParse(
						periodDO.getFrom()).getTime()));
			if (periodDO.getTo() != null)
				periodDO.setTo(Long.toString(DateUtil
						.tryParse(periodDO.getTo()).getTime()));
			extensionDO.setNumber(new Integer(source.getName()));
			extensionDO.setPeriod(periodDO);

			PeriodDO noticeDO = value == null ? new PeriodDO() : PeriodDO
					.fromXml(value);
			if (noticeDO.getFrom() != null)
				noticeDO.setFrom(Long.toString(DateUtil.tryParse(
						noticeDO.getFrom()).getTime()));
			if (noticeDO.getTo() != null)
				noticeDO.setTo(Long.toString(DateUtil
						.tryParse(noticeDO.getTo()).getTime()));
			extensionDO.setNotice(noticeDO);

			destination.setExtension(extensionDO);
			/* Clear name and description as we don't need them */
			destination.setName(null);
			destination.setDescription(null);
			destination.setText(null);
		}
	}

	@Override
	public MessageDO create(ContractDetailDO dojo)
			throws ContracticServiceException {
		// Construct the contract object
		final ContractDetail contractDetail = new ContractDetail();

		UserDO userDO = dojo.getUser();

		final Contract contract = contractService.get(dojo.getContract()
				.getId());
		Group group = null;

		ValueDO groupValueDO = dojo.getGroup();
		if (groupValueDO != null && StringUtils.hasText(groupValueDO.getId()))
			group = groupService.get(groupValueDO.getId());

		if (groupValueDO != null && !StringUtils.hasText(groupValueDO.getId())
				&& StringUtils.hasText(groupValueDO.getValue())) {
			// Create a group if it doesn't exist
			group = createGroup(contract, groupValueDO,
					userDO);
		}

		contractDetail.setName(dojo.getName());
		
		logger.info("Populating Value");
		populateValue(dojo, contractDetail);
		logger.info("Populating Description");
		populateDescription(dojo, contractDetail);
		logger.info("Populating CCN");
		populateCCN(dojo, contractDetail);
		// populateExtension(dojo, contractDetail);
		logger.info("Populating Term Period");
		populateTermPeriod(dojo, contractDetail);

		contractDetail.setContract(contract);
		contractDetail.setGroup(group);
		contractDetail.setLastUpdated(new Date());

		ValueDO returnValue = new ValueDO();
		// Save the person object
		try {
			logger.info("Creating contract detail");
			contractDetailDAO.create(contractDetail);

			String hashId = HashingUtil.get(contractDetail.getId());
			// Update the cache
			this.contractDetailIdCache.put(hashId, contractDetail.getId());
			returnValue.setId(hashId);
			dojo.setId(hashId);

			logger.info("Indexing contract detail for search");
			saveSearchEntity(contractDetail);

		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"Failed to create contract detail: %s", dojo), e);
		}

		// final String icon, final String title, final String body, final
		// String type
		MessageDO message = new MessageDO(null, "Contractic",
				"Contract detail created successfully", MessageDO.OK);
		message.setRef(returnValue);
		return message;
	}

	private Group createGroup(final Contract contract, ValueDO groupValueDO,
			UserDO userDO) throws ContracticServiceException {
		Group group;
		GroupDO groupDO = new GroupDO();
		groupDO.setName(groupValueDO.getValue().trim());
		groupDO.setOrganisationId(userDO.getOrganisationId());

		final Map<String, Object> params = new HashMap<>();

		params.put(ModuleService.GET_PARAM_NAME, Module.CONTRACT);
		List<ModuleDO> moduleList = moduleService.get(params);
		assert (moduleList.size() == 1);
		ModuleDO moduleDO = moduleList.get(0);

		groupDO.setModuleId(moduleDO.getId());
		groupDO.setActive(true);

		// If the group already exists, an exception is thrown which is then
		// swallowed here with a warning in the logs
		try {
			groupService.create(groupDO);
		} catch (ContracticServiceException cse) {
			logger.warn(String.format("Failed to create group from name '%s'",
					groupDO.getName()), cse);
		}

		// Now lets get the group, if it didn't exist, it would have been
		// created above
		params.clear();
		params.put(GroupService.GET_PARAM_NAME, groupDO.getName());
		params.put(CommonService.USER,
				userDO);
		params.put(GroupService.GET_PARAM_MODULEID, groupDO.getModuleId());

		List<GroupDO> groupList = groupService.get(params);
		assert (groupList.size() == 1);

		group = groupService.get(groupList.get(0).getId());
		return group;
	}

	private String getGroupName(Group group) {
		return group == null ? "" : group.getName();
	}

	@Override
	public List<ContractDetailDO> get(Map<String, Object> params)
			throws ContracticServiceException {
		final String hash = (String) params.get(GET_PARAM_HASHID);
		final String contractId = (String) params.get(GET_PARAM_CONTRACTID);
		final String groupId = (String) params.get(GET_PARAM_GROUPID);
		final String groupName = (String) params.get(GET_PARAM_GROUPNAME);

		final UserDO userDO = (UserDO) params.get(CommonService.USER);
		if (userDO == null)
			throw new ContracticServiceException(
					"Invalid session user provided");
		final String organisationId = userDO.getOrganisationId();

		if (!StringUtils.hasText(organisationId)) {
			throw new ContracticServiceException("OrganisationId not supplied");
		}

		// Retrieve model objects
		List<ContractDetail> contractDetails = null;
		try {
			ContractDetail contractDetail = new ContractDetail();

			if (StringUtils.hasText(groupId)) {
				Group group = groupService.get(groupId);
				contractDetail.setGroup(group);
			}

			// Ensure we haven't set the group before
			/*
			 * In order to be able to use regular expressions, we do all group
			 * filtering at code level rather than at DB level
			 */
			/*
			 * if(contractDetail.getGroup()==null &&
			 * StringUtils.hasText(groupName)){ Map<String, Object> params1 =
			 * new HashMap<>();
			 * 
			 * params1.put(ModuleService.GET_PARAM_NAME, "Contract");
			 * List<ModuleDO> moduleList = moduleService.get(params);
			 * assert(moduleList.size() == 1); ModuleDO moduleDO =
			 * moduleList.get(0);
			 * 
			 * params1.clear();
			 * params1.put(GroupService.GET_PARAM_ORGANISATIONID,
			 * organisationId); params1.put(GroupService.GET_PARAM_MODULEID,
			 * moduleDO.getId()); params1.put(GroupService.GET_PARAM_NAME,
			 * groupName);
			 * 
			 * List<GroupDO> groupList = groupService.get(params1);
			 * assert(groupList.size() == 1); Group group =
			 * groupService.get(groupList.get(0).getId());
			 * contractDetail.setGroup(group); }
			 */

			if (StringUtils.hasText(hash)) {
				Long id = this.contractDetailIdCache.get(hash);
				contractDetail.setId(id);
				;
			}

			if (StringUtils.hasText(contractId)) {
				Contract contract = contractService.get(contractId);
				contractDetail.setContract(contract);
			}

			contractDetails = contractDetailDAO.read(contractDetail);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(
					"An error occured while attempting to read contract details",
					e);
		}

		// Transform to domain object(s) to be passed to controller

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();

		final List<ContractDetailDO> pojoList = contractDetails
				.stream()
				.parallel()
				.filter(detail -> {

					// If we have the detail id, we don't perform any filtering
					if (StringUtils.hasText(hash))
						return true;

					/*
					 * if the groupIs has been set, we don't need to perform the
					 * filter
					 */
					if (StringUtils.hasText(groupId)) {
						return true;
					}
					if (!StringUtils.hasText(groupName)) {
						return detail.getGroup() == null;
					}

					// Group name has been provided so we only need those detail
					// records
					// with group names
					// We use this utility function in order to return empty ""
					// string in order to include null Groups as having ""
					return getGroupName(detail.getGroup()).matches(groupName);

				})
				.map(contract -> {
					ContractDetailDO contractDO = null;
					try {
						contractDO = makeDO(contract, userDO);
						// Side effect 1
						contractDetailIdCache.put(contractDO.getId(),
								contract.getId());

					} catch (ContracticServiceException
							| NoSuchAlgorithmException ex) {
						cse.add(new ContracticServiceException(
								String.format(
										"An error occured when hashing the contract %s",
										contract), ex));
					}

					return contractDO;
				}).collect(Collectors.toList());

		if (cse.size() != 0)
			throw cse.get(0);

		return pojoList;
	}

	private ContractDetailDO makeDO(ContractDetail contractDetail, UserDO userDO)
			throws ContracticServiceException, NoSuchAlgorithmException {
		String hashId = HashingUtil.get(contractDetail.getId());
		ContractDetailDO contractDetailDO = new ContractDetailDO();

		contractDetailDO.setId(hashId);
		contractDetailDO.setName(contractDetail.getName());
		contractDetailDO.setContract(new ContractDO());

		populateValue(contractDetail, contractDetailDO);
		populateDescription(contractDetail, contractDetailDO);
		populateCCN(contractDetail, contractDetailDO);
		populateExtension(contractDetail, contractDetailDO);
		populateTermPeriod(contractDetail, contractDetailDO);

		// Get the organisation
		final Map<String, Object> params = new HashMap<>();

		String organisationId = userDO.getOrganisationId();

		// Get and populate the group
		final Group detailGroup = contractDetail.getGroup();
		if (detailGroup != null) {
			params.clear();
			params.put(CommonService.USER, userDO);
			params.put(GroupService.GET_PARAM_ID, contractDetail.getGroup()
					.getId());
			final List<GroupDO> groupList = groupService.get(params);
			assert (groupList.size() == 1);
			GroupDO groupDO = groupList.get(0);
			contractDetailDO.setGroup(new ValueDO(groupDO.getId(), groupDO
					.getName()));
		}

		return contractDetailDO;
	}

	public MessageDO delete(final ContractDetailDO pojo)
			throws ContracticServiceException {
		Long contractId = this.contractDetailIdCache.get(pojo.getId());

		if (contractId == null) {
			// final String icon, final String title, final String body, final
			// String type
			return new MessageDO(null, "Internal system error",
					"Invalid contract (may have been deleted)",
					MessageDO.WARNING);
		}

		final ContractDetail contractDetail = get(pojo.getId());

		try {
			// Delete from data source
			contractDetailDAO.delete(contractDetail);

			// if successfull, remove from Id cache
			this.contractDetailIdCache.remove(pojo.getId());

			return new MessageDO(null, "", String.format(
					"ContractDetail '%s' deleted successfully!",
					contractDetail.getName()), MessageDO.OK);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(
					String.format(
							"An error occurred when performing delete operation on contract object %s",
							contractDetail), e);
		}
	}

}
