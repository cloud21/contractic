package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"organisationId", "user"})
public class GroupObjectDO {
	
	private String id;
	private ValueDO group;
	private ValueDO module;
	private ValueDO external;
	private String active;
	private UserDO user;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public ValueDO getGroup() {
		return group;
	}
	public void setGroup(ValueDO group) {
		this.group = group;
	}
	public ValueDO getModule() {
		return module;
	}
	public void setModule(ValueDO module) {
		this.module = module;
	}
	public ValueDO getExternal() {
		return external;
	}
	public void setExternal(ValueDO external) {
		this.external = external;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public UserDO getUser() {
		return user;
	}
	public void setUser(UserDO user) {
		this.user = user;
	}
	
	
}
