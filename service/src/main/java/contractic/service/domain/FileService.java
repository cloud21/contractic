package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import contractic.model.data.store.Contract;
import contractic.service.util.Cache;
import contractic.service.util.EhcacheUtil;
import contractic.service.util.HashingUtil;

/**
 * Service responsible for serving document files
 * @author Michael Sekamanya
 *
 */
public class FileService implements DomainService<FileDO, Long, FileDO, MessageDO> {

	public final static String DOCUMENTTYPE_IDCACHE_NAME = "file.idcache.name";
	
	private Cache<String, FileDO> fileIdCache;
	
	public FileDO get(String hash) throws ContracticServiceException{
		return fileIdCache.get(hash);
	}
	
	/**
	 * 
	 * @param file
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws ContracticServiceException
	 * @throws InterruptedException
	 */
	public FileDO get(FileDO file) throws  ContracticServiceException{
		
		/*This routing should be looking up a given file in a bi-directional cache. Currently this implementation allows for multiple
		 * instances of the file to be stored in the fileCache*/
		
		Long id;
		try {
			id = generateFileId();
			
			String hash = HashingUtil.get(id);
			this.fileIdCache.put(hash, file);
			
			file.setId(hash);
			
			return file;
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Error getting fileDO %s", file), e);
		}

	}	
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.fileIdCache = new Cache(DOCUMENTTYPE_IDCACHE_NAME, String.class, FileDO.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	public MessageDO delete(FileDO documentDO) throws ContracticServiceException {
		final String hash = documentDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Invalid document hash id provided");
			
		final FileDO file = fileIdCache.get(hash);	
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", "File updated successfully", MessageDO.OK);
	}

	public MessageDO update(DocumentDO documentDO) throws ContracticServiceException {
		return null;
	}


	private synchronized Long generateFileId() throws InterruptedException{
		Long id = System.currentTimeMillis();
		Thread.sleep(1L);
		return id;
	}
	
	public MessageDO create(final FileDO fileDO) throws ContracticServiceException {

		String hash = null;
		try{
			Long id = generateFileId();
			hash = HashingUtil.get(id);
			this.fileIdCache.put(hash, fileDO);
		} catch (NoSuchAlgorithmException | InterruptedException e) {
			throw new ContracticServiceException(String.format("Failed to create the file %s", fileDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		MessageDO messageDO = new MessageDO(null, "Contractic", "", MessageDO.OK);
		messageDO.setRef(hash);
		
		return messageDO;
	}

	@Override
	public FileDO get(Long id) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageDO update(FileDO file) throws ContracticServiceException {
		/*This routing should be looking up a given file in a bi-directional cache. Currently this implementation allows for multiple
		 * instances of the file to be stored in the fileCache*/
		
		Long id;
		try {
			id = generateFileId();
			
			String hash = HashingUtil.get(id);
			this.fileIdCache.put(hash, file);
			
			file.setId(hash);
			
			return null;
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Error getting fileDO %s", file), e);
		}
	}

	@Override
	public List<FileDO> get(Map<String, Object> params)
			throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}
