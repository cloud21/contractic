package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Address;
import contractic.model.data.store.AddressType;
import contractic.model.data.store.Contact;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Supplier;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class ContactService implements DomainService<Contact, Long, ContactDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	public final static String GET_PARAM_ID = "id";
	public static final String GET_PARAM_MODULE = "module";
	public static final String GET_PARAM_EXTERNALID = "externalId";
	public static final String USERID = "user.id";

	public final static String CONTACT_IDCACHE_NAME = "contact.idcache.name";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	
	/**
	 * This helps us to find a contact by a given address
	 */
	protected static final Object GET_PARAM_ADDRESSID = "addressId";
	public static final String GET_PARAM_SEARCH_QUERY = "query";
	public static final String GET_PARAM_TYPEID = "typeId";
	public static final String GET_PARAM_ACTIVE = "active";
	public static final String GET_PARAM_MODULEID = "module.id";

	private Cache<String, Long> contactIdCache;

	@Autowired
	private CrudOperation<Contact> contactDAO;
	@Autowired
	private DomainService<AddressType, Long, StaticDO, MessageDO> addressTypeService;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	@Autowired
	private DomainService<Address, Long, AddressDO, MessageDO> addressService;
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	@Autowired
	private DomainService<Supplier, Long, SupplierDO, MessageDO> supplierService;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private CommonService commonService;

	/**
	 * Updates the contact supplied. Note we currently only update the details of the underlying address.
	 * @param contactDO
	 * @return
	 * @throws ContracticServiceException
	 * @throws ContracticModelException 
	 */
	public MessageDO update(final ContactDO contactDO) throws ContracticServiceException {
		
		// Get the contact object
		Contact contact = get(contactDO.getId());
		
		ValueDO addressValueDO = contactDO.getAddress();
		if(StringUtils.hasText(addressValueDO.getId())){
			Address address = addressService.get(addressValueDO.getId());
			contact.setAddress(address);
		}else if(StringUtils.hasText(addressValueDO.getValue())){
			AddressDO addressDO = new AddressDO();
			addressDO.setSection1(contactDO.getValue());
			addressDO.setTypeId(contactDO.getTypeId());
			addressDO.setOrganisationId(contactDO.getOrganisationId());
			addressService.create(addressDO);
			Address address = addressService.get(addressDO.getId());
			contact.setAddress(address);
		}
		
		try {
			contactDAO.update(contact);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update contact: %s", contactDO), e);
		}
		
		return new MessageDO(null, "Contractic", String.format("Contact '%s' updated successfully", contactDO.getValue()), MessageDO.OK);
	}
	
	public MessageDO create(final ContactDO contactDO) throws ContracticServiceException {

		Contact contact = new Contact();

		// Get the module for this contact
		final Module module = moduleService.get(contactDO.getModuleId());
		contact.setModule(module);
		
		ValueDO addressValueDO = contactDO.getAddress();
		if(StringUtils.hasText(addressValueDO.getId())){
			Address address = addressService.get(addressValueDO.getId());
			contact.setAddress(address);
		}else if(StringUtils.hasText(addressValueDO.getValue())){
			AddressDO addressDO = new AddressDO();
			addressDO.setSection1(addressValueDO.getValue());
			addressDO.setTypeId(contactDO.getType().getId());
			addressDO.setOrganisationId(contactDO.getOrganisationId());
			MessageDO result = addressService.create(addressDO);
			if(result.getType().equals(MessageDO.ERROR))
				throw new ContracticServiceException(String.format("Could not create address '%s'", result.getBody()));
			Address address = addressService.get(addressDO.getId());
			contact.setAddress(address);
		}
						
		// Set the externalId
		final Long externalId = commonService.getExternalId(contactDO.getExternalId(), module);
		
		contact.setExternalId(externalId);
		ValueDO returnValueDO = new ValueDO();
		// Now save the contact
		try {
			contactDAO.create(contact);
			
			// Now updat the ID cache
			String hashId = HashingUtil.get(contact.getId());
			contactDO.setId(hashId);
			this.contactIdCache.put(hashId, contact.getId());
			returnValueDO.setId(hashId);
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create the contact %s", contactDO), e);
		}
			
		MessageDO message = new MessageDO(null, "Contractic", String.format("Contact '%s' created successfully", contactDO.getValue()), MessageDO.OK);
		message.setRef(returnValueDO);
		return message;
		
	}

	public Contact get(final String hash) throws ContracticServiceException {
		final Long id = this.contactIdCache.get(hash);
		return get(id);
	}

	public Contact get(final Long id) throws ContracticServiceException {
		if (id == null)
			throw new ContracticServiceException("Null contact id supplied");

		Contact contact = new Contact();
		contact.setId(id);

		try {
			List<Contact> types = contactDAO.read(contact);
			return types.get(0);

		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"An error occured while retrieving contact with id %d", id));
		}
	}

	private ContactDO makeDO(Contact contact, String organisationId) throws NoSuchAlgorithmException,
			ContracticServiceException {
		ContactDO contactDO = new ContactDO();

		String hashId = HashingUtil.get(contact.getId());

		// Set the contact hash key
		contactDO.setId(hashId);
		contactDO.setValue(contact.getAddress().getSection1());
		
		// Set the address type hash key
		final Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(AddressTypeService.GET_PARAM_NAME, contact.getAddress()
				.getType().getName());

		List<StaticDO> addressTypeList = addressTypeService.get(paramMap);
		assert (addressTypeList.size() == 1);
		StaticDO addressType = addressTypeList.get(0);
		contactDO.setTypeId(addressTypeList.get(0).getId());
		contactDO.setType(new ValueDO(addressType.getId(), addressType.getValue()));
		
		paramMap.clear();
		paramMap.put(AddressService.GET_PARAM_ORGANISATIONID, organisationId);
		paramMap.put(AddressService.GET_PARAM_ID, contact.getAddress().getId());
		List<AddressDO> addressList = addressService.get(paramMap);
		assert addressList.size() == 1;
		AddressDO address = addressList.get(0);
		contactDO.setAddress(new ValueDO(address.getId(), address.stringify()));
		
		// Set the module hash key
		paramMap.clear();
		paramMap.put(ModuleService.GET_PARAM_NAME, contact.getModule().getName());
		List<ModuleDO> moduleList = moduleService.get(paramMap);
		contactDO.setModuleId(moduleList.get(0).getId());
		
		// Set the external ref
//		String externalId = null;
//		if(contact.getModule().getName().equals("Person")){
//			PersonDO personDO = personService.get(contact.getExternalId());
//			externalId = personDO.getId();
//			contactDO.setExternalId(externalId);
//		}
		

		return contactDO;
	}

	public List<ContactDO> get(final Map<String, Object> params)
			throws ContracticServiceException {

		final String name = (String) params.get(GET_PARAM_NAME);
		final Long id = (Long) params.get(GET_PARAM_ID);
		Module module = (Module)params.get(GET_PARAM_MODULE);
		String moduleId = (String)params.get(GET_PARAM_MODULEID);
		final String externalIdString = (String)params.get(GET_PARAM_EXTERNALID);
		final String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);
		final String addressId = (String)params.get(GET_PARAM_ADDRESSID);
		final String searchQuery = (String)params.get(GET_PARAM_SEARCH_QUERY);

		if(!StringUtils.hasText(organisationId)){
			throw new ContracticServiceException("OrganisationId not supplied");
		}
		
		final Organisation organisation = organisationService.get(organisationId);
		
		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
		
		final List<Contact> contacts = new Vector<>();
		try {
			Contact contact = new Contact();

			contact.setModule(module);
			if(StringUtils.hasText(moduleId)){
				module = moduleService.get(moduleId);
				contact.setModule(module);
			}
			
			if(module!=null && StringUtils.hasText(externalIdString)){
				Long externalId = commonService.getExternalId(externalIdString, module);
				contact.setExternalId(externalId);
			}
			
			if(addressId != null){
				Address address = addressService.get(addressId);
				contact.setAddress(address);
			}else{
				Address address = new Address();
				address.setOrganisation(organisation);
				contact.setAddress(address);
			}
			
			if(StringUtils.hasText(searchQuery)){
				Map<String, Object> sparams = new HashMap<>();
				sparams.put(AddressService.GET_PARAM_ORGANISATIONID, organisationId);
				sparams.put(AddressService.GET_PARAM_SEARCH_QUERY, searchQuery);
				List<AddressDO> addressDOList = addressService.get(sparams);
						addressDOList.parallelStream().forEach(address -> {
							try{
								final Address addr = addressService.get(address.getId());
								contact.setAddress(addr);
								List<Contact> conts = contactDAO.read(contact);
								contacts.addAll(conts);
							} catch (ContracticModelException e){
								cse.add(new ContracticServiceException("Failed to read address " + address, e));
							} catch (ContracticServiceException e) {
								cse.add(e);
							}
						});
			}else{
				List<Contact> conts = contactDAO.read(contact);
				contacts.addAll(conts);
			}
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(
					"An error occured while attempting to read contacts", e);
		}

		if(cse.size() > 0)
			throw cse.get(0);
		
		final List<ContactDO> pojoList = contacts
				.stream()
				.map(contact -> {

					ContactDO contactDO = null;
					try {
						contactDO = makeDO(contact, organisationId);
						// Side effect 1
						contactIdCache.put(contactDO.getId(), contact.getId());
					} catch (NoSuchAlgorithmException | ContracticServiceException ex) {
						cse.add(new ContracticServiceException(String.format(
								"An error occured when hashing the contact %s",
								contact), ex));
					}

					return contactDO;
				}).collect(Collectors.toList());

		if (cse.size() != 0)
			throw cse.get(0);

		return pojoList;
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.contactIdCache = new Cache(CONTACT_IDCACHE_NAME,
					String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(
					String.format("Initializing %s service failed", this
							.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	@Override
	public MessageDO delete(ContactDO contactDO) throws ContracticServiceException {
		Contact contact = get(contactDO.getId());
		try {
			contactDAO.delete(contact);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("An error occured when deleting contact object %s", contactDO), e);
		}
		
		return new MessageDO(null, "Contractic", String.format("Contact '%s' deleted successfully", contactDO.getValue()), MessageDO.OK);
	}
	
	public static void saveObjectContact( 
			CrudOperation<Contact> contactDAO, final Long externalId, Module module, Contact[] contact) throws ContracticServiceException, ContracticModelException{
		if(contact == null || contact.length==0)
			return;
		Contact contactObject = new Contact();
		contactObject.setModule(module);
		contactObject.setExternalId(externalId);
		// Set the addess here just so we can constrain our queries to the one organisatio
		contactObject.setAddress(contact[0].getAddress());
		
		List<Contact> groupObjectList = contactDAO.read(contactObject);
		List<ContracticServiceException> cse = new ArrayList<>();
		groupObjectList.stream().forEach(gO -> {
			try {
				contactDAO.delete(gO);
			} catch (Exception e) {
				cse.add(new ContracticServiceException(String.format("An error occured when deleting contact object %s", gO), e));
			}
		});
		if(cse.size() > 0)
			throw cse.get(0);
		
		Arrays.stream(contact).forEach((gO -> {
			try {
				gO.setModule(module);
				gO.setExternalId(externalId);
				contactDAO.create(gO);
			} catch (Exception e) {
				cse.add(new ContracticServiceException(String.format("An error occured when saving contact object %s", gO), e));
			}
		}));
		if(cse.size() > 0)
			throw cse.get(0);
	}

}
