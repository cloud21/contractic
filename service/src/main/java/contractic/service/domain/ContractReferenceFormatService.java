package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.ContractReferenceFormat;
import contractic.model.data.store.ContractType;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.UserRolePermissionAction;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class ContractReferenceFormatService implements DomainService<ContractReferenceFormat, Long, ContractReferenceFormatDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	private final static String CONTRACTREFERENCEFORMAT_IDCACHE_NAME = "contracttype.idcache.name";
	public static final String GET_PARAM_ID = "contracttype.id";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public static final String USERID = "user.id";
	
	public final static Pattern ContractReferencePattern = Pattern.compile("([#&]+)+");
	
	private Cache<String, Long> contractReferenceFormatIdCache;
	
	@Autowired
	private CommonService commonService;
	@Autowired
	private CrudOperation<ContractReferenceFormat> contractReferenceFormatDAO;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<ContractType, Long, ContractTypeDO, MessageDO> contractTypeService;
	

	public ContractReferenceFormat get(final String uuid) throws ContracticServiceException{
		final Long id = this.contractReferenceFormatIdCache.get(uuid);
		return get(id);
	}
	
	public ContractReferenceFormat get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null contract reference id supplied");
		
		ContractReferenceFormat contractReferenceFormat = new ContractReferenceFormat();
		contractReferenceFormat.setId(id);
		
		try {
			List<ContractReferenceFormat> types = contractReferenceFormatDAO.read(contractReferenceFormat);
			return types.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving contract reference with id %d", id));
		}
	}
	
	
	private ContractReferenceFormatDO makeDO(final ContractReferenceFormat contractReferenceFormat, UserDO userDO) throws NoSuchAlgorithmException, ContracticServiceException{
		String hashId = HashingUtil.get(contractReferenceFormat.getId());
		ContractReferenceFormatDO contractReferenceFormatDO = new ContractReferenceFormatDO();
		
		contractReferenceFormatDO.setId(hashId);
		contractReferenceFormatDO.setName(contractReferenceFormat.getName());
		contractReferenceFormatDO.setDescription(contractReferenceFormat.getDescription());
		contractReferenceFormatDO.setActive(contractReferenceFormat.getActive());
		contractReferenceFormatDO.setDifault(contractReferenceFormat.getDefault());
		contractReferenceFormatDO.setFormat(contractReferenceFormat.getFormat());
		
		ContractType contractType = contractReferenceFormat.getContractType();
		if(contractType != null){
			Map<String, Object> params = new HashMap<>();
			params.put(ContractTypeService.GET_PARAM_ID, contractReferenceFormat.getContractType().getId());
			params.put(ContractTypeService.GET_PARAM_ORGANISATIONID, userDO.getOrganisationId());
			params.put(CommonService.USER, userDO);
			List<ContractTypeDO> list = contractTypeService.get(params);
			assert(list.size() == 1);
			ContractTypeDO contractTypeDO = list.get(0);
			contractReferenceFormatDO.setContractType(new ValueDO(contractTypeDO.getId(), contractTypeDO.getName()));
		}
		
		return contractReferenceFormatDO;
	}
	
	public List<ContractReferenceFormatDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASH);
		 final String name = (String)params.get(GET_PARAM_NAME);
		 Long id = (Long)params.get(GET_PARAM_ID);
		 String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);

		if(!StringUtils.hasText(organisationId)){
			throw new ContracticServiceException("OrganisationId not supplied");
		}
	 
		UserDO userDO = (UserDO)params.get(CommonService.USER);
		if(userDO == null)
			throw new ContracticServiceException("Invalid user supplied");
		
		List<ContractReferenceFormat> types = null;
		try {
			ContractReferenceFormat type = new ContractReferenceFormat();
			
			Organisation organisation = organisationService.get(organisationId);
			type.setOrganisation(organisation);
			
			if(StringUtils.hasText(name)){
				type.setName(name);
			}
			
			type.setId(id);
			if(StringUtils.hasText(hash)){
				type.setId(contractReferenceFormatIdCache.get(hash));
			}
		
			types = contractReferenceFormatDAO.read(type);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read address types", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		final List<ContractReferenceFormatDO> pojoList = types.stream()
			.map(type -> {
				ContractReferenceFormatDO typeDO = null;
				try{
					typeDO = makeDO(type, userDO);						
					// Side effect 1
					contractReferenceFormatIdCache.put(typeDO.getId(), type.getId());

				}catch(final ContracticServiceException | NoSuchAlgorithmException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the contract reference %s", type)));
				}
				
				return typeDO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.contractReferenceFormatIdCache = new Cache(CONTRACTREFERENCEFORMAT_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	public MessageDO delete(ContractReferenceFormatDO contractReferenceFormatDO) throws ContracticServiceException {
		
		UserDO userDO = contractReferenceFormatDO.getSessionUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.DELETE, Module.ORGANISATION);
		
		final String hash = contractReferenceFormatDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Invalid contract reference id supplied");
			
		final Long id = contractReferenceFormatIdCache.get(hash);	
	
		// Save the person object
		ContractReferenceFormat contractReferenceFormat = null;
		try {

			contractReferenceFormat = get(id);
			contractReferenceFormatDAO.delete(contractReferenceFormat);
		
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Failed to delete type: %s", contractReferenceFormatDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Contract reference format '%s' updated successfully", contractReferenceFormat.getName()), MessageDO.OK);
	}

	public MessageDO update(ContractReferenceFormatDO contractReferenceFormatDO) throws ContracticServiceException {
		
		UserDO userDO = contractReferenceFormatDO.getSessionUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.DELETE, Module.ORGANISATION);
		
		final String hash = contractReferenceFormatDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Invalid document hash id provided");
			
		final Long id = contractReferenceFormatIdCache.get(hash);
		
		// Save the person object
		try {

			ContractReferenceFormat contractReferenceFormat = get(id);
			
			// Update the fields
			contractReferenceFormat.setName(contractReferenceFormatDO.getName());
			contractReferenceFormat.setDescription(contractReferenceFormatDO.getDescription());
			contractReferenceFormat.setActive(contractReferenceFormatDO.getActive());
			contractReferenceFormat.setFormat(contractReferenceFormatDO.getFormat());
			contractReferenceFormat.setDefault(contractReferenceFormatDO.getDifault());
			
			ValueDO contractTypeDO = contractReferenceFormatDO.getContractType();
			if(contractTypeDO != null && StringUtils.hasText(contractTypeDO.getId())){
				ContractType contractType = contractTypeService.get(contractTypeDO.getId());
				contractReferenceFormat.setContractType(contractType);
			}
			
			// Update the data store
			contractReferenceFormatDAO.update(contractReferenceFormat);
		
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update contract reference: %s", contractReferenceFormatDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Contract reference format '%s' updated successfully", contractReferenceFormatDO.getName()), MessageDO.OK);
	}

	public MessageDO create(ContractReferenceFormatDO contractReferenceFormatDO) throws ContracticServiceException {
		
		UserDO userDO = contractReferenceFormatDO.getSessionUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.DELETE, Module.ORGANISATION);
		
		final ContractReferenceFormat contractReferenceFormat = new ContractReferenceFormat();
		
		// Save the person object
		try {
					
			Organisation organisation = organisationService.get(contractReferenceFormatDO.getOrganisationId());
			contractReferenceFormat.setOrganisation(organisation);
			
			contractReferenceFormat.setName(contractReferenceFormatDO.getName());
			contractReferenceFormat.setDescription(contractReferenceFormatDO.getDescription());
			contractReferenceFormat.setActive(contractReferenceFormatDO.getActive());
			contractReferenceFormat.setFormat(contractReferenceFormatDO.getFormat());
			contractReferenceFormat.setDefault(contractReferenceFormatDO.getDifault());
			
			ValueDO contractTypeDO = contractReferenceFormatDO.getContractType();
			if(contractTypeDO != null && contractTypeDO.getId()!= null){
				ContractType contractType = contractTypeService.get(contractTypeDO.getId());
				contractReferenceFormat.setContractType(contractType);
			}
			
			contractReferenceFormatDAO.create(contractReferenceFormat);
			
			String hashId = HashingUtil.get(contractReferenceFormat.getId());
			// Update the cache
			this.contractReferenceFormatIdCache.put(hashId, contractReferenceFormat.getId());
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create contract reference: %s", contractReferenceFormatDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Contract reference format '%s' created successfully", contractReferenceFormatDO.getName()), MessageDO.OK);
	}
}
