package contractic.service.domain;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import contractic.model.data.store.Note;

public class NoteAudit implements AuditJob<NoteDO> {

	private final List<AuditDO> auditList = new LinkedList<>();
	
	@Override
	public List<AuditDO> run(NoteDO source, NoteDO target) {
		
		// Log change
		if(!Objects.equals(source.getType(), target.getType()))
			auditList.add(AuditJob.createAudit(Note.TYPE, source.getType().getValue(), target.getType().getValue()));
		
		if(!Objects.equals(source.getHeader(), target.getHeader()))
			auditList.add(AuditJob.createAudit(Note.HEADER, source.getHeader(), target.getHeader()));

		if(!Objects.equals(source.getDetail(), target.getDetail()))
			auditList.add(AuditJob.createAudit(Note.DETAIL, source.getDetail(), target.getDetail()));

		return auditList;
	}

}
