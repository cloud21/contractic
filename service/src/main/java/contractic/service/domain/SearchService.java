package contractic.service.domain;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import contractic.model.data.store.Contract;
import contractic.model.data.store.ContractDetail;
import contractic.model.data.store.Document;
import contractic.model.data.store.Module;
import contractic.model.data.store.Note;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Supplier;
import contractic.model.data.store.Task;
import contractic.service.util.SearchClient;
import contractic.service.util.SearchClient.SearchEntity;

public class SearchService implements
		DomainService<SearchEntity, Long, SearchEntityDO, MessageDO> {

	public static final String GET_PARAM_QUERY = "search.query";
	public static final String GET_PARAM_COLLECTION = "search.collection";
	private Properties properties;

	@Autowired
	private DomainService<Contract, Long, ContractDO, MessageDO> contractService;
	@Autowired
	private DomainService<ContractDetail, Long, ContractDetailDO, MessageDO> contractDetailService;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	@Autowired
	private DomainService<Supplier, Long, SupplierDO, MessageDO> supplierService;
	@Autowired
	private DomainService<Note, Long, NoteDO, MessageDO> noteService;
	@Autowired
	private DomainService<Task, Long, TaskDO, MessageDO> taskService;
	@Autowired
	private DomainService<Document, Long, DocumentDO, MessageDO> documentService;

	private static Logger logger = Logger.getLogger(SearchService.class);
	
	public static class COLLECTION{
		public static final String CONTRACT = Module.CONTRACT;
		public static final String CONTRACTDETAIL = Module.CONTRACTDETAIL;
		public static final String SUPPLIER = Module.SUPPLIER;
		public static final String DOCUMENT = Module.DOCUMENT;
		public static final String TASK = Module.TASK;
		public static final String NOTE = "Note";
	}
	//public final static String COLLECTION_

	public SearchService(final Properties properties) {
		this.properties = properties;
	}

	public MessageDO update(final SearchEntityDO searchEntity) {
		return create(searchEntity);
	}

	public MessageDO create(final SearchEntityDO searchEntity) {

		try (final CloseableHttpClient httpClient = HttpClientBuilder.create()
				.build()) {
			final SearchClient searchClient = new SearchClient(httpClient,
					properties);

			SearchEntity entity = new SearchEntity();
			entity.setId(searchEntity.getId());
			entity.setCollection(searchEntity.getCollection());
			entity.setText(searchEntity.getText());
			entity.setOrganisationId(searchEntity.getOrganisationId());
			
			searchClient.post(entity.getCollection().toLowerCase(),
					entity);
		} catch (Exception e) {
			logger.warn(String.format(
					"Error when endexing entity '%s' for each", searchEntity),
					e);
		}

		return new MessageDO();
	}

	public SearchEntity get(final String hash)
			throws ContracticServiceException {
		throw new UnsupportedOperationException("Not implemented");
	}

	public SearchEntity get(final Long id) throws ContracticServiceException {
		throw new UnsupportedOperationException("Not implemented");
	}

	private ContractDO getContractDO(final Long id, final UserDO userDO) throws ContracticServiceException{
		final Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, userDO);
		params.put(ContractService.GET_PARAM_ID, id);
		List<ContractDO> contractList = contractService.get(params);
		if (contractList.size() != 1)
			return null;
		return contractList.get(0);
	}
	
	private <M, I, D, V> D getDomainObject(DomainService
			<M, I, D, MessageDO> domainService, final I id, final UserDO userDO) throws ContracticServiceException{
		final Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, userDO);
		params.put(DomainService.GET_PARAM_ID, id);
		List<D> domainList = domainService.get(params);
		if (domainList.size() != 1)
			return null;
		return domainList.get(0);
	}
	
	private Object getExternalDomainObject(final Long externalId, final String moduleName, UserDO userDO) throws ContracticServiceException{
		switch(moduleName){
		case Module.CONTRACT:
			return getDomainObject(contractService, externalId, userDO);
		case Module.SUPPLIER:
			return getDomainObject(supplierService, externalId, userDO);
		case Module.PERSON:
			return getDomainObject(personService, externalId, userDO);
		case Module.TASK:
			return getDomainObject(taskService, externalId, userDO);
		default:
			return null;
		}
	}
	
	private SearchEntityDO makeDO(SearchEntity searchEntity, UserDO userDO)
			throws ContracticServiceException {
		final SearchEntityDO searchEntityDO = new SearchEntityDO();
		searchEntityDO.setText(searchEntity.getText());

		switch (searchEntity.getCollection()) {
		case Module.CONTRACT: 
		{
			ContractDO contractDO = getDomainObject(contractService, searchEntity.getId(), userDO);
			if (contractDO == null)
				throw new ContracticServiceException(String.format(
						"Did not find corresponding contract id %s",
						searchEntity.getId()));
			searchEntityDO.setExternal(contractDO);
			searchEntityDO.setModule(CommonService.getModule(moduleService,
					Module.CONTRACT));
		}
			break;
			
		case Module.CONTRACTDETAIL:
			ContractDetail contractDetail = contractDetailService.get(searchEntity.getId());
			ContractDO contractDO = getDomainObject(contractService, contractDetail.getContract().getId(), userDO);
			if (contractDO == null)
				throw new ContracticServiceException(String.format(
						"Did not find corresponding contract id %s",
						searchEntity.getId()));
			ContractDetailDO contractDetailDO = getDomainObject(contractDetailService, contractDetail.getId(), userDO);
			searchEntityDO.setExternal(contractDO);
			searchEntityDO.setContractDetail(contractDetailDO);
			searchEntityDO.setModule(CommonService.getModule(moduleService,
					Module.CONTRACT));
			break;

		case Module.PERSON:
			PersonDO personDO = getDomainObject(personService, searchEntity.getId(), userDO);
			if (personDO == null)
				throw new ContracticServiceException(String.format(
						"Did not find corresponding person id %s",
						searchEntity.getId()));
			searchEntityDO.setExternal(personDO);
			searchEntityDO.setModule(CommonService.getModule(moduleService,
					Module.PERSON));
			break;

		case COLLECTION.NOTE:
			Note note = noteService.get(searchEntity.getId());
			Object domainObject = getExternalDomainObject(note.getExternalId(), note.getModule().getName(), userDO);
			NoteDO noteDO = getDomainObject(noteService, searchEntity.getId(), userDO);
			if (noteDO == null)
				throw new ContracticServiceException(String.format(
						"Did not find corresponding note id %s",
						searchEntity.getId()));
			searchEntityDO.setExternal(domainObject);
			searchEntityDO.setNote(noteDO);
			searchEntityDO.setModule(new ModuleDO(noteDO.getModuleId()));
			break;
			
		case Module.SUPPLIER:
			final SupplierDO supplierDO = getDomainObject(supplierService, searchEntity.getId(), userDO);
			if (supplierDO == null)
				throw new ContracticServiceException(String.format(
						"Did not find corresponding supplier id %s",
						searchEntity.getId()));
			searchEntityDO.setExternal(supplierDO);
			searchEntityDO.setModule(CommonService.getModule(moduleService,
					Module.SUPPLIER));
			break;

		default:
			throw new IllegalArgumentException(String.format("Supplied collection '%s' not supported", searchEntity.getCollection()));
		}

		return searchEntityDO;
	}

	public List<SearchEntityDO> get(final Map<String, Object> parameters)
			throws ContracticServiceException {
		final String query = (String) parameters.get(GET_PARAM_QUERY);
		final String collection = (String) parameters.get(GET_PARAM_COLLECTION);
		final UserDO userDO = (UserDO) parameters.get(CommonService.USER);

		final Organisation organisation = organisationService.get(userDO
				.getOrganisationId());
		final SearchClient.SearchEntity entity = new SearchClient.SearchEntity();

		entity.setOrganisationId(organisation.getId());
		entity.setCollection(collection);
		entity.setText(query);

		try (CloseableHttpClient httpClient = HttpClientBuilder.create()
				.build()) {

			SearchClient searchClient = new SearchClient(httpClient, properties);

			final SearchEntity[] docList = searchClient.search(collection,
					SearchEntity[].class, entity);

			List<SearchEntityDO> resultList = Arrays
					.stream(docList)
					.parallel()
					.map(doc -> {
						try {
							return makeDO(doc, userDO);
						} catch (Exception e) {
							logger.warn(
									"Error while making search domain object",
									e);
							return null;
						}
					}).filter(doc -> doc != null).collect(Collectors.toList());
			return resultList;
		} catch (Exception e) {
			throw new ContracticServiceException(
					"An error occured while executing the search service", e);
		}
	}

	@Override
	public void init() throws ContracticServiceException {
	}

	@Override
	public void destroy() throws ContracticServiceException {
	}

	@Override
	public MessageDO delete(SearchEntityDO dojo)
			throws ContracticServiceException {
		throw new UnsupportedOperationException("Not implemented");
	}

}
