package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.AddressType;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class AddressTypeService implements DomainService<AddressType, Long, StaticDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	public final static String ADDRESSTYPE_IDCACHE_NAME = "addresstype.idcache.name";
	public static final String GET_PARAM_ID = "id";
	private Cache<String, Long> addressTypeIdCache;
	
	@Autowired
	private CrudOperation<AddressType> addressTypeDAO;
	

	public AddressType get(final String uuid) throws ContracticServiceException{
		final Long id = this.addressTypeIdCache.get(uuid);
		return get(id);
	}
	
	public AddressType get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		AddressType addressType = new AddressType();
		addressType.setId(id);
		
		try {
			List<AddressType> types = addressTypeDAO.read(addressType);
			return types.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving address type with id %d", id));
		}
	}
	
	
	private StaticDO makeDO(final AddressType addressType) throws NoSuchAlgorithmException{
		String hashId = HashingUtil.get(addressType.getId());
		StaticDO staticDO = new StaticDO();
		staticDO.setId(hashId);
		staticDO.setValue(addressType.getName());
		return staticDO;
	}
	
	public List<StaticDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASH);
		 final String name = (String)params.get(GET_PARAM_NAME);
		 Long id = (Long)params.get(GET_PARAM_ID);
		 

			List<AddressType> types = null;
			try {
				AddressType type = new AddressType();
				
				if(StringUtils.hasText(name)){
					type.setName(name);
				}
				
				if(StringUtils.hasText(hash)){
					id = addressTypeIdCache.get(hash);
				}
				
				if(id!=null)
					type.setId(id);
								
				types = addressTypeDAO.read(type);
			} catch (ContracticModelException e) {
				throw new ContracticServiceException("An error occured while attempting to read address types", e);
			}

			// List of all exceptions that occur in the predicate
			List<ContracticServiceException> cse = new ArrayList<>();
			
			// If any exceptions were thrown, throw the first one here
			if(cse.size()!=0)
				throw cse.get(0);
		
			final List<StaticDO> pojoList = types.stream()
				.map(type -> {
					StaticDO staticDO = null;
					try{
						staticDO = makeDO(type);						
						// Side effect 1
						addressTypeIdCache.put(staticDO.getId(), type.getId());

					}catch(NoSuchAlgorithmException ex){
						cse.add(new ContracticServiceException(String.format("An error occured when hashing the user %s", type)));
					}
					
					return staticDO;
				}).collect(Collectors.toList());
			
			if(cse.size()!=0)
				throw cse.get(0);
			
			return pojoList;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.addressTypeIdCache = new Cache(ADDRESSTYPE_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}


	@Override
	public MessageDO update(StaticDO dojo) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public MessageDO create(StaticDO dojo) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageDO delete(StaticDO dojo) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}
