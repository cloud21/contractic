package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Country;
import contractic.model.data.store.Module;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class CountryService implements DomainService<Country, Long, CountryDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	private final static String COUNTRY_IDCACHE_NAME = "country.idcache.name";
	public static final String GET_PARAM_ID = "country.id";
	public static final String GET_PARAM_ACTIVE = "country.active";

	private Cache<String, Long> countryIdCache;
	
	@Autowired
	private CrudOperation<Country> countryDAO;
	

	public MessageDO update(final CountryDO pojo){
		return null;
	}
	
	public MessageDO create(final CountryDO pojo){
		return null;
	}
	
	public static Country getCountry(final CrudOperation<Country> countryDAO, String name) throws ContracticModelException{
		Country country = new Country();
		country.setName(name);
		List<Country> countryList = countryDAO.read(country);
		assert countryList.size() == 1;
		return countryList.get(0);
	}
	
	public Country get(final String hash) throws ContracticServiceException{
		final Long id = this.countryIdCache.get(hash);
		return get(id);
	}
	
	public Country get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null country id supplied");
		
		Country country = new Country();
		country.setId(id);
		
		try {
			List<Country> countrys = countryDAO.read(country);
			return countrys.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving country with id %d", id));
		}
	}
	
	private CountryDO makeDO(Country country) throws NoSuchAlgorithmException{
		CountryDO countryDO = new CountryDO();
		String hashId = HashingUtil.get(country.getId());
		countryDO.setId(hashId);
		countryDO.setName(country.getName());
		countryDO.setCurrencyCode(country.getCurrencyCode());
		countryDO.setCurrencyName(country.getCurrencyName());
		countryDO.setDialingCode(country.getDialingCode());
		countryDO.setHtmlCode(country.getHtmlCode());
		countryDO.setActive(country.getActive());
		return countryDO;
	}
	
	public List<CountryDO> get(final Map<String, Object> parameters) throws ContracticServiceException{
		final String hash = (String)parameters.get(GET_PARAM_HASH);
		final String name = (String)parameters.get(GET_PARAM_NAME);
		final Boolean active = (Boolean)parameters.get(GET_PARAM_ACTIVE);
		final Long id = (Long)parameters.get(GET_PARAM_ID);
		
		List<Country> countries = null;
		try {
			Country country = new Country();
			
			if(StringUtils.hasText(name)){
				country.setName(name);
			}
			
			if(id != null){
				country.setId(id);
			}
			
			country.setActive(active);
			
			countries = countryDAO.read(country);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read modules", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		List<CountryDO> pojoList = countries.stream()
			.map(country -> {
				CountryDO moduleDO = null;
				try{
					moduleDO = makeDO(country);
					
					// Side effect 1
					countryIdCache.put(moduleDO.getId(), country.getId());

				}catch(NoSuchAlgorithmException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the country %s", country)));
				}
				
				return moduleDO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
	}


	@Override
	public void init() throws ContracticServiceException {
		try {
			this.countryIdCache = new Cache(COUNTRY_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	@Override
	public MessageDO delete(CountryDO dojo) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}

}
