package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"user", "organisationId"})
public class DocumentDO {
	private String id;
	private String description;
	private String title;
	private String created;
	private ValueDO type;
	private ValueDO external;
	private ValueDO module;
	private ValueDO status;
	private FileDO file;
	private String size;
	private String organisationId;
	private UserDO user;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String received) {
		this.created = received;
	}

	public ValueDO getType() {
		return type;
	}
	public void setType(ValueDO type) {
		this.type = type;
	}

	public ValueDO getExternal() {
		return external;
	}
	public void setExternal(ValueDO external) {
		this.external = external;
	}
	public ValueDO getModule() {
		return module;
	}
	public void setModule(ValueDO module) {
		this.module = module;
	}
	public ValueDO getStatus() {
		return status;
	}
	public void setStatus(ValueDO status) {
		this.status = status;
	}
	public FileDO getFile() {
		return file;
	}
	public void setFile(FileDO file) {
		this.file = file;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public UserDO getUser() {
		return user;
	}
	public void setUser(UserDO user) {
		this.user = user;
	}

	
}
