package contractic.service.domain;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.User;
import contractic.model.data.store.UserRole;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class UserService implements DomainService<User, Long, UserDO, MessageDO> {

	public final static String GET_PARAM_USERNAME = "username";
	public final static String GET_PARAM_ORGANISATIONID = "organisation.id";
	
	public final static String USER_IDCACHE_NAME = "user.idcache.name";
	public static final String GET_PARAM_ID = "user.id";
	private Cache<String, Long> userIdCache;
	
	private final static Logger logger = Logger.getLogger(UserService.class);
	
	@Autowired
	private CrudOperation<User> userDAO;
	
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	
	@Autowired
	private DomainService<UserRole, Long, UserRoleDO, MessageDO> userRoleService;

	/**
	 * Get a User Domain Object.
	 * @param params Keys can be login, password, uuid
	 * @return
	 * @throws ContracticServiceException 
	 */
	public List<UserDO> get(final Map<String, Object> params) throws ContracticServiceException{
		final String hash = (String)params.get(GET_PARAM_HASHID);
		final String login = (String)params.get(GET_PARAM_USERNAME);
		final String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);
		Long id = (Long)params.get(GET_PARAM_ID);
		
		List<User> users = null;

		User user = new User();
		
		if(StringUtils.hasText(login)){
			user.setLoginId(login);
		}
		
		if(id==null && !StringUtils.hasText(login) && !StringUtils.hasText(organisationId)){
			throw new RuntimeException("Organisation id needed to read user");
		}
		
		user.setId(id);
		
		if(StringUtils.hasText(hash)){
			id = userIdCache.get(hash);
			user.setId(id);
		}
		
		Person person = new Person();
		if(StringUtils.hasText(organisationId)){
			Organisation organisation = organisationService.get(organisationId);
			person.setOrganisation(organisation);
			user.setPerson(person);
		}
		
		if(StringUtils.hasText(login)){
			user.setLoginId(login);
		}
		
		try {				
			users = userDAO.read(user);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read user", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		List<UserDO> pojoList = users.stream()
			.map(usr -> {
				UserDO userPOJO = null;
				try{
					userPOJO = new UserDO();				
					userPOJO = makeDO(usr);
					userIdCache.put(userPOJO.getId(), usr.getId());
					
				}catch(ContracticServiceException ce){
					cse.add(ce);
				}catch(NoSuchAlgorithmException |IOException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the user %s", usr), ex));
				}
				
				return userPOJO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
		
	}
	
	private Map<String, Object> makeOrganisation(OrganisationDO organisationDO){
		final Map<String, Object> organisationMap = new HashMap<>();
		organisationMap.put(OrganisationDO.FIELD_CODE, organisationDO.getCode());
		organisationMap.put(OrganisationDO.FIELD_ID, organisationDO.getId());
		return organisationMap;
	}
	
	private UserDO makeDO(User user) throws NoSuchAlgorithmException, ContracticServiceException, JsonParseException, JsonMappingException, IOException{
		UserDO userPOJO = new UserDO();
		String hashId = HashingUtil.get(user.getId());
		
		if(user.getProfile() == null)
			userPOJO.setProfile(new HashMap<String, Object>());
		else{
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> profile = mapper.readValue(user.getProfile(),
				                    new TypeReference<Map<String, Object>>() {
				                    });
			userPOJO.setProfile(profile);
		}
		
		// Get the organisation's domain object. We need it to query the organisation service
		Map<String, Object> params = new HashMap<>();
		params.put(OrganisationService.GET_PARAM_ID, user.getPerson().getOrganisation().getId());
		List<OrganisationDO> organisationDOList = organisationService.get(params);
		assert(organisationDOList.size() == 1);
		OrganisationDO organisation = organisationDOList.get(0);
		userPOJO.setOrganisationId(organisation.getId());
		
		userPOJO.setOrganisation(this.makeOrganisation(organisation));
		
		// Get the user role
		params.clear();
		params.put(UserRoleService.GET_PARAM_ID, user.getUserRole().getId());
		params.put(UserRoleService.GET_PARAM_ORGANISATIONID, organisation.getId());
		List<UserRoleDO> roleList = userRoleService.get(params);
		assert(roleList.size() == 1);
		UserRoleDO userRole = roleList.get(0);
		userPOJO.setRole(new ValueDO(userRole.getId(), userRole.getName()));
		
		// Get the person's domain object
		params.clear();
		params.put(DomainService.GET_PARAM_ID, user.getPerson().getId());
		params.put(CommonService.USER, userPOJO);
		List<PersonDO> personDOList = personService.get(params);
		assert(personDOList.size() == 1);
		PersonDO personDO = personDOList.get(0);
		userPOJO.setPerson(personDO);
		
		// Now create the user domain object
		userPOJO.setId(hashId);
		userPOJO.setLogin(user.getLoginId());
		userPOJO.setPersonId(personDO.getId());
		userPOJO.setPersonName(String.join(" ", new String[] {
				(personDO.getFirstName() == null ? "" : personDO.getFirstName()),
				(personDO.getMiddleName() == null ? "" : personDO.getMiddleName()),
				(personDO.getLastName() == null ? "" : personDO.getLastName())}));
		userPOJO.setActive(user.getActive());
		//***// Password is never returned
		
		return userPOJO;
	}
	
	public User get(final String uuid) throws ContracticServiceException{
		final Long id = this.userIdCache.get(uuid);
		return get(id);
	}
	
	public User get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		User user = new User();
		user.setId(id);
		
		try {
			List<User> users = userDAO.read(user);
			return users.get(0);
			
		} catch (ContracticModelException e) {
			throw new RuntimeException(String.format("An error occured while retrieving user with id %d", id));
		}
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.userIdCache = new Cache(USER_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	public MessageDO create(UserDO pojo) throws ContracticServiceException {
		// Construct the user object
		final User user = new User();
		user.setLoginId(pojo.getLogin());

		ValueDO returnValue = new ValueDO();
		// Save the user object
		try {
			Person person = personService.get(pojo.getPersonId());
			user.setPerson(person);
			//String hashed = BCrypt.hashpw(password, BCrypt.gensalt());
			if(StringUtils.hasText(pojo.getPassword()))
				user.setPassword(BCrypt.hashpw(pojo.getPassword(), BCrypt.gensalt()));
			
			Boolean active = pojo.getActive();
			user.setActive(active==null ? Boolean.FALSE : pojo.getActive());
			
			user.setLastUpdated(new Date());
			
			if(pojo.getProfile()!=null){
				ObjectMapper objectMapper = new ObjectMapper();
				String profileString = objectMapper.writeValueAsString(pojo.getProfile());
				user.setProfile(profileString);
			}
			
			UserRole userRole = userRoleService.get(pojo.getRole().getId());
			user.setUserRole(userRole);
			
			userDAO.create(user);
			
			String hashId = HashingUtil.get(user.getId());
			// Update the cache
			this.userIdCache.put(hashId, user.getId());
			
			returnValue.setId(hashId);
		
		} catch (final NoSuchAlgorithmException | ContracticModelException | JsonProcessingException e) {
			String message = String.format("Failed to create user: '%s'", pojo.getPerson().getShortName());
			throw new ContracticServiceException(message, e);
		}
		
		MessageDO messageDO = new MessageDO(null, "Contractic", String.format("User '%s' created successfully", pojo.getLogin()), MessageDO.OK);
		messageDO.setRef(returnValue);
		//final String icon, final String title, final String body, final String type
		return messageDO;
	}

	public MessageDO update(UserDO pojo) throws ContracticServiceException {
		
		// Save the user object
		try {
			
			final User user = this.get(pojo.getId());

			if(StringUtils.hasText(pojo.getPassword())){
				user.setPassword(BCrypt.hashpw(pojo.getPassword(), BCrypt.gensalt()));
			}
			
			Boolean active = pojo.getActive();
			
			user.setActive(active==null ? Boolean.FALSE : pojo.getActive());
			user.setLastUpdated(new Date());

			if(pojo.getProfile()!=null){
				ObjectMapper objectMapper = new ObjectMapper();
				String profileString = objectMapper.writeValueAsString(pojo.getProfile());
				user.setProfile(profileString);
			}
			
			UserRole userRole = userRoleService.get(pojo.getRole().getId());
			user.setUserRole(userRole);
			
			userDAO.update(user);
			
			String hashId = HashingUtil.get(user.getId());
			// Update the cache
			this.userIdCache.put(hashId, user.getId());
		
		} catch (Exception e) {
			String message = String.format("Failed to update user: '%s'", pojo.getPerson().getShortName());
			logger.error(message, e);
			throw new ContracticServiceException(message, e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("User '%s' updated successfully", pojo.getLogin()), MessageDO.OK);
	}

	public MessageDO delete(UserDO userDO) throws ContracticServiceException {
		Long userId = this.userIdCache.get(userDO.getId());

		if(userId == null){
			//final String icon, final String title, final String body, final String type
			return new MessageDO(null, "Internal system error", "Invalid user (may have been deleted)", MessageDO.ERROR);
		}
		
		try {
			final User user = get(userId);
			// Delete from data source
			userDAO.delete(user);
			
			// if successfull, remove from Id cache
			this.userIdCache.remove(userDO.getId());
			
			return new MessageDO(null, "Contractic", "Person deleted successfully!", MessageDO.OK);
		} catch (Exception e) {
			throw new ContracticServiceException("Failed to delete user", e);
		}
	}

}
