package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.AuditAction;
import contractic.service.util.Cache;

public class AuditActionService implements DomainService<AuditAction, Long, StaticDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	public final static String AUDITACTION_IDCACHE_NAME = "auditaction.idcache.name";
	public static final String GET_PARAM_ID = "id";
	private Cache<String, Long> auditActionIdCache;
	
	@Autowired
	private CrudOperation<AuditAction> auditActionDAO;
	

	public AuditAction get(final String uuid) throws ContracticServiceException{
		final Long id = this.auditActionIdCache.get(uuid);
		return get(id);
	}
	
	public AuditAction get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		AuditAction auditActionType = new AuditAction();
		auditActionType.setId(id);
		
		try {
			List<AuditAction> types = auditActionDAO.read(auditActionType);
			return types.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving audit action with id %d", id));
		}
	}
	
	
	private StaticDO makeDO(final AuditAction auditAction) throws NoSuchAlgorithmException{
		StaticDO staticDO = new StaticDO();
		staticDO.setId(auditAction.getName());
		staticDO.setValue(auditAction.getName());
		return staticDO;
	}
	
	public List<StaticDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASH);
		 final String name = (String)params.get(GET_PARAM_NAME);
		 Long id = (Long)params.get(GET_PARAM_ID);
		 

			List<AuditAction> actionList = null;
			try {
				AuditAction action = new AuditAction();
				
				if(StringUtils.hasText(name)){
					action.setName(name);
				}
				
				if(StringUtils.hasText(hash)){
					id = auditActionIdCache.get(hash);
				}
				
				if(id!=null)
					action.setId(id);
								
				actionList = auditActionDAO.read(action);
			} catch (ContracticModelException e) {
				throw new ContracticServiceException("An error occured while attempting to read address types", e);
			}

			// List of all exceptions that occur in the predicate
			List<ContracticServiceException> cse = new ArrayList<>();
			
			// If any exceptions were thrown, throw the first one here
			if(cse.size()!=0)
				throw cse.get(0);
		
			final List<StaticDO> pojoList = actionList.stream()
				.map(type -> {
					StaticDO staticDO = null;
					try{
						staticDO = makeDO(type);						
						// Side effect 1
						auditActionIdCache.put(staticDO.getId(), type.getId());

					}catch(NoSuchAlgorithmException ex){
						cse.add(new ContracticServiceException(String.format("An error occured when hashing the user %s", type)));
					}
					
					return staticDO;
				}).collect(Collectors.toList());
			
			if(cse.size()!=0)
				throw cse.get(0);
			
			return pojoList;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.auditActionIdCache = new Cache(AUDITACTION_IDCACHE_NAME, String.class, Long.class);
			
			// This helps us get all the audit actions into the cache for use later
			get(new HashMap<String, Object>());
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}


	@Override
	public MessageDO update(StaticDO dojo) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public MessageDO create(StaticDO dojo) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageDO delete(StaticDO dojo) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}
