package contractic.service.domain;

import java.util.Arrays;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"shortName", "organisationId"})
public class PersonDO {
	
	private String id;
	private String reference;
	private String title;
	private String firstName;
	private String middleName;
	private String lastName;
	private String jobTitle;
	private String gender;
	private String created;
	private Boolean active;
	private String organisationId;
	private ContactDO[] contact;
	private String userId;
	private GroupDO[] department;
	
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}

	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	
	public ContactDO[] getContact() {
		return contact;
	}
	public void setContact(ContactDO[] contact) {
		this.contact = contact;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public GroupDO[] getDepartment() {
		return department;
	}
	public void setDepartment(GroupDO[] department) {
		this.department = department;
	}
	public String getShortName() {
		String[] names = new String[]{firstName, lastName};
		return String.join(", ", names);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonDO other = (PersonDO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PersonDO [id=" + id + ", reference=" + reference + ", title="
				+ title + ", firstName=" + firstName + ", middleName="
				+ middleName + ", lastName=" + lastName + ", jobTitle="
				+ jobTitle + ", gender=" + gender + ", created=" + created
				+ ", enabled=" + active + ", organisationId=" + organisationId
				+ ", contact=" + Arrays.toString(contact) + ", userId="
				+ userId + "]";
	}
	
}
