package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"organisationId"})
public class ObjectContactDO {
	private ValueDO external;
	private ValueDO module;
	private String organisationId;
	private ContactDO[] contact;
	public ValueDO getExternal() {
		return external;
	}
	public void setExternal(ValueDO external) {
		this.external = external;
	}
	public ValueDO getModule() {
		return module;
	}
	public void setModule(ValueDO module) {
		this.module = module;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	public ContactDO[] getContact() {
		return contact;
	}
	public void setContact(ContactDO[] contact) {
		this.contact = contact;
	}
}
