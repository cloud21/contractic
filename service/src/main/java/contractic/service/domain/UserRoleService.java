package contractic.service.domain;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.UserRole;
import contractic.model.data.store.UserRolePermission;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class UserRoleService implements DomainService<UserRole, Long, UserRoleDO, MessageDO> {

	private static Logger logger = Logger.getLogger(UserRoleService.class);
	
	public final static String GET_PARAM_USERNAME = "username";
	public final static String GET_PARAM_ORGANISATIONID = "organisation.id";
	
	public final static String USER_ROLE_IDCACHE_NAME = "user.role.idcache.name";
	public static final String GET_PARAM_ID = "user.id";

	public static final String GET_LOG_READ = "enable.read.log";
	private Cache<String, Long> userRoleIdCache;
	
	@Autowired
	private CrudOperation<UserRole> userRoleDAO;
	
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	
	@Autowired
	private DomainService<UserRolePermission, Long, UserRolePermissionDO, MessageDO> userRolePermissionService;
	
	@Autowired
	private CommonService commonService;

	/**
	 * Get a UserRole Domain Object.
	 * @param params Keys can be login, password, uuid
	 * @return
	 * @throws ContracticServiceException 
	 */
	public List<UserRoleDO> get(final Map<String, Object> params) throws ContracticServiceException{
		final String hash = (String)params.get(GET_PARAM_HASHID);
		Long id = (Long)params.get(GET_PARAM_ID);
		String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);
		
		if(!StringUtils.hasText(organisationId))
			throw new ContracticServiceException("Missing organisation id");
		
		List<UserRole> users = null;
		try {
			UserRole user = new UserRole();
			
			user.setId(id);
			if(StringUtils.hasText(hash)){
				id = userRoleIdCache.get(hash);
				user.setId(id);
			}
			
			if(StringUtils.hasText(organisationId)){
				Organisation organisation = organisationService.get(organisationId);
				user.setOrganisation(organisation);
			}

			users = userRoleDAO.read(user);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read organisations", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		List<UserRoleDO> pojoList = users.stream()
			.map(user -> {
				UserRoleDO userPOJO = null;
				try{
					userPOJO = new UserRoleDO();				
					userPOJO = makeDO(user, organisationId);
					userRoleIdCache.put(userPOJO.getId(), user.getId());
					
				}catch(ContracticServiceException ce){
					cse.add(ce);
				}catch(NoSuchAlgorithmException |IOException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the user %s", user), ex));
				}
				
				return userPOJO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
		
	}
	
	private UserRoleDO makeDO(UserRole userRole, String organisationId) throws NoSuchAlgorithmException, ContracticServiceException, JsonParseException, JsonMappingException, IOException{
		UserRoleDO userPOJO = new UserRoleDO();
		String hashId = HashingUtil.get(userRole.getId());
		
		userPOJO.setId(hashId);
		userPOJO.setName(userRole.getName());
		userPOJO.setDescription(userRole.getDescription());
		userPOJO.setOrganisationId(organisationId);
		userPOJO.setActive(userRole.getActive());
		
		// Get rid of existing permissions
		final Map<String, Object> params = new HashMap<>();
		params.put(UserRolePermissionService.GET_PARAM_ROLEID, userRole.getId());
		params.put(UserRolePermissionService.GET_PARAM_ORGANISATIONID, organisationId);
		List<UserRolePermissionDO> permissionDOList = userRolePermissionService.get(params);
		if(permissionDOList != null)
			userPOJO.setPermission(permissionDOList.toArray(new UserRolePermissionDO[0]));
		
		return userPOJO;
	}
	
	public UserRole get(final String uuid) throws ContracticServiceException{
		final Long id = this.userRoleIdCache.get(uuid);
		return get(id);
	}
	
	public UserRole get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		UserRole user = new UserRole();
		user.setId(id);
		
		try {
			List<UserRole> users = userRoleDAO.read(user);
			return users.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving user with id %d", id));
		}
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.userRoleIdCache = new Cache(USER_ROLE_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	public MessageDO create(UserRoleDO roleDO) throws ContracticServiceException{
		
		
		final UserDO userDO = roleDO.getUser();
		if(userDO == null)
			throw new ContracticServiceException("Invalid session user provided");
		
		// Construct the person object
		final UserRole userRole = new UserRole();
		
		userRole.setName(roleDO.getName());
		userRole.setDescription(roleDO.getDescription());
		userRole.setActive(roleDO.getActive());
		
		final Organisation organisation = organisationService.get(roleDO.getOrganisationId());
		userRole.setOrganisation(organisation);
		
		UserRolePermissionDO[] permissions = roleDO.getPermission();
		
		if(permissions == null)
			return new MessageDO(null, "Contractic", "No permission supplied for this user role", MessageDO.OK);
		
		List<ContracticServiceException> exceptionList = new ArrayList<>();
		
		Set<UserRolePermission> permissionSet = Arrays.stream(permissions)
				.map(permission -> {
			UserRolePermission rolePermission = new UserRolePermission();
			boolean read = (permission==null || permission.getRead() == null) ? false : permission.getRead();
			boolean write = (permission==null || permission.getWrite() == null) ? false : permission.getWrite();
			boolean delete = (permission==null || permission.getDelete() == null) ? false : permission.getDelete();
			
			try {
				final Module module = moduleService.get(permission.getModule().getId());
				rolePermission.setModule(module);
				rolePermission.setUserRole(userRole);
			} catch (ContracticServiceException e) {
				exceptionList.add(e);
				logger.warn(String.format("Could not create permission set for role '%s'",  roleDO.getName()), e);
				return null;
			}
			
			rolePermission.setRead(read);
			rolePermission.setWrite(write);
			rolePermission.setDelete(delete);
			
			return rolePermission;
		})
		.filter(permission -> permission != null)
		.collect(Collectors.toSet());
		
		if(exceptionList.size()>0)
			throw exceptionList.get(0);
		
		userRole.setPermissions(permissionSet);
		
		// Save the person object
		try {
			userRoleDAO.create(userRole);
			
			String hashId = HashingUtil.get(userRole.getId());
			// Update the cache
			this.userRoleIdCache.put(hashId, userRole.getId());
			
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create user role: %s", roleDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("User role '%s' created successfully", roleDO.getName()), MessageDO.OK);
	}

	public static <T> Set<T> getIntersection(Set<T> set1, Set<T> set2) {
	    boolean set1IsLarger = set1.size() > set2.size();
	    Set<T> cloneSet = new HashSet<T>(set1IsLarger ? set2 : set1);
	    cloneSet.retainAll(set1IsLarger ? set1 : set2);
	    return cloneSet;
	}
	
	public MessageDO update(UserRoleDO roleDO) throws ContracticServiceException{
		// Construct the object
		UserDO userDO = roleDO.getUser();
		
		// Save the object
		try {
			
			UserRolePermissionDO[] permissions = roleDO.getPermission();
			
			if(permissions == null){
				// Throw exception here
				return new MessageDO(null, "Contractic", "No permission supplied for this user role", MessageDO.OK);
			}
			
			// Get the most recent view
			List<UserRole> noteList = null;
			{
				UserRole userRole = get(roleDO.getId());
				noteList = userRoleDAO.read(userRole);
			}
			assert(noteList!=null && noteList.size() == 1);
			final UserRole userRole = noteList.get(0);
			
			// Now update fields
			userRole.setName(roleDO.getName());
			userRole.setDescription(roleDO.getDescription());
			userRole.setActive(roleDO.getActive());
			
			// Get rid of existing permissions
			final Map<String, Object> params = new HashMap<>();
			params.put(UserRolePermissionService.GET_PARAM_ROLEID, userRole.getId());
			params.put(CommonService.USER, userDO);
			params.put(UserRolePermissionService.GET_PARAM_ORGANISATIONID, userDO.getOrganisationId());
			List<UserRolePermissionDO> permissionDOList = userRolePermissionService.get(params);
			final List<ContracticServiceException> cse = new ArrayList<>();
			permissionDOList.forEach(permission -> {
				try {
					permission.setUser(userDO);
					userRolePermissionService.delete(permission);
				} catch (ContracticServiceException e) {
					cse.add(e);
				}
			});
			
			if(cse.size() > 0)
				throw cse.get(0);
			
			// Add new permissions
			Arrays.stream(permissions).forEach(permission -> {
				try {
					permission.setRole(new ValueDO(roleDO.getId(), roleDO.getName()));
					userRolePermissionService.create(permission);
				} catch (ContracticServiceException e) {
					cse.add(e);
				}
			});
			
			if(cse.size() > 0)
				throw cse.get(0);
			
			/*List<ContracticServiceException> exceptionList = new ArrayList<>();
			
			Set<UserRolePermission> newPermissionSet = Arrays.stream(permissions)
					.map(permission -> {
				UserRolePermission rolePermission = new UserRolePermission();
				boolean read = (permission==null || permission.getRead() == null) ? false : Boolean.parseBoolean(permission.getRead());
				boolean write = (permission==null || permission.getWrite() == null) ? false : Boolean.parseBoolean(permission.getWrite());
				boolean delete = (permission==null || permission.getDelete() == null) ? false : Boolean.parseBoolean(permission.getDelete());
				
				try {
					final Module module = moduleService.get(permission.getModule().getId());
					rolePermission.setModule(module);
					rolePermission.setUserRole(userRole);
				} catch (ContracticServiceException e) {
					exceptionList.add(e);
					logger.warn(String.format("Could not create permission set for role '%s'",  roleDO.getName()), e);
					return null;
				}
				
				rolePermission.setRead(read);
				rolePermission.setWrite(write);
				rolePermission.setDelete(delete);
				
				return rolePermission;
			})
			.filter(permission -> permission != null)
			.collect(Collectors.toSet());
			
			if(exceptionList.size()>0)
				throw exceptionList.get(0);
			
			Set<UserRolePermission> permissionSet = getIntersection(userRole.getPermissions(), newPermissionSet);
			
			userRole.setPermissions(permissionSet);*/
			
			userRoleDAO.update(userRole);
			
			String hashId = HashingUtil.get(userRole.getId());
			// Update the cache
			this.userRoleIdCache.put(hashId, userRole.getId());
			
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update user role: %s", roleDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", "Role updated successfully", MessageDO.OK);
	}
	
	public MessageDO delete(UserRoleDO userRoleDO) throws ContracticServiceException{
		// Construct the object
		UserRole userRole = get(userRoleDO.getId());
		
		// Save the object
		try {
			
			// Get the most recent view
			List<UserRole> noteList = userRoleDAO.read(userRole);
			assert(noteList.size() == 1);
			userRole = noteList.get(0);
			
			userRoleDAO.delete(userRole);
			
			String hashId = HashingUtil.get(userRole.getId());
			// Update the cache
			this.userRoleIdCache.put(hashId, userRole.getId());
			
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to delete user role: %s", userRoleDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", "Role updated successfully", MessageDO.OK);
	}

}
