package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"organisationId"})
public class ContactDO {
	/**
	 * Use type field
	 */
	@Deprecated
	private String typeId;
	@Deprecated
	private String value;
	/**
	 * Use address field
	 */
	@Deprecated
	private String id;
	private String externalId;
	private String moduleId;
	private String organisationId;
	private ValueDO address;
	private ValueDO type;
	private UserDO sessionUser;
	
	public ContactDO(){
		
	}
	
	public ContactDO( String id,String contactType, String contact, String externalId, String moduleId, String organisationId) {
		this.id = id;
		this.typeId = contactType;
		this.value = contact;
		this.externalId = externalId;
	}
	
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String contactType) {
		this.typeId = contactType;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String contact) {
		this.value = contact;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}

	public ValueDO getAddress() {
		return address;
	}

	public void setAddress(ValueDO address) {
		this.address = address;
	}

	public ValueDO getType() {
		return type;
	}

	public void setType(ValueDO type) {
		this.type = type;
	}

	public UserDO getSessionUser() {
		return sessionUser;
	}

	public void setSessionUser(UserDO sessionUser) {
		this.sessionUser = sessionUser;
	}
	
	
}
