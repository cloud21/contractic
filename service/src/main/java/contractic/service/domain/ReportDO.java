package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"organisationId", "command", "externalRef"})
public class ReportDO {
	private String id;
	private String name;
	private String description;
	private ValueDO group;
	private ValueDO owner;
	private String schedule;
	private String command;
	private String organisationId;
	private Object data;
	private UserDO user;
	private Boolean active;
	private Long lastrun;
	private Long created;
	private String externalRef;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ValueDO getGroup() {
		return group;
	}
	public void setGroup(ValueDO group) {
		this.group = group;
	}
	public ValueDO getOwner() {
		return owner;
	}
	public void setOwner(ValueDO owner) {
		this.owner = owner;
	}
	public String getSchedule() {
		return schedule;
	}
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public UserDO getUser() {
		return user;
	}
	public void setUser(UserDO user) {
		this.user = user;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Long getLastrun() {
		return lastrun;
	}
	public void setLastrun(Long lastrun) {
		this.lastrun = lastrun;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public String getExternalRef() {
		return externalRef;
	}
	public void setExternalRef(String externalRef) {
		this.externalRef = externalRef;
	}
	
}
