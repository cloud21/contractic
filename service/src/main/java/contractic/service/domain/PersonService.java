package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Address;
import contractic.model.data.store.AddressType;
import contractic.model.data.store.Audit;
import contractic.model.data.store.Contact;
import contractic.model.data.store.Contract;
import contractic.model.data.store.Group;
import contractic.model.data.store.GroupObject;
import contractic.model.data.store.GroupSet;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.User;
import contractic.model.data.store.UserRolePermissionAction;
import contractic.service.util.Cache;
import contractic.service.util.ConcurrencyUtil;
import contractic.service.util.HashingUtil;
import contractic.service.util.SearchClient.SearchEntity;

public final class PersonService implements
		DomainService<Person, Long, PersonDO, MessageDO> {
	public final static String PERSON_IDCACHE_NAME = "person.idcache.name";
	public static final String GET_PARAM_SEARCH_QUERY = "search.query";

	public static final String GET_PARAM_ACTIVE = "person.active";

	// This cache helps us hide database IDs from each request
	private Cache<String, Long> personIdCache;

	// DAOs
	@Autowired
	private CrudOperation<Person> personDAO;
	// Services
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	@Autowired
	private DomainService<Address, Long, AddressDO, MessageDO> addressService;
	@Autowired
	private DomainService<Contact, Long, ContactDO, MessageDO> contactService;
	@Autowired
	private DomainService<AddressType, Long, StaticDO, MessageDO> addressTypeService;
	@Autowired
	private DomainService<GroupSet, Long, GroupSetDO, MessageDO> groupSetService;
	@Autowired
	private DomainService<Group, Long, GroupDO, MessageDO> groupService;
	@Autowired
	private CrudOperation<Module> moduleDAO;
	@Autowired
	private CrudOperation<GroupObject> groupObjectDAO;
	@Autowired
	private DomainService<Audit, Long, AuditDO, MessageDO> auditService;
	@Autowired
	private DomainService<SearchEntity, Long, SearchEntityDO, MessageDO> searchService;
	@Autowired
	private CrudOperation<Contact> contactDAO;

	private CommonService commonService;

	private static final Logger logger = Logger.getLogger(Person.class);

	// public PersonDO getByUUID(final String orgId, final String personId)
	// throws ContracticServiceException{
	// Long id = personIdCache.get(personId);
	// Organisation org = organisationService.get(orgId);
	// return get(org, id);
	// }
	//
	/**
	 * Retrieve a PersonDO
	 * 
	 * @param id
	 *            the datastore person Id
	 * @return
	 * @throws ContracticServiceException
	 */
	public Person get(Long id) throws ContracticServiceException {
		if (id == null)
			throw new ContracticServiceException("Null person id supplied");

		Person person = new Person();
		person.setId(id);

		try {
			List<Person> persons = personDAO.read(person);
			return persons.get(0);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"An error occured while retrieving use with id %d", id));
		}
	}

	private PersonDO makeDO(Person person, UserDO userDO)
			throws NoSuchAlgorithmException, ContracticServiceException {

		String hashId = HashingUtil.get(person.getId());

		PersonDO personDO = new PersonDO();
		// Side effect 2
		personDO.setId(hashId);
		personDO.setTitle(person.getTitle());
		personDO.setFirstName(person.getFirstName());
		personDO.setMiddleName(person.getMiddleName());
		personDO.setLastName(person.getLastName());
		personDO.setGender(person.getGender());
		personDO.setJobTitle(person.getJobTitle());
		personDO.setOrganisationId(userDO.getOrganisationId());
		personDO.setActive(person.getActive());

		// Get the group
		// A person can exist in more than one group
		ModuleDO moduleDO = CommonService.getModule(moduleService,
				Module.PERSON);
		GroupSetDO groupSetDO = GroupSetService.getGroupSet(groupSetService,
				userDO.getOrganisationId(), GroupSet.DEPARTMENT);
		if (groupSetDO != null) {
			final Map<String, Object> params = new HashMap<>();
			params.put(GroupService.GET_PARAM_GROUPSETID, groupSetDO.getId());
			params.put(GroupService.GET_PARAM_MODULEID, moduleDO.getId());
			params.put(GroupService.GET_PARAM_EXTERNALID, person.getId());
			params.put(CommonService.USER, userDO);
			final List<GroupDO> groupList = groupService.get(params);

			personDO.setDepartment(groupList.toArray(new GroupDO[0]));
		}
		return personDO;
	}

	public List<PersonDO> get(final Map<String, Object> params)
			throws ContracticServiceException {

		final String hashId = (String) params.get(GET_PARAM_HASHID);

		final Long personId = (Long) params.get(GET_PARAM_ID);
		final Boolean active = (Boolean) params.get(GET_PARAM_ACTIVE);
		final UserDO userDO = (UserDO) params.get(CommonService.USER);
		if (userDO == null)
			throw new ContracticServiceException(
					"Invalid session user provided");

		final String organisationId = userDO.getOrganisationId();
		if (!StringUtils.hasText(organisationId)) {
			throw new ContracticServiceException("OrganisationId not supplied");
		}

		// commonService.checkPermission(userDO, UserRolePermissionAction.READ,
		// Module.ORGANISATION);

		final String queryString = (String) params.get(GET_PARAM_SEARCH_QUERY);

		List<Person> persons = null;
		try {
			Person person = new Person();

			Organisation organisation = organisationService.get(organisationId);
			if (organisation == null)
				throw new ContracticServiceException(
						"Invalid OrganisationId supplied");

			person.setOrganisation(organisation);

			Long id = personId;
			if (StringUtils.hasText(hashId)) {
				id = this.personIdCache.get(hashId);
			}

			if (id != null) {
				person.setId(id);
			}

			if (active != null) {
				person.setActive(active);
			}

			if (queryString != null) {
				person.setFirstName(queryString);
				person.setLastName(queryString);
				person.setMiddleName(queryString);
			}

			persons = personDAO.read(person);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(
					"An error occured while attempting to read organisations",
					e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();

		List<PersonDO> pojoList = persons
				.stream()
				.parallel()
				.map(person -> {
					PersonDO personPOJO = null;
					try {
						personPOJO = makeDO(person, userDO);
						// Side effect ???
						personIdCache.put(personPOJO.getId(), person.getId());
						return personPOJO;

					} catch (NoSuchAlgorithmException
							| ContracticServiceException ex) {
						cse.add(new ContracticServiceException(
								String.format(
										"An error occured when hashing the organisation %s",
										person), ex));
					}

					return personPOJO;
				}).collect(Collectors.toList());

		// This ensures we only return valid entry list
		if (cse.size() != 0)
			throw cse.get(0);

		return pojoList;

	}

	// protected PersonDO get(final Organisation organisation, final Long
	// personId) throws ContracticServiceException{
	// if(organisation == null)
	// throw new
	// ContracticServiceException("Invalid organisation supplied (null)");
	//
	// if(personId == null)
	// throw new ContracticServiceException("Invalid person supplied (null)");
	//
	// Person person = new Person();
	// person.setId(personId);
	// person.setOrganisation(organisation);
	//
	// OrganisationDO organisationPojo =
	// organisationService.get(organisation.getId());
	//
	// PersonDO personPOJO = null;
	//
	// try {
	// List<Person> personList = personDAO.read(person);
	// if(personList.size() != 1)
	// return null;
	// person = personList.get(0);
	//
	//
	// personPOJO = makeDO(person, organisationPojo.getId());
	// // Side effect ???
	// personIdCache.put(personPOJO.getId(), person.getId());
	//
	// }catch(NoSuchAlgorithmException | ContracticModelException ex){
	// throw new
	// ContracticServiceException(String.format("An error occured while attempting to read person with id %s",
	// personId));
	// }
	//
	// return personPOJO;
	// }

	public Person get(final String hashId) throws ContracticServiceException {
		Long id = personIdCache.get(hashId);

		if (id == null)
			throw new ContracticServiceException("Invalid id supplied (null)");
		Person person = new Person();
		person.setId(id);

		List<Person> personList = null;
		try {
			personList = personDAO.read(person);
			if (personList.size() != 1)
				return null;
			return personList.get(0);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(
					String.format(
							"An error occured while attempting to read organisation with id %s",
							id));
		}
	}

	// Make contacts from the person domain object
	// private List<Contact> makeContact(final ContactDO[] contacts) throws
	// ContracticServiceException{
	//
	// if(contacts == null)
	// return null;
	//
	//
	// // The person Id = external Id
	// //final Long personId = this.get(pojo.getOrganisationId(),
	// pojo.getId()).getId();
	//
	// final List<ContracticServiceException> cseList = new
	// ArrayList<>(contacts.length);
	//
	// final List<Contact> contactList = Arrays.stream(contacts)
	// .filter(entry -> StringUtils.hasText(entry.getTypeId()) &&
	// StringUtils.hasText(entry.getValue()))
	// .map(entry -> {
	//
	// Contact contact = null;
	// try {
	// contact = new Contact();
	// // If the contact id has been supplied, we update the address
	// if(StringUtils.hasText(entry.getId())){
	//
	// // Get the contact object
	// contact = contactService.get(entry.getId());
	// contactService.update(entry);
	//
	// }else{
	//
	// // No contact id has been provided so we create the contact
	// contactService.create(entry);
	//
	// }
	//
	// } catch (ContracticServiceException e) {
	// cseList.add(e);
	// }
	//
	// return contact;
	// }).collect(Collectors.toList());
	//
	// if(cseList.size()!=0)
	// throw cseList.get(0);
	//
	// return contactList;
	// }
	//
	public MessageDO update(final PersonDO pojo)
			throws ContracticServiceException {
		if (!StringUtils.hasText(pojo.getId())) {
			return new MessageDO(null, "Contractic",
					"Security data has been corrupted, can't continue",
					MessageDO.ERROR);
		}

		if (!StringUtils.hasText(pojo.getOrganisationId())) {
			return new MessageDO(null, "Contractic",
					"Organisation name can't be empty", MessageDO.ERROR);
		}

		final Long personId = this.personIdCache.get(pojo.getId());
		if (personId == null) {
			return new MessageDO(null, "Contractic",
					"Security data has been corrupted, can't continue",
					MessageDO.ERROR);
		}

		Person person = get(personId);
		person.setTitle(WordUtils.capitalize(pojo.getTitle()));
		person.setFirstName(WordUtils.capitalize(pojo.getFirstName()));
		person.setMiddleName(WordUtils.capitalize(pojo.getMiddleName()));
		person.setLastName(WordUtils.capitalize(pojo.getLastName()));
		person.setGender(pojo.getGender());
		person.setJobTitle(WordUtils.capitalize(pojo.getJobTitle()));
		person.setActive(pojo.getActive());

		try {
			personDAO.update(person);

			Module module = ModuleService.getModule(moduleDAO, Module.PERSON);
			saveDepartment(pojo, person, module);
			// saveContact(pojo, person, module);

			saveSearchEntity(person);

		} catch (ContracticModelException e) {
			// We should be rolling back here
			throw new ContracticServiceException(String.format(
					"Failed to update person: '%s'", pojo), e);
		}

		// final String icon, final String title, final String body, final
		// String type
		return new MessageDO(null, "Contractic", String.format(
				"Person '%s' updated successfully", pojo.getFirstName()),
				MessageDO.OK);
	}

	private void saveDepartment(final PersonDO pojo, final Person person,
			final Module module) throws ContracticServiceException,
			ContracticModelException {
		GroupSetDO departmentGroupSetDO = GroupSetService.getGroupSet(
				groupSetService, pojo.getOrganisationId(), GroupSet.DEPARTMENT);
		GroupSet departmentGroupSet = groupSetService.get(departmentGroupSetDO
				.getId());
		Long externalId = person.getId();

		List<ContracticServiceException> cse = new ArrayList<>();
		List<Group> groupList = Arrays
				.stream(pojo.getDepartment())
				.map(department -> {
					String groupId = department.getId();
					try {
						return groupService.get(groupId);
					} catch (Exception e) {
						cse.add(new ContracticServiceException(String.format(
								"Error occured when saving group object '%s'",
								department), e));
					}
					return null;
				}).collect(Collectors.toList());

		if (cse.size() > 0)
			throw cse.get(0);

		GroupService.saveObjectGroup(groupObjectDAO, externalId,
				groupList.toArray(new Group[0]), departmentGroupSet, module);
	}

	/*
	 * private void saveContact(final PersonDO pojo, final Person person, final
	 * Module module) throws ContracticServiceException,
	 * ContracticModelException {
	 * 
	 * ContactDO[] contacts = pojo.getContact(); if(contacts == null) return;
	 * Long externalId = person.getId(); List<ContracticServiceException> cse =
	 * new ArrayList<>(); List<Contact> contactList =
	 * Arrays.stream(contacts).map(contactDO -> { try { Contact contact = new
	 * Contact(); contact.setModule(module); contact.setActive(true);
	 * 
	 * ValueDO addressValueDO = contactDO.getAddress(); if(addressValueDO==null
	 * || !StringUtils.hasText(addressValueDO.getId())){ AddressDO addressDO =
	 * new AddressDO(); addressDO.setOrganisationId(pojo.getOrganisationId());
	 * addressDO.setTypeId(contactDO.getType().getId());
	 * addressDO.setSection1(contactDO.getValue());
	 * addressService.create(addressDO); addressValueDO = new
	 * ValueDO(addressDO.getId(), ""); contactDO.setAddress(addressValueDO); }
	 * 
	 * Address address = addressService.get(contactDO.getAddress().getId());
	 * contact.setAddress(address); return contact; } catch (Exception e) {
	 * cse.add(new ContracticServiceException(String.format(
	 * "Error occured when saving group object '%s'", contactDO), e)); } return
	 * null; }).collect(Collectors.toList());
	 * 
	 * if(cse.size()>0) throw cse.get(0);
	 * 
	 * ContactService.saveObjectContact(contactDAO, externalId, module,
	 * contactList.toArray(new Contact[0])); }
	 */

	public MessageDO delete(final PersonDO pojo)
			throws ContracticServiceException {
		Long personId = this.personIdCache.get(pojo.getId());

		if (personId == null) {
			// final String icon, final String title, final String body, final
			// String type
			return new MessageDO(null, "Internal system error",
					"Invalid user (may have been deleted)", MessageDO.WARNING);
		}

		final Person person = get(personId);

		try {
			// Delete from data source
			personDAO.delete(person);

			// if successfull, remove from Id cache
			this.personIdCache.remove(pojo.getId());

			return new MessageDO(null, "",
					String.format("Person '%s' deleted successfully!",
							person.getFirstName()), MessageDO.OK);
		} catch (Exception e) {
			throw new ContracticServiceException(
					String.format(
							"An error occurred when performing delete operation on person %s",
							person.getFirstName()), e);
		}
	}

	private void saveSearchEntity(Person person) {
		ConcurrencyUtil.IOBoundExecutorService.submit(() -> {
			SearchEntityDO searchEntity = new SearchEntityDO();
			searchEntity.setId(person.getId());
			searchEntity.setCollection(Module.PERSON);
			searchEntity.setOrganisationId(person.getOrganisation().getId());
			searchEntity.setText(String.join(
					",",
					new String[] { person.getFirstName(),
							person.getMiddleName(), person.getLastName(),
							person.getJobTitle() }));

			try {
				searchService.create(searchEntity);
			} catch (Exception e) {
				logger.warn(String.format(
						"An error occurred when indexing person '%s-%s-%s'",
						person.getFirstName(), person.getLastName(), person
								.getOrganisation().getName()), e);
			}
		});
	}

	/**
	 * 
	 * @param pojo
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO create(final PersonDO pojo)
			throws ContracticServiceException {

		// Construct the person object
		final Person person = new Person();

		person.setFirstName(WordUtils.capitalize(pojo.getFirstName()));
		person.setMiddleName(WordUtils.capitalize(pojo.getMiddleName()));
		person.setLastName(WordUtils.capitalize(pojo.getLastName()));
		person.setGender(pojo.getGender());
		person.setJobTitle(WordUtils.capitalize(pojo.getJobTitle()));
		person.setCreated(new Date());
		person.setTitle(WordUtils.capitalize(pojo.getTitle()));
		person.setActive(true);

		Organisation organisation = organisationService.get(pojo
				.getOrganisationId());
		person.setOrganisation(organisation);

		ValueDO returnValue = new ValueDO();
		// Save the person object
		try {
			personDAO.create(person);

			String hashId = HashingUtil.get(person.getId());
			// Update the cache
			this.personIdCache.put(hashId, person.getId());

			Module module = ModuleService.getModule(moduleDAO, Module.PERSON);
			saveDepartment(pojo, person, module);
			// saveContact(pojo, person, module);
			returnValue.setId(hashId);

			saveSearchEntity(person);
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			// We should be rolling back here
			throw new ContracticServiceException(String.format(
					"Failed to create person: %s", pojo), e);
		}

		// final String icon, final String title, final String body, final
		// String type
		MessageDO message = new MessageDO(null, "Contractic", String.format(
				"Person '%s' created successfully", pojo.getFirstName()),
				MessageDO.OK);
		message.setRef(returnValue);
		return message;
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.personIdCache = new Cache(PERSON_IDCACHE_NAME, String.class,
					Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(
					String.format("Initializing %s service failed", this
							.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	private void logSessionDeleteAction(UserDO userDO, String personId,
			String logMessage) throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService,
				Module.PERSON);
		CommonService.logCreateAction(auditService, userDO, personId, moduleDO,
				logMessage, null, null, null);
	}

	private void logSessionCreateAction(UserDO userDO, String personId,
			String logMessage) throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService,
				Module.PERSON);
		CommonService.logCreateAction(auditService, userDO, personId, moduleDO,
				logMessage, null, null, null);
	}

	private void logSessionReadAction(UserDO userDO, String personId,
			String logMessage) throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService,
				Module.PERSON);
		CommonService.logReadAction(auditService, userDO, personId, moduleDO,
				logMessage, null, null, null);
	}

	private void logSessionUpdateAction(UserDO userDO, String personId,
			String logMessage, String field, String from, String to)
			throws ContracticServiceException {
		ModuleDO moduleDO = CommonService.getModule(moduleService,
				Module.PERSON);
		CommonService.logUpdateAction(auditService, userDO, personId, moduleDO,
				logMessage, field, from, to);
	}

}
