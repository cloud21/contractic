package contractic.service.domain;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.StaxDriver;

@XStreamAlias("value")
public class ContractValueDO {
	private String actual;
	private String projected;
	public ContractValueDO(){}
	public ContractValueDO(final String projected, final String actual){
		this.actual = actual;
		this.projected = projected;
	}

	public String getActual() {
		return actual;
	}
	public void setActual(String actual) {
		this.actual = actual;
	}
	public String getProjected() {
		return projected;
	}
	public void setProjected(String projected) {
		this.projected = projected;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actual == null) ? 0 : actual.hashCode());
		result = prime * result
				+ ((projected == null) ? 0 : projected.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContractValueDO other = (ContractValueDO) obj;
		if (actual == null) {
			if (other.actual != null)
				return false;
		} else if (!actual.equals(other.actual))
			return false;
		if (projected == null) {
			if (other.projected != null)
				return false;
		} else if (!projected.equals(other.projected))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "[actual=" + actual + ", projected=" + projected
				+ "]";
	}
	/**
	 * Convert to ContractValue from XML
	 * @param xml
	 * @return
	 */
	public final static ContractValueDO fromXml(String xml){
		XStream xstream = new XStream();
		xstream.processAnnotations(ContractValueDO.class);
		return (ContractValueDO)xstream.fromXML(xml);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public final static String toXml(ContractValueDO value){
		XStream xstream = new XStream(new StaxDriver());
		xstream.processAnnotations(ContractValueDO.class);
		return xstream.toXML(value);
	}
	
}


