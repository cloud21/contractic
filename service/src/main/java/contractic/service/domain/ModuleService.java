package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Module;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class ModuleService implements DomainService<Module, Long, ModuleDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	private final static String MODULE_IDCACHE_NAME = "module.idcache.name";
	public static final String GET_PARAM_ID = "module.id";
	public static final String USERID = "user.id";

	private Cache<String, Long> moduleIdCache;
	
	@Autowired
	private CrudOperation<Module> moduleDAO;
	

	public MessageDO update(final ModuleDO pojo){
		return null;
	}
	
	public MessageDO create(final ModuleDO pojo){
		return null;
	}
	
	public static  Module getModule(final CrudOperation<Module> moduleDAO, String name) throws ContracticModelException{
		Module module = new Module();
		module.setName(name);
		List<Module> moduleList = moduleDAO.read(module);
		assert moduleList.size() == 1;
		return moduleList.get(0);
	}
	
	public Module get(final String hash) throws ContracticServiceException{
		final Long id = this.moduleIdCache.get(hash);
		return get(id);
	}
	
	public Module get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null module id supplied");
		
		Module module = new Module();
		module.setId(id);
		
		try {
			List<Module> modules = moduleDAO.read(module);
			return modules.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving user with id %d", id));
		}
	}
	
	private ModuleDO createDO(Module module) throws NoSuchAlgorithmException{
		ModuleDO moduleDO = new ModuleDO();
		String hashId = HashingUtil.get(module.getId());
		moduleDO.setId(hashId);
		moduleDO.setName(module.getName());
		moduleDO.setDescription(module.getDescription());
		return moduleDO;
	}
	
	public List<ModuleDO> get(final Map<String, Object> parameters) throws ContracticServiceException{
		final String uuid = (String)parameters.get(GET_PARAM_HASH);
		final String name = (String)parameters.get(GET_PARAM_NAME);
		final Long id = (Long)parameters.get(GET_PARAM_ID);
		
		List<Module> modules = null;
		try {
			Module module = new Module();
			
			if(StringUtils.hasText(name)){
				module.setName(name);
			}
			
			if(id != null){
				module.setId(id);
			}
			
			modules = moduleDAO.read(module);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read modules", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		List<ModuleDO> pojoList = modules.stream()
			.map(module -> {
				ModuleDO moduleDO = null;
				try{
					moduleDO = createDO(module);
					
					// Side effect 1
					moduleIdCache.put(moduleDO.getId(), module.getId());

				}catch(NoSuchAlgorithmException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the module %s", module)));
				}
				
				return moduleDO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
	}


	@Override
	public void init() throws ContracticServiceException {
		try {
			this.moduleIdCache = new Cache(MODULE_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	@Override
	public MessageDO delete(ModuleDO dojo) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}

}
