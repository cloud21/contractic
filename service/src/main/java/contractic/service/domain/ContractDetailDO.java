package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"user"})
public class ContractDetailDO {
	private String id;
	private String name;
	private String description;
	private ContractValueDO value;
	private PeriodDO period;
	private ContractCCNDO ccn;
	private ContractExtensionDO extension;
	private ContractTermPeriodDO term;
	/*
	 * This field is equivalent to the value field for none special bespoke detail.
	 * For example: For detail in the VALUE group, this field will be null but the 
	 * value field will be populated
	 */
	private String text;
	private ContractDO contract;
	private ValueDO group;
	private UserDO user;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ContractValueDO getValue() {
		return value;
	}
	public void setValue(ContractValueDO value) {
		this.value = value;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	public ContractDO getContract() {
		return contract;
	}
	public void setContract(ContractDO contract) {
		this.contract = contract;
	}
	public ValueDO getGroup() {
		return group;
	}
	public void setGroup(ValueDO group) {
		this.group = group;
	}
	public PeriodDO getPeriod() {
		return period;
	}
	public void setPeriod(PeriodDO period) {
		this.period = period;
	}
	public ContractCCNDO getCcn() {
		return ccn;
	}
	public void setCcn(ContractCCNDO ccn) {
		this.ccn = ccn;
	}
	
	public ContractExtensionDO getExtension() {
		return extension;
	}
	public void setExtension(ContractExtensionDO extension) {
		this.extension = extension;
	}
	public UserDO getUser() {
		return user;
	}
	public void setUser(UserDO user) {
		this.user = user;
	}
	public ContractTermPeriodDO getTerm() {
		return term;
	}
	public void setTerm(ContractTermPeriodDO term) {
		this.term = term;
	}
}
