package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Address;
import contractic.model.data.store.AddressType;
import contractic.model.data.store.Contact;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Supplier;
import contractic.model.data.store.SupplierPerson;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;


public final class SupplierPersonService implements DomainService<SupplierPerson, Long, SupplierPersonDO, MessageDO>{
	public final static String PERSON_IDCACHE_NAME = "person.idcache.name";

	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public static final String GET_PARAM_ID = "person.id";
	public static final String GET_PARAM_SEARCH_QUERY = "search.query";
	public static final String GET_PARAM_ACTIVE = "person.active";
	public static final String GET_PARAM_SUPPLIERID = "supplier.id";
	
	public static final String USERID = "user.id";
	
	// This cache helps us hide database IDs from each request
	private Cache<String, Long> personIdCache;
	
	// DAOs
	@Autowired
	private CrudOperation<SupplierPerson> supplierPersonDAO;
	
	// Services
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	
	@Autowired
	private DomainService<Address, Long, AddressDO, MessageDO> addressService;
	
	@Autowired
	private DomainService<Contact, Long, ContactDO, MessageDO> contactService;
	
	@Autowired
	private DomainService<AddressType, Long, StaticDO, MessageDO> addressTypeService;
	
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;

	@Autowired
	private DomainService<Supplier, Long, SupplierDO, MessageDO> supplierService;
	
//	public PersonDO getByUUID(final String orgId, final String personId) throws ContracticServiceException{
//		Long id = personIdCache.get(personId);
//		Organisation org = organisationService.get(orgId);
//		return get(org, id);
//	}
//	
	/**
	 * Retrieve a PersonDO
	 * @param id the datastore person Id
	 * @return
	 * @throws ContracticServiceException
	 */
	public SupplierPerson get(Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null person id supplied");
		
		SupplierPerson person = new SupplierPerson();
		person.setId(id);
		
		try {
			List<SupplierPerson> persons = supplierPersonDAO.read(person);
			return persons.get(0);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving use with id %d", id));
		}
	}
	
	private SupplierPersonDO makeDO(SupplierPerson person, UserDO userDO) throws NoSuchAlgorithmException, ContracticServiceException{

		String hashId = HashingUtil.get(person.getId());
		String orgId = userDO.getOrganisationId();
		SupplierPersonDO supplierPersonDO = new SupplierPersonDO();
		// Side effect 2
		supplierPersonDO.setId(hashId);
		supplierPersonDO.setOrganisationId(orgId);
		supplierPersonDO.setActive(Boolean.toString(person.getActive()));
		
		final Map<String, Object> params = new HashMap<>();
		params.put(DomainService.GET_PARAM_ID, person.getPerson().getId());
		params.put(CommonService.USER, userDO);
		List<PersonDO> personList = personService.get(params);
		assert personList.size() == 1;
		supplierPersonDO.setPerson(personList.get(0));
		
		return supplierPersonDO;
	}
	
	public List<SupplierPersonDO> get(final Map<String, Object> params) throws ContracticServiceException{
		
		final String hashId = (String)params.get(GET_PARAM_HASHID);
		final String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);
		final Long supplierPersonId = (Long)params.get(GET_PARAM_ID);
		final String supplierId = (String)params.get(GET_PARAM_SUPPLIERID);
		final Boolean active = (Boolean)params.get(GET_PARAM_ACTIVE);
		
		final UserDO userDO = (UserDO)params.get(CommonService.USER);
		if(userDO == null)
			throw new ContracticServiceException("Invalid session user provided");
		
		if(!StringUtils.hasText(organisationId)){
			throw new ContracticServiceException("OrganisationId not supplied");
		}
		
		List<SupplierPerson> persons = null;
		try {
			SupplierPerson person = new SupplierPerson();
			Supplier supplier = new Supplier();
			
			Organisation organisation = organisationService.get(organisationId);
			if(organisation == null)
				throw new ContracticServiceException("Invalid OrganisationId supplied");
			
			supplier.setOrganisation(organisation);
			
			if(supplierId!=null){
				supplier = supplierService.get(supplierId);
			}
			
			person.setSupplier(supplier);
			
			Long id = supplierPersonId;
			if(StringUtils.hasText(hashId)){
				id = this.personIdCache.get(hashId);
			}
			
			if(id != null){
				person.setId(id);
			}
			
			if(active != null){
				person.setActive(active);
			}
			
			persons = supplierPersonDAO.read(person);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read organisations", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		List<SupplierPersonDO> pojoList = persons.stream()
			.parallel()
			.map(person -> {
				SupplierPersonDO personPOJO = null;
				try{			
					personPOJO = makeDO(person, userDO);
					// Side effect ???
					personIdCache.put(personPOJO.getId(), person.getId());
					return personPOJO;
					
				}catch(NoSuchAlgorithmException | ContracticServiceException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the organisation %s", person), ex));
				}
				
				return personPOJO;
			}).collect(Collectors.toList());
		
		// This ensures we only return valid entry list
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
		
	}
	
	public SupplierPerson get(final String hashId) throws ContracticServiceException{
		Long id = personIdCache.get(hashId);

		if(id == null)
			throw new ContracticServiceException("Invalid id supplied (null)");
		SupplierPerson person = new SupplierPerson();
		person.setId(id);

		List<SupplierPerson> personList = null;
		try {
			personList = supplierPersonDAO.read(person);
			if(personList.size() != 1)
				return null;
			return personList.get(0);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while attempting to read organisation with id %s", id));
		}
	}

	public MessageDO update(final SupplierPersonDO pojo) throws ContracticServiceException {
		if(!StringUtils.hasText(pojo.getId())){
			return new MessageDO(null, "Contractic", "Security data has been corrupted, can't continue", MessageDO.ERROR);
		}
		
		if(!StringUtils.hasText(pojo.getOrganisationId())){
			return new MessageDO(null, "Contractic", "Organisation name can't be empty", MessageDO.ERROR);
		}
		
		final Long personId = this.personIdCache.get(pojo.getId());
		if(personId == null){
			return new MessageDO(null, "Contractic", "Security data has been corrupted, can't continue", MessageDO.ERROR);
		}
			

		Organisation organisation = organisationService.get(pojo.getOrganisationId());
		SupplierPerson supplierPerson = new SupplierPerson();
		try{
		
			supplierPerson.setId(personId);
			
			List<SupplierPerson> personList = this.supplierPersonDAO.read(supplierPerson);
			if(personList.size() != 1){
				return new MessageDO(null, "Contractic", "No such person exists. Data corruption detected", MessageDO.ERROR);
			}
			
			supplierPerson = personList.get(0);			
			supplierPerson.setExternalRef(pojo.getExternalRef());
			supplierPerson.setActive(Boolean.parseBoolean(pojo.getActive()));
			
			supplierPersonDAO.update(supplierPerson);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update person: '%s'", pojo), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", "Supplier person updated successfully", MessageDO.OK);
	}
	
	public MessageDO delete(final SupplierPersonDO pojo) throws ContracticServiceException{
		Long personId = this.personIdCache.get(pojo.getId());

		if(personId == null){
			//final String icon, final String title, final String body, final String type
			return new MessageDO(null, "Internal system error", "Invalid user (may have been deleted)", MessageDO.WARNING);
		}
		
		final SupplierPerson person = new SupplierPerson();
		person.setId(personId);
		
		try {
			// Delete from data source
			supplierPersonDAO.delete(person);
			
			// if successfull, remove from Id cache
			this.personIdCache.remove(pojo.getId());
			
			return new MessageDO(null, "", "Person deleted successfully!", MessageDO.OK);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occurred when performing delete operation on person object %s", person), e);
		}
	}
	
	/**
	 * 
	 * @param pojo
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO create(final SupplierPersonDO pojo) throws ContracticServiceException{
		
		// Construct the person object
		final SupplierPerson supplierPerson = new SupplierPerson();
		
		Supplier supplier = supplierService.get(pojo.getSupplier().getId());
		supplierPerson.setSupplier(supplier);
		if(pojo.getPerson()!=null){
			Person person = personService.get(pojo.getPerson().getId());
			supplierPerson.setPerson(person);
		}
		supplierPerson.setCreated(new Date());
		supplierPerson.setExternalRef(pojo.getExternalRef());
		supplierPerson.setActive(true);
		
		// Save the person object
		try {
			supplierPersonDAO.create(supplierPerson);
			
			String hashId = HashingUtil.get(supplierPerson.getId());
			// Update the cache
			this.personIdCache.put(hashId, supplierPerson.getId());
					
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create person: %s", pojo), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", "Person created successfully", MessageDO.OK);
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.personIdCache = new Cache(PERSON_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub
		
	}


}
