package contractic.service.domain;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.UserRole;
import contractic.model.data.store.UserRolePermission;
import contractic.model.data.store.UserRolePermissionAction;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class UserRolePermissionService implements DomainService<UserRolePermission, Long, UserRolePermissionDO, MessageDO> {

	public final static String GET_PARAM_USERNAME = "username";
	public final static String GET_PARAM_ORGANISATIONID = "organisation.id";
	
	private final static String USER_ROLE_PERMISSION_IDCACHE_NAME = "user.role.permission.idcache.name";
	public static final String GET_PARAM_ID = "user.id";
	public static final String GET_PARAM_MODULEID = "module.id";
	public static final String GET_PARAM_ROLEID = "role.id";
	private Cache<String, Long> userRoleIdCache;
	
	@Autowired
	private CrudOperation<UserRolePermission> userRolePermissionDAO;
	
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	@Autowired
	private DomainService<UserRole, Long, UserRoleDO, MessageDO> userRoleService;
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	
	@Autowired
	private CommonService commonService;

	/**
	 * Get a UserRolePermission Domain Object.
	 * @param params Keys can be login, password, uuid
	 * @return
	 * @throws ContracticServiceException 
	 */
	public List<UserRolePermissionDO> get(final Map<String, Object> params) throws ContracticServiceException{
		final String hash = (String)params.get(GET_PARAM_HASHID);
		final String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);
		Long id = (Long)params.get(GET_PARAM_ID);
		final String moduleId = (String)params.get(GET_PARAM_MODULEID);
		final Long roleId = (Long)params.get(GET_PARAM_ROLEID);
		
		/**
		 * No need for UserDO. Anyone can read user permissions.
		 */
		
		if(!StringUtils.hasText(organisationId))
			throw new ContracticServiceException("Organisation must be supplied");
		
		List<UserRolePermission> users = null;
		try {
			UserRolePermission user = new UserRolePermission();
			
			Organisation organisation = organisationService.get(organisationId);
			
			if(roleId!=null){
				/**
				 * Here the organisation will be set
				 */
				UserRole userRole = userRoleService.get(roleId);
				user.setUserRole(userRole);
			}else{
				/**
				 * We need this assignment so that we make sure we are getting this organisation's user roles
				 */
				UserRole role = new UserRole();
				role.setOrganisation(organisation);
				user.setUserRole(role);
			}
			
			if(StringUtils.hasText(hash)){
				id = userRoleIdCache.get(hash);
				user.setId(id);
			}
			
			if(StringUtils.hasText(moduleId)){
				Module module = moduleService.get(moduleId);
				user.setModule(module);
			}
			
			users = userRolePermissionDAO.read(user);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read organisations", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		List<UserRolePermissionDO> pojoList = users.stream()
			.map(user -> {
				UserRolePermissionDO userPOJO = null;
				try{
					userPOJO = new UserRolePermissionDO();				
					userPOJO = makeDO(user, organisationId);
					userRoleIdCache.put(userPOJO.getId(), user.getId());
					
				}catch(ContracticServiceException ce){
					cse.add(ce);
				}catch(NoSuchAlgorithmException |IOException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the user %s", user), ex));
				}
				
				return userPOJO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
		
	}
	
	private UserRolePermissionDO makeDO(UserRolePermission user, String organisationId) throws NoSuchAlgorithmException, ContracticServiceException, JsonParseException, JsonMappingException, IOException{
		UserRolePermissionDO userPOJO = new UserRolePermissionDO();
		String hashId = HashingUtil.get(user.getId());
		
		userPOJO.setId(hashId);
		userPOJO.setRead(user.getRead());
		userPOJO.setWrite(user.getWrite());
		userPOJO.setDelete(user.getDelete());
		
		// Get the ModuleID
		Module module = user.getModule();
		if(module == null){
			throw new ContracticServiceException(String.format("User role supplied without a module: %s", user));
		}
		
		final Map<String, Object> params = new HashMap<>();
		params.put(ModuleService.GET_PARAM_ID, module.getId());
		final List<ModuleDO> moduleList = moduleService.get(params);
		assert(moduleList.size() == 1);
		final ModuleDO moduleDO = moduleList.get(0);
		userPOJO.setModule(new ValueDO(moduleDO.getId(), moduleDO.getName()));
		
/*		params.clear();
		params.put(UserRoleService.GET_PARAM_ID, user.getUserRole().getId());
		params.put(UserRoleService.GET_PARAM_ORGANISATIONID, organisationId);
		final List<UserRoleDO> roleList = userRoleService.get(params);
		assert(moduleList.size() == 1);
		final UserRoleDO roleDO = roleList.get(0);
		userPOJO.setRole(roleDO);*/
		
		return userPOJO;
	}
	
	public UserRolePermission get(final String uuid) throws ContracticServiceException{
		final Long id = this.userRoleIdCache.get(uuid);
		return get(id);
	}
	
	public UserRolePermission get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		UserRolePermission user = new UserRolePermission();
		user.setId(id);
		
		try {
			List<UserRolePermission> users = userRolePermissionDAO.read(user);
			return users.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving user with id %d", id), e);
		}
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.userRoleIdCache = new Cache(USER_ROLE_PERMISSION_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	/**
	 * Also see UserRole#create function
	 */
	public MessageDO create(UserRolePermissionDO roleDO) throws ContracticServiceException{
		
		UserDO userDO = roleDO.getUser();
		
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		// Construct the person object
		final UserRolePermission rolePermission = new UserRolePermission();
		
		rolePermission.setRead(roleDO.getRead());
		rolePermission.setWrite(roleDO.getWrite());
		rolePermission.setDelete(roleDO.getDelete());
		
		Module module = moduleService.get(roleDO.getModule().getId());
		rolePermission.setModule(module);
		if(roleDO.getExternalId()!=null){
			Long externalId = commonService.getExternalId(roleDO.getExternalId(), module);
			rolePermission.setExternalId(externalId);
		}
		UserRole userRole = userRoleService.get(roleDO.getRole().getId());
		rolePermission.setUserRole(userRole);
		
		// Save the person object
		try {
			userRolePermissionDAO.create(rolePermission);
			
			String hashId = HashingUtil.get(rolePermission.getId());
			// Update the cache
			this.userRoleIdCache.put(hashId, rolePermission.getId());
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create user role: %s", roleDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", "Note created successfully", MessageDO.OK);
	}

	public MessageDO update(UserRolePermissionDO userRoleDO) throws ContracticServiceException{
		UserDO userDO = userRoleDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		// Construct the object
		UserRolePermission userRole = get(userRoleDO.getId());
		
		// Save the person object
		try {
			
			// Get the most recent view of the note
			List<UserRolePermission> noteList = userRolePermissionDAO.read(userRole);
			assert(noteList.size() == 1);
			userRole = noteList.get(0);
			
			// Now update fields
			Module module = moduleService.get(userRoleDO.getModule().getId());
			userRole.setModule(module);
			userRole.setRead(userRoleDO.getRead());
			userRole.setWrite(userRoleDO.getWrite());
			userRole.setDelete(userRoleDO.getDelete());

			userRolePermissionDAO.update(userRole);
			
			String hashId = HashingUtil.get(userRole.getId());
			// Update the cache
			this.userRoleIdCache.put(hashId, userRole.getId());
			
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create note: %s", userRoleDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", "Note updated successfully", MessageDO.OK);
	}
	
	public MessageDO delete(UserRolePermissionDO permissionDO) throws ContracticServiceException{
		
		UserDO userDO = permissionDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		// Construct the object
		UserRolePermission userRole = get(permissionDO.getId());
		
		// Save the object
		try {
			
			// Get the most recent view
			List<UserRolePermission> noteList = userRolePermissionDAO.read(userRole);
			assert(noteList.size() == 1);
			userRole = noteList.get(0);
			
			userRolePermissionDAO.delete(userRole);
			
			String hashId = HashingUtil.get(userRole.getId());
			// Update the cache
			this.userRoleIdCache.remove(hashId);
			
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to delete user role: %s", permissionDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", "Role updated successfully", MessageDO.OK);
	}

}
