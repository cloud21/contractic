package contractic.service.domain;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.dao.OrganisationDAO;
import contractic.model.data.store.Organisation;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public final class OrganisationService implements DomainService<Organisation, Long, OrganisationDO, MessageDO>{
	
	public static final String GET_PARAM_HASH = "hash.id";
	public static final String GET_PARAM_ORGANISATION = "organisation";
	public static final String ORGANISATION_IDCACHE_NAME = "organisation.idcache.name";
	public static final String GET_PARAM_ID = "organisation.id";
	public static final String GET_PARAM_CODE = "organisation.appcode";
	
	// This cache helps us hide database IDs from each request
	private Cache<String, Long> organisationIdCache;
	
	@Autowired
	private CrudOperation<Organisation> organisationDAO;
	
/*	// Public methods accessible to controllers
	public OrganisationDO getByUUID(String uuid) throws ContracticServiceException{
		Long id = organisationIdCache.get(uuid);
		return get(id);
	}*/
	
	public List<OrganisationDO> get() throws ContracticServiceException{
		List<Organisation> organisations = null;
		try {
			organisations = organisationDAO.read(new Organisation());
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read organisations", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();

		organisations.stream()
			.forEach(org -> {
				try{
					organisationIdCache.put(HashingUtil.get(org.getId()), org.getId());
				}catch(NoSuchAlgorithmException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the organisation %s", org)));
				}
			});
		
		// If any exceptions were thrown, throw the first one here
		if(cse.size()!=0)
			throw cse.get(0);
	
		List<OrganisationDO> pojoList = organisations.stream()
			.map(org -> {
				OrganisationDO orgPOJO = null;
				try{
					orgPOJO = new OrganisationDO();
					String hashId = HashingUtil.get(org.getId());
					
					// Side effect 1
					organisationIdCache.put(hashId, org.getId());
					
					// Side effect 2
					orgPOJO.setId(hashId);
					orgPOJO.setName(org.getName());
					orgPOJO.setCreated(org.getCreated().toString());
					orgPOJO.setActive(Boolean.toString(org.getActive()));
				}catch(NoSuchAlgorithmException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the organisation %s", org)));
				}
				
				return orgPOJO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
	}
	
	public List<OrganisationDO> get(final Map<String, Object> params) throws ContracticServiceException{
		
		final String hashId = (String)params.get(GET_PARAM_HASH);
		Long id = (Long)params.get(GET_PARAM_ID);
		final String applicationCode = (String)params.get(GET_PARAM_CODE);
		
		List<Organisation> organisations = null;
		try {
			Organisation organisation = new Organisation();
			
			if(StringUtils.hasText(hashId) && organisation.getId() == null){
				id = this.organisationIdCache.get(hashId);
				organisation.setId(id);
			}
			
			if(StringUtils.hasText(applicationCode))
				organisation.setCode(applicationCode);
			
			if(id!=null)
				organisation.setId(id);
			
			organisations = organisationDAO.read(organisation);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read organisations", e);
		}

		List<ContracticServiceException> cse = new ArrayList<>();
		
		List<OrganisationDO> pojoList = organisations.stream()
			.map(org -> {
				OrganisationDO orgPOJO = null;
				try{
					
					
					// Side effect 1
					
					
					// Side effect 2
					orgPOJO = makeDO(org);
					
					organisationIdCache.put(orgPOJO.getId(), org.getId());
				}catch(Exception ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the organisation %s", org), ex));
				}
				
				return orgPOJO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
	}

	private OrganisationDO makeDO(Organisation org) throws NoSuchAlgorithmException, JsonParseException, JsonMappingException, IOException {
		
		OrganisationDO orgPOJO = new OrganisationDO();
		String hashId = HashingUtil.get(org.getId());
		
		if(org.getProfile() == null)
			orgPOJO.setProfile(new HashMap<String, Object>());
		else{
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> profile = mapper.readValue(org.getProfile(),
				                    new TypeReference<Map<String, Object>>() {
				                    });
			orgPOJO.setProfile(profile);
		}
		
		orgPOJO.setId(hashId);
		orgPOJO.setName(org.getName());
		orgPOJO.setCreated(org.getCreated().toString());
		orgPOJO.setActive(Boolean.toString(org.getActive()));
		orgPOJO.setCode(org.getCode());
		
		return orgPOJO;
	}
	
	/**
	 * Update an existing organisation
	 * @param pojo
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO update(OrganisationDO pojo) throws ContracticServiceException{
		
		if(!StringUtils.hasText(pojo.getId())){
			return new MessageDO(null, "Contractic", "Security data has been corrupted, can't continue", MessageDO.ERROR);
		}
		
		if(!StringUtils.hasText(pojo.getName())){
			return new MessageDO(null, "Contractic", "Organisation name can't be empty", MessageDO.ERROR);
		}
		
		Long id = this.organisationIdCache.get(pojo.getId());
		if(id == null){
			return new MessageDO(null, "Contractic", "Security data has been corrupted, can't continue", MessageDO.ERROR);
		}
			

		Organisation organisation = new Organisation();
		try{
		
		organisation.setId(id);
		List<Organisation> orgList = this.organisationDAO.read(organisation);
		if(orgList.size() != 1){
			return new MessageDO(null, "Contractic", "No such organisation exists. Data corruption detected", MessageDO.ERROR);
		}
		assert(orgList.size() == 1);
		organisation = orgList.get(0);
		organisation.setName(pojo.getName());
		organisation.setActive(Boolean.parseBoolean(pojo.getActive()));
				} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update organisation: '%s'", pojo), e);
		}
		
		try {
			if(pojo.getProfile()!=null){
				ObjectMapper objectMapper = new ObjectMapper();
				String profileString = objectMapper.writeValueAsString(pojo.getProfile());
				organisation.setProfile(profileString);
			}		
			organisationDAO.update(organisation);
		} catch (ContracticModelException | JsonProcessingException e) {
			throw new ContracticServiceException(String.format("Failed to update organisation: '%s'", pojo), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Organisation '%s' updated successfully", pojo.getName()), MessageDO.OK);
	}
	
	/**
	 * Create a new organisation
	 * @param pojo
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO create(OrganisationDO pojo) throws ContracticServiceException{
		final Organisation organisation = new Organisation();
		
		if(!StringUtils.hasText(pojo.getName())){
			return new MessageDO(null, "Contractic", "Organisation name can't be empty", MessageDO.ERROR);
		}
		
		organisation.setName(pojo.getName());
		organisation.setActive(Boolean.parseBoolean(pojo.getActive()));
		organisation.setCreated(new Date());
		
		try {
			organisationDAO.create(organisation);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create the organisation: %s", pojo), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Organisation %s created successfully", pojo.getName()), MessageDO.OK);
	}
	
	// Protected methods to be used only by this class and other classes within the .domain package
//	protected List<Organisation> get(Long id) throws ContracticServiceException{
//		if(id == null)
//			throw new ContracticServiceException("Invalid id supplier (null)");
//		Organisation org = new Organisation();
//		org.setId(id);
//
//		try {
//			return organisationDAO.read(org);
//		} catch (ContracticModelException e) {
//			throw new ContracticServiceException(String.format("An error occured while attempting to read organisation with id %s", id));
//		}
//	}
	
	public Organisation get(String uuid) throws ContracticServiceException{
		Long id = this.organisationIdCache.get(uuid);
		if(id == null)
			throw new ContracticServiceException("Invalid id supplied (null)");
		Organisation org = new Organisation();
		org.setId(id);

		List<Organisation> orgList = null;
		try {
			orgList = organisationDAO.read(org);
			if(orgList.size() != 1)
				return null;
			return orgList.get(0);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while attempting to read organisation with id %s", id));
		}
	}
	
	public Organisation get(Long id) throws ContracticServiceException{
		
		if(id == null)
			throw new ContracticServiceException("Invalid id supplier (null)");
		Organisation org = new Organisation();
		org.setId(id);

		List<Organisation> orgList = null;
		try {
			orgList = organisationDAO.read(org);
			if(orgList.size() != 1)
				return null;
			return orgList.get(0);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while attempting to read organisation with id %s", id));
		}
		
/*		try {
			OrganisationDO orgPOJO = makeDO(org);
			this.organisationIdCache.put(orgPOJO.getId(), id);
			return orgPOJO;
		} catch (NoSuchAlgorithmException e) {
			throw new ContracticServiceException(String.format("An error occured while obtaining hash for organisation id %s", id));
		}*/
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.organisationIdCache = new Cache(ORGANISATION_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// Delete cache here
		
	}

	@Override
	public MessageDO delete(OrganisationDO dojo)
			throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}
	
	// Private utility methods
}
