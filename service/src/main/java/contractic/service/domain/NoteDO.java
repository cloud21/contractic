package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"sessionUser"})
public class NoteDO {
	private String id;
	private String header;
	private String detail;
	private String externalId;
	private ValueDO type;
	private String moduleId;
	private Boolean active;
	private String created;
	private ValueDO user;
	private UserDO sessionUser;
	private Object external;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public ValueDO getType() {
		return type;
	}
	public void setType(ValueDO type) {
		this.type = type;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public ValueDO getUser() {
		return user;
	}
	public void setUser(ValueDO user) {
		this.user = user;
	}
	public UserDO getSessionUser() {
		return sessionUser;
	}
	public void setSessionUser(UserDO sessionUser) {
		this.sessionUser = sessionUser;
	}
	public Object getExternal() {
		return external;
	}
	public void setExternal(Object external) {
		this.external = external;
	}
	
}
