package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Address;
import contractic.model.data.store.Contact;
import contractic.model.data.store.Task;
import contractic.model.data.store.TaskContact;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class TaskContactService implements DomainService<TaskContact, Long, TaskContactDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	public final static String GET_PARAM_ID = "id";
	public static final String GET_PARAM_MODULE = "module";
	public static final String GET_PARAM_EXTERNALID = "externalId";

	public final static String TASKCONTACT_IDCACHE_NAME = "task.contact.idcache.name";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public static final String GET_PARAM_TASKID = "task.id";
	public static final String USERID = "user.id";

	private Cache<String, Long> taskContactIdCache;

	@Autowired
	private CrudOperation<TaskContact> taskContactDAO;
	@Autowired
	private DomainService<Contact, Long, ContactDO, MessageDO> contactService;
	@Autowired
	private DomainService<Address, Long, AddressDO, MessageDO> addressService;
	@Autowired
	private DomainService<Task, Long, TaskDO, MessageDO> taskService;

	/**
	 * Updates the contact supplied. Note we currently only update the details of the underlying address.
	 * @param pojo
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO update(final TaskContactDO pojo) throws ContracticServiceException {
		
		// Get the module object for 'Person'
		//final Module module = moduleService.get(pojo.getModuleId());
		
		// Get the contact object
		TaskContact taskContact = get(pojo.getId());
		taskContact.setUpdated(new Date());
		
		return new MessageDO(null, "Contractic", String.format("Contact '%' updated successfully", pojo.getContact().getValue()), MessageDO.OK);
	}

	/**
	 * Updates the contact supplied. Note we currently only update the details of the underlying address.
	 * @param pojo
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO delete(final TaskContactDO pojo) throws ContracticServiceException {
		
		// Get the module object for 'Person'
		//final Module module = moduleService.get(pojo.getModuleId());
		
		TaskContact contact = get(pojo.getId());
		try {
			taskContactDAO.delete(contact);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("Failed to delete task contact");
		}
		
		return new MessageDO(null, "Contractic", "Task contact deleted successfully", MessageDO.OK);
	}
	
	public MessageDO create(final TaskContactDO taskContactDO) throws ContracticServiceException {

		TaskContact taskContact = new TaskContact();
		
		// Get the contact
		Contact contact = null;
		ValueDO contactDO = taskContactDO.getContact();
		
		if(!StringUtils.hasText(contactDO.getId()) || !StringUtils.hasText(contactDO.getValue()))
			throw new ContracticServiceException("Invalid contact supplied");
		
		if(StringUtils.hasText(contactDO.getId())){
			contact = contactService.get(contactDO.getId());
		}else if(StringUtils.hasText(contactDO.getValue())){
/*			Map<String, Object> params = new HashMap<>();
			params.put(AddressService.GET_PARAM_SEARCH_QUERY, contactDO.getValue());
			params.put(AddressService.GET_PARAM_ORGANISATIONID, taskContactDO.getOrganisationId());
			
			ValueDO typeDO = taskContactDO.getType();
			if(typeDO!=null)
				params.put(AddressService.GET_PARAM_TYPEID, typeDO.getId());
			
			List<AddressDO> addressList = addressService.get(params);
			assert addressList.size() == 1;*/
			
		}
		taskContact.setContact(contact);
		
		final Task task = taskService.get(taskContactDO.getTask().getId());
		taskContact.setTask(task);
		taskContact.setUpdated(new Date());
		
		// Now save the contact
		try {
			taskContactDAO.create(taskContact);
			
			// Now updat the ID cache
			String hashId = HashingUtil.get(taskContact.getId());
			taskContactDO.setId(hashId);
			this.taskContactIdCache.put(hashId, taskContact.getId());
			
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to create the contact %s", taskContactDO), e);
		}
			
		return new MessageDO(null, "Contractic", String.format("Task contact '%s' created successfully", taskContactDO.getContact().getValue()), MessageDO.OK);
		
	}

	public TaskContact get(final String hash) throws ContracticServiceException {
		final Long id = this.taskContactIdCache.get(hash);
		return get(id);
	}

	public TaskContact get(final Long id) throws ContracticServiceException {
		if (id == null)
			throw new ContracticServiceException("Null contact id supplied");

		TaskContact contact = new TaskContact();
		contact.setId(id);

		try {
			List<TaskContact> contacts = taskContactDAO.read(contact);
			return contacts.get(0);

		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"An error occured while retrieving contact with id %d", id));
		}
	}

	private TaskContactDO makeDO(TaskContact contact, String organisationId) throws NoSuchAlgorithmException,
			ContracticServiceException {
		TaskContactDO taskContactDO = new TaskContactDO();

		String hashId = HashingUtil.get(contact.getId());

		// Set the contact hash key
		taskContactDO.setId(hashId);
		taskContactDO.setOrganisationId(organisationId);
		
		final Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(AddressService.GET_PARAM_ID, contact.getContact().getAddress().getId());
		paramMap.put(AddressService.GET_PARAM_ORGANISATIONID, organisationId);
		List<AddressDO> addressList = addressService.get(paramMap);
		assert addressList.size() == 1;
		AddressDO addressDO = addressList.get(0);
		taskContactDO.setContact(new ValueDO(addressDO.getId(), addressDO.stringify()));
		taskContactDO.setType(addressDO.getType());

		
/*		paramMap.clear();
		paramMap.put(TaskService.GET_PARAM_ID, contact.getTask().getId());
		paramMap.put(TaskService.GET_PARAM_ORGANISATIONID, organisationId);

		List<TaskDO> taskList = taskService.get(paramMap);
		assert taskList.size() == 1;
		TaskDO taskDO = taskList.get(0);
		taskContactDO.setTask(new ValueDO(taskDO.getId(), taskDO.getTitle()));	*/	
		
		return taskContactDO;
	}

	public List<TaskContactDO> get(final Map<String, Object> params)
			throws ContracticServiceException {

		final String hash = (String) params.get(GET_PARAM_HASH);
		Long id = (Long) params.get(GET_PARAM_ID);
		final String taskId = (String)params.get(GET_PARAM_TASKID);
		final String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);

		if(!StringUtils.hasText(organisationId)){
			throw new ContracticServiceException("OrganisationId not supplied");
		}
		
		List<TaskContact> contacts = null;
		try {
			TaskContact contact = new TaskContact();

			if(StringUtils.hasText(hash)){
				id = this.taskContactIdCache.get(hash);
			}
			
			if(id != null)
				contact.setId(id);
			
			if(StringUtils.hasText(taskId)){
				Task task = taskService.get(taskId);
				assert task!=null;
				contact.setTask(task);
			}
						
			contacts = taskContactDAO.read(contact);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(
					"An error occured while attempting to read contacts", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();

		final List<TaskContactDO> pojoList = contacts
				.stream()
				.map(contact -> {

					TaskContactDO contactDO = null;
					try {
						contactDO = makeDO(contact, organisationId);
						// Side effect 1
						taskContactIdCache.put(contactDO.getId(), contact.getId());
					} catch (NoSuchAlgorithmException | ContracticServiceException ex) {
						cse.add(new ContracticServiceException(String.format(
								"An error occured when hashing the contact %s",
								contact)));
					}

					return contactDO;
				}).collect(Collectors.toList());

		if (cse.size() != 0)
			throw cse.get(0);

		return pojoList;
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.taskContactIdCache = new Cache(TASKCONTACT_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(
					String.format("Initializing %s service failed", this
							.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

}
