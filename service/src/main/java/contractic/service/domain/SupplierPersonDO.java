package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"organisationId"})
public class SupplierPersonDO {
	
	private String id;
	private String externalRef;
	private String active;
	private String organisationId;
	private PersonDO person;
	private ValueDO supplier;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getExternalRef() {
		return externalRef;
	}
	public void setExternalRef(String externalRef) {
		this.externalRef = externalRef;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	public PersonDO getPerson() {
		return person;
	}
	public void setPerson(PersonDO person) {
		this.person = person;
	}
	public ValueDO getSupplier() {
		return supplier;
	}
	public void setSupplier(ValueDO supplier) {
		this.supplier = supplier;
	}
	
		
}
