package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Status;
import contractic.model.data.store.UserRolePermissionAction;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class StatusService implements DomainService<Status, Long, StatusDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	public final static String DOCUMENTTYPE_IDCACHE_NAME = "status.idcache.name";
	public static final String GET_PARAM_ID = "status.id";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public static final String GET_PARAM_MODULEID = "module.id";
	public static final String USERID = "user.id";
	
	private Cache<String, Long> statusIdCache;
	
	@Autowired
	private CommonService commonService;
	@Autowired
	private CrudOperation<Status> statusDAO;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	

	public Status get(final String uuid) throws ContracticServiceException{
		final Long id = this.statusIdCache.get(uuid);
		return get(id);
	}
	
	public Status get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null user id supplied");
		
		Status status = new Status();
		status.setId(id);
		
		try {
			List<Status> statusList = statusDAO.read(status);
			return statusList.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving status with id %d", id));
		}
	}
	
	
	private StatusDO makeDO(final Status status) throws NoSuchAlgorithmException, ContracticServiceException{
		String hashId = HashingUtil.get(status.getId());
		StatusDO statusDO = new StatusDO();
		statusDO.setId(hashId);
		statusDO.setName(status.getName());
		statusDO.setDescription(status.getDescription());
		statusDO.setActive(status.getActive());
		
		Map<String, Object> params = new HashMap<>();
		params.put(ModuleService.GET_PARAM_ID, status.getModule().getId());
		List<ModuleDO> moduleList = moduleService.get(params);
		assert(moduleList.size() == 1);
		
		statusDO.setModuleId(moduleList.get(0).getId());
		
		return statusDO;
	}
	
	public List<StatusDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASH);
		 final String name = (String)params.get(GET_PARAM_NAME);
		 Long id = (Long)params.get(GET_PARAM_ID);
		 
		 final String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);
		 final String moduleId = (String)params.get(GET_PARAM_MODULEID);

			if(!StringUtils.hasText(organisationId)){
				throw new ContracticServiceException("OrganisationId not supplied");
			}
			List<Status> statusList = null;
			try {
				Status status = new Status();
				
				Organisation organisation = organisationService.get(organisationId);
				status.setOrganisation(organisation);
				
				
				
				if(StringUtils.hasText(moduleId)){
					Module module = moduleService.get(moduleId);
					status.setModule(module);
				}
				
				if(StringUtils.hasText(name)){
					status.setName(name);
				}
							
				if(StringUtils.hasText(hash)){
					id = this.statusIdCache.get(hash);
				}
				
				if(id !=null){
					status.setId(id);
				}
								
				statusList = statusDAO.read(status);
			} catch (ContracticModelException e) {
				throw new ContracticServiceException("An error occured while attempting to read statuses", e);
			}

			// List of all exceptions that occur in the predicate
			List<ContracticServiceException> cse = new ArrayList<>();
		
			final List<StatusDO> pojoList = statusList.stream()
				.map(status -> {
					StatusDO statusDO = null;
					try{
						statusDO = makeDO(status);						
						// Side effect 1
						statusIdCache.put(statusDO.getId(), status.getId());

					}catch(final ContracticServiceException | NoSuchAlgorithmException ex){
						cse.add(new ContracticServiceException(String.format("An error occured when hashing the status %s", status)));
					}
					
					return statusDO;
				}).collect(Collectors.toList());
			
			if(cse.size()!=0)
				throw cse.get(0);
			
			return pojoList;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.statusIdCache = new Cache(DOCUMENTTYPE_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	public MessageDO delete(StatusDO statusDO) throws ContracticServiceException {
		
		UserDO userDO = statusDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.DELETE, Module.ORGANISATION);
		
		final String hash = statusDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("");
			
		final Long id = statusIdCache.get(hash);	
		
		final String organisationId = statusDO.getOrganisationId();
		
		// Save the person object
		Status status = null;
		try {

			status = new Status();
			status.setId(id);
			Organisation organisation = organisationService.get(organisationId);
			status.setOrganisation(organisation);
			List<Status> statusList = statusDAO.read(status);
			assert(statusList.size()==1);
			status = statusList.get(0);
			
			statusDAO.delete(status);
		
		} catch (Exception e) {
			String message = String.format("Failed to delete status: %s", status.getName());
			MessageDO messageDO = new MessageDO(null, "Contractic", message, MessageDO.ERROR);
			throw new ContracticServiceException(message, messageDO, e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Status '%s' deleted successfully", status.getName()), MessageDO.OK);
	}

	public MessageDO update(StatusDO statusDO) throws ContracticServiceException {
		
		UserDO userDO = statusDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		final String hash = statusDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Invalid status hash id provided");
					
		final String organisationId = statusDO.getOrganisationId();
		if(!StringUtils.hasText(organisationId))
			throw new ContracticServiceException("Invalid organisation hash id provided");
		
		final Long id = statusIdCache.get(hash);
		
		// Save the person object
		Status status = null;
		try {

			status = new Status();
			status.setId(id);
			Organisation organisation = organisationService.get(organisationId);
			status.setOrganisation(organisation);
			List<Status> statusList = statusDAO.read(status);
			assert(statusList.size()==1);
			status = statusList.get(0);
			
			status.setName(statusDO.getName());
			status.setDescription(statusDO.getDescription());
			status.setActive(statusDO.getActive());
			status.setModule(moduleService.get(statusDO.getModuleId()));
			
			statusDAO.update(status);
		
		} catch (Exception e) {
			String message = String.format("Failed to update status: %s", status.getName());
			MessageDO messageDO = new MessageDO(null, "Contractic", message, MessageDO.ERROR);
			throw new ContracticServiceException(message, messageDO, e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Status '%s' updated successfully", statusDO.getName()), MessageDO.OK);
	}

	/**
	 * Creates the status. Validation of input is left to the client (e.g. controllers)
	 * @param statusDO
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO create(StatusDO statusDO) throws ContracticServiceException {
		
		UserDO userDO = statusDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		final Status status = new Status();
		status.setName(statusDO.getName());
		status.setDescription(statusDO.getDescription());
		status.setActive(statusDO.getActive());
		
		// Set the organisation
		Organisation organisation = organisationService.get(statusDO.getOrganisationId());
		status.setOrganisation(organisation);
		
		// Set the module
		Module module = moduleService.get(statusDO.getModuleId());
		status.setModule(module);
		
		// Save the person object
		try {
			statusDAO.create(status);
			
			String hashId = HashingUtil.get(status.getId());
			// Update the cache
			this.statusIdCache.put(hashId, status.getId());
		
		} catch (Exception e) {
			String message = String.format("Failed to create status: %s", statusDO);
			MessageDO messageDO = new MessageDO(null, "Contractic", message, MessageDO.ERROR);
			throw new ContracticServiceException(message, messageDO, e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Status '%s' created successfully", statusDO.getName()), MessageDO.OK);
	}
}
