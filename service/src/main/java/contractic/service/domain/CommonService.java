package contractic.service.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Audit;
import contractic.model.data.store.AuditAction;
import contractic.model.data.store.Contract;
import contractic.model.data.store.ContractDetail;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Supplier;
import contractic.model.data.store.Task;
import contractic.model.data.store.User;
import contractic.model.data.store.UserRolePermission;
import contractic.model.data.store.UserRolePermissionAction;
import contractic.service.util.ConcurrencyUtil;

public class CommonService {
	
	public static final String USER = "session.user";
	
	public static final Logger logger = Logger.getLogger(CommonService.class);

	@Autowired
	protected DomainService<Person, Long, PersonDO, MessageDO> personService;
	
	@Autowired
	protected DomainService<Supplier, Long, SupplierDO, MessageDO> supplierService;
	
	@Autowired
	protected DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	
	@Autowired
	protected DomainService<Contract, Long, ContractDO, MessageDO> contractService;
	
	@Autowired
	protected DomainService<ContractDetail, Long, ContractDetailDO, MessageDO> contractDetailService;
	
	@Autowired
	protected DomainService<Task, Long, TaskDO, MessageDO> taskService;
	
	@Autowired
	protected DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	
	@Autowired
	protected DomainService<Audit, Long, AuditDO, MessageDO> auditActionService;
	
	@Autowired
	protected DomainService<User, Long, UserDO, MessageDO> userService;
	
	@Autowired
	protected CrudOperation<UserRolePermission> userRolePermissionDAO;
	
	@Autowired
	protected CrudOperation<Module> moduleDAO;


	private String version;
	private String build;
	private String environment;
	
	
	public CommonService() {
		super();
	}
	
	
	
	public String getVersion() {
		return version;
	}



	public void setVersion(String version) {
		this.version = version;
	}



	public String getBuild() {
		return build;
	}



	public void setBuild(String build) {
		this.build = build;
	}



	public String getEnvironment() {
		return environment;
	}



	public void setEnvironment(String environment) {
		this.environment = environment;
	}



	/**
	 * This method converts the supplied hash string to the equivalent database id in the supplied module
	 * @param externalIdString
	 * @param module
	 * @return
	 * @throws ContracticServiceException
	 */
	
	protected Long getExternalId(String externalIdString, Module module)
			throws ContracticServiceException {
		Long externalId = null;
		String moduleName = module.getName();
		if(moduleName.equals(Module.PERSON)){
			Person person = personService.get(externalIdString);
			externalId = person.getId();
		}else if(moduleName.equals(Module.SUPPLIER)){
			Supplier supplier = supplierService.get(externalIdString);
			externalId = supplier.getId();
		}else if(moduleName.equals(Module.CONTRACT)){
			Contract contract = contractService.get(externalIdString);
			externalId = contract.getId();
		}else if(moduleName.equals(Module.CONTRACTDETAIL)){
			ContractDetail contractDetail = contractDetailService.get(externalIdString);
			externalId = contractDetail.getId();
		}else if(moduleName.equals(Module.ORGANISATION)){
			Organisation organisation = organisationService.get(externalIdString);
			externalId = organisation.getId();
		}else if(moduleName.equals(Module.TASK)){
			Task task = taskService.get(externalIdString);
			externalId = task.getId();
		}
		return externalId;
	}
	
	
	protected static void logAction(final DomainService<Audit, Long, AuditDO, MessageDO> auditService, 
			String action, 
			UserDO userDO, 
			String externalId, 
			ModuleDO moduleDO, String description, String field, String from, String to) throws ContracticServiceException{
		
		ConcurrencyUtil.IOBoundExecutorService.submit(new Runnable(){

			@Override
			public void run() {
				AuditDO auditDO = new AuditDO();
				auditDO.setAction(new ValueDO(action));
				auditDO.setDescription(description);
				auditDO.setField(field);
				auditDO.setFrom(from);
				auditDO.setTo(to);
				auditDO.setUser(new ValueDO(userDO.getId()));
				auditDO.setExternalId(externalId);
				auditDO.setModule(new ValueDO(moduleDO.getId(), moduleDO.getName()));
				auditDO.setOrganisationId(userDO.getOrganisationId());
				
				try {
					auditService.create(auditDO);
				} catch (ContracticServiceException e) {
					logger.warn(String.format("Error creating audit log"), e);
				}
			}
			
		});
	}

	protected static ModuleDO getModule(final DomainService<Module, Long, ModuleDO, MessageDO> moduleService, final String name) throws ContracticServiceException{
		final Map<String, Object> params = new HashMap<>();
		params.put(ModuleService.GET_PARAM_NAME, name);
		List<ModuleDO> moduleList = moduleService.get(params);
		assert moduleList.size() == 1;
		return moduleList.get(0);
	}
	
	protected static void logDeleteAction(final DomainService<Audit, Long, AuditDO, MessageDO> auditService, 
			UserDO userDO, 
			String externalId, 
			ModuleDO moduleDO, String description, String field, String from, String to) throws ContracticServiceException{
		final String action = AuditAction.DELETE;
		logAction(auditService, action, userDO, externalId, moduleDO, description, field, from, to);
	}
	
	protected static void logCreateAction(final DomainService<Audit, Long, AuditDO, MessageDO> auditService, 
			UserDO userDO, 
			String externalId, 
			ModuleDO moduleDO, String description, String field, String from, String to) throws ContracticServiceException{
		final String action = AuditAction.CREATE;
		logAction(auditService, action, userDO, externalId, moduleDO, description, field, from, to);
	}
	protected static void logUpdateAction(final DomainService<Audit, Long, AuditDO, MessageDO> auditService, 
			UserDO userDO, 
			String externalId, 
			ModuleDO moduleDO, String description, String field, String from, String to) throws ContracticServiceException{
		final String action = AuditAction.UPDATE;
		logAction(auditService, action, userDO, externalId, moduleDO, description, field, from, to);
	}
	
	protected static void logReadAction(final DomainService<Audit, Long, AuditDO, MessageDO> auditService, 
			UserDO userDO, 
			String externalId, 
			ModuleDO moduleDO, String description, String field, String from, String to) throws ContracticServiceException{
		final String action = AuditAction.READ;
		logAction(auditService, action, userDO, externalId, moduleDO, description, field, from, to);
	}
	
	protected void checkPermission(UserDO userDO, UserRolePermissionAction action, String moduleName)
			throws ContracticServiceException {
		ServiceAccessValidator accessValidator = new ServiceAccessValidator(moduleDAO, userRolePermissionDAO, userService);
		accessValidator.checkPermission(userDO, action, moduleName);
	}
}