package contractic.service.domain;

public class ContracticServiceException extends Exception {
	private MessageDO messageDO;
	public ContracticServiceException(String arg0) {
		super(arg0);
	}

	public ContracticServiceException(String arg0, Throwable arg1) {
		this(arg0, null, arg1);
	}

	public ContracticServiceException(String arg0, MessageDO messageDO) {
		super(arg0, null);
		this.messageDO = messageDO;
	}
	
	public ContracticServiceException(String arg0, MessageDO messageDO, Throwable arg1) {
		super(arg0, arg1);
		this.messageDO = messageDO;
	}

	public MessageDO getMessageDO() {
		return messageDO;
	}
	
}
