package contractic.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"organisationId", "task"})
public class TaskContactDO {
	private String id;
	private ValueDO type;
	private ValueDO contact;
	private ValueDO task;
	private String organisationId;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ValueDO getContact() {
		return contact;
	}

	public void setContact(ValueDO contact) {
		this.contact = contact;
	}

	public ValueDO getTask() {
		return task;
	}

	public void setTask(ValueDO task) {
		this.task = task;
	}

	public ValueDO getType() {
		return type;
	}

	public void setType(ValueDO type) {
		this.type = type;
	}

	public String getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	
}
