package contractic.service.domain;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import contractic.model.data.store.Task;

public class TaskAudit implements AuditJob<TaskDO> {

	private final List<AuditDO> auditList = new LinkedList<>();

	@Override
	public List<AuditDO> run(TaskDO source, TaskDO target) {
		
		// Log change
		if(!Objects.equals(source.getTitle(), target.getTitle()))
			auditList.add(AuditJob.createAudit(Task.TITLE, source.getTitle(), target.getTitle()));

		if(!Objects.equals(source.getDescription(), target.getDescription()))
			auditList.add(AuditJob.createAudit(Task.DESCRIPTION, source.getDescription(), target.getDescription()));

		AuditDO startDateAudit = AuditJob.auditDateFieldChange(source.getStartDate(), target.getStartDate(), Task.STARTDATE);
		if(startDateAudit != null)
			auditList.add(startDateAudit);

		AuditDO endDateAudit = AuditJob.auditDateFieldChange(source.getEndDate(), target.getEndDate(), Task.ENDDATE);
		if(endDateAudit != null)
			auditList.add(endDateAudit);
		
		if(!Objects.equals(source.getDescription(), target.getDescription()))
			auditList.add(AuditJob.createAudit(Task.DESCRIPTION, source.getDescription(), target.getDescription()));
		
		AuditDO statusAudit = AuditJob.auditForeignFieldChange(source.getStatus(), target.getStatus(), Task.STATUS);
		if(statusAudit!=null)
			auditList.add(statusAudit);
		
		AuditDO personAudit = AuditJob.auditForeignFieldChange(source.getPerson(), target.getPerson(), Task.PERSON);
		if(personAudit!=null)
			auditList.add(personAudit);
		
		AuditDO parentAudit = AuditJob.auditForeignFieldChange(source.getParent(), target.getParent(), Task.PARENT);
		if(parentAudit!=null)
			auditList.add(parentAudit);
		
		// Log Start Date and End Date change
		return auditList;
	}
	


	
}
