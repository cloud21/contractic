package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.dao.SupplierDAO;
import contractic.model.data.store.Contact;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Supplier;
import contractic.model.data.store.UserRolePermissionAction;
import contractic.service.util.Cache;
import contractic.service.util.ConcurrencyUtil;
import contractic.service.util.HashingUtil;
import contractic.service.util.SearchClient.SearchEntity;

public class SupplierService implements DomainService<Supplier, Long, SupplierDO, MessageDO> {

	public final static String GET_PARAM_NAME = "name";		
	public final static String SUPPLIER_IDCACHE_NAME = "supplier.idcache.name";
	public static final String GET_PARAM_SEARCH_QUERY = "supplier.search.query";
	public static final String GET_PARAM_ACTIVE = "supplier.active";
	
	public static final String USERID = "user.id";

	private Cache<String, Long> supplierIdCache;
	
	private static final Logger logger = Logger
			.getLogger(Supplier.class);
	
	@Autowired
	private CommonService commonService;
	@Autowired
	private CrudOperation<Supplier> supplierDAO;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	@Autowired
	private DomainService<Contact, Long, ContactDO, MessageDO> contactService;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<SearchEntity, Long, SearchEntityDO, MessageDO> searchService;
	
	public void setSupplierDAO(SupplierDAO supplierDAO) {
		this.supplierDAO = supplierDAO;
	}

	public Supplier get(final String uuid) throws ContracticServiceException{
		final Long id = this.supplierIdCache.get(uuid);
		return get(id);
	}
	
	public Supplier get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null supplier id supplied");
		
		Supplier supplier = new Supplier();
		supplier.setId(id);
		
		try {
			List<Supplier> types = supplierDAO.read(supplier);
			return types.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving supplier with id %d", id));
		}
	}
	
	
	private SupplierDO makeDO(final Supplier supplier, String organisationId) throws NoSuchAlgorithmException, ContracticServiceException{
		String hashId = HashingUtil.get(supplier.getId());
		SupplierDO supplierDO = new SupplierDO();
		supplierDO.setId(hashId);
		supplierDO.setName(supplier.getName());
		supplierDO.setReference(supplier.getReference());
		supplierDO.setExternalRef(supplier.getExternalRef());
		supplierDO.setActive(Boolean.toString(supplier.isActive()));
		supplierDO.setCreated(supplier.getCreated().toString());
		
/*		final Map<String, Object> params = new HashMap<>();
		params.put(ModuleService.GET_PARAM_NAME, "Supplier"); // Parametize me
		final List<ModuleDO> moduleList = moduleService.get(params);
		assert(moduleList.size() == 1);
		final Module module = moduleService.get(moduleList.get(0).getId());
		
		final Map<String, Object> contactParams = new HashMap<>();
		contactParams.put(ContactService.GET_PARAM_EXTERNALID, supplier.getId());
		contactParams.put(ContactService.GET_PARAM_MODULE, module);
		contactParams.put(ContactService.GET_PARAM_ORGANISATIONID, organisationId);
		List<ContactDO> contactDOList = contactService.get(contactParams);
		supplierDO.setContact(contactDOList.toArray(new ContactDO[0]));*/
		
		return supplierDO;
	}
	
	public List<SupplierDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASHID);
		 final String queryString = (String)params.get(GET_PARAM_SEARCH_QUERY);
		 final Long supplierId = (Long)params.get(GET_PARAM_ID);

			final UserDO userDO = (UserDO)params.get(CommonService.USER);
			if(userDO == null)
				throw new ContracticServiceException("Invalid session user provided");
			
			final String organisationId = userDO.getOrganisationId();
		 
			List<Supplier> suppliers = null;
			try {
				Supplier supplier = new Supplier();
				
				if(!StringUtils.hasText(hash) && supplierId == null && !StringUtils.hasText(organisationId))
					throw new ContracticServiceException("Invalid parameters, must supply organisation or id");
				supplier.setId(supplierId);
				if(StringUtils.hasText(organisationId)){
					Organisation organisation = organisationService.get(organisationId);
					supplier.setOrganisation(organisation);
				}				
				
				
				if(StringUtils.hasText(hash)){
					Long id = this.supplierIdCache.get(hash);
					supplier.setId(id);;
				}
				
				if(queryString!=null){
					supplier.setName(queryString);
				}
				
				suppliers = supplierDAO.read(supplier);
			} catch (Exception e) {
				throw new ContracticServiceException("An error occured while attempting to read address types", e);
			}

			// List of all exceptions that occur in the predicate
			List<ContracticServiceException> cse = new ArrayList<>();
		
			final List<SupplierDO> pojoList = suppliers.stream()
				.map(supplier -> {
					SupplierDO supplierDO = null;
					try{
						supplierDO = makeDO(supplier, organisationId);						
						// Side effect 1
						supplierIdCache.put(supplierDO.getId(), supplier.getId());

					}catch(ContracticServiceException | NoSuchAlgorithmException ex){
						cse.add(new ContracticServiceException(String.format("An error occured when hashing the supplier %s", supplier), ex));
					}
					
					return supplierDO;
				}).collect(Collectors.toList());
			
			if(cse.size()!=0)
				throw cse.get(0);
			
			return pojoList;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.supplierIdCache = new Cache(SUPPLIER_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}
	
	private void saveSearchEntity(Supplier supplier) {
		ConcurrencyUtil.IOBoundExecutorService.submit(() -> {
			SearchEntityDO searchEntity = new SearchEntityDO();
			searchEntity.setId(supplier.getId());
			searchEntity.setCollection(Module.SUPPLIER);
			searchEntity.setOrganisationId(supplier.getOrganisation().getId());
			searchEntity.setText(String.join(
					",",
					new String[] { supplier.getName(), supplier.getExternalRef(), supplier.getReference() }));

			try {
				searchService.create(searchEntity);
			} catch (Exception e) {
				logger.warn(String.format("An error occurred when indexing person '%s-%s-%s'",  supplier.getName(), supplier.getOrganisation().getName()), e);
			}
		});
	}

	public MessageDO create(SupplierDO supplierDO) throws ContracticServiceException {
		UserDO userDO = supplierDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		// Construct the object
			final Supplier supplier = new Supplier();
			supplier.setName(WordUtils.capitalize(supplierDO.getName()));
			supplier.setCreated(new Date());
			supplier.setActive(Boolean.parseBoolean(supplierDO.getActive()));
			supplier.setReference(supplierDO.getReference());
			supplier.setExternalRef(supplierDO.getExternalRef());
			
			Organisation organisation = organisationService.get(supplierDO.getOrganisationId());
			supplier.setOrganisation(organisation);
			
			// Save the person object
			try {
				supplierDAO.create(supplier);
				
				String hashId = HashingUtil.get(supplier.getId());
				// Update the cache
				this.supplierIdCache.put(hashId, supplier.getId());
				
				saveSearchEntity(supplier);
				
				final ModuleDO moduleDO = CommonService.getModule(moduleService, Module.SUPPLIER);
				
				// Now make and save the Contact objects
				final List<ContracticServiceException> cse = new ArrayList<>();
				if (supplierDO.getContact()!=null){
		    		Arrays.stream(supplierDO.getContact()).forEach(contact -> {
		
		    			contact.setModuleId(moduleDO.getId());
		    			try {
		    				contact.setExternalId(hashId);
		    				contact.setOrganisationId(supplierDO.getOrganisationId());
							contactService.create(contact);
						} catch (ContracticServiceException e) {
							cse.add(e);
						}
		    		});
				}
				
				if(cse.size()>0)
					throw cse.get(0);
			
			} catch (Exception e) {
				throw new ContracticServiceException(String.format("Failed to create supplier: %s", supplierDO), e);
			}
			
			//final String icon, final String title, final String body, final String type
			return new MessageDO(null, "Contractic", String.format("Supplier '%s' created successfully", supplierDO.getName()), MessageDO.OK);
	}

	public MessageDO update(SupplierDO supplierDO) throws ContracticServiceException {
		
		UserDO userDO = supplierDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		if(!StringUtils.hasText(supplierDO.getId())){
			return new MessageDO(null, "Contractic", "Security data has been corrupted, can't continue", MessageDO.ERROR);
		}
		
		if(!StringUtils.hasText(supplierDO.getOrganisationId())){
			return new MessageDO(null, "Contractic", "Organisation name can't be empty", MessageDO.ERROR);
		}
		
		final Long supplierId = this.supplierIdCache.get(supplierDO.getId());
		if(supplierId == null){
			return new MessageDO(null, "Contractic", "Security data has been corrupted, can't continue", MessageDO.ERROR);
		}
		
		final Organisation organisation = organisationService.get(supplierDO.getOrganisationId());
		Supplier supplier = new Supplier();
		try{
			supplier.setId(supplierId);
			supplier.setOrganisation(organisation);	
			
			List<Supplier> supplierList = this.supplierDAO.read(supplier);
			if(supplierList.size() != 1){
				return new MessageDO(null, "Contractic", "No such person exists. Data corruption detected", MessageDO.ERROR);
			}
			
			supplier = supplierList.get(0);
			supplier.setName(WordUtils.capitalize(supplierDO.getName()));
			supplier.setExternalRef(supplierDO.getExternalRef());
			supplier.setActive(Boolean.parseBoolean(supplierDO.getActive()));
			
			// Contacts are updated through the contract service. The client would need to know if an update or a create is required

			supplierDAO.update(supplier);
			
			saveSearchEntity(supplier);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Failed to update supplier: '%s'", supplierDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Supplier '%s' updated successfully", supplierDO.getName()), MessageDO.OK);
	}

	public MessageDO delete(SupplierDO supplierDO) throws ContracticServiceException {
		
		UserDO userDO = supplierDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		final String hash = supplierDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Could not find supplied hash");
			
		final Long id = supplierIdCache.get(hash);	
		
		final String organisationId = supplierDO.getOrganisationId();
		
		// Save the person object
		Supplier supplier = null;
		try {

			supplier = new Supplier();
			supplier.setId(id);
			Organisation organisation = organisationService.get(organisationId);
			supplier.setOrganisation(organisation);
			List<Supplier> statusList = supplierDAO.read(supplier);
			assert(statusList.size()==1);
			supplier = statusList.get(0);
			
			supplierDAO.delete(supplier);
		
		} catch (Exception e) {
			String message = String.format("Failed to delete supplier: %s", supplier.getName());
			MessageDO messageDO = new MessageDO(null, "Contractic", message, MessageDO.ERROR);
			throw new ContracticServiceException(message, messageDO, e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Supplier '%s' deleted successfully", supplier.getName()), MessageDO.OK);
	}
}
