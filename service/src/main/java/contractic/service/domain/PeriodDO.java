package contractic.service.domain;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.StaxDriver;

@XStreamAlias("period")
public class PeriodDO {
	public static class Cycle{
		public final static String DAY = "D";
		public final static String MONTH = "M";
		public final static String YEAR = "Y";
				
		public String frequency;
		public Integer value;
		public Cycle(){}
		public Cycle(String frequency, Integer value){
			this.frequency = frequency;
			this.value = value;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((frequency == null) ? 0 : frequency.hashCode());
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Cycle other = (Cycle) obj;
			if (frequency == null) {
				if (other.frequency != null)
					return false;
			} else if (!frequency.equals(other.frequency))
				return false;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}
		@Override
		public String toString() {
			return "[frequency=" + frequency + ", value=" + value + "]";
		}
	}
	private Cycle cycle;
	private String from;
	private String to;
	
	public PeriodDO(final String from, final String to){
		this.from = from;
		this.to = to;
	}
	
	public PeriodDO(){
		
	}
	
	public Cycle getCycle() {
		return cycle;
	}
	public void setCycle(Cycle cycle) {
		this.cycle = cycle;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cycle == null) ? 0 : cycle.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeriodDO other = (PeriodDO) obj;
		if (cycle == null) {
			if (other.cycle != null)
				return false;
		} else if (!cycle.equals(other.cycle))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}
	/**
	 * Convert to ContractValue from XML
	 * @param xml
	 * @return
	 */
	public final static PeriodDO fromXml(String xml){
		XStream xstream = new XStream();
		xstream.processAnnotations(PeriodDO.class);
		return (PeriodDO)xstream.fromXML(xml);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public final static String toXml(PeriodDO value){
		XStream xstream = new XStream(new StaxDriver());
		xstream.processAnnotations(PeriodDO.class);
		return xstream.toXML(value);
	}
	
	@Override
	public String toString() {
		return "ContractValuePeriodDO [cycle=" + cycle + ", from=" + from
				+ ", to=" + to + "]";
	}
	
}
