package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Contract;
import contractic.model.data.store.Group;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Report;
import contractic.model.data.store.ReportOrganisation;
import contractic.model.data.util.HibernateUtil;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class ReportService implements DomainService<ReportOrganisation, Long, ReportDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	public final static String GET_PARAM_ID = "id";
	public static final String USERID = "user.id";
	public final static String GET_EXTERNAL_REF = "external.ref";

	public final static String REPORT_IDCACHE_NAME = "report.idcache.name";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public static final String GET_PARAM_ACTIVE = "active";
	public static final String GET_PARAM_EXECUTE = "execute.report";
	public static final String GET_REPORT_PARAMS = "report.params";

	private Cache<String, Long> reportIdCache;

	@Autowired
	private CrudOperation<ReportOrganisation> reportOrganisationDAO;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	@Autowired
	private DomainService<Group, Long, GroupDO, MessageDO> groupService;
	@Autowired
	private DomainService<Person, Long, PersonDO, MessageDO> personService;
	@Autowired
	private DomainService<Contract, Long, ContractDO, MessageDO> contractService;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private CommonService commonService;
	@Autowired
	private CrudOperation<Module> moduleDAO;

	/**
	 * Updates the contact supplied. Note we currently only update the details of the underlying address.
	 * @param pojo
	 * @return
	 * @throws ContracticServiceException
	 */
	public MessageDO update(final ReportDO pojo) throws ContracticServiceException {
		
		// Get the object
		ReportOrganisation report = get(pojo.getId());

		report.setName(pojo.getName());
		report.setSchedule(pojo.getSchedule());
		report.setDescription(pojo.getDescription());
		report.setActive(pojo.getActive());
		
		// By design, only the address service has access to the addressDAO object so we update the address through
		// the service
		try {
			reportOrganisationDAO.update(report);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occurred while updating the report", e);
		}
		
		return new MessageDO(null, "Contractic", String.format("ReportOrganisation '%s' updated successfully", pojo.getName()), MessageDO.OK);
	}

	public MessageDO create(final ReportDO contactDO) throws ContracticServiceException {
		return null;
	}

	public ReportOrganisation get(final String hash) throws ContracticServiceException {
		final Long id = this.reportIdCache.get(hash);
		return get(id);
	}

	public ReportOrganisation get(final Long id) throws ContracticServiceException {
		if (id == null)
			throw new ContracticServiceException("Null report id supplied");

		ReportOrganisation report = new ReportOrganisation();
		report.setId(id);

		try {
			List<ReportOrganisation> types = reportOrganisationDAO.read(report);
			return types.get(0);

		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format(
					"An error occured while retrieving report with id %d", id));
		}
	}

	private ReportDO makeDO(ReportOrganisation report, UserDO userDO) throws NoSuchAlgorithmException,
			ContracticServiceException {
		ReportDO reportDO = new ReportDO();

		String hashId = HashingUtil.get(report.getId());
		String organisationId = userDO.getOrganisationId();

		// Set the contact hash key
		reportDO.setId(hashId);
		reportDO.setName(report.getName() == null ? report.getReport().getName() : report.getName());
		reportDO.setDescription(report.getDescription() == null ? report.getReport().getDescription() : report.getDescription());
		reportDO.setActive(report.getActive());
		reportDO.setSchedule(report.getSchedule());
		reportDO.setExternalRef(report.getReport().getExternalRef());
		final Date lastrun = report.getLastRun();
		if(lastrun!=null)
			reportDO.setLastrun(lastrun.getTime());
		reportDO.setCreated(report.getCreated().getTime());

		// Get the owner
		Person person = report.getOwner();
		if(person!=null){
			final Map<String, Object> paramMap = new HashMap<>();
			paramMap.put(CommonService.USER, userDO);
			paramMap.put(DomainService.GET_PARAM_ID, report.getOwner().getId());
			List<PersonDO> personList = personService.get(paramMap);
			assert personList.size() == 1;
			PersonDO personDO = personList.get(0);
			reportDO.setOwner(new ValueDO(personDO.getId(), personDO.getFirstName()));
		}
		
		Group group = report.getGroup();
		if(group != null){
			final Map<String, Object> paramMap = new HashMap<>();
			paramMap.put(CommonService.USER, userDO);
			paramMap.put(GroupService.GET_PARAM_ID, group.getId());
			List<GroupDO> groupList = groupService.get(paramMap);
			assert groupList.size() == 1;
			GroupDO groupDO = groupList.get(0);
			reportDO.setGroup(new ValueDO(groupDO.getId(), groupDO.getName()));
		}
		return reportDO;
	}

	private Object executeReport(final Report report, final Map<String, List<String>> paramMap, Organisation organisation) throws ContracticServiceException, ContracticModelException{
		Session session = HibernateUtil.currentSession();
		
		/*If contract id has been supplied, it would be the contract hash. So we replace that contract id with the actual database Id*/
		populateDbIds(paramMap);
		
		XStream xStream = new XStream(new StaxDriver());
		xStream.alias("map", java.util.Map.class);
		String xml = xStream.toXML(paramMap);
		
		Query query = session.createSQLQuery(report.getCommand())
		.setParameter("Params", xml)
		.setParameter("OrganisationId", organisation.getId());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		return query.list();
	}

	private void populateDbIds(final Map<String, List<String>> paramMap)
			throws ContracticServiceException, ContracticModelException {
		{
			Module mod = null;
			String idName = "contractId";
			List<String> hashList = paramMap.get(idName);
			if(hashList!=null){
				mod = ModuleService.getModule(moduleDAO, Module.CONTRACT);
			}else{
				idName = "personId";
				hashList = paramMap.get(idName);
				mod = ModuleService.getModule(moduleDAO, Module.PERSON);
			}
			
			if(hashList == null)
				return;
			
			final Module module = mod;

			List<ContracticServiceException> cse = new ArrayList<>();
			List<String> idList = hashList.stream().map(hash -> {
				try{
					Long id = commonService.getExternalId(hash, module);
					return id.toString();
				}catch(ContracticServiceException e){
					cse.add(e);
					return null;
				}
			}).collect(Collectors.toList());
			if(cse.size() > 0)
				throw cse.get(0);
			paramMap.put(idName, idList);
		}
	}
	
	public List<ReportDO> get(final Map<String, Object> params)
			throws ContracticServiceException {

		final String hash = (String)params.get(GET_PARAM_HASH);
		final Long id = (Long) params.get(GET_PARAM_ID);
		final String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);
		final Boolean execute = (Boolean)params.get(GET_PARAM_EXECUTE);
		final Map<String, List<String>> reportParams = (Map<String, List<String>>)params.get(GET_REPORT_PARAMS);
		final UserDO userDO = (UserDO)params.get(CommonService.USER);
		final String externalRef = (String)params.get(GET_EXTERNAL_REF);

		if(!StringUtils.hasText(organisationId)){
			throw new ContracticServiceException("OrganisationId not supplied");
		}
		
		if(userDO == null)
			throw new ContracticServiceException("UserDO not supplied");
		
		final Organisation organisation = organisationService.get(organisationId);
		
		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
		
		List<ReportOrganisation> reports = null;
		try {
			ReportOrganisation organisationReport = new ReportOrganisation();

			organisationReport.setOrganisation(organisation);
			if(StringUtils.hasText(externalRef)){
				Report report = new Report();
				report.setExternalRef(externalRef);
				organisationReport.setReport(report);
			}

			organisationReport.setId(id);
			if(StringUtils.hasText(hash)){
				organisationReport.setId(this.reportIdCache.get(hash));
			}
			
			reports = reportOrganisationDAO.read(organisationReport);
			
			/*For formality, we can only execute if the resulting report query yielded one result*/
			if(reports!=null && reports.size() == 1 && execute!=null && execute){
				assert reports.size() == 1 : "Execution of report requested but did not retrieve exactly one result";
				Object data = executeReport(reports.get(0).getReport(), reportParams, reports.get(0).getOrganisation());
				ReportDO reportDO = new ReportDO();
				reportDO.setId(hash);
				reportDO.setData(data);
				return Arrays.asList(new ReportDO[]{reportDO});
			}
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(
					"An error occured while attempting to read contacts", e);
		}

		if(cse.size() > 0)
			throw cse.get(0);
		
		final List<ReportDO> pojoList = reports
				.stream()
				.map(report -> {

					ReportDO reportDO = null;
					try {
						reportDO = makeDO(report, userDO);
						// Side effect 1
						reportIdCache.put(reportDO.getId(), report.getId());
					} catch (NoSuchAlgorithmException | ContracticServiceException ex) {
						cse.add(new ContracticServiceException(String.format(
								"An error occured when hashing the report %s",
								report)));
					}

					return reportDO;
				}).collect(Collectors.toList());

		if (cse.size() != 0)
			throw cse.get(0);

		return pojoList;
	}

	@Override
	public void init() throws ContracticServiceException {
		try {
			this.reportIdCache = new Cache(REPORT_IDCACHE_NAME,
					String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(
					String.format("Initializing %s service failed", this
							.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	@Override
	public MessageDO delete(ReportDO dojo) throws ContracticServiceException {
		// TODO Auto-generated method stub
		return null;
	}

}
