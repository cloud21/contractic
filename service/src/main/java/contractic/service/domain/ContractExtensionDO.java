package contractic.service.domain;

public class ContractExtensionDO {
	private Integer number;
	private PeriodDO period;
	private PeriodDO notice;
	private Boolean active;
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public PeriodDO getPeriod() {
		return period;
	}
	public void setPeriod(PeriodDO period) {
		this.period = period;
	}
	public PeriodDO getNotice() {
		return notice;
	}
	public void setNotice(PeriodDO notice) {
		this.notice = notice;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
}
