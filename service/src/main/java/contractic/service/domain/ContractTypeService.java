package contractic.service.domain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.ContractType;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.UserRolePermissionAction;
import contractic.service.util.Cache;
import contractic.service.util.HashingUtil;

public class ContractTypeService implements DomainService<ContractType, Long, ContractTypeDO, MessageDO> {

	public final static String GET_PARAM_HASH = "hash";
	public final static String GET_PARAM_NAME = "name";
	
	private final static String CONTRACTTYPE_IDCACHE_NAME = "contracttype.idcache.name";
	public static final String GET_PARAM_ID = "contracttype.id";
	public static final String GET_PARAM_ORGANISATIONID = "organisation.id";
	public static final String USERID = "user.id";
	
	private final static Logger logger = Logger.getLogger(ContractTypeService.class);
	
	private Cache<String, Long> contractTypeIdCache;
	
	@Autowired
	private CommonService commonService;
	@Autowired
	private CrudOperation<ContractType> contractTypeDAO;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	

	public ContractType get(final String uuid) throws ContracticServiceException{
		final Long id = this.contractTypeIdCache.get(uuid);
		return get(id);
	}
	
	public ContractType get(final Long id) throws ContracticServiceException{
		if(id==null)
			throw new ContracticServiceException("Null contract type id supplied");
		
		ContractType contractType = new ContractType();
		contractType.setId(id);
		
		try {
			List<ContractType> types = contractTypeDAO.read(contractType);
			return types.get(0);
			
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("An error occured while retrieving contract type with id %d", id));
		}
	}
	
	
	private ContractTypeDO makeDO(final ContractType contractType) throws NoSuchAlgorithmException, ContracticServiceException{
		String hashId = HashingUtil.get(contractType.getId());
		ContractTypeDO contractTypeDO = new ContractTypeDO();
		contractTypeDO.setId(hashId);
		contractTypeDO.setName(contractType.getName());
		contractTypeDO.setDescription(contractType.getDescription());
		contractTypeDO.setActive(contractType.getActive());
		
		Map<String, Object> params = new HashMap<>();
		params.put(OrganisationService.GET_PARAM_ID, contractType.getOrganisation().getId());
		List<OrganisationDO> orgList = organisationService.get(params);
		assert(orgList.size() == 1);
		contractTypeDO.setOrganisationId(orgList.get(0).getId());
		
		return contractTypeDO;
	}
	
	public List<ContractTypeDO> get(final Map<String, Object> params) throws ContracticServiceException{
		 final String hash = (String)params.get(GET_PARAM_HASH);
		 final String name = (String)params.get(GET_PARAM_NAME);
		 Long id = (Long)params.get(GET_PARAM_ID);
		 String organisationId = (String)params.get(GET_PARAM_ORGANISATIONID);

		if(!StringUtils.hasText(organisationId)){
			throw new ContracticServiceException("OrganisationId not supplied");
		}
	 
		List<ContractType> types = null;
		try {
			ContractType type = new ContractType();
			
			Organisation organisation = organisationService.get(organisationId);
			type.setOrganisation(organisation);
			
			if(StringUtils.hasText(name)){
				type.setName(name);
			}
						
			if(StringUtils.hasText(hash)){
				id = this.contractTypeIdCache.get(hash);
			}
					
			if(id !=null){
				type.setId(id);
			}
							
			types = contractTypeDAO.read(type);
		} catch (ContracticModelException e) {
			throw new ContracticServiceException("An error occured while attempting to read address types", e);
		}

		// List of all exceptions that occur in the predicate
		List<ContracticServiceException> cse = new ArrayList<>();
	
		final List<ContractTypeDO> pojoList = types.stream()
			.map(type -> {
				ContractTypeDO typeDO = null;
				try{
					typeDO = makeDO(type);						
					// Side effect 1
					contractTypeIdCache.put(typeDO.getId(), type.getId());

				}catch(final ContracticServiceException | NoSuchAlgorithmException ex){
					cse.add(new ContracticServiceException(String.format("An error occured when hashing the contract type %s", type)));
				}
				
				return typeDO;
			}).collect(Collectors.toList());
		
		if(cse.size()!=0)
			throw cse.get(0);
		
		return pojoList;
	}
	
	@Override
	public void init() throws ContracticServiceException {
		try {
			this.contractTypeIdCache = new Cache(CONTRACTTYPE_IDCACHE_NAME, String.class, Long.class);
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Initializing %s service failed", this.getClass().getName()), e);
		}
	}

	@Override
	public void destroy() throws ContracticServiceException {
		// TODO Auto-generated method stub

	}

	public MessageDO delete(ContractTypeDO contractTypeDO) throws ContracticServiceException {
		
		UserDO userDO = contractTypeDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.DELETE, Module.ORGANISATION);
		
		final String hash = contractTypeDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("");
			
		final Long id = contractTypeIdCache.get(hash);	
		
		final String organisationId = contractTypeDO.getOrganisationId();
		Organisation organisation = organisationService.get(organisationId);
		
		// Save the person object
		try {

			ContractType contractType = new ContractType();
			contractType.setId(id);
			
			contractType.setOrganisation(organisation);
			List<ContractType> typeList = contractTypeDAO.read(contractType);
			assert(typeList.size()==1);
			contractType = typeList.get(0);
			
			contractTypeDAO.delete(contractType);
		
		} catch (Exception e) {
			throw new ContracticServiceException(String.format("Failed to delete type: %s", contractTypeDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Document type '%s' updated successfully", contractTypeDO.getName()), MessageDO.OK);
	}

	public MessageDO update(ContractTypeDO contractTypeDO) throws ContracticServiceException {
		
		UserDO userDO = contractTypeDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.WRITE, Module.ORGANISATION);
		
		final String hash = contractTypeDO.getId();
		if(!StringUtils.hasText(hash))
			throw new ContracticServiceException("Invalid document hash id provided");
			
		final Long id = contractTypeIdCache.get(hash);
		
		final String organisationId = contractTypeDO.getOrganisationId();
		Organisation organisation = organisationService.get(organisationId);
		
		// Save the person object
		try {

			ContractType contractType = new ContractType();
			
			// Find the document type and load it into cache
			contractType.setId(id);
			contractType.setOrganisation(organisation);
			List<ContractType> typeList = contractTypeDAO.read(contractType);
			assert(typeList.size()==1);
			contractType = typeList.get(0);
			
			// Update the fields
			contractType.setName(contractTypeDO.getName());
			contractType.setDescription(contractTypeDO.getDescription());
			contractType.setActive(contractTypeDO.getActive());
			
			// Update the data store
			contractTypeDAO.update(contractType);
		
		} catch (ContracticModelException e) {
			throw new ContracticServiceException(String.format("Failed to update contract type: %s", contractTypeDO), e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Contract type '%s' updated successfully", contractTypeDO.getName()), MessageDO.OK);
	}

	public MessageDO create(ContractTypeDO contractTypeDO) throws ContracticServiceException {
		
		UserDO userDO = contractTypeDO.getUser();
		commonService.checkPermission(userDO, UserRolePermissionAction.DELETE, Module.ORGANISATION);
		
		final ContractType contractType = new ContractType();
		contractType.setName(contractTypeDO.getName());
		contractType.setDescription(contractTypeDO.getDescription());
		contractType.setActive(contractTypeDO.getActive());
		
		Organisation organisation = organisationService.get(contractTypeDO.getOrganisationId());
		contractType.setOrganisation(organisation);
		
		// Save the person object
		try {
			contractTypeDAO.create(contractType);
			
			String hashId = HashingUtil.get(contractType.getId());
			// Update the cache
			this.contractTypeIdCache.put(hashId, contractType.getId());
		
		} catch (NoSuchAlgorithmException | ContracticModelException e) {
			String message = String.format("Failed to create contract type: %s", contractTypeDO);
			logger.error(message, e);
			throw new ContracticServiceException(message, e);
		}
		
		//final String icon, final String title, final String body, final String type
		return new MessageDO(null, "Contractic", String.format("Contract type '%s' created successfully", contractTypeDO.getName()), MessageDO.OK);
	}
	
}
