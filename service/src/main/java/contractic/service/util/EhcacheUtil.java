package contractic.service.util;

import java.net.URL;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

public final class EhcacheUtil {
	private static CacheManager cacheManager = null;
	
	public static CacheManager getCacheManager() throws Exception{
		if(cacheManager==null)
			throw new Exception("Ehcache has not been initialized yet");
		return cacheManager;
	}
	
//	public static void initialize() throws Exception{
//		final URL myUrl = EhcacheUtil.class.getResource("/ehcache.xml"); 
//		initialize(myUrl);
//	}
	
	public static void initialize(final URL myUrl) throws Exception{
		cacheManager = CacheManager.getInstance();
		if(cacheManager == null)
			cacheManager = CacheManager.create();
	}
	
	public static void initialize(){
		cacheManager = CacheManager.create();
	}
	
	public static Cache getCache(String name){
		return cacheManager.getCache(name);
	}
	
	public static <K, V> Cache createCache(String name, Class<K> keyClass, Class<V> valueClass) throws Exception{
		CacheManager cacheManager = getCacheManager();
		cacheManager.addCache(name);
		return cacheManager.getCache(name);
	}
	
	public static <K, V> Ehcache createCache(String name) throws Exception{
		CacheManager cacheManager = getCacheManager();
		return cacheManager.addCacheIfAbsent(name);
	}
	
	public static void destroy(){
		if(cacheManager!=null)
			cacheManager.shutdown();
	}
	
	@SuppressWarnings("unchecked")
	public static <K, V> V get(Cache cache, K key){
		Element element = cache.get(key);
		return (V)element.getObjectValue();
	}
	
	public static <K, V> void put(Cache cache, K key, V value){
		cache.put(new Element(key, value));
	}
}