package contractic.service.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public final class DateUtil {

	private DateUtil() {
		// TODO Auto-generated constructor stub
	}
	
	// Parse date based on various formats
	public static Date tryParse(final String dateString, final String[] formats){
		if(dateString == null)
			return null;
		final List<DateFormat> knownPatterns = Arrays.stream(formats)
				.map(format -> new SimpleDateFormat(format))
				.collect(Collectors.toList());
		for(final DateFormat pattern : knownPatterns){
			try{
				return new Date(pattern.parse(dateString).getTime());
			}catch(ParseException pe){
				// Loop on
			}
		}
		return null;
	}

	public static Date tryParse(final String dateString){
		final String[] formats = {
				"yyyy-MM-dd HH:mm:ss.S",
				"yyyy-MM-dd HH:mm:ss",
				"yyyy-MM-dd",
				"dd/MM/yyyy HH:mm:ss.S",
				"dd/MM/yyyy HH:mm:ss",
				"dd/MM/yyyy",
		};
		try{
			long time = Long.parseLong(dateString);
			return new Date(time);
		}catch(NumberFormatException nfe){
			return tryParse(dateString, formats);
		}
	}

	public static String toString(Date date, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
}
