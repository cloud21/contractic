package contractic.service.util;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.Statistics;

public class BidiCache<K, V> {
	private final Ehcache cache;
	public BidiCache(final String name, Class<K> keyClass, Class<V> valueClass) throws Exception{
		this.cache = EhcacheUtil.createCache(name);
	}
	
	@SuppressWarnings("unchecked")
	public V get(K key){
		Element element = cache.get(key);
		return (V)element.getObjectValue();
	}
	
	public void put(K key, V value){
		cache.put(new Element(key, value));
	}
	
	public void remove(K key){
		cache.remove(key);
	}
	
	public void clear(){
		cache.removeAll();
	}
	
	public Statistics getStatistics(){
		return cache.getStatistics();
	}
}
