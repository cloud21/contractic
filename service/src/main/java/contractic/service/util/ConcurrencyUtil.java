package contractic.service.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConcurrencyUtil {
	public static ExecutorService CPUBoundExecutorService = 
			Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
	public static ExecutorService IOBoundExecutorService = 
			Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 4);
}
