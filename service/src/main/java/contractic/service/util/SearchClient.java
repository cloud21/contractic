package contractic.service.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class SearchClient {
	
	@JsonIgnoreProperties({"_version_"})
	public static class SearchEntity{
		private Long id;
		private String collection;
		private Long organisationId;
		private String text;
		private Long _version_;
		
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		@JsonProperty("collection_s")
		public String getCollection() {
			return collection;
		}
		public void setCollection(String collection) {
			this.collection = collection;
		}
		@JsonProperty("organisationId_i")
		public Long getOrganisationId() {
			return organisationId;
		}
		public void setOrganisationId(Long organisationId) {
			this.organisationId = organisationId;
		}
		@JsonProperty("text_s")
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public Long get_version_() {
			return _version_;
		}
		public void set_version_(Long _version_) {
			this._version_ = _version_;
		}

		
	}
	
	public static final String SEARCH_SERVER_URL = "search.server.url";
	private final HttpClient client;
	private final Properties properties;
	private final ObjectMapper mapper = new ObjectMapper();
	
	public SearchClient(final HttpClient client, final Properties properties){
		this.client = client;
		this.properties = properties;
	}
	
    private String send(HttpUriRequest request) throws ClientProtocolException, IOException{

        HttpResponse response = client.execute(request);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        
        StringBuilder sb = new StringBuilder();
        String line = "";
        while ((line = rd.readLine()) != null) {
        	sb.append(line);
        }
        return sb.toString();
    }
    
    private HttpPost makeUpdateRequest(String collectionName, Object postDocument) throws JsonProcessingException, UnsupportedEncodingException, URISyntaxException{
    	//final String url = String.format("%s/update?wt=json&commit=true", this.properties.getProperty(SEARCH_SERVER_URL));
    	
    	URIBuilder builder = new URIBuilder(this.properties.getProperty(SEARCH_SERVER_URL))
    		.setPath(String.format("/solr/%s/update", collectionName))
    		.addParameter("wt", "json")
    		.addParameter("commit", Boolean.TRUE.toString());
    	
    	
    	HttpPost post = new HttpPost(builder.build());
    	
    	final Map<String, Object> postAction = new HashMap<>();
    	postAction.put("add", postDocument);
    	
    	String content = mapper.writeValueAsString(postAction);
    	StringEntity entity = new StringEntity(content);
    	entity.setContentType("application/json");
    	post.setEntity(entity);
    	return post;
    }
    
	public void post(String collectionName, Object document) throws Exception{
		final Map<String, Object> postDocument = new HashMap<>();
		postDocument.put("boost", Integer.valueOf("1"));
		postDocument.put("overwrite", Boolean.TRUE);
		postDocument.put("commitWithin", Integer.valueOf("1000"));
		postDocument.put("doc", document);
		
		final HttpPost post = makeUpdateRequest(collectionName, postDocument);
    	
		String json = send(post);
		
		final MapType type = mapper.getTypeFactory().constructMapType(
		    Map.class, String.class, Object.class);
		final Map<String, Object> fields = mapper.readValue(json, type);
		
		checkAndThrowError(fields);
	}

	private void checkAndThrowError(final Map<String, Object> fields)
			throws Exception {
		final Map<String, Object> responseHeader = (Map<String, Object>)fields.get("responseHeader");
		final Integer status = Integer.parseInt(responseHeader.get("status").toString());
		if(status!=0){
			final Map<String, Object> errorMap = (Map<String, Object>)fields.get("error");
			String msg = (String)errorMap.get("msg");
			throw new Exception(msg);
		}
	}
	
    private HttpGet makeSelectRequest(String collection, SearchEntity searchEntity) throws JsonProcessingException, UnsupportedEncodingException, URISyntaxException{
    	
    	URIBuilder builder = new URIBuilder(this.properties.getProperty(SEARCH_SERVER_URL))
    		.setPath(String.format("/solr/%s/select", collection))
    		.addParameter("wt", "json")
    		.addParameter("fq", String.format("organisationId_i:%d", searchEntity.organisationId));
    		
    	
    	String[] tokens = searchEntity.text.split(" ");
    	List<String> queryParts = Arrays.stream(tokens).map(token -> 
    		String.format("text_s:*%s*", token.trim())
    	).collect(Collectors.toList());
    	
    	builder.addParameter("q", String.join(" OR ", queryParts));
    	
    	return new HttpGet(builder.build());
    }
	
	public <T> T[] search(String collectionName, Class<T[]> clazz, SearchEntity query) throws Exception{
				
		final HttpGet get = makeSelectRequest(collectionName, query);
		String json = send(get);
		
		final MapType type = mapper.getTypeFactory().constructMapType(
		    Map.class, String.class, Object.class);
		final Map<String, Object> fields = mapper.readValue(json, type);
		
		checkAndThrowError(fields);
		
		final Map<String, Object> responseMap = (Map<String, Object>)fields.get("response");
        final ArrayList resultList = (ArrayList)responseMap.get("docs");
		
		return mapper.convertValue(resultList, clazz);
		
	}
}
