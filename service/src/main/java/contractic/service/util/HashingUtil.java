package contractic.service.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Contains utility functions to handle client side hashing
 */
public final class HashingUtil {
	
	/**
	 * Hash the given value into an MD5 hash. Used to obfuscate database field IDs to the client
	 * @param value
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String get(Long value) throws NoSuchAlgorithmException{
		return get(value, 1);
	}
	
	/**
	 * Hash the given value into an MD5 hash. Used to obfuscate database field IDs to the client
	 * @param value
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String get(Long value, int signum) throws NoSuchAlgorithmException{
		MessageDigest m = MessageDigest.getInstance("MD5");
		m.reset();
		m.update(value.toString().getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(signum, digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	
	public static BigInteger combine(int[] values){
		int[] base = new int[]{2,3,5,7,9,11};
		return combine(values, base);
	}
	
	public static BigInteger combine(int[] values, int[] base){
		if(values.length != base.length)
			throw new UnsupportedOperationException(String.format("Only upto %s values are currently supported", base.length));
		BigInteger result = BigInteger.valueOf(1);
		for(int idx=0; idx<values.length; ++idx){
			BigInteger res1 = BigInteger.valueOf(base[idx]);
			BigInteger res2 = res1.pow(values[idx]);
			result = result.multiply(res2);
		}
		return result;
/*		return IntStream.range(0, base.length)
			.parallel()
			.mapToLong(idx -> BigInteger.valueOf(base[idx]).pow(values[idx]).longValue())
			.reduce(0,  (l, r) -> l * r);*/
	}
		
	/**
	 * Cantor pairing function
	 * @param a
	 * @param b
	 * @return
	 */
	public static long combine(int a, int b){
		return (a + b) * (a + b + 1) / 2 + b;
	}
	
	public static int[] uncombine(long a){
		int[] pair = new int[2];
		long t = (int)Math.floor((-1D + Math.sqrt(1D + 8 * a))/2D);
		long x = t * (t + 3) / 2 - a;
		long y = a - t * (t + 1) / 2;
		pair[0] = (int)x;
		pair[1] = (int)y;
		return pair;
	}

	private static SecretKeySpec generateKey(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		byte[] key = text.getBytes("UTF-8");
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		key = sha.digest(key);
		key = Arrays.copyOf(key, 16); // use only first 128 bit

		return new SecretKeySpec(key, "AES");
	}
	
	public static String cipher(final String text, final String key) throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException{
		Cipher cipher = Cipher.getInstance("AES");
		Key aesKey = generateKey(key);
		cipher.init(Cipher.ENCRYPT_MODE, aesKey);
		byte[] encrypted = cipher.doFinal(text.getBytes());
        return new String(encrypted);
	}
	
	public static String decipher(final String text, final String key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		Cipher cipher = Cipher.getInstance("AES");
		Key aesKey = generateKey(key);
        cipher.init(Cipher.DECRYPT_MODE, aesKey);
        return new String(cipher.doFinal(text.getBytes()));
	}
	
}
