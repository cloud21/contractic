package contractic.service.util;

import java.util.List;
import java.util.stream.Collectors;

public class XmlUtil {
	public static String toXml(final String nodeName, final String nodeValue){
		String xmlFormat = "<%1$s>%2$s</%1$s>";
		return String.format(xmlFormat, nodeName, nodeValue);
	}

	public static String toXml(final List<String> list, final String nodeName, final String itemNodeName){
		List<String> nodeList = list.stream().map(item -> toXml(itemNodeName, item)).collect(Collectors.toList());
		String xml = String.join("", nodeList);
		return toXml(nodeName, xml);
	}
}
