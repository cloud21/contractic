package contractic.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import contractic.service.domain.ContracticServiceException;
import contractic.service.domain.DomainService;
import contractic.service.domain.OrganisationDO;
import contractic.service.domain.UserDO;
import contractic.service.domain.UserService;
import contractic.service.util.ConcurrencyUtil;

/**
 * Main class to run contractic as an application. Used only for testing and profiling
 *
 */
public class Main 
{
	
	private static List<UserDO> getUsers(final DomainService<?, ?, ?, ?> userService, final UserDO sessionUserDO) throws ContracticServiceException{
        final Map<String, Object> params = new HashMap<>();
        params.put(UserService.GET_PARAM_ORGANISATIONID, sessionUserDO.getOrganisationId());
        return (List<UserDO>)userService.get(params);
	}
	
	private static UserDO getUsers(final DomainService<?, ?, ?, ?> userService, String login) throws ContracticServiceException{
        final Map<String, Object> params = new HashMap<>();
        params.put(UserService.GET_PARAM_USERNAME, login);
        return ((List<UserDO>)userService.get(params)).get(0);
	}
	
    public static void main( String[] args ) throws ContracticServiceException, InterruptedException
    {
        ApplicationContext ctx = 
        	      new AnnotationConfigApplicationContext(ServiceContextConfig.class);
        
        DomainService<?, ?, ?, ?> userService = (DomainService<?, ?, ?, ?>) ctx.getBean("userService");
        UserDO sessionUserDO = getUsers(userService, "mawandm@yahoo.co.uk");
        
        List<UserDO> userList = getUsers(userService, sessionUserDO);
        
        userList.forEach(userDO -> {
            ServiceTestProcess testProcess = new ServiceTestProcess(userDO, ctx);
                ConcurrencyUtil.IOBoundExecutorService.submit(testProcess);	
        });
        
        
        Thread.sleep(Long.MAX_VALUE);
  
    }
}
