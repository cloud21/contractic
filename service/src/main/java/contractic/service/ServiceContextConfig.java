package contractic.service;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import contractic.model.data.dao.AddressDAO;
import contractic.model.data.dao.AddressTypeDAO;
import contractic.model.data.dao.AuditActionDAO;
import contractic.model.data.dao.AuditDAO;
import contractic.model.data.dao.ContactDAO;
import contractic.model.data.dao.ContractDAO;
import contractic.model.data.dao.ContractDetailDAO;
import contractic.model.data.dao.ContractReferenceFormatDAO;
import contractic.model.data.dao.ContractTypeDAO;
import contractic.model.data.dao.CountryDAO;
import contractic.model.data.dao.CrudOperation;
import contractic.model.data.dao.DocumentDAO;
import contractic.model.data.dao.DocumentTypeDAO;
import contractic.model.data.dao.GroupDAO;
import contractic.model.data.dao.GroupObjectDAO;
import contractic.model.data.dao.GroupSetDAO;
import contractic.model.data.dao.GroupUserDAO;
import contractic.model.data.dao.ModuleDAO;
import contractic.model.data.dao.NoteDAO;
import contractic.model.data.dao.NoteTypeDAO;
import contractic.model.data.dao.OrganisationDAO;
import contractic.model.data.dao.PersonDAO;
import contractic.model.data.dao.ReportOrganisationDAO;
import contractic.model.data.dao.StatusDAO;
import contractic.model.data.dao.SupplierDAO;
import contractic.model.data.dao.SupplierPersonDAO;
import contractic.model.data.dao.TaskContactDAO;
import contractic.model.data.dao.TaskDAO;
import contractic.model.data.dao.UserDAO;
import contractic.model.data.dao.UserRoleDAO;
import contractic.model.data.dao.UserRolePermissionDAO;
import contractic.model.data.store.Address;
import contractic.model.data.store.AddressType;
import contractic.model.data.store.Audit;
import contractic.model.data.store.AuditAction;
import contractic.model.data.store.Contact;
import contractic.model.data.store.Contract;
import contractic.model.data.store.ContractDetail;
import contractic.model.data.store.ContractReferenceFormat;
import contractic.model.data.store.ContractType;
import contractic.model.data.store.Country;
import contractic.model.data.store.Document;
import contractic.model.data.store.DocumentType;
import contractic.model.data.store.Group;
import contractic.model.data.store.GroupObject;
import contractic.model.data.store.GroupSet;
import contractic.model.data.store.GroupUser;
import contractic.model.data.store.Module;
import contractic.model.data.store.Note;
import contractic.model.data.store.NoteType;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.ReportOrganisation;
import contractic.model.data.store.Status;
import contractic.model.data.store.Supplier;
import contractic.model.data.store.SupplierPerson;
import contractic.model.data.store.Task;
import contractic.model.data.store.TaskContact;
import contractic.model.data.store.User;
import contractic.model.data.store.UserRole;
import contractic.model.data.store.UserRolePermission;
import contractic.model.data.util.HibernateUtil;
import contractic.service.domain.AddressDO;
import contractic.service.domain.AddressService;
import contractic.service.domain.AddressTypeService;
import contractic.service.domain.AuditActionService;
import contractic.service.domain.AuditDO;
import contractic.service.domain.AuditService;
import contractic.service.domain.CommonService;
import contractic.service.domain.ContactDO;
import contractic.service.domain.ContactService;
import contractic.service.domain.ContractDO;
import contractic.service.domain.ContractDetailDO;
import contractic.service.domain.ContractDetailService;
import contractic.service.domain.ContractReferenceFormatDO;
import contractic.service.domain.ContractReferenceFormatService;
import contractic.service.domain.ContractService;
import contractic.service.domain.ContractTypeDO;
import contractic.service.domain.ContractTypeService;
import contractic.service.domain.CountryDO;
import contractic.service.domain.CountryService;
import contractic.service.domain.DocumentDO;
import contractic.service.domain.DocumentService;
import contractic.service.domain.DocumentTypeDO;
import contractic.service.domain.DocumentTypeService;
import contractic.service.domain.DomainService;
import contractic.service.domain.FileDO;
import contractic.service.domain.FileService;
import contractic.service.domain.GroupDO;
import contractic.service.domain.GroupObjectDO;
import contractic.service.domain.GroupObjectService;
import contractic.service.domain.GroupService;
import contractic.service.domain.GroupSetDO;
import contractic.service.domain.GroupSetService;
import contractic.service.domain.GroupUserDO;
import contractic.service.domain.MessageDO;
import contractic.service.domain.ModuleDO;
import contractic.service.domain.ModuleService;
import contractic.service.domain.NoteDO;
import contractic.service.domain.NoteService;
import contractic.service.domain.NoteTypeDO;
import contractic.service.domain.NoteTypeService;
import contractic.service.domain.OrganisationDO;
import contractic.service.domain.OrganisationService;
import contractic.service.domain.PersonDO;
import contractic.service.domain.PersonService;
import contractic.service.domain.ReportDO;
import contractic.service.domain.ReportService;
import contractic.service.domain.SearchEntityDO;
import contractic.service.domain.SearchService;
import contractic.service.domain.SessionDO;
import contractic.service.domain.SessionService;
import contractic.service.domain.StaticDO;
import contractic.service.domain.StatusService;
import contractic.service.domain.SupplierDO;
import contractic.service.domain.SupplierPersonDO;
import contractic.service.domain.SupplierPersonService;
import contractic.service.domain.SupplierService;
import contractic.service.domain.TaskContactDO;
import contractic.service.domain.TaskContactService;
import contractic.service.domain.TaskDO;
import contractic.service.domain.TaskService;
import contractic.service.domain.UserDO;
import contractic.service.domain.UserRoleDO;
import contractic.service.domain.UserRolePermissionDO;
import contractic.service.domain.UserRolePermissionService;
import contractic.service.domain.UserRoleService;
import contractic.service.domain.UserService;
import contractic.service.util.EhcacheUtil;
import contractic.service.util.SearchClient.SearchEntity;

@Configuration
@PropertySource("classpath:application.properties")
@PropertySource("classpath:version.properties")
public class ServiceContextConfig {

	@Value("${documentFileRoot}")
	private String documentFileRoot;
	
	@Value("${enableSecurity}")
	private String enableSecurity;
	
	@Value("${enableLogging}")
	private String enableLogging;
	
	@Value("${version}")
	private String version;
	
	@Value("${environment}")
	private String environment;
	
	private CrudOperation<Organisation> organisationDAOBean;
	private volatile DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationServiceBean;

	private volatile DomainService<UserDO, Long, SessionDO, MessageDO> sessionServiceBean;

	private CrudOperation<Status> statusDAOBean;
	private volatile StatusService statusServiceBean;

	private CrudOperation<AuditAction> auditActionDAOBean;
	private volatile DomainService<AuditAction, Long, StaticDO, MessageDO> auditActionServiceBean;
	
	private CrudOperation<Audit> auditDAOBean;
	private volatile DomainService<Audit, Long, AuditDO, MessageDO> auditServiceBean;
	
	private CrudOperation<Person> personDAOBean;
	private volatile DomainService<Person, Long, PersonDO, MessageDO> personServiceBean;

	private CrudOperation<User> userDAOBean;
	private DomainService<User, Long, UserDO, MessageDO> userServiceBean;
	
	private CrudOperation<AddressType> addressTypeDAOBean;
	private DomainService<AddressType, Long, StaticDO, MessageDO> addressTypeServiceBean;
	
	private CrudOperation<Contact> contactDAOBean;
	private DomainService<Contact, Long, ContactDO, MessageDO> contactServiceBean;
	
	private CrudOperation<Address> addressDAOBean;
	private DomainService<Address, Long, AddressDO, MessageDO> addressServiceBean;
	
	private CrudOperation<Module> moduleDAOBean;
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleServiceBean;
	
	private CrudOperation<Note> noteDAOBean;
	private DomainService<Note, Long, NoteDO, MessageDO> noteServiceBean;
	
	private CrudOperation<Supplier> supplierDAOBean;
	private DomainService<Supplier, Long, SupplierDO, MessageDO> supplierServiceBean;
	
	private CrudOperation<NoteType> noteTypeDAOBean;
	private DomainService<NoteType, Long, NoteTypeDO, MessageDO> noteTypeServiceBean;
	
	private CrudOperation<DocumentType> documentTypeDAOBean;
	private DomainService<DocumentType, Long, DocumentTypeDO, MessageDO> documentTypeServiceBean;
	
	private CrudOperation<ContractType> contractTypeDAOBean;
	private DomainService<ContractType, Long, ContractTypeDO, MessageDO> contractTypeServiceBean;

	private CrudOperation<Contract> contractDAOBean;
	private DomainService<Contract, Long, ContractDO, MessageDO> contractServiceBean;
	
	private CrudOperation<ContractDetail> contractDetailDAOBean;
	private DomainService<ContractDetail, Long, ContractDetailDO, MessageDO> contractDetailServiceBean;
	
	private CrudOperation<Group> groupDAOBean;
	private volatile DomainService<Group, Long, GroupDO, MessageDO> groupServiceBean;
	
	private CrudOperation<Document> documentDAOBean;
	private DomainService<Document, Long, DocumentDO, MessageDO> documentServiceBean;
	
	private CrudOperation<Task> taskDAOBean;
	private DomainService<Task, Long, TaskDO, MessageDO> taskServiceBean;
	
	private CrudOperation<TaskContact> taskContactDAOBean;
	private DomainService<TaskContact, Long, TaskContactDO, MessageDO> taskContactServiceBean;
	
	private CrudOperation<SupplierPerson> supplierPersonDAOBean;
	private DomainService<SupplierPerson, Long, SupplierPersonDO, MessageDO> supplierPersonService;
	
	private CrudOperation<GroupUser> groupUserDAOBean;
	private DomainService<GroupUser, Long, GroupUserDO, MessageDO> groupUserService;
	
	private CrudOperation<UserRole> userRoleDAOBean;
	private DomainService<UserRole, Long, UserRoleDO, MessageDO> userRoleService;
	
	private CrudOperation<UserRolePermission> userRolePermissionDAOBean;
	private DomainService<UserRolePermission, Long, UserRolePermissionDO, MessageDO> userRolePermissionService;
	
	private CrudOperation<ReportOrganisation> reportOrganisationDAOBean;
	private DomainService<ReportOrganisation, Long, ReportDO, MessageDO> reportService;
	
	private CrudOperation<GroupObject> groupObjectDAOBean;
	private DomainService<GroupObject, Long, GroupObjectDO, MessageDO> groupObjectService;
	
	private CrudOperation<GroupSet> groupSetDAOBean;
	private DomainService<GroupSet, Long, GroupSetDO, MessageDO> groupSetService;

	private CrudOperation<Country> countryDAOBean;
	private DomainService<Country, Long, CountryDO, MessageDO> countryService;
	
	private CrudOperation<ContractReferenceFormat> contractReferenceFormatDAOBean;
	private DomainService<ContractReferenceFormat, Long, ContractReferenceFormatDO, MessageDO> contractReferenceFormatService;
	
	private CommonService commonServiceBean;
	
	private DomainService<FileDO, Long, FileDO, MessageDO> fileServiceBean;
	
	private DomainService<SearchEntity, Long, SearchEntityDO, MessageDO> searchServiceBean;
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean
	public DomainService<UserDO, Long, SessionDO, MessageDO> sessionService() {
		if (sessionServiceBean == null) {
			sessionServiceBean = new SessionService();
		}
		return sessionServiceBean;
	}

	@Bean
	public CrudOperation<Status> statusDAO() {
		statusDAOBean = new StatusDAO();
		return statusDAOBean;
	}

	@Bean
	public StatusService statusService() {
		if (statusServiceBean == null) {
			statusServiceBean = new StatusService();
		}
		return statusServiceBean;
	}
	
	@Bean
	public CrudOperation<SupplierPerson> supplierPersonDAO() {
		supplierPersonDAOBean = new SupplierPersonDAO();
		return supplierPersonDAOBean;
	}

	@Bean
	public DomainService<SupplierPerson, Long, SupplierPersonDO, MessageDO> supplierPersonService() {
		if (supplierPersonService == null) {
			supplierPersonService = new SupplierPersonService();
		}
		return supplierPersonService;
	}
	
	@Bean
	public CrudOperation<Audit> auditDAO() {
		auditDAOBean = new AuditDAO();
		return auditDAOBean;
	}

	@Bean
	public DomainService<Audit, Long, AuditDO, MessageDO> auditService() {
		if (auditServiceBean == null) {
			auditServiceBean = new AuditService();
		}
		return auditServiceBean;
	}
	
	@Bean
	public CrudOperation<AuditAction> auditActionDAO() {
		auditActionDAOBean = new AuditActionDAO();
		return auditActionDAOBean;
	}

	@Bean
	public DomainService<AuditAction, Long, StaticDO, MessageDO> auditActionService() {
		if (auditActionServiceBean == null) {
			auditActionServiceBean = new AuditActionService();
		}
		return auditActionServiceBean;
	}
	
	@Bean
	public CrudOperation<User> userDAO() {
		userDAOBean = new UserDAO();
		return userDAOBean;
	}

	@Bean
	public DomainService<User, Long, UserDO, MessageDO> userService() {
		if (userServiceBean == null) {
			userServiceBean = new UserService();
		}
		return userServiceBean;
	}

	@Bean
	public CrudOperation<Person> personDAO() {
		personDAOBean = new PersonDAO();
		return personDAOBean;
	}

	@Bean
	public DomainService<Person, Long, PersonDO, MessageDO> personService() {
		if (personServiceBean == null) {
			personServiceBean = new PersonService();
		}
		return personServiceBean;
	}

	@Bean
	public CrudOperation<Organisation> organisationDAO() {
		organisationDAOBean = new OrganisationDAO();
		return organisationDAOBean;
	}
	
	@Bean
	public DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService() {
		if (organisationServiceBean == null) {
			organisationServiceBean = new OrganisationService();
		}
		return organisationServiceBean;
	}
	
	@Bean
	public CrudOperation<AddressType> addressTypeDAO() {
		addressTypeDAOBean = new AddressTypeDAO();
		return addressTypeDAOBean;
	}
	
	@Bean
	public DomainService<AddressType, Long, StaticDO, MessageDO> addressTypeService() {
		if (addressTypeServiceBean == null) {
			addressTypeServiceBean = new AddressTypeService();
		}
		return addressTypeServiceBean;
	}

	@Bean
	public CrudOperation<Module> moduleDAO() {
		moduleDAOBean = new ModuleDAO();
		return moduleDAOBean;
	}
	
	@Bean
	public DomainService<Module, Long, ModuleDO, MessageDO> moduleService() {
		if (moduleServiceBean == null) {
			moduleServiceBean = new ModuleService();
		}
		return moduleServiceBean;
	}
	
	@Bean
	public CrudOperation<Address> addressDAO() {
		addressDAOBean = new AddressDAO();
		return addressDAOBean;
	}
	
	@Bean
	public DomainService<Address, Long, AddressDO, MessageDO> addressService() {
		if (addressServiceBean == null) {
			addressServiceBean = new AddressService();
		}
		return addressServiceBean;
	}
	
	@Bean
	public CrudOperation<Note> noteDAO() {
		noteDAOBean = new NoteDAO();
		return noteDAOBean;
	}
	
	@Bean
	public DomainService<Note, Long, NoteDO, MessageDO> noteService() {
		if (noteServiceBean == null) {
			noteServiceBean = new NoteService();
		}
		return noteServiceBean;
	}
	
	@Bean
	public CrudOperation<NoteType> noteTypeDAO() {
		noteTypeDAOBean = new NoteTypeDAO();
		return noteTypeDAOBean;
	}
	
	@Bean
	public DomainService<NoteType, Long, NoteTypeDO, MessageDO> noteTypeService() {
		if (noteTypeServiceBean == null) {
			noteTypeServiceBean = new NoteTypeService();
		}
		return noteTypeServiceBean;
	}
	
	@Bean
	public CrudOperation<Supplier> supplierDAO() {
		supplierDAOBean = new SupplierDAO();
		return supplierDAOBean;
	}
	
	@Bean
	public DomainService<Supplier, Long, SupplierDO, MessageDO> supplierService() {
		if (supplierServiceBean == null) {
			supplierServiceBean = new SupplierService();
		}
		return supplierServiceBean;
	}
	
	@Bean
	public CrudOperation<Contact> contactDAO() {
		contactDAOBean = new ContactDAO();
		return contactDAOBean;
	}
	
	@Bean
	public DomainService<Contact, Long, ContactDO, MessageDO> contactService() {
		if (contactServiceBean == null) {
			contactServiceBean = new ContactService();
		}
		return contactServiceBean;
	}
	
	@Bean
	public CrudOperation<DocumentType> documentTypeDAO() {
		documentTypeDAOBean = new DocumentTypeDAO();
		return documentTypeDAOBean;
	}
	
	@Bean
	public DomainService<DocumentType, Long, DocumentTypeDO, MessageDO> documentTypeService() {
		if (documentTypeServiceBean == null) {
			documentTypeServiceBean = new DocumentTypeService();
		}
		return documentTypeServiceBean;
	}
	
	@Bean
	public CrudOperation<ContractType> contractTypeDAO() {
		contractTypeDAOBean = new ContractTypeDAO();
		return contractTypeDAOBean;
	}
	
	@Bean
	public DomainService<ContractType, Long, ContractTypeDO, MessageDO> contractTypeService() {
		if (contractTypeServiceBean == null) {
			contractTypeServiceBean = new ContractTypeService();
		}
		return contractTypeServiceBean;
	}
	
	@Bean
	public CrudOperation<Contract> contractDAO() {
		contractDAOBean = new ContractDAO();
		return contractDAOBean;
	}
	
	@Bean
	public DomainService<Contract, Long, ContractDO, MessageDO> contractService() {
		if (contractServiceBean == null) {
			contractServiceBean = new ContractService();
		}
		return contractServiceBean;
	}
	
	@Bean
	public CrudOperation<ContractDetail> contractDetailDAO() {
		contractDetailDAOBean = new ContractDetailDAO();
		return contractDetailDAOBean;
	}
	
	@Bean
	public DomainService<ContractDetail, Long, ContractDetailDO, MessageDO> contractDetailService() {
		if (contractDetailServiceBean == null) {
			contractDetailServiceBean = new ContractDetailService();
		}
		return contractDetailServiceBean;
	}
	
	@Bean
	public CrudOperation<Group> groupDAO() {
		groupDAOBean = new GroupDAO();
		return groupDAOBean;
	}
	
	@Bean
	public DomainService<Group, Long, GroupDO, MessageDO> groupService() {
		if (groupServiceBean == null) {
			groupServiceBean = new GroupService();
		}
		return groupServiceBean;
	}
	
	@Bean
	public CrudOperation<Document> documentDAO() {
		documentDAOBean = new DocumentDAO();
		return documentDAOBean;
	}
	
	@Bean
	public DomainService<Document, Long, DocumentDO, MessageDO> documentService() throws Exception {
		if (documentServiceBean == null) {
			File rootFile = new File(documentFileRoot);
			if(!rootFile.exists())
				throw new Exception(String.format("Invalid document file '%s' root provided", documentFileRoot));
			DocumentService serviceBean = new DocumentService();
			serviceBean.setDocumentFileRoot(documentFileRoot);
			documentServiceBean = serviceBean;
		}
		return documentServiceBean;
	}
	
	@Bean
	public CrudOperation<Task> taskDAO() {
		taskDAOBean = new TaskDAO();
		return taskDAOBean;
	}
	
	@Bean
	public DomainService<Task, Long, TaskDO, MessageDO> taskService() {
		if (taskServiceBean == null) {
			taskServiceBean = new TaskService();
		}
		return taskServiceBean;
	}
	
	@Bean
	public CrudOperation<TaskContact> taskContactDAO() {
		taskContactDAOBean = new TaskContactDAO();
		return taskContactDAOBean;
	}
	
	@Bean
	public DomainService<TaskContact, Long, TaskContactDO, MessageDO> taskContactService() {
		if (taskContactServiceBean == null) {
			taskContactServiceBean = new TaskContactService();
		}
		return taskContactServiceBean;
	}
	
	@Bean
	public CrudOperation<GroupUser> groupUserDAO() {
		groupUserDAOBean = new GroupUserDAO();
		return groupUserDAOBean;
	}
	
/*	@Bean
	public DomainService<GroupUser, Long, GroupUserDO, MessageDO> groupUserService() {
		if (groupUserServiceBean == null) {
			groupUserServiceBean = new GroupUserService();
		}
		return taskContactServiceBean;
	}*/
	
	@Bean
	public CrudOperation<UserRole> userRoleDAO() {
		userRoleDAOBean = new UserRoleDAO();
		return userRoleDAOBean;
	}
	
	@Bean
	public DomainService<UserRole, Long, UserRoleDO, MessageDO> userRoleService() {
		if (userRoleService == null) {
			userRoleService = new UserRoleService();
		}
		return userRoleService;
	}
	
	@Bean
	public CrudOperation<UserRolePermission> userRolePermissionDAO() {
		userRolePermissionDAOBean = new UserRolePermissionDAO();
		return userRolePermissionDAOBean;
	}
	
	@Bean
	public DomainService<UserRolePermission, Long, UserRolePermissionDO, MessageDO> userRolePermissionService() {
		if (userRolePermissionService == null) {
			userRolePermissionService = new UserRolePermissionService();
		}
		return userRolePermissionService;
	}
	
	@Bean
	public CrudOperation<ReportOrganisation> reportOrganisationDAO() {
		reportOrganisationDAOBean = new ReportOrganisationDAO();
		return reportOrganisationDAOBean;
	}
	
	@Bean
	public DomainService<ReportOrganisation, Long, ReportDO, MessageDO> reportService() {
		if (this.reportService == null) {
			
			//REPORT_VIEW_HOME
			reportService = new ReportService();
		}
		return reportService;
	}
	
	@Bean
	public CrudOperation<GroupObject> groupObjectDAO() {
		groupObjectDAOBean = new GroupObjectDAO();
		return groupObjectDAOBean;
	}
	
	@Bean
	public DomainService<GroupObject, Long, GroupObjectDO, MessageDO> groupObjectService() {
		if (this.groupObjectService == null) {
			groupObjectService = new GroupObjectService();
		}
		return groupObjectService;
	}
	
	@Bean
	public CrudOperation<GroupSet> groupSetDAO() {
		groupSetDAOBean = new GroupSetDAO();
		return groupSetDAOBean;
	}
	
	@Bean
	public DomainService<GroupSet, Long, GroupSetDO, MessageDO> groupSetService() {
		if (this.groupSetService == null) {
			groupSetService = new GroupSetService();
		}
		return groupSetService;
	}
	
	
	@Bean
	public CrudOperation<Country> countryDAO() {
		countryDAOBean = new CountryDAO();
		return countryDAOBean;
	}
	
	@Bean
	public DomainService<Country, Long, CountryDO, MessageDO> countryService() {
		if (this.countryService == null) {
			countryService = new CountryService();
		}
		return countryService;
	}
	
	@Bean
	public CrudOperation<ContractReferenceFormat> contractReferenceFormatDAO() {
		contractReferenceFormatDAOBean = new ContractReferenceFormatDAO();
		return contractReferenceFormatDAOBean;
	}
	
	@Bean
	public DomainService<ContractReferenceFormat, Long, ContractReferenceFormatDO, MessageDO> contractReferenceFormatService() {
		if (this.contractReferenceFormatService == null) {
			contractReferenceFormatService = new ContractReferenceFormatService();
		}
		return contractReferenceFormatService;
	}
	
	@Bean
	public DomainService<FileDO, Long, FileDO, MessageDO> fileService() {
		if (fileServiceBean == null) {
			fileServiceBean = new FileService();
		}
		return fileServiceBean;
	}
	
	@Bean
	public DomainService<SearchEntity, Long, SearchEntityDO, MessageDO> searchService() {
		if (searchServiceBean == null) {
			searchServiceBean = new SearchService(this.properties);
		}
		return searchServiceBean;
	}
	
	@Bean
	public CommonService commonService() {
		if (commonServiceBean == null) {
			commonServiceBean = new CommonService();
			commonServiceBean.setVersion(version);
			commonServiceBean.setEnvironment(environment);
		}
		return commonServiceBean;
	}
	
	private final Properties properties;
	
	public ServiceContextConfig() throws IOException{
		Resource resource = new ClassPathResource("/application.properties");
		properties = PropertiesLoaderUtils.loadProperties(resource);
	}
	
	@PostConstruct
	public void PostConstruct() throws Exception {
		HibernateUtil.initialize();
		EhcacheUtil.initialize();
		
		organisationService().init();
		personService().init();
		this.userService().init();
		this.sessionService().init();
		this.addressTypeService().init();
		this.addressService().init();
		this.moduleService().init();
		this.contactService().init();
		
		this.supplierService().init();
		this.noteTypeService().init();
		
		this.statusService().init();
		
		this.documentTypeService().init();
		
		this.contractTypeService().init();
		this.contractService().init();
		
		this.contractDetailService().init();
		this.groupService().init();
		
		this.documentService().init();
		this.fileService().init();
		
		this.taskService().init();
		
		this.noteService().init();
		
		this.taskContactService().init();
		
		this.auditActionService().init();
		this.auditService().init();
		
		this.userRoleService().init();
		this.userRolePermissionService().init();
		this.reportService().init();
		this.groupObjectService().init();
		this.groupSetService().init();
		this.countryService().init();
		this.contractReferenceFormatService().init();
		
		this.searchService().init();
	}

	@PreDestroy
	public void PreDestroy() throws Exception {
		organisationServiceBean.destroy();
		EhcacheUtil.destroy();
	}
}
