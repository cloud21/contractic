package contractic.service.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Assert;
import org.junit.Test;

public class HasingUtilUtilTest{
	@Test
	public void test() throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		int[] values = new int[]{1003, 1002};
		long keyVal = HashingUtil.combine(values[0], values[1]);
		int[] uvalues = HashingUtil.uncombine(keyVal);
		Assert.assertArrayEquals(uvalues, values);
	}
}
