package contractic.service.util;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import contractic.model.data.dao.BaseDAOTest;
import contractic.service.domain.ContractCCNDO;

public class XmlTransformUtilTest  extends BaseDAOTest{

	@Test
	public void test() {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("UserId", new Integer(1000));
		paramMap.put("Action", "Read");
		
		XStream xStream = new XStream(new StaxDriver());
		xStream.alias("map", java.util.Map.class);
		String xml = xStream.toXML(paramMap);

		System.out.println(xml);
	}

}
