package contractic.service.util;

import java.util.List;
import java.util.Properties;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import contractic.model.data.dao.CrudOperation;
import contractic.model.data.store.Contract;
import contractic.model.data.store.Document;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.service.ServiceContextConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceContextConfig.class})
public class SearchClientTest {

	@Autowired
	private CrudOperation<Document> documentDAO;
	@Autowired
	private CrudOperation<Contract> contractDAO;
	@Autowired
	private CrudOperation<Organisation> organisationDAO;
	
	@Test
	@Ignore
	public void testSearchIndexContract() throws Exception {
		final Properties properties = new Properties();
		
		properties.setProperty(SearchClient.SEARCH_SERVER_URL, "http://localhost:8983/solr/contractic");
		
		try(final CloseableHttpClient httpClient = HttpClientBuilder.create().build()){
			final SearchClient searchClient = new SearchClient(httpClient, properties);
			
			final List<Organisation> organisationList = organisationDAO.read(new Organisation());
			for(Organisation organisation : organisationList){
				Contract contract = new Contract();
				contract.setOrganisation(organisation);
				
				List<Contract> contractList = contractDAO.read(contract);
				
				for(Contract c : contractList){
					SearchClient.SearchEntity searchEntity = new SearchClient.SearchEntity();
					searchEntity.setId(c.getId());
					searchEntity.setCollection(Module.CONTRACT);
					searchEntity.setOrganisationId(c.getOrganisation().getId());
					searchEntity.setText(String.join(",", 
							new String[]{
							c.getTitle(), 
							c.getDescription(), 
							c.getOwner().getFirstName(),
							c.getOwner().getMiddleName(),
							c.getOwner().getLastName(),
							c.getReference(),
							c.getSupplier().getName(),
							c.getType().getName()}));
					
					searchClient.post("contract", searchEntity);
				}
			}
		}
		
		//SearchClient searchClien
	}
	
	@Test
	@Ignore
	public void testSearchIndexDocument() throws Exception {
		final Properties properties = new Properties();
		
		properties.setProperty(SearchClient.SEARCH_SERVER_URL, "http://localhost:8983/solr/contractic");
		
		try(final CloseableHttpClient httpClient = HttpClientBuilder.create().build()){
			final SearchClient searchClient = new SearchClient(httpClient, properties);
			
			final List<Organisation> organisationList = organisationDAO.read(new Organisation());
			for(Organisation organisation : organisationList){
				Document document = new Document();
				document.setOrganisation(organisation);
				
				List<Document> documentList = documentDAO.read(document);
				
				for(Document doc : documentList){
					SearchClient.SearchEntity searchEntity = new SearchClient.SearchEntity();
					searchEntity.setId(doc.getId());
					searchEntity.setCollection(Module.DOCUMENT);
					searchEntity.setOrganisationId(document.getOrganisation().getId());
					searchEntity.setText(String.join(",", new String[]{doc.getTitle(), doc.getDescription(), doc.getFileName()}));
					
					searchClient.post("document", searchEntity);
				}
			}
		}
		
		//SearchClient searchClien
	}
	
	@Test
	public void testSearch() throws Exception{
		final Properties properties = new Properties();
		
		properties.setProperty(SearchClient.SEARCH_SERVER_URL, "http://localhost:8983/solr/contractic/");
		
		try(CloseableHttpClient httpClient = HttpClientBuilder.create().build()){
			SearchClient searchClient = new SearchClient(httpClient, properties);
			
			SearchClient.SearchEntity entity = new SearchClient.SearchEntity();
			entity.setOrganisationId(1000L);
			entity.setText("*CreateTestCategory*");
			
			final SearchClient.SearchEntity[] docList = searchClient.search(Module.CONTRACT.toLowerCase(), SearchClient.SearchEntity[].class, entity);
			System.out.println(new ObjectMapper().writeValueAsString(docList));
		}
	}

}
