package contractic.service.domain;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Assert;
import org.junit.Test;

public class AuditJobTest {

	@Test
	public void test() {
		ValueDO source = new ValueDO();
		AuditDO auditDO = AuditJob.auditForeignFieldChange(source, null, "Field");
		Assert.assertNotNull(auditDO);
	}

	@Test
	public void test2() {
		ValueDO source = new ValueDO("sourceId", "Source Value");
		ValueDO target = new ValueDO("targetId", "Target Value");
		AuditDO auditDO = AuditJob.auditForeignFieldChange(source, target, "Field");
		Assert.assertNotNull(auditDO);
	}
	
	@Test
	public void test3() {
		AuditDO auditDO = AuditJob.auditForeignFieldChange(null, null, "Field");
		Assert.assertNull(auditDO);
	}
	
	@Test
	public void test4() throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		AuditDO auditDO = AuditJob.auditDateFieldChange(Long.toString(dateFormat.parse("12/02/2016").getTime()), "12/03/2016", "Field");
		Assert.assertNotNull(auditDO);
	}
	
	@Test
	public void test5() throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		AuditDO auditDO = AuditJob.auditDateFieldChange(null, "12/02/2016", "Field");
		Assert.assertNotNull(auditDO);
	}
	
	@Test
	public void test6() throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		AuditDO auditDO = AuditJob.auditDateFieldChange(Long.toString(dateFormat.parse("12/02/2016").getTime()), null, "Field");
		Assert.assertNotNull(auditDO);
	}
}
