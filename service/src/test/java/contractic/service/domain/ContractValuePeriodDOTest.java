package contractic.service.domain;

import org.junit.Ignore;
import org.junit.Test;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

public class ContractValuePeriodDOTest {

	
	@Test
	public void testSerialization() {
		PeriodDO valuePeriod = new PeriodDO();
		//valuePeriod.setCycle(new ContractValuePeriodDO.Cycle("Q", 5));
		valuePeriod.setFrom("2015-01-01");
		valuePeriod.setTo("2016-12-31");
		XStream xstream = new XStream(new StaxDriver());
		xstream.processAnnotations(PeriodDO.class);
		String xml = xstream.toXML(valuePeriod);
		System.out.println(xml);
	}
	
	//@Ignore
	@Test
	public void testDeserialization(){
		String xml = "<period><cycle><frequency>Q</frequency><value>5</value></cycle></period>";
		XStream xstream = new XStream();
		xstream.processAnnotations(PeriodDO.class);
		PeriodDO contractValue = (PeriodDO)xstream.fromXML(xml);
		System.out.println(contractValue);
	}


}
