package contractic.service.domain;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class NoteAuditTest {

	@Test
	public void testRun() {
		NoteDO source = new NoteDO();
		source.setHeader("Header");
		source.setDetail("Detail");
		
		NoteDO target = new NoteDO();
		target.setHeader("Header1");
		
		AuditJob<NoteDO> auditJob = new NoteAudit();
		List<AuditDO> auditList = auditJob.run(source, target);
		Assert.assertEquals(2, auditList.size());
	}

}
