package contractic.service.domain;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class ContractServiceReferenceGeneratorTest {

	private String format;
	private Long userId;
	
	public  ContractServiceReferenceGeneratorTest(String format, String userId, String expected){
		this.format = format;
		this.userId = Long.parseLong(userId);
	}
	
    @Parameterized.Parameters
    public static List<String[]> params() {
    	Random random = new Random();
        return Arrays.asList(new String[][] { 
        		{"########AA##", new Integer(random.nextInt(10000) + 1000).toString(), MessageDO.OK},
        		{"HO####CT##", new Integer(random.nextInt(10000) + 1000).toString(), MessageDO.ERROR},
        		{"AAA/#########-OP", new Integer(random.nextInt(10000) + 1000).toString(), MessageDO.OK},
        		{"AAA-############", new Integer(random.nextInt(10000) + 1000).toString(), MessageDO.ERROR},
        		{"CR############", new Integer(random.nextInt(10000) + 1000).toString().toString(), MessageDO.OK},
        		});
    }
    
	
	@Test
	public void testGenerateReference() {
		String reference = ContractService.generateReference(format, userId);
		System.out.println(reference);
	}

}
