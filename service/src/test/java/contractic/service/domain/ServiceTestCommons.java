package contractic.service.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Assert;

import contractic.model.data.store.Module;
import contractic.model.data.store.User;
import contractic.model.data.store.UserRole;


public class ServiceTestCommons {
	
	public static ModuleDO getModule(DomainService<Module, Long, ModuleDO, MessageDO> moduleService, UserDO userDO, String name) throws ContracticServiceException{

		final Map<String, Object> params = new HashMap<>();
		params.put(ModuleService.GET_PARAM_NAME, name);
		List<ModuleDO> list = moduleService.get(params);
		Assert.assertTrue(list!=null && list.size()!=0);
		
		list = list.stream()
				.filter(role -> role.getName().equals(name))
				.collect(Collectors.toList());
		
		return list.get(0);
	}

	public static SessionDO getSession(DomainService<String, Long, SessionDO, MessageDO> sessionService, 
			String username, String password) throws ContracticServiceException{
		SessionDO sessionDO = new SessionDO();
		sessionDO.setUsername("michael.sekamanya@cloud21.net");
		sessionDO.setPassword("12345");
		MessageDO messageDO = sessionService.create(sessionDO);
		
		return sessionDO;
	}
	
	public static UserDO getUser(DomainService<User, Long, UserDO, MessageDO> userService, 
			String username) throws ContracticServiceException{
		final Map<String, Object> params = new HashMap<>();
		params.put(UserService.GET_PARAM_USERNAME, username);
		List<UserDO> userList = userService.get(params);
		assert userList.size() == 1;
		return userList.get(0);
	}
	
	public static UserRoleDO getUserRole(DomainService<UserRole, Long, UserRoleDO, MessageDO> userRoleService, UserDO userDO, String name) throws ContracticServiceException{

		final Map<String, Object> params = new HashMap<>();
		params.put(UserRoleService.GET_PARAM_ORGANISATIONID, userDO.getOrganisationId());
		List<UserRoleDO> roleList = userRoleService.get(params);
		Assert.assertTrue(roleList!=null && roleList.size()!=0);
		
		roleList = roleList.stream()
				.filter(role -> role.getName().equals(name))
				.collect(Collectors.toList());
		if(roleList.size() == 0)
			return null;
		return roleList.get(0);
	}
}
