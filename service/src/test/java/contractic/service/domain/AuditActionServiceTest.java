package contractic.service.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import contractic.model.data.store.AuditAction;
import contractic.service.ServiceContextConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceContextConfig.class})
public class AuditActionServiceTest {

	@Autowired
	private DomainService<AuditAction, Long, StaticDO, MessageDO> auditActionService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}
	

	@Test
	public void testGet() throws ContracticServiceException {
		try {
			Map<String, Object> params = new HashMap<>();
			
			params.put(AuditActionService.GET_PARAM_NAME, "Create");
			
			List<StaticDO> list = auditActionService.get(params);
			
			Assert.assertTrue(list!=null && list.size() > 0);
		} catch (ContracticServiceException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
}
