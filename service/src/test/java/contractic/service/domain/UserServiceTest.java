package contractic.service.domain;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import contractic.model.data.store.User;
import contractic.model.data.store.UserRole;
import contractic.model.data.store.UserRolePermission;
import contractic.service.ServiceContextConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceContextConfig.class})
public class UserServiceTest {

	@Autowired
	private DomainService<User, Long, UserDO, MessageDO> userService;
	@Autowired
	private DomainService<UserRole, Long, UserRoleDO, MessageDO> userRoleService;
	@Autowired
	private DomainService<UserRolePermission, Long, UserRolePermissionDO, MessageDO> userRolePermissionService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@Ignore
	@Test
	public void testGet() throws ContracticServiceException {
		try {
			Map<String, Object> params = new HashMap<>();
			
			params.put(UserService.GET_PARAM_USERNAME, "admin");
			
			List<UserDO> list = this.userService.get(params);
			//final List<PersonDO> personList = personService.get(orgList.get(0).getId());
			assertNotNull(list);
		} catch (ContracticServiceException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testUpdate() throws ContracticServiceException{
		
		String username = "Kim.Hillman@cloud21.net";
		
		UserDO userDO = ServiceTestCommons.getUser(userService, username);
		
		UserRoleDO userRoleDO = ServiceTestCommons.getUserRole(userRoleService, userDO, "ContractManager");
		
		userDO.setRole(new ValueDO(userRoleDO.getId(), userRoleDO.getName()));
		
		MessageDO resultDO = userService.update(userDO);
		
		Assert.assertTrue(resultDO != null && resultDO.getType()!=null && resultDO.getType().equals(MessageDO.OK));
	}
	
}
