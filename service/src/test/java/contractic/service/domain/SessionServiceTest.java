package contractic.service.domain;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import contractic.model.data.store.Organisation;
import contractic.model.data.store.User;
import contractic.service.ServiceContextConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceContextConfig.class})
public class SessionServiceTest {

	@Autowired
	private DomainService<UserDO, Long, SessionDO, MessageDO> sessionService;
	@Autowired
	private DomainService<User, Long, UserDO, MessageDO> userService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testGet() throws ContracticServiceException {
		try {
			String username = "michael.sekamanya@cloud21.net";
			SessionDO sessionDO = new SessionDO();
			sessionDO.setUsername(username);
			sessionDO.setPassword("12345");
			
			UserDO userDO = ServiceTestCommons.getUser(userService, username);
			sessionDO.setUser(userDO);
			
			MessageDO messageDO = sessionService.create(sessionDO);
			
			
			assertNotNull(messageDO);
		} catch (ContracticServiceException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
}
