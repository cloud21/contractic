package contractic.service.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.User;
import contractic.model.data.store.UserRole;
import contractic.service.ServiceContextConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceContextConfig.class})
public class UserRoleServiceTest {

	@Autowired
	private DomainService<User, Long, UserDO, MessageDO> userService;
	@Autowired
	private DomainService<UserRole, Long, UserRoleDO, MessageDO> userRoleService;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO> organisationService;
	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	
	@Ignore
	@Test
	public void testGetMapOfStringObject() {
		Assert.fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testGetString() {
		Assert.fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testGetLong() {
		Assert.fail("Not yet implemented");
	}
	
	@Ignore
	@Test
	public void testCreate() throws ContracticServiceException {
		String username = "Kim.Hillman@cloud21.net";
		
		
		UserDO userDO = ServiceTestCommons.getUser(userService, username);
		
		String organisationId = userDO.getOrganisationId();
		
		UserRoleDO userRoleDO = new UserRoleDO();
		userRoleDO.setActive(true);
		userRoleDO.setDescription("Procurement Clerks");
		userRoleDO.setName("Procurement Clerks");
		userRoleDO.setOrganisationId(organisationId);
		
		final String[] moduleNames = {Module.CONTRACT, Module.ORGANISATION, Module.PERSON, Module.SUPPLIER};
		final Map<String, boolean[]> permissionMap = new HashMap<>();
		permissionMap.put(moduleNames[0], new boolean[] {true, true, false});
		permissionMap.put(moduleNames[1], new boolean[] {true, false, false});
		permissionMap.put(moduleNames[2], new boolean[] {true, false, false});
		permissionMap.put(moduleNames[3], new boolean[] {true, true, false});
		
		UserRolePermissionDO[] userPermissions = Arrays.stream(moduleNames).map(moduleName -> {
			UserRolePermissionDO permissionDO = new UserRolePermissionDO();
			try {
				ModuleDO moduleDO = ServiceTestCommons.getModule(moduleService, userDO, moduleName);
				permissionDO.setModule(new ValueDO(moduleDO.getId(), moduleDO.getName()));
				boolean[] permissions = permissionMap.get(moduleName);
				permissionDO.setOrganisationId(organisationId);
				permissionDO.setRead(permissions[0]);
				permissionDO.setWrite(permissions[1]);
				permissionDO.setDelete(permissions[2]);
				return permissionDO;
			} catch (Exception e) {
				return null;
			}
		})
		.filter(permission -> permission != null)
		.collect(Collectors.toList())
		.toArray(new UserRolePermissionDO[0]);
		
		userRoleDO.setPermission(userPermissions);
		
		MessageDO resultMessage = userRoleService.create(userRoleDO);
		Assert.assertTrue(resultMessage != null && resultMessage.getType().equals(MessageDO.OK));
	}

	@Ignore
	@Test
	public void testUpdate() throws ContracticServiceException {
		String username = "Kim.Hillman@cloud21.net";
		
		final UserDO userDO = ServiceTestCommons.getUser(userService, username);
		String organisationId = userDO.getOrganisationId();
		final Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, userDO);
		params.put(UserRoleService.GET_PARAM_ORGANISATIONID, organisationId);
		List<UserRoleDO> roleList = userRoleService.get(params);
		Assert.assertTrue(roleList!=null && roleList.size()!=0);
		
		roleList = roleList.stream()
				.filter(role -> role.getName().equals("Procurement Clerks"))
				.collect(Collectors.toList());
		
		UserRoleDO userRoleDO = roleList.get(0);
		userRoleDO.setUser(userDO);
		final String[] moduleNames = {Module.CONTRACT, Module.ORGANISATION, Module.PERSON, Module.SUPPLIER};
		final Map<String, boolean[]> permissionMap = new HashMap<>();
		permissionMap.put(moduleNames[0], new boolean[] {true, true, false});
		permissionMap.put(moduleNames[1], new boolean[] {true, false, false});
		//permissionMap.put(moduleNames[2], new boolean[] {true, false, false});
		//permissionMap.put(moduleNames[3], new boolean[] {true, true, false});
		
		UserRolePermissionDO[] userPermissions = Arrays.stream(moduleNames).map(moduleName -> {
			UserRolePermissionDO permissionDO = new UserRolePermissionDO();
			try {
				ModuleDO moduleDO = ServiceTestCommons.getModule(moduleService, userDO, moduleName);
				permissionDO.setModule(new ValueDO(moduleDO.getId(), moduleDO.getName()));
				boolean[] permissions = permissionMap.get(moduleName);
				permissionDO.setOrganisationId(organisationId);
				permissionDO.setRead(permissions[0]);
				permissionDO.setWrite(permissions[1]);
				permissionDO.setDelete(permissions[2]);
				permissionDO.setUser(userDO);
				permissionDO.setRole(new ValueDO(userRoleDO.getId(), userRoleDO.getName()));
				return permissionDO;
			} catch (Exception e) {
				return null;
			}
		})
		.filter(permission -> permission != null)
		.collect(Collectors.toList())
		.toArray(new UserRolePermissionDO[0]);
		
		userRoleDO.setPermission(userPermissions);
		
		userRoleDO.setDescription("Departmental Procurement Clerks");
		
		MessageDO resultMessage = userRoleService.update(userRoleDO);
		Assert.assertTrue(resultMessage != null && resultMessage.getType().equals(MessageDO.OK));
	}

	@Ignore
	@Test
	public void testDelete() throws ContracticServiceException {
		String username = "contractic-admin@cloud21.net";
		
		UserDO userDO = ServiceTestCommons.getUser(userService, username);
		
		final Map<String, Object> params = new HashMap<>();
		params.put(CommonService.USER, userDO);
		List<UserRoleDO> roleList = userRoleService.get(params);
		Assert.assertTrue(roleList!=null && roleList.size()!=0);
		
		roleList = roleList.stream()
				.filter(role -> role.getName().equals("Procurement Clerks"))
				.collect(Collectors.toList());
		
		UserRoleDO userRoleDO = roleList.get(0);
		
		MessageDO resultMessage = userRoleService.delete(userRoleDO);
		Assert.assertTrue(resultMessage != null && resultMessage.getType().equals(MessageDO.OK));
	}

}
