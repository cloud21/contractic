package contractic.service.domain;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.User;
import contractic.model.data.store.UserRole;
import contractic.model.data.store.UserRolePermission;
import contractic.service.ServiceContextConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceContextConfig.class})
public class UserRolePermissionServiceTest {

	@Autowired
	private DomainService<Module, Long, ModuleDO, MessageDO> moduleService;
	@Autowired
	private DomainService<User, Long, UserDO, MessageDO> userService;
	@Autowired
	private DomainService<UserRole, Long, UserRoleDO, MessageDO>  userRoleService;
	@Autowired
	private DomainService<Organisation, Long, OrganisationDO, MessageDO>  organisationService;
	@Autowired
	private DomainService<UserRolePermission, Long, UserRolePermissionDO, MessageDO>  userRolePermissionService;
	
	@Test
	public void testGetMapOfStringObject() {
		fail("Not yet implemented");
	}
	
	private ModuleDO getModule(UserDO userDO, String name) throws ContracticServiceException{

		final Map<String, Object> params = new HashMap<>();
		params.put(ModuleService.GET_PARAM_NAME, name);
		List<ModuleDO> list = moduleService.get(params);
		Assert.assertTrue(list!=null && list.size()!=0);
		
		list = list.stream()
				.filter(role -> role.getName().equals(name))
				.collect(Collectors.toList());
		
		return list.get(0);
	}


	
	@Ignore
	@Test
	public void testCreate() throws ContracticServiceException {

		String username = "Kim.Hillman@cloud21.net";
		
		UserDO userDO = ServiceTestCommons.getUser(userService, username);
		
		UserRoleDO userRoleDO = ServiceTestCommons.getUserRole(userRoleService, userDO, "ContractManager");
		UserRolePermissionDO permissionDO = new UserRolePermissionDO();
		//permissionDO.setRole(userRoleDO);
		
		ModuleDO moduleDO = getModule(userDO, Module.CONTRACT);
		permissionDO.setModule(new ValueDO(moduleDO.getId(), moduleDO.getName()));
		
		permissionDO.setOrganisationId(userDO.getOrganisationId());
		permissionDO.setWrite(true);
		permissionDO.setRead(true);
		permissionDO.setDelete(false);
		
		MessageDO resultMessage = userRolePermissionService.create(permissionDO);
		Assert.assertTrue(resultMessage != null && resultMessage.getType().equals(MessageDO.OK));
	}

	//@Ignore
	@Test
	public void testUpdate() throws ContracticServiceException {
		String username = "Kim.Hillman@cloud21.net";
		
		UserDO userDO = ServiceTestCommons.getUser(userService, username);
		ModuleDO moduleDO = this.getModule(userDO, Module.CONTRACT);
		
		final Map<String, Object> params = new HashMap<>();
		params.put(UserRolePermissionService.GET_PARAM_ORGANISATIONID, userDO.getOrganisationId());
		params.put(UserRolePermissionService.GET_PARAM_MODULEID, moduleDO.getId());
		params.put(CommonService.USER, userDO);
		
		List<UserRolePermissionDO> roleList = userRolePermissionService.get(params);
		Assert.assertTrue(roleList!=null && roleList.size()!=0);
		
		UserRolePermissionDO userRolePermissionDO = roleList.get(0);
		
		userRolePermissionDO.setWrite(false);
		userRolePermissionDO.setDelete(false);
		
		MessageDO resultMessage = userRolePermissionService.update(userRolePermissionDO);
		Assert.assertTrue(resultMessage != null && resultMessage.getType().equals(MessageDO.OK));
	}

	@Ignore
	@Test
	public void testDelete() throws ContracticServiceException {
		String username = "contractic-admin@cloud21.net";
		
		UserDO userDO = ServiceTestCommons.getUser(userService, username);
		
		final Map<String, Object> params = new HashMap<>();
		params.put(UserRoleService.GET_PARAM_ORGANISATIONID, userDO.getOrganisationId());
		List<UserRoleDO> roleList = userRoleService.get(params);
		Assert.assertTrue(roleList!=null && roleList.size()!=0);
		
		roleList = roleList.stream()
				.filter(role -> role.getName().equals("ContractManager"))
				.collect(Collectors.toList());
		
		UserRoleDO userRoleDO = roleList.get(0);
		
		MessageDO resultMessage = userRoleService.delete(userRoleDO);
		Assert.assertTrue(resultMessage != null && resultMessage.getType().equals(MessageDO.OK));
	}
}
