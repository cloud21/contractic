package contractic.service.domain;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class TaskAuditTest {

	@Test
	public void testRun() {
		TaskDO source = new TaskDO();
		TaskDO target = new TaskDO();
		source.setStatus(new ValueDO("id", "Complete"));
		target.setStatus(new ValueDO("id", "Assigned"));
		
		AuditJob<TaskDO> auditJob = new TaskAudit();
		List<AuditDO> auditList = auditJob.run(source, target);
		
		Assert.assertTrue(auditList.size() == 1);
	}
	
	@Test
	public void testRun2() {
		TaskDO source = new TaskDO();
		TaskDO target = new TaskDO();
		source.setStatus(new ValueDO("id", "Complete"));
	
		AuditJob<TaskDO> auditJob = new TaskAudit();
		List<AuditDO> auditList = auditJob.run(source, target);
		
		Assert.assertTrue(auditList.size() == 1);
	}

	@Test
	public void testRun3() {
		TaskDO source = new TaskDO();
		TaskDO target = new TaskDO();
		target.setStatus(new ValueDO("id", "Complete"));
	
		AuditJob<TaskDO> auditJob = new TaskAudit();
		List<AuditDO> auditList = auditJob.run(source, target);
		
		Assert.assertTrue(auditList.size() == 1);
	}
	
	@Test
	public void testRun4() {
		TaskDO source = new TaskDO();
		TaskDO target = new TaskDO();
	
		AuditJob<TaskDO> auditJob = new TaskAudit();
		List<AuditDO> auditList = auditJob.run(source, target);
		
		Assert.assertTrue(auditList.size() == 0);
	}
}
