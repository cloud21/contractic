package contractic.service.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import contractic.model.data.dao.OrganisationDAO;
import contractic.model.data.store.Organisation;
import contractic.service.ServiceContextConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceContextConfig.class})
public class OrganisationServiceTest {

	@Autowired
	private OrganisationDAO organisationDAO;
	@Autowired
	private OrganisationService organisationService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {

	}
	
	@Test
	public void testGet() throws ContracticServiceException {
		try {
			final Map<String, Object> params = new HashMap<>();
			final List<OrganisationDO> pojoList = organisationService.get();
			assertNotNull(pojoList);
		} catch (ContracticServiceException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testGetByIdString() throws ContracticServiceException {
		try {
			final Map<String, Object> params = new HashMap<>();
			final List<OrganisationDO> pojoList = organisationService.get();
			OrganisationDO organisationPojo = pojoList.get(0);
			
			Organisation orgPojo = organisationService.get(organisationPojo.getId());
			
			assertEquals(orgPojo, organisationPojo);
		} catch (ContracticServiceException e) {
			e.printStackTrace();
			throw e;
		}
	}


	@Ignore
	@Test
	public void testGetLong() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testGetString() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testGetByIdLong() {
		fail("Not yet implemented");
	}

}
