package contractic.service.domain;

import org.junit.Test;

import com.thoughtworks.xstream.XStream;

public class ContractValueDOTest {

	@Test
	public void testSerialization() {
		ContractValueDO contractValue = new ContractValueDO("18088", null);
		System.out.println(contractValue);
	}
	
	@Test
	public void testDeserialization(){
		String xml = "<value><actual>18088</actual></value>";
		XStream xstream = new XStream();
		xstream.processAnnotations(ContractValueDO.class);
		ContractValueDO contractValue = (ContractValueDO)xstream.fromXML(xml);
		System.out.println(contractValue.getActual());
		System.out.println(contractValue.getProjected());
	}

}
