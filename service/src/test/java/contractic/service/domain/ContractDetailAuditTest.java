package contractic.service.domain;

import java.util.List;

import org.junit.Test;

import contractic.model.data.store.ContractDetail;
import contractic.model.data.store.Group;
import contractic.service.domain.AuditDO;
import contractic.service.domain.AuditJob;
import contractic.service.domain.ContractDetailAudit;
import contractic.service.domain.ContractDetailDO;
import contractic.service.domain.ContractDetailService;
import contractic.service.domain.ContractValueDO;
import contractic.service.domain.ValueDO;

public class ContractDetailAuditTest {

	@Test
	public void testRun() throws ContracticServiceException {
		AuditJob<ContractDetailDO> auditJob = new ContractDetailAudit();
		ContractDetailDO source = new ContractDetailDO();
		ContractValueDO sourceValue = new ContractValueDO();
		sourceValue.setActual("87788");
		sourceValue.setProjected("89998");
		source.setValue(sourceValue);
		source.setGroup(new ValueDO(null, ContractDetail.VALUE_GROUP_FIELD));
		ContractDetail sourceDetail = new ContractDetail();
		Group valueGroup = new Group();
		valueGroup.setName(ContractDetail.VALUE_GROUP_FIELD);
		sourceDetail.setGroup(valueGroup);
		ContractDetailService.populateValue(source, sourceDetail);
		
		ContractDetailDO target = new ContractDetailDO();
		ContractValueDO targetValue = new ContractValueDO();
		targetValue.setActual("87788");
		targetValue.setProjected("8999899");
		target.setValue(targetValue);
		target.setGroup(new ValueDO(null, ContractDetail.VALUE_GROUP_FIELD));
		ContractDetail targetDetail = new ContractDetail();
		targetDetail.setGroup(valueGroup);
		ContractDetailService.populateValue(target, targetDetail);
		
		List<AuditDO> auditList = auditJob.run(source, target);
		
	}

}
