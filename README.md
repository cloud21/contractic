# README #

### What is this repository for? ###

This is an Open Source contract management product called "Contractic" licensed as [GPL 3.0](http://www.gnu.org/licenses/gpl-3.0.en.html)
The website can be found here: http://www.contractic.net/
The technical wiki can be found here: https://bitbucket.org/cloud21/contractic/wiki/Home

![Slide1.JPG](https://bitbucket.org/repo/LL4yRx/images/3262763491-Slide1.JPG)

![Slide2.JPG](https://bitbucket.org/repo/LL4yRx/images/1292502899-Slide2.JPG)

### What is Contractic ###
Contractic is a fully integrated platform designed to provide organisations of all sizes and complexities with a smart and flexible solution which will improve the effectiveness and efficiency of the complete spectrum of supply chain processes.

It comprises three modules which may be operated independently or as a fully integrated system.

•	Pre-Procurement
•	Procurement
•	Contract Management

![contractic.png](https://bitbucket.org/repo/LL4yRx/images/1722106530-contractic.png)

### What is Contract Management ###
Virtually every enterprise, whether public, private or charitable enters into contracts to either supply or receive goods and services. These contracts may be as simple as a page or two of terms and conditions and as complex as hundreds of pages of legal language with attached appendices and schedules. Contracts may be for a single transaction that is completed on the spot, or involve the delivery of a series of products and services over a period of many years.

While the single transaction contract requires little in the way of ongoing management, it does not take a great step up in complexity for there to be a task or series of tasks required to ensure that the delivery of the goods and services are in compliance with the agreed specifications, terms and conditions and monitoring of the deliverables in the form of key performance indicators. 
Managing service delivery, relationships with suppliers and administering the contract is the role of contract management. This may only require infrequent and uncomplicated work, but often it is a series of ongoing tasks requiring close coordination in order to deliver all of the operational and financial benefits to the buyer. 

Effective contract management ensures that the key parameters of an agreed transaction are recorded and then managed to ensure consistent delivery. It should also be a process that is flexible enough to enable changes in business and operating conditions to be incorporated into the contractual arrangements.

In many cases contract management is the next stage in the process after a procurement project has been undertaken, and might be considered a down-stream extension of the process, since the procurement will have resulted in the formation of a contract. Often, the way in which services are delivered will be recorded, along with similar outputs from other contacts, to provide a body of information that can be used in the pre-procurement phase to inform the lead-up to and execution of a new procurement. It may thus be seen as a cycle.
![cycle.png](https://bitbucket.org/repo/LL4yRx/images/2529801161-cycle.png)

For the entire supply chain process to work with optimal effectiveness and efficiency, each stage in the cycle should be able to inform the other stages, but all too often they are separate, manual processes, with low levels of automation or interconnection. Where there are more automated solutions, these tend to be limited to one aspect of the cycle and can lack the flexibility to cope with the myriad of supply chain combinations and permutations.

### The value proposition ###

**Intelligent, integrated, effective supply chain management software.**

Cloud21 Contractic is an innovative solution to automating and integrating management of the supply chain. Configurable and flexible, it enables collaboration and thoroughness in all facets of the process.

* Be better informed about your suppliers – delivering a systematic approach to gathering and analysing the market. Enabling better decisions to be made about procurement.
* Write better contracts – by opening your procurements to considered and consistent processes.
* Deliver real benefits – though the effective and efficient management and control of your contracts.
* Ensure that real value for money is achieved
* Engage with suppliers throughout the contract life with joint access to the system.

### What is the business case? ###

* Well controlled contract management will deliver measurable operational and financial benefits and avoid unnecessary losses from poor management
* Don’t waste time trying to find paper based copies – have ‘one truth’ located safely and securely and always available online
* Integration of procurement operations with contract management will deliver more effective contractual arrangements, enabling better control, and reinforcing the operational and financial benefits




### How do I get set up? ###

* Instructions are still under construction: contact us if you are interested.

### Contribution guidelines ###

* Contact contractic@ctotech.net

### Who do I talk to? ###

* Contact contractic@ctotech.net