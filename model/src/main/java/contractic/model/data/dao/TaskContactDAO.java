package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.TaskContact;
import contractic.model.data.util.HibernateUtil;

public class TaskContactDAO implements CrudOperation<TaskContact> {

	@Override
	public TaskContact create(TaskContact contact) throws ContracticModelException {
		if (contact == null)
			throw new IllegalArgumentException("Null person supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(contact);
    		tx.commit();
    		return contact;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving contract %s", contact), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<TaskContact> read(final TaskContact task) throws ContracticModelException {
		if (task == null)
			throw new IllegalArgumentException("Null task supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(TaskContact.class);
		
		// If Id is provided, we don't need to check other properties
		if (task.getId() != null){
			criteria.add(Restrictions.eq("Id", task.getId()));
		}else{
			if (task.getContact() != null)
				criteria.add(Restrictions.eq("Contact", task.getContact()));
			
			if (task.getTask() != null)
				criteria.add(Restrictions.eq("Task", task.getTask()));
		}
		
		return criteria.list();
	}

	@Override
	public void update(TaskContact contact) throws ContracticModelException {
		if (contact == null)
			throw new IllegalArgumentException("Null contract supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(contact);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving contact %s", contact),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(TaskContact contact) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			// Merge first to get the most recent view of the object
			Object p = session.merge(contact);
			session.delete(p);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete contact %s", contact),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
