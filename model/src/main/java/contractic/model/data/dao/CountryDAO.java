package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Country;
import contractic.model.data.util.HibernateUtil;

public class CountryDAO implements CrudOperation<Country>{

	@Override
	public Country create(Country entity) throws ContracticModelException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Country> read(Country entity) throws ContracticModelException {
		if (entity == null)
			throw new IllegalArgumentException("Null country supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Country.class);
		
		String name = entity.getName();
		String currencyCode = entity.getCurrencyCode();
		String currencyName = entity.getCurrencyName();
		Boolean active = entity.getActive();
		
		if(active!=null && active)
			criteria.add(Restrictions.eq("Active", entity.getActive()));
		if (name != null)
			criteria.add(Restrictions.ilike("Name", entity.getName(), MatchMode.ANYWHERE));
		if(currencyCode != null)
			criteria.add(Restrictions.eq("CurrencyCode", entity.getCurrencyCode()));
		if (currencyName!=null)
			criteria.add(Restrictions.ilike("CurrencyName", entity.getCurrencyName(), MatchMode.ANYWHERE));
		
		criteria.setCacheable(true);
		
		return criteria.list();
	}

	@Override
	public void update(Country entity) throws ContracticModelException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Country entity) throws ContracticModelException {
		// TODO Auto-generated method stub
		
	}

}
