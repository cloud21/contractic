package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.AddressType;
import contractic.model.data.store.Status;
import contractic.model.data.util.HibernateUtil;

public class StatusDAO implements CrudOperation<Status> {

	private static ResourceBundle errorResourceBundle = CrudOperation.getErrorBundle();
	
	@Override
	public Status create(Status status) throws ContracticModelException {
		if (status == null)
			throw new IllegalArgumentException("Null status supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(status);
    		tx.commit();
    		return status;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving status %s", status), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<Status> read(Status status) throws ContracticModelException {
		if (status == null)
			throw new IllegalArgumentException("Null status supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Status.class);
		
		if (status.getId() != null)
			criteria.add(Restrictions.eq("Id", status.getId()));
		
		if(status.getName()!=null)
			criteria.add(Restrictions.eq("Name", status.getName()));
		
		if(status.getOrganisation()!=null)
			criteria.add(Restrictions.eq("Organisation", status.getOrganisation()));
		
		if (status.getModule() != null)
			criteria.add(Restrictions.eq("Module", status.getModule()));
		
		
		return criteria.list();
	}

	@Override
	public void update(Status status) throws ContracticModelException {
		if (status == null)
			throw new IllegalArgumentException("Null status supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(status);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving status %s", status),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Status status) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(status);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete status %s", status),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
