package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Person;
import contractic.model.data.store.UserRole;
import contractic.model.data.util.HibernateUtil;

public class UserRoleDAO implements CrudOperation<UserRole> {

	@Override
	public UserRole create(UserRole user) throws ContracticModelException {
		if (user == null)
			throw new IllegalArgumentException("Null user supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(user);
    		tx.commit();
    		return user;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving organisation %s", user), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<UserRole> read(UserRole user) throws ContracticModelException {
		if (user == null)
			throw new IllegalArgumentException("Null user supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(UserRole.class);
		
		Long id = user.getId();
		if (id != null)
			criteria.add(Restrictions.eq("Id", user.getId()));
		
		if (user.getOrganisation() != null && user.getOrganisation().getId()!=null)
			criteria.add(Restrictions.eq("Organisation", user.getOrganisation()));
		
		return criteria.list();
	}

	@Override
	public void update(UserRole user) throws ContracticModelException {
		if (user == null)
			throw new IllegalArgumentException("Null user supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(user);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving use roler %s", user),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(UserRole user) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(user);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when deleting user %s", user),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
