package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.ContractReferenceFormat;
import contractic.model.data.store.ContractType;
import contractic.model.data.store.Organisation;
import contractic.model.data.util.ExceptionUtils;
import contractic.model.data.util.HibernateUtil;

public class ContractReferenceFormatDAO implements CrudOperation<ContractReferenceFormat> {

	private static ResourceBundle errorResourceBundle = CrudOperation.getErrorBundle();
	
	@Override
	public ContractReferenceFormat create(ContractReferenceFormat contractReferenceFormat) throws ContracticModelException {
		if (contractReferenceFormat == null)
			throw new IllegalArgumentException("Null contract reference supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(contractReferenceFormat);
    		tx.commit();
    		return contractReferenceFormat;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}
    		
    		String message = String.format("Error occured when saving contract reference format %s", contractReferenceFormat.getName());
    		if(ExceptionUtils.voilatesContraint("UQ_ContractReferenceFormat_Format_ContractTypeId_OrganisationId", ex)){
    			String code = "3001";
    			String format = errorResourceBundle.getString(code);
    			//message = String.format("Contract type '%s' already exists", contractType.getName());
    			message = String.format(format, contractReferenceFormat.getName());
    		}
    		
    		throw new ContracticModelException(message, ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<ContractReferenceFormat> read(ContractReferenceFormat contractReferenceFormat) throws ContracticModelException {
		if (contractReferenceFormat == null)
			throw new IllegalArgumentException("Null contract reference supplied");
		final Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(ContractReferenceFormat.class);

		/*If the id is supplied, then the calling application knows exactly what record they want*/
		if (contractReferenceFormat.getId() != null)
			criteria.add(Restrictions.eq("Id", contractReferenceFormat.getId()));
		else{
			
			final Organisation organisation = contractReferenceFormat.getOrganisation();
			final ContractType contractType = contractReferenceFormat.getContractType();
			final String description = contractReferenceFormat.getDescription();
			final String name = contractReferenceFormat.getName();
			final Boolean active = contractReferenceFormat.getActive();
			final Boolean _default = contractReferenceFormat.getDefault();
			
			/*if not id is supplied, then we must make sure the calling application supplies an organisation*/
			if(organisation == null || organisation.getId() == null)
				throw new IllegalArgumentException("A valid Organisation must be supplied");
			
			if(name!=null)
				criteria.add(Restrictions.ilike("Name", name, MatchMode.ANYWHERE));
			
			if(description!=null)
				criteria.add(Restrictions.ilike("Description", description, MatchMode.ANYWHERE));
			
			if (contractType != null)
				criteria.add(Restrictions.eq("ContractType", contractType));
			
			if (active != null)
				criteria.add(Restrictions.eq("Active", active));
			
			if (_default != null)
				criteria.add(Restrictions.eq("Default", _default));
			
			criteria.add(Restrictions.eq("Organisation", organisation));
		}
		
		return criteria.list();
	}

	@Override
	public void update(ContractReferenceFormat contractReferenceFormat) throws ContracticModelException {
		if (contractReferenceFormat == null)
			throw new IllegalArgumentException("Null contract format supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(contractReferenceFormat);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
    		String message = String.format("Error occured when saving contract reference format %s", contractReferenceFormat.getName());
    		if(ExceptionUtils.voilatesContraint("UQ_ContractReferenceFormat_Format_ContractTypeId_OrganisationId", ex)){
    			String code = "3001";
    			String format = errorResourceBundle.getString(code);
    			message = String.format(format, contractReferenceFormat.getName());
    		}
    		
    		throw new ContracticModelException(message, ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(ContractReferenceFormat contractType) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(contractType);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete contract reference %s", contractType.getName()),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
