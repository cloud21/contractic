package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Person;
import contractic.model.data.store.Supplier;
import contractic.model.data.util.HibernateUtil;

public class SupplierDAO implements CrudOperation<Supplier> {

	private static ResourceBundle errorResourceBundle = CrudOperation.getErrorBundle();
	
	@Override
	public Supplier create(Supplier supplier) throws ContracticModelException {
		if (supplier == null)
			throw new IllegalArgumentException("Null person supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(supplier);
    		tx.commit();
    		return supplier;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving supplier %s", supplier), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<Supplier> read(final Supplier supplier) throws ContracticModelException {
		if (supplier == null)
			throw new IllegalArgumentException("Null person supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Supplier.class);
		
		Long id = supplier.getId();
		if (id != null)
			criteria.add(Restrictions.eq("Id", supplier.getId()));
		
		if (supplier.getOrganisation() != null && supplier.getOrganisation().getId()!=null)
			criteria.add(Restrictions.eq("Organisation", supplier.getOrganisation()));

		if(supplier.getName()!=null)
			criteria.add(Restrictions.ilike("Name", supplier.getName(), MatchMode.ANYWHERE));
		
		List<Supplier> supplierList = criteria.list();
		
		return supplierList;
		
	}

	@Override
	public void update(Supplier supplier) throws ContracticModelException {
		if (supplier == null)
			throw new IllegalArgumentException("Null person supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(supplier);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving person %s", supplier),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Supplier supplier) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			// Merge first to get the most recent view of the object
			Object p = session.merge(supplier);
			session.delete(p);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when deleting supplier %s", supplier),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
