package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.AddressType;
import contractic.model.data.store.DocumentType;
import contractic.model.data.util.ExceptionUtils;
import contractic.model.data.util.HibernateUtil;

public class DocumentTypeDAO implements CrudOperation<DocumentType> {

	private static ResourceBundle errorResourceBundle = CrudOperation.getErrorBundle();
	
	@Override
	public DocumentType create(DocumentType documentType) throws ContracticModelException {
		if (documentType == null)
			throw new IllegalArgumentException("Null document type supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(documentType);
    		tx.commit();
    		return documentType;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		String message = String.format("Error occured when saving document type %s", documentType.getName());
    		if(ExceptionUtils.voilatesContraint("UQ_DocumentType_Name_OrganisationId", ex)){
    			String code = "5000";
    			String format = errorResourceBundle.getString(code);
    			//message = String.format("Contract type '%s' already exists", contractType.getName());
    			message = String.format(format, documentType.getName());
    		}
    		throw new ContracticModelException(message, ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<DocumentType> read(DocumentType documentType) throws ContracticModelException {
		if (documentType == null)
			throw new IllegalArgumentException("Null document type supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(DocumentType.class);

		if (documentType.getId() != null)
			criteria.add(Restrictions.eq("Id", documentType.getId()));
		
		if(documentType.getName()!=null)
			criteria.add(Restrictions.eq("Code", documentType.getName()));
		
		if(documentType.getOrganisation()!=null)
			criteria.add(Restrictions.eq("Organisation", documentType.getOrganisation()));
		
		return criteria.list();
	}

	@Override
	public void update(DocumentType documentType) throws ContracticModelException {
		if (documentType == null)
			throw new IllegalArgumentException("Null address supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(documentType);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
    		String message = String.format("Error occured when saving document type %s", documentType.getName());
    		if(ExceptionUtils.voilatesContraint("UQ_DocumentType_Name_OrganisationId", ex)){
    			String code = "5000";
    			String format = errorResourceBundle.getString(code);
    			//message = String.format("Contract type '%s' already exists", contractType.getName());
    			message = String.format(format, documentType.getName());
    		}
    		throw new ContracticModelException(message, ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(DocumentType documentType) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(documentType);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
    		String message = String.format("Error occured when deleting document type '%s'", documentType.getName());
    		if(ExceptionUtils.voilatesContraint("FK_Document_DocumentType_Id", ex)){
    			String code = "5001";
    			String format = errorResourceBundle.getString(code);
    			//message = String.format("Contract type '%s' already exists", contractType.getName());
    			message = String.format(format, documentType.getName());
    		}
    		throw new ContracticModelException(message, ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
