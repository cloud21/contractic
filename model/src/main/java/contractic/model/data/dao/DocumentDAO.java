package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Document;
import contractic.model.data.store.Person;
import contractic.model.data.util.HibernateUtil;

public class DocumentDAO implements CrudOperation<Document> {

	@Override
	public Document create(Document document) throws ContracticModelException {
		if (document == null)
			throw new IllegalArgumentException("Null document supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(document);
    		tx.commit();
    		return document;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving document %s", document), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<Document> read(final Document document) throws ContracticModelException {
		if (document == null)
			throw new IllegalArgumentException("Null document supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Document.class);
		
		if (document.getId() != null)
			criteria.add(Restrictions.eq("Id", document.getId()));
		
		if (document.getDocumentType() != null)
			criteria.add(Restrictions.eq("DocumentType", document.getDocumentType()));

		if (document.getStatus() != null)
			criteria.add(Restrictions.eq("Status", document.getStatus()));
		
		if (document.getModule() != null && document.getModule().getId()!=null){
			criteria.add(Restrictions.eq("Module", document.getModule()));
			if (document.getExternalId() != null)
				criteria.add(Restrictions.eq("ExternalId", document.getExternalId()));
		}
		
		if (document.getOrganisation() != null && document.getOrganisation().getId()!=null)
			criteria.add(Restrictions.eq("Organisation", document.getOrganisation()));
		
		List<Document> documentList = criteria.list();
		
		return documentList;
		
	}

	@Override
	public void update(Document document) throws ContracticModelException {
		if (document == null)
			throw new IllegalArgumentException("Null person supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(document);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving document %s", document),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Document document) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			// Merge first to get the most recent view of the object
			Object p = session.merge(document);
			session.delete(p);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete document %s", document),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
