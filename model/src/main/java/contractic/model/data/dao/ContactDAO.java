package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Address;
import contractic.model.data.store.Contact;
import contractic.model.data.store.Module;
import contractic.model.data.store.Person;
import contractic.model.data.store.UserRole;
import contractic.model.data.store.UserRolePermission;
import contractic.model.data.util.HibernateUtil;

public class ContactDAO implements CrudOperation<Contact> {

	@Override
	public Contact create(Contact contact) throws ContracticModelException {
		if (contact == null)
			throw new IllegalArgumentException("Null contact supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(contact);
    		tx.commit();
    		return contact;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving contact %s", contact), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<Contact> read(Contact contact) throws ContracticModelException {
		if (contact == null)
			throw new IllegalArgumentException("Null contact supplied");
		Session session = HibernateUtil.currentSession();
		
		Long id = contact.getId();
		Module module = contact.getModule();
		Address address = contact.getAddress();
		
		// If an id is supplied we know we will always get the single id record
		if(id == null){
			if(address == null || 
					address.getOrganisation() == null || 
							address.getOrganisation().getId() == null)
				throw new ContracticModelException("Contract address and/or organisation must be supplied");
		}
		
		final Criteria criteria = session.createCriteria(Contact.class, "contact");
		


		if (id != null)
			criteria.add(Restrictions.eq("Id", contact.getId()));
		else{
			
			if (address != null && address.getOrganisation() != null && address.getOrganisation().getId()!=null){
				criteria.createAlias("contact.Address", "address");
				criteria.add(Restrictions.eq("address.Organisation", address.getOrganisation()));
			}
			
			if(module!=null)
				criteria.add(Restrictions.eq("contact.Module", module));
			
			if (contact.getExternalId() != null)
				criteria.add(Restrictions.eq("ExternalId", contact.getExternalId()));
		}
		
		
		return criteria.list();
		
	}

	@Override
	public void update(Contact entity) throws ContracticModelException {
		if (entity == null)
			throw new IllegalArgumentException("Null contract supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(entity);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving contact %s", entity),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Contact entity) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			// Merge first to get the most recent view of the object
			Object p = session.merge(entity);
			session.delete(p);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete contact %s", entity),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
