package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import contractic.model.ContracticModelException;

/**
 * Contract for any CRUD operation
 * @author Michael Sekamanya
 *
 * @param <E>
 */
public interface CrudOperation <E> {
	
	public static ResourceBundle getErrorBundle(){
		String name = "ModelErrors";
		return ResourceBundle.getBundle(name);
	}
	
	/**
	 * Create an entity
	 * @param entity Entity to create
	 * @return the newly created Entity. This may be same as the one passed in or a completely new one with id fields populated
	 * @throws ContracticModelException If anything unusual occurs
	 */
	E create(E entity) throws ContracticModelException;
	
	/**
	 * Reads to obtain a collection of entities
	 * @param entity The entity whose properties are used to query the datasource
	 * @return A list of read entities
	 * @throws ContracticModelException If anything unusual occurs
	 */
	List<E> read(E entity) throws ContracticModelException;
	
	/**
	 * Update the datasource
	 * @param entity Entity being updated in the datasource
	 * @throws ContracticModelException
	 */
	void update(E entity) throws ContracticModelException;
	
	/**
	 * Delete from the datasource
	 * @param entity
	 * @throws ContracticModelException
	 */
	void delete(E entity) throws ContracticModelException;
}
