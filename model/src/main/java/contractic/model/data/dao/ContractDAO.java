package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Contract;
import contractic.model.data.util.ExceptionUtils;
import contractic.model.data.util.HibernateUtil;

public class ContractDAO implements CrudOperation<Contract> {

	private static ResourceBundle errorResourceBundle = CrudOperation.getErrorBundle();
	
	@Override
	public Contract create(Contract contract) throws ContracticModelException {
		if (contract == null)
			throw new IllegalArgumentException("Null person supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(contract);
    		tx.commit();
    		return contract;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}
    		
    		String message = String.format("Error occured when saving contract '%s'", contract);
    		if(ExceptionUtils.voilatesContraint("UQ_Contract_Reference_OrganisationId", ex)){
    			String code = "2003";
    			String format = errorResourceBundle.getString(code);
    			//message = String.format("Contract type '%s' already exists", contractType.getName());
    			message = String.format(format, contract.getReference());
    		}
    		throw new ContracticModelException(message, ex);
    		
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<Contract> read(final Contract contract) throws ContracticModelException {
		if (contract == null)
			throw new IllegalArgumentException("Null person supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Contract.class);
		
		final Long id = contract.getId();
		if (id != null)
			criteria.add(Restrictions.eq("Id", contract.getId()));
		
		if (contract.getOrganisation() != null && contract.getOrganisation().getId()!=null)
			criteria.add(Restrictions.eq("Organisation", contract.getOrganisation()));
		
		return criteria.list();
	}

	@Override
	public void update(Contract contract) throws ContracticModelException {
		if (contract == null)
			throw new IllegalArgumentException("Null contract supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(contract);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
    		String message = String.format("Error occured when saving contract '%s'", contract);
    		if(ExceptionUtils.voilatesContraint("UQ_Contract_Reference_OrganisationId", ex)){
    			String code = "2003";
    			String format = errorResourceBundle.getString(code);
    			//message = String.format("Contract type '%s' already exists", contractType.getName());
    			message = String.format(format, contract.getReference());
    		}
    		throw new ContracticModelException(message, ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Contract contract) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			// Merge first to get the most recent view of the object
			Object p = session.merge(contract);
			session.delete(p);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete contract '%s'", contract),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
