package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Task;
import contractic.model.data.util.HibernateUtil;

public class TaskDAO implements CrudOperation<Task> {

	@Override
	public Task create(Task contract) throws ContracticModelException {
		if (contract == null)
			throw new IllegalArgumentException("Null person supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(contract);
    		tx.commit();
    		return contract;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving contract %s", contract), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<Task> read(final Task task) throws ContracticModelException {
		if (task == null)
			throw new IllegalArgumentException("Null task supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Task.class);
		
		// If Id is provided, we don't need to check other properties
		if (task.getId() != null){
			criteria.add(Restrictions.eq("Id", task.getId()));
		}else{
			
			if(task.getOrganisation()==null)
				throw new ContracticModelException("Must supply an organsiation");
			
			criteria.add(Restrictions.eq("Organisation", task.getOrganisation()));
		
			/*The task number will always be unique atleast within a specific organisation*/
			if (task.getNumber() != null){
				criteria.add(Restrictions.eq("Number", task.getNumber()));
			}else{
				if(task.getDescription()!=null)
					criteria.add(Restrictions.ilike("Description", task.getDescription(), MatchMode.ANYWHERE));
	
				if(task.getTitle()!=null)
					criteria.add(Restrictions.ilike("Title", task.getTitle(), MatchMode.ANYWHERE));
				
				if(task.getParent()==null){
					criteria.add(Restrictions.isNull("Parent"));
				}else{
					criteria.add(Restrictions.eq("Parent", task.getParent()));
				}
				
				if (task.getModule() != null && task.getModule().getId()!=null)
					criteria.add(Restrictions.eq("Module", task.getModule()));
				
				if (task.getExternalId() != null && (task.getModule() == null || task.getModule().getId()==null))
					throw new ContracticModelException("A task is fully defined by the module and external Id. One of these hasn't been supplied");
				
				if (task.getExternalId() != null)
					criteria.add(Restrictions.eq("ExternalId", task.getExternalId()));
			}
		}
		
		criteria.addOrder(Order.asc("Id"));
		return criteria.list();
	}

	@Override
	public void update(Task contract) throws ContracticModelException {
		if (contract == null)
			throw new IllegalArgumentException("Null contract supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(contract);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving contract %s", contract),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Task contract) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			// Merge first to get the most recent view of the object
			Object p = session.merge(contract);
			session.delete(p);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete contract %s", contract),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
