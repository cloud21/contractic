package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Address;
import contractic.model.data.store.AddressType;
import contractic.model.data.util.HibernateUtil;

public class AddressTypeDAO implements CrudOperation<AddressType> {

	@Override
	public AddressType create(AddressType addressType) throws ContracticModelException {
		if (addressType == null)
			throw new IllegalArgumentException("Null addess type supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(addressType);
    		tx.commit();
    		return addressType;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving address type %s", addressType), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<AddressType> read(AddressType address) throws ContracticModelException {
		if (address == null)
			throw new IllegalArgumentException("Null address type supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(AddressType.class);
		Long id = address.getId();
		if (id != null)
			criteria.add(Restrictions.eq("Id", address.getId()));
		
		if(address.getName()!=null)
			criteria.add(Restrictions.eq("Name", address.getName()));
				
		return criteria.list();
	}

	@Override
	public void update(AddressType address) throws ContracticModelException {
		if (address == null)
			throw new IllegalArgumentException("Null address supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(address);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving address %s", address),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(AddressType address) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(address);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete address %s", address),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
