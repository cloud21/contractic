package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.ContractDetail;
import contractic.model.data.util.ExceptionUtils;
import contractic.model.data.util.HibernateUtil;

public class ContractDetailDAO implements CrudOperation<ContractDetail> {

	private static ResourceBundle errorResourceBundle = CrudOperation.getErrorBundle();
	
	@Override
	public ContractDetail create(ContractDetail contractDetail) throws ContracticModelException {
		if (contractDetail == null)
			throw new IllegalArgumentException("Null contract detail supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(contractDetail);
    		tx.commit();
    		return contractDetail;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}
    		    		
    		String message = "Error occured when saving contract detail";
    		if(ExceptionUtils.voilatesContraint("UQ_ContractDetail_Name_Description_ContractId_GroupId", ex)){
    			String code = "2004";
    			message = errorResourceBundle.getString(code);
    		}
    		throw new ContracticModelException(message, ex);
    		
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<ContractDetail> read(final ContractDetail contractDetail) throws ContracticModelException {
		if (contractDetail == null)
			throw new IllegalArgumentException("Null contract detail supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(ContractDetail.class);
		
		final Long id = contractDetail.getId();
		if (id != null)
			criteria.add(Restrictions.eq("Id", contractDetail.getId()));
		
		if(contractDetail.getContract()!=null)
			criteria.add(Restrictions.eq("Contract", contractDetail.getContract()));
		
		if(contractDetail.getGroup()!=null)
			criteria.add(Restrictions.eq("Group", contractDetail.getGroup()));
		
		return criteria.list();
	}

	@Override
	public void update(ContractDetail contractDetail) throws ContracticModelException {
		if (contractDetail == null)
			throw new IllegalArgumentException("Null contract detail supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(contractDetail);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
    		String message = "Error occured when saving contract detail";
    		if(ExceptionUtils.voilatesContraint("UQ_ContractDetail_Name_Description_ContractId_GroupId", ex)){
    			String code = "2004";
    			message = errorResourceBundle.getString(code);
    		}
    		throw new ContracticModelException(message, ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(ContractDetail contractDetail) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			// Merge first to get the most recent view of the object
			Object p = session.merge(contractDetail);
			session.delete(p);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException("Error occured when delete contract detail",
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
