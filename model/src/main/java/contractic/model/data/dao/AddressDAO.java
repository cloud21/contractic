package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Address;
import contractic.model.data.util.HibernateUtil;

public class AddressDAO implements CrudOperation<Address> {

	
	@Override
	public Address create(Address address) throws ContracticModelException {
		if (address == null)
			throw new IllegalArgumentException("Null person supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(address);
    		tx.commit();
    		return address;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving address %s", address), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<Address> read(Address address) throws ContracticModelException {
		if (address == null)
			throw new IllegalArgumentException("Null address supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Address.class);
		Long id = address.getId();
		if (id != null)
			criteria.add(Restrictions.eq("Id", address.getId()));
		
		if (address.getOrganisation() != null && address.getOrganisation().getId()!=null)
			criteria.add(Restrictions.eq("Organisation", address.getOrganisation()));

		if (address.getType() != null && address.getType().getId()!=null)
			criteria.add(Restrictions.eq("Type", address.getType()));
		
		if(address.getSection1()!=null)
			criteria.add(Restrictions.ilike("Section1", address.getSection1(), MatchMode.ANYWHERE));
		
		return criteria.list();
	}

	@Override
	public void update(Address address) throws ContracticModelException {
		if (address == null)
			throw new IllegalArgumentException("Null address supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(address);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving address %s", address),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Address address) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(address);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete address %s", address),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
