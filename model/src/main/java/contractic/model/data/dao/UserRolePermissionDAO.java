package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Module;
import contractic.model.data.store.UserRole;
import contractic.model.data.store.UserRolePermission;
import contractic.model.data.util.HibernateUtil;

public class UserRolePermissionDAO implements CrudOperation<UserRolePermission> {

	@Override
	public UserRolePermission create(UserRolePermission permission) throws ContracticModelException {
		if (permission == null)
			throw new IllegalArgumentException("Null permission supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(permission);
    		tx.commit();
    		return permission;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving permission %s", permission), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<UserRolePermission> read(UserRolePermission permission) throws ContracticModelException {
		if (permission == null)
			throw new IllegalArgumentException("Null role supplied");
		Session session = HibernateUtil.currentSession();

		Long id = permission.getId();
		UserRole role = permission.getUserRole();
		Module module = permission.getModule();
		
		// If an id is supplied we know we will always get the single id record
		if(id == null){
			if(role == null || 
					role.getOrganisation() == null || 
							role.getOrganisation().getId() == null)
				throw new ContracticModelException("User role and/or organisation must be supplied");
		}
		
		final Criteria criteria = session.createCriteria(UserRolePermission.class, "permission");
		


		if (id != null)
			criteria.add(Restrictions.eq("Id", permission.getId()));
		
		if(role!=null && role.getId()!=null){
			criteria.add(Restrictions.eq("permission.UserRole", role));
		}
		
		if (role != null && role.getOrganisation() != null && role.getOrganisation().getId()!=null){
			criteria.createAlias("permission.UserRole", "role");
			criteria.add(Restrictions.eq("role.Organisation", role.getOrganisation()));
		}
		
		if(module!=null)
			criteria.add(Restrictions.eq("permission.Module", module));
		
		return criteria.list();
	}

	@Override
	public void update(UserRolePermission user) throws ContracticModelException {
		if (user == null)
			throw new IllegalArgumentException("Null user supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(user);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving use roler %s", user),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(UserRolePermission user) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(user);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when deleting user %s", user),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
