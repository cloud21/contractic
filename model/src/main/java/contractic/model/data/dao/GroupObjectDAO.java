package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Group;
import contractic.model.data.store.GroupObject;
import contractic.model.data.store.GroupSet;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.User;
import contractic.model.data.util.HibernateUtil;

public class GroupObjectDAO implements CrudOperation<GroupObject> {

	private static ResourceBundle errorResourceBundle = CrudOperation.getErrorBundle();
	
	@Override
	public GroupObject create(GroupObject groupObject) throws ContracticModelException {
		if (groupObject == null)
			throw new IllegalArgumentException("Null group object supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(groupObject);
    		tx.commit();
    		return groupObject;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving group object %s", groupObject), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<GroupObject> read(GroupObject groupObject) throws ContracticModelException {
		if (groupObject == null)
			throw new IllegalArgumentException("Null group supplied");
		Session session = HibernateUtil.currentSession();

		final GroupSet groupSet = groupObject.getGroupSet();
		if(groupSet == null)
			throw new IllegalArgumentException("Invalid group. The group object must have a group with a valid group set");
		
		final Organisation organisation = groupSet.getOrganisation();
		if(organisation == null)
			throw new IllegalArgumentException("Invalid group. The group object must have a group with a valid group set with a valid organisation");
		
		final Criteria criteria = session.createCriteria(GroupObject.class);
		
		criteria.add(Restrictions.eq("GroupSet", groupSet));
		
		if (groupObject.getId() != null)
			criteria.add(Restrictions.eq("Id", groupObject.getId()));
		
		if (groupObject.getModule() != null)
			criteria.add(Restrictions.eq("Module", groupObject.getModule()));
		
		if (groupObject.getGroup() != null)
			criteria.add(Restrictions.eq("Group", groupObject.getGroup()));
		
		Long externalId = groupObject.getExternalId();
		if(externalId != null)
			criteria.add(Restrictions.eq("ExternalId", externalId));
		
		return criteria.list();
	}

	@Override
	public void update(GroupObject groupObject) throws ContracticModelException {
		if (groupObject == null)
			throw new IllegalArgumentException("Null group object supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(groupObject);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving group object %s", groupObject),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(GroupObject group) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(group);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete group %s", group),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
