package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Group;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Report;
import contractic.model.data.store.ReportOrganisation;
import contractic.model.data.util.HibernateUtil;

public class ReportOrganisationDAO implements CrudOperation<ReportOrganisation> {

	@Override
	public ReportOrganisation create(ReportOrganisation reportOrganisation) throws ContracticModelException {
		if (reportOrganisation == null)
			throw new IllegalArgumentException("Null user supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(reportOrganisation);
    		tx.commit();
    		return reportOrganisation;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving repot %s", reportOrganisation), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<ReportOrganisation> read(ReportOrganisation reportOrganisation) throws ContracticModelException {
		if (reportOrganisation == null)
			throw new IllegalArgumentException("Null user supplied");
		Session session = HibernateUtil.currentSession();

		Long id = reportOrganisation.getId();
		Boolean active = reportOrganisation.getActive();
		Group group = reportOrganisation.getGroup();
		Organisation organisation = reportOrganisation.getOrganisation();
		Report report = reportOrganisation.getReport();
		Person person = reportOrganisation.getOwner();
		
		final Criteria criteria = session.createCriteria(ReportOrganisation.class, "ro");
		
		if(id!=null)
			criteria.add(Restrictions.eq("ro.Id", id));
		else{
			if(organisation == null || organisation.getId()==null)
				throw new IllegalArgumentException("Organisation must be supplied");
			
			criteria.add(Restrictions.eq("ro.Organisation", organisation));
			
			if(report!=null && report.getExternalRef()!=null){
				criteria.createAlias("ro.Report", "report");
				criteria.add(Restrictions.eq("report.ExternalRef", report.getExternalRef()));
			}
			
			if(active != null)
				criteria.add(Restrictions.eq("ro.Active", active));
			
			if(group != null)
				criteria.add(Restrictions.eq("ro.Group", group));
			
			if(person!=null)
				criteria.add(Restrictions.eq("ro.Owner", person));
		}
		
		return criteria.list();
	}

	@Override
	public void update(ReportOrganisation user) throws ContracticModelException {
		if (user == null)
			throw new IllegalArgumentException("Null user supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(user);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving report %s", user),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(ReportOrganisation report) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(report);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when deleting report %s", report),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
