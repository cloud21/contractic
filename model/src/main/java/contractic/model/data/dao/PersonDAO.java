package contractic.model.data.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Contact;
import contractic.model.data.store.Module;
import contractic.model.data.store.Person;
import contractic.model.data.util.HibernateUtil;

public class PersonDAO implements CrudOperation<Person> {

	@Override
	public Person create(Person person) throws ContracticModelException {
		if (person == null)
			throw new IllegalArgumentException("Null person supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(person);
    		tx.commit();
    		return person;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving person %s", person), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<Person> read(final Person person) throws ContracticModelException {
		if (person == null)
			throw new IllegalArgumentException("Null person supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Person.class);
		
		Long id = person.getId();
		if (id != null)
			criteria.add(Restrictions.eq("Id", person.getId()));
		
		if (person.getOrganisation() != null && person.getOrganisation().getId()!=null)
			criteria.add(Restrictions.eq("Organisation", person.getOrganisation()));

		if(person.getActive() != null)
			criteria.add(Restrictions.eq("Active", person.getActive()));
		
		if(person.getFirstName()!=null || person.getMiddleName()!=null || person.getLastName()!=null){
			criteria.add(Restrictions.disjunction()
					.add(Restrictions.ilike("FirstName", person.getFirstName(), MatchMode.ANYWHERE))
					.add(Restrictions.ilike("MiddleName", person.getMiddleName(), MatchMode.ANYWHERE))
					.add(Restrictions.ilike("LastName", person.getLastName(), MatchMode.ANYWHERE))
					.add(Restrictions.ilike("JobTitle", person.getJobTitle(), MatchMode.ANYWHERE)));
		}
		
		return criteria.list();
	}

	@Override
	public void update(Person person) throws ContracticModelException {
		if (person == null)
			throw new IllegalArgumentException("Null person supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(person);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving person %s", person),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Person person) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			// Merge first to get the most recent view of the object
			Object p = session.merge(person);
			session.delete(p);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete person %s", person),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
