package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.GroupSet;
import contractic.model.data.store.Organisation;
import contractic.model.data.util.HibernateUtil;

public class GroupSetDAO implements CrudOperation<GroupSet> {

	@Override
	public GroupSet create(GroupSet groupSet) throws ContracticModelException {
		if (groupSet == null)
			throw new IllegalArgumentException("Null group set supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(groupSet);
    		tx.commit();
    		return groupSet;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving group set %s", groupSet), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<GroupSet> read(GroupSet groupSet) throws ContracticModelException {
		if (groupSet == null)
			throw new IllegalArgumentException("Null group set supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(GroupSet.class);
		Long id = groupSet.getId();
		if(id != null){
			criteria.add(Restrictions.eq("Id", id));
		}else{
			Organisation organisation = groupSet.getOrganisation();
			if(organisation == null)
				throw new IllegalArgumentException("Organisation must be supplied");
			criteria.add(Restrictions.eq("Organisation", organisation));
			
			if (groupSet.getId() != null)
				criteria.add(Restrictions.eq("Id", groupSet.getId()));
			
			if(groupSet.getName()!=null)
				criteria.add(Restrictions.eq("Name", groupSet.getName()));
			
			if(groupSet.getDescription()!=null)
				criteria.add(Restrictions.ilike("Description", groupSet.getDescription(), MatchMode.ANYWHERE));
			
			if (groupSet.getActive() != null)
				criteria.add(Restrictions.eq("Active", groupSet.getActive()));
		}
		return criteria.list();
	}

	@Override
	public void update(GroupSet groupSet) throws ContracticModelException {
		if (groupSet == null)
			throw new IllegalArgumentException("Null group set supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(groupSet);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving group set %s", groupSet),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(GroupSet group) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(group);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete group %s", group),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
