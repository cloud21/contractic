package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.ContractType;
import contractic.model.data.util.ExceptionUtils;
import contractic.model.data.util.HibernateUtil;

public class ContractTypeDAO implements CrudOperation<ContractType> {

	private static ResourceBundle errorResourceBundle = CrudOperation.getErrorBundle();
	

	
	@Override
	public ContractType create(ContractType contractType) throws ContracticModelException {
		if (contractType == null)
			throw new IllegalArgumentException("Null contract type supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(contractType);
    		tx.commit();
    		return contractType;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}
    		String message = String.format("Error occured when saving contract type %s", contractType);
    		if(ExceptionUtils.voilatesContraint("UQ_ContractType_Name_Organisation", ex)){
    			String code = "2000";
    			String format = errorResourceBundle.getString(code);
    			//message = String.format("Contract type '%s' already exists", contractType.getName());
    			message = String.format(format, contractType.getName());
    		}
    		throw new ContracticModelException(message, ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<ContractType> read(ContractType contractType) throws ContracticModelException {
		if (contractType == null)
			throw new IllegalArgumentException("Null contract type supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(ContractType.class);

		if (contractType.getId() != null)
			criteria.add(Restrictions.eq("Id", contractType.getId()));
		
		if(contractType.getName()!=null)
			criteria.add(Restrictions.ilike("Name", contractType.getName(), MatchMode.ANYWHERE));
		
		if(contractType.getOrganisation()!=null)
			criteria.add(Restrictions.eq("Organisation", contractType.getOrganisation()));
		
		return criteria.list();
	}

	@Override
	public void update(ContractType contractType) throws ContracticModelException {
		if (contractType == null)
			throw new IllegalArgumentException("Null contract type supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(contractType);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
    		String message = String.format("Error occured when saving contract type %s", contractType);
    		if(ExceptionUtils.voilatesContraint("UQ_ContractType_Name_Organisation", ex)){
    			String code = "2000";
    			String format = errorResourceBundle.getString(code);
    			//message = String.format("Contract type '%s' already exists", contractType.getName());
    			message = String.format(format, contractType.getName());
    		}
    		throw new ContracticModelException(message, ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(ContractType contractType) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(contractType);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			
    		String message = String.format("Error occured when saving contract type %s", contractType);
    		if(ExceptionUtils.voilatesContraint("FK_Contract_ContractType_Id", ex)){
    			String code = "2001";
    			String format = errorResourceBundle.getString(code);
    			message = String.format(format, contractType.getName());
    		}
    		throw new ContracticModelException(message, ex);
			
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
