package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Module;
import contractic.model.data.store.Person;
import contractic.model.data.store.User;
import contractic.model.data.util.HibernateUtil;

public class ModuleDAO implements CrudOperation<Module>{

	@Override
	public Module create(Module entity) throws ContracticModelException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Module> read(Module entity) throws ContracticModelException {
		if (entity == null)
			throw new IllegalArgumentException("Null module supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Module.class);
		
		Long id = entity.getId();
		String name = entity.getName();
		
		if (id != null)
			criteria.add(Restrictions.eq("Id", entity.getId()));
		
		if (name != null)
			criteria.add(Restrictions.eq("Name", entity.getName()));
		
		criteria.setCacheable(true);
		
		return criteria.list();
	}

	@Override
	public void update(Module entity) throws ContracticModelException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Module entity) throws ContracticModelException {
		// TODO Auto-generated method stub
		
	}

}
