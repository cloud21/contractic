package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Organisation;
import contractic.model.data.util.HibernateUtil;

public class OrganisationDAO implements CrudOperation<Organisation> {

	@Override
	public Organisation create(Organisation organisation)
			throws ContracticModelException {
		if (organisation == null)
			throw new IllegalArgumentException("Null organisation supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.save(organisation);
			tx.commit();
			return organisation;
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving organisation %s", organisation),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public List<Organisation> read(Organisation organisation)
			throws ContracticModelException {
		if (organisation == null)
			throw new IllegalArgumentException("Null organisation supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Organisation.class);
		Long id = organisation.getId();
		if (id != null)
			criteria.add(Restrictions.eq("Id", organisation.getId()));

		if (organisation.getCode()!=null)
			criteria.add(Restrictions.eq("Code", organisation.getCode()));
		
		return criteria.list();
	}

	@Override
	public void update(Organisation organisation)
			throws ContracticModelException {
		if (organisation == null)
			throw new IllegalArgumentException("Null organisation supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(organisation);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving organisation %s", organisation),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Organisation entity) throws ContracticModelException {
		// TODO Auto-generated method stub

	}

}
