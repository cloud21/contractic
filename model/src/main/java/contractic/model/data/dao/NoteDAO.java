package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Contact;
import contractic.model.data.store.Note;
import contractic.model.data.util.HibernateUtil;

public class NoteDAO implements CrudOperation<Note> {

	@Override
	public Note create(Note note) throws ContracticModelException {
		if (note == null)
			throw new IllegalArgumentException("Null contact supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(note);
    		tx.commit();
    		return note;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving note %s", note), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<Note> read(Note note) throws ContracticModelException {
		if (note == null)
			throw new IllegalArgumentException("Null note supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Note.class);
		
		Long id = note.getId();
		if (id != null)
			criteria.add(Restrictions.eq("Id", note.getId()));
		
		if (note.getModule() != null && note.getModule().getId()!=null)
			criteria.add(Restrictions.eq("Module", note.getModule()));
		
		if (note.getExternalId() != null && (note.getModule() == null || note.getModule().getId()==null))
			throw new ContracticModelException("A note is fully defined by the module and external Id. One of these hasn't been supplied");
		
		if (note.getExternalId() != null)
			criteria.add(Restrictions.eq("ExternalId", note.getExternalId()));

		return criteria.list();
		
	}

	@Override
	public void update(Note note) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(note);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving note %s", note),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Note note) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			// Merge first to get the most recent view of the object
			Object p = session.merge(note);
			session.delete(p);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when deleting note %s", note),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
