package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Group;
import contractic.model.data.util.HibernateUtil;

public class GroupDAO implements CrudOperation<Group> {

	@Override
	public Group create(Group group) throws ContracticModelException {
		if (group == null)
			throw new IllegalArgumentException("Null group supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(group);
    		tx.commit();
    		return group;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving group %s", group), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<Group> read(Group group) throws ContracticModelException {
		if (group == null)
			throw new IllegalArgumentException("Null group supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Group.class);
		
		if (group.getId() != null)
			criteria.add(Restrictions.eq("Id", group.getId()));
		else{
			if(group.getName()!=null)
				criteria.add(Restrictions.eq("Name", group.getName()));
			
			if(group.getDescription()!=null)
				criteria.add(Restrictions.ilike("Description", group.getDescription(), MatchMode.ANYWHERE));
			
			if(group.getOrganisation()!=null)
				criteria.add(Restrictions.eq("Organisation", group.getOrganisation()));
			
			if(group.getGroupSet() != null)
				criteria.add(Restrictions.eq("GroupSet", group.getGroupSet()));
			else
				criteria.add(Restrictions.isNull("GroupSet"));
			
			if (group.getModule() != null)
				criteria.add(Restrictions.eq("Module", group.getModule()));
		}
		
		return criteria.list();
	}

	@Override
	public void update(Group group) throws ContracticModelException {
		if (group == null)
			throw new IllegalArgumentException("Null group supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(group);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving group %s", group),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Group group) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(group);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete group %s", group),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
