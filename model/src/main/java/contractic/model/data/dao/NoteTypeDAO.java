package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.AddressType;
import contractic.model.data.store.NoteType;
import contractic.model.data.util.HibernateUtil;

public class NoteTypeDAO implements CrudOperation<NoteType> {

	private static ResourceBundle errorResourceBundle = CrudOperation.getErrorBundle();
	
	@Override
	public NoteType create(NoteType noteType) throws ContracticModelException {
		if (noteType == null)
			throw new IllegalArgumentException("Null note type supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(noteType);
    		tx.commit();
    		return noteType;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving note type %s", noteType), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<NoteType> read(NoteType noteType) throws ContracticModelException {
		if (noteType == null)
			throw new IllegalArgumentException("Null note type supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(NoteType.class);
		Long id = noteType.getId();
		if (id != null)
			criteria.add(Restrictions.eq("Id", noteType.getId()));
		
		if(noteType.getName()!=null)
			criteria.add(Restrictions.ilike("Name", noteType.getName(), MatchMode.ANYWHERE));
		
		if(noteType.getOrganisation()!=null)
			criteria.add(Restrictions.eq("Organisation", noteType.getOrganisation()));
		
		criteria.setCacheable(true);
		
		return criteria.list();
	}

	@Override
	public void update(NoteType noteType) throws ContracticModelException {
		if (noteType == null)
			throw new IllegalArgumentException("Null noteType supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(noteType);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving note type %s", noteType),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(NoteType noteType) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(noteType);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete note type %s", noteType),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
