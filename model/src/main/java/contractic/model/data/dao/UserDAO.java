package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Person;
import contractic.model.data.store.User;
import contractic.model.data.util.ExceptionUtils;
import contractic.model.data.util.HibernateUtil;

public class UserDAO implements CrudOperation<User> {

	private static ResourceBundle errorResourceBundle = CrudOperation.getErrorBundle();
	
	@Override
	public User create(User user) throws ContracticModelException {
		if (user == null)
			throw new IllegalArgumentException("Null user supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(user);
    		tx.commit();
    		return user;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		String message = String.format("Error occured when saving user %s", user.getLoginId());
    		if(ExceptionUtils.voilatesContraint("UQ_User_LoginId", ex)){
    			String code = "4001";
    			String format = errorResourceBundle.getString(code);
    			//message = String.format("Contract type '%s' already exists", contractType.getName());
    			message = String.format(format, user.getLoginId());
    		}
    		throw new ContracticModelException(message, ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<User> read(User user) throws ContracticModelException {
		if (user == null)
			throw new IllegalArgumentException("Null user supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(User.class, "user");
		Long id = user.getId();
		String login = user.getLoginId();
		Person person = user.getPerson();
		
		if (person != null && person.getOrganisation() != null && person.getOrganisation().getId()!=null){
			criteria.createAlias("user.Person", "person");
			criteria.add(Restrictions.eq("person.Organisation", person.getOrganisation()));
		}
		
		if (id != null)
			criteria.add(Restrictions.eq("user.Id", user.getId()));
		
		if (login != null)
			criteria.add(Restrictions.eq("user.LoginId", user.getLoginId()));
		
		return criteria.list();
	}

	@Override
	public void update(User user) throws ContracticModelException {
		if (user == null)
			throw new IllegalArgumentException("Null user supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(user);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
    		String message = String.format("Error occured when saving user %s", user.getLoginId());
    		if(ExceptionUtils.voilatesContraint("UQ_User_LoginId", ex)){
    			String code = "4001";
    			String format = errorResourceBundle.getString(code);
    			//message = String.format("Contract type '%s' already exists", contractType.getName());
    			message = String.format(format, user.getLoginId());
    		}
    		throw new ContracticModelException(message, ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(User user) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(user);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
    		String message = String.format("Error occured when deleting user %s", user.getLoginId());
    		if(ExceptionUtils.voilatesContraint("FK_Audit_UserId|FK_GroupUser_UserId|FK_Note_UserId|FK_UserPermi_UserId", ex)){
    			String code = "4002";
    			String format = errorResourceBundle.getString(code);
    			//message = String.format("Contract type '%s' already exists", contractType.getName());
    			message = String.format(format, user.getLoginId());
    		}
    		throw new ContracticModelException(message, ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
