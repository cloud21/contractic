package contractic.model.data.dao;

import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.GroupUser;
import contractic.model.data.util.HibernateUtil;

public class GroupUserDAO implements CrudOperation<GroupUser> {

	private static ResourceBundle errorResourceBundle = CrudOperation.getErrorBundle();
	
	@Override
	public GroupUser create(GroupUser groupUser) throws ContracticModelException {
		if (groupUser == null)
			throw new IllegalArgumentException("Null group user supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(groupUser);
    		tx.commit();
    		return groupUser;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving group user %s", groupUser), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<GroupUser> read(GroupUser groupUser) throws ContracticModelException {
		if (groupUser == null)
			throw new IllegalArgumentException("Null status supplied");
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(GroupUser.class);
		
		if (groupUser.getId() != null)
			criteria.add(Restrictions.eq("Id", groupUser.getId()));
		
		return criteria.list();
	}

	@Override
	public void update(GroupUser groupUser) throws ContracticModelException {
		if (groupUser == null)
			throw new IllegalArgumentException("Null status supplied");

		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(groupUser);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving status %s", groupUser),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(GroupUser status) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.delete(status);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when delete status %s", status),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
