package contractic.model.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Audit;
import contractic.model.data.util.HibernateUtil;

public class AuditDAO implements CrudOperation<Audit> {

	@Override
	public Audit create(Audit audit) throws ContracticModelException {
		if (audit == null)
			throw new IllegalArgumentException("Null audit object supplied");
		
		Session session = null;
		Transaction tx = null;

    	try{
    		session = HibernateUtil.currentSession();
    		tx = session.beginTransaction();
    		session.save(audit);
    		tx.commit();
    		return audit;
    	}catch(Throwable ex){
    		try{
    			tx.rollback();
    		}catch(RuntimeException rbe){
    			ex.printStackTrace();
    		}   		
    		throw new ContracticModelException(String.format("Error occured when saving audit %s", audit), ex);
    	}finally{
    		if(session!=null){
    			session.close();
    		}
    	}
	}

	@Override
	public List<Audit> read(Audit audit) throws ContracticModelException {
		if (audit == null)
			throw new IllegalArgumentException("Null audit supplied");
		if(audit.getOrganisation()==null)
			throw new IllegalArgumentException("Organisation must be supplied");
		
		Session session = HibernateUtil.currentSession();

		final Criteria criteria = session.createCriteria(Audit.class);
		
		Long id = audit.getId();
		if (id != null)
			criteria.add(Restrictions.eq("Id", audit.getId()));
		else{
			
			if (audit.getUser()!=null)
				criteria.add(Restrictions.eq("User", audit.getUser()));
			if (audit.getModule() != null && audit.getModule().getId()!=null)
				criteria.add(Restrictions.eq("Module", audit.getModule()));
			
			if (audit.getExternalId() != null && (audit.getModule() == null || audit.getModule().getId()==null))
				throw new ContracticModelException("An audit is fully defined by the module and external Id. One of these hasn't been supplied");
			
			if (audit.getExternalId() != null)
				criteria.add(Restrictions.eq("ExternalId", audit.getExternalId()));
			
			criteria.add(Restrictions.eq("Organisation", audit.getOrganisation()));
		}

		return criteria.list();
		
	}

	@Override
	public void update(Audit note) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			session.update(note);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when saving note %s", note),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Audit note) throws ContracticModelException {
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.currentSession();
			tx = session.beginTransaction();
			// Merge first to get the most recent view of the object
			Object p = session.merge(note);
			session.delete(p);
			tx.commit();
		} catch (Throwable ex) {
			try {
				tx.rollback();
			} catch (RuntimeException rbe) {
				ex.printStackTrace();
			}
			throw new ContracticModelException(String.format(
					"Error occured when deleting note %s", note),
					ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
