package contractic.model.data.store;

import java.util.Date;

public class ReportOrganisation {
	private Long Id;
	private String Name;
	private String Description;
	private String Schedule;
	private Report Report;
	private Organisation Organisation;
	private Group Group;
	private Person Owner;
	private Boolean Active;
	private Date Created;
	private Date LastRun;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Report getReport() {
		return Report;
	}
	public void setReport(Report report) {
		Report = report;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
	public Date getCreated() {
		return Created;
	}
	public void setCreated(Date created) {
		Created = created;
	}
	public Date getLastRun() {
		return LastRun;
	}
	public void setLastRun(Date lastRun) {
		LastRun = lastRun;
	}
	public Group getGroup() {
		return Group;
	}
	public void setGroup(Group group) {
		Group = group;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getSchedule() {
		return Schedule;
	}
	public void setSchedule(String schedule) {
		Schedule = schedule;
	}
	public Person getOwner() {
		return Owner;
	}
	public void setOwner(Person owner) {
		Owner = owner;
	}
	
}
