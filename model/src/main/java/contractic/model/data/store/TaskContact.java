package contractic.model.data.store;

import java.util.Date;

public class TaskContact {
	private Long Id;
	private Contact Contact;
	private Task Task;
	private Date Updated;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Contact getContact() {
		return Contact;
	}
	public void setContact(Contact contact) {
		Contact = contact;
	}
	public Task getTask() {
		return Task;
	}
	public void setTask(Task task) {
		Task = task;
	}
	public Date getUpdated() {
		return Updated;
	}
	public void setUpdated(Date updated) {
		Updated = updated;
	}
	
	
}
