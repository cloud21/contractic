package contractic.model.data.store;

public enum UserRolePermissionAction {
	/*
	 * User can read record from the collection
	 */
	READ, 
	
	/**
	 * User can write (create and edit) to the collection
	 */
	WRITE, 
	
	/**
	 * User can delete from the collection
	 */
	DELETE
}
