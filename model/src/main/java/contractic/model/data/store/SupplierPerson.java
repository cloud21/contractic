package contractic.model.data.store;

import java.util.Date;

public class SupplierPerson {
	private Long Id;
	private Date Created;
	private Boolean Active;
	private String ExternalRef;
	private Person Person;

	private Supplier Supplier;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Date getCreated() {
		return Created;
	}
	public void setCreated(Date created) {
		Created = created;
	}

	public Person getPerson() {
		return Person;
	}
	public void setPerson(Person person) {
		Person = person;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
	public String getExternalRef() {
		return ExternalRef;
	}
	public void setExternalRef(String externalRef) {
		ExternalRef = externalRef;
	}
	public Supplier getSupplier() {
		return Supplier;
	}
	public void setSupplier(Supplier supplier) {
		Supplier = supplier;
	}
	
}
