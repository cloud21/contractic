package contractic.model.data.store;

public class Group {
	private Long Id;
	private String Name;
	private String Description;
	private Boolean Active;
	private Module Module;
	private Group Parent;
	private GroupSet GroupSet;
	private Organisation Organisation;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
	public Module getModule() {
		return Module;
	}
	public void setModule(Module module) {
		Module = module;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
	public Group getParent() {
		return Parent;
	}
	public void setParent(Group parent) {
		Parent = parent;
	}
	public GroupSet getGroupSet() {
		return GroupSet;
	}
	public void setGroupSet(GroupSet groupSet) {
		GroupSet = groupSet;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((GroupSet == null) ? 0 : GroupSet.hashCode());
		result = prime * result + ((Module == null) ? 0 : Module.hashCode());
		result = prime * result + ((Name == null) ? 0 : Name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (GroupSet == null) {
			if (other.GroupSet != null)
				return false;
		} else if (!GroupSet.equals(other.GroupSet))
			return false;
		if (Module == null) {
			if (other.Module != null)
				return false;
		} else if (!Module.equals(other.Module))
			return false;
		if (Name == null) {
			if (other.Name != null)
				return false;
		} else if (!Name.equals(other.Name))
			return false;
		return true;
	}
	
}
