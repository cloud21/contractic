package contractic.model.data.store;

public class NoteType {
	private Long Id;
	private String Name;
	private String Description;
	private Boolean Active;
	private Organisation Organisation;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
}
