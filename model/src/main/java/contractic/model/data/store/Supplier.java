package contractic.model.data.store;

import java.util.Date;

public class Supplier {
	private Long Id;
	private String Reference;
	private String Name;
	private Date Created;
	private boolean Active;
	private String ExternalRef;
	private Organisation Organisation;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getReference() {
		return Reference;
	}
	public void setReference(String reference) {
		Reference = reference;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public Date getCreated() {
		return Created;
	}
	public void setCreated(Date created) {
		Created = created;
	}

	public boolean isActive() {
		return Active;
	}
	public void setActive(boolean active) {
		Active = active;
	}
	public String getExternalRef() {
		return ExternalRef;
	}
	public void setExternalRef(String externalRef) {
		ExternalRef = externalRef;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
	
	
}
