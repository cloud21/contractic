package contractic.model.data.store;

public class Report {
	private Long Id;
	private String Name;
	private String Description;
	private String Command;
	private Boolean Active;
	private String ExternalRef;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getCommand() {
		return Command;
	}
	public void setCommand(String command) {
		Command = command;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
	public String getExternalRef() {
		return ExternalRef;
	}
	public void setExternalRef(String externalRef) {
		ExternalRef = externalRef;
	}
	
}
