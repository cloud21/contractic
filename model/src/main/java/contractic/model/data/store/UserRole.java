package contractic.model.data.store;

import java.util.HashSet;
import java.util.Set;

public class UserRole {
	private Long Id;
	private String Name;
	private String Description;
	private Organisation Organisation;
	private Boolean Active;
	private Set<UserRolePermission> Permissions = new HashSet<>();
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
	public Set<UserRolePermission> getPermissions() {
		return Permissions;
	}
	public void setPermissions(Set<UserRolePermission> permissions) {
		Permissions = permissions;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Name == null) ? 0 : Name.hashCode());
		result = prime * result
				+ ((Organisation == null) ? 0 : Organisation.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRole other = (UserRole) obj;
		if (Name == null) {
			if (other.Name != null)
				return false;
		} else if (!Name.equals(other.Name))
			return false;
		if (Organisation == null) {
			if (other.Organisation != null)
				return false;
		} else if (!Organisation.equals(other.Organisation))
			return false;
		return true;
	}

	
	
}
