package contractic.model.data.store;

public class GroupObject {
	private Long Id;
	private Group Group;
	private Module Module;
	private Long ExternalId;
	private Boolean Active;
	private GroupSet GroupSet;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Group getGroup() {
		return Group;
	}
	public void setGroup(Group group) {
		Group = group;
	}
	public Module getModule() {
		return Module;
	}
	public void setModule(Module module) {
		Module = module;
	}
	public Long getExternalId() {
		return ExternalId;
	}
	public void setExternalId(Long externalId) {
		ExternalId = externalId;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
	public GroupSet getGroupSet() {
		return GroupSet;
	}
	public void setGroupSet(GroupSet groupSet) {
		GroupSet = groupSet;
	}
}
