package contractic.model.data.store;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Entity implementation class for Entity: Person
 *
 */

public class Person implements Serializable {

	private Long Id;
	private String Title;
	private String FirstName;
	private String MiddleName;
	private String LastName;
	private String JobTitle;
	private String Gender;
	private Date Created;
	private Boolean Active;
	private Organisation Organisation;
	private User user;
	
	private static final long serialVersionUID = 1L;

	public Person() {
		super();
	}   
	public Long getId() {
		return this.Id;
	}

	public void setId(Long Id) {
		this.Id = Id;
	}   
	public String getTitle() {
		return this.Title;
	}

	public void setTitle(String Title) {
		this.Title = Title;
	}   
	public String getFirstName() {
		return this.FirstName;
	}

	public void setFirstName(String FirstName) {
		this.FirstName = FirstName;
	}   
	public String getMiddleName() {
		return this.MiddleName;
	}

	public void setMiddleName(String MiddleName) {
		this.MiddleName = MiddleName;
	}   
	public String getLastName() {
		return this.LastName;
	}

	public void setLastName(String LastName) {
		this.LastName = LastName;
	}   
	public String getJobTitle() {
		return this.JobTitle;
	}

	public void setJobTitle(String JobTitle) {
		this.JobTitle = JobTitle;
	}   
	public String getGender() {
		return this.Gender;
	}

	public void setGender(String Gender) {
		this.Gender = Gender;
	}   
	public Date getCreated() {
		return this.Created;
	}

	public void setCreated(Date Created) {
		this.Created = Created;
	}   
	public Boolean getActive() {
		return this.Active;
	}

	public void setActive(Boolean Enabled) {
		this.Active = Enabled;
	}
	
	public Organisation getOrganisation() {
		return this.Organisation;
	}

	public void setOrganisation(Organisation Organisation) {
		this.Organisation = Organisation;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}



}
