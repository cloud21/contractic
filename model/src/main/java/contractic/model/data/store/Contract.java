package contractic.model.data.store;

import java.util.Date;

public class Contract {
	public static final String TITLE = "Title";
	public static final String DESCRIPTION = "Description";
	public static final String STARTDATE = "StartDate";
	public static final String ENDDATE = "EndDate";
	public static final String REFERENCE = "Reference";
	public static final String SUPPLIER = "Supplier";
	public static final String OWNER = "Owner";
	public static final String STATUS = "Status";
	public static final String TYPE = "Type";
	public static final String TERM = "Term";
	public static final String VALUE = "Value";
	public static final String VALUEUOM = "ValueUOM";
	
	private Long Id;
	private String Title;
	private String Description;
	private Date StartDate;
	private Date EndDate;
	private String term;
	private String reference;
	private Double value;
	private String valueUOM;
	private Supplier Supplier;
	private Person Owner;
	private Status Status;
	private ContractType Type;
	private Organisation Organisation;
	
	private Group Group;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public Date getStartDate() {
		return StartDate;
	}
	public void setStartDate(Date startDate) {
		StartDate = startDate;
	}
	public Date getEndDate() {
		return EndDate;
	}
	public void setEndDate(Date endDate) {
		EndDate = endDate;
	}
	public Supplier getSupplier() {
		return Supplier;
	}
	public void setSupplier(Supplier supplier) {
		Supplier = supplier;
	}
	public Person getOwner() {
		return Owner;
	}
	public void setOwner(Person owner) {
		Owner = owner;
	}
	public Status getStatus() {
		return Status;
	}
	public void setStatus(Status status) {
		Status = status;
	}
	public ContractType getType() {
		return Type;
	}
	public void setType(ContractType type) {
		Type = type;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}

	@Override
	public String toString() {
		return "Contract [Title=" + Title + ", reference=" + reference + "]";
	}
	public Group getGroup() {
		return Group;
	}
	public void setGroup(Group group) {
		Group = group;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public String getValueUOM() {
		return valueUOM;
	}
	public void setValueUOM(String valueUOM) {
		this.valueUOM = valueUOM;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	
}
