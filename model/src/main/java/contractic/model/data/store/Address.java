package contractic.model.data.store;

public class Address {
	private Long Id;
	private String Section1;
	private String Section2;
	private String Section3;
	private String Section4;
	private String Section5;
	private String Section6;
	private String Section7;
	private String Section8;
	private Organisation Organisation;
	private AddressType Type;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getSection1() {
		return Section1;
	}
	public void setSection1(String section1) {
		Section1 = section1;
	}
	public String getSection2() {
		return Section2;
	}
	public void setSection2(String section2) {
		Section2 = section2;
	}
	public String getSection3() {
		return Section3;
	}
	public void setSection3(String section3) {
		Section3 = section3;
	}
	public String getSection4() {
		return Section4;
	}
	public void setSection4(String section4) {
		Section4 = section4;
	}
	public String getSection5() {
		return Section5;
	}
	public void setSection5(String section5) {
		Section5 = section5;
	}
	public String getSection6() {
		return Section6;
	}
	public void setSection6(String section6) {
		Section6 = section6;
	}
	public String getSection7() {
		return Section7;
	}
	public void setSection7(String section7) {
		Section7 = section7;
	}
	public String getSection8() {
		return Section8;
	}
	public void setSection8(String section8) {
		Section8 = section8;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
	public AddressType getType() {
		return Type;
	}
	public void setType(AddressType type) {
		Type = type;
	}
	
	
	
}
