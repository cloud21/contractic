package contractic.model.data.store;

public class AuditAction {
	public static final String CREATE = "Create";
	public static final String DELETE = "Delete";
	public static final String UPDATE = "Update";
	public static final String READ = "Read";
	private Long Id;
	private String Name;
	private String Description;
	private String Format;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getFormat() {
		return Format;
	}
	public void setFormat(String format) {
		Format = format;
	}
	
	
}
