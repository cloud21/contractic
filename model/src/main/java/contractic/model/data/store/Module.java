package contractic.model.data.store;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Module
 *
 */

public class Module implements Serializable {

	public static final String PERSON = "Person";
	public static final String ORGANISATION = "Organisation";
	public static final String SUPPLIER = "Supplier";
	public static final String CONTRACT = "Contract";
	public static final String TASK = "Task";
	public static final String DOCUMENT = "Document";
	public static final String CONTRACTDETAIL = "ContractDetail";
	
	private Long Id;
	private String Name;
	private String Description;
	private static final long serialVersionUID = 1L;
	

	public Module() {
		super();
	}   
	public Long getId() {
		return this.Id;
	}

	public void setId(Long Id) {
		this.Id = Id;
	}   
	public String getName() {
		return this.Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}   
	public String getDescription() {
		return this.Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Name == null) ? 0 : Name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Module other = (Module) obj;
		if (Name == null) {
			if (other.Name != null)
				return false;
		} else if (!Name.equals(other.Name))
			return false;
		return true;
	}
   
}
