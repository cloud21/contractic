package contractic.model.data.store;

import java.util.Date;

public class ContractDetail {
	
	public final static String VALUE_GROUP_FIELD = "VALUE";
	public final static String SUMMARY_GROUP_FIELD = "SUMMARY";
	public final static String CCN_GROUP_FIELD = "CCN";
	public final static String EXTENSION_GROUP_FIELD = "EXTENSION";
	
	public final static String CONTRACT_TERM_PERIOD_GROUP_FIELD = "CONTRACT_TERM_PERIOD";
	public static final String CONTRACT_TERM_NOTICE_GROUP_FIELD = "CONTRACT_TERM_NOTICE";
	public static final String CONTRACT_TERM_GOTO_MARKET_GROUP_FIELD = "CONTRACT_TERM_GOTO_MARKET";
	
	public static final String VALUE = "Value";
	public static final String NAME = "Name";
	public static final String DETAIL = "Detail";
	public static final String GROUP = "Group";
	
	private Long Id;
	private String Name;
	private String Description;
	private String Value;
	private Date LastUpdated;
	private Contract Contract;
	private Group Group;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getValue() {
		return Value;
	}
	public void setValue(String value) {
		Value = value;
	}
	public Date getLastUpdated() {
		return LastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		LastUpdated = lastUpdated;
	}
	public Contract getContract() {
		return Contract;
	}
	public void setContract(Contract contract) {
		Contract = contract;
	}
	public Group getGroup() {
		return Group;
	}
	public void setGroup(Group group) {
		Group = group;
	}
	
	
}
