package contractic.model.data.store;

public class ContractReferenceFormat {
	private Long Id;
	private String Format;
	private String Name;
	private String Description;
	private Boolean Default;
	private Boolean Active;
	private ContractType ContractType;
	private Organisation Organisation;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getFormat() {
		return Format;
	}
	public void setFormat(String format) {
		Format = format;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public Boolean getDefault() {
		return Default;
	}
	public void setDefault(Boolean default1) {
		Default = default1;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
	public ContractType getContractType() {
		return ContractType;
	}
	public void setContractType(ContractType contractType) {
		ContractType = contractType;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
}
