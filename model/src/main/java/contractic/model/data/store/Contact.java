package contractic.model.data.store;

/**
 * Any contact
 * @author Michael Sekamanya
 *
 */
public class Contact {
	private Long Id;
	private boolean active;
	private Long ExternalId;
	private Address Address;
	private Module Module;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public boolean getActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Long getExternalId() {
		return ExternalId;
	}
	public void setExternalId(Long externalId) {
		ExternalId = externalId;
	}
	public Address getAddress() {
		return Address;
	}
	public void setAddress(Address address) {
		Address = address;
	}
	public Module getModule() {
		return this.Module;
	}
	public void setModule(Module module) {
		this.Module = module;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Id == null) ? 0 : Id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (Id == null) {
			if (other.Id != null)
				return false;
		} else if (!Id.equals(other.Id))
			return false;
		return true;
	}
	
	
	
}
