package contractic.model.data.store;

public class UserRolePermission {

	private Long Id;
	private Module Module;
	private Long ExternalId;
	private Boolean Read;
	private Boolean Write;
	private Boolean Delete;
	private UserRole UserRole;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Module getModule() {
		return Module;
	}
	public void setModule(Module module) {
		Module = module;
	}
	public Long getExternalId() {
		return ExternalId;
	}
	public void setExternalId(Long externalId) {
		ExternalId = externalId;
	}
	public Boolean getRead() {
		return Read;
	}
	public void setRead(Boolean read) {
		Read = read;
	}
	public Boolean getWrite() {
		return Write;
	}
	public void setWrite(Boolean write) {
		Write = write;
	}
	public Boolean getDelete() {
		return Delete;
	}
	public void setDelete(Boolean delete) {
		Delete = delete;
	}
	public UserRole getUserRole() {
		return UserRole;
	}
	public void setUserRole(UserRole userRole) {
		UserRole = userRole;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Delete == null) ? 0 : Delete.hashCode());
		result = prime * result + ((Module == null) ? 0 : Module.hashCode());
		result = prime * result + ((Read == null) ? 0 : Read.hashCode());
		result = prime * result
				+ ((UserRole == null) ? 0 : UserRole.hashCode());
		result = prime * result + ((Write == null) ? 0 : Write.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRolePermission other = (UserRolePermission) obj;
		if (Delete == null) {
			if (other.Delete != null)
				return false;
		} else if (!Delete.equals(other.Delete))
			return false;
		if (Module == null) {
			if (other.Module != null)
				return false;
		} else if (!Module.equals(other.Module))
			return false;
		if (Read == null) {
			if (other.Read != null)
				return false;
		} else if (!Read.equals(other.Read))
			return false;
		if (UserRole == null) {
			if (other.UserRole != null)
				return false;
		} else if (!UserRole.equals(other.UserRole))
			return false;
		if (Write == null) {
			if (other.Write != null)
				return false;
		} else if (!Write.equals(other.Write))
			return false;
		return true;
	}
	
}
