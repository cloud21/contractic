package contractic.model.data.store;

import java.util.Date;

import javax.management.relation.Role;

public class User {
	private Long Id;
	private String LoginId;
	private String Password;
	private String Profile;
	private Boolean Active;
	private Date LastUpdated;
	private Person Person;
	private UserRole UserRole;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getLoginId() {
		return LoginId;
	}
	public void setLoginId(String login) {
		this.LoginId = login;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getProfile() {
		return Profile;
	}
	public void setProfile(String profile) {
		Profile = profile;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
	public Date getLastUpdated() {
		return LastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		LastUpdated = lastUpdated;
	}
	public Person getPerson() {
		return Person;
	}
	public void setPerson(Person person) {
		Person = person;
	}
	public UserRole getUserRole() {
		return UserRole;
	}
	public void setUserRole(UserRole userRole) {
		UserRole = userRole;
	}

}
