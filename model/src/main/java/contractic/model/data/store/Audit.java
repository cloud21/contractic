package contractic.model.data.store;

import java.util.Date;

public class Audit {
	private Long Id;
	private String Description;
	private Long ExternalId;
	private Module Module;
	private User User;
	private AuditAction Action;
	private Date Created;
	private String ChangeField;
	private String ChangeFrom;
	private String ChangeTo;
	private Organisation Organisation;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public Long getExternalId() {
		return ExternalId;
	}
	public void setExternalId(Long externalId) {
		ExternalId = externalId;
	}
	public Module getModule() {
		return Module;
	}
	public void setModule(Module module) {
		Module = module;
	}
	public User getUser() {
		return User;
	}
	public void setUser(User user) {
		User = user;
	}
	public AuditAction getAction() {
		return Action;
	}
	public void setAction(AuditAction action) {
		Action = action;
	}
	public Date getCreated() {
		return Created;
	}
	public void setCreated(Date created) {
		Created = created;
	}
	public String getChangeField() {
		return ChangeField;
	}
	public void setChangeField(String changeField) {
		ChangeField = changeField;
	}
	public String getChangeFrom() {
		return ChangeFrom;
	}
	public void setChangeFrom(String changeFrom) {
		ChangeFrom = changeFrom;
	}
	public String getChangeTo() {
		return ChangeTo;
	}
	public void setChangeTo(String changeTo) {
		ChangeTo = changeTo;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
}
