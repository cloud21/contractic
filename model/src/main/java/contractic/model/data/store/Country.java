package contractic.model.data.store;

public class Country {
	private Long Id;
	private String Name;
	private String CurrencyCode;
	private String CurrencyName;
	private String DialingCode;
	private String HtmlCode;
	private Boolean Active;
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getCurrencyCode() {
		return CurrencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}
	public String getCurrencyName() {
		return CurrencyName;
	}
	public void setCurrencyName(String currencyName) {
		CurrencyName = currencyName;
	}
	public String getDialingCode() {
		return DialingCode;
	}
	public void setDialingCode(String dialingCode) {
		DialingCode = dialingCode;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getHtmlCode() {
		return HtmlCode;
	}
	public void setHtmlCode(String htmlCode) {
		HtmlCode = htmlCode;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
}
