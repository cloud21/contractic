package contractic.model.data.store;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Entity implementation class for Entity: Organisation
 *
 */

public class Organisation implements Serializable {

	private Long Id;
	private String Name;
	private Date Created;
	private boolean Active;
	private String Code;
	private String Profile;
	private static final long serialVersionUID = 1L;

	public Organisation() {
		super();
	}   
	public Long getId() {
		return this.Id;
	}

	public void setId(Long Id) {
		this.Id = Id;
	}   
	public String getName() {
		return this.Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}   
	public Date getCreated() {
		return this.Created;
	}

	public void setCreated(Date Created) {
		this.Created = Created;
	}   
	public boolean getActive() {
		return this.Active;
	}

	public void setActive(boolean Active) {
		this.Active = Active;
	}
	
	public String getCode() {
		return Code;
	}
	public void setCode(String code) {
		Code = code;
	}
	public String getProfile() {
		return Profile;
	}
	public void setProfile(String profile) {
		Profile = profile;
	}
	@Override
	public String toString() {
		return "Organisation [Id=" + Id + ", Name=" + Name + ", Created="
				+ Created + ", Active=" + Active + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Code == null) ? 0 : Code.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Organisation other = (Organisation) obj;
		if (Code == null) {
			if (other.Code != null)
				return false;
		} else if (!Code.equals(other.Code))
			return false;
		return true;
	}
	
	
}
