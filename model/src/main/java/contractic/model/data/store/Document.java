package contractic.model.data.store;

import java.util.Date;

public class Document {
	private Long Id;
	private String Title;
	private String Description;
	private Date Received;
	private String FilePath;
	private Long ExternalId;
	private DocumentType DocumentType;
	private Status Status;
	private Module Module;
	private String FileType;
	private String FileName;
	private Organisation Organisation;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public Date getReceived() {
		return Received;
	}
	public void setReceived(Date received) {
		Received = received;
	}
	public String getFilePath() {
		return FilePath;
	}
	public void setFilePath(String path) {
		FilePath = path;
	}
	public Long getExternalId() {
		return ExternalId;
	}
	public void setExternalId(Long externalId) {
		ExternalId = externalId;
	}
	public DocumentType getDocumentType() {
		return DocumentType;
	}
	public void setDocumentType(DocumentType documentType) {
		DocumentType = documentType;
	}
	public Status getStatus() {
		return Status;
	}
	public void setStatus(Status status) {
		Status = status;
	}
	public Module getModule() {
		return Module;
	}
	public void setModule(Module module) {
		Module = module;
	}
	public String getFileType() {
		return FileType;
	}
	public void setFileType(String fileType) {
		FileType = fileType;
	}
	public String getFileName() {
		return FileName;
	}
	public void setFileName(String fileName) {
		FileName = fileName;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
	
}
