package contractic.model.data.store;

public class Status {
	private Long Id;
	private String name;
	private String description;
	private Boolean Active;
	private Module Module;
	private Organisation Organisation;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public void setActive(Boolean active) {
		Active = active;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(boolean active) {
		Active = active;
	}
	public Module getModule() {
		return Module;
	}
	public void setModule(Module module) {
		Module = module;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
	
	
}
