package contractic.model.data.store;

import java.util.Date;

public class Note {
	public static final String HEADER = "Header";
	public static final String TYPE = "Type";
	public static final String DETAIL = "Detail";
	
	private Long Id;
	private String Header;
	private String Detail;
	private Long ExternalId;
	private NoteType NoteType;
	private Module Module;
	private Boolean Active;
	private Date Created;
	private User User;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getHeader() {
		return Header;
	}
	public void setHeader(String header) {
		Header = header;
	}
	public String getDetail() {
		return Detail;
	}
	public void setDetail(String detail) {
		Detail = detail;
	}
	public Long getExternalId() {
		return ExternalId;
	}
	public void setExternalId(Long externalId) {
		ExternalId = externalId;
	}
	public NoteType getNoteType() {
		return NoteType;
	}
	public void setNoteType(NoteType noteType) {
		NoteType = noteType;
	}
	public Module getModule() {
		return Module;
	}
	public void setModule(Module module) {
		Module = module;
	}

	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
	public Date getCreated() {
		return Created;
	}
	public void setCreated(Date created) {
		Created = created;
	}
	public User getUser() {
		return User;
	}
	public void setUser(User user) {
		User = user;
	}
	
}
