package contractic.model.data.store;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Models a task within the system
 * @author Michael Sekamanya
 *
 */
public class Task {
	public static final String DESCRIPTION = "Description";
	public static final String TITLE = "Title";
	public static final String STATUS = "Status";
	public static final String PERSON = "Person";
	public static final String PARENT = "Parent";
	public static final String ENDDATE = "EndDate";
	public static final String STARTDATE = "StartDate";
	
	private Long Id;
	private String Title;
	private String Description;
	private Date Created;
	private Date StartDate;
	private Date EndDate;
	private Person Person;
	private Long ExternalId;
	private Status Status;
	private Module Module;
	private Task Parent;
	private Set<Task> Children;
	private Long Number;
	private Organisation Organisation;
	private Date RepeatStart;
	private Date RepeatEnd;
	private String RepeatYear;
	private String RepeatMonth;
	private String RepeatWeek;
	private String RepeatDay;
	private String RepeatDate;
	private boolean hasChildren;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String name) {
		Title = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public Date getCreated() {
		return Created;
	}
	public void setCreated(Date created) {
		Created = created;
	}
	public Date getStartDate() {
		return StartDate;
	}
	public void setStartDate(Date startDate) {
		StartDate = startDate;
	}
	public Date getEndDate() {
		return EndDate;
	}
	public void setEndDate(Date endDate) {
		EndDate = endDate;
	}
	public Person getPerson() {
		return Person;
	}
	public void setPerson(Person person) {
		Person = person;
	}
	public Long getExternalId() {
		return ExternalId;
	}
	public void setExternalId(Long externalId) {
		ExternalId = externalId;
	}
	public Status getStatus() {
		return Status;
	}
	public void setStatus(Status status) {
		Status = status;
	}
	public Module getModule() {
		return Module;
	}
	public void setModule(Module module) {
		Module = module;
	}
	public Task getParent() {
		return Parent;
	}
	public void setParent(Task parent) {
		Parent = parent;
	}
	public Organisation getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(Organisation organisation) {
		Organisation = organisation;
	}
	public Long getNumber() {
		return Number;
	}
	public void setNumber(Long number) {
		Number = number;
	}
	public Set<Task> getChildren() {
		return Children;
	}
	public void setChildren(Set<Task> children) {
		Children = children;
	}
	public Date getRepeatStart() {
		return RepeatStart;
	}
	public void setRepeatStart(Date repeatStart) {
		RepeatStart = repeatStart;
	}
	public Date getRepeatEnd() {
		return RepeatEnd;
	}
	public void setRepeatEnd(Date repeatEnd) {
		RepeatEnd = repeatEnd;
	}
	public String getRepeatYear() {
		return RepeatYear;
	}
	public void setRepeatYear(String repeatYear) {
		RepeatYear = repeatYear;
	}
	public String getRepeatMonth() {
		return RepeatMonth;
	}
	public void setRepeatMonth(String repeatMonth) {
		RepeatMonth = repeatMonth;
	}
	public String getRepeatWeek() {
		return RepeatWeek;
	}
	public void setRepeatWeek(String repeatWeek) {
		RepeatWeek = repeatWeek;
	}
	public String getRepeatDay() {
		return RepeatDay;
	}
	public void setRepeatDay(String repeatDay) {
		RepeatDay = repeatDay;
	}
	public String getRepeatDate() {
		return RepeatDate;
	}
	public void setRepeatDate(String repeatDate) {
		RepeatDate = repeatDate;
	}
	public boolean isHasChildren() {
		return hasChildren;
	}
	public void setHasChildren(boolean hasChildren) {
		this.hasChildren = hasChildren;
	}
	
}
