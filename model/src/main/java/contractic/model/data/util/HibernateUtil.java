package contractic.model.data.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	  public static SessionFactory sessionFactory;

	  public static void initialize(final URL url){
		    try {
			      // Create the SessionFactory from hibernate.cfg.xml
			      sessionFactory = new Configuration().configure(url).buildSessionFactory();
			    } catch (Throwable ex) {
			      throw new ExceptionInInitializerError(ex);
			    }
	  }
	  
	  public static void initialize(final String filename) throws MalformedURLException {
	      // Create the SessionFactory from hibernate.cfg.xml
	      File f = new File(filename);
	      initialize(f.toURI().toURL());
	  }
	  
	  public static void initialize(){
	        try {
	            // Create the SessionFactory from hibernate.cfg.xml
	            sessionFactory = new Configuration().configure().buildSessionFactory();
	        } catch (Throwable ex) {
	            // Make sure you log the exception, as it might be swallowed
	            System.err.println("Initial SessionFactory creation failed." + ex);
	            throw new ExceptionInInitializerError(ex);
	        }
	  }

	  public static final ThreadLocal session = new ThreadLocal();

	  public static Session currentSession() throws HibernateException {
	    Session s = (Session) session.get();
	    // Open a new Session, if this thread has none yet
	    if (s == null || !s.isOpen()) {
	    	s = null;
	    	s = sessionFactory.openSession();
	    	// Store it in the ThreadLocal variable
	    	session.set(s);
	    }
	    return s;
	  }

	  public static void closeSession() throws HibernateException {
	    Session s = (Session) session.get();
	    if (s != null)
	      s.close();
	    session.set(null);
	  }
	}
