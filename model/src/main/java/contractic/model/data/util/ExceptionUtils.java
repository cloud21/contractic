package contractic.model.data.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtils {
	public static String toString(Throwable ex){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		return sw.toString();
	}
	
	public static boolean voilatesContraint(String name, Throwable ex){
		String st = ExceptionUtils.toString(ex);
		String format = String.format("(?s).*(%s).*", name);
		return st.matches(format);
	}
}
