package contractic.model;

public class ContracticModelException extends Exception {
	public ContracticModelException(String message, Throwable t){
		super(message, t);
	}
	public ContracticModelException(String message){
		super(message);
	}
}
