package contractic.model.data.util;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.dao.BaseDAOTest;
import contractic.model.data.dao.ContractDAO;
import contractic.model.data.dao.ContractDetailDAO;
import contractic.model.data.dao.OrganisationDAO;
import contractic.model.data.store.Contract;
import contractic.model.data.store.ContractDetail;
import contractic.model.data.store.Organisation;

public class ConnectionPoolTest  extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	private List<Organisation> getOrganisations(Long id) throws ContracticModelException{
		final Organisation organisation = new Organisation();
		if(id!=null)
			organisation.setId(id);
		final OrganisationDAO organisationDAO = new OrganisationDAO();
		return organisationDAO.read(organisation);
	}
	
	private List<Contract> getContracts(Contract contract) throws ContracticModelException{
		final ContractDAO contractDAO = new ContractDAO();
		return contractDAO.read(contract);
	}
	
	private List<ContractDetail> getContractDetail(ContractDetail contractDetail) throws ContracticModelException{
		final ContractDetailDAO cd = new ContractDetailDAO();
		return cd.read(contractDetail);
	}
	
	@Test
	public void test() throws InterruptedException {
		final int MaxThreadCount = 100;
		final int ClientCount = 200;
		// Create 100 threads
		final ExecutorService executorService = Executors.newFixedThreadPool(MaxThreadCount);

		Random random = new Random();
		int Low = 10;
		int High = 100;
		
		IntStream.range(1, ClientCount).forEach(idx -> {
			
			// Each thread reads all organisations
			executorService.submit(new Runnable(){

				@Override
				public void run() {
					
					try {
						List<Organisation> orgList = getOrganisations(1000L);
						assert orgList.size() == 1;
						final Organisation organisation = orgList.get(0);
					
						while(true){
							Contract contract = new Contract();
							contract.setOrganisation(organisation);
							
							List<Contract> contractList = getContracts(contract);
							contractList.forEach(System.out::println);
						
							int Result = random.nextInt(High-Low) + Low;
							Thread.sleep(Result * 1000);			
						}
					
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}		
			});
			
		});
		
		Thread.sleep(Integer.MAX_VALUE);
	}

}
