package contractic.model.data.util;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class HibernatePersistenceTest {

	private EntityManager em;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
    public void testJDBCConnectivity() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
    {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        String connString = "jdbc:sqlserver://127.0.0.1;instanceName=SQLEXPRESS;databaseName=c21cms;user=sa;password=semica;";

        Connection conn = null;
        try{
            conn = DriverManager.getConnection(connString);
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        conn.close();

    }
	
	
	@Test
	public void testHibernateConnectivity() {
		try{
			String filename = "/hibernate.cfg.xml";
			final URL url = this.getClass().getResource(filename);
	    	HibernateUtil.initialize(url);
	    	Session session = HibernateUtil.currentSession();
			
			//EntityTransaction transaction = em.getTransaction();
//			CriteriaBuilder qb = em.getCriteriaBuilder();
//			CriteriaQuery<Organisation> c = qb.createQuery(Organisation.class);
//			Root<Organisation> p = c.from(Organisation.class);
//			TypedQuery<Organisation> q = em.createQuery(c); 
//			List<Organisation> result = q.getResultList();
//			result.stream().forEach(org -> System.out.println(org.getName()));
		}catch(Throwable ex){
			ex.printStackTrace();
			throw ex;
		}
	}
}
