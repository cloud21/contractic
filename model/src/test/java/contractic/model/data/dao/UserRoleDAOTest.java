package contractic.model.data.dao;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.UserRole;

public class UserRoleDAOTest extends BaseDAOTest{
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreate() throws ContracticModelException {
		
		Organisation organisation = new Organisation();
		CrudOperation<Organisation> oop = new OrganisationDAO();
		final List<Organisation> organisationList = oop.read(organisation);
		
		CrudOperation<UserRole> urop = new UserRoleDAO();
		
		final UserRole userRole = new UserRole();
		
		organisationList.forEach(org -> {
			userRole.setName("Administrator");
			userRole.setDescription("System Administrator");
			userRole.setOrganisation(org);
			userRole.setActive(true);
			
			try {
				urop.create(userRole);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});			
		
		
	}

/*	@Test
	public void testRead() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}*/

}
