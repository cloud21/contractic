package contractic.model.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Address;
import contractic.model.data.store.AddressType;
import contractic.model.data.store.Organisation;

public class AddressDAOTest extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreate() throws ContracticModelException {
		
		// Get the Email address type
		final AddressType addressType = new AddressType();
		final AddressTypeDAO adtOp = new AddressTypeDAO();
		List<AddressType> at = adtOp.read(addressType);
		Optional<AddressType> emailAddressType = at.stream().filter(type -> type.getName().equalsIgnoreCase("Email")).findFirst();
		assertTrue(emailAddressType.isPresent());
		
		// Get the organisation CTO
		Organisation organisation = new Organisation();
		CrudOperation<Organisation> orgOp = new OrganisationDAO();
		List<Organisation> orgList = orgOp.read(organisation);
		Optional<Organisation> organisationList = orgList.stream().filter(org -> org.getName().contains("CTO")).findFirst();
		assertTrue(organisationList.isPresent());
		
		// Now create an email contact
		Address address = new Address();
		address.setOrganisation(organisationList.get());
		address.setType(emailAddressType.get());
		address.setSection1("michael.sekamanya@cloud21.net");
		
		final AddressDAO adOp = new AddressDAO();
		adOp.create(address);
		
		assertNotNull(address.getId());
	}

	@Test
	public void testRead() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

}
