package contractic.model.data.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.Supplier;

public class SupplierDAOTest extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testCreate() throws ContracticModelException {
		
		Organisation org = new Organisation();
		org.setId(1000L);
		CrudOperation<Organisation> op = new OrganisationDAO();
		List<Organisation> orgList = op.read(org);
		
		assertEquals(orgList.size(), 1);
		org = orgList.get(0);
		
		Supplier supplier = new Supplier();
		supplier.setName("Dell Corporation");
		supplier.setReference("SUPPLIER-REF-001");
		supplier.setCreated(new Date());
		supplier.setOrganisation(org);
		
		SupplierDAO supplierDAO = new SupplierDAO();
		supplierDAO.create(supplier);
		assertNotNull(supplier.getId());
	}

	//@Ignore
	@Test
	public void testRead() throws ContracticModelException {
		Organisation org = new Organisation();
		org.setId(1000L);
		CrudOperation<Organisation> op = new OrganisationDAO();
		List<Organisation> orgList = op.read(org);
		
		assertEquals(orgList.size(), 1);
		org = orgList.get(0);
		
		Supplier supplier = new Supplier();
		supplier.setOrganisation(org);
		
		SupplierDAO supplierDAO = new SupplierDAO();
		List<Supplier> list = supplierDAO.read(supplier);
		
		// We just inserted a record in the create test so we only test if size is greater than one
		assertTrue(list.size() >= 1);
	}

	@Ignore
	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

}
