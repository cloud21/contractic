package contractic.model.data.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Module;
import contractic.model.data.store.Note;
import contractic.model.data.store.NoteType;
import contractic.model.data.store.Person;
import contractic.model.data.store.Supplier;

public class NoteDAOTest extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	//@Ignore
	@Test
	public void testCreate() throws ContracticModelException {
		
		// Get the 'Supplier' module
		Module module = new Module();
		module.setName("Supplier");

		CrudOperation<Module> mop = new ModuleDAO();
		List<Module> moduleList = mop.read(module);
		
		assertEquals(moduleList.size(), 1);
		module = moduleList.get(0);
		
		
		// Get the 'Info' note type
		NoteType noteType = new NoteType();
		noteType.setName("Info");
		
		CrudOperation<NoteType> nop = new NoteTypeDAO();
		List<NoteType> noteTypeList = nop.read(noteType);
		
		assertEquals(noteTypeList.size(), 1);
		noteType = noteTypeList.get(0);
		
		// Get the supplier for which we are creating this note
		Supplier supplier = new Supplier();
		CrudOperation<Supplier> sop = new SupplierDAO();
		List<Supplier> supplierList = sop.read(supplier);
		assertTrue(supplierList.size() >= 1);
		supplier = supplierList.get(0);
		
		Note note = new Note();
		note.setModule(module);
		note.setNoteType(noteType);
		note.setExternalId(supplier.getId());
		note.setActive(true);
		note.setHeader("Testing");
		note.setDetail("Testing details");
		

		final NoteDAO operation = new NoteDAO();
		operation.create(note);
		
		assertNotNull(note.getId());
	}


	@Ignore
	@Test
	public void testRead() throws ContracticModelException {
		Person person = new Person();
		person.setId(19441L);
		
		final PersonDAO operation = new PersonDAO();
		try {
			List<Person> personList = operation.read(person);
			assertTrue(personList.size() != 0);
		} catch (ContracticModelException e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Ignore
	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

}
