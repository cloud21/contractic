package contractic.model.data.dao;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.store.AddressType;
import contractic.model.data.store.Document;
import contractic.model.data.store.DocumentType;
import contractic.model.data.store.Module;
import contractic.model.data.store.Person;
import contractic.model.data.store.Status;

public class DocumentDAOTest extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreate() throws ContracticModelException {
		// Get the document type
		DocumentType documentType = new DocumentType();
		final DocumentTypeDAO op = new DocumentTypeDAO();
		List<DocumentType> doctypeList = op.read(documentType);
		assertTrue(doctypeList !=null && doctypeList.size()>0);
		documentType = doctypeList.get(0);
		
		// Get the status to be assumed by this document
		Status status = new Status();
		status.setName("Complete");
		final StatusDAO sop = new StatusDAO();
		List<Status> statusList = sop.read(status);
		assertTrue(statusList != null && statusList.size()>0);
		status = statusList.get(0);
		
		// Get the module for which we want to create this document
		Module module = new Module();
		module.setName("Person");
		final ModuleDAO mop = new ModuleDAO();
		List<Module> moduleList = mop.read(module);
		assertTrue(moduleList!=null && moduleList.size() >0);
		module = moduleList.get(0);
		
		// Get the object within that module that we want to create this document for
		Long externalId = null;
		if(module.getName().equals("Person")){
			Person person = new Person();
			person.setActive(true);
			PersonDAO personDAO = new PersonDAO();
			List<Person> personList = personDAO.read(person);
			assertTrue(personList!=null && personList.size()>0);
			externalId = personList.get(0).getId();
		}
		
		// Create the document
		Document document = new Document();
		document.setDocumentType(documentType);
		document.setStatus(status);
		document.setModule(module);
		document.setExternalId(externalId);
		document.setTitle("Person document");
		document.setDescription("Person document, person document, person document, person document, person document, person document, "
				+ "person document, person document, person document, person document, person document, person document, person document, "
				+ "person document, person document, person document, person document, person document, person document, ");
		document.setReceived(new Date());
		document.setFilePath("path/to/file.pdf");
		
		DocumentDAO documentDAO = new DocumentDAO();
		documentDAO.create(document);
		Assert.assertNotNull(document.getId());
	}

	@Ignore
	@Test
	public void testRead() throws ContracticModelException {
		final AddressType addressType = new AddressType();
		final AddressTypeDAO op = new AddressTypeDAO();
		
		List<AddressType> list = op.read(addressType);
		assertTrue(list !=null && list.size()>0);

	}

	@Ignore
	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

}
