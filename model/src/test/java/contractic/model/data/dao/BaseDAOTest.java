package contractic.model.data.dao;

import java.net.URL;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import contractic.model.data.util.HibernateUtil;

public class BaseDAOTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		String filename = "/hibernate.cfg.xml";
		final URL url = this.getClass().getResource(filename);
    	HibernateUtil.initialize(url);
	}

	@After
	public void tearDown() throws Exception {
	}
}
