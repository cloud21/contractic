package contractic.model.data.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;

public class PersonDAOTest extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	//@Ignore
	@Test
	public void testCreate() throws ContracticModelException {
		
		Organisation org = new Organisation();
		org.setCode("croydonnhs");
		CrudOperation<Organisation> op = new OrganisationDAO();
		List<Organisation> orgList = op.read(org);
		
		assertEquals(orgList.size(), 1);
		org = orgList.get(0);
		
		Person person = new Person();
		person.setTitle("Mr");
		person.setFirstName("Alan");
		person.setLastName("Binks");
		person.setJobTitle("Strategic Head of Procurement");
		person.setActive(true);
		person.setGender("M");
		person.setCreated(new Date());
		person.setOrganisation(org);
		
		final PersonDAO operation = new PersonDAO();
		operation.create(person);
		assertNotNull(person.getId());
	}

	@Ignore
	@Test
	public void testRead() throws ContracticModelException {
		Person person = new Person();
		person.setId(19441L);
		
		final PersonDAO operation = new PersonDAO();
		List<Person> personList = operation.read(person);
		assertTrue(personList.size() != 0);
	}

	@Ignore
	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

}
