package contractic.model.data.dao;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Organisation;

public class OrganisationDAOTest extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testCreate() throws ContracticModelException{
		final Organisation org = new Organisation();
		org.setName("Croydon Health Services");
		org.setCode("croydonnhs");
		org.setCreated(new Date());
		org.setActive(true);
		
		final OrganisationDAO orgDAO = new OrganisationDAO();
		orgDAO.create(org);
	}
	
	@Ignore
	@Test
	public void testRead() throws ContracticModelException {
		final Organisation organisation = new Organisation();
		final OrganisationDAO organisationDAO = new OrganisationDAO();
		try {
			//organisation.setCode("GOSH");
			List<Organisation> organisationList = organisationDAO.read(organisation);
			organisationList.stream().forEach(org -> System.out.println(org));	
		} catch (ContracticModelException e) {
			e.printStackTrace();
			throw e;
		}
			
	}

}
