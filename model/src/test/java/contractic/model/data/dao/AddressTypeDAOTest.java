package contractic.model.data.dao;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.store.AddressType;

public class AddressTypeDAOTest extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreate() throws ContracticModelException {
	}

	@Test
	public void testRead() throws ContracticModelException {
		final AddressType addressType = new AddressType();
		final AddressTypeDAO op = new AddressTypeDAO();
		
		try {
			List<AddressType> list = op.read(addressType);
			assertTrue(list !=null && list.size()>0);
		} catch (ContracticModelException e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Ignore
	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

}
