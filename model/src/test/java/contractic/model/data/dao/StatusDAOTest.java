package contractic.model.data.dao;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Module;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Status;

public class StatusDAOTest extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreate() throws ContracticModelException {
		
		final OrganisationDAO orgDAO = new OrganisationDAO();
		List<Organisation> organisationList = orgDAO.read(new Organisation());
		
		final ModuleDAO moduleDAO = new ModuleDAO();
		List<Module> moduleList = moduleDAO.read(new Module());
		
		final Status status = new Status();
		final StatusDAO op = new StatusDAO();
		
		status.setModule(moduleList.get(0));
		status.setOrganisation(organisationList.get(0));
		status.setName("Complete");
		status.setActive(true);
		
		try {
			op.create(status);
			Assert.assertNotNull(status.getId());
		} catch (ContracticModelException e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Ignore
	@Test
	public void testRead() throws ContracticModelException {
		final Status status = new Status();
		final StatusDAO op = new StatusDAO();
		
		try {
			List<Status> list = op.read(status);
			assertTrue(list !=null && list.size()>0);
		} catch (ContracticModelException e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Ignore
	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

}
