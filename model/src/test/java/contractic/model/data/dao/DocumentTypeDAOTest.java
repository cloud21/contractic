package contractic.model.data.dao;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.store.DocumentType;
import contractic.model.data.store.Organisation;

public class DocumentTypeDAOTest extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testCreate() throws ContracticModelException {
		final DocumentType documentType = new DocumentType();
		final DocumentTypeDAO op = new DocumentTypeDAO();
		
		final OrganisationDAO orgDAO = new OrganisationDAO();
		List<Organisation> organisationList = orgDAO.read(new Organisation());
		Assert.assertTrue(organisationList.size() > 0);
		
		documentType.setName("SCHD"); // Create random letters
		documentType.setDescription("Contract Schedule");
		documentType.setOrganisation(organisationList.get(0));
		documentType.setActive(true);
		
		op.create(documentType);
		Assert.assertTrue(documentType.getId()!=0);
	}

	//@Ignore
	@Test
	public void testRead() throws ContracticModelException {
		final DocumentType documentType = new DocumentType();
		final DocumentTypeDAO op = new DocumentTypeDAO();
		
		// Test reading all
		List<DocumentType> list = op.read(documentType);
		Assert.assertTrue(list !=null && list.size()>0);
		
		final OrganisationDAO orgDAO = new OrganisationDAO();
		List<Organisation> organisationList = orgDAO.read(new Organisation());
		Assert.assertTrue(organisationList.size() > 0);
		
		// Test reading for a specific organisation
		documentType.setOrganisation(organisationList.get(0));
		list = op.read(documentType);
		Assert.assertTrue(list !=null && list.size()>0);
	}

	@Ignore
	@Test
	public void testUpdate() {
		Assert.fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testDelete() {
		Assert.fail("Not yet implemented");
	}

}
