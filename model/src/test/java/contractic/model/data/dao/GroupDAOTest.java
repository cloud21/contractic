package contractic.model.data.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Group;
import contractic.model.data.store.GroupSet;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.User;

public class GroupDAOTest extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testCreate() throws ContracticModelException {
		
		Organisation org = new Organisation();
		org.setCode("croydonnhs");
		CrudOperation<Organisation> op = new OrganisationDAO();
		List<Organisation> orgList = op.read(org);
		
		assertEquals(orgList.size(), 1);
		org = orgList.get(0);
		
	}

	//@Ignore
	@Test
	public void testRead() throws ContracticModelException {
		
		// Get the organisation
		Organisation organisation = new Organisation();
		organisation.setCode("homeoffice");
		CrudOperation<Organisation> op = new OrganisationDAO();
		List<Organisation> orgList = op.read(organisation);
		assertEquals(1, orgList.size());
		organisation = orgList.get(0);
		
		GroupSet groupSet = new GroupSet();
		groupSet.setName("DEPARTMENT");
		groupSet.setOrganisation(organisation);
		
		CrudOperation<GroupSet> gsop = new GroupSetDAO();
		List<GroupSet> groupSetList = gsop.read(groupSet);
		assertEquals(1, groupSetList.size());
		groupSet = groupSetList.get(0);
		
		Group group = new Group();
		group.setGroupSet(groupSet);
		CrudOperation<Group> gop = new GroupDAO();
		List<Group> groupList = gop.read(group);
		assertTrue(groupList.size()>0);
		group = groupList.get(0);
		
	}

	@Ignore
	@Test
	public void testUpdate() throws ContracticModelException {
		User user = new User();
		user.setLoginId("michael.sekamanya@creditlte.com");
		
		UserDAO userDAO = new UserDAO();
		List<User> userList = userDAO.read(user);
		
		assertEquals(userList.size(), 1);
		
		user = userList.get(0);
		Boolean oldValue = user.getActive();
		
		user.setActive(!oldValue);
		
		CrudOperation<User> userOP = new UserDAO();
		try{
			userOP.update(user);
		}catch(ContracticModelException ex){
			ex.printStackTrace();
			throw ex;
		}
		//assertNotNull(user.getId());
	}

	@Ignore
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

}
