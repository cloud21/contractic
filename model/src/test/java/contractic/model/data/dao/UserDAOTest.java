package contractic.model.data.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import contractic.model.ContracticModelException;
import contractic.model.data.store.Organisation;
import contractic.model.data.store.Person;
import contractic.model.data.store.User;

public class UserDAOTest extends BaseDAOTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	//@Ignore
	@Test
	public void testCreate() throws ContracticModelException {
		
		Organisation org = new Organisation();
		org.setCode("croydonnhs");
		CrudOperation<Organisation> op = new OrganisationDAO();
		List<Organisation> orgList = op.read(org);
		
		assertEquals(orgList.size(), 1);
		org = orgList.get(0);
		
		Person person = new Person();
		person.setOrganisation(org);
		CrudOperation<Person> personOP = new PersonDAO();
		try{
			List<Person> personList = personOP.read(person);
			assertEquals(personList.size(), 1);
			person = personList.get(0);
		}catch(ContracticModelException ex){
			ex.printStackTrace();
			throw ex;
		}
		
		User user = new User();
		user.setLoginId("Kim.Hillman@cloud21.net");
		user.setPassword("$2a$10$kB.533oyDw7ld6eurfO.s.wAX1pyL4v3pMa7uXJpC34LtkWUxJjc.");
		user.setPerson(person);
		user.setActive(true);
		
		CrudOperation<User> userOP = new UserDAO();
		try{
			userOP.create(user);
		}catch(ContracticModelException ex){
			ex.printStackTrace();
			throw ex;
		}
		assertNotNull(user.getId());
	}

	@Ignore
	@Test
	public void testRead() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testUpdate() throws ContracticModelException {
		User user = new User();
		user.setLoginId("michael.sekamanya@creditlte.com");
		
		UserDAO userDAO = new UserDAO();
		List<User> userList = userDAO.read(user);
		
		assertEquals(userList.size(), 1);
		
		user = userList.get(0);
		Boolean oldValue = user.getActive();
		
		user.setActive(!oldValue);
		
		CrudOperation<User> userOP = new UserDAO();
		try{
			userOP.update(user);
		}catch(ContracticModelException ex){
			ex.printStackTrace();
			throw ex;
		}
		//assertNotNull(user.getId());
	}

	@Ignore
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

}
