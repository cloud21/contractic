import csv
import json
import requests
import random
import traceback
import time

url = 'http://cloud21.localhost:8080/contractic'
username = 'username@domain.com'
password = 'password'

def get_session(username, password):
	try:
		r = requests.post('%s/session' % url, 
                          json={"username": username, "password": password})
		print (r)
		return json.loads(r.content)
	except requests.ConnectionError as ce:
		traceback.print_stack()
		raise

def delete_session(session):
    r = requests.delete('%s/session' % url, 
                        headers = {'Authorization' : session['token']},
                        json={'token': session['token'], 'username': session['username']})

def get_collection(session, name, params):
    endpoint = '%s/organisation/%s/%s' % (url, session['user']['organisation']['id'], name)
    r = requests.get(endpoint,
        headers = {'Authorization' : session['token']},
        params = params)
    return json.loads(r.content)

def post_collection(session, name, item):
    endpoint = '%s/organisation/%s/%s' % (url, session['user']['organisation']['id'], name)
    headers = {'Authorization': session['token']}
    r = requests.post(endpoint, json=item, headers=headers)
    return json.loads(r.content)
	
def put_collection(session, name, item):
    endpoint = '%s/organisation/%s/%s' % (url, session['user']['organisation']['id'], name)
    headers = {'Authorization': session['token']}
    r = requests.put(endpoint, json=item, headers=headers)
    return json.loads(r.content)

def get_contracts(session):
	return get_collection(session, 'contract', {})

def get_contracttypes(session):
    return get_collection(session, 'contracttype', {})

def get_suppliers(session):
    return get_collection(session, 'supplier', {})

def get_persons(session):
    return get_collection(session, 'person', {})

def get_groupsets(session):
    return get_collection(session, 'groupset', {})

def get_modules(session):
    endpoint = '%s/module' % url
    r = requests.get(endpoint, headers = {'Authorization' : session['token']})
    return json.loads(r.content)

def get_procedures(session, groupsets):
    proc_groupset = [gs for gs in groupsets if gs['name'] == 'PROCEDURE'];
    params = {'groupSetId': proc_groupset[0]['id']}
    return get_collection(session, 'group', params)

def get_statuses(session, module_name, modules):
    module = [m for m in modules if m['name'] == module_name];
    params = {'moduleId': module[0]['id']}
    return get_collection(session, 'status', params)

def post_file(path, session):
	files = {'file' : open(path, 'rb')}
	headers = {'Authorization': session}
	r = requests.post('%s/file' % url, 
		files=files, headers = headers)
	return json.loads(r.content)

def post_document(document, session):
	headers = {'Authorization': session}
	r = requests.post('%s/document' % url, json=document, 
		headers=headers)
	return json.loads(r.content)

def post_contract(session, contract):
    return post_collection(session, 'contract', contract)
	
def put_contract(session, contract):
    return put_collection(session, 'contract', contract)

def post_contractdetail(session, detail):
    return post_collection(session, 'contractdetail', detail)

def post_contracts():

		groupsets = get_groupsets(session)

		contracttypes = get_contracttypes(session)
		procedures = get_procedures(session, groupsets)
		persons = get_persons(session)
		suppliers = get_suppliers(session)
		modules = get_modules(session)
		statuses = get_statuses(session, 'Contract', modules)
		currencies = ['GBP','USD','EUR']

		try:
				f = open('c:\\users\\michael sekamanya\\projects\\contractic\\client.py\\contract.csv', 'rt')
				reader = csv.reader(f)
				reader.next()
				for row in reader:
					title = row[0]
					description = row[1]

					contract = {}
					contract['id'] = None
					contract['supplier'] = {'id': random.choice(suppliers)['id']}
					contract['owner'] = {'id': random.choice(persons)['id']}
					contract['type'] = {'id': random.choice(contracttypes)['id']}
					contract['status'] = {'id': random.choice(statuses)['id']}
					contract['department'] = {'id': session['user']['person']['department'][0]['id']}
					contract['term'] = []
					contract['procedure'] = {'id': random.choice(procedures)['id']}
					contract['title'] = title[:255]
					contract['description'] = description[:4096]
					contract['startDate'] = '20/01/2015'
					contract['initialValue'] = "150000"
					contract['valueUOM'] = random.choice(currencies)

					response = post_contract(session, contract)
					if not response or not response['ref']:
						print "Failed to create contract '%s'" % title 
					contract = response['ref']
					#post_contractsummary(contract)
					time.sleep(random.randint(1,5))
		finally:
				f.close()
				

	
def post_contractsummary(contract):
	contractdetail = {
	        'contract' : {'id': contract['id'], 'value': None},
	        'group' : {'id': None, 'value': 'SUMMARY'},
	        'name' : 'Section 2',
	        'text' : 'Summary Text'
	}

	contractdetail = post_contractdetail(session, contractdetail)

	
try:
	session = get_session(username, password)
	session = session['ref']
	#post_contracts()
	contracts = get_contracts(session)
	#post_contractsummary(contracts[0])
	for contract in contracts:
		put_contract(session, contract)
		time.sleep(random.randint(1,5))
	# post_contractsummary(contracts[0])
	
finally:
    delete_session(session)